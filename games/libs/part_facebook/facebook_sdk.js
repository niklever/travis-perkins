//local : 914263968645659',
//dev   :  524012927755821
//live : 1702124053351449 
window.fbAsyncInit = function () {
    FB.init({
        appId: '1702124053351449',
        xfbml: true,
        version: 'v2.4'
    });
};
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


var FacebookSDK = {
    loginFacebook : function(){
        FB.login(function(response) {
            FB.api('/me?fields=id,name,email,location,hometown,first_name,last_name', function(response) {
                console.log(response);
                /*"location": {
    "id": "108458769184495",
    "name": "Ho Chi Minh City, Vietnam"
  }*/
                var idUser = response.id;
                var email = response.email;
                var firstName = response.first_name;
                var lastName  = response.last_name;
                var image = 'https://graph.facebook.com/'+response.id+'/picture?type=normal';
                var location = response.location && response.location.name ? response.location.name : "N/A";
                
                var param = {
                    name : response.name ? response.name : firstName + " " + lastName,
                    email : email,
                    facebook_id : idUser,
                    country : location    
                };
                
                apiHelper.fblogin(param,exportRoot);

            });
        }, {scope: 'email'});
    },
    shareFacebook : function(){
        FB.ui( {
                method: 'feed',
                name: 'Travis Perkins',
                link:  'http://travisperkins.fbapphouse.com/games/',
                caption: 'Test Your Skills with the Young Grafters App by Travis Perkins',
                description: 'Test Your Skills with the Young Grafters App by Travis Perkins !',
                picture : 'http://travisperkins.fbapphouse.com/games/images/travis-perkins-logo.png',
                message: ''
            });
    }
}
