(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 960,
	height: 540,
	fps: 24,
	color: "#FFFFFF",
	manifest: [
		{src:"images/Boss_01__000.png", id:"Boss_01__000"},
		{src:"images/Boss_01__001.png", id:"Boss_01__001"},
		{src:"images/Boss_01__002.png", id:"Boss_01__002"},
		{src:"images/Boss_01__003.png", id:"Boss_01__003"},
		{src:"images/Boss_01__004.png", id:"Boss_01__004"},
		{src:"images/Boss_01__005.png", id:"Boss_01__005"},
		{src:"images/Boss_01__006.png", id:"Boss_01__006"},
		{src:"images/Boss_01__007.png", id:"Boss_01__007"},
		{src:"images/Boss_01__008.png", id:"Boss_01__008"},
		{src:"images/Boss_01__009.png", id:"Boss_01__009"},
		{src:"images/Boss_01__010.png", id:"Boss_01__010"},
		{src:"images/Boss_01__011.png", id:"Boss_01__011"},
		{src:"images/Boss_01__012.png", id:"Boss_01__012"},
		{src:"images/Boss_01__013.png", id:"Boss_01__013"},
		{src:"images/Boss_01__014.png", id:"Boss_01__014"},
		{src:"images/Boss_01__015.png", id:"Boss_01__015"},
		{src:"images/Boss_01__016.png", id:"Boss_01__016"},
		{src:"images/Boss_01__017.png", id:"Boss_01__017"},
		{src:"images/Boss_01__018.png", id:"Boss_01__018"},
		{src:"images/Boss_01__019.png", id:"Boss_01__019"},
		{src:"images/Boss_01__020.png", id:"Boss_01__020"},
		{src:"images/Boss_01__021.png", id:"Boss_01__021"},
		{src:"images/Boss_01__022.png", id:"Boss_01__022"},
		{src:"images/Boss_01__023.png", id:"Boss_01__023"},
		{src:"images/Boss_01__024.png", id:"Boss_01__024"},
		{src:"images/Boss_01__025.png", id:"Boss_01__025"},
		{src:"images/Boss_01__026.png", id:"Boss_01__026"},
		{src:"images/Health_and_Safety_Officer__000.png", id:"Health_and_Safety_Officer__000"},
		{src:"images/Health_and_Safety_Officer__001.png", id:"Health_and_Safety_Officer__001"},
		{src:"images/Health_and_Safety_Officer__002.png", id:"Health_and_Safety_Officer__002"},
		{src:"images/Health_and_Safety_Officer__003.png", id:"Health_and_Safety_Officer__003"},
		{src:"images/Health_and_Safety_Officer__004.png", id:"Health_and_Safety_Officer__004"},
		{src:"images/Health_and_Safety_Officer__005.png", id:"Health_and_Safety_Officer__005"},
		{src:"images/Health_and_Safety_Officer__006.png", id:"Health_and_Safety_Officer__006"},
		{src:"images/Health_and_Safety_Officer__007.png", id:"Health_and_Safety_Officer__007"},
		{src:"images/Health_and_Safety_Officer__008.png", id:"Health_and_Safety_Officer__008"},
		{src:"images/Health_and_Safety_Officer__009.png", id:"Health_and_Safety_Officer__009"},
		{src:"images/Health_and_Safety_Officer__010.png", id:"Health_and_Safety_Officer__010"},
		{src:"images/Health_and_Safety_Officer__011.png", id:"Health_and_Safety_Officer__011"},
		{src:"images/Health_and_Safety_Officer__012.png", id:"Health_and_Safety_Officer__012"},
		{src:"images/Health_and_Safety_Officer__013.png", id:"Health_and_Safety_Officer__013"},
		{src:"images/Health_and_Safety_Officer__014.png", id:"Health_and_Safety_Officer__014"},
		{src:"images/Health_and_Safety_Officer__015.png", id:"Health_and_Safety_Officer__015"},
		{src:"images/Health_and_Safety_Officer__016.png", id:"Health_and_Safety_Officer__016"},
		{src:"images/Health_and_Safety_Officer__017.png", id:"Health_and_Safety_Officer__017"},
		{src:"images/Health_and_Safety_Officer__018.png", id:"Health_and_Safety_Officer__018"},
		{src:"images/Health_and_Safety_Officer__019.png", id:"Health_and_Safety_Officer__019"},
		{src:"images/Health_and_Safety_Officer__020.png", id:"Health_and_Safety_Officer__020"},
		{src:"images/Health_and_Safety_Officer__021.png", id:"Health_and_Safety_Officer__021"},
		{src:"images/Health_and_Safety_Officer__022.png", id:"Health_and_Safety_Officer__022"},
		{src:"images/Health_and_Safety_Officer__023.png", id:"Health_and_Safety_Officer__023"},
		{src:"images/Health_and_Safety_Officer__024.png", id:"Health_and_Safety_Officer__024"},
		{src:"images/Health_and_Safety_Officer__025.png", id:"Health_and_Safety_Officer__025"},
		{src:"images/Health_and_Safety_Officer__026.png", id:"Health_and_Safety_Officer__026"}
	]
};



// symbols:



(lib.Boss_01__000 = function() {
	this.initialize(img.Boss_01__000);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__001 = function() {
	this.initialize(img.Boss_01__001);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__002 = function() {
	this.initialize(img.Boss_01__002);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__003 = function() {
	this.initialize(img.Boss_01__003);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__004 = function() {
	this.initialize(img.Boss_01__004);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__005 = function() {
	this.initialize(img.Boss_01__005);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__006 = function() {
	this.initialize(img.Boss_01__006);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__007 = function() {
	this.initialize(img.Boss_01__007);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__008 = function() {
	this.initialize(img.Boss_01__008);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__009 = function() {
	this.initialize(img.Boss_01__009);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__010 = function() {
	this.initialize(img.Boss_01__010);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__011 = function() {
	this.initialize(img.Boss_01__011);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__012 = function() {
	this.initialize(img.Boss_01__012);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__013 = function() {
	this.initialize(img.Boss_01__013);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__014 = function() {
	this.initialize(img.Boss_01__014);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__015 = function() {
	this.initialize(img.Boss_01__015);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__016 = function() {
	this.initialize(img.Boss_01__016);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__017 = function() {
	this.initialize(img.Boss_01__017);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__018 = function() {
	this.initialize(img.Boss_01__018);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__019 = function() {
	this.initialize(img.Boss_01__019);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__020 = function() {
	this.initialize(img.Boss_01__020);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__021 = function() {
	this.initialize(img.Boss_01__021);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__022 = function() {
	this.initialize(img.Boss_01__022);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__023 = function() {
	this.initialize(img.Boss_01__023);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__024 = function() {
	this.initialize(img.Boss_01__024);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__025 = function() {
	this.initialize(img.Boss_01__025);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__026 = function() {
	this.initialize(img.Boss_01__026);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Health_and_Safety_Officer__000 = function() {
	this.initialize(img.Health_and_Safety_Officer__000);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__001 = function() {
	this.initialize(img.Health_and_Safety_Officer__001);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__002 = function() {
	this.initialize(img.Health_and_Safety_Officer__002);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__003 = function() {
	this.initialize(img.Health_and_Safety_Officer__003);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__004 = function() {
	this.initialize(img.Health_and_Safety_Officer__004);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__005 = function() {
	this.initialize(img.Health_and_Safety_Officer__005);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__006 = function() {
	this.initialize(img.Health_and_Safety_Officer__006);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__007 = function() {
	this.initialize(img.Health_and_Safety_Officer__007);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__008 = function() {
	this.initialize(img.Health_and_Safety_Officer__008);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__009 = function() {
	this.initialize(img.Health_and_Safety_Officer__009);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__010 = function() {
	this.initialize(img.Health_and_Safety_Officer__010);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__011 = function() {
	this.initialize(img.Health_and_Safety_Officer__011);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__012 = function() {
	this.initialize(img.Health_and_Safety_Officer__012);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__013 = function() {
	this.initialize(img.Health_and_Safety_Officer__013);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__014 = function() {
	this.initialize(img.Health_and_Safety_Officer__014);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__015 = function() {
	this.initialize(img.Health_and_Safety_Officer__015);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__016 = function() {
	this.initialize(img.Health_and_Safety_Officer__016);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__017 = function() {
	this.initialize(img.Health_and_Safety_Officer__017);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__018 = function() {
	this.initialize(img.Health_and_Safety_Officer__018);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__019 = function() {
	this.initialize(img.Health_and_Safety_Officer__019);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__020 = function() {
	this.initialize(img.Health_and_Safety_Officer__020);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__021 = function() {
	this.initialize(img.Health_and_Safety_Officer__021);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__022 = function() {
	this.initialize(img.Health_and_Safety_Officer__022);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__023 = function() {
	this.initialize(img.Health_and_Safety_Officer__023);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__024 = function() {
	this.initialize(img.Health_and_Safety_Officer__024);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__025 = function() {
	this.initialize(img.Health_and_Safety_Officer__025);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Health_and_Safety_Officer__026 = function() {
	this.initialize(img.Health_and_Safety_Officer__026);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Yes_Btn = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("yes", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 112;
	this.text.setTransform(79.2,13.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-67.7,-28.5).lineTo(67.6,-28.5).curveTo(81.8,-28.5,81.8,-14.3).lineTo(81.8,14.3).curveTo(81.8,28.5,67.6,28.5).lineTo(-67.7,28.5).curveTo(-81.8,28.5,-81.8,14.3).lineTo(-81.8,-14.3).curveTo(-81.8,-28.5,-67.7,-28.5).closePath();
	this.shape.setTransform(81.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#7EEA9B").beginStroke().moveTo(-67.7,28.5).curveTo(-81.8,28.5,-81.8,14.3).lineTo(-81.8,-14.3).curveTo(-81.8,-28.5,-67.7,-28.5).lineTo(67.6,-28.5).curveTo(81.8,-28.5,81.8,-14.3).lineTo(81.8,14.3).curveTo(81.8,28.5,67.6,28.5).closePath();
	this.shape_1.setTransform(81.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("rgba(0,0,0,0.502)").beginStroke().moveTo(-67.7,28.5).curveTo(-81.8,28.5,-81.8,14.3).lineTo(-81.8,-14.3).curveTo(-81.8,-28.5,-67.7,-28.5).lineTo(67.6,-28.5).curveTo(81.8,-28.5,81.8,-14.3).lineTo(81.8,14.3).curveTo(81.8,28.5,67.6,28.5).closePath();
	this.shape_2.setTransform(90.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.WindmillSpin = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#B9B9B9").beginStroke().moveTo(-2.7,6.8).curveTo(-6.1,4.9,-9.6,1.9).lineTo(2.1,-10.3).curveTo(3.7,-9,5.7,-7.8).curveTo(7.3,-6.9,9.6,-5.9).lineTo(4.9,10.3).curveTo(0.6,8.7,-2.7,6.8).closePath();
	this.shape.setTransform(34.9,114.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#A9A9A9").beginStroke().moveTo(-8.4,7.8).lineTo(-4.3,-8.6).curveTo(-1.5,-8.3,0,-8.3).curveTo(1.5,-8.3,4.4,-8.6).lineTo(8.4,7.8).curveTo(4.3,8.6,0,8.6).curveTo(-4.3,8.6,-8.4,7.8).closePath();
	this.shape_1.setTransform(64.7,120.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#979797").beginStroke().moveTo(-9.6,-5.9).curveTo(-7.3,-6.9,-5.7,-7.8).curveTo(-3.8,-9,-2.1,-10.3).lineTo(9.6,1.9).curveTo(6.1,4.9,2.7,6.8).curveTo(-0.7,8.8,-4.9,10.3).closePath();
	this.shape_2.setTransform(94.5,114.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#868686").beginStroke().moveTo(-10.3,-2.1).curveTo(-9,-3.7,-7.9,-5.7).curveTo(-6.9,-7.3,-6,-9.6).lineTo(10.3,-4.9).curveTo(8.8,-0.7,6.7,2.7).curveTo(4.7,6.3,1.9,9.6).closePath();
	this.shape_3.setTransform(114.1,94.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#747474").beginStroke().moveTo(-8.6,4.4).curveTo(-8.3,1.5,-8.3,0).curveTo(-8.3,-1.5,-8.6,-4.3).lineTo(7.8,-8.4).curveTo(8.6,-4.3,8.6,0).curveTo(8.6,4.3,7.8,8.4).closePath();
	this.shape_4.setTransform(120.8,64.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#636363").beginStroke().moveTo(-7.9,5.7).curveTo(-9,3.8,-10.3,2.1).lineTo(1.9,-9.6).curveTo(4.9,-6.1,6.7,-2.7).curveTo(8.7,0.6,10.3,4.9).lineTo(-6,9.6).curveTo(-6.9,7.3,-7.9,5.7).closePath();
	this.shape_5.setTransform(114.1,34.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#5C5C5C").beginStroke().moveTo(-5.7,7.9).curveTo(-7.8,6.7,-9.6,6).lineTo(-4.9,-10.3).curveTo(-0.7,-8.8,2.7,-6.7).curveTo(6.1,-4.9,9.6,-1.9).lineTo(-2.1,10.3).curveTo(-3.8,9,-5.7,7.9).closePath();
	this.shape_6.setTransform(94.5,15.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#525252").beginStroke().moveTo(0,8.3).curveTo(-2.2,8.3,-4.3,8.6).lineTo(-8.4,-7.9).curveTo(-4.3,-8.6,0,-8.6).curveTo(4.3,-8.6,8.4,-7.9).lineTo(4.4,8.6).curveTo(2.2,8.3,0,8.3).closePath();
	this.shape_7.setTransform(64.7,8.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#CBCBCB").beginStroke().moveTo(-6.7,2.7).curveTo(-8.8,-0.7,-10.3,-4.9).lineTo(5.9,-9.6).curveTo(6.7,-7.8,7.8,-5.7).curveTo(9,-3.7,10.3,-2.1).lineTo(-1.9,9.6).curveTo(-4.8,6.3,-6.7,2.7).closePath();
	this.shape_8.setTransform(15.3,94.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#DCDCDC").beginStroke().moveTo(-8.6,0).curveTo(-8.6,-4.3,-7.8,-8.4).lineTo(8.6,-4.3).curveTo(8.3,-2,8.3,0).curveTo(8.3,2,8.6,4.4).lineTo(-7.8,8.4).curveTo(-8.6,4.3,-8.6,0).closePath();
	this.shape_9.setTransform(8.5,64.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#EDEDED").beginStroke().moveTo(-10.3,4.9).curveTo(-8.7,0.6,-6.7,-2.7).curveTo(-4.9,-6.1,-1.9,-9.6).lineTo(10.3,2.1).curveTo(9,3.8,7.8,5.7).curveTo(6.7,7.8,5.9,9.6).closePath();
	this.shape_10.setTransform(15.3,34.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#F6F6F6").beginStroke().moveTo(-9.6,-1.9).curveTo(-6.1,-4.9,-2.7,-6.7).curveTo(0.6,-8.7,4.9,-10.3).lineTo(9.6,6).curveTo(7.8,6.7,5.7,7.9).curveTo(3.7,9,2.1,10.3).closePath();
	this.shape_11.setTransform(34.9,15.4);

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,129.5,129.5);


(lib.WhiteCover = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("rgba(255,255,255,0.502)").beginStroke().moveTo(-296.1,221).lineTo(-296.1,-221.1).lineTo(296.1,-221.1).lineTo(296.1,221).closePath();
	this.shape.setTransform(296.1,221.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,592.2,442.1);


(lib.TW_butn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-25.1,15.8).curveTo(-23.9,16,-22.7,15.9).curveTo(-15.4,15.9,-9.9,11.5).curveTo(-13.2,11.4,-15.9,9.5).curveTo(-18.5,7.5,-19.5,4.3).curveTo(-18.7,4.5,-17.5,4.5).curveTo(-16.3,4.5,-14.8,4.2).curveTo(-18.4,3.5,-20.7,0.6).curveTo(-23.1,-2.2,-23.1,-5.9).lineTo(-23.1,-6).curveTo(-20.9,-4.8,-18.4,-4.8).curveTo(-20.5,-6.2,-21.8,-8.4).curveTo(-23,-10.7,-23,-13.3).curveTo(-23,-16.2,-21.6,-18.5).curveTo(-17.7,-13.8,-12.3,-11).curveTo(-6.7,-8.1,-0.4,-7.7).curveTo(-0.6,-9,-0.6,-10.1).curveTo(-0.6,-14.3,2.4,-17.4).curveTo(5.4,-20.4,9.7,-20.4).curveTo(14.2,-20.4,17.2,-17.2).curveTo(20.5,-17.8,23.7,-19.6).curveTo(22.6,-16.1,19.2,-13.9).curveTo(22.4,-14.4,25.1,-15.6).curveTo(23,-12.5,20,-10.3).lineTo(20,-8.9).curveTo(20,2.3,12.5,10.8).curveTo(4,20.4,-9.3,20.4).curveTo(-17.9,20.4,-25.1,15.8).closePath();
	this.shape.setTransform(19.2,19.2,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#23AADB").beginStroke().moveTo(-14.1,19.1).curveTo(-19.1,19.1,-19.1,14.1).lineTo(-19.1,-14.1).curveTo(-19.1,-19.1,-14.1,-19.1).lineTo(14.1,-19.1).curveTo(19.1,-19.1,19.1,-14.1).lineTo(19.1,14.1).curveTo(19.1,19.1,14.1,19.1).closePath();
	this.shape_1.setTransform(19,19.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.1,0.1,38.2,38.2);


(lib.TP_logoWhite = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#231F20").beginStroke().moveTo(-3.2,3.8).curveTo(-4.8,2.3,-4.8,0).lineTo(-4.8,-0).curveTo(-4.8,-2.3,-3.3,-3.8).curveTo(-1.7,-5.4,0.6,-5.4).curveTo(3.2,-5.4,4.8,-3.7).lineTo(3,-1.7).curveTo(1.9,-2.9,0.6,-2.9).curveTo(-0.5,-2.9,-1.2,-2).curveTo(-1.9,-1.2,-1.9,-0.1).lineTo(-1.9,-0).curveTo(-1.9,1.2,-1.2,2).curveTo(-0.5,2.8,0.7,2.8).curveTo(1.9,2.8,3.1,1.7).lineTo(4.8,3.5).curveTo(3.1,5.4,0.6,5.4).curveTo(-1.7,5.4,-3.2,3.8).closePath();
	this.shape.setTransform(567.8,26.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#231F20").beginStroke().moveTo(-1.5,7).lineTo(-1.5,-7).lineTo(1.5,-7).lineTo(1.5,7).closePath();
	this.shape_1.setTransform(559.8,24.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#231F20").beginStroke().moveTo(-5.4,6.8).lineTo(-5.4,-6.6).lineTo(-2.5,-6.6).lineTo(-2.5,-5.1).curveTo(-1.2,-6.8,0.7,-6.8).curveTo(2.6,-6.8,4,-5.5).curveTo(5.4,-3.9,5.4,-1.5).lineTo(5.4,-1.4).curveTo(5.4,1.1,4,2.5).curveTo(2.7,3.9,0.7,3.9).curveTo(-1.2,3.9,-2.5,2.3).lineTo(-2.5,6.8).closePath().moveTo(-1.8,-3.5).curveTo(-2.5,-2.8,-2.5,-1.5).lineTo(-2.5,-1.4).curveTo(-2.5,-0.1,-1.8,0.6).curveTo(-1.1,1.4,-0,1.4).curveTo(1,1.4,1.7,0.6).curveTo(2.5,-0.1,2.5,-1.4).lineTo(2.5,-1.5).curveTo(2.5,-2.8,1.7,-3.5).curveTo(1,-4.3,-0,-4.3).curveTo(-1.1,-4.3,-1.8,-3.5).closePath();
	this.shape_2.setTransform(551.1,27.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#CCB55E").beginStroke().moveTo(-17.7,17.7).curveTo(-25.1,10.4,-25.1,0).curveTo(-25.1,-10.4,-17.7,-17.7).curveTo(-10.3,-25,-0,-25.1).curveTo(10.4,-25,17.7,-17.7).curveTo(25.1,-10.4,25.1,0).curveTo(25.1,10.4,17.7,17.7).curveTo(10.4,25.1,-0,25).curveTo(-10.3,25.1,-17.7,17.7).closePath();
	this.shape_3.setTransform(558.2,25.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-8.2,20.4).curveTo(-13,18.7,-16.9,15.6).lineTo(-12.5,9).curveTo(-5.6,14.3,1.4,14.3).curveTo(4.5,14.3,6.3,13).curveTo(7.9,11.8,8,9.7).lineTo(8,9.5).curveTo(7.9,7.3,5.1,5.9).curveTo(3.8,5.2,-1.5,3.6).curveTo(-7.6,1.9,-10.5,-0.3).curveTo(-14.7,-3.4,-14.7,-8.9).lineTo(-14.7,-9).curveTo(-14.8,-14.9,-10.3,-18.5).curveTo(-6.1,-22,0.3,-22).curveTo(4.3,-22,8.4,-20.7).curveTo(12.5,-19.4,15.9,-17.1).lineTo(12,-10.2).curveTo(5.5,-14.2,0.1,-14.2).curveTo(-2.7,-14.2,-4.3,-13).curveTo(-5.8,-11.9,-5.8,-10).lineTo(-5.8,-9.8).curveTo(-5.8,-7.8,-2.9,-6.3).curveTo(-1.4,-5.5,3.7,-3.9).curveTo(9.7,-2,12.6,0.1).curveTo(16.9,3.3,16.9,8.5).lineTo(16.9,8.6).curveTo(16.9,15.1,12.3,18.6).curveTo(8,22,1.1,22).curveTo(-3.5,22,-8.2,20.4).closePath();
	this.shape_4.setTransform(502.5,46.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#FFFFFF").beginStroke().moveTo(9.3,21.7).lineTo(9.3,-2.5).curveTo(9.3,-7.4,6.9,-10.1).curveTo(4.6,-12.8,0.2,-12.8).curveTo(-4,-12.8,-6.6,-10).curveTo(-9.3,-7.2,-9.3,-2.3).lineTo(-9.3,21.7).lineTo(-19,21.7).lineTo(-19,-20.7).lineTo(-9.3,-20.7).lineTo(-9.3,-14.2).curveTo(-4.3,-21.6,3.9,-21.6).curveTo(11,-21.6,15.1,-17.2).curveTo(19,-12.8,19,-5.3).lineTo(19,21.7).closePath();
	this.shape_5.setTransform(460.4,46.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-4.8,29.1).lineTo(-4.8,-13.3).lineTo(4.9,-13.3).lineTo(4.9,29.1).closePath().moveTo(-5.2,-19.8).lineTo(-5.2,-29.1).lineTo(5.2,-29.1).lineTo(5.2,-19.8).closePath();
	this.shape_6.setTransform(425.8,38.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#FFFFFF").beginStroke().moveTo(8.6,29.3).lineTo(-4.2,11).lineTo(-10.1,17.2).lineTo(-10.1,29.3).lineTo(-19.8,29.3).lineTo(-19.8,-29.3).lineTo(-10.1,-29.3).lineTo(-10.1,5.7).lineTo(7.5,-13.1).lineTo(19.3,-13.1).lineTo(2.4,4.2).lineTo(19.8,29.3).closePath();
	this.shape_7.setTransform(395.5,38.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-12.4,21.6).lineTo(-12.4,-20.8).lineTo(-2.6,-20.8).lineTo(-2.6,-11.2).curveTo(-0.6,-16.2,3,-18.9).curveTo(7,-21.8,12.3,-21.6).lineTo(12.3,-11.3).lineTo(11.8,-11.3).curveTo(5.2,-11.3,1.4,-7.1).curveTo(-2.6,-2.7,-2.6,5.5).lineTo(-2.6,21.6).closePath();
	this.shape_8.setTransform(356.9,46.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-14.3,16).curveTo(-20.5,9.8,-20.5,0.1).lineTo(-20.5,-0).curveTo(-20.5,-9.3,-14.7,-15.6).curveTo(-8.8,-22.1,0.2,-22.1).curveTo(9.9,-22.1,15.4,-15.3).curveTo(20.5,-9,20.5,0.7).lineTo(20.4,3.4).lineTo(-10.8,3.4).curveTo(-10.1,8.5,-6.6,11.4).curveTo(-3.4,14.2,1.4,14.2).curveTo(4.8,14.2,7.7,12.9).curveTo(10.2,11.7,12.9,9.1).lineTo(18.6,14.2).curveTo(11.9,22.1,1.2,22.1).curveTo(-8.1,22.1,-14.3,16).closePath().moveTo(-7.3,-11.1).curveTo(-10.2,-8,-10.9,-3).lineTo(10.8,-3).curveTo(10.4,-7.9,7.7,-10.9).curveTo(4.7,-14.2,0.1,-14.2).curveTo(-4.3,-14.2,-7.3,-11.1).closePath();
	this.shape_9.setTransform(316.2,46.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-21.7,28.1).lineTo(-21.7,-28.1).lineTo(0.4,-28.1).curveTo(10.2,-28.1,16,-22.9).curveTo(21.7,-17.8,21.8,-9.3).lineTo(21.8,-9.2).curveTo(21.8,-0,15.2,5.1).curveTo(9,10,-0.7,10).lineTo(-11.9,10).lineTo(-11.9,28.1).closePath().moveTo(-11.9,1.1).lineTo(-0.4,1.1).curveTo(5.2,1.1,8.4,-1.7).curveTo(11.8,-4.5,11.8,-9).lineTo(11.8,-9.1).curveTo(11.8,-14,8.4,-16.6).curveTo(5.3,-19.2,-0.4,-19.1).lineTo(-11.9,-19.1).closePath();
	this.shape_10.setTransform(269.7,39.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-8.2,20.4).curveTo(-12.9,18.7,-16.8,15.6).lineTo(-12.5,9).curveTo(-5.6,14.3,1.4,14.3).curveTo(4.4,14.3,6.2,13).curveTo(8,11.8,7.9,9.7).lineTo(7.9,9.5).curveTo(8,7.3,5.1,5.9).curveTo(3.8,5.2,-1.5,3.6).curveTo(-7.6,1.9,-10.5,-0.3).curveTo(-14.8,-3.4,-14.8,-8.9).lineTo(-14.8,-9).curveTo(-14.7,-14.9,-10.3,-18.5).curveTo(-6.2,-22,0.4,-22).curveTo(4.3,-22,8.5,-20.7).curveTo(12.5,-19.4,15.9,-17.1).lineTo(12.1,-10.2).curveTo(5.5,-14.2,0.1,-14.2).curveTo(-2.7,-14.2,-4.3,-13).curveTo(-5.8,-11.9,-5.8,-10).lineTo(-5.8,-9.8).curveTo(-5.8,-7.8,-3,-6.3).curveTo(-1.4,-5.5,3.7,-3.9).curveTo(9.7,-2,12.6,0.1).curveTo(16.8,3.3,16.8,8.5).lineTo(16.8,8.6).curveTo(16.8,15.1,12.3,18.6).curveTo(8,22,1.2,22).curveTo(-3.5,22,-8.2,20.4).closePath();
	this.shape_11.setTransform(200,46.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-4.8,29.1).lineTo(-4.8,-13.3).lineTo(4.9,-13.3).lineTo(4.9,29.1).closePath().moveTo(-5.2,-19.8).lineTo(-5.2,-29.1).lineTo(5.2,-29.1).lineTo(5.2,-19.8).closePath();
	this.shape_12.setTransform(171,38.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-4.3,21.3).lineTo(-21.8,-21.3).lineTo(-11.3,-21.3).lineTo(0.1,9.8).lineTo(11.6,-21.3).lineTo(21.8,-21.3).lineTo(4.4,21.3).closePath();
	this.shape_13.setTransform(137.6,47);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-14.7,18.6).curveTo(-19.2,15,-19.2,8.9).lineTo(-19.2,8.7).curveTo(-19.2,2,-14.4,-1.8).curveTo(-9.8,-5.1,-2,-5.2).curveTo(4.3,-5.2,9.7,-3.4).lineTo(9.7,-4.2).curveTo(9.6,-8.6,7,-10.9).curveTo(4.3,-13.3,-0.8,-13.3).curveTo(-6.5,-13.3,-12.9,-10.7).lineTo(-15.5,-18.4).curveTo(-11.6,-20.2,-8.3,-20.9).curveTo(-4.2,-21.9,0.6,-21.9).curveTo(9.9,-22,14.6,-17.2).curveTo(19.3,-12.7,19.3,-4).lineTo(19.3,21.1).lineTo(9.6,21.1).lineTo(9.6,15.9).curveTo(4.5,21.9,-4.1,21.9).curveTo(-10.5,22,-14.7,18.6).closePath().moveTo(-7.1,3.4).curveTo(-9.6,5.2,-9.6,8.2).lineTo(-9.6,8.4).curveTo(-9.6,11.3,-7.2,13.1).curveTo(-5,14.6,-1.4,14.6).curveTo(3.4,14.6,6.7,12.2).curveTo(9.8,9.7,9.8,5.7).lineTo(9.8,3.4).curveTo(5.4,1.7,0.1,1.7).curveTo(-4.5,1.7,-7.1,3.4).closePath();
	this.shape_14.setTransform(92.7,46.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-12.4,21.6).lineTo(-12.4,-20.8).lineTo(-2.7,-20.8).lineTo(-2.7,-11.2).curveTo(-0.6,-16.3,3,-18.9).curveTo(7,-21.8,12.4,-21.6).lineTo(12.4,-11.3).lineTo(11.8,-11.3).curveTo(5.2,-11.3,1.4,-7.1).curveTo(-2.7,-2.7,-2.7,5.5).lineTo(-2.7,21.6).closePath();
	this.shape_15.setTransform(59.5,46.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-5,28.1).lineTo(-5,-19).lineTo(-22.8,-19).lineTo(-22.8,-28.1).lineTo(22.8,-28.1).lineTo(22.8,-19).lineTo(5,-19).lineTo(5,28.1).closePath();
	this.shape_16.setTransform(22.8,39.9);

	this.addChild(this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,583.3,69);


(lib.TP_logo_white = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-17.7,17.7).curveTo(-25.1,10.4,-25,-0).curveTo(-25.1,-10.4,-17.7,-17.7).curveTo(-10.3,-25.1,-0,-25.1).curveTo(10.4,-25.1,17.7,-17.7).curveTo(25,-10.4,25.1,-0).curveTo(25,10.4,17.7,17.7).curveTo(10.4,25,-0,25.1).curveTo(-10.3,25,-17.7,17.7).closePath().moveTo(-12.5,9.2).lineTo(-9.7,9.2).lineTo(-9.7,4.8).curveTo(-8.3,6.4,-6.4,6.4).curveTo(-4.4,6.3,-3.2,4.9).curveTo(-1.7,3.5,-1.7,1).lineTo(-1.7,1).curveTo(-1.7,-1.5,-3.2,-3).curveTo(-4.5,-4.4,-6.4,-4.4).curveTo(-8.3,-4.4,-9.7,-2.7).lineTo(-9.7,-4.2).lineTo(-12.5,-4.2).closePath().moveTo(6.3,-2.8).curveTo(4.8,-1.2,4.8,1).lineTo(4.8,1.1).curveTo(4.8,3.3,6.4,4.8).curveTo(7.9,6.4,10.2,6.4).curveTo(12.6,6.4,14.4,4.5).lineTo(12.7,2.8).curveTo(11.5,3.9,10.3,3.8).curveTo(9.1,3.9,8.4,3).curveTo(7.7,2.3,7.7,1).lineTo(7.7,1).curveTo(7.7,-0.2,8.4,-1).curveTo(9.1,-1.8,10.2,-1.8).curveTo(11.5,-1.8,12.6,-0.7).lineTo(14.4,-2.6).curveTo(12.8,-4.3,10.2,-4.4).curveTo(7.8,-4.4,6.3,-2.8).closePath().moveTo(0.1,6.2).lineTo(3,6.2).lineTo(3,-7.9).lineTo(0.1,-7.9).closePath().moveTo(-8.9,3.1).curveTo(-9.6,2.3,-9.7,1).lineTo(-9.7,1).curveTo(-9.6,-0.3,-8.9,-1.1).curveTo(-8.2,-1.9,-7.1,-1.9).curveTo(-6.1,-1.9,-5.4,-1.1).curveTo(-4.6,-0.3,-4.6,1).lineTo(-4.6,1).curveTo(-4.6,2.3,-5.4,3.1).curveTo(-6.1,3.8,-7.1,3.8).curveTo(-8.2,3.8,-8.9,3.1).closePath();
	this.shape.setTransform(558.2,25.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#231F20").beginStroke().moveTo(15.6,23.7).curveTo(9.4,17.5,9.4,7.8).lineTo(9.4,7.6).curveTo(9.4,-1.6,15.2,-7.9).curveTo(21.1,-14.5,30.1,-14.5).curveTo(39.8,-14.5,45.3,-7.6).curveTo(50.4,-1.4,50.4,8.4).lineTo(50.2,11.1).lineTo(19.1,11.1).curveTo(19.9,16.2,23.2,19.1).curveTo(26.5,21.9,31.3,21.9).curveTo(34.7,21.8,37.5,20.5).curveTo(40.2,19.4,42.8,16.8).lineTo(48.5,21.9).curveTo(41.8,29.8,31.1,29.8).curveTo(21.8,29.8,15.6,23.7).closePath().moveTo(22.6,-3.4).curveTo(19.7,-0.4,19,4.7).lineTo(40.8,4.7).curveTo(40.3,-0.3,37.5,-3.2).curveTo(34.7,-6.5,30,-6.5).curveTo(25.6,-6.5,22.6,-3.4).closePath().moveTo(-208.4,26.4).curveTo(-212.9,22.8,-212.9,16.6).lineTo(-212.9,16.5).curveTo(-212.9,9.7,-208,6).curveTo(-203.5,2.6,-195.7,2.6).curveTo(-189.4,2.6,-184,4.3).lineTo(-184,3.5).curveTo(-184,-0.9,-186.7,-3.2).curveTo(-189.3,-5.5,-194.4,-5.6).curveTo(-200.1,-5.6,-206.6,-2.9).lineTo(-209.2,-10.6).curveTo(-205.3,-12.4,-202,-13.2).curveTo(-197.8,-14.2,-193.1,-14.2).curveTo(-183.8,-14.2,-179,-9.5).curveTo(-174.4,-4.9,-174.4,3.7).lineTo(-174.4,28.9).lineTo(-184.1,28.9).lineTo(-184.1,23.6).curveTo(-189.2,29.7,-197.8,29.7).curveTo(-204.1,29.7,-208.4,26.4).closePath().moveTo(-200.7,11.2).curveTo(-203.3,12.9,-203.2,16).lineTo(-203.2,16.1).curveTo(-203.2,19.1,-200.9,20.8).curveTo(-198.6,22.4,-195,22.4).curveTo(-190.2,22.4,-187,20).curveTo(-183.8,17.5,-183.8,13.5).lineTo(-183.8,11.1).curveTo(-188.3,9.4,-193.5,9.4).curveTo(-198.1,9.4,-200.7,11.2).closePath().moveTo(208.1,28.1).curveTo(203.2,26.3,199.3,23.3).lineTo(203.7,16.7).curveTo(210.6,21.9,217.6,22).curveTo(220.7,22,222.5,20.7).curveTo(224.2,19.4,224.2,17.4).lineTo(224.2,17.2).curveTo(224.2,15,221.3,13.6).curveTo(220,12.9,214.7,11.2).curveTo(208.6,9.6,205.7,7.4).curveTo(201.4,4.3,201.4,-1.2).lineTo(201.4,-1.4).curveTo(201.4,-7.2,205.8,-10.9).curveTo(210,-14.3,216.5,-14.3).curveTo(220.5,-14.3,224.6,-13).curveTo(228.7,-11.7,232.1,-9.5).lineTo(228.2,-2.5).curveTo(221.7,-6.5,216.3,-6.6).curveTo(213.5,-6.5,211.9,-5.3).curveTo(210.3,-4.2,210.3,-2.3).lineTo(210.3,-2.2).curveTo(210.3,-0.1,213.3,1.4).curveTo(214.8,2.2,219.9,3.8).curveTo(225.9,5.7,228.8,7.8).curveTo(233,11,233.1,16.1).lineTo(233.1,16.3).curveTo(233,22.8,228.5,26.3).curveTo(224.2,29.7,217.3,29.6).curveTo(212.7,29.6,208.1,28.1).closePath().moveTo(-94.5,28.1).curveTo(-99.3,26.3,-103.2,23.3).lineTo(-98.8,16.7).curveTo(-91.9,21.9,-85,22).curveTo(-81.9,22,-80.1,20.7).curveTo(-78.4,19.5,-78.4,17.4).lineTo(-78.4,17.2).curveTo(-78.4,15,-81.3,13.6).curveTo(-82.6,12.9,-87.9,11.2).curveTo(-94,9.6,-96.9,7.4).curveTo(-101.1,4.3,-101.1,-1.2).lineTo(-101.1,-1.4).curveTo(-101.1,-7.2,-96.7,-10.9).curveTo(-92.5,-14.3,-86,-14.3).curveTo(-82.1,-14.3,-77.9,-13).curveTo(-73.8,-11.7,-70.5,-9.5).lineTo(-74.3,-2.5).curveTo(-80.8,-6.5,-86.2,-6.6).curveTo(-89,-6.5,-90.6,-5.3).curveTo(-92.2,-4.2,-92.2,-2.3).lineTo(-92.2,-2.2).curveTo(-92.2,-0.1,-89.3,1.4).curveTo(-87.8,2.2,-82.6,3.8).curveTo(-76.6,5.7,-73.7,7.8).curveTo(-69.5,11,-69.5,16.1).lineTo(-69.5,16.3).curveTo(-69.5,22.8,-74.1,26.3).curveTo(-78.3,29.7,-85.2,29.6).curveTo(-89.8,29.6,-94.5,28.1).closePath().moveTo(-153,29.2).lineTo(-170.5,-13.6).lineTo(-160,-13.6).lineTo(-148.7,17.6).lineTo(-137.2,-13.6).lineTo(-126.9,-13.6).lineTo(-144.3,29.2).closePath().moveTo(183.3,28.9).lineTo(183.3,4.8).curveTo(183.3,-0.2,181,-2.9).curveTo(178.6,-5.6,174.3,-5.6).curveTo(170.1,-5.6,167.5,-2.8).curveTo(164.8,0.1,164.8,4.9).lineTo(164.8,28.9).lineTo(155.1,28.9).lineTo(155.1,-13.6).lineTo(164.8,-13.6).lineTo(164.8,-6.9).curveTo(169.7,-14.5,178,-14.5).curveTo(185.1,-14.4,189.2,-10).curveTo(193.1,-5.6,193.1,1.9).lineTo(193.1,28.9).closePath().moveTo(134.6,28.9).lineTo(134.6,-13.6).lineTo(144.3,-13.6).lineTo(144.3,28.9).closePath().moveTo(117.7,28.9).lineTo(105,10.5).lineTo(99.1,16.7).lineTo(99.1,28.9).lineTo(89.4,28.9).lineTo(89.4,-29.8).lineTo(99.1,-29.8).lineTo(99.1,5.2).lineTo(116.6,-13.6).lineTo(128.4,-13.6).lineTo(111.6,3.7).lineTo(129,28.9).closePath().moveTo(58.2,28.9).lineTo(58.2,-13.6).lineTo(67.9,-13.6).lineTo(67.9,-4).curveTo(70,-9,73.6,-11.7).curveTo(77.5,-14.6,82.9,-14.3).lineTo(82.9,-4.1).lineTo(82.3,-4.1).curveTo(75.7,-4.1,72,0.1).curveTo(67.9,4.5,67.9,12.7).lineTo(67.9,28.9).closePath().moveTo(-38.4,28.9).lineTo(-38.4,-27.4).lineTo(-16.2,-27.4).curveTo(-6.4,-27.3,-0.6,-22.2).curveTo(5.1,-17.1,5.1,-8.6).lineTo(5.1,-8.4).curveTo(5.1,0.7,-1.5,5.9).curveTo(-7.7,10.7,-17.4,10.7).lineTo(-28.5,10.7).lineTo(-28.5,28.9).closePath().moveTo(-28.5,1.9).lineTo(-17,1.9).curveTo(-11.5,1.9,-8.2,-1).curveTo(-4.9,-3.8,-4.9,-8.2).lineTo(-4.9,-8.4).curveTo(-4.9,-13.2,-8.2,-15.8).curveTo(-11.4,-18.4,-17,-18.4).lineTo(-28.5,-18.4).closePath().moveTo(-120.2,28.9).lineTo(-120.2,-13.6).lineTo(-110.4,-13.6).lineTo(-110.4,28.9).closePath().moveTo(-239.1,28.9).lineTo(-239.1,-13.6).lineTo(-229.4,-13.6).lineTo(-229.4,-4).curveTo(-227.4,-9,-223.8,-11.7).curveTo(-219.8,-14.6,-214.4,-14.3).lineTo(-214.4,-4.1).lineTo(-215,-4.1).curveTo(-221.6,-4.1,-225.4,0.1).curveTo(-229.4,4.5,-229.4,12.7).lineTo(-229.4,28.9).closePath().moveTo(-268.5,28.9).lineTo(-268.5,-18.2).lineTo(-286.3,-18.2).lineTo(-286.3,-27.4).lineTo(-240.7,-27.4).lineTo(-240.7,-18.2).lineTo(-258.5,-18.2).lineTo(-258.5,28.9).closePath().moveTo(259.4,-4.9).lineTo(259.4,-18.3).lineTo(262.2,-18.3).lineTo(262.2,-16.8).curveTo(263.6,-18.5,265.5,-18.5).curveTo(267.4,-18.5,268.7,-17.1).curveTo(270.2,-15.6,270.2,-13.1).lineTo(270.2,-13.1).curveTo(270.2,-10.6,268.7,-9.2).curveTo(267.5,-7.8,265.5,-7.7).curveTo(263.6,-7.7,262.2,-9.3).lineTo(262.2,-4.9).closePath().moveTo(263,-15.2).curveTo(262.3,-14.4,262.2,-13.1).lineTo(262.2,-13.1).curveTo(262.3,-11.8,263,-11).curveTo(263.7,-10.3,264.8,-10.3).curveTo(265.8,-10.3,266.5,-11).curveTo(267.3,-11.8,267.3,-13.1).lineTo(267.3,-13.1).curveTo(267.3,-14.4,266.5,-15.2).curveTo(265.8,-16,264.8,-16).curveTo(263.7,-16,263,-15.2).closePath().moveTo(278.3,-9.3).curveTo(276.7,-10.8,276.7,-13).lineTo(276.7,-13.1).curveTo(276.7,-15.3,278.2,-16.9).curveTo(279.7,-18.5,282.1,-18.5).curveTo(284.7,-18.4,286.3,-16.7).lineTo(284.5,-14.8).curveTo(283.4,-15.9,282.1,-15.9).curveTo(281,-15.9,280.3,-15.1).curveTo(279.6,-14.3,279.6,-13.1).lineTo(279.6,-13.1).curveTo(279.6,-11.8,280.3,-11.1).curveTo(281,-10.2,282.2,-10.3).curveTo(283.4,-10.2,284.6,-11.3).lineTo(286.3,-9.6).curveTo(284.5,-7.7,282.1,-7.7).curveTo(279.8,-7.7,278.3,-9.3).closePath().moveTo(272,-7.9).lineTo(272,-22).lineTo(274.9,-22).lineTo(274.9,-7.9).closePath().moveTo(134.3,-20.1).lineTo(134.3,-29.3).lineTo(144.6,-29.3).lineTo(144.6,-20.1).closePath().moveTo(-120.5,-20.1).lineTo(-120.5,-29.3).lineTo(-110.1,-29.3).lineTo(-110.1,-20.1).closePath();
	this.shape_1.setTransform(286.3,39.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,583.3,69);


(lib.Time = function() {
	this.initialize();

	// Layer 2
	this.mc_TimerCounter = new cjs.Text("1:35", "35px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_TimerCounter.name = "mc_TimerCounter";
	this.mc_TimerCounter.textAlign = "center";
	this.mc_TimerCounter.lineHeight = 37;
	this.mc_TimerCounter.setTransform(140,22.7);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(3,1,1).moveTo(64.8,27.1).lineTo(64.8,31.6).lineTo(60.1,31.6).lineTo(-60.3,31.6).lineTo(-64.8,31.6).lineTo(-64.8,27.1).lineTo(-64.8,-26.7).lineTo(-64.8,-31.7).lineTo(-60.3,-31.7).lineTo(60.1,-31.7).lineTo(64.8,-31.7).lineTo(64.8,-27.1).lineTo(64.8,-26.7).closePath();
	this.shape.setTransform(138.6,35.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#F4D96F").beginStroke().moveTo(-60.2,27.2).lineTo(-60.2,-27.2).lineTo(60.2,-27.2).lineTo(60.2,27.2).closePath().moveTo(-55.7,-1.3).lineTo(-55.7,14.9).lineTo(-55.7,22.7).lineTo(55.9,22.7).lineTo(55.9,-1.3).lineTo(55.9,-22.6).lineTo(-55.7,-22.6).closePath();
	this.shape_1.setTransform(138.5,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#474747").beginStroke().moveTo(-55.8,12).lineTo(-55.8,4.2).lineTo(-55.8,-12).lineTo(55.8,-12).lineTo(55.8,12).closePath();
	this.shape_2.setTransform(138.6,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#666666").beginStroke().moveTo(-55.8,10.7).lineTo(-55.8,-10.7).lineTo(55.8,-10.7).lineTo(55.8,10.7).closePath();
	this.shape_3.setTransform(138.6,23.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB55E").beginStroke().moveTo(60,31.6).lineTo(-60.4,31.6).lineTo(-64.8,31.6).lineTo(-64.8,27.1).lineTo(-64.8,-26.6).lineTo(-64.8,-31.7).lineTo(-60.4,-31.7).lineTo(60,-31.7).lineTo(64.8,-31.7).lineTo(64.8,-27.1).lineTo(64.8,-26.6).lineTo(64.8,27.1).lineTo(64.8,31.6).closePath().moveTo(-60.4,27.1).lineTo(60,27.1).lineTo(60,-27.3).lineTo(-60.4,-27.3).closePath();
	this.shape_4.setTransform(138.6,35.4);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.mc_TimerCounter);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(72.3,2.2,132.6,66.3);


(lib.TableLeg = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#C1A841").beginStroke().moveTo(-7,37.7).lineTo(-9.8,37.3).lineTo(-10.7,37.2).lineTo(-11.3,-37.8).lineTo(11.2,-30.4).curveTo(10,-26.3,9.8,-24.5).curveTo(9.4,-21.6,9.9,-16.5).curveTo(10.1,-14.2,10.1,5).curveTo(10.1,20.1,10.2,23.4).curveTo(10.4,31.7,11.2,37.8).closePath();
	this.shape.setTransform(26.7,50.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#A38E36").beginStroke().moveTo(2.7,38.4).curveTo(0.4,33.5,-5,25.5).lineTo(-6.3,23.4).curveTo(-6.2,12.3,-5.4,-5.6).lineTo(-4.6,-25.3).lineTo(-4.8,-43.7).lineTo(5.4,-31).lineTo(5.7,-31).lineTo(5.5,-31.2).curveTo(5.3,-31.4,5.5,-31.3).lineTo(5.8,-31.2).lineTo(6.4,43.8).closePath();
	this.shape_1.setTransform(9.6,44);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#26210C").beginStroke().moveTo(-0.8,50.5).curveTo(-2.6,50.4,-3.9,49.9).curveTo(-5,49.5,-5.6,49).curveTo(-6.1,48.6,-7.3,46.9).curveTo(-8.7,44.9,-9.4,44.1).curveTo(-10.3,43,-12.2,40).lineTo(-20,27.1).curveTo(-19.9,25.4,-19.8,20.1).lineTo(-19.8,19.7).curveTo(-20,17.7,-20,11.9).lineTo(-19.7,1.5).curveTo(-19.4,-6.3,-19.5,-8.2).lineTo(-18.9,-49.3).curveTo(-18.6,-49.7,-17.6,-50.1).lineTo(-16.7,-50.5).curveTo(-15.6,-47.9,-15.2,-42.8).lineTo(-15.1,-24.3).lineTo(-15.9,-4.6).curveTo(-16.7,13.3,-16.8,24.4).lineTo(-15.4,26.5).curveTo(-10,34.5,-7.8,39.4).lineTo(-4.1,44.7).lineTo(-3.2,44.9).lineTo(-0.4,45.3).lineTo(17.9,45.3).curveTo(17,39.2,16.8,30.9).curveTo(16.7,27.6,16.7,12.6).curveTo(16.7,-6.6,16.5,-9).curveTo(16,-14.1,16.4,-17).curveTo(16.6,-18.8,17.9,-22.8).lineTo(18.3,-23.3).curveTo(19.6,-22.9,20,-21.6).lineTo(20,47.4).lineTo(19.7,47.6).lineTo(19.6,50.5).closePath();
	this.shape_2.setTransform(20,43);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-7.5,40.1,101.1);


(lib.Symbol1 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-9.9,-6.2).curveTo(-4.5,-5.9,0,-9.9).curveTo(4.5,-5.9,10,-6.2).curveTo(10.3,6.4,0,10).curveTo(-10.3,6.4,-9.9,-6.2).closePath();
	this.shape.setTransform(23.9,24.2);

	this.text = new cjs.Text("Leaderboards", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(43.3,15.9,0.59,0.59);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#4F2A3F").beginStroke().moveTo(-103.7,19.3).curveTo(-108.8,19.3,-108.8,14.3).lineTo(-108.8,-14.3).curveTo(-108.8,-19.3,-103.7,-19.3).lineTo(103.7,-19.3).curveTo(108.8,-19.3,108.8,-14.3).lineTo(108.8,14.3).curveTo(108.8,19.3,103.7,19.3).closePath();
	this.shape_1.setTransform(113.8,24.2);

	this.addChild(this.shape_1,this.text,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(5,4.9,217.6,38.6);


(lib.Sure = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("are you sure you want to quit the game?", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 395;
	this.text.setTransform(135,-105.1,1.18,1.18);

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-292.6,98.7).lineTo(191.3,98.7).curveTo(205.5,98.7,205.5,84.5).lineTo(205.5,26.2).lineTo(306.8,-98.7).lineTo(205.5,-26.3).lineTo(205.5,-45).curveTo(205.5,-59.2,191.3,-59.2).lineTo(-292.6,-59.2).curveTo(-306.8,-59.2,-306.8,-45).lineTo(-306.8,84.5).curveTo(-306.8,98.7,-292.6,98.7).closePath();
	this.shape.setTransform(187.7,-76.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#EDED5D").beginStroke().moveTo(-292.6,98.7).curveTo(-306.8,98.6,-306.8,84.5).lineTo(-306.8,-45.1).curveTo(-306.8,-59.2,-292.6,-59.2).lineTo(191.3,-59.2).curveTo(205.5,-59.2,205.5,-45.1).lineTo(205.5,-26.3).lineTo(306.8,-98.6).lineTo(205.5,26.1).lineTo(205.5,84.5).curveTo(205.5,98.6,191.3,98.7).closePath();
	this.shape_1.setTransform(187.7,-76.1);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-121.1,-176.8,617.6,201.3);


(lib.shirt04Greg = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#594259").beginStroke().moveTo(6.8,3.5).curveTo(7.1,3.1,6.9,3).curveTo(6,2.2,-6.5,3).curveTo(-8.6,2.3,-9.4,0.2).curveTo(-9.6,-0.4,-9.9,-2.6).curveTo(-10.3,-5.4,-0.2,-2.1).curveTo(9.9,1.2,9.9,2.5).lineTo(9.9,3.1).lineTo(9.7,3.4).lineTo(9.4,3.6).curveTo(9.1,3.8,8.2,3.8).curveTo(6.7,3.8,6.8,3.5).closePath();
	this.shape.setTransform(72.3,2.8);

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#826283").beginStroke().moveTo(10.3,3.3).lineTo(4.6,2.2).lineTo(2.7,2).lineTo(0.6,1.9).lineTo(0.1,1.9).lineTo(0,1.9).curveTo(-4.7,1.4,-9.1,0.6).curveTo(-13.2,-0.3,-13.2,-0.7).curveTo(-13.2,-2.3,-6,-3.2).curveTo(1.3,-4.1,5.6,-2.9).curveTo(10.4,-1.8,13.2,2.6).lineTo(12.4,3.4).lineTo(12.2,3.6).lineTo(12,3.6).curveTo(11.5,3.6,10.3,3.3).closePath();
	this.shape_1.setTransform(41.9,0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#826282").beginStroke().moveTo(21.5,7.4).lineTo(21.5,6.3).curveTo(21.5,3.8,18.5,0.9).curveTo(14.9,-2.5,9,-4.1).lineTo(8.5,-4.3).lineTo(2.1,-6.3).lineTo(-3.5,-7.9).lineTo(-4.8,-8.3).curveTo(7.7,-7.2,15.4,-5).curveTo(19,-4,22.1,-2.5).lineTo(24.2,-1.4).curveTo(26.2,-0.4,26.3,0.7).curveTo(26.8,1.6,26.1,2.9).lineTo(25.6,4.8).lineTo(25.1,6.6).curveTo(24.9,7.4,24.3,8.1).lineTo(23.4,8.3).lineTo(23,8.3).curveTo(22.2,8,21.5,7.4).closePath().moveTo(-18.9,-1.4).curveTo(-19.5,-1.5,-19.9,-1.7).lineTo(-19.7,-1.8).lineTo(-18.9,-2.7).curveTo(-21.7,-7.1,-26.5,-8.2).lineTo(-26.4,-8.2).lineTo(-25.5,-8.2).curveTo(-22.4,-8.2,-20.4,-7.4).curveTo(-18.3,-6.7,-17.1,-5).curveTo(-15.9,-4.1,-14.9,-1.9).lineTo(-14.6,-0.7).lineTo(-14.5,0.2).lineTo(-15.6,0.3).lineTo(-18.9,-1.4).closePath();
	this.shape_2.setTransform(74,5.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#5A986F").beginStroke().moveTo(-2,-0.8).lineTo(-1.8,-0.7).curveTo(0.8,-0,2,0.8).curveTo(0.1,-0.1,-2,-0.8).closePath();
	this.shape_3.setTransform(94,-0.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#000000").beginStroke().moveTo(-8.8,20.8).lineTo(-9.6,19.1).curveTo(-9.6,17.9,-8.5,17.5).curveTo(-8.9,16.5,-9.7,12.3).curveTo(-10.7,7.6,-11.1,5.9).curveTo(-12.6,0.7,-13.9,-4.4).lineTo(-14.3,-4.3).curveTo(-15.4,-4.3,-17.5,-5.7).lineTo(-21.7,-8.5).curveTo(-28.1,-12.4,-33.9,-11.2).lineTo(-36,-12.7).curveTo(-36,-14.5,-33.1,-14.5).lineTo(-33,-14.5).lineTo(-32.5,-14.4).lineTo(-30.4,-14.4).lineTo(-28.5,-14.2).lineTo(-22.8,-13).curveTo(-21.1,-12.6,-20.8,-12.8).curveTo(-20.4,-12.5,-19.9,-12.4).lineTo(-16.6,-10.7).lineTo(-15.4,-10.2).lineTo(-15.3,-10.1).lineTo(-15.4,-10.2).lineTo(-15.5,-10.3).lineTo(-15.5,-10.5).lineTo(-15.5,-10.9).lineTo(-15.6,-11.8).lineTo(-15.9,-13).curveTo(-16.9,-15.2,-18,-16.1).curveTo(-19.3,-17.7,-21.4,-18.5).curveTo(-23.4,-19.2,-26.5,-19.3).lineTo(-27.4,-19.3).lineTo(-26.6,-19.9).curveTo(-26.1,-20.2,-25.6,-20.2).curveTo(-25,-21.1,-22.8,-21.1).lineTo(-20.5,-21).lineTo(-20.7,-21.3).curveTo(-20.8,-21.3,-20.8,-21.4).curveTo(-20.8,-21.5,-20.8,-21.5).curveTo(-20.8,-21.5,-20.7,-21.6).curveTo(-20.7,-21.6,-20.6,-21.6).curveTo(-17.9,-23.2,-6.1,-22.3).curveTo(6.9,-21.3,17,-17.8).curveTo(19.1,-17.1,20.9,-16.2).lineTo(21.5,-16).curveTo(29.3,-12.5,29.3,-8.8).curveTo(29.3,-6.3,28.5,-3.8).curveTo(27.7,-1.5,26.5,-0.5).lineTo(29,2.4).curveTo(30.3,4.1,31.3,6.5).curveTo(34.1,13.5,34.4,16.4).curveTo(36,16.5,36,18.9).curveTo(36,20.2,35.8,20.5).curveTo(35.2,21.6,32.3,21.6).lineTo(29.9,21.5).lineTo(28.9,21.3).lineTo(28.7,20.1).curveTo(29.5,18.9,30.8,17.8).curveTo(30.1,16.8,27.9,11.1).curveTo(25.2,4,23.6,0.5).curveTo(22.6,0.5,22,0.3).lineTo(21.7,0.2).curveTo(21.7,0.2,21.6,0.1).curveTo(21.6,0.1,21.6,0.1).curveTo(21.6,0,21.5,-0).curveTo(21.5,-0,21.5,-0.1).curveTo(21.3,-0.2,21.2,-1).lineTo(22.4,-2.8).lineTo(23.3,-3).curveTo(24,-3.7,24.2,-4.4).lineTo(24.6,-6.3).lineTo(25.2,-8.1).curveTo(25.8,-9.5,25.3,-10.4).curveTo(25.2,-11.4,23.3,-12.4).lineTo(21.1,-13.5).curveTo(18,-15.1,14.4,-16.1).curveTo(6.7,-18.2,-5.8,-19.4).lineTo(-4.4,-19).lineTo(1.1,-17.3).lineTo(7.5,-15.3).lineTo(8,-15.1).curveTo(14,-13.6,17.5,-10.2).curveTo(20.5,-7.3,20.5,-4.7).lineTo(20.5,-3.7).curveTo(20.4,-2.4,20.2,-1.6).lineTo(19.9,-1.4).curveTo(19.5,-1.2,18.3,-1.2).curveTo(16.8,-1.2,15.5,-4.5).lineTo(14.6,-7.2).lineTo(12.9,-7.1).curveTo(6.4,-5.9,3.1,-5.9).curveTo(-4.3,-5.9,-8.9,-7.9).curveTo(-8.4,-6.9,-8.2,-6.2).lineTo(-3.8,-3.5).curveTo(1.5,-0.2,1.5,-0).curveTo(1.5,0.8,0.5,1.2).curveTo(-0.3,1.5,-1.1,1.5).lineTo(-7.9,-2.1).curveTo(-7.4,0.1,-7.4,3).curveTo(-7.3,3.1,-5.9,8.6).curveTo(-5.1,11.8,-4.4,18.5).curveTo(-3,19.2,-3,20).curveTo(-3,21,-4.7,21.9).curveTo(-6.1,22.6,-6.7,22.6).curveTo(-7.8,22.6,-8.8,20.8).closePath().moveTo(-6.8,-11.1).curveTo(-4.6,-10.5,-3.1,-10.3).curveTo(-4.5,-11.1,-5.5,-11.4).lineTo(-6.8,-11.1).closePath().moveTo(-16.2,-19.9).lineTo(-16.2,-19.9).lineTo(-16.1,-20.2).lineTo(-17.3,-20.3).lineTo(-16.2,-19.9).closePath();
	this.shape_4.setTransform(75,16.9);

	// Layer 3
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#000000").beginStroke().moveTo(3.8,8.3).curveTo(-5.8,8.2,-10.8,5.7).curveTo(-18.7,1.9,-19.2,-1.8).curveTo(-19.6,-4.3,-14.9,-6.3).curveTo(-10.3,-8.3,-1.1,-8.3).curveTo(8.1,-8.3,14.1,-6.2).curveTo(20.1,-4.1,19.1,-0.9).lineTo(18.4,2.5).curveTo(17.7,4.6,15.6,6.3).curveTo(13.2,8.3,4.3,8.3).lineTo(3.8,8.3).closePath();
	this.shape_5.setTransform(82.1,11.5);

	// Layer 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#594359").beginStroke().moveTo(2,44).lineTo(1.7,44).lineTo(1.7,41.8).lineTo(1.7,41.7).lineTo(1.7,41.6).lineTo(1.7,41.4).lineTo(1.7,40.6).lineTo(1.6,38.4).lineTo(1.5,38.4).lineTo(1.5,38.4).lineTo(1.5,31.1).lineTo(1.5,23.8).lineTo(15.5,23.8).lineTo(15,46).lineTo(13.7,46.1).curveTo(3,44,2,44).closePath().moveTo(-15,15.7).curveTo(-15.5,14.8,-15.5,12.3).curveTo(-15.5,5.4,-13,-4.4).lineTo(-13,-4.6).curveTo(-10,-15.9,-4.9,-25.7).curveTo(2.1,-38.8,11.5,-46.1).curveTo(5,-36.9,0.1,-23.2).curveTo(-3.2,-13.8,-4.9,-5).lineTo(-4.2,-5).lineTo(-6.5,10.6).lineTo(-6.5,11.8).lineTo(-6,18.9).lineTo(-6.9,18.9).lineTo(-8.3,19).curveTo(-13.1,19,-15,15.7).closePath();
	this.shape_6.setTransform(20,59.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#826282").beginStroke().moveTo(-37.7,50.8).lineTo(-43.8,49.7).lineTo(-46.1,49.2).lineTo(-51.6,48.1).lineTo(-50.2,48).lineTo(-49.7,25.7).lineTo(-47.6,25.7).lineTo(-47.6,26).curveTo(-45,25.6,-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.9,-38.6,6.9).lineTo(-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-42.9,-7.1,-43.7,4.5).lineTo(-43.4,4.7).lineTo(-44.1,13.7).lineTo(-44.1,14.1).lineTo(-44.1,14.4).lineTo(-44.1,14.9).lineTo(-44.1,16.7).lineTo(-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).lineTo(-71.7,12.5).lineTo(-69.5,-3).lineTo(-70.1,-3).curveTo(-68.5,-11.9,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(47.8,-15.2,46.8,-17.7).curveTo(45.1,-21.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-27.6,52.7,-37.7,50.8).closePath();
	this.shape_7.setTransform(85.2,58);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_8.setTransform(86,143.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_9.setTransform(67.5,124.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_10.setTransform(86.9,128);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_11.setTransform(94.8,140.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#000000").beginStroke().moveTo(-22.1,77.7).curveTo(-41.7,74.4,-47.4,72.4).curveTo(-53.4,70.3,-53.5,66.6).curveTo(-58.7,38.4,-58.7,32.2).lineTo(-58.7,31).lineTo(-60.2,30.4).curveTo(-60.7,30.7,-61.7,30.7).curveTo(-63.5,30.7,-63.9,27.5).lineTo(-63.9,27).lineTo(-63.9,23.9).lineTo(-63.9,23.7).lineTo(-63.9,23.6).lineTo(-63.9,22.5).lineTo(-63.8,21.2).lineTo(-63.7,19.4).lineTo(-62.8,6).lineTo(-69.1,6).lineTo(-69.4,5.9).lineTo(-69.8,5.8).curveTo(-71.9,5.4,-78.8,2.7).curveTo(-79.8,2,-80.3,1.2).curveTo(-81.1,0,-81.1,-4.7).lineTo(-81.1,-5).lineTo(-81.1,-5.7).lineTo(-81.1,-6.1).lineTo(-81,-7.6).curveTo(-80.7,-13.7,-78.9,-21.4).lineTo(-78.8,-22.3).curveTo(-75.9,-34.9,-70.5,-45.5).curveTo(-55.8,-74.7,-28.8,-77.3).lineTo(-28.8,-77.7).lineTo(-0.4,-77.7).curveTo(29.9,-73,42.9,-65.3).curveTo(48.3,-62.2,52.6,-57.6).lineTo(60.2,-48.8).lineTo(71.5,-32.5).curveTo(80.3,-23,80.4,-16).lineTo(80.5,-16).lineTo(80.8,-15.5).curveTo(81.1,-15.2,81.1,-14.1).curveTo(81.1,-12.9,76.3,-8.5).lineTo(73.8,-6.8).curveTo(68.8,-3.7,65.6,-2.6).lineTo(67,3.8).lineTo(68.1,9.7).curveTo(69.2,17.8,69.2,21.3).curveTo(69.2,26.3,67,27.7).lineTo(66.7,27).curveTo(64,28.8,57.4,31.4).lineTo(56.2,34.1).curveTo(54.4,38.4,54.2,41.9).curveTo(54,43.8,54.5,47.8).curveTo(54.5,49.4,54,50.7).curveTo(57.5,51.4,60.4,52.8).curveTo(68.5,56.8,68.5,64.4).curveTo(68.5,66.2,66.7,67.5).curveTo(63.8,69.7,57.2,69.5).lineTo(25.7,69.5).curveTo(21.2,69.6,18.6,67.3).curveTo(16.8,65.6,16.7,63.7).curveTo(15.9,61.5,14.7,56).lineTo(14.5,54.8).lineTo(14.4,55.2).curveTo(12.4,64.6,10.5,68.4).lineTo(10.8,68.8).lineTo(10.8,68.9).lineTo(10.9,68.9).lineTo(10.9,69.3).lineTo(10.9,70.1).curveTo(10.9,72.5,8,74.7).curveTo(4.9,77.1,0.3,77.7).closePath().moveTo(-48,67.2).curveTo(-46.9,68.5,-38,70).curveTo(-23.4,72.5,-21.6,72.9).lineTo(-11.9,72.9).lineTo(-0.2,72.9).curveTo(1.6,72,4.2,70.2).lineTo(-5.1,67.8).curveTo(-17.3,64.7,-26.6,64.7).curveTo(-28.6,64.7,-35.8,65.3).lineTo(-36.2,65.3).curveTo(-44.4,66.1,-45.9,66.6).lineTo(-48.4,66.6).lineTo(-48,67.2).closePath().moveTo(1.9,64.3).curveTo(5,65.5,7.3,66.7).curveTo(7.8,63.1,9.7,52.2).curveTo(11.5,42.8,12.1,37.7).lineTo(-18.3,38.2).curveTo(-27.9,37.7,-38.6,35.8).curveTo(-42.7,35.1,-47,34.1).curveTo(-52.1,33,-55.4,32.1).lineTo(-54.9,33.7).curveTo(-51.2,47.4,-51.2,61.9).curveTo(-49.3,61.4,-40.4,60.6).lineTo(-35,60.2).curveTo(-29.9,59.8,-26.9,59.8).curveTo(-10.4,59.8,1.9,64.3).closePath().moveTo(43.3,53.5).curveTo(36.9,54.2,27.9,58.7).curveTo(24.8,60.2,21.4,62.1).curveTo(22.2,62.6,22.1,62.9).curveTo(22.6,63.3,26.9,64.7).lineTo(57.3,64.7).lineTo(58.3,64.5).lineTo(58.6,64.5).curveTo(60.4,64.2,63.2,63.9).curveTo(57.1,53.3,46.2,53.3).curveTo(44.7,53.3,43.3,53.5).closePath().moveTo(31.8,37.4).lineTo(25.4,37.5).lineTo(16.7,37.7).lineTo(16.7,38.6).curveTo(16.7,40.6,16.5,43.2).curveTo(17.3,45.1,18.1,48.1).curveTo(19.3,52.8,19.6,57.4).curveTo(21.5,55.5,24.8,53.8).curveTo(26.5,53,28.2,52.4).curveTo(34.5,50,42.6,49.8).curveTo(46.6,49.6,50.1,50).lineTo(49.8,48.8).curveTo(49.7,47.7,49.7,44.3).curveTo(49.7,36.6,50.2,33.8).curveTo(42.5,35.9,31.8,37.4).closePath().moveTo(-59.6,13.3).lineTo(-59.6,20.6).lineTo(-59.6,20.6).lineTo(-59.5,20.6).lineTo(-59.4,22.8).lineTo(-59.4,23.6).lineTo(-59.4,23.7).lineTo(-59.4,23.8).lineTo(-59.4,24).lineTo(-59.4,26.2).lineTo(-59.1,26.2).curveTo(-58.1,26.2,-47.4,28.3).lineTo(-42,29.4).lineTo(-39.6,29.9).lineTo(-33.6,31).curveTo(-23.5,32.9,-18.5,33.3).lineTo(25.3,31.8).curveTo(41.1,32,51.6,27.4).curveTo(58.9,24.3,65.2,23.2).curveTo(63.5,18,62.4,11.3).curveTo(60.7,0.1,59.5,-5.7).curveTo(59,-7.9,52.2,-21.3).curveTo(46,-33.5,46,-38.8).curveTo(46,-41.1,46.1,-42.8).curveTo(49.2,-41.7,50.9,-37.5).curveTo(51.9,-35,54.2,-29).curveTo(56.3,-25.7,58.6,-21.4).curveTo(60.9,-16.8,62.5,-12.8).lineTo(63.7,-9.1).curveTo(65.6,-10.4,68.3,-11.9).curveTo(72.5,-14.2,75.9,-15.2).curveTo(75.3,-21.5,70.2,-28.9).curveTo(68.2,-31.7,56.6,-45.2).lineTo(49.6,-53.5).curveTo(45.4,-57.9,40.6,-60.8).curveTo(28.1,-68.4,-0.2,-72.8).lineTo(-28.4,-72.8).curveTo(-40.2,-71.3,-49.6,-63.9).curveTo(-59,-56.6,-66,-43.5).curveTo(-71.1,-33.7,-74.1,-22.4).lineTo(-74.1,-22.2).curveTo(-76.6,-12.4,-76.6,-5.5).curveTo(-76.6,-3,-76.1,-2.1).curveTo(-73.9,1.6,-68,1.1).lineTo(-67.1,1.1).lineTo(-40,1.5).lineTo(-40,-2.5).lineTo(-40,-3.1).lineTo(-40,-4.9).lineTo(-40,-5.3).lineTo(-40,-5.7).lineTo(-40,-6.1).lineTo(-39.3,-15.1).lineTo(-39.5,-15.3).curveTo(-38.8,-26.8,-36.6,-37.4).curveTo(-35.2,-44.1,-34.4,-43.8).curveTo(-33.3,-43.4,-33.3,-34).curveTo(-33.3,-26.2,-34.2,-15).lineTo(-34.4,-12.8).curveTo(-35.4,-1.9,-35.7,3.6).lineTo(-35.6,3.8).curveTo(-35.6,4.1,-35.7,4.5).lineTo(-35.8,5.2).lineTo(-36.2,5.6).curveTo(-36.6,6,-37.8,6).lineTo(-38.5,6).curveTo(-40.9,5.9,-43.5,6.2).lineTo(-43.5,6).lineTo(-45.6,6).lineTo(-59.6,6).lineTo(-59.6,13.3).closePath();
	this.shape_12.setTransform(81.1,77.7);

	// Layer 2
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.beginFill("#826282").beginStroke().moveTo(-35.1,12.9).curveTo(-37.9,11.5,-38.4,9.3).curveTo(-38.5,8.8,-38.6,5.7).curveTo(-38.5,-1.2,-31.7,-7.5).curveTo(-23.1,-15.4,-7,-16.9).lineTo(18,-16.8).lineTo(29,-13.6).curveTo(38.5,-10.8,38.6,-9.7).curveTo(38.5,-6.8,35.8,-2.5).curveTo(32.9,2.3,27.9,6.4).curveTo(15.3,16.9,-1.3,16.9).curveTo(-27.6,16.9,-35.1,12.9).closePath();
	this.shape_13.setTransform(40.6,14.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.beginFill("#000000").beginStroke().moveTo(-41.5,13.6).curveTo(-45,10.4,-45,5.1).curveTo(-45,-3.1,-36.4,-10).curveTo(-26.8,-17.8,-9.5,-20.7).curveTo(5.5,-21.4,18.5,-20.7).curveTo(20,-20.2,32.9,-17.2).curveTo(45,-14,45,-11.7).curveTo(45,-7.9,43,-3.3).curveTo(40.4,2.8,35.1,7.6).curveTo(20.4,21,-10,21).curveTo(-33.6,21,-41.5,13.6).closePath().moveTo(-32.3,-7.8).curveTo(-39.1,-1.6,-39.1,5.3).curveTo(-39.1,8.4,-39,8.9).curveTo(-38.4,11.1,-35.7,12.6).curveTo(-28.2,16.5,-1.9,16.5).curveTo(14.7,16.5,27.4,6).curveTo(32.3,1.9,35.2,-2.9).curveTo(38,-7.2,38,-10.1).curveTo(38,-11.2,28.5,-14).lineTo(17.5,-17.1).lineTo(-7.5,-17.3).curveTo(-23.6,-15.8,-32.3,-7.8).closePath();
	this.shape_14.setTransform(41.1,14.9);

	this.addChild(this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-3.8,-6.1,166,161.6);


(lib.shirt04 = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#594259").beginStroke().moveTo(6.8,3.5).curveTo(7.1,3.1,6.9,3).curveTo(6,2.2,-6.5,3).curveTo(-8.6,2.3,-9.4,0.2).curveTo(-9.6,-0.4,-9.9,-2.6).curveTo(-10.3,-5.4,-0.2,-2.1).curveTo(9.9,1.2,9.9,2.5).lineTo(9.9,3.1).lineTo(9.7,3.4).lineTo(9.4,3.6).curveTo(9.1,3.8,8.2,3.8).curveTo(6.7,3.8,6.8,3.5).closePath();
	this.shape.setTransform(72.3,2.8);

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#826283").beginStroke().moveTo(10.3,3.3).lineTo(4.6,2.2).lineTo(2.7,2).lineTo(0.6,1.9).lineTo(0.1,1.9).lineTo(0,1.9).curveTo(-4.7,1.4,-9.1,0.6).curveTo(-13.2,-0.3,-13.2,-0.7).curveTo(-13.2,-2.3,-6,-3.2).curveTo(1.3,-4.1,5.6,-2.9).curveTo(10.4,-1.8,13.2,2.6).lineTo(12.4,3.4).lineTo(12.2,3.6).lineTo(12,3.6).curveTo(11.5,3.6,10.3,3.3).closePath();
	this.shape_1.setTransform(41.9,0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#826282").beginStroke().moveTo(21.5,7.4).lineTo(21.5,6.3).curveTo(21.5,3.8,18.5,0.9).curveTo(14.9,-2.5,9,-4.1).lineTo(8.5,-4.3).lineTo(2.1,-6.3).lineTo(-3.5,-7.9).lineTo(-4.8,-8.3).curveTo(7.7,-7.2,15.4,-5).curveTo(19,-4,22.1,-2.5).lineTo(24.2,-1.4).curveTo(26.2,-0.4,26.3,0.7).curveTo(26.8,1.6,26.1,2.9).lineTo(25.6,4.8).lineTo(25.1,6.6).curveTo(24.9,7.4,24.3,8.1).lineTo(23.4,8.3).lineTo(23,8.3).curveTo(22.2,8,21.5,7.4).closePath().moveTo(-18.9,-1.4).curveTo(-19.5,-1.5,-19.9,-1.7).lineTo(-19.7,-1.8).lineTo(-18.9,-2.7).curveTo(-21.7,-7.1,-26.5,-8.2).lineTo(-26.4,-8.2).lineTo(-25.5,-8.2).curveTo(-22.4,-8.2,-20.4,-7.4).curveTo(-18.3,-6.7,-17.1,-5).curveTo(-15.9,-4.1,-14.9,-1.9).lineTo(-14.6,-0.7).lineTo(-14.5,0.2).lineTo(-15.6,0.3).lineTo(-18.9,-1.4).closePath();
	this.shape_2.setTransform(74,5.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#5A986F").beginStroke().moveTo(-2,-0.8).lineTo(-1.8,-0.7).curveTo(0.8,-0,2,0.8).curveTo(0.1,-0.1,-2,-0.8).closePath();
	this.shape_3.setTransform(94,-0.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#000000").beginStroke().moveTo(-8.8,20.8).lineTo(-9.6,19.1).curveTo(-9.6,17.9,-8.5,17.5).curveTo(-8.9,16.5,-9.7,12.3).curveTo(-10.7,7.6,-11.1,5.9).curveTo(-12.6,0.7,-13.9,-4.4).lineTo(-14.3,-4.3).curveTo(-15.4,-4.3,-17.5,-5.7).lineTo(-21.7,-8.5).curveTo(-28.1,-12.4,-33.9,-11.2).lineTo(-36,-12.7).curveTo(-36,-14.5,-33.1,-14.5).lineTo(-33,-14.5).lineTo(-32.5,-14.4).lineTo(-30.4,-14.4).lineTo(-28.5,-14.2).lineTo(-22.8,-13).curveTo(-21.1,-12.6,-20.8,-12.8).curveTo(-20.4,-12.5,-19.9,-12.4).lineTo(-16.6,-10.7).lineTo(-15.4,-10.2).lineTo(-15.3,-10.1).lineTo(-15.4,-10.2).lineTo(-15.5,-10.3).lineTo(-15.5,-10.5).lineTo(-15.5,-10.9).lineTo(-15.6,-11.8).lineTo(-15.9,-13).curveTo(-16.9,-15.2,-18,-16.1).curveTo(-19.3,-17.7,-21.4,-18.5).curveTo(-23.4,-19.2,-26.5,-19.3).lineTo(-27.4,-19.3).lineTo(-26.6,-19.9).curveTo(-26.1,-20.2,-25.6,-20.2).curveTo(-25,-21.1,-22.8,-21.1).lineTo(-20.5,-21).lineTo(-20.7,-21.3).curveTo(-20.8,-21.3,-20.8,-21.4).curveTo(-20.8,-21.5,-20.8,-21.5).curveTo(-20.8,-21.5,-20.7,-21.6).curveTo(-20.7,-21.6,-20.6,-21.6).curveTo(-17.9,-23.2,-6.1,-22.3).curveTo(6.9,-21.3,17,-17.8).curveTo(19.1,-17.1,20.9,-16.2).lineTo(21.5,-16).curveTo(29.3,-12.5,29.3,-8.8).curveTo(29.3,-6.3,28.5,-3.8).curveTo(27.7,-1.5,26.5,-0.5).lineTo(29,2.4).curveTo(30.3,4.1,31.3,6.5).curveTo(34.1,13.5,34.4,16.4).curveTo(36,16.5,36,18.9).curveTo(36,20.2,35.8,20.5).curveTo(35.2,21.6,32.3,21.6).lineTo(29.9,21.5).lineTo(28.9,21.3).lineTo(28.7,20.1).curveTo(29.5,18.9,30.8,17.8).curveTo(30.1,16.8,27.9,11.1).curveTo(25.2,4,23.6,0.5).curveTo(22.6,0.5,22,0.3).lineTo(21.7,0.2).curveTo(21.7,0.2,21.6,0.1).curveTo(21.6,0.1,21.6,0.1).curveTo(21.6,0,21.5,-0).curveTo(21.5,-0,21.5,-0.1).curveTo(21.3,-0.2,21.2,-1).lineTo(22.4,-2.8).lineTo(23.3,-3).curveTo(24,-3.7,24.2,-4.4).lineTo(24.6,-6.3).lineTo(25.2,-8.1).curveTo(25.8,-9.5,25.3,-10.4).curveTo(25.2,-11.4,23.3,-12.4).lineTo(21.1,-13.5).curveTo(18,-15.1,14.4,-16.1).curveTo(6.7,-18.2,-5.8,-19.4).lineTo(-4.4,-19).lineTo(1.1,-17.3).lineTo(7.5,-15.3).lineTo(8,-15.1).curveTo(14,-13.6,17.5,-10.2).curveTo(20.5,-7.3,20.5,-4.7).lineTo(20.5,-3.7).curveTo(20.4,-2.4,20.2,-1.6).lineTo(19.9,-1.4).curveTo(19.5,-1.2,18.3,-1.2).curveTo(16.8,-1.2,15.5,-4.5).lineTo(14.6,-7.2).lineTo(12.9,-7.1).curveTo(6.4,-5.9,3.1,-5.9).curveTo(-4.3,-5.9,-8.9,-7.9).curveTo(-8.4,-6.9,-8.2,-6.2).lineTo(-3.8,-3.5).curveTo(1.5,-0.2,1.5,-0).curveTo(1.5,0.8,0.5,1.2).curveTo(-0.3,1.5,-1.1,1.5).lineTo(-7.9,-2.1).curveTo(-7.4,0.1,-7.4,3).curveTo(-7.3,3.1,-5.9,8.6).curveTo(-5.1,11.8,-4.4,18.5).curveTo(-3,19.2,-3,20).curveTo(-3,21,-4.7,21.9).curveTo(-6.1,22.6,-6.7,22.6).curveTo(-7.8,22.6,-8.8,20.8).closePath().moveTo(-6.8,-11.1).curveTo(-4.6,-10.5,-3.1,-10.3).curveTo(-4.5,-11.1,-5.5,-11.4).lineTo(-6.8,-11.1).closePath().moveTo(-16.2,-19.9).lineTo(-16.2,-19.9).lineTo(-16.1,-20.2).lineTo(-17.3,-20.3).lineTo(-16.2,-19.9).closePath();
	this.shape_4.setTransform(75,16.9);

	// Layer 3
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#000000").beginStroke().moveTo(3.8,8.3).curveTo(-5.8,8.2,-10.8,5.7).curveTo(-18.7,1.9,-19.2,-1.8).curveTo(-19.6,-4.3,-14.9,-6.3).curveTo(-10.3,-8.3,-1.1,-8.3).curveTo(8.1,-8.3,14.1,-6.2).curveTo(20.1,-4.1,19.1,-0.9).lineTo(18.4,2.5).curveTo(17.7,4.6,15.6,6.3).curveTo(13.2,8.3,4.3,8.3).lineTo(3.8,8.3).closePath();
	this.shape_5.setTransform(82.1,11.5);

	// Layer 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#594359").beginStroke().moveTo(2,44).lineTo(1.7,44).lineTo(1.7,41.8).lineTo(1.7,41.7).lineTo(1.7,41.6).lineTo(1.7,41.4).lineTo(1.7,40.6).lineTo(1.6,38.4).lineTo(1.5,38.4).lineTo(1.5,38.4).lineTo(1.5,31.1).lineTo(1.5,23.8).lineTo(15.5,23.8).lineTo(15,46).lineTo(13.7,46.1).curveTo(3,44,2,44).closePath().moveTo(-15,15.7).curveTo(-15.5,14.8,-15.5,12.3).curveTo(-15.5,5.4,-13,-4.4).lineTo(-13,-4.6).curveTo(-10,-15.9,-4.9,-25.7).curveTo(2.1,-38.8,11.5,-46.1).curveTo(5,-36.9,0.1,-23.2).curveTo(-3.2,-13.8,-4.9,-5).lineTo(-4.2,-5).lineTo(-6.5,10.6).lineTo(-6.5,11.8).lineTo(-6,18.9).lineTo(-6.9,18.9).lineTo(-8.3,19).curveTo(-13.1,19,-15,15.7).closePath();
	this.shape_6.setTransform(20,59.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#826282").beginStroke().moveTo(-37.7,50.8).lineTo(-43.8,49.7).lineTo(-46.1,49.2).lineTo(-51.6,48.1).lineTo(-50.2,48).lineTo(-49.7,25.7).lineTo(-47.6,25.7).lineTo(-47.6,26).curveTo(-45,25.6,-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.9,-38.6,6.9).lineTo(-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-42.9,-7.1,-43.7,4.5).lineTo(-43.4,4.7).lineTo(-44.1,13.7).lineTo(-44.1,14.1).lineTo(-44.1,14.4).lineTo(-44.1,14.9).lineTo(-44.1,16.7).lineTo(-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).lineTo(-71.7,12.5).lineTo(-69.5,-3).lineTo(-70.1,-3).curveTo(-68.5,-11.9,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(47.8,-15.2,46.8,-17.7).curveTo(45.1,-21.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-27.6,52.7,-37.7,50.8).closePath();
	this.shape_7.setTransform(85.2,58);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_8.setTransform(86,143.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_9.setTransform(67.5,124.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_10.setTransform(86.9,128);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_11.setTransform(94.8,140.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#000000").beginStroke().moveTo(-22.1,77.7).curveTo(-41.7,74.4,-47.4,72.4).curveTo(-53.4,70.3,-53.5,66.6).curveTo(-58.7,38.4,-58.7,32.2).lineTo(-58.7,31).lineTo(-60.2,30.4).curveTo(-60.7,30.7,-61.7,30.7).curveTo(-63.5,30.7,-63.9,27.5).lineTo(-63.9,27).lineTo(-63.9,23.9).lineTo(-63.9,23.7).lineTo(-63.9,23.6).lineTo(-63.9,22.5).lineTo(-63.8,21.2).lineTo(-63.7,19.4).lineTo(-62.8,6).lineTo(-69.1,6).lineTo(-69.4,5.9).lineTo(-69.8,5.8).curveTo(-71.9,5.4,-78.8,2.7).curveTo(-79.8,2,-80.3,1.2).curveTo(-81.1,0,-81.1,-4.7).lineTo(-81.1,-5).lineTo(-81.1,-5.7).lineTo(-81.1,-6.1).lineTo(-81,-7.6).curveTo(-80.7,-13.7,-78.9,-21.4).lineTo(-78.8,-22.3).curveTo(-75.9,-34.9,-70.5,-45.5).curveTo(-55.8,-74.7,-28.8,-77.3).lineTo(-28.8,-77.7).lineTo(-0.4,-77.7).curveTo(29.9,-73,42.9,-65.3).curveTo(48.3,-62.2,52.6,-57.6).lineTo(60.2,-48.8).lineTo(71.5,-32.5).curveTo(80.3,-23,80.4,-16).lineTo(80.5,-16).lineTo(80.8,-15.5).curveTo(81.1,-15.2,81.1,-14.1).curveTo(81.1,-12.9,76.3,-8.5).lineTo(73.8,-6.8).curveTo(68.8,-3.7,65.6,-2.6).lineTo(67,3.8).lineTo(68.1,9.7).curveTo(69.2,17.8,69.2,21.3).curveTo(69.2,26.3,67,27.7).lineTo(66.7,27).curveTo(64,28.8,57.4,31.4).lineTo(56.2,34.1).curveTo(54.4,38.4,54.2,41.9).curveTo(54,43.8,54.5,47.8).curveTo(54.5,49.4,54,50.7).curveTo(57.5,51.4,60.4,52.8).curveTo(68.5,56.8,68.5,64.4).curveTo(68.5,66.2,66.7,67.5).curveTo(63.8,69.7,57.2,69.5).lineTo(25.7,69.5).curveTo(21.2,69.6,18.6,67.3).curveTo(16.8,65.6,16.7,63.7).curveTo(15.9,61.5,14.7,56).lineTo(14.5,54.8).lineTo(14.4,55.2).curveTo(12.4,64.6,10.5,68.4).lineTo(10.8,68.8).lineTo(10.8,68.9).lineTo(10.9,68.9).lineTo(10.9,69.3).lineTo(10.9,70.1).curveTo(10.9,72.5,8,74.7).curveTo(4.9,77.1,0.3,77.7).closePath().moveTo(-48,67.2).curveTo(-46.9,68.5,-38,70).curveTo(-23.4,72.5,-21.6,72.9).lineTo(-11.9,72.9).lineTo(-0.2,72.9).curveTo(1.6,72,4.2,70.2).lineTo(-5.1,67.8).curveTo(-17.3,64.7,-26.6,64.7).curveTo(-28.6,64.7,-35.8,65.3).lineTo(-36.2,65.3).curveTo(-44.4,66.1,-45.9,66.6).lineTo(-48.4,66.6).lineTo(-48,67.2).closePath().moveTo(1.9,64.3).curveTo(5,65.5,7.3,66.7).curveTo(7.8,63.1,9.7,52.2).curveTo(11.5,42.8,12.1,37.7).lineTo(-18.3,38.2).curveTo(-27.9,37.7,-38.6,35.8).curveTo(-42.7,35.1,-47,34.1).curveTo(-52.1,33,-55.4,32.1).lineTo(-54.9,33.7).curveTo(-51.2,47.4,-51.2,61.9).curveTo(-49.3,61.4,-40.4,60.6).lineTo(-35,60.2).curveTo(-29.9,59.8,-26.9,59.8).curveTo(-10.4,59.8,1.9,64.3).closePath().moveTo(43.3,53.5).curveTo(36.9,54.2,27.9,58.7).curveTo(24.8,60.2,21.4,62.1).curveTo(22.2,62.6,22.1,62.9).curveTo(22.6,63.3,26.9,64.7).lineTo(57.3,64.7).lineTo(58.3,64.5).lineTo(58.6,64.5).curveTo(60.4,64.2,63.2,63.9).curveTo(57.1,53.3,46.2,53.3).curveTo(44.7,53.3,43.3,53.5).closePath().moveTo(31.8,37.4).lineTo(25.4,37.5).lineTo(16.7,37.7).lineTo(16.7,38.6).curveTo(16.7,40.6,16.5,43.2).curveTo(17.3,45.1,18.1,48.1).curveTo(19.3,52.8,19.6,57.4).curveTo(21.5,55.5,24.8,53.8).curveTo(26.5,53,28.2,52.4).curveTo(34.5,50,42.6,49.8).curveTo(46.6,49.6,50.1,50).lineTo(49.8,48.8).curveTo(49.7,47.7,49.7,44.3).curveTo(49.7,36.6,50.2,33.8).curveTo(42.5,35.9,31.8,37.4).closePath().moveTo(-59.6,13.3).lineTo(-59.6,20.6).lineTo(-59.6,20.6).lineTo(-59.5,20.6).lineTo(-59.4,22.8).lineTo(-59.4,23.6).lineTo(-59.4,23.7).lineTo(-59.4,23.8).lineTo(-59.4,24).lineTo(-59.4,26.2).lineTo(-59.1,26.2).curveTo(-58.1,26.2,-47.4,28.3).lineTo(-42,29.4).lineTo(-39.6,29.9).lineTo(-33.6,31).curveTo(-23.5,32.9,-18.5,33.3).lineTo(25.3,31.8).curveTo(41.1,32,51.6,27.4).curveTo(58.9,24.3,65.2,23.2).curveTo(63.5,18,62.4,11.3).curveTo(60.7,0.1,59.5,-5.7).curveTo(59,-7.9,52.2,-21.3).curveTo(46,-33.5,46,-38.8).curveTo(46,-41.1,46.1,-42.8).curveTo(49.2,-41.7,50.9,-37.5).curveTo(51.9,-35,54.2,-29).curveTo(56.3,-25.7,58.6,-21.4).curveTo(60.9,-16.8,62.5,-12.8).lineTo(63.7,-9.1).curveTo(65.6,-10.4,68.3,-11.9).curveTo(72.5,-14.2,75.9,-15.2).curveTo(75.3,-21.5,70.2,-28.9).curveTo(68.2,-31.7,56.6,-45.2).lineTo(49.6,-53.5).curveTo(45.4,-57.9,40.6,-60.8).curveTo(28.1,-68.4,-0.2,-72.8).lineTo(-28.4,-72.8).curveTo(-40.2,-71.3,-49.6,-63.9).curveTo(-59,-56.6,-66,-43.5).curveTo(-71.1,-33.7,-74.1,-22.4).lineTo(-74.1,-22.2).curveTo(-76.6,-12.4,-76.6,-5.5).curveTo(-76.6,-3,-76.1,-2.1).curveTo(-73.9,1.6,-68,1.1).lineTo(-67.1,1.1).lineTo(-40,1.5).lineTo(-40,-2.5).lineTo(-40,-3.1).lineTo(-40,-4.9).lineTo(-40,-5.3).lineTo(-40,-5.7).lineTo(-40,-6.1).lineTo(-39.3,-15.1).lineTo(-39.5,-15.3).curveTo(-38.8,-26.8,-36.6,-37.4).curveTo(-35.2,-44.1,-34.4,-43.8).curveTo(-33.3,-43.4,-33.3,-34).curveTo(-33.3,-26.2,-34.2,-15).lineTo(-34.4,-12.8).curveTo(-35.4,-1.9,-35.7,3.6).lineTo(-35.6,3.8).curveTo(-35.6,4.1,-35.7,4.5).lineTo(-35.8,5.2).lineTo(-36.2,5.6).curveTo(-36.6,6,-37.8,6).lineTo(-38.5,6).curveTo(-40.9,5.9,-43.5,6.2).lineTo(-43.5,6).lineTo(-45.6,6).lineTo(-59.6,6).lineTo(-59.6,13.3).closePath();
	this.shape_12.setTransform(81.1,77.7);

	// Layer 2
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.beginFill("#826282").beginStroke().moveTo(-35.1,12.9).curveTo(-37.9,11.5,-38.4,9.3).curveTo(-38.5,8.8,-38.6,5.7).curveTo(-38.5,-1.2,-31.7,-7.5).curveTo(-23.1,-15.4,-7,-16.9).lineTo(18,-16.8).lineTo(29,-13.6).curveTo(38.5,-10.8,38.6,-9.7).curveTo(38.5,-6.8,35.8,-2.5).curveTo(32.9,2.3,27.9,6.4).curveTo(15.3,16.9,-1.3,16.9).curveTo(-27.6,16.9,-35.1,12.9).closePath();
	this.shape_13.setTransform(40.6,14.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.beginFill("#000000").beginStroke().moveTo(-41.5,13.6).curveTo(-45,10.4,-45,5.1).curveTo(-45,-3.1,-36.4,-10).curveTo(-26.8,-17.8,-9.5,-20.7).curveTo(5.5,-21.4,18.5,-20.7).curveTo(20,-20.2,32.9,-17.2).curveTo(45,-14,45,-11.7).curveTo(45,-7.9,43,-3.3).curveTo(40.4,2.8,35.1,7.6).curveTo(20.4,21,-10,21).curveTo(-33.6,21,-41.5,13.6).closePath().moveTo(-32.3,-7.8).curveTo(-39.1,-1.6,-39.1,5.3).curveTo(-39.1,8.4,-39,8.9).curveTo(-38.4,11.1,-35.7,12.6).curveTo(-28.2,16.5,-1.9,16.5).curveTo(14.7,16.5,27.4,6).curveTo(32.3,1.9,35.2,-2.9).curveTo(38,-7.2,38,-10.1).curveTo(38,-11.2,28.5,-14).lineTo(17.5,-17.1).lineTo(-7.5,-17.3).curveTo(-23.6,-15.8,-32.3,-7.8).closePath();
	this.shape_14.setTransform(41.1,14.9);

	this.addChild(this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-3.8,-6.1,166,161.6);


(lib.shirt03 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#879342").beginStroke().moveTo(-5.1,30.4).curveTo(-9.2,29.7,-11.7,27.3).curveTo(-17.2,22.3,-17.5,6.8).curveTo(-28.4,4,-29,-7.6).curveTo(-29.2,-13.4,-27.3,-18.5).curveTo(-22.2,-25.7,-12.6,-28.7).curveTo(-7.8,-30.2,-4,-30.3).curveTo(9.9,-32,19,-27.5).curveTo(27.6,-23.3,28.8,-15).curveTo(30,-7.2,24.4,0.3).curveTo(21.7,4.1,18.7,6.2).curveTo(19.2,15.8,18.8,20.5).curveTo(18.4,25.3,16.6,27.4).curveTo(15.2,29.3,11.8,29.9).curveTo(10.8,30.1,3.7,30.6).lineTo(0,30.8).curveTo(-3,30.8,-5.1,30.4).closePath().moveTo(-1.4,21.7).lineTo(-0.3,21.8).curveTo(0.9,22,0.9,22.6).lineTo(0.8,27.6).curveTo(2.1,27.6,3.3,27.4).lineTo(3.4,27.4).lineTo(3.5,22.6).curveTo(3.5,22.3,3.7,21.9).curveTo(4.2,21.3,5.4,21.3).lineTo(6.6,21.4).curveTo(7.7,21.6,7.7,22.2).lineTo(7.7,26.5).curveTo(10.7,25.5,12.9,23.6).curveTo(17.1,20.1,16.3,15.4).curveTo(14.3,14.4,11.6,13.9).lineTo(11.6,17.4).lineTo(11.3,18.1).curveTo(10.8,18.8,9.6,18.7).curveTo(9,18.7,8.5,18.6).curveTo(7.3,18.5,7.3,17.8).lineTo(7.3,13.3).curveTo(5.9,13.2,4.3,13.2).lineTo(4.3,17.6).lineTo(4.1,18.3).curveTo(3.6,19,2.4,18.9).lineTo(1.3,18.8).curveTo(0.1,18.6,0.1,18).lineTo(0.1,13.3).lineTo(-1.6,13.4).lineTo(-1.6,17.7).lineTo(-1.9,18.5).curveTo(-2.4,19.2,-3.6,19.1).curveTo(-4.2,19.1,-4.7,19).curveTo(-5.8,18.8,-5.8,18.2).lineTo(-5.8,13.9).lineTo(-7.4,14.3).curveTo(-10.9,15.1,-11.8,16.4).curveTo(-11.5,22.6,-6.7,25.5).curveTo(-5.2,26.5,-3.4,27.1).lineTo(-3.4,23).curveTo(-3.4,22.7,-3.1,22.4).curveTo(-2.6,21.7,-1.6,21.7).lineTo(-1.4,21.7).closePath().moveTo(-3.4,3.4).curveTo(-7.1,8.9,-3,9.9).curveTo(-0.8,10.4,0.5,7.2).curveTo(1,6.2,1.2,4.9).lineTo(1.4,3.8).lineTo(1.4,4.9).curveTo(1.4,6.2,1.8,7.2).curveTo(2.7,10.4,5.6,9.2).curveTo(8.6,8,5.2,2.9).curveTo(4.1,1.3,2.6,-0.4).lineTo(1.2,-1.9).curveTo(-1.5,0.6,-3.4,3.4).closePath().moveTo(-12.7,-21.1).curveTo(-16.7,-20.7,-19.5,-17.8).curveTo(-22.7,-14.4,-22.2,-9.9).curveTo(-21.6,-5.2,-16.8,-3).curveTo(-14.3,-1.9,-11.5,-2.5).curveTo(-9,-3.2,-6.8,-5.2).curveTo(-4.7,-7.1,-3.8,-9.8).curveTo(-2.8,-12.7,-3.6,-15.6).curveTo(-4,-16.6,-4.9,-17.9).curveTo(-6.7,-20.4,-9.2,-21).curveTo(-10.1,-21.2,-11.3,-21.2).lineTo(-12.7,-21.1).closePath().moveTo(8.2,-22).curveTo(3.6,-20.5,2.1,-15.3).curveTo(1.3,-12.7,2.2,-10).curveTo(3.2,-7.6,5.4,-5.9).curveTo(7.6,-4.1,10.4,-3.7).curveTo(13.4,-3.3,16.1,-4.6).lineTo(16.8,-5.1).lineTo(18.3,-6.3).curveTo(20.4,-8.5,20.8,-11.2).curveTo(20.9,-12.7,20.4,-14.6).curveTo(19.5,-18.5,16.3,-20.7).curveTo(13.7,-22.4,10.9,-22.4).curveTo(9.6,-22.4,8.2,-22).closePath();
	this.shape.setTransform(88.1,66.9);

	// Layer 5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#000000").beginStroke().moveTo(2.5,11.5).curveTo(-6,11.4,-10.3,8).curveTo(-14.2,4.9,-15.6,1.8).curveTo(-16.3,0.2,-16.6,-2.5).curveTo(-16.9,-5.9,-12.9,-8.7).curveTo(-8.8,-11.4,-0.9,-11.5).curveTo(7,-11.5,12.2,-8.6).curveTo(17.3,-5.7,16.5,-1.3).lineTo(15.6,3.4).curveTo(14.8,6.3,13,8.7).curveTo(10.9,11.5,3.1,11.5).lineTo(2.5,11.5).closePath();
	this.shape_1.setTransform(82.1,14.6);

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#A4B250").beginStroke().moveTo(13.7,46).curveTo(3,43.9,2,43.8).lineTo(1.7,43.8).lineTo(1.7,41.6).lineTo(1.6,38.3).lineTo(1.5,38.2).lineTo(1.5,30.9).lineTo(1.5,23.6).lineTo(15.5,23.6).lineTo(15,46.3).lineTo(13.7,46).closePath().moveTo(-15,15.6).curveTo(-15.5,14.7,-15.5,12.1).curveTo(-15.5,5.2,-13,-4.5).curveTo(-11.8,-9.2,-10.2,-13.7).curveTo(-7.9,-20,-4.9,-25.8).curveTo(2.1,-38.9,11.5,-46.3).curveTo(5,-37,0.1,-23.3).curveTo(-1.5,-18.9,-2.6,-14.7).curveTo(-6.5,-0.9,-6.5,11.7).lineTo(-6,18.8).lineTo(-6.9,18.8).lineTo(-8.3,18.9).curveTo(-13.1,18.9,-15,15.6).closePath();
	this.shape_2.setTransform(20,60.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#C1D25F").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).lineTo(-50.2,48.4).lineTo(-49.7,25.7).lineTo(-47.6,25.7).lineTo(-47.6,26).curveTo(-45,25.6,-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.9,-0.6,-37.7,-5.2).curveTo(-37.4,-10.2,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-41.7,-12.8,-42.4,-7.8).curveTo(-44.1,4.1,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,1.2,-67.9,-12.6).curveTo(-66.7,-16.8,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-45.5,-50.7,-35.3,-52.6).lineTo(-4.3,-53.1).curveTo(14.6,-50.1,26.4,-45.7).lineTo(26.4,-45.7).lineTo(26.5,-45.7).curveTo(33.6,-42.7,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(11.6,51.9).lineTo(11.5,51.9).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_3.setTransform(85.2,58);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#E5E5E5").beginStroke().moveTo(-27.8,14.3).curveTo(-29.6,13.9,-44.1,11.4).curveTo(-53,9.8,-54.1,8.6).lineTo(-54.6,7.9).lineTo(-52.1,7.9).curveTo(-50.6,7.5,-42.3,6.7).lineTo(-42,6.7).curveTo(-37.5,10.4,-24,13.2).lineTo(-18,14.3).closePath().moveTo(20.8,6).curveTo(16.4,4.6,16,4.3).curveTo(16,4,15.3,3.4).curveTo(18.7,1.5,21.7,0).curveTo(25.7,3.2,38.6,4.8).curveTo(41.2,5.1,52.1,5.9).lineTo(51.2,6).closePath().moveTo(-33.1,1.2).curveTo(-35.7,1.2,-40.1,1.5).lineTo(-41.3,-0.2).lineTo(-41.3,-0.3).curveTo(-42.5,-1.9,-42.6,-2.2).lineTo(-41.6,-3.1).curveTo(-40.2,-4.3,-38.7,-4.9).lineTo(-34.8,-6.6).curveTo(-24.7,-8.2,-14.2,-6.6).curveTo(-12.6,-6.1,-8.2,-4.1).curveTo(-8.5,-2.5,-9.5,-0.8).lineTo(-11.4,3.5).curveTo(-21.1,1.2,-33.1,1.2).closePath().moveTo(44,-8.6).curveTo(40.4,-9,36.4,-8.8).curveTo(34.3,-8.8,32.3,-8.5).lineTo(32.3,-9.1).curveTo(32.3,-11.1,32.9,-12.4).curveTo(37.3,-14.3,43.6,-14.3).curveTo(50.4,-14,54.6,-12.6).curveTo(53.8,-12.1,51.7,-9.1).lineTo(50.4,-7.3).curveTo(49.2,-7.7,44,-8.6).closePath();
	this.shape_4.setTransform(87.3,136.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_5.setTransform(94.8,140.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#1E4675").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).lineTo(-22.6,3.7).lineTo(-23.3,4.1).curveTo(-27.4,6.6,-27.4,9.9).lineTo(-26.7,13.4).lineTo(-26.6,13.6).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_6.setTransform(67.5,124.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#275994").beginStroke().moveTo(-3.9,14.1).lineTo(-6.2,13.3).curveTo(-5.5,10.4,-4.3,8.3).curveTo(-2.8,6,-2.8,3.7).curveTo(-2.8,1.1,-4.4,0.1).curveTo(-5.7,-0.7,-13.2,-3).curveTo(-24,-4.3,-34.8,-3).curveTo(-40.1,-1.2,-41.9,0.5).lineTo(-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(44,-14.1,44,-10.7).curveTo(37.4,-10.3,32.2,-8.9).curveTo(27.7,-6.9,27.7,-1.2).lineTo(27.7,0.6).curveTo(24.9,1.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_7.setTransform(86.9,128);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#000000").beginStroke().moveTo(-22.1,77.7).curveTo(-41.7,74.4,-47.4,72.4).curveTo(-53.4,70.3,-53.5,66.6).curveTo(-58.7,38.4,-58.7,32.2).lineTo(-58.7,31).lineTo(-60.2,30.4).curveTo(-60.7,30.7,-61.7,30.7).curveTo(-63.2,30.7,-63.7,28.5).lineTo(-63.9,27.5).lineTo(-63.9,24.1).curveTo(-63.9,21.2,-62.8,6).lineTo(-69.1,6).curveTo(-71.7,5.5,-78.8,2.7).curveTo(-79.8,2,-80.3,1.2).curveTo(-81.1,0,-81.1,-4.7).curveTo(-81.1,-12.1,-78.8,-22.3).curveTo(-77.9,-26.1,-76.8,-29.7).curveTo(-74.2,-38.1,-70.5,-45.5).curveTo(-55.8,-74.7,-28.8,-77.3).lineTo(-0.4,-77.7).curveTo(29.9,-73,42.9,-65.3).curveTo(48.3,-62.2,52.6,-57.6).lineTo(60.2,-48.8).lineTo(71.5,-32.5).curveTo(80.3,-23,80.4,-16).lineTo(80.5,-16).lineTo(80.8,-15.5).curveTo(81.1,-15.2,81.1,-14.1).curveTo(81.1,-12.9,79.6,-11.3).curveTo(75.1,-7.7,73.8,-6.8).curveTo(68.8,-3.7,65.6,-2.6).lineTo(67,3.8).curveTo(67.7,6.9,68.7,13.5).curveTo(69.2,17.8,69.2,21.3).curveTo(69.2,26.3,67,27.7).lineTo(66.7,27).curveTo(64,28.8,57.4,31.4).lineTo(56.2,34.1).lineTo(54.9,37).curveTo(54.4,38.2,54.3,39.5).lineTo(62.5,41.3).curveTo(64.4,42.7,65.1,43.8).curveTo(65.5,44.5,65.5,45.3).curveTo(65.5,47.6,62.5,51.3).lineTo(60.9,53.1).curveTo(68.4,57,68.5,64.4).curveTo(68.5,66.2,66.7,67.5).curveTo(63.8,69.7,57.2,69.5).lineTo(25.7,69.5).curveTo(21.2,69.6,18.6,67.3).curveTo(16.8,65.6,16.7,63.7).curveTo(15.9,61.5,14.7,56).lineTo(14.5,54.8).lineTo(14.4,55.2).curveTo(12.4,64.6,10.5,68.4).lineTo(10.8,68.8).lineTo(10.8,68.9).lineTo(10.9,68.9).lineTo(10.9,69.3).lineTo(10.9,70.1).curveTo(10.9,72.5,8,74.7).curveTo(4.9,77.1,0.3,77.7).closePath().moveTo(-48,67.2).curveTo(-46.9,68.5,-38,70).curveTo(-23.4,72.5,-21.6,72.9).lineTo(-11.9,72.9).lineTo(-0.2,72.9).curveTo(1.6,72,4.2,70.2).lineTo(-5.1,67.8).curveTo(-17.3,64.7,-26.6,64.7).curveTo(-28.6,64.7,-35.8,65.3).lineTo(-36.2,65.3).curveTo(-44.4,66.1,-45.9,66.6).lineTo(-48.4,66.6).lineTo(-48,67.2).closePath().moveTo(-7.5,47.2).curveTo(0.1,49.6,1.3,50.4).curveTo(2.9,51.3,3,54).curveTo(3,56.2,1.5,58.6).curveTo(0.2,60.6,-0.5,63.5).lineTo(1.9,64.3).curveTo(5,65.5,7.3,66.7).curveTo(7.8,63.1,9.7,52.2).curveTo(11.5,42.8,12.1,37.7).lineTo(-18.3,38.2).curveTo(-27.9,37.7,-38.6,35.8).curveTo(-42.7,35.1,-47,34.1).curveTo(-52.1,33,-55.4,32.1).lineTo(-54.9,33.7).curveTo(-51.2,47.4,-51.2,61.9).curveTo(-49.3,61.4,-40.4,60.6).lineTo(-40.2,60.6).lineTo(-40.3,60.4).lineTo(-41,56.9).curveTo(-41,53.6,-36.9,51.1).lineTo(-36.2,50.7).curveTo(-34.3,49.1,-29,47.2).curveTo(-23.6,46.6,-18.2,46.6).curveTo(-12.8,46.6,-7.5,47.2).closePath().moveTo(43.3,53.5).curveTo(36.9,54.2,27.9,58.7).curveTo(24.8,60.2,21.4,62.1).curveTo(22.2,62.6,22.1,62.9).curveTo(22.6,63.3,26.9,64.7).lineTo(57.3,64.7).lineTo(58.3,64.5).lineTo(58.6,64.5).curveTo(60.4,64.2,63.2,63.9).curveTo(57.1,53.3,46.2,53.3).curveTo(44.7,53.3,43.3,53.5).closePath().moveTo(-5.2,62.2).lineTo(-3.3,57.8).curveTo(-2.3,56.1,-2.1,54.5).curveTo(-6.5,52.5,-8,52.1).curveTo(-18.6,50.4,-28.6,52.1).lineTo(-32.6,53.7).curveTo(-34.1,54.3,-35.5,55.5).lineTo(-36.5,56.4).curveTo(-36.4,56.7,-35.1,58.4).lineTo(-35.1,58.4).lineTo(-33.9,60.1).curveTo(-29.6,59.8,-26.9,59.8).curveTo(-14.9,59.8,-5.2,62.2).closePath().moveTo(31.8,37.4).lineTo(25.4,37.5).lineTo(16.7,37.7).lineTo(16.7,38.6).curveTo(16.7,40.6,16.5,43.2).curveTo(17.3,45.1,18.1,48.1).curveTo(19.3,52.8,19.6,57.4).curveTo(21.5,55.5,24.8,53.8).curveTo(26.5,53,28.2,52.4).curveTo(30.7,51.5,33.5,50.9).lineTo(33.5,49.1).curveTo(33.4,43.4,38,41.3).curveTo(43.2,39.9,49.8,39.5).curveTo(49.8,36.2,50.2,33.8).curveTo(42.5,35.9,31.8,37.4).closePath().moveTo(50.1,50).curveTo(55.3,51,56.6,51.4).lineTo(57.9,49.5).curveTo(60,46.6,60.7,46).curveTo(56.5,44.6,49.8,44.3).curveTo(43.5,44.3,39.1,46.2).curveTo(38.4,47.5,38.4,49.6).lineTo(38.4,50.1).curveTo(40.4,49.8,42.6,49.8).lineTo(44.7,49.7).curveTo(47.6,49.7,50.1,50).closePath().moveTo(-59.6,13.3).lineTo(-59.6,20.6).lineTo(-59.5,20.6).lineTo(-59.4,24).lineTo(-59.4,26.2).lineTo(-59.1,26.2).curveTo(-58.1,26.2,-47.4,28.3).lineTo(-46.1,28.6).lineTo(-42,29.4).lineTo(-39.6,29.9).curveTo(-24.9,32.8,-18.5,33.3).lineTo(15.7,32.1).lineTo(15.7,32.1).lineTo(25.3,31.8).curveTo(41.1,32,51.6,27.4).curveTo(58.9,24.3,65.2,23.2).curveTo(63.5,18,62.4,11.3).curveTo(60.7,0.1,59.5,-5.7).curveTo(59,-7.9,52.2,-21.3).curveTo(46,-33.5,46,-38.8).curveTo(46,-41.1,46.1,-42.8).curveTo(50.6,-42.7,51.8,-38.1).lineTo(52.7,-33.7).curveTo(53.3,-30.8,54.2,-29).curveTo(56.3,-25.7,58.6,-21.4).curveTo(60.9,-16.8,62.5,-12.8).lineTo(63.7,-9.1).curveTo(65.6,-10.4,68.3,-11.9).curveTo(72.5,-14.2,75.9,-15.2).curveTo(75.3,-21.5,70.2,-28.9).curveTo(68.2,-31.7,56.6,-45.2).lineTo(49.6,-53.5).curveTo(45.4,-57.9,40.6,-60.8).curveTo(37.8,-62.5,30.6,-65.5).lineTo(30.6,-65.4).lineTo(30.6,-65.5).curveTo(18.7,-69.8,-0.2,-72.8).lineTo(-31.2,-72.4).curveTo(-41.3,-70.4,-49.6,-63.9).curveTo(-59,-56.6,-66,-43.5).curveTo(-69,-37.7,-71.3,-31.4).curveTo(-72.9,-26.9,-74.1,-22.2).curveTo(-76.6,-12.4,-76.6,-5.5).curveTo(-76.6,-3,-76.1,-2.1).curveTo(-73.9,1.6,-68,1.1).lineTo(-67.1,1.1).lineTo(-40,1.5).lineTo(-40,-2.5).curveTo(-39.9,-15.6,-38.3,-27.6).curveTo(-37.6,-32.6,-36.6,-37.4).curveTo(-35.2,-44.1,-34.4,-43.8).curveTo(-33.3,-43.4,-33.3,-34).curveTo(-33.3,-29.9,-33.6,-25).curveTo(-33.8,-20.4,-34.2,-15).curveTo(-35.3,-2.4,-35.7,3.6).lineTo(-35.6,3.8).curveTo(-35.6,4.1,-35.7,4.5).lineTo(-35.8,5.2).lineTo(-36.2,5.6).curveTo(-36.6,6,-37.8,6).lineTo(-38.5,6).curveTo(-40.9,5.9,-43.5,6.2).lineTo(-43.5,6).lineTo(-45.6,6).lineTo(-59.6,6).lineTo(-59.6,13.3).closePath();
	this.shape_8.setTransform(81.1,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,162.2,155.5);


(lib.Share_Twitter = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Share on Twitter", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(60.3,21.4,0.8,0.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-25.1,15.8).curveTo(-23.9,16,-22.7,15.9).curveTo(-15.4,15.9,-9.9,11.5).curveTo(-13.2,11.4,-15.9,9.5).curveTo(-18.5,7.5,-19.5,4.3).curveTo(-18.7,4.5,-17.5,4.5).curveTo(-16.3,4.5,-14.8,4.2).curveTo(-18.4,3.5,-20.7,0.6).curveTo(-23.1,-2.2,-23.1,-5.9).lineTo(-23.1,-6).curveTo(-20.9,-4.8,-18.4,-4.8).curveTo(-20.5,-6.2,-21.8,-8.4).curveTo(-23,-10.7,-23,-13.3).curveTo(-23,-16.2,-21.6,-18.5).curveTo(-17.7,-13.8,-12.3,-11).curveTo(-6.7,-8.1,-0.4,-7.7).curveTo(-0.6,-9,-0.6,-10.1).curveTo(-0.6,-14.3,2.4,-17.4).curveTo(5.4,-20.4,9.7,-20.4).curveTo(14.2,-20.4,17.2,-17.2).curveTo(20.5,-17.8,23.7,-19.6).curveTo(22.6,-16.1,19.2,-13.9).curveTo(22.4,-14.4,25.1,-15.6).curveTo(23,-12.5,20,-10.3).lineTo(20,-8.9).curveTo(20,2.3,12.5,10.8).curveTo(4,20.4,-9.3,20.4).curveTo(-17.9,20.4,-25.1,15.8).closePath();
	this.shape.setTransform(31.4,31.9,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#23AADB").beginStroke().moveTo(-161.9,30.6).curveTo(-166.9,30.6,-166.9,25.6).lineTo(-166.9,-25.6).curveTo(-166.9,-30.6,-161.9,-30.6).lineTo(161.9,-30.6).curveTo(166.9,-30.6,166.9,-25.6).lineTo(166.9,25.6).curveTo(166.9,30.6,161.9,30.6).closePath();
	this.shape_1.setTransform(166.9,30.6);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,333.9,61.2);


(lib.Share_FB = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Share on Facebook", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(56.6,20.6,0.8,0.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-6.9,32.3).lineTo(-6.9,2.8).lineTo(-16.8,2.8).lineTo(-16.8,-8.6).lineTo(-6.9,-8.6).lineTo(-6.9,-17.1).curveTo(-6.9,-24.4,-2.8,-28.4).curveTo(1.2,-32.3,7.9,-32.3).curveTo(13.6,-32.3,16.8,-31.8).lineTo(16.8,-21.6).lineTo(10.7,-21.6).curveTo(7.4,-21.6,6,-20).curveTo(5,-18.8,5,-16).lineTo(5,-8.6).lineTo(16.4,-8.6).lineTo(14.9,2.8).lineTo(5,2.8).lineTo(5,32.3).closePath();
	this.shape.setTransform(29,32.4,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#3A5A94").beginStroke().moveTo(-149.9,30.6).curveTo(-154.9,30.6,-154.9,25.6).lineTo(-154.9,-25.6).curveTo(-154.9,-30.6,-149.9,-30.6).lineTo(149.9,-30.6).curveTo(154.9,-30.6,154.9,-25.6).lineTo(154.9,25.6).curveTo(154.9,30.6,149.9,30.6).closePath();
	this.shape_1.setTransform(154.9,30.6);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,309.9,61.2);


(lib.Score = function() {
	this.initialize();

	// Layer 2
	this.mc_Scoring = new cjs.Text("000465", "35px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_Scoring.name = "mc_Scoring";
	this.mc_Scoring.textAlign = "right";
	this.mc_Scoring.lineHeight = 37;
	this.mc_Scoring.setTransform(187.8,23);

	this.mc_ScoringCounter = new cjs.Text("000465", "35px 'Laffayette Comic Pro'", "#999999");
	this.mc_ScoringCounter.name = "mc_ScoringCounter";
	this.mc_ScoringCounter.lineHeight = 37;
	this.mc_ScoringCounter.setTransform(58.3,23);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(3,1,1).moveTo(93.6,27.1).lineTo(93.6,31.6).lineTo(88.9,31.6).lineTo(-89.2,31.6).lineTo(-93.7,31.6).lineTo(-93.7,27.1).lineTo(-93.7,-26.7).lineTo(-93.7,-31.7).lineTo(-89.2,-31.7).lineTo(88.9,-31.7).lineTo(93.6,-31.7).lineTo(93.6,-27.1).lineTo(93.6,-26.7).closePath();
	this.shape.setTransform(124.5,35.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#F4D96F").beginStroke().moveTo(-89,27.2).lineTo(-89,-27.2).lineTo(89.1,-27.2).lineTo(89.1,27.2).closePath().moveTo(-84.5,-1.3).lineTo(-84.5,14.9).lineTo(-84.5,22.7).lineTo(84.8,22.7).lineTo(84.8,-1.3).lineTo(84.8,-22.6).lineTo(-84.5,-22.6).closePath();
	this.shape_1.setTransform(124.3,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#474747").beginStroke().moveTo(-84.7,12).lineTo(-84.7,4.2).lineTo(-84.7,-12).lineTo(84.6,-12).lineTo(84.6,12).closePath();
	this.shape_2.setTransform(124.5,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#666666").beginStroke().moveTo(-84.7,10.7).lineTo(-84.7,-10.7).lineTo(84.6,-10.7).lineTo(84.6,10.7).closePath();
	this.shape_3.setTransform(124.5,23.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB55E").beginStroke().moveTo(88.9,31.6).lineTo(-89.2,31.6).lineTo(-93.7,31.6).lineTo(-93.7,27.1).lineTo(-93.7,-26.6).lineTo(-93.7,-31.7).lineTo(-89.2,-31.7).lineTo(88.9,-31.7).lineTo(93.6,-31.7).lineTo(93.6,-27.1).lineTo(93.6,-26.6).lineTo(93.6,27.1).lineTo(93.6,31.6).closePath().moveTo(-89.2,27.1).lineTo(88.9,27.1).lineTo(88.9,-27.3).lineTo(-89.2,-27.3).closePath();
	this.shape_4.setTransform(124.5,35.4);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.mc_ScoringCounter,this.mc_Scoring);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(29.3,2.2,190.3,66.3);


(lib.S_off = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.8).lineTo(-4.6,6.5).lineTo(-6.5,4.7).lineTo(-1.8,-0).lineTo(-6.5,-4.6).lineTo(-4.6,-6.5).lineTo(-0,-1.8).lineTo(4.7,-6.5).lineTo(6.5,-4.6).lineTo(1.9,-0).lineTo(6.5,4.7).lineTo(4.7,6.5).closePath();
	this.shape.setTransform(6.5,6.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,13,13);


(lib.replaybtn = function() {
	this.initialize();

	// game_icons
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(-29,68.2).curveTo(-37.6,64.4,-47.2,57.1).curveTo(-61.5,46,-64.6,35).lineTo(-29.5,27.5).curveTo(-26.6,31.4,-23.9,33.5).curveTo(-21.4,35.4,-16.5,37.7).curveTo(-1.5,44.6,14.6,38.2).curveTo(30.4,31.9,37.5,16.6).curveTo(39.1,12.9,40,9.5).lineTo(40.1,8.7).curveTo(40.5,7,40.6,6).lineTo(40.9,4).lineTo(41,2.4).curveTo(41.3,-1.6,40.8,-5.3).curveTo(38.5,-21.8,25.3,-31.9).curveTo(12.1,-42,-4.4,-39.8).curveTo(-12.1,-38.8,-18.8,-35).curveTo(-25.3,-31.3,-30,-25.5).curveTo(-25.7,-20.1,-12.8,-7).lineTo(-73,2.3).lineTo(-75.4,-62.4).lineTo(-69.2,-56.7).curveTo(-62,-50.2,-57.2,-46.4).curveTo(-40,-69.6,-9,-73.8).curveTo(12.1,-76.6,31.4,-67.9).curveTo(50.1,-59.4,61.9,-42.6).lineTo(62,-42.7).curveTo(69.2,-32.4,72.6,-20.3).curveTo(76,-8.1,75.2,4.5).lineTo(75.2,5.7).curveTo(74.8,9.9,74.4,12.1).lineTo(71.7,22.9).curveTo(65.2,43,49.1,56.8).curveTo(32.5,70.9,10.9,73.8).curveTo(5.7,74.5,0.6,74.5).curveTo(-14.7,74.5,-29,68.2).closePath();
	this.shape.setTransform(35.1,35.3,0.26,0.26,0,0,0,-0.1,0.1);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-23.8,32.1).curveTo(-22.5,32.6,-21.1,33.1).curveTo(-16.3,34.4,-11,34.4).curveTo(3.4,34.4,13.6,24.2).curveTo(23.8,14,23.8,-0.4).curveTo(23.8,-14.9,13.6,-25.1).curveTo(6.5,-32.2,-2.7,-34.3).curveTo(-2.9,-34.4,-3,-34.4);
	this.shape_1.setTransform(45.9,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill().beginStroke("#333333").setStrokeStyle(4,1,1).moveTo(21.4,-32.9).curveTo(17.6,-33.7,13.4,-33.7).curveTo(-1,-33.7,-11.2,-23.5).curveTo(-21.4,-13.3,-21.4,1.1).curveTo(-21.4,15.6,-11.2,25.8).curveTo(-5.9,31.1,0.6,33.7);
	this.shape_2.setTransform(21.4,33.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#F4D96F").beginStroke().moveTo(-6.9,33.2).curveTo(-13.4,30.7,-18.8,25.3).curveTo(-29,15.1,-29,0.7).curveTo(-29,-13.8,-18.8,-24).curveTo(-8.6,-34.2,5.9,-34.2).curveTo(10.1,-34.2,13.9,-33.3).lineTo(14.2,-33.2).curveTo(23.6,-23.9,27.1,-13.5).curveTo(30.6,-3.2,27.8,6.4).curveTo(25,16.1,16.3,23.4).curveTo(8.1,30.3,-4.2,34.2).lineTo(-6.9,33.2).closePath();
	this.shape_3.setTransform(29,34.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB55E").beginStroke().moveTo(-22.5,33).curveTo(-10.2,29.2,-2,22.3).curveTo(6.7,15,9.5,5.3).curveTo(12.3,-4.4,8.8,-14.7).curveTo(5.3,-25,-4.1,-34.4).curveTo(5.1,-32.3,12.3,-25.1).curveTo(22.5,-14.9,22.5,-0.5).curveTo(22.5,14,12.3,24.2).curveTo(2.1,34.4,-12.4,34.4).curveTo(-17.7,34.4,-22.5,33).closePath();
	this.shape_4.setTransform(47.2,35.3);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.PeppaHead = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#BA9870").beginStroke().moveTo(14.4,47.8).curveTo(3.7,43.3,-8.8,37.2).lineTo(-7.7,34.6).curveTo(-5.8,30,-5.8,27.9).curveTo(-5.8,27.6,-8.2,25.4).curveTo(-11.1,26.9,-11.7,30.5).curveTo(-12.4,34.1,-13.7,34.9).curveTo(-33.4,22.4,-33.4,19).curveTo(-33.4,17.7,-31.8,15.2).curveTo(-30.3,12.8,-30.3,12).curveTo(-30.3,10.9,-30.6,10.5).lineTo(-30.9,10).curveTo(-33.1,10.5,-34.6,11.2).curveTo(-35,8.8,-35,4.3).curveTo(-35,-4.2,-31.4,-14.8).curveTo(-27.4,-26.9,-20,-36.8).curveTo(-11.8,-48,-1.1,-54.4).curveTo(3.1,-45.7,6.7,-37.7).curveTo(16.1,-16.5,18.1,-4.7).curveTo(10.2,-2.9,10.2,4.8).curveTo(10.2,6,10.5,6.3).lineTo(10.8,6.6).curveTo(12.3,6.8,14.3,6.6).lineTo(18.3,5.8).lineTo(19.4,5.6).curveTo(21.1,18,24.2,28).curveTo(27.6,39.2,35,54.3).curveTo(25.2,52.2,14.4,47.8).closePath();
	this.shape.setTransform(38.2,62.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#CC0066").beginStroke().moveTo(-4,1.2).lineTo(-9.7,-1.9).curveTo(-2.9,-3.2,9.7,-0.2).lineTo(6.9,1.2).curveTo(4,2.3,1.3,2.3).curveTo(-1.4,2.3,-4,1.2).closePath();
	this.shape_1.setTransform(89.7,104.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(22.3,33.5).curveTo(0.5,33.5,-22,22.5).curveTo(-30,18.6,-35.8,14.4).curveTo(-38.6,12.4,-39.9,11.1).curveTo(-41,12.2,-42.1,12.2).curveTo(-50.1,12.2,-59.5,2.5).curveTo(-63,-1,-65.5,-4.7).curveTo(-67.8,-8,-67.8,-9.1).curveTo(-67.8,-13.6,-67.4,-14.4).curveTo(-66.7,-15.8,-64.2,-16.9).curveTo(-62.7,-17.6,-60.5,-18.2).lineTo(-60.2,-17.6).curveTo(-59.9,-17.2,-59.9,-16.1).curveTo(-59.9,-15.3,-61.5,-12.9).curveTo(-63,-10.5,-63,-9.1).curveTo(-63,-5.7,-43.3,6.7).curveTo(-42,6,-41.4,2.4).curveTo(-40.7,-1.3,-37.8,-2.8).curveTo(-35.4,-0.6,-35.4,-0.3).curveTo(-35.4,1.9,-37.3,6.5).lineTo(-38.4,9.1).curveTo(-25.9,15.2,-15.2,19.6).curveTo(-4.4,24,5.4,26.2).curveTo(14.4,28.3,22.6,28.4).curveTo(33.6,28.7,42.8,20.5).curveTo(45.1,18.4,49.6,13.5).curveTo(53.6,9.2,55.8,7.5).curveTo(56.3,7.2,56.7,7.1).curveTo(57.1,7,57.5,7.2).curveTo(57.8,7.5,58,7.8).lineTo(58.3,8.4).curveTo(58.6,8.8,59.3,8.2).curveTo(60,7.6,56.5,13.1).curveTo(52.9,18.6,43.9,26.1).curveTo(35,33.5,22.3,33.5).lineTo(22.3,33.5).closePath().moveTo(9.2,16.9).curveTo(3,12.8,3,10.9).curveTo(3,6.2,14.1,6.5).curveTo(18.2,6.6,38.9,9).curveTo(41.3,9.3,41.3,11.8).curveTo(41.3,13.7,36.5,17.3).curveTo(30.6,21.7,23.5,21.7).curveTo(16.4,21.7,9.2,16.9).closePath().moveTo(12.1,11.9).lineTo(17.9,15).curveTo(23,17.2,28.8,15).lineTo(31.6,13.6).curveTo(22.7,11.5,16.7,11.5).curveTo(14.2,11.5,12.1,11.9).closePath().moveTo(21.5,1.9).curveTo(15.5,1,13.6,-1.2).curveTo(12.8,-2.2,12.8,-4.5).curveTo(12.8,-9.6,16.5,-7.2).curveTo(20.8,-3.5,21.9,-3.2).curveTo(28,-1.6,34.1,-3.2).lineTo(34.1,-5).lineTo(34.9,-5.5).curveTo(35.7,-6,36.9,-6).curveTo(38.1,-6,38.7,-5.8).lineTo(39.1,-5.6).lineTo(39.4,-5).curveTo(39.7,-4.6,39.7,-3.5).curveTo(39.7,-2.1,38.1,-0.4).lineTo(35.7,1.9).curveTo(32.4,2.5,28.9,2.5).curveTo(25.3,2.5,21.5,1.9).closePath().moveTo(52.7,-18.4).curveTo(51.3,-18.5,49.4,-19.5).curveTo(47.3,-20.6,45.8,-22.2).curveTo(44.4,-23.7,43.5,-24.2).curveTo(41.8,-24.9,39,-24.4).curveTo(37.7,-24.2,36.7,-23.7).curveTo(36,-24,35.5,-24.3).lineTo(35.3,-24.8).lineTo(35,-25.2).lineTo(34.9,-25.5).lineTo(35.1,-26).curveTo(37.8,-30.2,48.3,-30).curveTo(57.9,-29.8,63.6,-26.7).curveTo(65.6,-25.6,66.9,-23.9).curveTo(67.9,-22.5,67.8,-21.6).lineTo(67.8,-21).lineTo(67.7,-20.9).lineTo(67.7,-20.9).curveTo(66.6,-21.5,64.9,-21.8).lineTo(63.5,-21.9).lineTo(62.8,-21.9).curveTo(61.8,-21.9,61.2,-21.6).lineTo(59.7,-21).curveTo(59.2,-20.8,57.7,-19.8).curveTo(56.4,-18.9,55.5,-18.7).curveTo(54.3,-18.4,53.4,-18.4).lineTo(52.7,-18.4).closePath().moveTo(-6.9,-22.4).curveTo(-7.7,-22.8,-10.2,-22.5).lineTo(-11.3,-22.3).lineTo(-15.3,-21.5).curveTo(-17.3,-21.3,-18.8,-21.5).lineTo(-19.1,-21.9).curveTo(-19.4,-22.1,-19.4,-23.3).curveTo(-19.5,-31,-11.5,-32.9).curveTo(-8.2,-33.7,-3.4,-33.5).curveTo(7.9,-32.9,23.1,-27.2).curveTo(24.5,-26.7,22.9,-25.7).curveTo(21.4,-24.7,19,-24.5).lineTo(16.3,-24.4).lineTo(13.1,-24.5).curveTo(10.2,-21.1,5.5,-20.6).curveTo(3.8,-20.4,2.3,-20.4).curveTo(-2.9,-20.4,-6.9,-22.4).closePath().moveTo(6.9,-25.6).lineTo(7.3,-25.7).lineTo(6.9,-25.7).lineTo(6.9,-25.6).closePath();
	this.shape_2.setTransform(67.8,90.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-7.8,37.2).curveTo(-5,33.6,-4.1,31.7).lineTo(-2.4,28.4).curveTo(0.1,22.4,1.7,16).curveTo(2.7,12,3,6.4).curveTo(3.1,3.8,3.1,-3.4).curveTo(3,-9.5,3.1,-12.4).curveTo(3.2,-13.4,1.1,-24.2).curveTo(-1,-35.1,-1,-36.3).curveTo(-1,-38.2,-0.2,-38.4).curveTo(0.3,-38.5,0.6,-37.7).curveTo(0.9,-38.1,1.6,-37).curveTo(2.2,-36.1,4.1,-26.2).curveTo(5.8,-17.5,6.4,-13.4).lineTo(7.1,-8.1).curveTo(7.9,-2.1,7.7,1.6).curveTo(7.4,12.2,5.1,19.7).curveTo(3.7,24.8,2.3,28.2).curveTo(0.3,33,-2.8,37.9).curveTo(-3.1,38.4,-3.9,38.4).curveTo(-5.2,38.4,-7.8,37.2).closePath();
	this.shape_3.setTransform(128.5,63.4);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#D5AE81").beginStroke().moveTo(-36.7,25.5).curveTo(-43.9,8,-46.6,-10.5).curveTo(-46.6,-24.5,-41.5,-10.8).curveTo(-39.3,-4.9,-33.4,15.8).curveTo(-31.9,18.9,-29.6,26).curveTo(-27.2,33.1,-25.6,36.6).curveTo(-24.1,39.7,-22.4,42.3).lineTo(-24.2,31.3).curveTo(-25.6,22.5,-28.6,15.8).curveTo(-32.5,6.9,-36.6,-6.6).curveTo(-40.4,-19.4,-40.4,-22.7).curveTo(-40.4,-23.2,-40.2,-23.6).curveTo(-41.5,-23.9,-42.1,-24.3).curveTo(-43.6,-25.3,-43.6,-28).curveTo(-43.6,-28.8,-42.5,-29.3).lineTo(-42.6,-30).curveTo(-42.6,-30.9,-42.1,-31.7).lineTo(-47.6,-32).lineTo(-50.1,-34.4).curveTo(-50.6,-35,-50.7,-34.9).curveTo(-52.8,-44.4,-44.4,-46.5).curveTo(-36,-48.6,-33.3,-47.9).curveTo(-30.6,-47.3,-25.6,-56).curveTo(-20.6,-64.8,-11.7,-56.5).curveTo(-2.7,-48.3,10.7,-46.7).curveTo(19.5,-45.7,24.7,-43.8).curveTo(29.5,-42,37.2,-39.9).curveTo(42.7,-38.1,43,-36.2).curveTo(45.8,-33.8,48.7,-16.5).curveTo(50.2,-7.9,51.1,0.5).curveTo(51.1,2.8,50.1,9).curveTo(49.1,15.2,49.1,17.3).curveTo(49.1,20.2,46.7,26.4).curveTo(43.8,34.3,38.9,41).curveTo(24.6,60.5,1.2,60.5).curveTo(-22.2,60.5,-36.7,25.5).closePath().moveTo(9.2,54).lineTo(10.7,54).lineTo(12,53.7).lineTo(9.2,54).closePath();
	this.shape_4.setTransform(84.2,60.5);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,136.3,124.1);


(lib.PeppaClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#4E2A3F").setStrokeStyle(1,1,1).moveTo(31.2,6.8).curveTo(31,6.6,30.9,6.4).curveTo(25,-3.1,15.4,-5.9).curveTo(12.3,-6.8,5.6,-6.8).curveTo(-5.5,-6.8,-16.6,-4.1).curveTo(-24.8,-2.1,-29.1,0.2).curveTo(-30.3,0.9,-31.2,1.5);
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#BA986F").beginStroke().moveTo(35.1,56.3).curveTo(35.3,55.7,35.3,53.5).curveTo(35.7,49.5,35.2,46.3).curveTo(34.8,44.1,34,42.6).curveTo(38.2,42.5,42.4,42.2).lineTo(43.7,52.2).curveTo(42.1,52.5,40.5,53).curveTo(36.5,54.3,35.2,56.4).lineTo(35.1,56.3).closePath().moveTo(0.7,41.8).lineTo(-0.9,39.1).curveTo(4.3,40.3,9.7,41.1).lineTo(13.4,53).curveTo(9.9,53.6,7,54.9).curveTo(3.9,47.6,0.7,41.8).closePath().moveTo(-37.9,1.9).curveTo(-42.1,-2.1,-43.6,-8.6).curveTo(-43.9,-9.9,-42.8,-11.9).curveTo(-41.1,-14.7,-40.1,-16.7).curveTo(-35.5,-24.9,-34.7,-36).curveTo(-34.5,-38.6,-32.6,-47.1).lineTo(-30.6,-56.4).lineTo(-29.4,-55.9).curveTo(-28.6,-55.6,-28.1,-55.5).curveTo(-28.8,-51.3,-30,-37.5).curveTo(-31.3,-21.8,-31.4,-21.6).curveTo(-32.8,-17.1,-35.7,-12.7).curveTo(-37.4,-9,-35.9,-2.8).lineTo(-34.8,1.1).lineTo(-33.7,4.9).curveTo(-35.9,3.9,-37.9,1.9).closePath();
	this.shape_1.setTransform(47.9,96.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#D5AE81").beginStroke().moveTo(-10.1,53.4).curveTo(-14,53.1,-17.6,53.4).curveTo(-19.4,53.6,-21,53.9).lineTo(-24.7,42).curveTo(-15,43.5,-5,43.5).curveTo(-5,44.4,-4.8,45.2).lineTo(-4.1,50.3).lineTo(-3.6,54.7).curveTo(-6,53.8,-10.1,53.4).closePath().moveTo(19.4,52.6).curveTo(13.8,52.2,9.3,53).lineTo(8,43.1).curveTo(19.6,42.1,31.1,39.5).curveTo(30.6,40.7,30.3,41.9).curveTo(30.1,43,28.2,48.2).curveTo(26.8,51.8,26.6,53.8).curveTo(23.3,52.8,19.4,52.6).closePath().moveTo(-63.1,6.8).lineTo(-64.6,6.6).curveTo(-65.7,6.5,-66.7,6.2).lineTo(-68.1,5.7).lineTo(-69.2,1.9).lineTo(-70.3,-1.9).curveTo(-71.8,-8.2,-70.1,-11.8).curveTo(-67.2,-16.3,-65.8,-20.7).curveTo(-65.7,-21,-64.4,-36.7).curveTo(-63.2,-50.5,-62.5,-54.7).curveTo(-61.4,-54.6,-60,-54).lineTo(-58.1,-53.2).curveTo(-55.4,-52.2,-50.8,-48.7).lineTo(-49.6,-47.8).lineTo(-50.4,-45.9).curveTo(-53,-39.5,-55.3,-28.6).curveTo(-57.7,-17.4,-57.4,-15.4).lineTo(-57.4,-15.3).curveTo(-57.2,-14.4,-54.5,-9.5).lineTo(-53.6,-7.8).curveTo(-50.3,-1.5,-50,-0.4).lineTo(-49.9,-0.3).curveTo(-49.4,1.8,-50.9,3.6).lineTo(-51.6,4.3).curveTo(-54.3,6.8,-60.6,6.8).lineTo(-63.1,6.8).closePath().moveTo(53.3,-17.2).curveTo(53.5,-17.5,52.5,-19).curveTo(51.5,-20.6,51.1,-20.6).lineTo(51,-20.5).lineTo(50.9,-20.6).lineTo(50.6,-21.3).curveTo(50.4,-21.7,49.9,-24.1).curveTo(49.4,-26.5,48.7,-28.3).lineTo(47.1,-32.1).curveTo(46.6,-33.3,46.7,-33.5).curveTo(47.4,-37,47.6,-39.1).curveTo(48,-42.1,47.7,-44.9).curveTo(47,-51.5,46.3,-53.8).lineTo(48.6,-54.7).curveTo(49.3,-51.5,53.7,-41.3).curveTo(56,-35.8,56.7,-33.6).curveTo(57.1,-32.4,57.3,-31).curveTo(58.3,-27.3,60.8,-24.2).curveTo(61.8,-22.9,65.3,-19.5).curveTo(71,-13.9,71,-7.2).curveTo(71,-2.8,65.4,-1.2).curveTo(62.1,-0.4,61.5,-0.1).curveTo(61,0.1,60.7,0.4).curveTo(56.7,-11.4,53.3,-17.2).closePath();
	this.shape_2.setTransform(82.3,95.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#964F67").beginStroke().moveTo(0.6,73.5).curveTo(-4.7,73.1,-5.4,72.8).lineTo(-4.5,70.4).curveTo(-4,68.9,-2.4,68).lineTo(-1.1,67.4).lineTo(-0.4,67.2).lineTo(1.2,66.8).lineTo(1.1,67.5).curveTo(1.1,70.8,11,73.2).lineTo(13.6,73.8).curveTo(10.7,74,7.2,74).curveTo(5.5,74,0.6,73.5).closePath().moveTo(30.6,70.9).curveTo(30.6,69.5,30.3,68.3).curveTo(30.9,68.3,31.4,67.6).curveTo(32.2,66.6,32.7,66.2).curveTo(33.6,65.4,35.2,65).curveTo(36.2,69.5,37.5,71).curveTo(38.4,72,40.3,72.8).lineTo(33.1,73.4).curveTo(31.1,73.5,30.5,73.6).curveTo(30.6,72.6,30.6,70.9).closePath().moveTo(17.7,-10.4).curveTo(10.2,-11.4,-2.7,-12.8).curveTo(-12.8,-14,-21.2,-15.7).curveTo(-20.7,-16.6,-20,-18.4).curveTo(-18.7,-22.3,-17.8,-28.1).lineTo(-16.4,-36.3).curveTo(-15.9,-38.9,-15.9,-42).lineTo(-15.9,-43).lineTo(-15.9,-43.1).lineTo(-16,-44).lineTo(-16.1,-44.7).curveTo(-15.4,-45,-14.7,-45.1).curveTo(-14.2,-45.1,-13.3,-43.9).curveTo(-12.6,-42.8,-12.1,-41.7).curveTo(-11.8,-41,-11,-36.3).curveTo(-9.6,-30.9,-5.7,-26.5).curveTo(-1.7,-22.1,3.8,-19.9).curveTo(7,-18.7,13.2,-17.5).curveTo(18.8,-16.5,20.9,-15.5).curveTo(24.2,-13.7,25,-9.7).curveTo(20.8,-10,17.7,-10.4).closePath().moveTo(-32,-51.4).lineTo(-37.6,-53.4).curveTo(-40.2,-54.3,-40.3,-55.2).curveTo(-40.7,-57.9,-36.8,-62.3).curveTo(-33.8,-65.8,-30.2,-68.3).curveTo(-25.7,-71.5,-18.3,-74).curveTo(-20.4,-72.4,-23.8,-67.7).curveTo(-27.7,-62.6,-29.7,-58).curveTo(-30.4,-56.5,-30.9,-53.3).curveTo(-31.1,-51.4,-31.9,-51.4).lineTo(-32,-51.4).closePath();
	this.shape_3.setTransform(55.5,89.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#AF5D78").beginStroke().moveTo(-12.1,80.9).curveTo(-21.9,78.4,-21.9,75.2).lineTo(-21.9,74.5).curveTo(-16,73.2,-2,73.3).curveTo(-0,73.5,1.6,75).curveTo(3.1,76.5,3.1,78.3).lineTo(2.6,78.8).curveTo(1.8,79.4,0.2,80).curveTo(-3.2,81.1,-9.5,81.5).lineTo(-12.1,80.9).closePath().moveTo(14.5,78.6).curveTo(13.2,77.1,12.1,72.6).curveTo(15,71.7,20,71.8).curveTo(27.2,71.8,31.3,73.6).curveTo(34.8,75.2,35.7,77.9).curveTo(36,78.6,32.7,79.1).curveTo(27.3,79.7,17.3,80.5).curveTo(15.3,79.6,14.5,78.6).closePath().moveTo(1.9,-2.1).curveTo(1.1,-6,-2.2,-7.8).curveTo(-4.3,-8.9,-9.8,-9.9).curveTo(-16,-11,-19.2,-12.3).curveTo(-24.8,-14.5,-28.7,-18.9).curveTo(-32.6,-23.3,-34.1,-28.6).curveTo(-34.9,-33.4,-35.2,-34).curveTo(-35.6,-35.2,-36.4,-36.2).curveTo(-37.3,-37.5,-37.8,-37.4).curveTo(-38.5,-37.3,-39.1,-37.1).curveTo(-39.4,-38.9,-40.1,-39.4).curveTo(-41.1,-40.1,-42.7,-38).lineTo(-43.6,-36.6).lineTo(-45.2,-36.3).lineTo(-45.3,-36.6).lineTo(-49.4,-40.6).curveTo(-50.5,-41.7,-51.5,-42.3).lineTo(-55,-43.7).curveTo(-54.2,-43.6,-53.9,-45.7).curveTo(-53.4,-48.9,-52.8,-50.3).curveTo(-50.7,-54.9,-46.9,-60.1).curveTo(-43.4,-64.8,-41.3,-66.3).lineTo(-29,-70.5).curveTo(-23.3,-72.5,-22.5,-73.1).curveTo(-22.2,-73.3,-22.3,-73.8).lineTo(-22.6,-74.4).curveTo(-18.3,-76.8,-10.2,-78.8).curveTo(1,-81.5,12.1,-81.5).curveTo(18.8,-81.5,21.9,-80.6).curveTo(31.4,-77.8,37.4,-68.3).curveTo(36.2,-68.2,36.2,-67).curveTo(36.2,-66.3,41.7,-62.8).curveTo(48.2,-58.7,50.2,-56.7).curveTo(51.5,-55.4,53.7,-49).curveTo(54.4,-46.9,55,-45.6).curveTo(53.9,-45.2,52.6,-44.5).curveTo(50.1,-43.3,48.7,-42.4).curveTo(47.2,-45.6,44.6,-48.6).curveTo(41.9,-51.9,40,-52.3).curveTo(37.9,-52.7,37.9,-49.3).curveTo(37.9,-48.8,42,-43.1).curveTo(46.3,-37.2,46.9,-30.9).curveTo(47.4,-25.1,44.6,-20.8).curveTo(43.2,-18.6,41.6,-17.3).curveTo(41.6,-16.2,41.8,-15.9).lineTo(42,-15.6).curveTo(42.4,-15.2,44.3,-15.2).curveTo(44.8,-15.2,45.7,-15.6).lineTo(46.9,-16.2).curveTo(47,-14.9,47.4,-13.9).lineTo(48.5,-10.9).curveTo(49.6,-7.7,50.9,-5.3).lineTo(41.6,-3.4).curveTo(35.1,-2,18.7,-1.8).lineTo(14.9,-1.8).curveTo(7.4,-1.8,1.9,-2.1).closePath();
	this.shape_4.setTransform(78.6,81.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#7A7A7A").beginStroke().moveTo(-2.7,21.7).lineTo(-17.3,16.4).lineTo(-18.8,16).curveTo(-17.3,13.6,-13.5,5.7).lineTo(-12.4,3.4).curveTo(-10.2,3.7,-7.8,3.8).curveTo(-3.5,4,-0.1,2.5).lineTo(0.8,2).curveTo(1.9,1.5,3.5,0.1).curveTo(5.9,-2.1,6.4,-4).curveTo(6.9,-6.2,5.4,-11.1).curveTo(4.3,-14.4,3.4,-15.9).curveTo(1.4,-19.6,0.5,-21.5).curveTo(2.1,-24.7,2.7,-26.4).curveTo(6.7,-23.8,18.3,-22.2).lineTo(18.8,-22.2).lineTo(15.3,-0.6).curveTo(12.3,19.5,12.2,26.4).curveTo(4.9,24.4,-2.7,21.7).closePath();
	this.shape_5.setTransform(29.7,103.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#8E8E8E").beginStroke().moveTo(-52.4,22).curveTo(-52.2,15.2,-49.2,-5).lineTo(-45.8,-26.5).curveTo(-39.5,-25.6,-20.8,-23.9).curveTo(-11.9,-23.1,5.8,-23.6).curveTo(24.6,-24.2,33.3,-25.8).curveTo(36.2,-26.4,38.2,-27).lineTo(47.5,0.7).curveTo(51.4,12.3,52.3,14.2).curveTo(50.6,15.2,49.1,15.6).lineTo(47.5,16).curveTo(36.6,19.4,20.6,23.2).curveTo(18.4,23.7,4.2,25.3).curveTo(-11.3,27,-16.2,27).curveTo(-33.4,27,-52.4,22).closePath();
	this.shape_6.setTransform(94.2,107.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#000000").beginStroke().moveTo(-25.8,80.7).curveTo(-31.9,79.6,-32.6,78.5).curveTo(-33.4,77.4,-33.4,75.2).curveTo(-33.4,70.1,-28.2,66.5).curveTo(-30,63.8,-32.7,58.3).curveTo(-37.9,47.3,-37.9,47.5).lineTo(-37.9,46.9).curveTo(-53.9,42.3,-69.4,33.9).lineTo(-69.5,34).curveTo(-70.9,33.5,-71.4,32.8).lineTo(-71.7,32.6).lineTo(-71.7,32.3).lineTo(-71.7,30.7).curveTo(-71.7,28.6,-66.8,18.1).curveTo(-70.7,17,-73.1,15.1).curveTo(-77.4,11.6,-78.5,4).curveTo(-79.3,-0.9,-78.4,-3.7).curveTo(-77.8,-5.5,-75.8,-8.3).curveTo(-70.8,-15.4,-69.3,-26.5).curveTo(-68.6,-32,-67.6,-36.7).lineTo(-65.4,-46.7).lineTo(-65.3,-49.4).curveTo(-66.2,-49.7,-66.7,-50.4).curveTo(-67.2,-51.1,-67.3,-52.4).lineTo(-67.2,-53.8).lineTo(-67.2,-53.9).curveTo(-66.4,-63.1,-52.8,-71).curveTo(-40.1,-78.5,-23.3,-81).curveTo(-23.2,-81,-23,-80.4).lineTo(-22.9,-80.3).lineTo(-22.6,-79.7).curveTo(-22.5,-79.2,-22.8,-79).curveTo(-23.6,-78.4,-29.3,-76.4).lineTo(-41.6,-72.2).curveTo(-49,-69.7,-53.5,-66.6).curveTo(-57.1,-64.1,-60.2,-60.5).curveTo(-64,-56.1,-63.7,-53.4).curveTo(-63.5,-52.5,-61,-51.6).lineTo(-55.3,-49.6).lineTo(-51.8,-48.1).curveTo(-50.8,-47.6,-49.7,-46.5).lineTo(-45.6,-42.4).lineTo(-45.5,-42.2).lineTo(-43.9,-42.4).lineTo(-43,-43.9).curveTo(-41.4,-46,-40.4,-45.3).curveTo(-39.7,-44.8,-39.4,-43).lineTo(-39.4,-42.3).lineTo(-39.3,-41.4).lineTo(-39.3,-41.2).lineTo(-39.3,-40.2).curveTo(-39.3,-37.2,-39.8,-34.5).lineTo(-41.2,-26.3).curveTo(-42,-20.5,-43.4,-16.6).curveTo(-44,-14.8,-44.6,-13.9).curveTo(-36.1,-12.2,-26.1,-11).curveTo(-13.2,-9.6,-5.6,-8.6).curveTo(-2.6,-8.2,1.6,-8).curveTo(8.5,-7.5,18.4,-7.6).curveTo(34.8,-7.9,41.3,-9.2).lineTo(50.6,-11.2).curveTo(49.3,-13.6,48.2,-16.8).lineTo(47.1,-19.8).curveTo(46.7,-20.8,46.6,-22.1).lineTo(45.4,-21.5).curveTo(44.5,-21.1,44,-21.1).curveTo(42.1,-21.1,41.7,-21.5).lineTo(41.5,-21.8).curveTo(41.3,-22.1,41.3,-23.2).curveTo(42.9,-24.5,44.3,-26.7).curveTo(47.1,-30.9,46.6,-36.8).curveTo(46,-43,41.7,-48.9).curveTo(37.6,-54.7,37.6,-55.2).curveTo(37.6,-58.5,39.7,-58.2).curveTo(41.6,-57.7,44.3,-54.5).curveTo(46.9,-51.4,48.4,-48.3).curveTo(49.8,-49.2,52.3,-50.4).curveTo(53.6,-51.1,54.7,-51.4).curveTo(54.1,-52.8,53.4,-54.9).curveTo(51.2,-61.3,49.9,-62.6).curveTo(47.9,-64.6,41.4,-68.7).curveTo(35.9,-72.2,35.9,-72.9).curveTo(35.9,-74.1,37.1,-74.2).curveTo(37.9,-74.3,39.4,-73.8).curveTo(42.8,-72.8,47,-69.7).curveTo(58,-61.4,58,-51.7).lineTo(58,-51).lineTo(58,-50.5).lineTo(57.9,-49.9).lineTo(57.8,-49.7).lineTo(57.6,-49.5).lineTo(57.5,-49.4).lineTo(56.4,-48.9).curveTo(55.6,-48.7,55.8,-48.1).curveTo(58.2,-41.8,61.1,-35.5).curveTo(63.2,-31,64.7,-26).curveTo(66.6,-19.4,69.3,-15.9).curveTo(70.7,-14,74,-11.6).curveTo(76.5,-9.6,77.5,-7.5).curveTo(78.9,-4.3,78.9,2).curveTo(78.9,5.3,75.9,8.5).curveTo(72.2,12.4,66.2,12.8).lineTo(65.4,12.9).lineTo(65.7,14).curveTo(70.7,29.6,70.7,35.4).lineTo(70.6,36.4).curveTo(70,37.6,69.5,38).curveTo(69,38.5,68.4,38.5).lineTo(44,45.6).lineTo(39.7,46.8).curveTo(39.6,49.3,37.9,55.2).curveTo(36.2,61.1,34.1,63.8).lineTo(36,64.7).curveTo(42.3,68.3,42,73.6).curveTo(41.9,75.1,41.4,75.6).curveTo(40.9,76.4,39,76.8).curveTo(37.6,77.2,21.6,79.2).curveTo(15,79.9,9.5,79.2).curveTo(7.3,78.7,7.2,78).lineTo(6.9,76.5).lineTo(6.5,77.1).curveTo(4.4,79.5,-4.7,80.7).curveTo(-10.5,81,-15.8,81).curveTo(-21,81,-25.8,80.7).closePath().moveTo(-22.2,68.6).lineTo(-23.8,69).lineTo(-24.4,69.2).lineTo(-25.8,69.8).curveTo(-27.3,70.7,-27.9,72.1).lineTo(-28.7,74.6).curveTo(-28,74.9,-22.7,75.3).curveTo(-17.8,75.8,-16.1,75.7).curveTo(-12.7,75.8,-9.8,75.6).curveTo(-3.5,75.2,-0.1,74.1).curveTo(1.5,73.5,2.3,72.9).lineTo(2.8,72.4).curveTo(2.8,70.7,1.3,69.2).curveTo(-0.3,67.6,-2.3,67.4).lineTo(-4.3,67.4).curveTo(-16.8,67.4,-22.2,68.6).closePath().moveTo(11.8,66.7).curveTo(10.3,67.2,9.3,68).curveTo(8.8,68.3,8,69.3).curveTo(7.5,70.1,6.9,70.1).curveTo(7.3,71.3,7.3,72.7).curveTo(7.3,74.4,7.1,75.4).curveTo(7.8,75.3,9.8,75.2).lineTo(17,74.6).curveTo(27,73.8,32.4,73.2).curveTo(35.7,72.8,35.4,72).curveTo(34.5,69.4,31,67.7).curveTo(26.9,65.9,19.7,65.9).lineTo(19.1,65.9).curveTo(14.5,65.9,11.8,66.7).closePath().moveTo(11.4,51.6).curveTo(7.2,51.9,3,52).curveTo(3.8,53.4,4.2,55.7).curveTo(4.7,58.9,4.3,62.9).curveTo(4.3,65,4.1,65.7).lineTo(4.2,65.7).curveTo(5.5,63.7,9.5,62.4).curveTo(11.1,61.9,12.7,61.6).curveTo(17.2,60.7,22.8,61.2).curveTo(26.7,61.4,30,62.3).curveTo(30.2,60.4,31.6,56.7).curveTo(33.5,51.5,33.7,50.4).curveTo(34,49.2,34.5,48.1).curveTo(23,50.7,11.4,51.6).closePath().moveTo(-30.3,51.2).curveTo(-27.1,57,-24,64.2).curveTo(-21.1,63,-17.6,62.4).curveTo(-16,62.1,-14.2,62).curveTo(-10.6,61.7,-6.7,62).curveTo(-2.6,62.4,-0.2,63.2).lineTo(-0.7,58.9).lineTo(-1.4,53.8).curveTo(-1.6,53,-1.6,52.1).curveTo(-11.6,52,-21.3,50.5).curveTo(-26.7,49.7,-31.9,48.5).lineTo(-30.3,51.2).closePath().moveTo(-62.7,21.5).curveTo(-66.5,29.4,-68,31.8).lineTo(-66.5,32.2).lineTo(-52,37.5).curveTo(-44.4,40.2,-37,42.2).curveTo(-18.1,47.2,-0.9,47.1).curveTo(4,47.1,19.5,45.4).curveTo(33.7,43.8,36,43.3).curveTo(51.9,39.5,62.8,36.1).lineTo(64.4,35.8).curveTo(66,35.3,67.7,34.3).curveTo(66.7,32.4,62.8,20.8).lineTo(53.5,-6.9).curveTo(51.6,-6.3,48.6,-5.7).curveTo(39.9,-4.1,21.1,-3.5).curveTo(3.5,-3,-5.5,-3.7).curveTo(-24.2,-5.5,-30.4,-6.3).lineTo(-30.9,-6.4).curveTo(-42.5,-8,-46.5,-10.6).curveTo(-47.2,-8.9,-48.7,-5.7).curveTo(-47.8,-3.8,-45.8,-0.1).curveTo(-45,1.4,-43.9,4.7).curveTo(-42.3,9.6,-42.8,11.8).curveTo(-43.3,13.7,-45.7,15.9).curveTo(-47.4,17.3,-48.4,17.8).lineTo(-49.4,18.3).curveTo(-52.7,19.8,-57,19.6).curveTo(-59.5,19.5,-61.6,19.2).lineTo(-62.7,21.5).closePath().moveTo(-63.6,-37.7).curveTo(-65.5,-29.2,-65.7,-26.7).curveTo(-66.5,-15.5,-71.1,-7.3).curveTo(-72.1,-5.3,-73.8,-2.6).curveTo(-74.9,-0.5,-74.6,0.8).curveTo(-73.1,7.3,-68.9,11.3).curveTo(-66.9,13.3,-64.7,14.3).lineTo(-63.3,14.7).curveTo(-62.3,15.1,-61.2,15.2).lineTo(-59.7,15.3).curveTo(-51.4,15.9,-48.2,12.8).lineTo(-47.5,12.2).curveTo(-46,10.3,-46.5,8.2).lineTo(-46.6,8.1).curveTo(-46.9,7,-50.2,0.8).lineTo(-51.1,-0.9).curveTo(-53.8,-5.9,-54,-6.7).lineTo(-54,-6.9).curveTo(-54.3,-8.9,-51.9,-20).curveTo(-49.6,-31,-47,-37.3).lineTo(-46.2,-39.3).lineTo(-47.4,-40.2).curveTo(-52,-43.7,-54.7,-44.7).lineTo(-56.6,-45.4).curveTo(-58,-46,-59.1,-46.1).curveTo(-59.6,-46.2,-60.4,-46.6).lineTo(-61.6,-47).lineTo(-63.6,-37.7).closePath().moveTo(55.9,-10.5).curveTo(56.9,-8.9,56.7,-8.7).curveTo(60.1,-2.8,64.1,8.9).curveTo(64.4,8.7,64.9,8.4).curveTo(65.5,8.2,68.8,7.3).curveTo(74.4,5.8,74.4,1.4).curveTo(74.4,-5.4,68.7,-10.9).curveTo(65.2,-14.3,64.2,-15.6).curveTo(61.7,-18.7,60.7,-22.5).curveTo(60.5,-23.8,60.1,-25.1).curveTo(59.4,-27.3,57.1,-32.7).curveTo(52.7,-42.9,52,-46.1).lineTo(49.7,-45.2).curveTo(50.4,-43,51.1,-36.3).curveTo(51.4,-33.6,51,-30.6).curveTo(50.8,-28.5,50.1,-25).curveTo(50,-24.7,50.5,-23.5).lineTo(52.1,-19.8).curveTo(52.8,-18,53.3,-15.5).curveTo(53.8,-13.2,54,-12.7).lineTo(54.3,-12).lineTo(54.4,-11.9).lineTo(54.5,-12).curveTo(54.9,-12,55.9,-10.5).closePath().moveTo(-47.2,-22.7).curveTo(-49.6,-17.2,-49.9,-12.8).curveTo(-48.8,-14.1,-48,-14.5).lineTo(-48.3,-15).curveTo(-48.4,-15.2,-48.4,-16.5).curveTo(-48.4,-17.3,-47.4,-20.9).lineTo(-46.5,-24.2).lineTo(-47.2,-22.7).closePath().moveTo(37.3,-73.8).lineTo(37.1,-74.2).lineTo(37.3,-73.8).closePath().moveTo(-25,-79).curveTo(-24.1,-79.7,-22.9,-80.3).curveTo(-24.1,-79.7,-25,-79).closePath();
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.Options_btn = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(3,17.1).lineTo(3,2.2).lineTo(17.9,2.2).lineTo(17.9,17.1).closePath().moveTo(-17.9,17.1).lineTo(-17.9,2.2).lineTo(-3,2.2).lineTo(-3,17.1).closePath().moveTo(3,-2.2).lineTo(3,-17.1).lineTo(17.9,-17.1).lineTo(17.9,-2.2).closePath().moveTo(-17.9,-2.2).lineTo(-17.9,-17.1).lineTo(-3,-17.1).lineTo(-3,-2.2).closePath();
	this.shape.setTransform(35.8,35.8);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-23.8,32.1).curveTo(-22.5,32.6,-21.1,33.1).curveTo(-16.3,34.4,-11,34.4).curveTo(3.4,34.4,13.6,24.2).curveTo(23.8,14,23.8,-0.4).curveTo(23.8,-14.9,13.6,-25.1).curveTo(6.5,-32.2,-2.7,-34.3).curveTo(-2.9,-34.4,-3,-34.4);
	this.shape_1.setTransform(45.9,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill().beginStroke("#333333").setStrokeStyle(4,1,1).moveTo(21.4,-32.9).curveTo(17.6,-33.7,13.4,-33.7).curveTo(-1,-33.7,-11.2,-23.5).curveTo(-21.4,-13.3,-21.4,1.1).curveTo(-21.4,15.6,-11.2,25.8).curveTo(-5.9,31.1,0.6,33.7);
	this.shape_2.setTransform(21.4,33.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#F4D96F").beginStroke().moveTo(-6.9,33.2).curveTo(-13.4,30.7,-18.8,25.3).curveTo(-29,15.1,-29,0.7).curveTo(-29,-13.8,-18.8,-24).curveTo(-8.6,-34.2,5.9,-34.2).curveTo(10.1,-34.2,13.9,-33.3).lineTo(14.2,-33.2).curveTo(23.6,-23.9,27.1,-13.5).curveTo(30.6,-3.2,27.8,6.4).curveTo(25,16.1,16.3,23.4).curveTo(8.1,30.3,-4.2,34.2).lineTo(-6.9,33.2).closePath();
	this.shape_3.setTransform(29,34.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB55E").beginStroke().moveTo(-22.5,33).curveTo(-10.2,29.2,-2,22.3).curveTo(6.7,15,9.5,5.3).curveTo(12.3,-4.4,8.8,-14.7).curveTo(5.3,-25,-4.1,-34.4).curveTo(5.1,-32.3,12.3,-25.1).curveTo(22.5,-14.9,22.5,-0.5).curveTo(22.5,14,12.3,24.2).curveTo(2.1,34.4,-12.4,34.4).curveTo(-17.7,34.4,-22.5,33).closePath();
	this.shape_4.setTransform(47.2,35.3);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.No_Btn = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("no", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 112;
	this.text.setTransform(79.2,13.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-67.7,-28.5).lineTo(67.6,-28.5).curveTo(81.8,-28.5,81.8,-14.3).lineTo(81.8,14.3).curveTo(81.8,28.5,67.6,28.5).lineTo(-67.7,28.5).curveTo(-81.8,28.5,-81.8,14.3).lineTo(-81.8,-14.3).curveTo(-81.8,-28.5,-67.7,-28.5).closePath();
	this.shape.setTransform(81.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#7EEA9B").beginStroke().moveTo(-67.7,28.5).curveTo(-81.8,28.5,-81.8,14.3).lineTo(-81.8,-14.3).curveTo(-81.8,-28.5,-67.7,-28.5).lineTo(67.6,-28.5).curveTo(81.8,-28.5,81.8,-14.3).lineTo(81.8,14.3).curveTo(81.8,28.5,67.6,28.5).closePath();
	this.shape_1.setTransform(81.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("rgba(0,0,0,0.502)").beginStroke().moveTo(-67.7,28.5).curveTo(-81.8,28.5,-81.8,14.3).lineTo(-81.8,-14.3).curveTo(-81.8,-28.5,-67.7,-28.5).lineTo(67.6,-28.5).curveTo(81.8,-28.5,81.8,-14.3).lineTo(81.8,14.3).curveTo(81.8,28.5,67.6,28.5).closePath();
	this.shape_2.setTransform(90.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.NewMessageStatic = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("New message!", "25px 'Arial'");
	this.text.textAlign = "center";
	this.text.lineHeight = 31;
	this.text.lineWidth = 185;
	this.text.setTransform(102.5,13.2);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(100.5,-0.6).lineTo(100.5,0.6).lineTo(100.5,8.9).curveTo(100.5,16.9,92.5,16.9).lineTo(-92.5,16.9).curveTo(-100.5,16.9,-100.5,8.9).lineTo(-100.5,0.6).lineTo(-100.5,-0.6).lineTo(-100.5,-0.9).lineTo(-100.5,-1.3).lineTo(-100.5,-8.9).curveTo(-100.5,-16.9,-92.5,-16.9).lineTo(92.5,-16.9).curveTo(100.5,-16.9,100.5,-8.9).lineTo(100.5,-1.3).lineTo(100.5,-0.9);
	this.shape.setTransform(104.5,26.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#D3BB61").beginStroke().moveTo(-92.5,16.9).curveTo(-100.5,16.9,-100.5,8.9).lineTo(-100.5,0.6).lineTo(-100.5,-0.6).lineTo(-100.5,-0.9).lineTo(-100.5,-1.3).lineTo(-100.5,-8.9).curveTo(-100.5,-16.9,-92.5,-16.9).lineTo(92.5,-16.9).curveTo(100.5,-16.9,100.5,-8.9).lineTo(100.5,-1.3).lineTo(100.5,-0.9).lineTo(100.5,-0.6).lineTo(100.5,0.6).lineTo(100.5,8.9).curveTo(100.5,16.9,92.5,16.9).closePath();
	this.shape_1.setTransform(104.5,26.1);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,8.2,203,38.4);


(lib.NewMessage = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text = new cjs.Text("New message!", "25px 'Arial'");
	this.text.textAlign = "center";
	this.text.lineHeight = 31;
	this.text.lineWidth = 185;
	this.text.setTransform(102.5,13.2);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(100.5,-0.6).lineTo(100.5,0.6).lineTo(100.5,8.9).curveTo(100.5,16.9,92.5,16.9).lineTo(-92.5,16.9).curveTo(-100.5,16.9,-100.5,8.9).lineTo(-100.5,0.6).lineTo(-100.5,-0.6).lineTo(-100.5,-0.9).lineTo(-100.5,-1.3).lineTo(-100.5,-8.9).curveTo(-100.5,-16.9,-92.5,-16.9).lineTo(92.5,-16.9).curveTo(100.5,-16.9,100.5,-8.9).lineTo(100.5,-1.3).lineTo(100.5,-0.9);
	this.shape.setTransform(104.5,26.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#D3BB61").beginStroke().moveTo(-92.5,16.9).curveTo(-100.5,16.9,-100.5,8.9).lineTo(-100.5,0.6).lineTo(-100.5,-0.6).lineTo(-100.5,-0.9).lineTo(-100.5,-1.3).lineTo(-100.5,-8.9).curveTo(-100.5,-16.9,-92.5,-16.9).lineTo(92.5,-16.9).curveTo(100.5,-16.9,100.5,-8.9).lineTo(100.5,-1.3).lineTo(100.5,-0.9).lineTo(100.5,-0.6).lineTo(100.5,0.6).lineTo(100.5,8.9).curveTo(100.5,16.9,92.5,16.9).closePath();
	this.shape_1.setTransform(104.5,26.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).to({state:[]},4).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3,8.2,203,38.4);


(lib.Male_Head_01_Front = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(-21.1,4.3).curveTo(-22.2,3.1,-22.2,1.9).curveTo(-22.2,1.4,-22.1,1).lineTo(-28.4,1.1).curveTo(-30.3,1.4,-32.8,2.3).curveTo(-34.8,2.9,-35.7,2.9).curveTo(-36.9,2.9,-37.8,2.7).lineTo(-38.3,2.6).lineTo(-38.7,0.8).lineTo(-38.2,-0.4).curveTo(-38,-1.1,-37.4,-1.5).curveTo(-35.6,-2.7,-28.3,-3.4).curveTo(-22.2,-5.1,-9.5,-3.8).curveTo(-7.3,-1.9,-7.3,-1.4).curveTo(-7.3,-1.2,-8.2,-0.4).curveTo(-8.9,0.4,-9.5,0.7).lineTo(-13.5,0.8).curveTo(-13.2,1.3,-13.2,2).curveTo(-13.2,4.1,-14.6,5.1).curveTo(-15.6,5.9,-17,5.9).curveTo(-19.6,5.9,-21.1,4.3).closePath().moveTo(25.6,2.5).curveTo(23.5,2.1,22.6,1.3).curveTo(22.4,1,22.2,0.7).lineTo(22.1,0.4).lineTo(22.2,-0.4).lineTo(21.8,-0.4).curveTo(20.7,-0.6,19.7,-1).lineTo(18.9,-1.3).lineTo(18.9,-1.6).lineTo(18.8,-1.7).curveTo(18.7,-2,18.9,-3).curveTo(19.4,-6.3,28.5,-5.9).curveTo(32.6,-5.7,38.3,-4.8).lineTo(38.4,-4.8).lineTo(38.5,-4.2).curveTo(38.7,-3.8,38.5,-2.7).curveTo(38.4,-1.9,32.9,-0.8).lineTo(30.8,-0.4).lineTo(30.5,-0.3).lineTo(30.5,-0.2).curveTo(30,2.7,27,2.7).lineTo(25.6,2.5).closePath();
	this.shape.setTransform(96.2,88.1);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#EACFB7").beginStroke().moveTo(-19.6,4.8).lineTo(-20.7,4.7).curveTo(-20.7,4.5,-21,4.2).curveTo(-21.3,4,-21.3,2.2).lineTo(-33.5,3.2).curveTo(-34.9,3.3,-36,3.2).lineTo(-36.7,3).curveTo(-36.7,2.9,-36.7,2.9).curveTo(-36.7,2.9,-36.8,2.9).curveTo(-36.8,2.9,-36.9,2.9).curveTo(-36.9,3,-37,3).curveTo(-37.2,3.2,-37.3,1.8).curveTo(-37.4,0.4,-37,-0).curveTo(-36.2,-0.8,-33.3,-1.3).curveTo(-28.9,-2.1,-7.2,-3.8).lineTo(-2.9,-4.1).curveTo(-2.9,-4,4.5,-4.2).lineTo(19.2,-4.7).curveTo(24.2,-4.9,27.8,-4.8).curveTo(32.9,-4.6,35.1,-3.8).curveTo(37.3,-3,37.3,-1.3).curveTo(37.3,0.2,36.1,0.5).lineTo(34,0.5).lineTo(32.6,0.5).curveTo(32.5,2.9,32.2,3.6).lineTo(29.4,4.1).curveTo(24.9,4,23.7,2.4).curveTo(23.4,2,23.3,-0).lineTo(20,-0.4).curveTo(-6.2,0.6,-7.2,0.1).lineTo(-7.2,0.1).lineTo(-12.1,1).curveTo(-12.2,2.5,-13.4,3.5).curveTo(-14.7,4.7,-16.7,4.8).lineTo(-18.1,4.9).lineTo(-19.6,4.8).closePath();
	this.shape_1.setTransform(94.6,87.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#DEBBA4").beginStroke().moveTo(21.4,58.2).curveTo(14.1,55.4,6.6,48).curveTo(3.7,45.1,1.7,40.7).lineTo(-1,34.9).lineTo(0.3,34.2).curveTo(2.2,33.1,2.2,30.7).curveTo(2.2,29.6,2,29).curveTo(1.3,27.6,-0.7,27.6).curveTo(-1.8,27.6,-5.2,28.7).curveTo(-8.5,29.8,-9.7,29.8).curveTo(-13.6,29.8,-17.3,27.8).curveTo(-22.7,24.9,-26.2,18.4).curveTo(-26.5,17.8,-26.4,16.8).lineTo(-26.3,14.9).curveTo(-26.4,11.2,-23.7,9.3).curveTo(-22.7,8.6,-22,7.8).curveTo(-18.9,8.5,-16.2,10).curveTo(-14.2,11.2,-11.7,11.2).curveTo(-10.6,11.2,-10,10.9).curveTo(-9.1,10.5,-9.1,9.1).curveTo(-5.4,11.5,-1.1,11.9).curveTo(3.3,12.4,2.4,9.7).curveTo(-1.7,-2.6,-3.6,-18.3).curveTo(-5.4,-34.7,-3.4,-45.1).curveTo(-1.6,-54.4,2.2,-59.4).lineTo(2.6,-59.9).curveTo(13.9,-44.9,16,-27.2).curveTo(17.3,-16.4,18,8.8).curveTo(19.9,24.3,22.4,37.6).curveTo(24.9,50.7,26.5,59.9).curveTo(23.7,59.1,21.4,58.2).closePath();
	this.shape_2.setTransform(30.8,89.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#AA8462").beginStroke().moveTo(9.8,42.5).curveTo(5.5,42.1,1.8,39.8).lineTo(1.8,39.7).curveTo(1.8,38.1,-0.9,36.2).curveTo(-3.2,34.6,-6.8,33.5).curveTo(-8.5,33,-9.8,33).curveTo(-13.6,11.1,-13.6,10.1).curveTo(-13.6,2.9,-12.1,-5.7).curveTo(-9.9,-18.3,-5.2,-28.2).curveTo(-1,-36.8,4.8,-42.6).curveTo(9.1,-37,12.4,-31.6).lineTo(13.6,-29.4).lineTo(13.5,-29.3).lineTo(13.1,-28.7).curveTo(9.3,-23.8,7.5,-14.5).curveTo(5.5,-4,7.3,12.4).curveTo(9.2,28,13.3,40.3).curveTo(14.1,42.6,11,42.6).lineTo(9.8,42.5).closePath();
	this.shape_3.setTransform(19.9,58.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CEA077").beginStroke().moveTo(-29.3,6.8).curveTo(-33.7,5.7,-37.3,10).lineTo(-38.6,7.8).curveTo(-41.8,2.4,-46.1,-3.2).curveTo(-32.8,-16.5,-10.9,-14.4).lineTo(14.7,-14.4).curveTo(34.8,-10,46.1,0).curveTo(42.7,14.1,21.1,14.7).lineTo(19.1,14.7).curveTo(2,14.7,-29.3,6.8).closePath();
	this.shape_4.setTransform(70.8,19.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#000000").beginStroke().moveTo(-1.1,77.7).curveTo(-7.2,76.8,-11.6,75.5).curveTo(-22.3,72.3,-29.7,67.2).curveTo(-41.3,59.1,-44.9,46.1).lineTo(-44.9,46).lineTo(-48.2,46.1).curveTo(-58.3,46.1,-64,39.4).curveTo(-68.1,34.6,-68.3,29.9).lineTo(-69.2,29.5).curveTo(-69.4,27.4,-69.4,25.1).curveTo(-69.4,20.7,-69,18.8).curveTo(-68,14.7,-64.7,13.7).curveTo(-65.9,10.7,-66.9,-0).lineTo(-67.7,-10.3).curveTo(-67.7,-33.1,-58.9,-51.3).curveTo(-54.4,-60.7,-48,-66.9).curveTo(-33.5,-81,-9.4,-78.7).lineTo(-9.4,-79.1).lineTo(15.3,-79.1).curveTo(41.2,-74.5,54.6,-60.4).curveTo(60.6,-54,64.1,-44.2).curveTo(66.3,-38.2,67.3,-32.8).curveTo(66.9,-32.9,68.1,-28.5).curveTo(69.4,-23.6,69.4,-12.5).curveTo(69.5,0.1,67.4,6.2).curveTo(67,7.3,67.2,11.2).curveTo(67.5,15.4,67.4,16.1).curveTo(66.5,21.9,65.3,26.5).curveTo(63.5,33.6,59.9,42).curveTo(59.6,42.6,59.1,45.3).lineTo(58.6,48.9).curveTo(57.7,60.7,50.3,69.1).curveTo(42.6,77.7,31.5,78.8).lineTo(10.8,79.1).curveTo(2.8,78.3,-1.1,77.7).closePath().moveTo(-36.7,39.2).curveTo(-36.5,39.8,-36.5,41).curveTo(-36.5,43.3,-38.4,44.5).lineTo(-39.7,45.1).lineTo(-37,50.9).curveTo(-35,55.4,-32.1,58.2).curveTo(-24.6,65.6,-17.3,68.4).curveTo(-15,69.3,-12.2,70.1).curveTo(1,73.8,23.3,73.7).curveTo(33.5,73.6,41.1,68.9).curveTo(51.2,62.6,53,49.3).lineTo(53,49.1).curveTo(49.8,49.5,45.8,50.6).curveTo(41.4,51.7,38.1,52.2).curveTo(30.5,53.5,21.9,53.2).curveTo(16.2,53,7.2,51.6).lineTo(0.8,49.3).curveTo(0.8,46.3,6,47).curveTo(20.8,49.4,36.6,47.1).curveTo(41.3,46.4,44.9,45.4).curveTo(48.4,44.5,52.9,42.8).curveTo(53.6,42.5,54.7,40.1).lineTo(58.8,31.1).curveTo(60.9,26,61.8,21.3).curveTo(62.5,17.1,62.5,11.3).curveTo(62.5,9.9,62.7,9).curveTo(62.5,8.4,62.5,7.7).curveTo(63.4,-0.2,64,-8.2).curveTo(65,-23.8,62.7,-31.2).curveTo(61.4,-36.3,58.8,-42.3).curveTo(53.7,-54.4,47.5,-59.8).curveTo(36.1,-69.8,16.1,-74.2).lineTo(-9.5,-74.2).curveTo(-31.4,-76.3,-44.8,-63).curveTo(-50.6,-57.2,-54.7,-48.6).curveTo(-59.5,-38.7,-61.7,-26.1).curveTo(-63.2,-17.5,-63.2,-10.3).curveTo(-63.2,-9.3,-59.3,12.6).curveTo(-58.1,12.6,-56.4,13.1).curveTo(-52.8,14.2,-50.5,15.8).curveTo(-47.8,17.7,-47.8,19.3).lineTo(-47.8,19.4).curveTo(-47.8,20.7,-48.7,21.1).curveTo(-49.3,21.4,-50.4,21.4).curveTo(-52.9,21.4,-54.9,20.3).curveTo(-57.6,18.8,-60.7,18).curveTo(-61.4,18.8,-62.4,19.5).curveTo(-65,21.4,-65,25.1).lineTo(-65.1,27).curveTo(-65.1,28,-64.9,28.6).curveTo(-61.3,35.1,-56,38).curveTo(-52.3,40,-48.3,40).curveTo(-47.1,40,-43.8,38.9).curveTo(-40.5,37.8,-39.3,37.8).curveTo(-37.3,37.8,-36.7,39.2).closePath().moveTo(17.8,33.5).curveTo(16.7,32.4,16.6,30.9).lineTo(16.7,28.9).curveTo(16.7,27.2,17.7,25.1).curveTo(18.9,22.7,20.4,22.8).curveTo(22,23,22.1,24.7).lineTo(22,28.9).lineTo(27.4,31.4).lineTo(41.2,31.4).curveTo(43,30.7,43.5,29.2).curveTo(43.8,28.3,43.8,25.4).curveTo(43.7,21.2,46.2,22.6).curveTo(48.8,24.1,48.8,27.9).curveTo(48.8,31.8,47.2,33.5).curveTo(44.1,36.8,34.4,36.8).curveTo(21.3,36.8,17.8,33.5).closePath();
	this.shape_5.setTransform(69.4,79.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-26.3,63.2).curveTo(-27.8,54,-30.3,40.8).curveTo(-32.8,27.6,-34.8,12.1).curveTo(-35.4,-13.1,-36.8,-23.9).curveTo(-38.9,-41.6,-50.2,-56.6).lineTo(-50.1,-56.8).curveTo(-46.4,-61.1,-42,-60).curveTo(-8.9,-51.5,8.4,-52).curveTo(29.9,-52.6,33.4,-66.7).curveTo(39.6,-61.3,44.7,-49.3).curveTo(47.3,-43.2,48.6,-38.2).curveTo(50.9,-30.8,49.9,-15.1).curveTo(49.3,-7.2,48.4,0.8).curveTo(48.4,1.4,48.6,2.1).curveTo(48.4,3,48.4,4.4).curveTo(48.4,10.1,47.7,14.3).curveTo(46.8,19.1,44.7,24.1).lineTo(40.6,33.1).curveTo(39.5,35.5,38.8,35.8).curveTo(34.3,37.6,30.8,38.5).curveTo(27.2,39.4,22.5,40.1).curveTo(6.7,42.5,-8.1,40.1).curveTo(-13.3,39.4,-13.3,42.3).lineTo(-6.9,44.6).curveTo(2.1,46,7.8,46.2).curveTo(16.4,46.5,24,45.3).curveTo(27.3,44.7,31.7,43.6).curveTo(35.7,42.6,38.9,42.1).lineTo(38.9,42.3).curveTo(37.1,55.6,27,62).curveTo(19.4,66.7,9.2,66.7).lineTo(8,66.7).curveTo(-13.4,66.7,-26.3,63.2).closePath().moveTo(3.6,18.2).curveTo(2.6,20.3,2.6,21.9).lineTo(2.5,23.9).curveTo(2.6,25.5,3.7,26.6).curveTo(7.2,29.9,20.3,29.9).curveTo(30,29.9,33.1,26.6).curveTo(34.7,24.8,34.7,21).curveTo(34.7,17.1,32.1,15.7).curveTo(29.6,14.2,29.7,18.5).curveTo(29.7,21.3,29.4,22.3).curveTo(28.9,23.8,27.1,24.4).lineTo(13.3,24.4).lineTo(7.9,21.9).lineTo(8,17.7).curveTo(7.9,16.1,6.3,15.9).lineTo(6.1,15.9).curveTo(4.7,15.9,3.6,18.2).closePath().moveTo(-10,5.6).curveTo(-9.6,5.9,-9.6,6.1).lineTo(-8.5,6.2).curveTo(-7.1,6.3,-5.6,6.2).curveTo(-3.6,6.1,-2.3,4.9).curveTo(-1.2,3.9,-1.1,2.4).lineTo(3.8,1.5).lineTo(3.9,1.5).curveTo(4.9,2,31.1,1).lineTo(34.4,1.4).curveTo(34.5,3.4,34.8,3.8).curveTo(36,5.4,40.4,5.5).lineTo(43.3,5).curveTo(43.5,4.3,43.6,1.9).lineTo(45,1.9).lineTo(47.2,1.9).curveTo(48.4,1.6,48.4,0.1).curveTo(48.4,-1.6,46.1,-2.4).curveTo(43.9,-3.2,38.9,-3.4).curveTo(35.3,-3.5,30.2,-3.3).lineTo(15.5,-2.8).curveTo(8.1,-2.6,8.1,-2.7).lineTo(3.9,-2.4).curveTo(-17.8,-0.7,-22.3,0.1).curveTo(-25.2,0.6,-25.9,1.4).curveTo(-26.4,1.8,-26.3,3.2).curveTo(-26.2,4.6,-25.9,4.4).curveTo(-25.9,4.4,-25.8,4.3).curveTo(-25.8,4.3,-25.7,4.3).curveTo(-25.7,4.3,-25.7,4.3).curveTo(-25.7,4.3,-25.7,4.4).lineTo(-24.9,4.6).curveTo(-23.9,4.7,-22.4,4.6).lineTo(-10.3,3.6).curveTo(-10.3,5.4,-10,5.6).closePath();
	this.shape_6.setTransform(83.5,86.1);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,138.9,158.3);


(lib.Male_Greg_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#BC9A72").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#44446F").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape_1.setTransform(18,46.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#6666A8").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_2.setTransform(85.2,58);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#D5AE81").beginStroke().moveTo(-70.6,9.4).lineTo(-75.5,7.6).lineTo(-75.4,6.9).curveTo(-74.7,2.8,-74.6,0.7).lineTo(-51.3,0.7).lineTo(-51,7.1).curveTo(-51.1,13.8,-54.3,13.8).curveTo(-57.3,13.8,-70.6,9.4).closePath().moveTo(59.2,-1.5).lineTo(57.8,-7.9).curveTo(61,-9,66,-12.1).lineTo(68.5,-13.8).curveTo(69.3,-12.4,71.8,-9.1).curveTo(75.5,-4.2,75.5,0.3).curveTo(75.5,2.7,66.6,3.5).curveTo(62.5,3.8,60.3,4.4).lineTo(59.2,-1.5).closePath();
	this.shape_3.setTransform(88.9,83);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_4.setTransform(86,143.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_5.setTransform(67.5,124.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_6.setTransform(86.9,128);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_7.setTransform(94.8,140.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.Male_Frank_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#7A4D30").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#975F3C").beginStroke().moveTo(-70.6,9.4).lineTo(-75.5,7.6).lineTo(-75.4,6.9).curveTo(-74.7,2.8,-74.6,0.7).lineTo(-51.3,0.7).lineTo(-51,7.1).curveTo(-51.1,13.8,-54.3,13.8).curveTo(-57.3,13.8,-70.6,9.4).closePath().moveTo(59.2,-1.5).lineTo(57.8,-7.9).curveTo(61,-9,66,-12.1).lineTo(68.5,-13.8).curveTo(69.3,-12.4,71.8,-9.1).curveTo(75.5,-4.2,75.5,0.3).curveTo(75.5,2.7,66.6,3.5).curveTo(62.5,3.8,60.3,4.4).lineTo(59.2,-1.5).closePath();
	this.shape_1.setTransform(88.9,83);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#70656E").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape_2.setTransform(18,46.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#91838F").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_3.setTransform(85.2,58);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_4.setTransform(86,143.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_5.setTransform(67.5,124.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_6.setTransform(86.9,128);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_7.setTransform(94.8,140.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.Male_Body_Frank_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#4181C0").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape.setTransform(18,46.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#478DD3").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_1.setTransform(85.2,58);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_2.setTransform(86,143.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_3.setTransform(67.5,124.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_4.setTransform(9.8,86.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_5.setTransform(86.9,128);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_6.setTransform(94.8,140.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-70.6,9.4).lineTo(-75.5,7.6).lineTo(-75.4,6.9).curveTo(-74.7,2.8,-74.6,0.7).lineTo(-51.3,0.7).lineTo(-51,7.1).curveTo(-51.1,13.8,-54.3,13.8).curveTo(-57.3,13.8,-70.6,9.4).closePath().moveTo(59.2,-1.5).lineTo(57.8,-7.9).curveTo(61,-9,66,-12.1).lineTo(68.5,-13.8).curveTo(69.3,-12.4,71.8,-9.1).curveTo(75.5,-4.2,75.5,0.3).curveTo(75.5,2.7,66.6,3.5).curveTo(62.5,3.8,60.3,4.4).lineTo(59.2,-1.5).closePath();
	this.shape_7.setTransform(88.9,83);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.Male_Body_02_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#497C59").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape.setTransform(18,46.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#5A996E").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_1.setTransform(85.2,58);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_2.setTransform(86,143.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_3.setTransform(67.5,124.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_4.setTransform(9.8,86.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_5.setTransform(86.9,128);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_6.setTransform(94.8,140.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-70.6,9.4).lineTo(-75.5,7.6).lineTo(-75.4,6.9).curveTo(-74.7,2.8,-74.6,0.7).lineTo(-51.3,0.7).lineTo(-51,7.1).curveTo(-51.1,13.8,-54.3,13.8).curveTo(-57.3,13.8,-70.6,9.4).closePath().moveTo(59.2,-1.5).lineTo(57.8,-7.9).curveTo(61,-9,66,-12.1).lineTo(68.5,-13.8).curveTo(69.3,-12.4,71.8,-9.1).curveTo(75.5,-4.2,75.5,0.3).curveTo(75.5,2.7,66.6,3.5).curveTo(62.5,3.8,60.3,4.4).lineTo(59.2,-1.5).closePath();
	this.shape_7.setTransform(88.9,83);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.MailSack03 = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("MAIL", "44px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 46;
	this.text.setTransform(65.9,75.3,0.66,0.66,0,-6.8,-6.3);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#896F3C").beginStroke().moveTo(-14.1,69.2).curveTo(-23.2,58.3,-23.2,42).curveTo(-23.2,30.1,-17.8,15.1).curveTo(-10,-6.1,-8.1,-14.5).curveTo(-7,-19.2,-5.7,-28.4).curveTo(-4.3,-38.3,-3.5,-42.3).curveTo(-0.7,-55.8,7.4,-58.7).curveTo(6.1,-48.2,5.7,-41.7).curveTo(5.1,-34.1,5.3,-27.7).curveTo(5.6,-18.8,2.7,-6.7).curveTo(-0.4,5.8,-0.4,10.9).curveTo(-0.4,37.4,8.4,62.2).curveTo(15.1,81.4,23.2,86.7).curveTo(-2.2,83.3,-14.1,69.2).closePath().moveTo(5.3,-64.9).curveTo(4.9,-67,-1.5,-75.4).curveTo(-9.1,-85.2,-9.4,-86).lineTo(-2.7,-86).curveTo(2.2,-86.1,5.4,-86.7).curveTo(6.4,-76.9,11.8,-71.6).curveTo(15.1,-68.2,20.5,-66.5).curveTo(13.6,-64.6,8.8,-64.6).curveTo(6.9,-64.6,5.3,-64.9).closePath();
	this.shape.setTransform(28.1,104.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#9B7E44").beginStroke().moveTo(-23.3,92.4).curveTo(-31.4,87,-38.1,67.8).curveTo(-46.9,43.1,-46.9,16.6).curveTo(-46.9,11.5,-43.8,-1.1).curveTo(-40.8,-13.1,-41.2,-22).curveTo(-41.4,-28.5,-40.8,-36).curveTo(-40.4,-42.5,-39.1,-53).curveTo(-38.1,-53.4,-37,-53.6).curveTo(-27.2,-55.3,-13.9,-59.9).curveTo(-0.6,-64.4,-0.3,-65.3).curveTo(-0.1,-66.1,5.2,-63.6).curveTo(8.9,-61.9,13.1,-56.3).curveTo(15.3,-53.2,18.3,-48.7).curveTo(27.3,-38.3,31.3,-32.7).curveTo(37.8,-23.3,38.6,-16.9).curveTo(41.2,4.7,44.9,20.2).curveTo(46.8,28.2,46.8,46.6).lineTo(46.9,58).curveTo(46.5,63.1,43.7,70.4).curveTo(43.2,71.8,41.9,76.8).curveTo(40.7,81,39.6,83.3).curveTo(36.1,90.6,27.2,92.4).lineTo(2,92.8).curveTo(-0.1,93.5,-5.6,93.5).curveTo(-15.1,93.5,-23.3,92.4).closePath().moveTo(-34.7,-65.9).curveTo(-40.1,-71.2,-41.1,-81).curveTo(-39.5,-81.4,-38.3,-81.8).curveTo(-37.1,-82.3,-33.7,-84.7).curveTo(-27.9,-88.7,-8.6,-88.8).curveTo(-7.5,-89,-7,-89.8).lineTo(-6.2,-91.4).curveTo(-4.9,-93.5,1,-93.5).curveTo(2,-90.7,2,-84.9).curveTo(2,-80.3,0.8,-76.2).lineTo(0.1,-73.6).curveTo(-0.1,-70.1,-16.7,-63.8).curveTo(-21.8,-62,-26,-60.8).curveTo(-31.3,-62.5,-34.7,-65.9).closePath();
	this.shape_1.setTransform(74.6,98.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-53.8,76.7).curveTo(-63.2,64.9,-63.1,47.6).curveTo(-63.1,33.7,-57.1,18).curveTo(-49.8,-1.2,-47.7,-16.2).curveTo(-46.6,-26.6,-45.9,-31.8).curveTo(-44.5,-40.8,-40.8,-47.1).curveTo(-38,-52,-33.2,-56.4).curveTo(-39.6,-64,-43,-68.1).curveTo(-49.4,-75.8,-49.4,-81).curveTo(-49.4,-83.1,-49.2,-83.7).curveTo(-48.7,-85.5,-46.9,-85.5).curveTo(-33.5,-86,-27,-87.3).curveTo(-24.9,-87.9,-22.9,-89.5).curveTo(-20.4,-91.6,-19.3,-92.2).curveTo(-13.6,-95.2,1.5,-94).curveTo(14,-102.3,17.3,-95.4).curveTo(18.4,-93.2,18.5,-89.3).curveTo(18.5,-87.2,18.4,-84.7).curveTo(18.4,-80.8,17.5,-75.5).curveTo(17,-72.8,16.5,-71).curveTo(19.3,-69.1,22.5,-66.4).curveTo(32.3,-57.8,36.5,-48.8).curveTo(44.7,-41.5,49.7,-33).curveTo(54.8,-24.4,55.4,-17).curveTo(57,6.5,60.5,19.8).curveTo(63.1,30,63.1,46.8).curveTo(63.1,65.3,57.4,80).curveTo(50.2,98.1,37.8,97.8).lineTo(14.6,98.2).curveTo(11.7,98.6,9.8,98.6).curveTo(-36.3,98.6,-53.8,76.7).closePath().moveTo(11.1,-65.2).curveTo(10.8,-64.4,-2.5,-59.8).curveTo(-15.7,-55.2,-25.5,-53.5).curveTo(-26.7,-53.3,-27.7,-52.9).curveTo(-35.7,-50.1,-38.6,-36.5).curveTo(-39.4,-32.5,-40.7,-22.7).curveTo(-42,-13.4,-43.2,-8.7).curveTo(-45,-0.3,-52.8,20.9).curveTo(-58.3,35.8,-58.3,47.8).curveTo(-58.3,64.1,-49.1,75).curveTo(-37.3,89.1,-11.9,92.5).curveTo(-3.7,93.6,5.9,93.6).curveTo(11.4,93.6,13.4,92.9).lineTo(38.6,92.5).curveTo(47.6,90.6,51.1,83.4).curveTo(52.2,81.1,53.3,76.9).curveTo(54.6,71.9,55.1,70.5).curveTo(57.9,63.2,58.3,58.1).lineTo(58.3,46.7).curveTo(58.2,28.3,56.4,20.3).curveTo(52.7,4.7,50.1,-16.8).curveTo(49.2,-23.2,42.7,-32.6).curveTo(38.7,-38.3,29.7,-48.6).curveTo(26.7,-53.2,24.5,-56.2).curveTo(20.3,-61.8,16.7,-63.5).curveTo(12.7,-65.4,11.5,-65.4).curveTo(11.4,-65.4,11.4,-65.4).curveTo(11.3,-65.4,11.2,-65.4).curveTo(11.2,-65.4,11.2,-65.3).curveTo(11.1,-65.3,11.1,-65.2).closePath().moveTo(-36.5,-69.6).curveTo(-30.1,-61.2,-29.8,-59.1).curveTo(-24.3,-58,-14.6,-60.7).curveTo(-10.4,-61.9,-5.3,-63.8).curveTo(11.3,-70.1,11.5,-73.5).lineTo(12.2,-76.1).curveTo(13.4,-80.2,13.4,-84.8).curveTo(13.5,-90.6,12.4,-93.4).curveTo(6.5,-93.4,5.2,-91.3).lineTo(4.4,-89.8).curveTo(3.9,-88.9,2.8,-88.7).curveTo(-16.5,-88.6,-22.3,-84.6).curveTo(-25.7,-82.2,-26.9,-81.8).curveTo(-28.1,-81.3,-29.7,-81).curveTo(-32.8,-80.4,-37.8,-80.2).lineTo(-44.4,-80.2).curveTo(-44.1,-79.5,-36.5,-69.6).closePath();
	this.shape_2.setTransform(63.2,98.6);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,126.3,197.2);


(lib.mailSack01 = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("MAIL", "44px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 46;
	this.text.setTransform(90.4,96.3,0.73,0.73,0,6,6.5);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#896F3C").beginStroke().moveTo(-30.4,78.4).curveTo(-37.4,67.1,-41.6,53.7).curveTo(-44.6,44,-44.7,40.4).curveTo(-44.7,29.4,-37.7,14.9).curveTo(-33.6,6.8,-31.6,2.3).curveTo(-27.9,-5.5,-26,-12.7).curveTo(-22.3,-25.4,-14.1,-39.2).curveTo(-5.6,-53.1,5.4,-64.7).curveTo(13.5,-62.8,21.8,-62.2).curveTo(17.9,-54.5,14.2,-41.8).curveTo(10,-27.7,6,-21.4).curveTo(2.7,-16.2,-5.3,-4.4).curveTo(-11.6,5.5,-11.9,8.7).curveTo(-12,9.4,-15.4,20.8).curveTo(-18.5,31.1,-18.5,42.1).curveTo(-18.5,59.6,2.4,87).curveTo(10.1,97.2,18.2,105.3).curveTo(23.8,110.9,26.9,112.9).curveTo(-9.8,111.4,-30.4,78.4).closePath().moveTo(3.8,-70.3).curveTo(-0.9,-74.9,-4.4,-85.2).lineTo(-8.4,-96.4).curveTo(-8.9,-97.7,-8.9,-104.2).curveTo(-8.8,-105.7,-8.3,-108.3).curveTo(-7.6,-111.3,-6.7,-112.7).lineTo(-0.1,-112.9).lineTo(0.7,-111.7).curveTo(3.9,-87.2,14.4,-77.7).curveTo(23.8,-69.2,44.7,-68).curveTo(38.7,-67.3,31.8,-66.8).curveTo(27.1,-66.5,23.1,-66.5).curveTo(8.5,-66.5,3.8,-70.3).closePath();
	this.shape.setTransform(49.6,119.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#000000").beginStroke().moveTo(41.6,117.9).lineTo(32.7,116.3).lineTo(12.3,116.5).curveTo(7.9,117.5,4.8,118.5).lineTo(-15.4,118.5).curveTo(-49.8,111.2,-69.9,82.9).curveTo(-77.6,72,-81.9,59.7).curveTo(-85.6,49.1,-85.5,40.9).curveTo(-85.6,27.4,-79.2,12.7).curveTo(-70.6,-5.2,-65.8,-17.7).curveTo(-62.8,-27.5,-60.9,-32.4).curveTo(-57.6,-41,-52,-47.1).curveTo(-49.8,-49.4,-46.7,-54.4).curveTo(-43.8,-59.3,-41.4,-61.7).curveTo(-39.2,-63.7,-38.8,-66.2).curveTo(-38.5,-67.7,-37.7,-68.5).curveTo(-42.1,-73.3,-45.4,-84.2).curveTo(-47.4,-90.5,-48.7,-97.8).curveTo(-49.8,-103.3,-49.7,-104.5).curveTo(-49.8,-124.3,-35.8,-117.6).curveTo(-32.1,-115.9,-25.3,-110.9).curveTo(-19.5,-106.7,-18.4,-106.7).curveTo(-16.7,-106.7,-13.1,-108.1).curveTo(-9.5,-109.6,-6.2,-109.6).curveTo(-1.2,-109.6,3.9,-106.5).curveTo(9,-103.5,14.6,-103.5).curveTo(18.8,-103.5,22.3,-105.3).curveTo(25.9,-107.1,34.1,-107.1).curveTo(44.4,-107.1,47.6,-99.4).curveTo(49.3,-95.5,48.8,-91.5).curveTo(48.8,-85.2,41.2,-76.8).curveTo(37.8,-73.1,35.2,-71.1).curveTo(47.2,-66.5,53.4,-58.9).curveTo(56.2,-55.6,59,-50.2).lineTo(63.9,-40.7).curveTo(67.5,-34.1,68.5,-23.9).lineTo(69.3,-7.1).curveTo(69.8,8,72.5,17.7).curveTo(74.3,24,78.1,30.7).curveTo(82.1,37.8,83.3,41.4).curveTo(85.5,48.5,85.5,59.4).curveTo(85.5,62.7,84.7,69.5).curveTo(83.4,80.7,80.5,90).curveTo(71.4,119.3,51.3,119.3).curveTo(50,119.3,41.6,117.9).closePath().moveTo(37.7,112.8).curveTo(42.4,114.3,47.5,114.3).curveTo(49.4,114.3,54.6,114).curveTo(70,105.2,76.4,87.9).curveTo(80.7,76.1,80.7,59.7).curveTo(80.6,49.5,78.4,42.4).curveTo(77.2,38.6,73.4,31.1).curveTo(69.7,24,68,17.9).curveTo(65.3,8.6,64.8,-5.1).curveTo(65.1,-15.6,64.8,-21.2).curveTo(64.2,-31.1,59.4,-37.5).curveTo(57.7,-39.9,54.6,-45.4).curveTo(51.3,-51,49.4,-53.8).curveTo(42.2,-63.5,30.4,-67.6).curveTo(7.2,-60.4,-14.2,-61.9).curveTo(-22.5,-62.5,-30.6,-64.4).curveTo(-41.6,-52.8,-50.1,-38.9).curveTo(-58.3,-25.1,-62,-12.4).curveTo(-63.9,-5.2,-67.5,2.6).curveTo(-69.6,7.1,-73.7,15.2).curveTo(-80.7,29.7,-80.7,40.7).curveTo(-80.6,44.3,-77.6,54).curveTo(-73.4,67.4,-66.4,78.7).curveTo(-45.8,111.7,-9.1,113.2).lineTo(-5.7,113.2).lineTo(8.3,112.2).lineTo(22.4,111.2).curveTo(33.1,111.2,37.7,112.8).closePath().moveTo(-42.7,-112.4).curveTo(-43.6,-111,-44.3,-108).curveTo(-44.8,-105.4,-44.9,-103.9).curveTo(-44.9,-97.4,-44.4,-96.1).lineTo(-40.4,-84.9).curveTo(-36.9,-74.6,-32.2,-70).curveTo(-26.2,-65.2,-4.2,-66.5).curveTo(2.7,-67,8.7,-67.7).curveTo(21.5,-69.4,29.7,-72.9).curveTo(32.9,-77,38.6,-81.4).curveTo(41.3,-83.5,42.4,-85.3).curveTo(43.9,-87.6,44,-91.3).curveTo(43.9,-95,43.3,-96.4).curveTo(42.3,-98.4,38,-102.1).curveTo(25.1,-102,21.9,-100.1).curveTo(20.7,-99.1,19.8,-98.8).curveTo(18.2,-98.2,14.6,-98.2).curveTo(10.5,-98.2,3.6,-101.1).curveTo(-1.7,-103.3,-3.3,-104.5).curveTo(-11.5,-103.6,-13.4,-102.4).curveTo(-14.8,-101.4,-18,-101.4).curveTo(-25.6,-101.4,-30.7,-105.8).curveTo(-32.7,-107.6,-35.3,-111.4).lineTo(-36,-112.6).closePath();
	this.shape_1.setTransform(85.6,119.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#9B7E44").beginStroke().moveTo(24.7,111.3).curveTo(20,109.8,9.3,109.8).lineTo(-4.8,110.8).lineTo(-18.8,111.8).lineTo(-22.2,111.7).curveTo(-25.3,109.7,-30.8,104.2).curveTo(-38.9,96.1,-46.7,85.8).curveTo(-67.6,58.4,-67.6,41).curveTo(-67.6,30,-64.5,19.7).curveTo(-61,8.3,-61,7.6).curveTo(-60.7,4.4,-54.3,-5.5).curveTo(-46.4,-17.4,-43.1,-22.5).curveTo(-39.1,-28.8,-34.9,-42.9).curveTo(-31.2,-55.6,-27.3,-63.3).curveTo(-5.9,-61.8,17.3,-69).curveTo(29.1,-65,36.3,-55.2).curveTo(38.3,-52.4,41.5,-46.8).curveTo(44.6,-41.3,46.4,-38.9).curveTo(51.1,-32.5,51.7,-22.6).curveTo(52,-17,51.7,-6.5).curveTo(52.2,7.1,54.9,16.5).curveTo(56.6,22.6,60.3,29.7).curveTo(64.1,37.2,65.3,40.9).curveTo(67.6,48.1,67.6,58.3).curveTo(67.6,74.7,63.3,86.5).curveTo(57,103.7,41.5,112.6).curveTo(36.4,112.9,34.4,112.9).curveTo(29.4,112.9,24.7,111.3).closePath().moveTo(-34.7,-78.9).curveTo(-45.2,-88.3,-48.4,-112.9).curveTo(-45.8,-109,-43.8,-107.2).curveTo(-38.7,-102.8,-31,-102.8).curveTo(-27.8,-102.8,-26.4,-103.8).curveTo(-24.6,-105.1,-16.3,-105.9).curveTo(-14.8,-104.8,-9.5,-102.5).curveTo(-2.5,-99.6,1.6,-99.6).curveTo(5.2,-99.6,6.7,-100.2).curveTo(7.6,-100.6,8.9,-101.5).curveTo(12,-103.5,25,-103.5).curveTo(29.2,-99.9,30.2,-97.8).curveTo(30.9,-96.4,30.9,-92.7).curveTo(30.9,-89.1,29.4,-86.7).curveTo(28.3,-85,25.5,-82.8).curveTo(19.9,-78.5,16.6,-74.3).curveTo(8.5,-70.9,-4.4,-69.2).curveTo(-25.2,-70.3,-34.7,-78.9).closePath();
	this.shape_2.setTransform(98.6,120.8);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,171.1,238.7);


(lib.LucyHead = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#CC0066").beginStroke().moveTo(-4,1.2).lineTo(-9.7,-1.9).curveTo(-2.9,-3.2,9.7,-0.2).lineTo(6.9,1.2).curveTo(4,2.3,1.3,2.3).curveTo(-1.4,2.3,-4,1.2).closePath();
	this.shape.setTransform(89.7,104.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#DDBAA4").beginStroke().moveTo(14.4,47.8).curveTo(3.7,43.3,-8.8,37.2).lineTo(-7.7,34.6).curveTo(-5.8,30,-5.8,27.9).curveTo(-5.8,27.6,-8.2,25.4).curveTo(-11.1,26.9,-11.7,30.5).curveTo(-12.4,34.1,-13.7,34.9).curveTo(-33.4,22.4,-33.4,19).curveTo(-33.4,17.7,-31.8,15.2).curveTo(-30.3,12.8,-30.3,12).curveTo(-30.3,10.9,-30.6,10.5).lineTo(-30.9,10).curveTo(-33.1,10.5,-34.6,11.2).curveTo(-35,8.8,-35,4.3).curveTo(-35,-4.2,-31.4,-14.8).curveTo(-27.4,-26.9,-20,-36.8).curveTo(-11.8,-48,-1.1,-54.4).curveTo(3.1,-45.7,6.7,-37.7).curveTo(16.1,-16.5,18.1,-4.7).curveTo(10.2,-2.9,10.2,4.8).curveTo(10.2,6,10.5,6.3).lineTo(10.8,6.6).curveTo(12.3,6.8,14.3,6.6).lineTo(18.3,5.8).lineTo(19.4,5.6).curveTo(21.1,18,24.2,28).curveTo(27.6,39.2,35,54.3).curveTo(25.2,52.2,14.4,47.8).closePath();
	this.shape_1.setTransform(38.2,62.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(22.3,33.5).curveTo(0.5,33.5,-22,22.5).curveTo(-30,18.6,-35.8,14.4).curveTo(-38.6,12.4,-39.9,11.1).curveTo(-41,12.2,-42.1,12.2).curveTo(-50.1,12.2,-59.5,2.5).curveTo(-63,-1,-65.5,-4.7).curveTo(-67.8,-8,-67.8,-9.1).curveTo(-67.8,-13.6,-67.4,-14.4).curveTo(-66.7,-15.8,-64.2,-16.9).curveTo(-62.7,-17.6,-60.5,-18.2).lineTo(-60.2,-17.6).curveTo(-59.9,-17.2,-59.9,-16.1).curveTo(-59.9,-15.3,-61.5,-12.9).curveTo(-63,-10.5,-63,-9.1).curveTo(-63,-5.7,-43.3,6.7).curveTo(-42,6,-41.4,2.4).curveTo(-40.7,-1.3,-37.8,-2.8).curveTo(-35.4,-0.6,-35.4,-0.3).curveTo(-35.4,1.9,-37.3,6.5).lineTo(-38.4,9.1).curveTo(-25.9,15.2,-15.2,19.6).curveTo(-4.4,24,5.4,26.2).curveTo(14.4,28.3,22.6,28.4).curveTo(33.6,28.7,42.8,20.5).curveTo(45.1,18.4,49.6,13.5).curveTo(53.6,9.2,55.8,7.5).curveTo(56.3,7.2,56.7,7.1).curveTo(57.1,7,57.5,7.2).curveTo(57.8,7.5,58,7.8).lineTo(58.3,8.4).curveTo(58.6,8.8,59.3,8.2).curveTo(60,7.6,56.5,13.1).curveTo(52.9,18.6,43.9,26.1).curveTo(35,33.5,22.3,33.5).lineTo(22.3,33.5).closePath().moveTo(9.2,16.9).curveTo(3,12.8,3,10.9).curveTo(3,6.2,14.1,6.5).curveTo(18.2,6.6,38.9,9).curveTo(41.3,9.3,41.3,11.8).curveTo(41.3,13.7,36.5,17.3).curveTo(30.6,21.7,23.5,21.7).curveTo(16.4,21.7,9.2,16.9).closePath().moveTo(12.1,11.9).lineTo(17.9,15).curveTo(23,17.2,28.8,15).lineTo(31.6,13.6).curveTo(22.7,11.5,16.7,11.5).curveTo(14.2,11.5,12.1,11.9).closePath().moveTo(21.5,1.9).curveTo(15.5,1,13.6,-1.2).curveTo(12.8,-2.2,12.8,-4.5).curveTo(12.8,-9.6,16.5,-7.2).curveTo(20.8,-3.5,21.9,-3.2).curveTo(28,-1.6,34.1,-3.2).lineTo(34.1,-5).lineTo(34.9,-5.5).curveTo(35.7,-6,36.9,-6).curveTo(38.1,-6,38.7,-5.8).lineTo(39.1,-5.6).lineTo(39.4,-5).curveTo(39.7,-4.6,39.7,-3.5).curveTo(39.7,-2.1,38.1,-0.4).lineTo(35.7,1.9).curveTo(32.4,2.5,28.9,2.5).curveTo(25.3,2.5,21.5,1.9).closePath().moveTo(52.7,-18.4).curveTo(51.3,-18.5,49.4,-19.5).curveTo(47.3,-20.6,45.8,-22.2).curveTo(44.4,-23.7,43.5,-24.2).curveTo(41.8,-24.9,39,-24.4).curveTo(37.7,-24.2,36.7,-23.7).curveTo(36,-24,35.5,-24.3).lineTo(35.3,-24.8).lineTo(35,-25.2).lineTo(34.9,-25.5).lineTo(35.1,-26).curveTo(37.8,-30.2,48.3,-30).curveTo(57.9,-29.8,63.6,-26.7).curveTo(65.6,-25.6,66.9,-23.9).curveTo(67.9,-22.5,67.8,-21.6).lineTo(67.8,-21).lineTo(67.7,-20.9).lineTo(67.7,-20.9).curveTo(66.6,-21.5,64.9,-21.8).lineTo(63.5,-21.9).lineTo(62.8,-21.9).curveTo(61.8,-21.9,61.2,-21.6).lineTo(59.7,-21).curveTo(59.2,-20.8,57.7,-19.8).curveTo(56.4,-18.9,55.5,-18.7).curveTo(54.3,-18.4,53.4,-18.4).lineTo(52.7,-18.4).closePath().moveTo(-6.9,-22.4).curveTo(-7.7,-22.8,-10.2,-22.5).lineTo(-11.3,-22.3).lineTo(-15.3,-21.5).curveTo(-17.3,-21.3,-18.8,-21.5).lineTo(-19.1,-21.9).curveTo(-19.4,-22.1,-19.4,-23.3).curveTo(-19.5,-31,-11.5,-32.9).curveTo(-8.2,-33.7,-3.4,-33.5).curveTo(7.9,-32.9,23.1,-27.2).curveTo(24.5,-26.7,22.9,-25.7).curveTo(21.4,-24.7,19,-24.5).lineTo(16.3,-24.4).lineTo(13.1,-24.5).curveTo(10.2,-21.1,5.5,-20.6).curveTo(3.8,-20.4,2.3,-20.4).curveTo(-2.9,-20.4,-6.9,-22.4).closePath().moveTo(6.9,-25.6).lineTo(7.3,-25.7).lineTo(6.9,-25.7).lineTo(6.9,-25.6).closePath();
	this.shape_2.setTransform(67.8,90.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-7.8,37.2).curveTo(-5,33.6,-4.1,31.7).lineTo(-2.4,28.4).curveTo(0.1,22.4,1.7,16).curveTo(2.7,12,3,6.4).curveTo(3.1,3.8,3.1,-3.4).curveTo(3,-9.5,3.1,-12.4).curveTo(3.2,-13.4,1.1,-24.2).curveTo(-1,-35.1,-1,-36.3).curveTo(-1,-38.2,-0.2,-38.4).curveTo(0.3,-38.5,0.6,-37.7).curveTo(0.9,-38.1,1.6,-37).curveTo(2.2,-36.1,4.1,-26.2).curveTo(5.8,-17.5,6.4,-13.4).lineTo(7.1,-8.1).curveTo(7.9,-2.1,7.7,1.6).curveTo(7.4,12.2,5.1,19.7).curveTo(3.7,24.8,2.3,28.2).curveTo(0.3,33,-2.8,37.9).curveTo(-3.1,38.4,-3.9,38.4).curveTo(-5.2,38.4,-7.8,37.2).closePath();
	this.shape_3.setTransform(128.5,63.4);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#EACFB7").beginStroke().moveTo(-36.7,25.5).curveTo(-43.9,8,-46.6,-10.5).curveTo(-46.6,-24.5,-41.5,-10.8).curveTo(-39.3,-4.9,-33.4,15.8).curveTo(-31.9,18.9,-29.6,26).curveTo(-27.2,33.1,-25.6,36.6).curveTo(-24.1,39.7,-22.4,42.3).lineTo(-24.2,31.3).curveTo(-25.6,22.5,-28.6,15.8).curveTo(-32.5,6.9,-36.6,-6.6).curveTo(-40.4,-19.4,-40.4,-22.7).curveTo(-40.4,-23.2,-40.2,-23.6).curveTo(-41.5,-23.9,-42.1,-24.3).curveTo(-43.6,-25.3,-43.6,-28).curveTo(-43.6,-28.8,-42.5,-29.3).lineTo(-42.6,-30).curveTo(-42.6,-30.9,-42.1,-31.7).lineTo(-47.6,-32).lineTo(-50.1,-34.4).curveTo(-50.6,-35,-50.7,-34.9).curveTo(-52.8,-44.4,-44.4,-46.5).curveTo(-36,-48.6,-33.3,-47.9).curveTo(-30.6,-47.3,-25.6,-56).curveTo(-20.6,-64.8,-11.7,-56.5).curveTo(-2.7,-48.3,10.7,-46.7).curveTo(19.5,-45.7,24.7,-43.8).curveTo(29.5,-42,37.2,-39.9).curveTo(42.7,-38.1,43,-36.2).curveTo(45.8,-33.8,48.7,-16.5).curveTo(50.2,-7.9,51.1,0.5).curveTo(51.1,2.8,50.1,9).curveTo(49.1,15.2,49.1,17.3).curveTo(49.1,20.2,46.7,26.4).curveTo(43.8,34.3,38.9,41).curveTo(24.6,60.5,1.2,60.5).curveTo(-22.2,60.5,-36.7,25.5).closePath().moveTo(9.2,54).lineTo(10.7,54).lineTo(12,53.7).lineTo(9.2,54).closePath();
	this.shape_4.setTransform(84.2,60.5);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,136.3,124.1);


(lib.LucyHair = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#E5C45A").beginStroke().moveTo(4.6,54.9).curveTo(1.6,52,-1,50.1).curveTo(-17.5,37.5,-24.7,23.5).curveTo(-31.3,10.6,-31.3,-6).curveTo(-31.3,-30,-19.2,-46.7).curveTo(-12.8,-55.5,-3.3,-61.6).curveTo(-0,-51.9,4.5,-41.2).curveTo(12.4,-22.9,21.7,-7.7).curveTo(27.5,2,31.3,6.3).curveTo(24.4,7,19.7,9.1).curveTo(18.3,10,17.5,11).curveTo(17,11.6,16.2,13.6).lineTo(16.2,13.7).curveTo(14,17.6,12.2,24.3).curveTo(10.4,31,10.4,35.2).lineTo(10.9,43.6).curveTo(11.8,53.6,14.3,61.6).curveTo(7.9,57.7,4.6,54.9).closePath();
	this.shape.setTransform(36.2,111);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#FFDA64").beginStroke().moveTo(49.5,48.5).curveTo(24.8,40.5,6,36.3).curveTo(-16.6,31.2,-28.9,31.9).lineTo(-32.3,32.2).curveTo(-36,27.9,-41.9,18.2).curveTo(-51.2,2.9,-59,-15.4).curveTo(-63.6,-26,-66.9,-35.7).curveTo(-64.9,-37,-62.8,-38.1).curveTo(-60.8,-39.3,-58.6,-40.3).curveTo(-47.9,-45.1,-37.2,-46.9).curveTo(-9.2,-51.5,11.6,-46.1).curveTo(26.1,-42.3,35.9,-34).curveTo(45.7,-25.7,52.2,-8.7).curveTo(55.4,-0.3,59.8,17.9).curveTo(60.3,19.7,61.4,29.1).curveTo(63.1,38.6,66.9,43.1).curveTo(63.1,46.3,58.4,47.9).curveTo(55.2,49,52.7,49).curveTo(50.9,49,49.5,48.5).closePath();
	this.shape_1.setTransform(99.8,85.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(39.5,54.5).curveTo(35.3,54.3,31.1,53.7).curveTo(29.3,53.4,22.9,51.5).curveTo(14.7,49.1,6.3,47.2).curveTo(-2.4,45.2,-12.1,42.7).lineTo(-21.2,40.3).curveTo(-29.9,38.7,-39.6,38.6).curveTo(-48.8,38.5,-51.4,39.7).curveTo(-51.9,40,-53.9,40.6).curveTo(-55.8,41.6,-56.3,43.5).lineTo(-56.4,43.5).curveTo(-59.6,46.3,-60.5,46.3).lineTo(-61,46.3).lineTo(-60.2,44.6).curveTo(-59.4,42.2,-60.6,40.1).lineTo(-60.6,40).curveTo(-59.8,38.1,-59.3,37.4).curveTo(-58.5,36.4,-57.1,35.5).curveTo(-52.4,33.5,-45.5,32.8).lineTo(-42.1,32.5).curveTo(-29.8,31.7,-7.3,36.8).curveTo(11.6,41.1,36.2,49.1).curveTo(39.9,50.3,45.1,48.5).curveTo(49.9,46.9,53.7,43.6).curveTo(49.9,39.1,48.1,29.7).curveTo(47,20.3,46.6,18.5).curveTo(42.2,0.2,38.9,-8.2).curveTo(32.5,-25.1,22.7,-33.4).curveTo(12.9,-41.7,-1.6,-45.5).curveTo(-22.4,-51,-50.4,-46.4).curveTo(-50.9,-46.3,-51,-46.6).lineTo(-50.9,-48.7).curveTo(-50.9,-50.7,-48.6,-52.1).curveTo(-46.1,-53.5,-40.9,-54.2).curveTo(-31.8,-55.2,-17.9,-53.5).curveTo(-4.1,-51.8,7.5,-48.1).curveTo(20.1,-44.2,24.7,-39.6).curveTo(32.2,-32.1,35.9,-27.3).curveTo(40.3,-21.5,43.4,-14.5).curveTo(45.7,-9.3,47.8,-0).curveTo(49,5.1,50.9,14.2).curveTo(53.9,27.5,57.8,35.7).curveTo(61,42.3,61,42.7).curveTo(61,44.3,57.5,47.9).curveTo(52.8,53,45.7,54.3).curveTo(44.3,54.5,41.8,54.5).lineTo(39.5,54.5).closePath();
	this.shape_2.setTransform(113,84.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#020100").beginStroke().moveTo(24.2,74.6).curveTo(16.9,74.6,1.1,60.8).curveTo(-11.5,49.9,-16.2,43.4).curveTo(-28.3,26.8,-30.6,19.6).curveTo(-31.8,15.7,-32.1,10.6).curveTo(-32.2,8.9,-32.2,-1.5).curveTo(-32.2,-25.5,-21.1,-43.2).curveTo(-11.5,-56.7,-1.2,-63.3).lineTo(1,-64.7).curveTo(3.2,-66,5.6,-67.1).curveTo(11.4,-69.8,17.4,-71.7).curveTo(25.5,-74.3,32.1,-74.6).lineTo(32.2,-74.6).curveTo(29.8,-73.3,29.8,-71.2).lineTo(29.8,-69.1).curveTo(29.8,-68.8,30.3,-68.9).curveTo(19.6,-67.1,8.9,-62.2).curveTo(6.7,-61.2,4.7,-60.1).curveTo(2.6,-58.9,0.6,-57.7).curveTo(-8.8,-51.6,-15.3,-42.7).curveTo(-27.4,-26,-27.4,-2.1).curveTo(-27.4,14.6,-20.8,27.4).curveTo(-13.6,41.4,3,54).curveTo(5.5,56,8.6,58.8).curveTo(11.9,61.6,18.2,65.5).curveTo(15.8,57.5,14.8,47.6).lineTo(14.3,39.1).curveTo(14.3,34.9,16.1,28.2).curveTo(17.9,21.5,20.1,17.6).curveTo(21.3,19.7,20.6,22.1).lineTo(19.8,23.8).lineTo(20.2,23.8).curveTo(21.1,23.8,24.3,21).lineTo(24.4,21).lineTo(22.1,29.5).curveTo(21.2,33.4,21,38.7).curveTo(20.9,43.4,21.3,45.8).curveTo(21.8,49.3,24.2,57.7).curveTo(25,60.5,25.7,65).curveTo(26.3,69.2,27.1,71.7).lineTo(27.3,72.3).lineTo(27.3,72.5).curveTo(27.3,74.4,26.9,73.8).curveTo(26.9,73.8,26.9,73.8).curveTo(26.9,73.9,26.8,73.9).curveTo(26.8,74,26.8,74).curveTo(26.7,74.1,26.7,74.2).curveTo(26.3,74.6,25,74.6).closePath();
	this.shape_3.setTransform(32.3,107.1);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.1,30,173.9,151.7);


(lib.LucyClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#4E2A3F").setStrokeStyle(1,1,1).moveTo(31.2,6.8).curveTo(31,6.6,30.9,6.4).curveTo(25,-3.1,15.4,-5.9).curveTo(12.3,-6.8,5.6,-6.8).curveTo(-5.5,-6.8,-16.6,-4.1).curveTo(-24.8,-2.1,-29.1,0.2).curveTo(-30.3,0.9,-31.2,1.5);
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#45635D").beginStroke().moveTo(0.6,73.5).curveTo(-4.7,73.1,-5.4,72.8).lineTo(-4.5,70.4).curveTo(-4,68.9,-2.4,68).lineTo(-1.1,67.4).lineTo(-0.4,67.2).lineTo(1.2,66.8).lineTo(1.1,67.5).curveTo(1.1,70.8,11,73.2).lineTo(13.6,73.8).curveTo(10.7,74,7.2,74).curveTo(5.5,74,0.6,73.5).closePath().moveTo(30.6,70.9).curveTo(30.6,69.5,30.3,68.3).curveTo(30.9,68.3,31.4,67.6).curveTo(32.2,66.6,32.7,66.2).curveTo(33.6,65.4,35.2,65).curveTo(36.2,69.5,37.5,71).curveTo(38.4,72,40.3,72.8).lineTo(33.1,73.4).curveTo(31.1,73.5,30.5,73.6).curveTo(30.6,72.6,30.6,70.9).closePath().moveTo(17.7,-10.4).curveTo(10.2,-11.4,-2.7,-12.8).curveTo(-12.8,-14,-21.2,-15.7).curveTo(-20.7,-16.6,-20,-18.4).curveTo(-18.7,-22.3,-17.8,-28.1).lineTo(-16.4,-36.3).curveTo(-15.9,-38.9,-15.9,-42).lineTo(-15.9,-43).lineTo(-15.9,-43.1).lineTo(-16,-44).lineTo(-16.1,-44.7).curveTo(-15.4,-45,-14.7,-45.1).curveTo(-14.2,-45.1,-13.3,-43.9).curveTo(-12.6,-42.8,-12.1,-41.7).curveTo(-11.8,-41,-11,-36.3).curveTo(-9.6,-30.9,-5.7,-26.5).curveTo(-1.7,-22.1,3.8,-19.9).curveTo(7,-18.7,13.2,-17.5).curveTo(18.8,-16.5,20.9,-15.5).curveTo(24.2,-13.7,25,-9.7).curveTo(20.8,-10,17.7,-10.4).closePath().moveTo(-32,-51.4).lineTo(-37.6,-53.4).curveTo(-40.2,-54.3,-40.3,-55.2).curveTo(-40.7,-57.9,-36.8,-62.3).curveTo(-33.8,-65.8,-30.2,-68.3).curveTo(-25.7,-71.5,-18.3,-74).curveTo(-20.4,-72.4,-23.8,-67.7).curveTo(-27.7,-62.6,-29.7,-58).curveTo(-30.4,-56.5,-30.9,-53.3).curveTo(-31.1,-51.4,-31.9,-51.4).lineTo(-32,-51.4).closePath();
	this.shape_1.setTransform(55.5,89.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#648C83").beginStroke().moveTo(-12.1,80.9).curveTo(-21.9,78.4,-21.9,75.2).lineTo(-21.9,74.5).curveTo(-16,73.2,-2,73.3).curveTo(-0,73.5,1.6,75).curveTo(3.1,76.5,3.1,78.3).lineTo(2.6,78.8).curveTo(1.8,79.4,0.2,80).curveTo(-3.2,81.1,-9.5,81.5).lineTo(-12.1,80.9).closePath().moveTo(14.5,78.6).curveTo(13.2,77.1,12.1,72.6).curveTo(15,71.7,20,71.8).curveTo(27.2,71.8,31.3,73.6).curveTo(34.8,75.2,35.7,77.9).curveTo(36,78.6,32.7,79.1).curveTo(27.3,79.7,17.3,80.5).curveTo(15.3,79.6,14.5,78.6).closePath().moveTo(1.9,-2.1).curveTo(1.1,-6,-2.2,-7.8).curveTo(-4.3,-8.9,-9.8,-9.9).curveTo(-16,-11,-19.2,-12.3).curveTo(-24.8,-14.5,-28.7,-18.9).curveTo(-32.6,-23.3,-34.1,-28.6).curveTo(-34.9,-33.4,-35.2,-34).curveTo(-35.6,-35.2,-36.4,-36.2).curveTo(-37.3,-37.5,-37.8,-37.4).curveTo(-38.5,-37.3,-39.1,-37.1).curveTo(-39.4,-38.9,-40.1,-39.4).curveTo(-41.1,-40.1,-42.7,-38).lineTo(-43.6,-36.6).lineTo(-45.2,-36.3).lineTo(-45.3,-36.6).lineTo(-49.4,-40.6).curveTo(-50.5,-41.7,-51.5,-42.3).lineTo(-55,-43.7).curveTo(-54.2,-43.6,-53.9,-45.7).curveTo(-53.4,-48.9,-52.8,-50.3).curveTo(-50.7,-54.9,-46.9,-60.1).curveTo(-43.4,-64.8,-41.3,-66.3).lineTo(-29,-70.5).curveTo(-23.3,-72.5,-22.5,-73.1).curveTo(-22.2,-73.3,-22.3,-73.8).lineTo(-22.6,-74.4).curveTo(-18.3,-76.8,-10.2,-78.8).curveTo(1,-81.5,12.1,-81.5).curveTo(18.8,-81.5,21.9,-80.6).curveTo(31.4,-77.8,37.4,-68.3).curveTo(36.2,-68.2,36.2,-67).curveTo(36.2,-66.3,41.7,-62.8).curveTo(48.2,-58.7,50.2,-56.7).curveTo(51.5,-55.4,53.7,-49).curveTo(54.4,-46.9,55,-45.6).curveTo(53.9,-45.2,52.6,-44.5).curveTo(50.1,-43.3,48.7,-42.4).curveTo(47.2,-45.6,44.6,-48.6).curveTo(41.9,-51.9,40,-52.3).curveTo(37.9,-52.7,37.9,-49.3).curveTo(37.9,-48.8,42,-43.1).curveTo(46.3,-37.2,46.9,-30.9).curveTo(47.4,-25.1,44.6,-20.8).curveTo(43.2,-18.6,41.6,-17.3).curveTo(41.6,-16.2,41.8,-15.9).lineTo(42,-15.6).curveTo(42.4,-15.2,44.3,-15.2).curveTo(44.8,-15.2,45.7,-15.6).lineTo(46.9,-16.2).curveTo(47,-14.9,47.4,-13.9).lineTo(48.5,-10.9).curveTo(49.6,-7.7,50.9,-5.3).lineTo(41.6,-3.4).curveTo(35.1,-2,18.7,-1.8).lineTo(14.9,-1.8).curveTo(7.4,-1.8,1.9,-2.1).closePath();
	this.shape_2.setTransform(78.6,81.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#7A7A7A").beginStroke().moveTo(-2.7,21.7).lineTo(-17.3,16.4).lineTo(-18.8,16).curveTo(-17.3,13.6,-13.5,5.7).lineTo(-12.4,3.4).curveTo(-10.2,3.7,-7.8,3.8).curveTo(-3.5,4,-0.1,2.5).lineTo(0.8,2).curveTo(1.9,1.5,3.5,0.1).curveTo(5.9,-2.1,6.4,-4).curveTo(6.9,-6.2,5.4,-11.1).curveTo(4.3,-14.4,3.4,-15.9).curveTo(1.4,-19.6,0.5,-21.5).curveTo(2.1,-24.7,2.7,-26.4).curveTo(6.7,-23.8,18.3,-22.2).lineTo(18.8,-22.2).lineTo(15.3,-0.6).curveTo(12.3,19.5,12.2,26.4).curveTo(4.9,24.4,-2.7,21.7).closePath();
	this.shape_3.setTransform(29.7,103.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB59F").beginStroke().moveTo(35.1,56.3).curveTo(35.3,55.7,35.3,53.5).curveTo(35.7,49.5,35.2,46.3).curveTo(34.8,44.1,34,42.6).curveTo(38.2,42.5,42.4,42.2).lineTo(43.7,52.2).curveTo(42.1,52.5,40.5,53).curveTo(36.5,54.3,35.2,56.4).lineTo(35.1,56.3).closePath().moveTo(0.7,41.8).lineTo(-0.9,39.1).curveTo(4.3,40.3,9.7,41.1).lineTo(13.4,53).curveTo(9.9,53.6,7,54.9).curveTo(3.9,47.6,0.7,41.8).closePath().moveTo(-37.9,1.9).curveTo(-42.1,-2.1,-43.6,-8.6).curveTo(-43.9,-9.9,-42.8,-11.9).curveTo(-41.1,-14.7,-40.1,-16.7).curveTo(-35.5,-24.9,-34.7,-36).curveTo(-34.5,-38.6,-32.6,-47.1).lineTo(-30.6,-56.4).lineTo(-29.4,-55.9).curveTo(-28.6,-55.6,-28.1,-55.5).curveTo(-28.8,-51.3,-30,-37.5).curveTo(-31.3,-21.8,-31.4,-21.6).curveTo(-32.8,-17.1,-35.7,-12.7).curveTo(-37.4,-9,-35.9,-2.8).lineTo(-34.8,1.1).lineTo(-33.7,4.9).curveTo(-35.9,3.9,-37.9,1.9).closePath();
	this.shape_4.setTransform(47.9,96.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#8E8E8E").beginStroke().moveTo(-52.4,22).curveTo(-52.2,15.2,-49.2,-5).lineTo(-45.8,-26.5).curveTo(-39.5,-25.6,-20.8,-23.9).curveTo(-11.9,-23.1,5.8,-23.6).curveTo(24.6,-24.2,33.3,-25.8).curveTo(36.2,-26.4,38.2,-27).lineTo(47.5,0.7).curveTo(51.4,12.3,52.3,14.2).curveTo(50.6,15.2,49.1,15.6).lineTo(47.5,16).curveTo(36.6,19.4,20.6,23.2).curveTo(18.4,23.7,4.2,25.3).curveTo(-11.3,27,-16.2,27).curveTo(-33.4,27,-52.4,22).closePath();
	this.shape_5.setTransform(94.2,107.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#EACFB7").beginStroke().moveTo(-10.1,53.4).curveTo(-14,53.1,-17.6,53.4).curveTo(-19.4,53.6,-21,53.9).lineTo(-24.7,42).curveTo(-15,43.5,-5,43.5).curveTo(-5,44.4,-4.8,45.2).lineTo(-4.1,50.3).lineTo(-3.6,54.7).curveTo(-6,53.8,-10.1,53.4).closePath().moveTo(19.4,52.6).curveTo(13.8,52.2,9.3,53).lineTo(8,43.1).curveTo(19.6,42.1,31.1,39.5).curveTo(30.6,40.7,30.3,41.9).curveTo(30.1,43,28.2,48.2).curveTo(26.8,51.8,26.6,53.8).curveTo(23.3,52.8,19.4,52.6).closePath().moveTo(-63.1,6.8).lineTo(-64.6,6.6).curveTo(-65.7,6.5,-66.7,6.2).lineTo(-68.1,5.7).lineTo(-69.2,1.9).lineTo(-70.3,-1.9).curveTo(-71.8,-8.2,-70.1,-11.8).curveTo(-67.2,-16.3,-65.8,-20.7).curveTo(-65.7,-21,-64.4,-36.7).curveTo(-63.2,-50.5,-62.5,-54.7).curveTo(-61.4,-54.6,-60,-54).lineTo(-58.1,-53.2).curveTo(-55.4,-52.2,-50.8,-48.7).lineTo(-49.6,-47.8).lineTo(-50.4,-45.9).curveTo(-53,-39.5,-55.3,-28.6).curveTo(-57.7,-17.4,-57.4,-15.4).lineTo(-57.4,-15.3).curveTo(-57.2,-14.4,-54.5,-9.5).lineTo(-53.6,-7.8).curveTo(-50.3,-1.5,-50,-0.4).lineTo(-49.9,-0.3).curveTo(-49.4,1.8,-50.9,3.6).lineTo(-51.6,4.3).curveTo(-54.3,6.8,-60.6,6.8).lineTo(-63.1,6.8).closePath().moveTo(53.3,-17.2).curveTo(53.5,-17.5,52.5,-19).curveTo(51.5,-20.6,51.1,-20.6).lineTo(51,-20.5).lineTo(50.9,-20.6).lineTo(50.6,-21.3).curveTo(50.4,-21.7,49.9,-24.1).curveTo(49.4,-26.5,48.7,-28.3).lineTo(47.1,-32.1).curveTo(46.6,-33.3,46.7,-33.5).curveTo(47.4,-37,47.6,-39.1).curveTo(48,-42.1,47.7,-44.9).curveTo(47,-51.5,46.3,-53.8).lineTo(48.6,-54.7).curveTo(49.3,-51.5,53.7,-41.3).curveTo(56,-35.8,56.7,-33.6).curveTo(57.1,-32.4,57.3,-31).curveTo(58.3,-27.3,60.8,-24.2).curveTo(61.8,-22.9,65.3,-19.5).curveTo(71,-13.9,71,-7.2).curveTo(71,-2.8,65.4,-1.2).curveTo(62.1,-0.4,61.5,-0.1).curveTo(61,0.1,60.7,0.4).curveTo(56.7,-11.4,53.3,-17.2).closePath();
	this.shape_6.setTransform(82.3,95.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#000000").beginStroke().moveTo(-25.8,80.7).curveTo(-31.9,79.6,-32.6,78.5).curveTo(-33.4,77.4,-33.4,75.2).curveTo(-33.4,70.1,-28.2,66.5).curveTo(-30,63.8,-32.7,58.3).curveTo(-37.9,47.3,-37.9,47.5).lineTo(-37.9,46.9).curveTo(-53.9,42.3,-69.4,33.9).lineTo(-69.5,34).curveTo(-70.9,33.5,-71.4,32.8).lineTo(-71.7,32.6).lineTo(-71.7,32.3).lineTo(-71.7,30.7).curveTo(-71.7,28.6,-66.8,18.1).curveTo(-70.7,17,-73.1,15.1).curveTo(-77.4,11.6,-78.5,4).curveTo(-79.3,-0.9,-78.4,-3.7).curveTo(-77.8,-5.5,-75.8,-8.3).curveTo(-70.8,-15.4,-69.3,-26.5).curveTo(-68.6,-32,-67.6,-36.7).lineTo(-65.4,-46.7).lineTo(-65.3,-49.4).curveTo(-66.2,-49.7,-66.7,-50.4).curveTo(-67.2,-51.1,-67.3,-52.4).lineTo(-67.2,-53.8).lineTo(-67.2,-53.9).curveTo(-66.4,-63.1,-52.8,-71).curveTo(-40.1,-78.5,-23.3,-81).curveTo(-23.2,-81,-23,-80.4).lineTo(-22.9,-80.3).lineTo(-22.6,-79.7).curveTo(-22.5,-79.2,-22.8,-79).curveTo(-23.6,-78.4,-29.3,-76.4).lineTo(-41.6,-72.2).curveTo(-49,-69.7,-53.5,-66.6).curveTo(-57.1,-64.1,-60.2,-60.5).curveTo(-64,-56.1,-63.7,-53.4).curveTo(-63.5,-52.5,-61,-51.6).lineTo(-55.3,-49.6).lineTo(-51.8,-48.1).curveTo(-50.8,-47.6,-49.7,-46.5).lineTo(-45.6,-42.4).lineTo(-45.5,-42.2).lineTo(-43.9,-42.4).lineTo(-43,-43.9).curveTo(-41.4,-46,-40.4,-45.3).curveTo(-39.7,-44.8,-39.4,-43).lineTo(-39.4,-42.3).lineTo(-39.3,-41.4).lineTo(-39.3,-41.2).lineTo(-39.3,-40.2).curveTo(-39.3,-37.2,-39.8,-34.5).lineTo(-41.2,-26.3).curveTo(-42,-20.5,-43.4,-16.6).curveTo(-44,-14.8,-44.6,-13.9).curveTo(-36.1,-12.2,-26.1,-11).curveTo(-13.2,-9.6,-5.6,-8.6).curveTo(-2.6,-8.2,1.6,-8).curveTo(8.5,-7.5,18.4,-7.6).curveTo(34.8,-7.9,41.3,-9.2).lineTo(50.6,-11.2).curveTo(49.3,-13.6,48.2,-16.8).lineTo(47.1,-19.8).curveTo(46.7,-20.8,46.6,-22.1).lineTo(45.4,-21.5).curveTo(44.5,-21.1,44,-21.1).curveTo(42.1,-21.1,41.7,-21.5).lineTo(41.5,-21.8).curveTo(41.3,-22.1,41.3,-23.2).curveTo(42.9,-24.5,44.3,-26.7).curveTo(47.1,-30.9,46.6,-36.8).curveTo(46,-43,41.7,-48.9).curveTo(37.6,-54.7,37.6,-55.2).curveTo(37.6,-58.5,39.7,-58.2).curveTo(41.6,-57.7,44.3,-54.5).curveTo(46.9,-51.4,48.4,-48.3).curveTo(49.8,-49.2,52.3,-50.4).curveTo(53.6,-51.1,54.7,-51.4).curveTo(54.1,-52.8,53.4,-54.9).curveTo(51.2,-61.3,49.9,-62.6).curveTo(47.9,-64.6,41.4,-68.7).curveTo(35.9,-72.2,35.9,-72.9).curveTo(35.9,-74.1,37.1,-74.2).curveTo(37.9,-74.3,39.4,-73.8).curveTo(42.8,-72.8,47,-69.7).curveTo(58,-61.4,58,-51.7).lineTo(58,-51).lineTo(58,-50.5).lineTo(57.9,-49.9).lineTo(57.8,-49.7).lineTo(57.6,-49.5).lineTo(57.5,-49.4).lineTo(56.4,-48.9).curveTo(55.6,-48.7,55.8,-48.1).curveTo(58.2,-41.8,61.1,-35.5).curveTo(63.2,-31,64.7,-26).curveTo(66.6,-19.4,69.3,-15.9).curveTo(70.7,-14,74,-11.6).curveTo(76.5,-9.6,77.5,-7.5).curveTo(78.9,-4.3,78.9,2).curveTo(78.9,5.3,75.9,8.5).curveTo(72.2,12.4,66.2,12.8).lineTo(65.4,12.9).lineTo(65.7,14).curveTo(70.7,29.6,70.7,35.4).lineTo(70.6,36.4).curveTo(70,37.6,69.5,38).curveTo(69,38.5,68.4,38.5).lineTo(44,45.6).lineTo(39.7,46.8).curveTo(39.6,49.3,37.9,55.2).curveTo(36.2,61.1,34.1,63.8).lineTo(36,64.7).curveTo(42.3,68.3,42,73.6).curveTo(41.9,75.1,41.4,75.6).curveTo(40.9,76.4,39,76.8).curveTo(37.6,77.2,21.6,79.2).curveTo(15,79.9,9.5,79.2).curveTo(7.3,78.7,7.2,78).lineTo(6.9,76.5).lineTo(6.5,77.1).curveTo(4.4,79.5,-4.7,80.7).curveTo(-10.5,81,-15.8,81).curveTo(-21,81,-25.8,80.7).closePath().moveTo(-22.2,68.6).lineTo(-23.8,69).lineTo(-24.4,69.2).lineTo(-25.8,69.8).curveTo(-27.3,70.7,-27.9,72.1).lineTo(-28.7,74.6).curveTo(-28,74.9,-22.7,75.3).curveTo(-17.8,75.8,-16.1,75.7).curveTo(-12.7,75.8,-9.8,75.6).curveTo(-3.5,75.2,-0.1,74.1).curveTo(1.5,73.5,2.3,72.9).lineTo(2.8,72.4).curveTo(2.8,70.7,1.3,69.2).curveTo(-0.3,67.6,-2.3,67.4).lineTo(-4.3,67.4).curveTo(-16.8,67.4,-22.2,68.6).closePath().moveTo(11.8,66.7).curveTo(10.3,67.2,9.3,68).curveTo(8.8,68.3,8,69.3).curveTo(7.5,70.1,6.9,70.1).curveTo(7.3,71.3,7.3,72.7).curveTo(7.3,74.4,7.1,75.4).curveTo(7.8,75.3,9.8,75.2).lineTo(17,74.6).curveTo(27,73.8,32.4,73.2).curveTo(35.7,72.8,35.4,72).curveTo(34.5,69.4,31,67.7).curveTo(26.9,65.9,19.7,65.9).lineTo(19.1,65.9).curveTo(14.5,65.9,11.8,66.7).closePath().moveTo(11.4,51.6).curveTo(7.2,51.9,3,52).curveTo(3.8,53.4,4.2,55.7).curveTo(4.7,58.9,4.3,62.9).curveTo(4.3,65,4.1,65.7).lineTo(4.2,65.7).curveTo(5.5,63.7,9.5,62.4).curveTo(11.1,61.9,12.7,61.6).curveTo(17.2,60.7,22.8,61.2).curveTo(26.7,61.4,30,62.3).curveTo(30.2,60.4,31.6,56.7).curveTo(33.5,51.5,33.7,50.4).curveTo(34,49.2,34.5,48.1).curveTo(23,50.7,11.4,51.6).closePath().moveTo(-30.3,51.2).curveTo(-27.1,57,-24,64.2).curveTo(-21.1,63,-17.6,62.4).curveTo(-16,62.1,-14.2,62).curveTo(-10.6,61.7,-6.7,62).curveTo(-2.6,62.4,-0.2,63.2).lineTo(-0.7,58.9).lineTo(-1.4,53.8).curveTo(-1.6,53,-1.6,52.1).curveTo(-11.6,52,-21.3,50.5).curveTo(-26.7,49.7,-31.9,48.5).lineTo(-30.3,51.2).closePath().moveTo(-62.7,21.5).curveTo(-66.5,29.4,-68,31.8).lineTo(-66.5,32.2).lineTo(-52,37.5).curveTo(-44.4,40.2,-37,42.2).curveTo(-18.1,47.2,-0.9,47.1).curveTo(4,47.1,19.5,45.4).curveTo(33.7,43.8,36,43.3).curveTo(51.9,39.5,62.8,36.1).lineTo(64.4,35.8).curveTo(66,35.3,67.7,34.3).curveTo(66.7,32.4,62.8,20.8).lineTo(53.5,-6.9).curveTo(51.6,-6.3,48.6,-5.7).curveTo(39.9,-4.1,21.1,-3.5).curveTo(3.5,-3,-5.5,-3.7).curveTo(-24.2,-5.5,-30.4,-6.3).lineTo(-30.9,-6.4).curveTo(-42.5,-8,-46.5,-10.6).curveTo(-47.2,-8.9,-48.7,-5.7).curveTo(-47.8,-3.8,-45.8,-0.1).curveTo(-45,1.4,-43.9,4.7).curveTo(-42.3,9.6,-42.8,11.8).curveTo(-43.3,13.7,-45.7,15.9).curveTo(-47.4,17.3,-48.4,17.8).lineTo(-49.4,18.3).curveTo(-52.7,19.8,-57,19.6).curveTo(-59.5,19.5,-61.6,19.2).lineTo(-62.7,21.5).closePath().moveTo(-63.6,-37.7).curveTo(-65.5,-29.2,-65.7,-26.7).curveTo(-66.5,-15.5,-71.1,-7.3).curveTo(-72.1,-5.3,-73.8,-2.6).curveTo(-74.9,-0.5,-74.6,0.8).curveTo(-73.1,7.3,-68.9,11.3).curveTo(-66.9,13.3,-64.7,14.3).lineTo(-63.3,14.7).curveTo(-62.3,15.1,-61.2,15.2).lineTo(-59.7,15.3).curveTo(-51.4,15.9,-48.2,12.8).lineTo(-47.5,12.2).curveTo(-46,10.3,-46.5,8.2).lineTo(-46.6,8.1).curveTo(-46.9,7,-50.2,0.8).lineTo(-51.1,-0.9).curveTo(-53.8,-5.9,-54,-6.7).lineTo(-54,-6.9).curveTo(-54.3,-8.9,-51.9,-20).curveTo(-49.6,-31,-47,-37.3).lineTo(-46.2,-39.3).lineTo(-47.4,-40.2).curveTo(-52,-43.7,-54.7,-44.7).lineTo(-56.6,-45.4).curveTo(-58,-46,-59.1,-46.1).curveTo(-59.6,-46.2,-60.4,-46.6).lineTo(-61.6,-47).lineTo(-63.6,-37.7).closePath().moveTo(55.9,-10.5).curveTo(56.9,-8.9,56.7,-8.7).curveTo(60.1,-2.8,64.1,8.9).curveTo(64.4,8.7,64.9,8.4).curveTo(65.5,8.2,68.8,7.3).curveTo(74.4,5.8,74.4,1.4).curveTo(74.4,-5.4,68.7,-10.9).curveTo(65.2,-14.3,64.2,-15.6).curveTo(61.7,-18.7,60.7,-22.5).curveTo(60.5,-23.8,60.1,-25.1).curveTo(59.4,-27.3,57.1,-32.7).curveTo(52.7,-42.9,52,-46.1).lineTo(49.7,-45.2).curveTo(50.4,-43,51.1,-36.3).curveTo(51.4,-33.6,51,-30.6).curveTo(50.8,-28.5,50.1,-25).curveTo(50,-24.7,50.5,-23.5).lineTo(52.1,-19.8).curveTo(52.8,-18,53.3,-15.5).curveTo(53.8,-13.2,54,-12.7).lineTo(54.3,-12).lineTo(54.4,-11.9).lineTo(54.5,-12).curveTo(54.9,-12,55.9,-10.5).closePath().moveTo(-47.2,-22.7).curveTo(-49.6,-17.2,-49.9,-12.8).curveTo(-48.8,-14.1,-48,-14.5).lineTo(-48.3,-15).curveTo(-48.4,-15.2,-48.4,-16.5).curveTo(-48.4,-17.3,-47.4,-20.9).lineTo(-46.5,-24.2).lineTo(-47.2,-22.7).closePath().moveTo(37.3,-73.8).lineTo(37.1,-74.2).lineTo(37.3,-73.8).closePath().moveTo(-25,-79).curveTo(-24.1,-79.7,-22.9,-80.3).curveTo(-24.1,-79.7,-25,-79).closePath();
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.Letter = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop()
		this.stop();
	}
	this.frame_1 = function() {
		this.stop()
		this.stop();
	}
	this.frame_2 = function() {
		this.stop();
		this.stop();
	}
	this.frame_3 = function() {
		this.stop();
		this.stop();
	}
	this.frame_4 = function() {
		this.stop();
		this.stop();
	}
	this.frame_5 = function() {
		this.stop();
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1).call(this.frame_2).wait(1).call(this.frame_3).wait(1).call(this.frame_4).wait(1).call(this.frame_5).wait(1));

	// Layer 2
	this.mc_TextFinance = new cjs.Text("finance", "44px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_TextFinance.name = "mc_TextFinance";
	this.mc_TextFinance.textAlign = "center";
	this.mc_TextFinance.lineHeight = 46;
	this.mc_TextFinance.setTransform(108.2,23.4,1.031,1.128,0,-35.1,0);

	this.timeline.addTween(cjs.Tween.get(this.mc_TextFinance).wait(1).to({scaleX:1.29,scaleY:1.13,x:107.8,text:"i.t.",lineWidth:62},0).wait(1).to({scaleX:1.19,x:108.1,y:20.7,text:"h.r.",lineWidth:77},0).wait(1).to({scaleX:0.7,scaleY:1.06,x:106.1,y:25.6,text:"commercial",lineWidth:263},0).wait(1).to({scaleX:0.82,scaleY:0.74,x:99.8,y:18.3,text:"marketing\n& comms",lineWidth:230},0).wait(1).to({scaleX:1.09,scaleY:0.88,skewX:-39.6,x:88.7,y:13.2,text:"supply chain",lineWidth:257},0).wait(1));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#999999").beginStroke().moveTo(-17.2,-3.8).lineTo(9.8,-3.5).lineTo(17.3,3.5).lineTo(-11.6,3.8).closePath();
	this.shape.setTransform(173.5,12.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#999999").beginStroke().moveTo(-2.4,6.8).lineTo(-14.3,-6.3).lineTo(1.9,-6.7).lineTo(14.3,6.8).closePath();
	this.shape_1.setTransform(175.6,17.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#999999").beginStroke().moveTo(-2.4,6.8).lineTo(-14.3,-6.4).lineTo(1.9,-6.7).lineTo(14.3,6.8).closePath();
	this.shape_2.setTransform(184.6,15.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#999999").beginStroke().moveTo(-17.2,-3.9).lineTo(9.8,-3.5).lineTo(17.3,3.5).lineTo(-11.6,3.9).closePath();
	this.shape_3.setTransform(172.6,11.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#999999").beginStroke().moveTo(-15.6,-8.1).lineTo(-1.2,-7.7).lineTo(15.6,7.7).lineTo(-0.6,8.1).closePath();
	this.shape_4.setTransform(187.7,16.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).wait(1));

	// Layer 4
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#4B8471").beginStroke().moveTo(-89.4,34).curveTo(-91,31.7,-95.8,26.3).curveTo(-104.3,16.8,-104.9,16.1).curveTo(-106.6,13.9,-109.4,6.3).curveTo(-111.7,0.1,-116.4,-3.6).curveTo(-123.4,-12.9,-131.8,-22.3).curveTo(-138.3,-29.6,-143,-34).curveTo(-129,-34,-127.4,-34.3).lineTo(-24.6,-34.3).curveTo(-23.6,-34.1,10.3,-33.8).lineTo(44,-33.5).lineTo(55.4,-33.2).curveTo(62.1,-33.2,65.3,-33.5).curveTo(70.3,-27.3,104.5,1.8).curveTo(135,27.8,143,33.7).lineTo(-14.3,34.3).closePath();
	this.shape_5.setTransform(124.5,39.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#E5B700").beginStroke().moveTo(-89.4,34).curveTo(-91,31.7,-95.8,26.3).curveTo(-104.3,16.8,-104.9,16.1).curveTo(-106.6,13.9,-109.4,6.3).curveTo(-111.7,0.1,-116.4,-3.6).curveTo(-123.4,-12.9,-131.8,-22.3).curveTo(-138.3,-29.6,-143,-34).curveTo(-129,-34,-127.4,-34.3).lineTo(-24.6,-34.3).curveTo(-23.6,-34.1,10.3,-33.8).lineTo(44,-33.5).lineTo(55.4,-33.2).curveTo(62.1,-33.2,65.3,-33.5).curveTo(70.3,-27.3,104.5,1.8).curveTo(135,27.8,143,33.7).lineTo(-14.3,34.3).closePath();
	this.shape_6.setTransform(124.5,39.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#BD3A24").beginStroke().moveTo(-89.4,34).curveTo(-91,31.7,-95.8,26.3).curveTo(-104.3,16.8,-104.9,16.1).curveTo(-106.6,13.9,-109.4,6.3).curveTo(-111.7,0.1,-116.4,-3.6).curveTo(-123.4,-12.9,-131.8,-22.3).curveTo(-138.3,-29.6,-143,-34).curveTo(-129,-34,-127.4,-34.3).lineTo(-24.6,-34.3).curveTo(-23.6,-34.1,10.3,-33.8).lineTo(44,-33.5).lineTo(55.4,-33.2).curveTo(62.1,-33.2,65.3,-33.5).curveTo(70.3,-27.3,104.5,1.8).curveTo(135,27.8,143,33.7).lineTo(-14.3,34.3).closePath();
	this.shape_7.setTransform(124.5,39.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#D67534").beginStroke().moveTo(-89.4,34).curveTo(-91,31.7,-95.8,26.3).curveTo(-104.3,16.8,-104.9,16.1).curveTo(-106.6,13.9,-109.4,6.3).curveTo(-111.7,0.1,-116.4,-3.6).curveTo(-123.4,-12.9,-131.8,-22.3).curveTo(-138.3,-29.6,-143,-34).curveTo(-129,-34,-127.4,-34.3).lineTo(-24.6,-34.3).curveTo(-23.6,-34.1,10.3,-33.8).lineTo(44,-33.5).lineTo(55.4,-33.2).curveTo(62.1,-33.2,65.3,-33.5).curveTo(70.3,-27.3,104.5,1.8).curveTo(135,27.8,143,33.7).lineTo(-14.3,34.3).closePath();
	this.shape_8.setTransform(124.5,39.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#527BCE").beginStroke().moveTo(-89.4,34).curveTo(-91,31.7,-95.8,26.3).curveTo(-104.3,16.8,-104.9,16.1).curveTo(-106.6,13.9,-109.4,6.3).curveTo(-111.7,0.1,-116.4,-3.6).curveTo(-123.4,-12.9,-131.8,-22.3).curveTo(-138.3,-29.6,-143,-34).curveTo(-129,-34,-127.4,-34.3).lineTo(-24.6,-34.3).curveTo(-23.6,-34.1,10.3,-33.8).lineTo(44,-33.5).lineTo(55.4,-33.2).curveTo(62.1,-33.2,65.3,-33.5).curveTo(70.3,-27.3,104.5,1.8).curveTo(135,27.8,143,33.7).lineTo(-14.3,34.3).closePath();
	this.shape_9.setTransform(124.5,39.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#834F9E").beginStroke().moveTo(-89.4,34).curveTo(-91,31.7,-95.8,26.3).curveTo(-104.3,16.8,-104.9,16.1).curveTo(-106.6,13.9,-109.4,6.3).curveTo(-111.7,0.1,-116.4,-3.6).curveTo(-123.4,-12.9,-131.8,-22.3).curveTo(-138.3,-29.6,-143,-34).curveTo(-129,-34,-127.4,-34.3).lineTo(-24.6,-34.3).curveTo(-23.6,-34.1,10.3,-33.8).lineTo(44,-33.5).lineTo(55.4,-33.2).curveTo(62.1,-33.2,65.3,-33.5).curveTo(70.3,-27.3,104.5,1.8).curveTo(135,27.8,143,33.7).lineTo(-14.3,34.3).closePath();
	this.shape_10.setTransform(124.5,39.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5}]}).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).wait(1));

	// Layer 1
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-89.4,34).curveTo(-91,31.7,-95.8,26.3).curveTo(-104.3,16.8,-104.9,16.1).curveTo(-106.6,13.9,-109.4,6.3).curveTo(-111.7,0.1,-116.4,-3.6).curveTo(-123.4,-12.9,-131.8,-22.3).curveTo(-138.3,-29.6,-143,-34).curveTo(-129,-34,-127.4,-34.3).lineTo(-24.6,-34.3).curveTo(-23.6,-34.1,10.3,-33.8).lineTo(44,-33.5).lineTo(55.4,-33.2).curveTo(62.1,-33.2,65.3,-33.5).curveTo(70.3,-27.3,104.5,1.8).curveTo(135,27.8,143,33.7).lineTo(132.3,33.9).lineTo(-14.3,34.3).closePath();
	this.shape_11.setTransform(124.5,39.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#000000").beginStroke().moveTo(17.5,39.1).lineTo(-91.2,38.7).lineTo(-93.2,37.1).lineTo(-93.6,36.7).curveTo(-100.3,30.8,-148.3,-35.2).curveTo(-148.3,-37.2,-148.1,-37.4).lineTo(-147.3,-36.7).curveTo(-147,-37.7,-145.8,-38).curveTo(-142.2,-39,-128.3,-39.2).lineTo(-128.3,-39.5).curveTo(-26,-39.6,-24.5,-39.2).lineTo(43.4,-38.7).curveTo(44,-38.6,55.1,-38.6).curveTo(66.5,-38.6,67.9,-38.4).curveTo(67.9,-38.2,68.3,-37.8).curveTo(68.6,-37.4,68.6,-36.2).curveTo(68.6,-35.7,81.7,-23.8).curveTo(96.1,-10.7,106.2,-2.7).curveTo(121.5,7,147.4,32.1).curveTo(148.9,36.3,148,38.1).curveTo(147.2,38.6,146.3,38.9).curveTo(145.3,39.3,142.4,39.1).lineTo(19,39.6).closePath().moveTo(-144.2,-34).curveTo(-139.5,-29.6,-133,-22.3).curveTo(-124.6,-12.9,-117.6,-3.5).curveTo(-112.9,0.1,-110.6,6.3).curveTo(-107.8,13.9,-106.1,16.1).curveTo(-105.5,16.8,-97,26.3).curveTo(-92.2,31.7,-90.6,34).lineTo(-15.5,34.3).lineTo(131.1,33.9).lineTo(142.2,34).lineTo(141.8,33.7).curveTo(133.8,27.8,103.3,1.8).curveTo(69.1,-27.3,64.1,-33.5).curveTo(60.9,-33.2,54.2,-33.2).lineTo(42.8,-33.5).lineTo(9.1,-33.8).curveTo(-24.8,-34.1,-25.8,-34.3).lineTo(-128.6,-34.3).curveTo(-130.2,-34,-144.2,-34).closePath();
	this.shape_12.setTransform(125.7,39.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11}]}).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.6,0,296.7,79.1);


(lib.Lead = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Leaderboard", "25px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 31;
	this.text.setTransform(11.8,7.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#502A40").beginStroke().moveTo(-89,19.5).lineTo(-89,-19.5).lineTo(89,-19.5).lineTo(89,19.5).closePath();
	this.shape.setTransform(89,19.5);

	this.addChild(this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,178.1,40.8);


(lib.LadyBoss_Bubble2 = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("this is not appropriate workwear. in an office environment you'll meet people from all areas of the business and possibly customers too, so think about the impression you give with what you wear. choose  something else.", "22px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 27;
	this.text.lineWidth = 346;
	this.text.setTransform(131.3,-154.3,1.227,1.227);

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-229.1,-162.6).lineTo(190.9,-162.6).curveTo(205.7,-162.6,205.7,-147.9).lineTo(205.7,-5.9).lineTo(243.8,22.4).lineTo(205.7,48.1).lineTo(205.7,147.8).curveTo(205.7,162.6,190.9,162.6).lineTo(-229.1,162.6).curveTo(-243.8,162.6,-243.8,147.8).lineTo(-243.8,-147.9).curveTo(-243.8,-162.6,-229.1,-162.6).closePath();
	this.shape.setTransform(154.4,-9.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#66CCFF").beginStroke().moveTo(-229.1,162.6).curveTo(-243.8,162.6,-243.8,147.8).lineTo(-243.8,-147.9).curveTo(-243.8,-162.6,-229.1,-162.6).lineTo(190.9,-162.6).curveTo(205.7,-162.6,205.7,-147.9).lineTo(205.7,-5.9).lineTo(243.8,22.4).lineTo(205.7,48.1).lineTo(205.7,147.8).curveTo(205.7,162.6,190.9,162.6).closePath();
	this.shape_1.setTransform(154.4,-9.4);

	// Layer 5
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("rgba(0,0,0,0.502)").beginStroke().moveTo(-229.1,162.6).curveTo(-243.8,162.6,-243.8,147.9).lineTo(-243.8,-147.9).curveTo(-243.8,-162.6,-229.1,-162.6).lineTo(191,-162.6).curveTo(205.7,-162.6,205.7,-147.9).lineTo(205.7,-5.9).lineTo(243.8,22.4).lineTo(205.7,48.1).lineTo(205.7,147.9).curveTo(205.7,162.6,191,162.6).closePath();
	this.shape_2.setTransform(163.7,-0.1);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-91.4,-174,499,344.8);


(lib.LadyBoss = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(-3,2).lineTo(-5.6,-5.8).curveTo(-5.6,-12.3,-2.6,-8.5).curveTo(-1.3,-7,1,-2.4).curveTo(1.5,-0.4,3.2,3.2).lineTo(5.5,8.6).lineTo(5.4,9.3).curveTo(5.3,9.2,4.5,9.2).curveTo(4.2,9.2,3.5,9.5).curveTo(3,9.9,2.5,9.9).curveTo(0.1,9.9,-3,2).closePath();
	this.shape.setTransform(263.9,241.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#BC8E67").beginStroke().moveTo(0.3,4.1).curveTo(-0.4,4.1,-0.6,3.4).lineTo(-0.6,2.7).lineTo(-0.6,2.6).lineTo(-0.6,2.5).curveTo(-1.5,0.7,-1.5,-0).lineTo(-1.5,-0.1).lineTo(-1.2,-0.3).lineTo(-1.2,-1.5).lineTo(-1,-2.5).curveTo(-0.8,-3,-0.2,-3.1).curveTo(0.2,-2.7,0.2,-1.9).curveTo(0.2,-1.4,0.1,-1).lineTo(-0,-0.7).lineTo(0.7,-0.8).lineTo(0.6,-2.6).curveTo(0.6,-3.7,1.1,-4.1).curveTo(1.5,-3.4,1.5,-1.4).lineTo(1.5,-0.9).curveTo(1.5,0.1,1.3,0.6).curveTo(0.9,1.5,0.7,3).lineTo(0.9,3.7).curveTo(0.8,4.1,0.3,4.1).lineTo(0.3,4.1).closePath();
	this.shape_1.setTransform(162,262.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#DAA577").beginStroke().moveTo(-16.4,6.9).curveTo(-32.7,5.3,-43.4,2.3).curveTo(-49.3,0.6,-51.9,-1).curveTo(-51.5,-1,-51.3,-1.4).lineTo(-51.6,-2.1).curveTo(-51.3,-3.6,-51,-4.5).curveTo(-50.8,-5,-50.7,-6).lineTo(-50.6,-6).curveTo(-49.7,-6.6,-47.8,-6.9).curveTo(-46.8,-6.5,-46.5,-5.7).lineTo(-46.5,-5.5).lineTo(-46.5,-5.4).lineTo(-46.5,-3.9).lineTo(-46.6,-1.9).lineTo(-44,-1).curveTo(-36.3,1.3,-28.6,2.6).lineTo(-28.3,2.5).curveTo(-15.6,2.5,-11.4,3).curveTo(-9.2,3.3,-8.4,4.1).lineTo(9.3,4.1).curveTo(9.2,3.8,9.2,3.1).curveTo(9.2,2.1,9.5,2).lineTo(10.5,1.9).lineTo(14.9,1.5).curveTo(15.6,0.8,16.1,0.5).curveTo(26.5,-0,29.3,-0.6).lineTo(43,-0.6).curveTo(44.5,-0.3,44.8,0.6).curveTo(44.9,0.8,44.9,2).curveTo(44.9,2.7,44.7,3.3).curveTo(44.9,3.5,44.9,4.4).lineTo(44.9,5.2).lineTo(46.2,5).curveTo(45.8,4.9,45.6,4.8).curveTo(45.7,4.2,45.6,4.1).curveTo(46.5,3.8,46.7,4.2).curveTo(50.1,4.2,50.4,4.1).curveTo(50.4,4.1,50.5,4.1).curveTo(50.5,4,50.6,4).curveTo(50.6,4,50.6,3.9).curveTo(50.7,3.9,50.7,3.8).curveTo(51.2,3.6,51.7,3.7).lineTo(51.9,4.1).curveTo(51.3,4.3,51,4.8).curveTo(50.5,5,49.1,5.1).lineTo(48,5.4).lineTo(42,6.7).lineTo(39.3,6.7).lineTo(39.1,6.7).lineTo(39.1,6.7).lineTo(38.7,6.9).closePath().moveTo(44.5,5.2).lineTo(44.3,5.3).lineTo(44.7,5.2).lineTo(44.6,5.2).lineTo(44.5,5.2).closePath().moveTo(41.4,2.1).curveTo(40.4,2.1,40.4,2.2).curveTo(40.7,2.7,41.8,2.6).curveTo(42.4,2.3,42.4,2).lineTo(41.4,2.1).closePath();
	this.shape_2.setTransform(214.2,267.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(27));

	// Layer 5
	this.instance = new lib.Health_and_Safety_Officer__000();
	this.instance.setTransform(68.5,0);

	this.instance_1 = new lib.Health_and_Safety_Officer__001();
	this.instance_1.setTransform(68.5,0);

	this.instance_2 = new lib.Health_and_Safety_Officer__002();
	this.instance_2.setTransform(68.5,0);

	this.instance_3 = new lib.Health_and_Safety_Officer__003();
	this.instance_3.setTransform(68.5,0);

	this.instance_4 = new lib.Health_and_Safety_Officer__004();
	this.instance_4.setTransform(68.5,0);

	this.instance_5 = new lib.Health_and_Safety_Officer__005();
	this.instance_5.setTransform(68.5,0);

	this.instance_6 = new lib.Health_and_Safety_Officer__006();
	this.instance_6.setTransform(68.5,0);

	this.instance_7 = new lib.Health_and_Safety_Officer__007();
	this.instance_7.setTransform(68.5,0);

	this.instance_8 = new lib.Health_and_Safety_Officer__008();
	this.instance_8.setTransform(68.5,0);

	this.instance_9 = new lib.Health_and_Safety_Officer__009();
	this.instance_9.setTransform(68.5,0);

	this.instance_10 = new lib.Health_and_Safety_Officer__010();
	this.instance_10.setTransform(68.5,0);

	this.instance_11 = new lib.Health_and_Safety_Officer__011();
	this.instance_11.setTransform(68.5,0);

	this.instance_12 = new lib.Health_and_Safety_Officer__012();
	this.instance_12.setTransform(68.5,0);

	this.instance_13 = new lib.Health_and_Safety_Officer__013();
	this.instance_13.setTransform(68.5,0);

	this.instance_14 = new lib.Health_and_Safety_Officer__014();
	this.instance_14.setTransform(68.5,0);

	this.instance_15 = new lib.Health_and_Safety_Officer__015();
	this.instance_15.setTransform(68.5,0);

	this.instance_16 = new lib.Health_and_Safety_Officer__016();
	this.instance_16.setTransform(68.5,0);

	this.instance_17 = new lib.Health_and_Safety_Officer__017();
	this.instance_17.setTransform(68.5,0);

	this.instance_18 = new lib.Health_and_Safety_Officer__018();
	this.instance_18.setTransform(68.5,0);

	this.instance_19 = new lib.Health_and_Safety_Officer__019();
	this.instance_19.setTransform(68.5,0);

	this.instance_20 = new lib.Health_and_Safety_Officer__020();
	this.instance_20.setTransform(68.5,0);

	this.instance_21 = new lib.Health_and_Safety_Officer__021();
	this.instance_21.setTransform(68.5,0);

	this.instance_22 = new lib.Health_and_Safety_Officer__022();
	this.instance_22.setTransform(68.5,0);

	this.instance_23 = new lib.Health_and_Safety_Officer__023();
	this.instance_23.setTransform(68.5,0);

	this.instance_24 = new lib.Health_and_Safety_Officer__024();
	this.instance_24.setTransform(68.5,0);

	this.instance_25 = new lib.Health_and_Safety_Officer__025();
	this.instance_25.setTransform(68.5,0);

	this.instance_26 = new lib.Health_and_Safety_Officer__026();
	this.instance_26.setTransform(68.5,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(68.5,0,260,369);


(lib.KateClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#4E2A3F").setStrokeStyle(1,1,1).moveTo(31.2,6.8).curveTo(31,6.6,30.9,6.4).curveTo(25,-3.1,15.4,-5.9).curveTo(12.3,-6.8,5.6,-6.8).curveTo(-5.5,-6.8,-16.6,-4.1).curveTo(-24.8,-2.1,-29.1,0.2).curveTo(-30.3,0.9,-31.2,1.5);
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#371E2D").beginStroke().moveTo(-16.8,4).curveTo(-22.1,3.6,-22.9,3.3).lineTo(-22,0.9).curveTo(-21.5,-0.6,-19.9,-1.5).lineTo(-18.5,-2.1).lineTo(-17.9,-2.3).lineTo(-16.3,-2.6).lineTo(-16.4,-1.9).curveTo(-16.3,1.3,-6.5,3.8).lineTo(-3.9,4.4).curveTo(-6.8,4.5,-10.3,4.5).curveTo(-11.9,4.5,-16.8,4).closePath().moveTo(13.1,1.4).curveTo(13.2,0.1,12.8,-1.2).curveTo(13.4,-1.2,13.9,-1.9).curveTo(14.7,-2.9,15.2,-3.3).curveTo(16.2,-4,17.7,-4.5).curveTo(18.8,0,20.1,1.5).curveTo(20.9,2.5,22.8,3.3).lineTo(15.7,3.9).curveTo(13.7,4,13,4.1).curveTo(13.2,3.1,13.1,1.4).closePath();
	this.shape_1.setTransform(73,158.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#4D2A3E").beginStroke().moveTo(-19,4.3).curveTo(-28.8,1.8,-28.8,-1.4).lineTo(-28.8,-2.1).curveTo(-22.9,-3.4,-8.9,-3.3).curveTo(-6.9,-3.1,-5.3,-1.6).curveTo(-3.8,-0.1,-3.8,1.7).lineTo(-4.3,2.2).curveTo(-5.1,2.8,-6.7,3.4).curveTo(-10.1,4.5,-16.4,4.9).lineTo(-19,4.3).closePath().moveTo(7.6,2).curveTo(6.3,0.5,5.2,-4).curveTo(8.1,-4.9,13.1,-4.8).curveTo(20.3,-4.8,24.4,-3).curveTo(27.9,-1.4,28.8,1.3).curveTo(29.1,2,25.8,2.5).curveTo(20.4,3.1,10.4,3.9).curveTo(8.4,3,7.6,2).closePath();
	this.shape_2.setTransform(85.5,158.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#7A7A7A").beginStroke().moveTo(-2.7,21.7).lineTo(-17.3,16.4).lineTo(-18.8,16).curveTo(-17.3,13.6,-13.5,5.7).lineTo(-12.4,3.4).curveTo(-10.2,3.7,-7.8,3.8).curveTo(-3.5,4,-0.1,2.5).lineTo(0.8,2).curveTo(1.9,1.5,3.5,0.1).curveTo(5.9,-2.1,6.4,-4).curveTo(6.9,-6.2,5.4,-11.1).curveTo(4.3,-14.4,3.4,-15.9).curveTo(1.4,-19.6,0.5,-21.5).curveTo(2.1,-24.7,2.7,-26.4).curveTo(6.7,-23.8,18.3,-22.2).lineTo(18.8,-22.2).lineTo(15.3,-0.6).curveTo(12.3,19.5,12.2,26.4).curveTo(4.9,24.4,-2.7,21.7).closePath();
	this.shape_3.setTransform(29.7,103.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB59F").beginStroke().moveTo(35.1,56.3).curveTo(35.3,55.7,35.3,53.5).curveTo(35.7,49.5,35.2,46.3).curveTo(34.8,44.1,34,42.6).curveTo(38.2,42.5,42.4,42.2).lineTo(43.7,52.2).curveTo(42.1,52.5,40.5,53).curveTo(36.5,54.3,35.2,56.4).lineTo(35.1,56.3).closePath().moveTo(0.7,41.8).lineTo(-0.9,39.1).curveTo(4.3,40.3,9.7,41.1).lineTo(13.4,53).curveTo(9.9,53.6,7,54.9).curveTo(3.9,47.6,0.7,41.8).closePath().moveTo(-37.9,1.9).curveTo(-42.1,-2.1,-43.6,-8.6).curveTo(-43.9,-9.9,-42.8,-11.9).curveTo(-41.1,-14.7,-40.1,-16.7).curveTo(-35.5,-24.9,-34.7,-36).curveTo(-34.5,-38.6,-32.6,-47.1).lineTo(-30.6,-56.4).lineTo(-29.4,-55.9).curveTo(-28.6,-55.6,-28.1,-55.5).curveTo(-28.8,-51.3,-30,-37.5).curveTo(-31.3,-21.8,-31.4,-21.6).curveTo(-32.8,-17.1,-35.7,-12.7).curveTo(-37.4,-9,-35.9,-2.8).lineTo(-34.8,1.1).lineTo(-33.7,4.9).curveTo(-35.9,3.9,-37.9,1.9).closePath();
	this.shape_4.setTransform(47.9,96.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#381E2D").beginStroke().moveTo(25.4,31.5).curveTo(17.9,30.5,5,29).curveTo(-5.1,27.8,-13.5,26.1).curveTo(-13,25.3,-12.3,23.4).curveTo(-11,19.6,-10.1,13.7).lineTo(-8.7,5.5).curveTo(-8.2,2.9,-8.2,-0.1).lineTo(-8.2,-1.1).lineTo(-8.2,-1.3).lineTo(-8.3,-2.2).lineTo(-8.4,-2.9).curveTo(-7.7,-3.1,-7,-3.2).curveTo(-6.5,-3.3,-5.6,-2).curveTo(-4.9,-1,-4.4,0.2).curveTo(-4.1,0.8,-3.3,5.6).curveTo(-1.9,10.9,2,15.3).curveTo(6,19.7,11.5,21.9).curveTo(14.7,23.2,20.9,24.3).curveTo(26.5,25.3,28.6,26.4).curveTo(31.9,28.2,32.7,32.1).curveTo(28.5,31.9,25.4,31.5).closePath().moveTo(-24.3,-9.5).lineTo(-29.9,-11.5).curveTo(-32.5,-12.5,-32.6,-13.4).curveTo(-33,-16.1,-29.1,-20.5).curveTo(-26.1,-24,-22.5,-26.5).curveTo(-18,-29.6,-10.6,-32.1).curveTo(-12.7,-30.6,-16.1,-25.9).curveTo(-20,-20.7,-22,-16.1).curveTo(-22.7,-14.7,-23.2,-11.5).curveTo(-23.5,-9.5,-24.2,-9.5).lineTo(-24.3,-9.5).closePath();
	this.shape_5.setTransform(47.8,47.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#8E8E8E").beginStroke().moveTo(-52.4,22).curveTo(-52.2,15.2,-49.2,-5).lineTo(-45.8,-26.5).curveTo(-39.5,-25.6,-20.8,-23.9).curveTo(-11.9,-23.1,5.8,-23.6).curveTo(24.6,-24.2,33.3,-25.8).curveTo(36.2,-26.4,38.2,-27).lineTo(47.5,0.7).curveTo(51.4,12.3,52.3,14.2).curveTo(50.6,15.2,49.1,15.6).lineTo(47.5,16).curveTo(36.6,19.4,20.6,23.2).curveTo(18.4,23.7,4.2,25.3).curveTo(-11.3,27,-16.2,27).curveTo(-33.4,27,-52.4,22).closePath();
	this.shape_6.setTransform(94.2,107.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#EACFB7").beginStroke().moveTo(-10.1,53.4).curveTo(-14,53.1,-17.6,53.4).curveTo(-19.4,53.6,-21,53.9).lineTo(-24.7,42).curveTo(-15,43.5,-5,43.5).curveTo(-5,44.4,-4.8,45.2).lineTo(-4.1,50.3).lineTo(-3.6,54.7).curveTo(-6,53.8,-10.1,53.4).closePath().moveTo(19.4,52.6).curveTo(13.8,52.2,9.3,53).lineTo(8,43.1).curveTo(19.6,42.1,31.1,39.5).curveTo(30.6,40.7,30.3,41.9).curveTo(30.1,43,28.2,48.2).curveTo(26.8,51.8,26.6,53.8).curveTo(23.3,52.8,19.4,52.6).closePath().moveTo(-63.1,6.8).lineTo(-64.6,6.6).curveTo(-65.7,6.5,-66.7,6.2).lineTo(-68.1,5.7).lineTo(-69.2,1.9).lineTo(-70.3,-1.9).curveTo(-71.8,-8.2,-70.1,-11.8).curveTo(-67.2,-16.3,-65.8,-20.7).curveTo(-65.7,-21,-64.4,-36.7).curveTo(-63.2,-50.5,-62.5,-54.7).curveTo(-61.4,-54.6,-60,-54).lineTo(-58.1,-53.2).curveTo(-55.4,-52.2,-50.8,-48.7).lineTo(-49.6,-47.8).lineTo(-50.4,-45.9).curveTo(-53,-39.5,-55.3,-28.6).curveTo(-57.7,-17.4,-57.4,-15.4).lineTo(-57.4,-15.3).curveTo(-57.2,-14.4,-54.5,-9.5).lineTo(-53.6,-7.8).curveTo(-50.3,-1.5,-50,-0.4).lineTo(-49.9,-0.3).curveTo(-49.4,1.8,-50.9,3.6).lineTo(-51.6,4.3).curveTo(-54.3,6.8,-60.6,6.8).lineTo(-63.1,6.8).closePath().moveTo(53.3,-17.2).curveTo(53.5,-17.5,52.5,-19).curveTo(51.5,-20.6,51.1,-20.6).lineTo(51,-20.5).lineTo(50.9,-20.6).lineTo(50.6,-21.3).curveTo(50.4,-21.7,49.9,-24.1).curveTo(49.4,-26.5,48.7,-28.3).lineTo(47.1,-32.1).curveTo(46.6,-33.3,46.7,-33.5).curveTo(47.4,-37,47.6,-39.1).curveTo(48,-42.1,47.7,-44.9).curveTo(47,-51.5,46.3,-53.8).lineTo(48.6,-54.7).curveTo(49.3,-51.5,53.7,-41.3).curveTo(56,-35.8,56.7,-33.6).curveTo(57.1,-32.4,57.3,-31).curveTo(58.3,-27.3,60.8,-24.2).curveTo(61.8,-22.9,65.3,-19.5).curveTo(71,-13.9,71,-7.2).curveTo(71,-2.8,65.4,-1.2).curveTo(62.1,-0.4,61.5,-0.1).curveTo(61,0.1,60.7,0.4).curveTo(56.7,-11.4,53.3,-17.2).closePath();
	this.shape_7.setTransform(82.3,95.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#4D2A3F").beginStroke().moveTo(1.9,39.5).curveTo(1.1,35.6,-2.2,33.8).curveTo(-4.3,32.7,-9.8,31.7).curveTo(-16,30.6,-19.2,29.3).curveTo(-24.8,27.1,-28.7,22.7).curveTo(-32.6,18.3,-34.1,13).curveTo(-34.9,8.2,-35.2,7.6).curveTo(-35.6,6.4,-36.4,5.4).curveTo(-37.3,4.1,-37.8,4.2).curveTo(-38.5,4.3,-39.1,4.5).curveTo(-39.4,2.7,-40.1,2.2).curveTo(-41.1,1.5,-42.7,3.6).lineTo(-43.6,5).lineTo(-45.2,5.3).lineTo(-45.3,5).lineTo(-49.4,1).curveTo(-50.5,-0.1,-51.5,-0.7).lineTo(-55,-2.1).curveTo(-54.2,-2,-53.9,-4.1).curveTo(-53.4,-7.3,-52.8,-8.7).curveTo(-50.7,-13.3,-46.9,-18.5).curveTo(-43.4,-23.2,-41.3,-24.7).lineTo(-29,-28.9).curveTo(-23.3,-30.9,-22.5,-31.5).curveTo(-22.2,-31.7,-22.3,-32.2).lineTo(-22.6,-32.8).curveTo(-18.3,-35.2,-10.2,-37.2).curveTo(1,-39.9,12.1,-39.9).curveTo(18.8,-39.9,21.9,-39).curveTo(31.4,-36.2,37.4,-26.7).curveTo(36.2,-26.6,36.2,-25.4).curveTo(36.2,-24.7,41.7,-21.2).curveTo(48.2,-17.1,50.2,-15.1).curveTo(51.5,-13.8,53.7,-7.4).curveTo(54.4,-5.3,55,-4).curveTo(53.9,-3.6,52.6,-2.9).curveTo(50.1,-1.7,48.7,-0.8).curveTo(47.2,-4,44.6,-7).curveTo(41.9,-10.3,40,-10.7).curveTo(37.9,-11.1,37.9,-7.7).curveTo(37.9,-7.2,42,-1.5).curveTo(46.3,4.4,46.9,10.7).curveTo(47.4,16.5,44.6,20.8).curveTo(43.2,23,41.6,24.3).curveTo(41.6,25.4,41.8,25.7).lineTo(42,26).curveTo(42.4,26.4,44.3,26.4).curveTo(44.8,26.4,45.7,26).lineTo(46.9,25.4).curveTo(47,26.7,47.4,27.7).lineTo(48.5,30.7).curveTo(49.6,33.9,50.9,36.3).lineTo(41.6,38.2).curveTo(35.1,39.6,18.7,39.8).lineTo(14.9,39.9).curveTo(7.4,39.9,1.9,39.5).closePath();
	this.shape_8.setTransform(78.6,39.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#000000").beginStroke().moveTo(-25.8,80.7).curveTo(-31.9,79.6,-32.6,78.5).curveTo(-33.4,77.4,-33.4,75.2).curveTo(-33.4,70.1,-28.2,66.5).curveTo(-30,63.8,-32.7,58.3).curveTo(-37.9,47.3,-37.9,47.5).lineTo(-37.9,46.9).curveTo(-53.9,42.3,-69.4,33.9).lineTo(-69.5,34).curveTo(-70.9,33.5,-71.4,32.8).lineTo(-71.7,32.6).lineTo(-71.7,32.3).lineTo(-71.7,30.7).curveTo(-71.7,28.6,-66.8,18.1).curveTo(-70.7,17,-73.1,15.1).curveTo(-77.4,11.6,-78.5,4).curveTo(-79.3,-0.9,-78.4,-3.7).curveTo(-77.8,-5.5,-75.8,-8.3).curveTo(-70.8,-15.4,-69.3,-26.5).curveTo(-68.6,-32,-67.6,-36.7).lineTo(-65.4,-46.7).lineTo(-65.3,-49.4).curveTo(-66.2,-49.7,-66.7,-50.4).curveTo(-67.2,-51.1,-67.3,-52.4).lineTo(-67.2,-53.8).lineTo(-67.2,-53.9).curveTo(-66.4,-63.1,-52.8,-71).curveTo(-40.1,-78.5,-23.3,-81).curveTo(-23.2,-81,-23,-80.4).lineTo(-22.9,-80.3).lineTo(-22.6,-79.7).curveTo(-22.5,-79.2,-22.8,-79).curveTo(-23.6,-78.4,-29.3,-76.4).lineTo(-41.6,-72.2).curveTo(-49,-69.7,-53.5,-66.6).curveTo(-57.1,-64.1,-60.2,-60.5).curveTo(-64,-56.1,-63.7,-53.4).curveTo(-63.5,-52.5,-61,-51.6).lineTo(-55.3,-49.6).lineTo(-51.8,-48.1).curveTo(-50.8,-47.6,-49.7,-46.5).lineTo(-45.6,-42.4).lineTo(-45.5,-42.2).lineTo(-43.9,-42.4).lineTo(-43,-43.9).curveTo(-41.4,-46,-40.4,-45.3).curveTo(-39.7,-44.8,-39.4,-43).lineTo(-39.4,-42.3).lineTo(-39.3,-41.4).lineTo(-39.3,-41.2).lineTo(-39.3,-40.2).curveTo(-39.3,-37.2,-39.8,-34.5).lineTo(-41.2,-26.3).curveTo(-42,-20.5,-43.4,-16.6).curveTo(-44,-14.8,-44.6,-13.9).curveTo(-36.1,-12.2,-26.1,-11).curveTo(-13.2,-9.6,-5.6,-8.6).curveTo(-2.6,-8.2,1.6,-8).curveTo(8.5,-7.5,18.4,-7.6).curveTo(34.8,-7.9,41.3,-9.2).lineTo(50.6,-11.2).curveTo(49.3,-13.6,48.2,-16.8).lineTo(47.1,-19.8).curveTo(46.7,-20.8,46.6,-22.1).lineTo(45.4,-21.5).curveTo(44.5,-21.1,44,-21.1).curveTo(42.1,-21.1,41.7,-21.5).lineTo(41.5,-21.8).curveTo(41.3,-22.1,41.3,-23.2).curveTo(42.9,-24.5,44.3,-26.7).curveTo(47.1,-30.9,46.6,-36.8).curveTo(46,-43,41.7,-48.9).curveTo(37.6,-54.7,37.6,-55.2).curveTo(37.6,-58.5,39.7,-58.2).curveTo(41.6,-57.7,44.3,-54.5).curveTo(46.9,-51.4,48.4,-48.3).curveTo(49.8,-49.2,52.3,-50.4).curveTo(53.6,-51.1,54.7,-51.4).curveTo(54.1,-52.8,53.4,-54.9).curveTo(51.2,-61.3,49.9,-62.6).curveTo(47.9,-64.6,41.4,-68.7).curveTo(35.9,-72.2,35.9,-72.9).curveTo(35.9,-74.1,37.1,-74.2).curveTo(37.9,-74.3,39.4,-73.8).curveTo(42.8,-72.8,47,-69.7).curveTo(58,-61.4,58,-51.7).lineTo(58,-51).lineTo(58,-50.5).lineTo(57.9,-49.9).lineTo(57.8,-49.7).lineTo(57.6,-49.5).lineTo(57.5,-49.4).lineTo(56.4,-48.9).curveTo(55.6,-48.7,55.8,-48.1).curveTo(58.2,-41.8,61.1,-35.5).curveTo(63.2,-31,64.7,-26).curveTo(66.6,-19.4,69.3,-15.9).curveTo(70.7,-14,74,-11.6).curveTo(76.5,-9.6,77.5,-7.5).curveTo(78.9,-4.3,78.9,2).curveTo(78.9,5.3,75.9,8.5).curveTo(72.2,12.4,66.2,12.8).lineTo(65.4,12.9).lineTo(65.7,14).curveTo(70.7,29.6,70.7,35.4).lineTo(70.6,36.4).curveTo(70,37.6,69.5,38).curveTo(69,38.5,68.4,38.5).lineTo(44,45.6).lineTo(39.7,46.8).curveTo(39.6,49.3,37.9,55.2).curveTo(36.2,61.1,34.1,63.8).lineTo(36,64.7).curveTo(42.3,68.3,42,73.6).curveTo(41.9,75.1,41.4,75.6).curveTo(40.9,76.4,39,76.8).curveTo(37.6,77.2,21.6,79.2).curveTo(15,79.9,9.5,79.2).curveTo(7.3,78.7,7.2,78).lineTo(6.9,76.5).lineTo(6.5,77.1).curveTo(4.4,79.5,-4.7,80.7).curveTo(-10.5,81,-15.8,81).curveTo(-21,81,-25.8,80.7).closePath().moveTo(-22.2,68.6).lineTo(-23.8,69).lineTo(-24.4,69.2).lineTo(-25.8,69.8).curveTo(-27.3,70.7,-27.9,72.1).lineTo(-28.7,74.6).curveTo(-28,74.9,-22.7,75.3).curveTo(-17.8,75.8,-16.1,75.7).curveTo(-12.7,75.8,-9.8,75.6).curveTo(-3.5,75.2,-0.1,74.1).curveTo(1.5,73.5,2.3,72.9).lineTo(2.8,72.4).curveTo(2.8,70.7,1.3,69.2).curveTo(-0.3,67.6,-2.3,67.4).lineTo(-4.3,67.4).curveTo(-16.8,67.4,-22.2,68.6).closePath().moveTo(11.8,66.7).curveTo(10.3,67.2,9.3,68).curveTo(8.8,68.3,8,69.3).curveTo(7.5,70.1,6.9,70.1).curveTo(7.3,71.3,7.3,72.7).curveTo(7.3,74.4,7.1,75.4).curveTo(7.8,75.3,9.8,75.2).lineTo(17,74.6).curveTo(27,73.8,32.4,73.2).curveTo(35.7,72.8,35.4,72).curveTo(34.5,69.4,31,67.7).curveTo(26.9,65.9,19.7,65.9).lineTo(19.1,65.9).curveTo(14.5,65.9,11.8,66.7).closePath().moveTo(11.4,51.6).curveTo(7.2,51.9,3,52).curveTo(3.8,53.4,4.2,55.7).curveTo(4.7,58.9,4.3,62.9).curveTo(4.3,65,4.1,65.7).lineTo(4.2,65.7).curveTo(5.5,63.7,9.5,62.4).curveTo(11.1,61.9,12.7,61.6).curveTo(17.2,60.7,22.8,61.2).curveTo(26.7,61.4,30,62.3).curveTo(30.2,60.4,31.6,56.7).curveTo(33.5,51.5,33.7,50.4).curveTo(34,49.2,34.5,48.1).curveTo(23,50.7,11.4,51.6).closePath().moveTo(-30.3,51.2).curveTo(-27.1,57,-24,64.2).curveTo(-21.1,63,-17.6,62.4).curveTo(-16,62.1,-14.2,62).curveTo(-10.6,61.7,-6.7,62).curveTo(-2.6,62.4,-0.2,63.2).lineTo(-0.7,58.9).lineTo(-1.4,53.8).curveTo(-1.6,53,-1.6,52.1).curveTo(-11.6,52,-21.3,50.5).curveTo(-26.7,49.7,-31.9,48.5).lineTo(-30.3,51.2).closePath().moveTo(-62.7,21.5).curveTo(-66.5,29.4,-68,31.8).lineTo(-66.5,32.2).lineTo(-52,37.5).curveTo(-44.4,40.2,-37,42.2).curveTo(-18.1,47.2,-0.9,47.1).curveTo(4,47.1,19.5,45.4).curveTo(33.7,43.8,36,43.3).curveTo(51.9,39.5,62.8,36.1).lineTo(64.4,35.8).curveTo(66,35.3,67.7,34.3).curveTo(66.7,32.4,62.8,20.8).lineTo(53.5,-6.9).curveTo(51.6,-6.3,48.6,-5.7).curveTo(39.9,-4.1,21.1,-3.5).curveTo(3.5,-3,-5.5,-3.7).curveTo(-24.2,-5.5,-30.4,-6.3).lineTo(-30.9,-6.4).curveTo(-42.5,-8,-46.5,-10.6).curveTo(-47.2,-8.9,-48.7,-5.7).curveTo(-47.8,-3.8,-45.8,-0.1).curveTo(-45,1.4,-43.9,4.7).curveTo(-42.3,9.6,-42.8,11.8).curveTo(-43.3,13.7,-45.7,15.9).curveTo(-47.4,17.3,-48.4,17.8).lineTo(-49.4,18.3).curveTo(-52.7,19.8,-57,19.6).curveTo(-59.5,19.5,-61.6,19.2).lineTo(-62.7,21.5).closePath().moveTo(-63.6,-37.7).curveTo(-65.5,-29.2,-65.7,-26.7).curveTo(-66.5,-15.5,-71.1,-7.3).curveTo(-72.1,-5.3,-73.8,-2.6).curveTo(-74.9,-0.5,-74.6,0.8).curveTo(-73.1,7.3,-68.9,11.3).curveTo(-66.9,13.3,-64.7,14.3).lineTo(-63.3,14.7).curveTo(-62.3,15.1,-61.2,15.2).lineTo(-59.7,15.3).curveTo(-51.4,15.9,-48.2,12.8).lineTo(-47.5,12.2).curveTo(-46,10.3,-46.5,8.2).lineTo(-46.6,8.1).curveTo(-46.9,7,-50.2,0.8).lineTo(-51.1,-0.9).curveTo(-53.8,-5.9,-54,-6.7).lineTo(-54,-6.9).curveTo(-54.3,-8.9,-51.9,-20).curveTo(-49.6,-31,-47,-37.3).lineTo(-46.2,-39.3).lineTo(-47.4,-40.2).curveTo(-52,-43.7,-54.7,-44.7).lineTo(-56.6,-45.4).curveTo(-58,-46,-59.1,-46.1).curveTo(-59.6,-46.2,-60.4,-46.6).lineTo(-61.6,-47).lineTo(-63.6,-37.7).closePath().moveTo(55.9,-10.5).curveTo(56.9,-8.9,56.7,-8.7).curveTo(60.1,-2.8,64.1,8.9).curveTo(64.4,8.7,64.9,8.4).curveTo(65.5,8.2,68.8,7.3).curveTo(74.4,5.8,74.4,1.4).curveTo(74.4,-5.4,68.7,-10.9).curveTo(65.2,-14.3,64.2,-15.6).curveTo(61.7,-18.7,60.7,-22.5).curveTo(60.5,-23.8,60.1,-25.1).curveTo(59.4,-27.3,57.1,-32.7).curveTo(52.7,-42.9,52,-46.1).lineTo(49.7,-45.2).curveTo(50.4,-43,51.1,-36.3).curveTo(51.4,-33.6,51,-30.6).curveTo(50.8,-28.5,50.1,-25).curveTo(50,-24.7,50.5,-23.5).lineTo(52.1,-19.8).curveTo(52.8,-18,53.3,-15.5).curveTo(53.8,-13.2,54,-12.7).lineTo(54.3,-12).lineTo(54.4,-11.9).lineTo(54.5,-12).curveTo(54.9,-12,55.9,-10.5).closePath().moveTo(-47.2,-22.7).curveTo(-49.6,-17.2,-49.9,-12.8).curveTo(-48.8,-14.1,-48,-14.5).lineTo(-48.3,-15).curveTo(-48.4,-15.2,-48.4,-16.5).curveTo(-48.4,-17.3,-47.4,-20.9).lineTo(-46.5,-24.2).lineTo(-47.2,-22.7).closePath().moveTo(37.3,-73.8).lineTo(37.1,-74.2).lineTo(37.3,-73.8).closePath().moveTo(-25,-79).curveTo(-24.1,-79.7,-22.9,-80.3).curveTo(-24.1,-79.7,-25,-79).closePath();
	this.shape_9.setTransform(78.9,87.4);

	this.addChild(this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.Jason = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(-18.5,5.1).curveTo(-19.7,3.9,-19.6,2.7).curveTo(-19.6,2.2,-19.5,1.8).lineTo(-25.8,1.9).curveTo(-27.7,2.2,-30.2,3.1).curveTo(-32.2,3.7,-33.1,3.7).curveTo(-34.3,3.7,-35.2,3.5).lineTo(-35.7,3.4).lineTo(-36.1,1.6).lineTo(-35.6,0.4).curveTo(-35.4,-0.3,-34.8,-0.7).curveTo(-33,-1.9,-25.6,-2.6).curveTo(-19.6,-4.3,-6.9,-3).curveTo(-4.7,-1.1,-4.7,-0.6).curveTo(-4.7,-0.4,-5.6,0.4).curveTo(-6.3,1.2,-6.9,1.5).lineTo(-10.9,1.6).curveTo(-10.7,2.1,-10.6,2.8).curveTo(-10.6,4.9,-12,5.9).curveTo(-13,6.7,-14.4,6.7).curveTo(-17,6.7,-18.5,5.1).closePath().moveTo(23,1.7).curveTo(20.8,1.3,20,0.5).curveTo(19.7,0.2,19.6,-0.1).lineTo(19.5,-0.4).lineTo(19.6,-1.2).lineTo(19.2,-1.2).curveTo(18.1,-1.4,17.1,-1.8).lineTo(16.4,-2.1).lineTo(16.3,-2.4).lineTo(16.2,-2.5).curveTo(16.1,-2.8,16.3,-3.8).curveTo(16.8,-7.1,25.9,-6.7).curveTo(30,-6.5,35.6,-5.6).lineTo(35.8,-5.6).lineTo(35.9,-5).curveTo(36.1,-4.6,35.9,-3.5).curveTo(35.8,-2.7,30.2,-1.6).lineTo(28.2,-1.2).lineTo(27.9,-1.1).lineTo(27.9,-1).curveTo(27.4,1.9,24.4,1.9).lineTo(23,1.7).closePath();
	this.shape.setTransform(96.2,87.3);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#D3D37E").beginStroke().moveTo(9.8,42.5).curveTo(5.5,42.1,1.8,39.8).lineTo(1.8,39.7).curveTo(1.8,38.1,-0.9,36.2).curveTo(-3.2,34.6,-6.8,33.5).curveTo(-8.5,33,-9.8,33).curveTo(-13.6,11.1,-13.6,10.1).curveTo(-13.6,2.9,-12.1,-5.7).curveTo(-9.9,-18.3,-5.2,-28.2).curveTo(-1,-36.8,4.8,-42.6).curveTo(9.1,-37,12.4,-31.6).lineTo(13.6,-29.4).lineTo(13.5,-29.3).lineTo(13.1,-28.7).curveTo(9.3,-23.8,7.5,-14.5).curveTo(5.5,-4,7.3,12.4).curveTo(9.2,28,13.3,40.3).curveTo(14.1,42.6,11,42.6).lineTo(9.8,42.5).closePath();
	this.shape_1.setTransform(19.9,58.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#FFFF99").beginStroke().moveTo(4,19.3).curveTo(-24.9,11.8,-33.1,3.3).curveTo(-37,-0.6,-40.3,-1.5).curveTo(-43.2,-2.3,-45.1,-0.8).lineTo(-46.3,-3).curveTo(-49.5,-8.4,-53.9,-14).curveTo(-40.5,-27.3,-18.6,-25.2).lineTo(7,-25.2).curveTo(27,-20.8,38.4,-10.8).curveTo(44.6,-5.4,49.7,6.7).curveTo(52.3,12.7,53.6,17.8).lineTo(53.9,18.7).curveTo(51.7,21.7,51.5,21.8).curveTo(47.5,23.4,40.1,25.2).curveTo(38.8,25.5,36.7,25.5).curveTo(27.5,25.5,4,19.3).closePath();
	this.shape_2.setTransform(78.5,30.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#EACFB7").beginStroke().moveTo(-19.6,4.8).lineTo(-20.7,4.7).curveTo(-20.7,4.5,-21,4.2).curveTo(-21.3,4,-21.3,2.2).lineTo(-33.5,3.2).curveTo(-34.9,3.3,-36,3.2).lineTo(-36.7,3).curveTo(-36.7,2.9,-36.7,2.9).curveTo(-36.7,2.9,-36.8,2.9).curveTo(-36.8,2.9,-36.9,2.9).curveTo(-36.9,3,-37,3).curveTo(-37.2,3.2,-37.3,1.8).curveTo(-37.4,0.4,-37,-0).curveTo(-36.2,-0.8,-33.3,-1.3).curveTo(-28.9,-2.1,-7.2,-3.8).lineTo(-2.9,-4.1).curveTo(-2.9,-4,4.5,-4.2).lineTo(19.2,-4.7).curveTo(24.2,-4.9,27.8,-4.8).curveTo(32.9,-4.6,35.1,-3.8).curveTo(37.3,-3,37.3,-1.3).curveTo(37.3,0.2,36.1,0.5).lineTo(34,0.5).lineTo(32.6,0.5).curveTo(32.5,2.9,32.2,3.6).lineTo(29.4,4.1).curveTo(24.9,4,23.7,2.4).curveTo(23.4,2,23.3,-0).lineTo(20,-0.4).curveTo(-6.2,0.6,-7.2,0.1).lineTo(-7.2,0.1).lineTo(-12.1,1).curveTo(-12.2,2.5,-13.4,3.5).curveTo(-14.7,4.7,-16.7,4.8).lineTo(-18.1,4.9).lineTo(-19.6,4.8).closePath();
	this.shape_3.setTransform(94.6,87.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#DEBBA4").beginStroke().moveTo(21.4,58.2).curveTo(14.1,55.4,6.6,48).curveTo(3.7,45.1,1.7,40.7).lineTo(-1,34.9).lineTo(0.3,34.2).curveTo(2.2,33.1,2.2,30.7).curveTo(2.2,29.6,2,29).curveTo(1.3,27.6,-0.7,27.6).curveTo(-1.8,27.6,-5.2,28.7).curveTo(-8.5,29.8,-9.7,29.8).curveTo(-13.6,29.8,-17.3,27.8).curveTo(-22.7,24.9,-26.2,18.4).curveTo(-26.5,17.8,-26.4,16.8).lineTo(-26.3,14.9).curveTo(-26.4,11.2,-23.7,9.3).curveTo(-22.7,8.6,-22,7.8).curveTo(-18.9,8.5,-16.2,10).curveTo(-14.2,11.2,-11.7,11.2).curveTo(-10.6,11.2,-10,10.9).curveTo(-9.1,10.5,-9.1,9.1).curveTo(-5.4,11.5,-1.1,11.9).curveTo(3.3,12.4,2.4,9.7).curveTo(-1.7,-2.6,-3.6,-18.3).curveTo(-5.4,-34.7,-3.4,-45.1).curveTo(-1.6,-54.4,2.2,-59.4).lineTo(2.6,-59.9).curveTo(13.9,-44.9,16,-27.2).curveTo(17.3,-16.4,18,8.8).curveTo(19.9,24.3,22.4,37.6).curveTo(24.9,50.7,26.5,59.9).curveTo(23.7,59.1,21.4,58.2).closePath();
	this.shape_4.setTransform(30.8,89.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#000000").beginStroke().moveTo(-1.1,77.7).curveTo(-7.2,76.8,-11.6,75.5).curveTo(-22.3,72.3,-29.7,67.2).curveTo(-41.3,59.1,-44.9,46.1).lineTo(-44.9,46).lineTo(-48.2,46.1).curveTo(-58.3,46.1,-64,39.4).curveTo(-68.1,34.6,-68.3,29.9).lineTo(-69.2,29.5).curveTo(-69.4,27.4,-69.4,25.1).curveTo(-69.4,20.7,-69,18.8).curveTo(-68,14.7,-64.7,13.7).curveTo(-65.9,10.7,-66.9,-0).lineTo(-67.7,-10.3).curveTo(-67.7,-33.1,-58.9,-51.3).curveTo(-54.4,-60.7,-48,-66.9).curveTo(-33.5,-81,-9.4,-78.7).lineTo(-9.4,-79.1).lineTo(15.3,-79.1).curveTo(41.2,-74.5,54.6,-60.4).curveTo(60.6,-54,64.1,-44.2).curveTo(66.3,-38.2,67.3,-32.8).curveTo(66.9,-32.9,68.1,-28.5).curveTo(69.4,-23.6,69.4,-12.5).curveTo(69.5,0.1,67.4,6.2).curveTo(67,7.3,67.2,11.2).curveTo(67.5,15.4,67.4,16.1).curveTo(66.5,21.9,65.3,26.5).curveTo(63.5,33.6,59.9,42).curveTo(59.6,42.6,59.1,45.3).lineTo(58.6,48.9).curveTo(57.7,60.7,50.3,69.1).curveTo(42.6,77.7,31.5,78.8).lineTo(10.8,79.1).curveTo(2.8,78.3,-1.1,77.7).closePath().moveTo(-36.7,39.2).curveTo(-36.5,39.8,-36.5,41).curveTo(-36.5,43.3,-38.4,44.5).lineTo(-39.7,45.1).lineTo(-37,50.9).curveTo(-35,55.4,-32.1,58.2).curveTo(-24.6,65.6,-17.3,68.4).curveTo(-15,69.3,-12.2,70.1).curveTo(1,73.8,23.3,73.7).curveTo(33.5,73.6,41.1,68.9).curveTo(51.2,62.6,53,49.3).lineTo(53,49.1).curveTo(49.9,49.5,45.9,50.5).lineTo(45.8,50.6).curveTo(41.4,51.7,38.1,52.2).curveTo(30.5,53.5,21.9,53.2).curveTo(16.2,53,7.2,51.6).lineTo(0.8,49.3).curveTo(0.8,46.3,6,47).curveTo(20.8,49.4,36.6,47.1).curveTo(41.3,46.4,44.9,45.4).curveTo(48.4,44.5,52.9,42.8).lineTo(53,42.8).lineTo(53.3,42.5).curveTo(53.9,41.9,54.7,40.1).lineTo(58.8,31.1).curveTo(60.9,26,61.8,21.3).curveTo(62.5,17.1,62.5,11.3).curveTo(62.5,9.9,62.7,9).curveTo(62.5,8.4,62.5,7.7).curveTo(63.4,-0.2,64,-8.2).curveTo(64.9,-22.9,63,-30.4).lineTo(62.7,-31.2).curveTo(61.4,-36.3,58.8,-42.3).curveTo(53.7,-54.4,47.5,-59.8).curveTo(36.1,-69.8,16.1,-74.2).lineTo(-9.5,-74.2).curveTo(-31.4,-76.3,-44.8,-63).curveTo(-50.6,-57.2,-54.7,-48.6).curveTo(-59.5,-38.7,-61.7,-26.1).curveTo(-63.2,-17.5,-63.2,-10.3).curveTo(-63.2,-9.3,-59.3,12.6).curveTo(-58.1,12.6,-56.4,13.1).curveTo(-52.8,14.2,-50.5,15.8).curveTo(-47.8,17.7,-47.8,19.3).lineTo(-47.8,19.4).curveTo(-47.8,20.7,-48.7,21.1).curveTo(-49.3,21.4,-50.4,21.4).curveTo(-52.9,21.4,-54.9,20.3).curveTo(-57.6,18.8,-60.7,18).curveTo(-61.4,18.8,-62.4,19.5).curveTo(-65,21.4,-65,25.1).lineTo(-65.1,27).curveTo(-65.1,28,-64.9,28.6).curveTo(-61.3,35.1,-56,38).curveTo(-52.3,40,-48.3,40).curveTo(-47.1,40,-43.8,38.9).curveTo(-40.5,37.8,-39.3,37.8).curveTo(-37.3,37.8,-36.7,39.2).closePath().moveTo(17.8,33.5).curveTo(16.7,32.4,16.6,30.9).lineTo(16.7,28.9).curveTo(16.7,27.2,17.7,25.1).curveTo(18.9,22.7,20.4,22.8).curveTo(22,23,22.1,24.7).lineTo(22,28.9).lineTo(27.4,31.4).lineTo(41.2,31.4).curveTo(43,30.7,43.5,29.2).curveTo(43.8,28.3,43.8,25.4).curveTo(43.7,21.2,46.2,22.6).curveTo(48.8,24.1,48.8,27.9).curveTo(48.8,31.8,47.2,33.5).curveTo(44.1,36.8,34.4,36.8).curveTo(21.3,36.8,17.8,33.5).closePath();
	this.shape_5.setTransform(69.4,79.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-26.3,58.7).curveTo(-27.8,49.5,-30.3,36.3).curveTo(-32.8,23.1,-34.8,7.6).curveTo(-35.4,-17.6,-36.8,-28.4).curveTo(-38.9,-46.1,-50.2,-61.1).lineTo(-50.1,-61.3).curveTo(-48.2,-62.8,-45.3,-62).curveTo(-42,-61.1,-38.2,-57.1).curveTo(-29.9,-48.7,-1,-41.1).curveTo(27.8,-33.6,35.1,-35.3).curveTo(42.5,-37,46.5,-38.7).curveTo(46.7,-38.7,48.9,-41.8).curveTo(50.8,-34.4,49.9,-19.6).curveTo(49.3,-11.7,48.4,-3.7).curveTo(48.4,-3.1,48.6,-2.4).curveTo(48.4,-1.5,48.4,-0.1).curveTo(48.4,5.6,47.7,9.8).curveTo(46.8,14.6,44.7,19.6).lineTo(40.6,28.6).curveTo(39.8,30.5,39.2,31.1).lineTo(38.9,31.3).lineTo(38.8,31.3).curveTo(34.3,33.1,30.8,34).curveTo(27.2,34.9,22.5,35.6).curveTo(6.7,38,-8.1,35.6).curveTo(-13.3,34.9,-13.3,37.8).lineTo(-6.9,40.1).curveTo(2.1,41.5,7.8,41.7).curveTo(16.4,42,24,40.8).curveTo(27.3,40.2,31.7,39.1).lineTo(31.8,39.1).curveTo(35.8,38.1,38.9,37.6).lineTo(38.9,37.8).curveTo(37.1,51.1,27,57.5).curveTo(19.4,62.2,9.2,62.2).lineTo(8,62.2).curveTo(-13.4,62.2,-26.3,58.7).closePath().moveTo(3.6,13.7).curveTo(2.6,15.8,2.6,17.4).lineTo(2.5,19.4).curveTo(2.6,21,3.7,22.1).curveTo(7.2,25.4,20.3,25.4).curveTo(30,25.4,33.1,22.1).curveTo(34.7,20.3,34.7,16.5).curveTo(34.7,12.6,32.1,11.2).curveTo(29.6,9.7,29.7,14).curveTo(29.7,16.8,29.4,17.8).curveTo(28.9,19.3,27.1,19.9).lineTo(13.3,19.9).lineTo(7.9,17.4).lineTo(8,13.2).curveTo(7.9,11.6,6.3,11.4).lineTo(6.1,11.4).curveTo(4.7,11.4,3.6,13.7).closePath().moveTo(-10,1.1).curveTo(-9.6,1.4,-9.6,1.6).lineTo(-8.5,1.7).curveTo(-7.1,1.8,-5.6,1.7).curveTo(-3.6,1.6,-2.3,0.4).curveTo(-1.2,-0.6,-1.1,-2.1).lineTo(3.8,-3).lineTo(3.9,-3).curveTo(4.9,-2.5,31.1,-3.5).lineTo(34.4,-3.1).curveTo(34.5,-1.1,34.8,-0.7).curveTo(36,0.9,40.4,1).lineTo(43.3,0.5).curveTo(43.5,-0.2,43.6,-2.6).lineTo(45,-2.6).lineTo(47.2,-2.6).curveTo(48.4,-2.9,48.4,-4.4).curveTo(48.4,-6.1,46.1,-6.9).curveTo(43.9,-7.7,38.9,-7.9).curveTo(35.3,-8,30.2,-7.8).lineTo(15.5,-7.3).curveTo(8.1,-7.1,8.1,-7.2).lineTo(3.9,-6.9).curveTo(-17.8,-5.2,-22.3,-4.4).curveTo(-25.2,-3.9,-25.9,-3.1).curveTo(-26.4,-2.7,-26.3,-1.3).curveTo(-26.2,0.1,-25.9,-0.1).curveTo(-25.9,-0.1,-25.8,-0.1).curveTo(-25.8,-0.2,-25.7,-0.2).curveTo(-25.7,-0.2,-25.7,-0.2).curveTo(-25.7,-0.1,-25.7,-0.1).lineTo(-24.9,0.1).curveTo(-23.9,0.2,-22.4,0.1).lineTo(-10.3,-0.9).curveTo(-10.3,0.9,-10,1.1).closePath();
	this.shape_6.setTransform(83.5,90.6);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,138.9,158.3);


(lib.JaneHead = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#7A4D30").beginStroke().moveTo(14.4,47.8).curveTo(3.7,43.3,-8.8,37.2).lineTo(-7.7,34.6).curveTo(-5.8,30,-5.8,27.9).curveTo(-5.8,27.6,-8.2,25.4).curveTo(-11.1,26.9,-11.7,30.5).curveTo(-12.4,34.1,-13.7,34.9).curveTo(-33.4,22.4,-33.4,19).curveTo(-33.4,17.7,-31.8,15.2).curveTo(-30.3,12.8,-30.3,12).curveTo(-30.3,10.9,-30.6,10.5).lineTo(-30.9,10).curveTo(-33.1,10.5,-34.6,11.2).curveTo(-35,8.8,-35,4.3).curveTo(-35,-4.2,-31.4,-14.8).curveTo(-27.4,-26.9,-20,-36.8).curveTo(-11.8,-48,-1.1,-54.4).curveTo(3.1,-45.7,6.7,-37.7).curveTo(16.1,-16.5,18.1,-4.7).curveTo(10.2,-2.9,10.2,4.8).curveTo(10.2,6,10.5,6.3).lineTo(10.8,6.6).curveTo(12.3,6.8,14.3,6.6).lineTo(18.3,5.8).lineTo(19.4,5.6).curveTo(21.1,18,24.2,28).curveTo(27.6,39.2,35,54.3).curveTo(25.2,52.2,14.4,47.8).closePath();
	this.shape.setTransform(38.2,62.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#CC0066").beginStroke().moveTo(-4,1.2).lineTo(-9.7,-1.9).curveTo(-2.9,-3.2,9.7,-0.2).lineTo(6.9,1.2).curveTo(4,2.3,1.3,2.3).curveTo(-1.4,2.3,-4,1.2).closePath();
	this.shape_1.setTransform(89.7,104.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(22.3,33.5).curveTo(0.5,33.5,-22,22.5).curveTo(-30,18.6,-35.8,14.4).curveTo(-38.6,12.4,-39.9,11.1).curveTo(-41,12.2,-42.1,12.2).curveTo(-50.1,12.2,-59.5,2.5).curveTo(-63,-1,-65.5,-4.7).curveTo(-67.8,-8,-67.8,-9.1).curveTo(-67.8,-13.6,-67.4,-14.4).curveTo(-66.7,-15.8,-64.2,-16.9).curveTo(-62.7,-17.6,-60.5,-18.2).lineTo(-60.2,-17.6).curveTo(-59.9,-17.2,-59.9,-16.1).curveTo(-59.9,-15.3,-61.5,-12.9).curveTo(-63,-10.5,-63,-9.1).curveTo(-63,-5.7,-43.3,6.7).curveTo(-42,6,-41.4,2.4).curveTo(-40.7,-1.3,-37.8,-2.8).curveTo(-35.4,-0.6,-35.4,-0.3).curveTo(-35.4,1.9,-37.3,6.5).lineTo(-38.4,9.1).curveTo(-25.9,15.2,-15.2,19.6).curveTo(-4.4,24,5.4,26.2).curveTo(14.4,28.3,22.6,28.4).curveTo(33.6,28.7,42.8,20.5).curveTo(45.1,18.4,49.6,13.5).curveTo(53.6,9.2,55.8,7.5).curveTo(56.3,7.2,56.7,7.1).curveTo(57.1,7,57.5,7.2).curveTo(57.8,7.5,58,7.8).lineTo(58.3,8.4).curveTo(58.6,8.8,59.3,8.2).curveTo(60,7.6,56.5,13.1).curveTo(52.9,18.6,43.9,26.1).curveTo(35,33.5,22.3,33.5).lineTo(22.3,33.5).closePath().moveTo(9.2,16.9).curveTo(3,12.8,3,10.9).curveTo(3,6.2,14.1,6.5).curveTo(18.2,6.6,38.9,9).curveTo(41.3,9.3,41.3,11.8).curveTo(41.3,13.7,36.5,17.3).curveTo(30.6,21.7,23.5,21.7).curveTo(16.4,21.7,9.2,16.9).closePath().moveTo(12.1,11.9).lineTo(17.9,15).curveTo(23,17.2,28.8,15).lineTo(31.6,13.6).curveTo(22.7,11.5,16.7,11.5).curveTo(14.2,11.5,12.1,11.9).closePath().moveTo(21.5,1.9).curveTo(15.5,1,13.6,-1.2).curveTo(12.8,-2.2,12.8,-4.5).curveTo(12.8,-9.6,16.5,-7.2).curveTo(20.8,-3.5,21.9,-3.2).curveTo(28,-1.6,34.1,-3.2).lineTo(34.1,-5).lineTo(34.9,-5.5).curveTo(35.7,-6,36.9,-6).curveTo(38.1,-6,38.7,-5.8).lineTo(39.1,-5.6).lineTo(39.4,-5).curveTo(39.7,-4.6,39.7,-3.5).curveTo(39.7,-2.1,38.1,-0.4).lineTo(35.7,1.9).curveTo(32.4,2.5,28.9,2.5).curveTo(25.3,2.5,21.5,1.9).closePath().moveTo(52.7,-18.4).curveTo(51.3,-18.5,49.4,-19.5).curveTo(47.3,-20.6,45.8,-22.2).curveTo(44.4,-23.7,43.5,-24.2).curveTo(41.8,-24.9,39,-24.4).curveTo(37.7,-24.2,36.7,-23.7).curveTo(36,-24,35.5,-24.3).lineTo(35.3,-24.8).lineTo(35,-25.2).lineTo(34.9,-25.5).lineTo(35.1,-26).curveTo(37.8,-30.2,48.3,-30).curveTo(57.9,-29.8,63.6,-26.7).curveTo(65.6,-25.6,66.9,-23.9).curveTo(67.9,-22.5,67.8,-21.6).lineTo(67.8,-21).lineTo(67.7,-20.9).lineTo(67.7,-20.9).curveTo(66.6,-21.5,64.9,-21.8).lineTo(63.5,-21.9).lineTo(62.8,-21.9).curveTo(61.8,-21.9,61.2,-21.6).lineTo(59.7,-21).curveTo(59.2,-20.8,57.7,-19.8).curveTo(56.4,-18.9,55.5,-18.7).curveTo(54.3,-18.4,53.4,-18.4).lineTo(52.7,-18.4).closePath().moveTo(-6.9,-22.4).curveTo(-7.7,-22.8,-10.2,-22.5).lineTo(-11.3,-22.3).lineTo(-15.3,-21.5).curveTo(-17.3,-21.3,-18.8,-21.5).lineTo(-19.1,-21.9).curveTo(-19.4,-22.1,-19.4,-23.3).curveTo(-19.5,-31,-11.5,-32.9).curveTo(-8.2,-33.7,-3.4,-33.5).curveTo(7.9,-32.9,23.1,-27.2).curveTo(24.5,-26.7,22.9,-25.7).curveTo(21.4,-24.7,19,-24.5).lineTo(16.3,-24.4).lineTo(13.1,-24.5).curveTo(10.2,-21.1,5.5,-20.6).curveTo(3.8,-20.4,2.3,-20.4).curveTo(-2.9,-20.4,-6.9,-22.4).closePath().moveTo(6.9,-25.6).lineTo(7.3,-25.7).lineTo(6.9,-25.7).lineTo(6.9,-25.6).closePath();
	this.shape_2.setTransform(67.8,90.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-7.8,37.2).curveTo(-5,33.6,-4.1,31.7).lineTo(-2.4,28.4).curveTo(0.1,22.4,1.7,16).curveTo(2.7,12,3,6.4).curveTo(3.1,3.8,3.1,-3.4).curveTo(3,-9.5,3.1,-12.4).curveTo(3.2,-13.4,1.1,-24.2).curveTo(-1,-35.1,-1,-36.3).curveTo(-1,-38.2,-0.2,-38.4).curveTo(0.3,-38.5,0.6,-37.7).curveTo(0.9,-38.1,1.6,-37).curveTo(2.2,-36.1,4.1,-26.2).curveTo(5.8,-17.5,6.4,-13.4).lineTo(7.1,-8.1).curveTo(7.9,-2.1,7.7,1.6).curveTo(7.4,12.2,5.1,19.7).curveTo(3.7,24.8,2.3,28.2).curveTo(0.3,33,-2.8,37.9).curveTo(-3.1,38.4,-3.9,38.4).curveTo(-5.2,38.4,-7.8,37.2).closePath();
	this.shape_3.setTransform(128.5,63.4);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#975F3C").beginStroke().moveTo(-36.9,19.9).curveTo(-44.1,2.3,-46.8,-16.1).curveTo(-46.8,-30.1,-41.7,-16.4).curveTo(-39.5,-10.5,-33.6,10.1).curveTo(-32.1,13.3,-29.8,20.3).curveTo(-27.4,27.5,-25.8,30.9).curveTo(-24.4,34,-22.6,36.6).lineTo(-24.4,25.7).curveTo(-25.8,16.8,-28.8,10.1).curveTo(-32.7,1.3,-36.8,-12.2).curveTo(-40.6,-25.1,-40.6,-28.4).curveTo(-40.6,-28.8,-40.4,-29.3).curveTo(-41.7,-29.6,-42.3,-30).curveTo(-43.8,-30.9,-43.8,-33.6).curveTo(-43.8,-34.4,-42.7,-35).lineTo(-42.8,-35.6).curveTo(-42.8,-36.6,-42.4,-37.3).lineTo(-47.8,-37.6).lineTo(-50.3,-40).curveTo(-50.9,-40.7,-50.9,-40.5).curveTo(-50.9,-44.1,-35.4,-43.7).curveTo(-32.3,-43.6,-28.8,-45.6).curveTo(-27.7,-46.2,-22.5,-50).curveTo(-14.1,-56,-6.9,-54.7).curveTo(-2.9,-53.9,10.5,-52.3).curveTo(19.3,-51.3,24.5,-49.4).curveTo(29.3,-47.7,37,-45.5).curveTo(42.5,-43.8,42.8,-41.9).curveTo(45.6,-39.4,48.5,-22.2).curveTo(50,-13.5,50.9,-5.1).curveTo(50.9,-2.8,49.9,3.3).curveTo(48.9,9.5,48.9,11.6).curveTo(48.9,14.6,46.5,20.7).curveTo(43.6,28.7,38.7,35.4).curveTo(24.4,54.9,1,54.9).curveTo(-22.4,54.9,-36.9,19.9).closePath().moveTo(9,48.4).lineTo(10.5,48.4).lineTo(11.8,48).lineTo(9,48.4).closePath();
	this.shape_4.setTransform(84.4,66.1);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,8.1,136.3,116);


(lib.JaneClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#4E2A3F").setStrokeStyle(1,1,1).moveTo(31.2,6.8).curveTo(31,6.6,30.9,6.4).curveTo(25,-3.1,15.4,-5.9).curveTo(12.3,-6.8,5.6,-6.8).curveTo(-5.5,-6.8,-16.6,-4.1).curveTo(-24.8,-2.1,-29.1,0.2).curveTo(-30.3,0.9,-31.2,1.5);
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#7A4D30").beginStroke().moveTo(35.1,56.3).curveTo(35.3,55.7,35.3,53.5).curveTo(35.7,49.5,35.2,46.3).curveTo(34.8,44.1,34,42.6).curveTo(38.2,42.5,42.4,42.2).lineTo(43.7,52.2).curveTo(42.1,52.5,40.5,53).curveTo(36.5,54.3,35.2,56.4).lineTo(35.1,56.3).closePath().moveTo(0.7,41.8).lineTo(-0.9,39.1).curveTo(4.3,40.3,9.7,41.1).lineTo(13.4,53).curveTo(9.9,53.6,7,54.9).curveTo(3.9,47.6,0.7,41.8).closePath().moveTo(-37.9,1.9).curveTo(-42.1,-2.1,-43.6,-8.6).curveTo(-43.9,-9.9,-42.8,-11.9).curveTo(-41.1,-14.7,-40.1,-16.7).curveTo(-35.5,-24.9,-34.7,-36).curveTo(-34.5,-38.6,-32.6,-47.1).lineTo(-30.6,-56.4).lineTo(-29.4,-55.9).curveTo(-28.6,-55.6,-28.1,-55.5).curveTo(-28.8,-51.3,-30,-37.5).curveTo(-31.3,-21.8,-31.4,-21.6).curveTo(-32.8,-17.1,-35.7,-12.7).curveTo(-37.4,-9,-35.9,-2.8).lineTo(-34.8,1.1).lineTo(-33.7,4.9).curveTo(-35.9,3.9,-37.9,1.9).closePath();
	this.shape_1.setTransform(47.9,96.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#975F3C").beginStroke().moveTo(-10.1,53.4).curveTo(-14,53.1,-17.6,53.4).curveTo(-19.4,53.6,-21,53.9).lineTo(-24.7,42).curveTo(-15,43.5,-5,43.5).curveTo(-5,44.4,-4.8,45.2).lineTo(-4.1,50.3).lineTo(-3.6,54.7).curveTo(-6,53.8,-10.1,53.4).closePath().moveTo(19.4,52.6).curveTo(13.8,52.2,9.3,53).lineTo(8,43.1).curveTo(19.6,42.1,31.1,39.5).curveTo(30.6,40.7,30.3,41.9).curveTo(30.1,43,28.2,48.2).curveTo(26.8,51.8,26.6,53.8).curveTo(23.3,52.8,19.4,52.6).closePath().moveTo(-63.1,6.8).lineTo(-64.6,6.6).curveTo(-65.7,6.5,-66.7,6.2).lineTo(-68.1,5.7).lineTo(-69.2,1.9).lineTo(-70.3,-1.9).curveTo(-71.8,-8.2,-70.1,-11.8).curveTo(-67.2,-16.3,-65.8,-20.7).curveTo(-65.7,-21,-64.4,-36.7).curveTo(-63.2,-50.5,-62.5,-54.7).curveTo(-61.4,-54.6,-60,-54).lineTo(-58.1,-53.2).curveTo(-55.4,-52.2,-50.8,-48.7).lineTo(-49.6,-47.8).lineTo(-50.4,-45.9).curveTo(-53,-39.5,-55.3,-28.6).curveTo(-57.7,-17.4,-57.4,-15.4).lineTo(-57.4,-15.3).curveTo(-57.2,-14.4,-54.5,-9.5).lineTo(-53.6,-7.8).curveTo(-50.3,-1.5,-50,-0.4).lineTo(-49.9,-0.3).curveTo(-49.4,1.8,-50.9,3.6).lineTo(-51.6,4.3).curveTo(-54.3,6.8,-60.6,6.8).lineTo(-63.1,6.8).closePath().moveTo(53.3,-17.2).curveTo(53.5,-17.5,52.5,-19).curveTo(51.5,-20.6,51.1,-20.6).lineTo(51,-20.5).lineTo(50.9,-20.6).lineTo(50.6,-21.3).curveTo(50.4,-21.7,49.9,-24.1).curveTo(49.4,-26.5,48.7,-28.3).lineTo(47.1,-32.1).curveTo(46.6,-33.3,46.7,-33.5).curveTo(47.4,-37,47.6,-39.1).curveTo(48,-42.1,47.7,-44.9).curveTo(47,-51.5,46.3,-53.8).lineTo(48.6,-54.7).curveTo(49.3,-51.5,53.7,-41.3).curveTo(56,-35.8,56.7,-33.6).curveTo(57.1,-32.4,57.3,-31).curveTo(58.3,-27.3,60.8,-24.2).curveTo(61.8,-22.9,65.3,-19.5).curveTo(71,-13.9,71,-7.2).curveTo(71,-2.8,65.4,-1.2).curveTo(62.1,-0.4,61.5,-0.1).curveTo(61,0.1,60.7,0.4).curveTo(56.7,-11.4,53.3,-17.2).closePath();
	this.shape_2.setTransform(82.3,95.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#4B9FB5").beginStroke().moveTo(25.4,31.5).curveTo(17.9,30.5,5,29).curveTo(-5.1,27.8,-13.5,26.1).curveTo(-13,25.3,-12.3,23.4).curveTo(-11,19.6,-10.1,13.7).lineTo(-8.7,5.5).curveTo(-8.2,2.9,-8.2,-0.1).lineTo(-8.2,-1.1).lineTo(-8.2,-1.3).lineTo(-8.3,-2.2).lineTo(-8.4,-2.9).curveTo(-7.7,-3.1,-7,-3.2).curveTo(-6.5,-3.3,-5.6,-2).curveTo(-4.9,-1,-4.4,0.2).curveTo(-4.1,0.8,-3.3,5.6).curveTo(-1.9,10.9,2,15.3).curveTo(6,19.7,11.5,21.9).curveTo(14.7,23.2,20.9,24.3).curveTo(26.5,25.3,28.6,26.4).curveTo(31.9,28.2,32.7,32.1).curveTo(28.5,31.9,25.4,31.5).closePath().moveTo(-24.3,-9.5).lineTo(-29.9,-11.5).curveTo(-32.5,-12.5,-32.6,-13.4).curveTo(-33,-16.1,-29.1,-20.5).curveTo(-26.1,-24,-22.5,-26.5).curveTo(-18,-29.6,-10.6,-32.1).curveTo(-12.7,-30.6,-16.1,-25.9).curveTo(-20,-20.7,-22,-16.1).curveTo(-22.7,-14.7,-23.2,-11.5).curveTo(-23.5,-9.5,-24.2,-9.5).lineTo(-24.3,-9.5).closePath();
	this.shape_3.setTransform(47.8,47.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#58BAD3").beginStroke().moveTo(1.9,39.5).curveTo(1.1,35.6,-2.2,33.8).curveTo(-4.3,32.7,-9.8,31.7).curveTo(-16,30.6,-19.2,29.3).curveTo(-24.8,27.1,-28.7,22.7).curveTo(-32.6,18.3,-34.1,13).curveTo(-34.9,8.2,-35.2,7.6).curveTo(-35.6,6.4,-36.4,5.4).curveTo(-37.3,4.1,-37.8,4.2).curveTo(-38.5,4.3,-39.1,4.5).curveTo(-39.4,2.7,-40.1,2.2).curveTo(-41.1,1.5,-42.7,3.6).lineTo(-43.6,5).lineTo(-45.2,5.3).lineTo(-45.3,5).lineTo(-49.4,1).curveTo(-50.5,-0.1,-51.5,-0.7).lineTo(-55,-2.1).curveTo(-54.2,-2,-53.9,-4.1).curveTo(-53.4,-7.3,-52.8,-8.7).curveTo(-50.7,-13.3,-46.9,-18.5).curveTo(-43.4,-23.2,-41.3,-24.7).lineTo(-29,-28.9).curveTo(-23.3,-30.9,-22.5,-31.5).curveTo(-22.2,-31.7,-22.3,-32.2).lineTo(-22.6,-32.8).curveTo(-18.3,-35.2,-10.2,-37.2).curveTo(1,-39.9,12.1,-39.9).curveTo(18.8,-39.9,21.9,-39).curveTo(31.4,-36.2,37.4,-26.7).curveTo(36.2,-26.6,36.2,-25.4).curveTo(36.2,-24.7,41.7,-21.2).curveTo(48.2,-17.1,50.2,-15.1).curveTo(51.5,-13.8,53.7,-7.4).curveTo(54.4,-5.3,55,-4).curveTo(53.9,-3.6,52.6,-2.9).curveTo(50.1,-1.7,48.7,-0.8).curveTo(47.2,-4,44.6,-7).curveTo(41.9,-10.3,40,-10.7).curveTo(37.9,-11.1,37.9,-7.7).curveTo(37.9,-7.2,42,-1.5).curveTo(46.3,4.4,46.9,10.7).curveTo(47.4,16.5,44.6,20.8).curveTo(43.2,23,41.6,24.3).curveTo(41.6,25.4,41.8,25.7).lineTo(42,26).curveTo(42.4,26.4,44.3,26.4).curveTo(44.8,26.4,45.7,26).lineTo(46.9,25.4).curveTo(47,26.7,47.4,27.7).lineTo(48.5,30.7).curveTo(49.6,33.9,50.9,36.3).lineTo(41.6,38.2).curveTo(35.1,39.6,18.7,39.8).lineTo(14.9,39.9).curveTo(7.4,39.9,1.9,39.5).closePath();
	this.shape_4.setTransform(78.6,39.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#7A7A7A").beginStroke().moveTo(2.8,42.7).curveTo(-2.5,42.3,-3.2,42).lineTo(-2.3,39.5).curveTo(-1.8,38.1,-0.3,37.2).lineTo(1.1,36.6).lineTo(1.7,36.4).lineTo(3.4,36).lineTo(3.3,36.7).curveTo(3.3,39.9,13.2,42.4).lineTo(15.8,43).curveTo(12.9,43.2,9.4,43.1).curveTo(7.7,43.2,2.8,42.7).closePath().moveTo(32.8,40.1).curveTo(32.8,38.7,32.4,37.5).curveTo(33,37.5,33.6,36.7).curveTo(34.4,35.7,34.8,35.4).curveTo(35.8,34.6,37.4,34.1).curveTo(38.4,38.7,39.7,40.2).curveTo(40.6,41.2,42.5,42).lineTo(35.3,42.6).curveTo(33.3,42.7,32.7,42.8).curveTo(32.8,41.8,32.8,40.1).closePath().moveTo(-26.4,4.9).lineTo(-41,-0.4).lineTo(-42.5,-0.8).curveTo(-41,-3.2,-37.2,-11.1).lineTo(-36.1,-13.4).curveTo(-33.9,-13.1,-31.5,-13).curveTo(-27.2,-12.8,-23.8,-14.3).lineTo(-22.9,-14.8).curveTo(-21.8,-15.3,-20.2,-16.7).curveTo(-17.8,-18.9,-17.3,-20.8).curveTo(-16.8,-23,-18.3,-27.9).curveTo(-19.4,-31.2,-20.3,-32.7).curveTo(-22.3,-36.4,-23.2,-38.3).curveTo(-21.6,-41.5,-21,-43.2).curveTo(-17,-40.6,-5.4,-39).lineTo(-4.9,-38.9).lineTo(-8.3,-17.4).curveTo(-11.4,2.7,-11.5,9.6).curveTo(-18.8,7.6,-26.4,4.9).closePath();
	this.shape_5.setTransform(53.4,120);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#8E8E8E").beginStroke().moveTo(-27.7,40.7).curveTo(-37.5,38.2,-37.6,35).lineTo(-37.5,34.3).curveTo(-31.7,32.9,-17.6,33).curveTo(-15.7,33.3,-14,34.8).curveTo(-12.5,36.3,-12.6,38.1).lineTo(-13,38.6).curveTo(-13.8,39.2,-15.4,39.8).curveTo(-18.9,40.9,-25.1,41.3).lineTo(-27.7,40.7).closePath().moveTo(-1.1,38.4).curveTo(-2.4,36.9,-3.5,32.4).curveTo(-0.7,31.5,4.3,31.6).curveTo(11.5,31.6,15.7,33.4).curveTo(19.2,35,20.1,37.7).curveTo(20.4,38.4,17,38.9).curveTo(11.6,39.4,1.6,40.2).curveTo(-0.3,39.4,-1.1,38.4).closePath().moveTo(-52.4,7.8).curveTo(-52.2,1,-49.2,-19.2).lineTo(-45.8,-40.7).curveTo(-39.5,-39.8,-20.8,-38.1).curveTo(-11.9,-37.3,5.8,-37.8).curveTo(24.6,-38.4,33.3,-40.1).curveTo(36.2,-40.6,38.2,-41.2).lineTo(47.5,-13.5).curveTo(51.4,-1.9,52.3,-0).curveTo(50.6,0.9,49.1,1.4).lineTo(47.5,1.8).curveTo(36.6,5.2,20.6,9).curveTo(18.4,9.5,4.2,11.1).curveTo(-11.3,12.8,-16.2,12.8).curveTo(-33.4,12.8,-52.4,7.8).closePath();
	this.shape_6.setTransform(94.2,121.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#000000").beginStroke().moveTo(-25.8,80.7).curveTo(-31.9,79.6,-32.6,78.5).curveTo(-33.4,77.4,-33.4,75.2).curveTo(-33.4,70.1,-28.2,66.5).curveTo(-30,63.8,-32.7,58.3).curveTo(-37.9,47.3,-37.9,47.5).lineTo(-37.9,46.9).curveTo(-53.9,42.3,-69.4,33.9).lineTo(-69.5,34).curveTo(-70.9,33.5,-71.4,32.8).lineTo(-71.7,32.6).lineTo(-71.7,32.3).lineTo(-71.7,30.7).curveTo(-71.7,28.6,-66.8,18.1).curveTo(-70.7,17,-73.1,15.1).curveTo(-77.4,11.6,-78.5,4).curveTo(-79.3,-0.9,-78.4,-3.7).curveTo(-77.8,-5.5,-75.8,-8.3).curveTo(-70.8,-15.4,-69.3,-26.5).curveTo(-68.6,-32,-67.6,-36.7).lineTo(-65.4,-46.7).lineTo(-65.3,-49.4).curveTo(-66.2,-49.7,-66.7,-50.4).curveTo(-67.2,-51.1,-67.3,-52.4).lineTo(-67.2,-53.8).lineTo(-67.2,-53.9).curveTo(-66.4,-63.1,-52.8,-71).curveTo(-40.1,-78.5,-23.3,-81).curveTo(-23.2,-81,-23,-80.4).lineTo(-22.9,-80.3).lineTo(-22.6,-79.7).curveTo(-22.5,-79.2,-22.8,-79).curveTo(-23.6,-78.4,-29.3,-76.4).lineTo(-41.6,-72.2).curveTo(-49,-69.7,-53.5,-66.6).curveTo(-57.1,-64.1,-60.2,-60.5).curveTo(-64,-56.1,-63.7,-53.4).curveTo(-63.5,-52.5,-61,-51.6).lineTo(-55.3,-49.6).lineTo(-51.8,-48.1).curveTo(-50.8,-47.6,-49.7,-46.5).lineTo(-45.6,-42.4).lineTo(-45.5,-42.2).lineTo(-43.9,-42.4).lineTo(-43,-43.9).curveTo(-41.4,-46,-40.4,-45.3).curveTo(-39.7,-44.8,-39.4,-43).lineTo(-39.4,-42.3).lineTo(-39.3,-41.4).lineTo(-39.3,-41.2).lineTo(-39.3,-40.2).curveTo(-39.3,-37.2,-39.8,-34.5).lineTo(-41.2,-26.3).curveTo(-42,-20.5,-43.4,-16.6).curveTo(-44,-14.8,-44.6,-13.9).curveTo(-36.1,-12.2,-26.1,-11).curveTo(-13.2,-9.6,-5.6,-8.6).curveTo(-2.6,-8.2,1.6,-8).curveTo(8.5,-7.5,18.4,-7.6).curveTo(34.8,-7.9,41.3,-9.2).lineTo(50.6,-11.2).curveTo(49.3,-13.6,48.2,-16.8).lineTo(47.1,-19.8).curveTo(46.7,-20.8,46.6,-22.1).lineTo(45.4,-21.5).curveTo(44.5,-21.1,44,-21.1).curveTo(42.1,-21.1,41.7,-21.5).lineTo(41.5,-21.8).curveTo(41.3,-22.1,41.3,-23.2).curveTo(42.9,-24.5,44.3,-26.7).curveTo(47.1,-30.9,46.6,-36.8).curveTo(46,-43,41.7,-48.9).curveTo(37.6,-54.7,37.6,-55.2).curveTo(37.6,-58.5,39.7,-58.2).curveTo(41.6,-57.7,44.3,-54.5).curveTo(46.9,-51.4,48.4,-48.3).curveTo(49.8,-49.2,52.3,-50.4).curveTo(53.6,-51.1,54.7,-51.4).curveTo(54.1,-52.8,53.4,-54.9).curveTo(51.2,-61.3,49.9,-62.6).curveTo(47.9,-64.6,41.4,-68.7).curveTo(35.9,-72.2,35.9,-72.9).curveTo(35.9,-74.1,37.1,-74.2).curveTo(37.9,-74.3,39.4,-73.8).curveTo(42.8,-72.8,47,-69.7).curveTo(58,-61.4,58,-51.7).lineTo(58,-51).lineTo(58,-50.5).lineTo(57.9,-49.9).lineTo(57.8,-49.7).lineTo(57.6,-49.5).lineTo(57.5,-49.4).lineTo(56.4,-48.9).curveTo(55.6,-48.7,55.8,-48.1).curveTo(58.2,-41.8,61.1,-35.5).curveTo(63.2,-31,64.7,-26).curveTo(66.6,-19.4,69.3,-15.9).curveTo(70.7,-14,74,-11.6).curveTo(76.5,-9.6,77.5,-7.5).curveTo(78.9,-4.3,78.9,2).curveTo(78.9,5.3,75.9,8.5).curveTo(72.2,12.4,66.2,12.8).lineTo(65.4,12.9).lineTo(65.7,14).curveTo(70.7,29.6,70.7,35.4).lineTo(70.6,36.4).curveTo(70,37.6,69.5,38).curveTo(69,38.5,68.4,38.5).lineTo(44,45.6).lineTo(39.7,46.8).curveTo(39.6,49.3,37.9,55.2).curveTo(36.2,61.1,34.1,63.8).lineTo(36,64.7).curveTo(42.3,68.3,42,73.6).curveTo(41.9,75.1,41.4,75.6).curveTo(40.9,76.4,39,76.8).curveTo(37.6,77.2,21.6,79.2).curveTo(15,79.9,9.5,79.2).curveTo(7.3,78.7,7.2,78).lineTo(6.9,76.5).lineTo(6.5,77.1).curveTo(4.4,79.5,-4.7,80.7).curveTo(-10.5,81,-15.8,81).curveTo(-21,81,-25.8,80.7).closePath().moveTo(-22.2,68.6).lineTo(-23.8,69).lineTo(-24.4,69.2).lineTo(-25.8,69.8).curveTo(-27.3,70.7,-27.9,72.1).lineTo(-28.7,74.6).curveTo(-28,74.9,-22.7,75.3).curveTo(-17.8,75.8,-16.1,75.7).curveTo(-12.7,75.8,-9.8,75.6).curveTo(-3.5,75.2,-0.1,74.1).curveTo(1.5,73.5,2.3,72.9).lineTo(2.8,72.4).curveTo(2.8,70.7,1.3,69.2).curveTo(-0.3,67.6,-2.3,67.4).lineTo(-4.3,67.4).curveTo(-16.8,67.4,-22.2,68.6).closePath().moveTo(11.8,66.7).curveTo(10.3,67.2,9.3,68).curveTo(8.8,68.3,8,69.3).curveTo(7.5,70.1,6.9,70.1).curveTo(7.3,71.3,7.3,72.7).curveTo(7.3,74.4,7.1,75.4).curveTo(7.8,75.3,9.8,75.2).lineTo(17,74.6).curveTo(27,73.8,32.4,73.2).curveTo(35.7,72.8,35.4,72).curveTo(34.5,69.4,31,67.7).curveTo(26.9,65.9,19.7,65.9).lineTo(19.1,65.9).curveTo(14.5,65.9,11.8,66.7).closePath().moveTo(11.4,51.6).curveTo(7.2,51.9,3,52).curveTo(3.8,53.4,4.2,55.7).curveTo(4.7,58.9,4.3,62.9).curveTo(4.3,65,4.1,65.7).lineTo(4.2,65.7).curveTo(5.5,63.7,9.5,62.4).curveTo(11.1,61.9,12.7,61.6).curveTo(17.2,60.7,22.8,61.2).curveTo(26.7,61.4,30,62.3).curveTo(30.2,60.4,31.6,56.7).curveTo(33.5,51.5,33.7,50.4).curveTo(34,49.2,34.5,48.1).curveTo(23,50.7,11.4,51.6).closePath().moveTo(-30.3,51.2).curveTo(-27.1,57,-24,64.2).curveTo(-21.1,63,-17.6,62.4).curveTo(-16,62.1,-14.2,62).curveTo(-10.6,61.7,-6.7,62).curveTo(-2.6,62.4,-0.2,63.2).lineTo(-0.7,58.9).lineTo(-1.4,53.8).curveTo(-1.6,53,-1.6,52.1).curveTo(-11.6,52,-21.3,50.5).curveTo(-26.7,49.7,-31.9,48.5).lineTo(-30.3,51.2).closePath().moveTo(-62.7,21.5).curveTo(-66.5,29.4,-68,31.8).lineTo(-66.5,32.2).lineTo(-52,37.5).curveTo(-44.4,40.2,-37,42.2).curveTo(-18.1,47.2,-0.9,47.1).curveTo(4,47.1,19.5,45.4).curveTo(33.7,43.8,36,43.3).curveTo(51.9,39.5,62.8,36.1).lineTo(64.4,35.8).curveTo(66,35.3,67.7,34.3).curveTo(66.7,32.4,62.8,20.8).lineTo(53.5,-6.9).curveTo(51.6,-6.3,48.6,-5.7).curveTo(39.9,-4.1,21.1,-3.5).curveTo(3.5,-3,-5.5,-3.7).curveTo(-24.2,-5.5,-30.4,-6.3).lineTo(-30.9,-6.4).curveTo(-42.5,-8,-46.5,-10.6).curveTo(-47.2,-8.9,-48.7,-5.7).curveTo(-47.8,-3.8,-45.8,-0.1).curveTo(-45,1.4,-43.9,4.7).curveTo(-42.3,9.6,-42.8,11.8).curveTo(-43.3,13.7,-45.7,15.9).curveTo(-47.4,17.3,-48.4,17.8).lineTo(-49.4,18.3).curveTo(-52.7,19.8,-57,19.6).curveTo(-59.5,19.5,-61.6,19.2).lineTo(-62.7,21.5).closePath().moveTo(-63.6,-37.7).curveTo(-65.5,-29.2,-65.7,-26.7).curveTo(-66.5,-15.5,-71.1,-7.3).curveTo(-72.1,-5.3,-73.8,-2.6).curveTo(-74.9,-0.5,-74.6,0.8).curveTo(-73.1,7.3,-68.9,11.3).curveTo(-66.9,13.3,-64.7,14.3).lineTo(-63.3,14.7).curveTo(-62.3,15.1,-61.2,15.2).lineTo(-59.7,15.3).curveTo(-51.4,15.9,-48.2,12.8).lineTo(-47.5,12.2).curveTo(-46,10.3,-46.5,8.2).lineTo(-46.6,8.1).curveTo(-46.9,7,-50.2,0.8).lineTo(-51.1,-0.9).curveTo(-53.8,-5.9,-54,-6.7).lineTo(-54,-6.9).curveTo(-54.3,-8.9,-51.9,-20).curveTo(-49.6,-31,-47,-37.3).lineTo(-46.2,-39.3).lineTo(-47.4,-40.2).curveTo(-52,-43.7,-54.7,-44.7).lineTo(-56.6,-45.4).curveTo(-58,-46,-59.1,-46.1).curveTo(-59.6,-46.2,-60.4,-46.6).lineTo(-61.6,-47).lineTo(-63.6,-37.7).closePath().moveTo(55.9,-10.5).curveTo(56.9,-8.9,56.7,-8.7).curveTo(60.1,-2.8,64.1,8.9).curveTo(64.4,8.7,64.9,8.4).curveTo(65.5,8.2,68.8,7.3).curveTo(74.4,5.8,74.4,1.4).curveTo(74.4,-5.4,68.7,-10.9).curveTo(65.2,-14.3,64.2,-15.6).curveTo(61.7,-18.7,60.7,-22.5).curveTo(60.5,-23.8,60.1,-25.1).curveTo(59.4,-27.3,57.1,-32.7).curveTo(52.7,-42.9,52,-46.1).lineTo(49.7,-45.2).curveTo(50.4,-43,51.1,-36.3).curveTo(51.4,-33.6,51,-30.6).curveTo(50.8,-28.5,50.1,-25).curveTo(50,-24.7,50.5,-23.5).lineTo(52.1,-19.8).curveTo(52.8,-18,53.3,-15.5).curveTo(53.8,-13.2,54,-12.7).lineTo(54.3,-12).lineTo(54.4,-11.9).lineTo(54.5,-12).curveTo(54.9,-12,55.9,-10.5).closePath().moveTo(-47.2,-22.7).curveTo(-49.6,-17.2,-49.9,-12.8).curveTo(-48.8,-14.1,-48,-14.5).lineTo(-48.3,-15).curveTo(-48.4,-15.2,-48.4,-16.5).curveTo(-48.4,-17.3,-47.4,-20.9).lineTo(-46.5,-24.2).lineTo(-47.2,-22.7).closePath().moveTo(37.3,-73.8).lineTo(37.1,-74.2).lineTo(37.3,-73.8).closePath().moveTo(-25,-79).curveTo(-24.1,-79.7,-22.9,-80.3).curveTo(-24.1,-79.7,-25,-79).closePath();
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.hns_stretch_bubble_mc = function() {
	this.initialize();

}).prototype = p = new cjs.Container();
p.nominalBounds = null;


(lib.hns_head_bubble_mc = function() {
	this.initialize();

}).prototype = p = new cjs.Container();
p.nominalBounds = null;


(lib.hns_base_bubble_ms = function() {
	this.initialize();

}).prototype = p = new cjs.Container();
p.nominalBounds = null;


(lib.HInfront = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#352011").beginStroke().moveTo(-59.8,66.8).curveTo(-69.3,66,-74.8,59.9).curveTo(-79.7,54.4,-79.8,47).curveTo(-79.9,37.6,-74.8,30.8).curveTo(-71.6,26.6,-71.2,25.9).curveTo(-69.5,23,-69.4,19.8).curveTo(-69.3,16,-72.1,10.2).curveTo(-74.9,4.3,-74.9,1.1).curveTo(-74.9,-2.1,-69.5,-8.5).curveTo(-63.8,-15.2,-63.1,-19.3).curveTo(-62.5,-23.1,-62.5,-28.7).curveTo(-62.5,-34.7,-62,-38.2).curveTo(-60.3,-50.9,-48.4,-62.3).curveTo(-45.8,-64.8,-42.8,-67.1).curveTo(-40.7,-62.1,-37.3,-56.1).curveTo(-35.4,-52.8,-31,-45.8).curveTo(-28.1,-41.1,-28,-40.4).lineTo(-27.5,-34.1).curveTo(-27.2,-31.5,-26.3,-30.4).curveTo(-24.6,-28.1,-15.6,-28.1).curveTo(-10.8,-28.1,-1.9,-29.4).lineTo(6.3,-30.7).lineTo(10.9,-29.5).curveTo(15.8,-28.3,18.6,-28.1).curveTo(32.6,-27.2,42.9,-31.1).curveTo(46.1,-32.3,48.7,-33.8).lineTo(50.6,-35.1).curveTo(52.2,-35.1,54.8,-34.3).curveTo(53.2,-33.7,52.1,-31.4).curveTo(50.5,-27.9,49.9,-27.1).lineTo(47.6,-24.7).curveTo(45.7,-22.7,43.5,-19.6).curveTo(40.8,-15.8,32.8,-12.2).curveTo(26.2,-9.2,15.9,-10.2).curveTo(15,-10.3,11.8,-12.5).lineTo(8.5,-15).curveTo(11.2,-16.9,13.8,-19.5).curveTo(16.1,-22,16.1,-22.7).curveTo(16.1,-24.3,15.7,-24.9).curveTo(15.3,-25.4,15.3,-25.6).curveTo(12.6,-25.7,9.8,-23.5).curveTo(8.1,-22.1,4.1,-18.4).curveTo(-4.9,-10.8,-18.7,-10.7).curveTo(-20,-10.7,-24.2,-12.7).curveTo(-28.3,-14.8,-29.7,-14.8).lineTo(-30.4,-14.7).lineTo(-30.8,-14.8).curveTo(-32.4,-14.8,-32.3,-14.3).curveTo(-32.3,-14.2,-32.3,-14.2).curveTo(-32.3,-14.1,-32.3,-14).curveTo(-32.4,-14,-32.4,-14).curveTo(-32.4,-13.9,-32.5,-13.9).lineTo(-32.7,-12.5).lineTo(-32.7,-12.3).lineTo(-32.7,-10.7).curveTo(-32.7,-8,-31.1,-4).curveTo(-29.4,0.1,-29.4,2.2).curveTo(-29.4,4.8,-30.7,7.5).lineTo(-33.4,12.5).curveTo(-37.5,19.8,-37.5,28.6).curveTo(-37.5,34.3,-34.3,40.9).curveTo(-31.2,47.5,-31.2,52.1).lineTo(-31.2,55.2).curveTo(-31.7,58.5,-33.8,60.9).curveTo(-38.9,67.1,-52.8,67.1).curveTo(-56.1,67.1,-59.8,66.8).closePath().moveTo(48.1,-17.4).curveTo(51.7,-21.2,54.1,-25.5).curveTo(56.3,-29.5,56.3,-31.6).lineTo(56.2,-33.8).curveTo(59.5,-32.6,61.7,-32.6).curveTo(69.7,-32.6,73.7,-37.7).curveTo(76.2,-40.9,79.4,-49.3).curveTo(79.8,-47,79.8,-44.3).curveTo(79.8,-40.1,78.3,-36.6).curveTo(76.7,-32.7,72.8,-28.4).curveTo(67.7,-22.8,61.2,-19.8).curveTo(55.8,-17.4,50.8,-17.2).lineTo(50,-17.2).curveTo(49.1,-17.2,48.1,-17.4).closePath();
	this.shape.setTransform(84.8,88.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#5B371E").beginStroke().moveTo(0.3,27.2).curveTo(-2.5,27,-7.4,25.8).lineTo(-12,24.6).lineTo(-20.2,25.9).curveTo(-29.1,27.2,-33.8,27.2).curveTo(-42.8,27.2,-44.6,25).curveTo(-45.5,23.8,-45.8,21.2).lineTo(-46.3,14.9).curveTo(-46.3,14.2,-49.2,9.6).curveTo(-53.6,2.6,-55.5,-0.8).curveTo(-58.9,-6.8,-61.1,-11.8).curveTo(-50.3,-20.1,-35,-25.3).curveTo(-20.1,-29.5,-5.2,-25.3).curveTo(8.1,-19.3,23.7,-14.6).curveTo(38.9,-10.4,44.5,-8.5).curveTo(53.6,-5.5,57.2,-1.7).curveTo(60.1,1.4,61.1,6.1).curveTo(58,14.5,55.4,17.7).curveTo(51.5,22.8,43.5,22.8).curveTo(41.2,22.8,38,21.6).lineTo(37.9,21.2).curveTo(37.7,20.6,37.2,20.8).lineTo(36.5,21.1).curveTo(33.9,20.2,32.3,20.2).lineTo(30.4,21.5).curveTo(27.8,23.1,24.7,24.3).curveTo(16.4,27.4,5.7,27.4).curveTo(3.1,27.4,0.3,27.2).closePath();
	this.shape_1.setTransform(103.1,32.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-81.5,67.8).curveTo(-83.9,63.7,-84.4,58.5).lineTo(-84.5,52.7).curveTo(-84.5,46.6,-79.2,37.3).curveTo(-73.9,28,-73.9,27.6).curveTo(-73.9,24.8,-76.7,19).curveTo(-79.6,13.1,-79.6,8.4).curveTo(-79.6,5.3,-77.8,2.5).curveTo(-76.9,1.1,-73.9,-2.2).curveTo(-68,-8.6,-67.3,-15.3).lineTo(-66.4,-25.3).curveTo(-66.1,-30.2,-66.1,-35.1).curveTo(-64.4,-47.4,-50.1,-59.9).curveTo(-47.4,-62.3,-44.5,-64.4).curveTo(-32.7,-73.4,-18.2,-78).curveTo(-2.8,-82.1,14.2,-78).curveTo(29.3,-72.1,47.2,-66.7).curveTo(65.5,-61.1,68.9,-59.8).curveTo(77.5,-56.4,80.8,-51.6).curveTo(81.7,-50.4,82.3,-48.9).curveTo(84.5,-44,84.5,-36.1).curveTo(84.5,-26.6,80.6,-21.3).curveTo(75.7,-14.5,60,-5.8).lineTo(48.9,-5.8).lineTo(46.2,-8).lineTo(46.2,-8).curveTo(34.5,3.1,23,3.1).curveTo(15.3,3.1,8.6,-1.4).curveTo(5.5,-3.4,4.1,-4.9).curveTo(-8.5,2,-18.2,2).curveTo(-22,2,-25,1.3).curveTo(-24.1,5.6,-23.7,10).curveTo(-23.7,16.7,-28.2,24.1).curveTo(-32.7,31.4,-32.7,35.9).curveTo(-32.7,37.9,-29.3,45.4).curveTo(-25.9,52.9,-25.9,59.9).curveTo(-25.9,67.8,-28.9,72).curveTo(-34.6,80.1,-51.1,80.1).curveTo(-74.2,80.1,-81.5,67.8).closePath().moveTo(-16.5,-72.5).curveTo(-31.7,-67.3,-42.5,-59).curveTo(-45.5,-56.7,-48.1,-54.3).curveTo(-60,-42.8,-61.7,-30.1).curveTo(-62.2,-26.6,-62.2,-20.6).curveTo(-62.2,-15,-62.8,-11.2).curveTo(-63.5,-7.1,-69.2,-0.4).curveTo(-74.6,5.9,-74.6,9.2).curveTo(-74.6,12.4,-71.8,18.3).curveTo(-69,24.1,-69.1,27.8).curveTo(-69.2,31.1,-70.9,33.9).curveTo(-71.3,34.7,-74.5,38.9).curveTo(-79.6,45.7,-79.5,55.1).curveTo(-79.4,62.5,-74.5,68).curveTo(-69,74.1,-59.5,74.9).curveTo(-39.8,76.7,-33.5,69).curveTo(-31.4,66.6,-30.9,63.3).lineTo(-30.9,60.1).curveTo(-30.9,55.6,-34,49).curveTo(-37.2,42.3,-37.2,36.6).curveTo(-37.2,27.9,-33.1,20.6).lineTo(-30.4,15.6).curveTo(-29.1,12.9,-29.1,10.3).curveTo(-29.1,8.1,-30.8,4.1).curveTo(-32.4,0.1,-32.4,-2.6).lineTo(-32.4,-4.2).lineTo(-32.4,-4.4).lineTo(-32.2,-5.8).curveTo(-32.1,-5.8,-32.1,-5.9).curveTo(-32.1,-5.9,-32,-5.9).curveTo(-32,-6,-32,-6.1).curveTo(-32,-6.1,-32,-6.2).curveTo(-32.1,-6.7,-30.5,-6.7).lineTo(-30.1,-6.7).lineTo(-29.4,-6.7).curveTo(-28,-6.7,-23.9,-4.7).curveTo(-19.7,-2.6,-18.4,-2.6).curveTo(-4.6,-2.7,4.4,-10.3).curveTo(8.4,-14,10.1,-15.4).curveTo(12.9,-17.7,15.6,-17.5).curveTo(15.6,-17.3,16,-16.8).curveTo(16.4,-16.2,16.4,-14.7).curveTo(16.4,-13.9,14.1,-11.4).curveTo(11.5,-8.8,8.8,-6.9).lineTo(12.1,-4.4).curveTo(15.3,-2.2,16.2,-2.1).curveTo(26.5,-1.1,33.1,-4.1).curveTo(41.1,-7.7,43.8,-11.5).curveTo(46,-14.7,47.9,-16.6).lineTo(50.2,-19.1).curveTo(50.8,-19.8,52.4,-23.3).curveTo(53.5,-25.6,55.1,-26.2).lineTo(55.7,-26.5).curveTo(56.3,-26.6,56.4,-26.1).lineTo(56.5,-25.7).lineTo(56.6,-23.6).curveTo(56.6,-21.4,54.4,-17.5).curveTo(52,-13.1,48.4,-9.3).curveTo(49.8,-9.1,51.1,-9.2).curveTo(56.1,-9.3,61.5,-11.8).curveTo(68,-14.8,73.1,-20.3).curveTo(77,-24.6,78.6,-28.5).curveTo(80.1,-32,80.1,-36.2).curveTo(80.1,-38.9,79.7,-41.2).curveTo(78.7,-45.9,75.8,-49).curveTo(72.2,-52.8,63.1,-55.8).curveTo(57.5,-57.6,42.2,-61.9).curveTo(26.7,-66.6,13.4,-72.5).curveTo(5.9,-74.6,-1.5,-74.6).curveTo(-9,-74.6,-16.5,-72.5).closePath();
	this.shape_2.setTransform(84.5,80.1);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,169,160.2);


(lib.HighlightCircle = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(3,1,1).moveTo(24.8,0).curveTo(24.8,10.2,17.5,17.5).curveTo(10.2,24.8,0,24.8).curveTo(-10.2,24.8,-17.6,17.5).curveTo(-24.8,10.2,-24.8,0).curveTo(-24.8,-10.2,-17.6,-17.6).curveTo(-10.2,-24.8,0,-24.8).curveTo(10.2,-24.8,17.5,-17.6).curveTo(24.8,-10.2,24.8,0).closePath();
	this.shape.setTransform(24.8,24.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,52.6,52.6);


(lib.HeadG = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#CC0066").beginStroke().moveTo(-4,1.2).lineTo(-9.7,-1.9).curveTo(-2.9,-3.2,9.7,-0.2).lineTo(6.9,1.2).curveTo(4,2.3,1.3,2.3).curveTo(-1.4,2.3,-4,1.2).closePath();
	this.shape.setTransform(89.7,104.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#DDBAA4").beginStroke().moveTo(14.4,47.8).curveTo(3.7,43.3,-8.8,37.2).lineTo(-7.7,34.6).curveTo(-5.8,30,-5.8,27.9).curveTo(-5.8,27.6,-8.2,25.4).curveTo(-11.1,26.9,-11.7,30.5).curveTo(-12.4,34.1,-13.7,34.9).curveTo(-33.4,22.4,-33.4,19).curveTo(-33.4,17.7,-31.8,15.2).curveTo(-30.3,12.8,-30.3,12).curveTo(-30.3,10.9,-30.6,10.5).lineTo(-30.9,10).curveTo(-33.1,10.5,-34.6,11.2).curveTo(-35,8.8,-35,4.3).curveTo(-35,-4.2,-31.4,-14.8).curveTo(-27.4,-26.9,-20,-36.8).curveTo(-11.8,-48,-1.1,-54.4).curveTo(3.1,-45.7,6.7,-37.7).curveTo(16.1,-16.5,18.1,-4.7).curveTo(10.2,-2.9,10.2,4.8).curveTo(10.2,6,10.5,6.3).lineTo(10.8,6.6).curveTo(12.3,6.8,14.3,6.6).lineTo(18.3,5.8).lineTo(19.4,5.6).curveTo(21.1,18,24.2,28).curveTo(27.6,39.2,35,54.3).curveTo(25.2,52.2,14.4,47.8).closePath();
	this.shape_1.setTransform(38.2,62.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-21.9,22.5).curveTo(-29.9,18.6,-35.8,14.4).curveTo(-38.6,12.4,-39.8,11.1).curveTo(-40.9,12.2,-42.1,12.2).curveTo(-50,12.2,-59.5,2.5).curveTo(-62.9,-1,-65.5,-4.7).curveTo(-67.8,-8,-67.8,-9.1).curveTo(-67.8,-13.6,-67.3,-14.4).curveTo(-66.6,-15.8,-64.2,-16.9).curveTo(-62.7,-17.6,-60.5,-18.2).lineTo(-60.2,-17.6).curveTo(-59.9,-17.2,-59.9,-16.1).curveTo(-59.9,-15.3,-61.4,-12.9).curveTo(-63,-10.5,-63,-9.1).curveTo(-63,-5.7,-43.3,6.7).curveTo(-42,6,-41.3,2.4).curveTo(-40.6,-1.3,-37.8,-2.8).curveTo(-35.4,-0.6,-35.4,-0.3).curveTo(-35.4,1.9,-37.2,6.5).lineTo(-38.4,9.1).curveTo(-25.9,15.2,-15.1,19.6).curveTo(-4.4,24,5.4,26.2).curveTo(14.4,28.3,22.6,28.4).curveTo(33.6,28.7,42.8,20.5).curveTo(45.1,18.4,49.7,13.5).curveTo(53.6,9.2,55.8,7.5).curveTo(56.3,7.2,56.7,7.1).curveTo(57.2,7,57.5,7.2).curveTo(57.9,7.5,58,7.8).lineTo(58.3,8.4).curveTo(58.6,8.8,58.6,9.9).curveTo(58.6,12.4,55.6,16.3).curveTo(52.4,20.4,47.5,24.1).curveTo(35,33.5,22.3,33.5).curveTo(0.5,33.5,-21.9,22.5).closePath().moveTo(9.2,16.9).curveTo(3.1,12.8,3.1,10.9).curveTo(3.1,6.2,14.2,6.5).curveTo(18.3,6.6,38.9,9).curveTo(41.3,9.3,41.3,11.8).curveTo(41.3,13.7,36.6,17.3).curveTo(30.6,21.7,23.5,21.7).curveTo(16.4,21.7,9.2,16.9).closePath().moveTo(12.2,11.9).lineTo(17.9,15).curveTo(23.1,17.2,28.8,15).lineTo(31.6,13.6).curveTo(22.8,11.5,16.8,11.5).curveTo(14.2,11.5,12.2,11.9).closePath().moveTo(21.5,1.9).curveTo(15.5,1,13.7,-1.2).curveTo(12.8,-2.2,12.8,-4.5).curveTo(12.8,-9.6,16.6,-7.2).curveTo(20.8,-3.5,21.9,-3.2).curveTo(28,-1.6,34.1,-3.2).lineTo(34.1,-5).lineTo(34.9,-5.5).curveTo(35.7,-6,36.9,-6).curveTo(38.2,-6,38.8,-5.8).lineTo(39.1,-5.6).lineTo(39.4,-5).curveTo(39.7,-4.6,39.7,-3.5).curveTo(39.7,-2.1,38.1,-0.4).lineTo(35.7,1.9).curveTo(32.5,2.5,29,2.5).curveTo(25.4,2.5,21.5,1.9).closePath().moveTo(52.4,-17.1).curveTo(51,-17.3,49.2,-18.3).curveTo(47.1,-19.5,45.7,-21.2).curveTo(44.4,-22.8,43.5,-23.3).curveTo(41.9,-24.1,39,-23.8).curveTo(37.8,-23.6,36.7,-23.2).curveTo(36,-23.5,35.6,-23.9).lineTo(35,-25.1).curveTo(38.8,-32.7,53.8,-27.9).curveTo(59.3,-26.2,63.7,-23.6).curveTo(67.9,-20.9,67.7,-19.5).lineTo(67.6,-18.7).curveTo(66.5,-19.4,64.8,-19.8).lineTo(63.5,-20).lineTo(62.7,-20).curveTo(61.8,-20,61.1,-19.8).lineTo(59.6,-19.3).curveTo(59,-19.1,57.5,-18.2).curveTo(56.2,-17.3,55.2,-17.2).curveTo(54.3,-17,53.4,-17).lineTo(52.4,-17.1).closePath().moveTo(-6.8,-22.4).curveTo(-7.6,-22.8,-10.2,-22.5).lineTo(-11.2,-22.3).lineTo(-15.3,-21.5).curveTo(-17.3,-21.3,-18.8,-21.5).lineTo(-19.1,-21.9).curveTo(-19.4,-22.1,-19.4,-23.3).curveTo(-19.4,-31,-11.5,-32.9).curveTo(-8.1,-33.7,-3.4,-33.5).curveTo(8,-32.9,23.2,-27.2).curveTo(24.5,-26.7,23,-25.7).curveTo(21.5,-24.7,19,-24.5).lineTo(16.3,-24.4).lineTo(13.1,-24.5).curveTo(10.2,-21.1,5.6,-20.6).curveTo(3.9,-20.4,2.3,-20.4).curveTo(-2.8,-20.4,-6.8,-22.4).closePath().moveTo(7,-25.6).lineTo(7.4,-25.7).lineTo(7,-25.7).lineTo(7,-25.6).closePath();
	this.shape_2.setTransform(67.8,90.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#EACFB7").beginStroke().moveTo(2,59.5).curveTo(-6.2,59.3,-15.2,57.3).curveTo(-22.5,42.1,-26,31).curveTo(-29,20.9,-30.8,8.6).curveTo(-28.2,8.3,-27.4,8.7).curveTo(-22.1,11.3,-15,10.5).curveTo(-10.4,10,-7.4,6.6).lineTo(-4.3,6.6).lineTo(-1.6,6.6).curveTo(0.9,6.3,2.4,5.4).curveTo(3.9,4.4,2.6,3.9).curveTo(-12.6,-1.8,-23.9,-2.4).curveTo(-28.7,-2.7,-32,-1.8).curveTo(-34.1,-13.6,-43.4,-34.8).curveTo(-47,-42.8,-51.3,-51.4).curveTo(-37.7,-59.5,-20.5,-59.5).curveTo(-10.3,-59.5,1.4,-55.8).curveTo(14.3,-51.7,24.8,-44.3).curveTo(51.3,-25.8,51.2,4.5).curveTo(51.3,20,46.3,26.6).curveTo(45.5,27.7,41.2,32).curveTo(38.3,35,36.1,38.1).curveTo(35.8,38.2,35.2,38.6).curveTo(33.1,40.2,29.1,44.6).curveTo(24.6,49.5,22.3,51.5).curveTo(13.3,59.5,2.7,59.5).lineTo(2,59.5).closePath().moveTo(-17.5,41.9).curveTo(-17.5,43.8,-11.3,48).curveTo(-4.2,52.8,2.9,52.8).curveTo(10.1,52.8,16,48.3).curveTo(20.8,44.7,20.7,42.9).curveTo(20.7,40.4,18.4,40.1).curveTo(-2.3,37.6,-6.4,37.5).lineTo(-7.5,37.5).curveTo(-17.5,37.5,-17.5,41.9).closePath().moveTo(-7.7,26.6).curveTo(-7.8,28.9,-6.9,29.9).curveTo(-5.1,32.1,0.9,33).curveTo(8.7,34,15.2,33).lineTo(17.6,30.7).curveTo(19.1,29,19.1,27.6).curveTo(19.1,26.4,18.8,26).lineTo(18.6,25.5).lineTo(18.2,25.3).curveTo(17.6,25.1,16.3,25.1).curveTo(15.1,25.1,14.3,25.6).lineTo(13.5,26.1).lineTo(13.5,27.9).curveTo(7.5,29.4,1.4,27.9).curveTo(0.2,27.5,-4,23.8).curveTo(-5.2,23.1,-6,23.1).curveTo(-7.7,23.1,-7.7,26.6).closePath().moveTo(23,7.8).curveTo(23.8,8.2,25.1,9.8).curveTo(26.5,11.5,28.7,12.7).curveTo(30.4,13.8,31.9,14).curveTo(33,14.2,34.7,13.9).curveTo(35.6,13.7,36.9,12.9).curveTo(38.5,11.9,39,11.7).lineTo(40.5,11.3).curveTo(41.2,11,42.2,11).lineTo(42.9,11.1).lineTo(44.2,11.3).curveTo(46,11.7,47,12.3).lineTo(47.2,11.6).curveTo(47.4,10.2,43.1,7.5).curveTo(38.7,4.8,33.2,3.1).curveTo(18.2,-1.6,14.4,6).lineTo(15.1,7.2).curveTo(15.5,7.6,16.1,7.8).curveTo(17.2,7.4,18.5,7.3).lineTo(19.9,7.2).curveTo(21.8,7.2,23,7.8).closePath();
	this.shape_3.setTransform(88.4,59.5);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,139.6,124.1);


(lib.Handlecopy = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#9E8C49").beginStroke().moveTo(-71.9,82.1).lineTo(-72,80.7).curveTo(-71.8,68,-50.3,52.2).curveTo(-42.3,46.3,-28.4,37.8).lineTo(-3.3,22.6).curveTo(28.6,3,43.6,-12).curveTo(64.8,-33.2,64.8,-55.1).curveTo(65,-59.5,64.9,-61.8).curveTo(64.9,-65.9,64.2,-68.8).curveTo(62.1,-76.8,53.1,-81.6).lineTo(51.8,-81.8).curveTo(53.6,-82.2,55.7,-82.2).curveTo(62.8,-82.2,67.9,-69.9).curveTo(72.1,-60.1,72.1,-53).curveTo(72.1,-29.7,42,-3).curveTo(20.4,16.2,-8.5,31.1).curveTo(-18.5,36.3,-34,47.1).curveTo(-49.7,58.1,-60.7,63.8).curveTo(-69.7,68.6,-71.9,82.2).lineTo(-71.9,82.1).closePath();
	this.shape.setTransform(80,103.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#000000").beginStroke().moveTo(-72.2,102.8).curveTo(-74.2,101.4,-75.8,99.9).curveTo(-79.3,96.7,-80.7,92.8).curveTo(-82.8,87.1,-82.8,85.2).lineTo(-82.8,79).curveTo(-82.8,64.7,-61.4,48.2).curveTo(-53.2,41.8,-39.4,33.3).curveTo(-31.4,28.3,-14.3,18.1).curveTo(18.2,-1.7,32.8,-15.8).curveTo(54.2,-36.5,54.2,-57.8).curveTo(54.2,-64.9,53.9,-66.8).curveTo(52.7,-73,48,-77).lineTo(38.3,-76.6).curveTo(36.9,-75.3,36.4,-73.5).curveTo(36,-72,36,-69.3).curveTo(36,-65.2,38,-60.5).curveTo(39.9,-55.9,39.9,-49.7).lineTo(39.9,-44.3).curveTo(39.1,-40.7,34.6,-40.7).curveTo(32.5,-40.7,31.1,-41).lineTo(30,-41.4).curveTo(30,-41.6,29.7,-42).curveTo(29.5,-42.3,29.4,-43.3).curveTo(22.6,-47,19,-61.2).curveTo(17.5,-67.1,16.8,-74.1).lineTo(16.4,-79.4).curveTo(16.4,-93.6,26.8,-100.5).curveTo(36.4,-106.8,49.6,-103.9).curveTo(63.4,-100.8,72.4,-89.1).curveTo(82.8,-75.8,82.8,-55.7).curveTo(82.8,-29,61,-4.3).curveTo(44.3,14.7,13.1,33.9).curveTo(2.5,40.4,-12.5,48.6).lineTo(-34.8,60.7).curveTo(-56.6,72.8,-56.6,78.1).curveTo(-56.6,82.8,-46.2,86.6).lineTo(-40.8,88.6).lineTo(-40,88.4).curveTo(-37.5,90.7,-37.5,94).curveTo(-37.5,95.5,-40.5,98.7).curveTo(-46.6,100.8,-51.4,102.8).curveTo(-57.1,104.8,-62.3,104.8).curveTo(-67.5,104.8,-72.2,102.8).closePath().moveTo(49,-83.1).lineTo(50.3,-82.9).curveTo(59.3,-78.1,61.4,-70.1).curveTo(62.1,-67.2,62.1,-63.1).curveTo(62.2,-60.8,62,-56.4).curveTo(62,-34.5,40.8,-13.3).curveTo(25.8,1.7,-6.1,21.3).lineTo(-31.2,36.5).curveTo(-45.1,45,-53.1,50.9).curveTo(-74.6,66.7,-74.8,79.4).lineTo(-74.7,80.7).lineTo(-74.7,80.8).curveTo(-74.6,85.6,-73.6,88.5).curveTo(-72.4,92.1,-69.9,94).lineTo(-69.6,94.6).curveTo(-63,99.1,-58.1,97.2).lineTo(-57.6,97.2).curveTo(-56,97.2,-55,96.1).curveTo(-52.6,95.9,-51.2,95.2).lineTo(-51.1,95.1).curveTo(-49.8,95.1,-49.3,94.9).curveTo(-48.7,94.7,-48.7,93.7).lineTo(-49.2,93).curveTo(-49.2,91.7,-57.7,88.2).curveTo(-61.5,86.2,-63.5,82.7).lineTo(-64.2,80.7).curveTo(-64.4,79.6,-64.4,78.4).curveTo(-64.4,70.1,-52.7,62.9).curveTo(-34.8,53.7,-22.6,46.2).curveTo(-17.2,42.9,0.1,33.1).curveTo(12,26.4,22.5,19.2).curveTo(49.1,1,63.5,-20.8).curveTo(76.1,-39.8,76.1,-55.7).curveTo(76.1,-67,70.7,-79.2).curveTo(62.5,-97.4,46.9,-97.4).curveTo(31.5,-97.4,26.5,-89.3).curveTo(24.9,-86.8,24.4,-82.9).lineTo(24,-82.2).curveTo(23.2,-80.7,23.1,-78.6).lineTo(23.2,-74.2).lineTo(24.8,-62.6).curveTo(27.3,-51.3,31.9,-51.3).curveTo(32.7,-51.3,33,-51.6).lineTo(33.3,-52).curveTo(33.5,-52.7,33.5,-53.7).lineTo(33.3,-54).lineTo(33.5,-54.9).curveTo(33.5,-56.6,31.5,-61.9).curveTo(29.4,-67.2,29.4,-73.1).curveTo(29.4,-76.6,32.8,-81.1).curveTo(36.5,-83.5,43,-83.5).curveTo(45.7,-83.5,49,-83.1).closePath();
	this.shape_1.setTransform(82.8,104.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#CCB55E").beginStroke().moveTo(-70.3,94.5).lineTo(-70.6,93.9).curveTo(-73.1,91.9,-74.3,88.3).curveTo(-75.3,85.5,-75.4,80.7).curveTo(-73.2,67.1,-64.2,62.4).curveTo(-53.2,56.6,-37.5,45.7).curveTo(-22,34.8,-12,29.6).curveTo(16.9,14.7,38.5,-4.5).curveTo(68.6,-31.2,68.6,-54.5).curveTo(68.6,-61.5,64.4,-71.4).curveTo(59.3,-83.6,52.2,-83.6).curveTo(50.2,-83.6,48.3,-83.2).curveTo(37.4,-84.7,32.1,-81.3).curveTo(28.7,-76.7,28.7,-73.2).curveTo(28.7,-67.4,30.8,-62.1).curveTo(32.8,-56.8,32.8,-55.1).lineTo(32.6,-54.1).lineTo(32.8,-53.8).curveTo(32.8,-52.9,32.6,-52.1).lineTo(32.3,-51.8).curveTo(32,-51.5,31.2,-51.5).curveTo(26.6,-51.5,24.1,-62.8).lineTo(22.5,-74.3).lineTo(22.4,-78.7).curveTo(22.5,-80.9,23.3,-82.3).lineTo(23.7,-83).curveTo(24.2,-87,25.8,-89.4).curveTo(30.8,-97.6,46.2,-97.6).curveTo(61.8,-97.6,70,-79.4).curveTo(75.4,-67.1,75.4,-55.8).curveTo(75.4,-39.9,62.8,-20.9).curveTo(48.4,0.8,21.8,19).curveTo(11.3,26.3,-0.6,33).curveTo(-17.9,42.7,-23.3,46).curveTo(-35.5,53.5,-53.4,62.7).curveTo(-65.1,69.9,-65.1,78.2).curveTo(-65.1,79.4,-64.9,80.6).lineTo(-64.2,82.5).curveTo(-62.2,86.1,-58.4,88.1).curveTo(-49.9,91.6,-49.9,92.9).lineTo(-49.4,93.5).curveTo(-49.4,94.5,-50,94.8).curveTo(-50.5,94.9,-51.8,95).lineTo(-51.9,95.1).curveTo(-53.3,95.7,-55.7,95.9).curveTo(-56.7,97,-58.3,97).lineTo(-58.8,97).curveTo(-60.3,97.6,-61.9,97.6).curveTo(-65.7,97.6,-70.3,94.5).closePath();
	this.shape_2.setTransform(83.5,105);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,165.7,209.7);


(lib.Handle = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#9E8C49").beginStroke().moveTo(-66.9,77.1).curveTo(-72.7,72.8,-72.9,67.4).curveTo(-73.5,54.3,-55.9,42.3).curveTo(-47.9,36.8,-30.7,29.2).curveTo(-21,24.7,-3.3,14).curveTo(14.3,3.3,21.3,-2.5).curveTo(43.4,-20.6,53.7,-34.8).curveTo(68.9,-55.5,71.6,-81.2).curveTo(73,-75.3,73,-69.7).curveTo(73,-53.8,60.4,-34.8).curveTo(46,-13,19.4,5.2).curveTo(8.8,12.4,-3,19.1).curveTo(-20.4,28.9,-25.8,32.2).curveTo(-38,39.7,-55.8,48.9).curveTo(-67.6,56.1,-67.6,64.4).curveTo(-67.6,65.5,-67.4,66.7).lineTo(-66.6,68.7).curveTo(-64.6,72.2,-60.8,74.2).curveTo(-52.4,77.7,-52.4,79).lineTo(-51.8,79.7).curveTo(-51.8,80.7,-52.5,80.9).curveTo(-52.9,81.1,-54,81.1).lineTo(-54.2,81.1).lineTo(-55.9,81.2).curveTo(-61.5,81.2,-66.9,77.1).closePath();
	this.shape.setTransform(86,118.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#000000").beginStroke().moveTo(-72.2,102.8).curveTo(-78.7,98.4,-80.7,92.8).curveTo(-82.8,87.1,-82.8,85.2).lineTo(-82.8,79).curveTo(-82.8,64.7,-61.4,48.2).curveTo(-53.2,41.8,-39.4,33.3).curveTo(-31.4,28.3,-14.3,18.1).curveTo(18.2,-1.7,32.8,-15.8).curveTo(54.2,-36.5,54.2,-57.8).curveTo(54.2,-64.9,53.9,-66.8).curveTo(52.7,-73,48,-77).lineTo(38.3,-76.6).curveTo(36.9,-75.3,36.4,-73.5).curveTo(36,-72,36,-69.3).curveTo(36,-65.2,38,-60.5).curveTo(39.9,-55.9,39.9,-49.7).lineTo(39.9,-44.3).curveTo(39.1,-40.7,34.6,-40.7).curveTo(32.5,-40.7,31.1,-41).lineTo(30,-41.4).curveTo(30,-41.6,29.7,-42).curveTo(29.5,-42.3,29.4,-43.3).curveTo(22.6,-47,19,-61.2).curveTo(17.5,-67.1,16.8,-74.1).lineTo(16.4,-79.4).curveTo(16.4,-93.6,26.8,-100.5).curveTo(36.4,-106.8,49.6,-103.9).curveTo(63.4,-100.8,72.4,-89.1).curveTo(74,-87.1,75.3,-85).curveTo(82.8,-72.8,82.8,-55.7).curveTo(82.8,-29,61,-4.3).curveTo(44.3,14.7,13.1,33.9).curveTo(2.5,40.4,-12.5,48.6).lineTo(-34.8,60.7).curveTo(-56.6,72.8,-56.6,78.1).curveTo(-56.6,82.8,-46.2,86.6).lineTo(-40.8,88.6).lineTo(-40,88.4).curveTo(-37.5,90.7,-37.5,94).curveTo(-37.5,95.5,-40.5,98.7).curveTo(-46.6,100.8,-51.4,102.8).curveTo(-57.1,104.8,-62.3,104.8).curveTo(-67.5,104.8,-72.2,102.8).closePath().moveTo(50.3,-82.9).curveTo(59.3,-78.1,61.4,-70.1).curveTo(62.1,-67.2,62.1,-63.1).curveTo(62.2,-60.8,62,-56.4).curveTo(62,-34.5,40.8,-13.3).curveTo(25.8,1.7,-6.1,21.3).lineTo(-31.2,36.5).curveTo(-45.1,45,-53.1,50.9).curveTo(-74.6,66.7,-74.8,79.4).lineTo(-74.7,80.7).curveTo(-74.6,85.6,-73.6,88.5).curveTo(-72.4,92.1,-69.9,94).lineTo(-69.6,94.6).curveTo(-63,99.1,-58.1,97.2).lineTo(-57.6,97.2).curveTo(-56,97.2,-55,96.1).curveTo(-52.6,95.9,-51.2,95.2).lineTo(-51.1,95.1).lineTo(-50.8,95.1).curveTo(-49.7,95.1,-49.3,94.9).curveTo(-48.7,94.7,-48.7,93.7).lineTo(-49.2,93).curveTo(-49.2,91.7,-57.7,88.2).curveTo(-61.5,86.2,-63.5,82.7).lineTo(-64.2,80.7).curveTo(-64.4,79.6,-64.4,78.4).curveTo(-64.4,70.1,-52.7,62.9).curveTo(-34.8,53.7,-22.6,46.2).curveTo(-17.2,42.9,0.1,33.1).curveTo(12,26.4,22.5,19.2).curveTo(49.1,1,63.5,-20.8).curveTo(76.1,-39.8,76.1,-55.7).curveTo(76.1,-61.3,74.8,-67.2).curveTo(73.4,-73.1,70.7,-79.2).curveTo(62.5,-97.4,46.9,-97.4).curveTo(31.5,-97.4,26.5,-89.3).curveTo(24.9,-86.8,24.4,-82.9).lineTo(24,-82.2).curveTo(23.2,-80.7,23.1,-78.6).lineTo(23.2,-74.2).lineTo(24.8,-62.6).curveTo(27.3,-51.3,31.9,-51.3).curveTo(32.7,-51.3,33,-51.6).lineTo(33.3,-52).curveTo(33.5,-52.7,33.5,-53.7).lineTo(33.3,-54).lineTo(33.5,-54.9).curveTo(33.5,-56.6,31.5,-61.9).curveTo(29.4,-67.2,29.4,-73.1).curveTo(29.4,-76.6,32.8,-81.1).curveTo(36.5,-83.5,43,-83.5).curveTo(46.3,-83.5,50.3,-82.9).closePath();
	this.shape_1.setTransform(82.8,104.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#CCB55E").beginStroke().moveTo(-69.6,94.5).lineTo(-69.8,93.9).curveTo(-72.3,91.9,-73.6,88.3).curveTo(-74.6,85.4,-74.7,80.6).lineTo(-74.8,79.2).curveTo(-74.5,66.6,-53.1,50.8).curveTo(-45,44.9,-31.1,36.3).lineTo(-6,21.1).curveTo(25.9,1.5,40.8,-13.4).curveTo(62.1,-34.6,62.1,-56.5).curveTo(62.2,-60.9,62.2,-63.3).curveTo(62.2,-67.3,61.4,-70.3).curveTo(59.3,-78.3,50.4,-83).curveTo(38.4,-84.9,32.8,-81.3).curveTo(29.5,-76.7,29.5,-73.2).curveTo(29.5,-67.4,31.5,-62.1).curveTo(33.5,-56.8,33.5,-55.1).lineTo(33.4,-54.1).lineTo(33.5,-53.8).curveTo(33.5,-52.9,33.4,-52.1).lineTo(33.1,-51.8).curveTo(32.7,-51.5,32,-51.5).curveTo(27.3,-51.5,24.8,-62.8).lineTo(23.3,-74.3).lineTo(23.2,-78.7).curveTo(23.2,-80.9,24,-82.3).lineTo(24.5,-83).curveTo(25,-87,26.5,-89.4).curveTo(31.6,-97.6,47,-97.6).curveTo(62.6,-97.6,70.7,-79.4).curveTo(73.4,-73.2,74.8,-67.3).curveTo(72.1,-41.7,56.9,-20.9).curveTo(46.6,-6.8,24.5,11.4).curveTo(17.5,17.1,-0.1,27.9).curveTo(-17.8,38.6,-27.5,43).curveTo(-44.7,50.6,-52.7,56.2).curveTo(-70.3,68.2,-69.7,81.3).curveTo(-69.5,86.6,-63.7,91).curveTo(-58.3,95,-52.7,95).lineTo(-51,95).lineTo(-51.2,95.1).curveTo(-52.6,95.7,-54.9,95.9).curveTo(-56,97,-57.5,97).lineTo(-58.1,97).curveTo(-59.6,97.6,-61.2,97.6).curveTo(-65,97.6,-69.6,94.5).closePath();
	this.shape_2.setTransform(82.8,105);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,165.7,209.7);


(lib.GregHead = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(-21.1,4.3).curveTo(-22.2,3.1,-22.2,1.9).curveTo(-22.2,1.4,-22.1,1).lineTo(-28.4,1.1).curveTo(-30.3,1.4,-32.8,2.3).curveTo(-34.8,2.9,-35.7,2.9).curveTo(-36.9,2.9,-37.8,2.7).lineTo(-38.3,2.6).lineTo(-38.7,0.8).lineTo(-38.2,-0.4).curveTo(-38,-1.1,-37.4,-1.5).curveTo(-35.6,-2.7,-28.3,-3.4).curveTo(-22.2,-5.1,-9.5,-3.8).curveTo(-7.3,-1.9,-7.3,-1.4).curveTo(-7.3,-1.2,-8.2,-0.4).curveTo(-8.9,0.4,-9.5,0.7).lineTo(-13.5,0.8).curveTo(-13.2,1.3,-13.2,2).curveTo(-13.2,4.1,-14.6,5.1).curveTo(-15.6,5.9,-17,5.9).curveTo(-19.6,5.9,-21.1,4.3).closePath().moveTo(25.6,2.5).curveTo(23.5,2.1,22.6,1.3).curveTo(22.4,1,22.2,0.7).lineTo(22.1,0.4).lineTo(22.2,-0.4).lineTo(21.8,-0.4).curveTo(20.7,-0.6,19.7,-1).lineTo(18.9,-1.3).lineTo(18.9,-1.6).lineTo(18.8,-1.7).curveTo(18.7,-2,18.9,-3).curveTo(19.4,-6.3,28.5,-5.9).curveTo(32.6,-5.7,38.3,-4.8).lineTo(38.4,-4.8).lineTo(38.5,-4.2).curveTo(38.7,-3.8,38.5,-2.7).curveTo(38.4,-1.9,32.9,-0.8).lineTo(30.8,-0.4).lineTo(30.5,-0.3).lineTo(30.5,-0.2).curveTo(30,2.7,27,2.7).lineTo(25.6,2.5).closePath();
	this.shape.setTransform(96.2,91.1);

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#D5AE81").beginStroke().moveTo(-1.8,3.7).curveTo(-3.9,3.3,-4.8,2.5).curveTo(-5,2.2,-5.1,1.9).lineTo(-5.3,1.6).lineTo(-5.2,0.8).lineTo(-5.6,0.8).curveTo(-6.7,0.6,-7.7,0.2).lineTo(-8.4,-0.1).lineTo(-8.5,-0.4).lineTo(-8.5,-0.5).curveTo(-8.7,-0.8,-8.5,-1.8).curveTo(-7.9,-5.1,-2.5,-3.2).curveTo(3,-1.3,8.6,-0.3).lineTo(5.5,0.4).lineTo(3.4,0.8).lineTo(3.1,0.9).lineTo(3.1,1).curveTo(2.6,3.9,-0.3,3.9).lineTo(-1.8,3.7).closePath();
	this.shape_1.setTransform(123.6,85.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-1.1,77.7).curveTo(-7.2,76.8,-11.6,75.5).curveTo(-22.3,72.3,-29.7,67.2).curveTo(-41.3,59.1,-44.9,46.1).lineTo(-44.9,46).lineTo(-48.2,46.1).curveTo(-58.3,46.1,-64,39.4).curveTo(-65.7,37.5,-67.5,33.3).curveTo(-69,29.6,-69.2,29.5).curveTo(-69.4,27.4,-69.4,25.1).curveTo(-69.4,20.7,-69,18.8).curveTo(-68,14.7,-64.7,13.7).curveTo(-65.9,10.7,-66.9,-0).lineTo(-67.7,-10.3).curveTo(-67.7,-33.1,-58.9,-51.3).curveTo(-54.4,-60.7,-48,-66.9).curveTo(-33.5,-81,-9.4,-78.7).lineTo(-9.4,-79.1).lineTo(15.3,-79.1).curveTo(41.2,-74.5,54.6,-60.4).curveTo(60.6,-54,64.1,-44.2).curveTo(66.3,-38.2,67.3,-32.8).curveTo(66.9,-32.9,68.1,-28.5).curveTo(69.4,-23.6,69.4,-12.5).curveTo(69.5,0.1,67.4,6.2).curveTo(67,7.3,67.2,11.2).curveTo(67.5,15.4,67.4,16.1).curveTo(66.5,21.9,65.3,26.5).curveTo(63.5,33.6,59.9,42).curveTo(59.6,42.6,59.1,45.3).lineTo(58.6,48.9).curveTo(57.7,60.7,50.3,69.1).curveTo(42.6,77.7,31.5,78.8).lineTo(10.8,79.1).curveTo(2.8,78.3,-1.1,77.7).closePath().moveTo(-36.7,39.2).curveTo(-36.5,39.8,-36.5,41).curveTo(-36.5,43.3,-38.4,44.5).lineTo(-39.7,45.1).lineTo(-37,50.9).curveTo(-35,55.4,-32.1,58.2).curveTo(-24.6,65.6,-17.3,68.4).curveTo(-15,69.3,-12.2,70.1).curveTo(1,73.8,23.3,73.7).curveTo(33.5,73.6,41.1,68.9).curveTo(51.2,62.6,53,49.3).lineTo(53,49.1).curveTo(49.8,49.5,45.8,50.6).curveTo(41.4,51.7,38.1,52.2).curveTo(30.5,53.5,21.9,53.2).curveTo(16.2,53,7.2,51.6).lineTo(0.8,49.3).curveTo(0.8,46.3,6,47).curveTo(20.8,49.4,36.6,47.1).curveTo(41.3,46.4,44.9,45.4).curveTo(48.4,44.5,52.9,42.8).curveTo(53.6,42.5,54.7,40.1).lineTo(58.8,31.1).curveTo(60.9,26,61.8,21.3).curveTo(62.5,17.1,62.5,11.3).curveTo(62.5,9.9,62.7,9).curveTo(62.5,8.4,62.5,7.7).lineTo(62.8,5.9).lineTo(62.8,5.9).curveTo(64,-2.6,64.5,-7.7).curveTo(65.8,-23,62.7,-31.2).curveTo(61,-36.5,57.8,-44.9).curveTo(52.7,-58.2,39.6,-65.8).curveTo(30.7,-71,16.1,-74.2).lineTo(-9.5,-74.2).curveTo(-31.4,-76.3,-44.8,-63).curveTo(-50.6,-57.2,-54.7,-48.6).curveTo(-59.5,-38.7,-61.7,-26.1).curveTo(-63.2,-17.5,-63.2,-10.3).curveTo(-63.2,-9.3,-59.3,12.6).curveTo(-58.1,12.6,-56.4,13.1).curveTo(-52.8,14.2,-50.5,15.8).curveTo(-47.8,17.7,-47.8,19.3).lineTo(-47.8,19.4).curveTo(-47.8,20.7,-48.7,21.1).curveTo(-49.3,21.4,-50.4,21.4).curveTo(-52.9,21.4,-54.9,20.3).curveTo(-57.6,18.8,-60.7,18).curveTo(-61.4,18.8,-62.4,19.5).curveTo(-65,21.4,-65,25.1).lineTo(-65.1,27).curveTo(-65.1,28,-64.9,28.6).curveTo(-61.3,35.1,-56,38).curveTo(-52.3,40,-48.3,40).curveTo(-47.1,40,-43.8,38.9).curveTo(-40.5,37.8,-39.3,37.8).curveTo(-37.3,37.8,-36.7,39.2).closePath();
	this.shape_2.setTransform(69.4,79.1);

	// Layer 3
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-14.9,4).curveTo(-16,2.9,-16.1,1.3).lineTo(-16,-0.7).curveTo(-15.9,-2.3,-15,-4.4).curveTo(-13.8,-6.9,-12.3,-6.7).curveTo(-10.6,-6.5,-10.6,-4.9).lineTo(-10.7,-0.7).lineTo(-5.3,1.8).lineTo(8.5,1.8).curveTo(10.4,1.2,10.8,-0.3).curveTo(11.1,-1.3,11.1,-4.1).curveTo(11.1,-8.4,13.6,-6.9).curveTo(16.1,-5.5,16.1,-1.6).curveTo(16.1,2.2,14.5,4).curveTo(11.4,7.3,1.8,7.3).curveTo(-11.4,7.3,-14.9,4).closePath();
	this.shape_3.setTransform(102.1,108.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#725942").beginStroke().moveTo(6.8,42.5).curveTo(2.8,42.1,-0.9,39.7).lineTo(-0.9,39.7).curveTo(-0.9,38,-3.6,36.1).curveTo(-5.9,34.5,-9.5,33.5).curveTo(-11.2,33,-12.5,32.9).curveTo(-16.3,11,-16.3,10).curveTo(-16.3,2.9,-14.8,-5.7).curveTo(-12.6,-18.4,-7.9,-28.2).curveTo(-3.7,-36.9,2.1,-42.6).curveTo(3.9,-40.2,9.5,-24.9).curveTo(15.3,-8.7,16.3,-7).lineTo(16.2,-6.8).lineTo(15.8,-6.3).curveTo(14.2,-4.2,12.8,-0.1).lineTo(10.2,8).curveTo(8.7,12.4,8.5,19.5).curveTo(8.3,28.7,10.6,40.2).curveTo(11.1,42.6,8,42.6).lineTo(6.8,42.5).closePath();
	this.shape_4.setTransform(22.6,58.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#917154").beginStroke().moveTo(-43.9,5.6).curveTo(-49.5,-9.7,-51.3,-12.1).curveTo(-38,-25.4,-16,-23.3).lineTo(9.5,-23.3).curveTo(24.2,-20.1,33,-14.9).curveTo(46.2,-7.3,51.3,6).curveTo(42.3,15.7,17.5,20).curveTo(-7,24.2,-29.1,20.4).curveTo(-33.7,19.6,-37.1,23.6).curveTo(-38.1,21.8,-43.9,5.6).closePath();
	this.shape_5.setTransform(76,28.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#BC9A72").beginStroke().moveTo(22.1,48.2).curveTo(7.6,42.9,1.2,30.1).curveTo(-2,23.7,-3,15.8).lineTo(-5.3,16.5).curveTo(-8.6,17.6,-9.8,17.6).curveTo(-13.7,17.6,-17.4,15.6).curveTo(-22.8,12.7,-26.3,6.2).curveTo(-26.6,5.6,-26.6,4.6).lineTo(-26.5,2.7).curveTo(-26.5,-1,-23.8,-2.9).curveTo(-22.8,-3.6,-22.2,-4.4).curveTo(-19.1,-3.6,-16.3,-2.1).curveTo(-14.3,-1,-11.8,-1).curveTo(-10.7,-1,-10.1,-1.3).curveTo(-9.2,-1.7,-9.2,-3).curveTo(-5.5,-0.7,-1.5,-0.2).curveTo(2.8,0.3,2.3,-2.5).curveTo(-0,-14.1,0.2,-23.3).curveTo(0.3,-30.4,1.9,-34.8).lineTo(4.5,-42.9).curveTo(5.9,-47,7.4,-49).lineTo(7.9,-49.6).curveTo(11.8,-39.2,14.3,-27.3).curveTo(16.5,-16.8,17.9,-3.4).curveTo(18.8,6,21.4,20.5).curveTo(21.6,20.5,21.4,20.5).lineTo(22.1,24.1).lineTo(22.3,25.4).curveTo(24.1,34.6,25.5,42.9).lineTo(26.3,47.7).lineTo(26.3,47.7).lineTo(26.6,49.6).curveTo(24.2,49,22.1,48.2).closePath();
	this.shape_6.setTransform(30.9,101.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#D5AE81").beginStroke().moveTo(-4.2,58.2).lineTo(-4.2,58.2).curveTo(-6.7,58.7,-8.9,58.7).curveTo(-20.3,58.7,-29.1,56.4).lineTo(-29.4,54.5).lineTo(-29.3,54.5).lineTo(-30.2,49.6).curveTo(-31.6,41.4,-33.3,32.2).lineTo(-33.6,30.8).lineTo(-34.2,27.3).curveTo(-34,27.3,-34.2,27.3).curveTo(-36.8,12.8,-37.8,3.4).curveTo(-39.1,-10,-41.3,-20.5).curveTo(-43.8,-32.4,-47.8,-42.8).lineTo(-47.7,-42.9).curveTo(-44.3,-46.9,-39.6,-46.2).curveTo(-17.6,-42.3,6.9,-46.5).curveTo(31.8,-50.9,40.7,-60.5).curveTo(43.9,-52.2,45.6,-46.9).curveTo(48.7,-38.6,47.4,-23.3).curveTo(46.7,-15.6,45.4,-7.9).curveTo(45.4,-7.3,45.6,-6.6).curveTo(45.4,-5.7,45.4,-4.3).curveTo(45.4,1.4,44.7,5.7).curveTo(43.8,10.4,41.7,15.4).lineTo(37.6,24.4).lineTo(36.8,26.1).lineTo(38.3,27.2).curveTo(37.9,29.3,37.5,29.5).lineTo(37.4,29.6).lineTo(37.4,29.6).curveTo(37.4,31.9,37.2,31.7).lineTo(36.7,32.2).curveTo(36.3,32.7,35.2,32.8).lineTo(35.4,33).lineTo(35.6,33.5).lineTo(35.9,33.4).lineTo(35.9,33.7).lineTo(35.7,34.7).lineTo(35.7,34.8).lineTo(35.7,35.9).curveTo(35.7,38.5,35.6,39).curveTo(35.5,39.5,35.1,40.1).curveTo(35.7,41.1,35.7,41.9).curveTo(35.7,43.5,33.4,46.5).curveTo(30.8,49.8,27,52.7).curveTo(17,60.5,6.6,60.5).curveTo(0.6,60.5,-4.2,58.2).closePath().moveTo(-13.4,33).lineTo(-9.4,34.1).lineTo(-6.8,34.1).lineTo(-8.4,32.8).curveTo(-8.9,32.3,-8.9,32.2).lineTo(-8.9,32.1).curveTo(-11.1,32.6,-13.4,33).closePath().moveTo(27.4,30.3).curveTo(27.6,30.5,27.6,31.8).curveTo(27.6,32.3,25.9,33.8).lineTo(28.4,32.6).curveTo(30.2,31.9,31.7,31.9).lineTo(32.2,31.9).lineTo(32,31.8).curveTo(30.2,30.6,27.2,30.1).curveTo(27.2,30.1,27.2,30.2).curveTo(27.2,30.2,27.2,30.2).curveTo(27.3,30.3,27.3,30.3).curveTo(27.4,30.3,27.4,30.3).closePath().moveTo(0.6,9.5).curveTo(-0.4,11.6,-0.4,13.2).lineTo(-0.5,15.2).curveTo(-0.4,16.8,0.7,17.9).curveTo(4.2,21.2,17.3,21.2).curveTo(27,21.2,30.1,17.9).curveTo(31.7,16.1,31.7,12.3).curveTo(31.7,8.5,29.1,7).curveTo(26.6,5.5,26.7,9.8).curveTo(26.7,12.6,26.4,13.6).curveTo(25.9,15.1,24.1,15.8).lineTo(10.3,15.8).lineTo(4.9,13.2).lineTo(5,9).curveTo(4.9,7.4,3.3,7.2).lineTo(3.1,7.2).curveTo(1.7,7.2,0.6,9.5).closePath();
	this.shape_7.setTransform(86.5,94.8);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,138.9,158.3);


(lib.GotItInstructionBit = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("rgba(0,0,0,0.502)").beginStroke().moveTo(-330.7,136.4).curveTo(-344.6,136.4,-344.9,122.9).lineTo(-344.9,-123.1).curveTo(-344.5,-136.4,-330.7,-136.4).lineTo(330.7,-136.4).curveTo(344.5,-136.4,344.9,-123.1).lineTo(344.9,123.2).curveTo(344.4,136.4,330.7,136.4).closePath();
	this.shape.setTransform(344.9,136.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,689.8,272.9);


(lib.GotIt = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("got it", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 158;
	this.text.setTransform(94.2,15.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-81.7,-28.5).lineTo(81.6,-28.5).curveTo(95.8,-28.5,95.8,-14.3).lineTo(95.8,14.3).curveTo(95.8,28.5,81.6,28.5).lineTo(-81.7,28.5).curveTo(-95.8,28.5,-95.8,14.3).lineTo(-95.8,-14.3).curveTo(-95.8,-28.5,-81.7,-28.5).closePath();
	this.shape.setTransform(95.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#7EEA9B").beginStroke().moveTo(-81.7,28.5).curveTo(-95.8,28.5,-95.8,14.3).lineTo(-95.8,-14.3).curveTo(-95.8,-28.5,-81.7,-28.5).lineTo(81.6,-28.5).curveTo(95.8,-28.5,95.8,-14.3).lineTo(95.8,14.3).curveTo(95.8,28.5,81.6,28.5).closePath();
	this.shape_1.setTransform(95.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("rgba(0,0,0,0.502)").beginStroke().moveTo(-81.7,28.5).curveTo(-95.8,28.5,-95.8,14.3).lineTo(-95.8,-14.3).curveTo(-95.8,-28.5,-81.7,-28.5).lineTo(81.6,-28.5).curveTo(95.8,-28.5,95.8,-14.3).lineTo(95.8,14.3).curveTo(95.8,28.5,81.6,28.5).closePath();
	this.shape_2.setTransform(104.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,202.2,67.5);


(lib.glissTurn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-10.1,17.7).lineTo(-41.6,27.1).lineTo(-23,0).lineTo(-41.6,-27).lineTo(-10.1,-17.7).lineTo(9.8,-43.8).lineTo(10.6,-10.9).lineTo(41.7,0).lineTo(10.6,11).lineTo(9.8,43.8).closePath();
	this.shape.setTransform(41.7,43.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,83.3,87.6);


(lib.Girl03 = function() {
	this.initialize();

	// 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#748475").beginStroke().moveTo(-15.2,16.9).curveTo(-21.5,11.2,-22.5,2.8).lineTo(-22.5,2.8).curveTo(-21.5,-1.9,-18.2,-4.8).curveTo(-14.7,-7.9,-9.8,-7.7).curveTo(-4.6,-7.5,-1.9,-3).curveTo(0.8,1.5,-1.7,5.8).curveTo(-3.1,8.2,-5.5,9.1).curveTo(-7.5,9.9,-9.8,9.3).curveTo(-11.3,9.3,-12.4,10.7).curveTo(-13.4,12,-13.2,12.6).curveTo(-14.2,11.2,-14.2,9.7).curveTo(-14,8.2,-13.1,7.6).lineTo(-13.8,6.5).curveTo(-15,4.1,-14.1,1.6).curveTo(-13.2,-1,-10.5,-1.5).curveTo(-8,-1.9,-6.9,0.1).curveTo(-5.8,1.9,-7,3.4).curveTo(-6.5,0.7,-8.9,0.6).curveTo(-11.3,0.4,-11.5,2.3).curveTo(-11.8,3.5,-10.9,4.5).curveTo(-10.1,5.5,-8.7,5.8).curveTo(-5.2,6.5,-4.3,2.9).curveTo(-3.8,1,-5,-0.9).curveTo(-6.1,-2.7,-8.1,-3.5).curveTo(-13,-5.6,-16.3,-1).curveTo(-19.3,3.4,-17.6,9.1).curveTo(-16.2,13.9,-12.8,16.2).curveTo(-11.7,17.2,-11.4,16.8).curveTo(-11.3,16.4,-11.5,15.2).curveTo(-11.9,14,-11.5,12.7).curveTo(-11.1,11.4,-10,10.7).curveTo(-10.6,12.2,-9.9,14.8).curveTo(-9,17.4,-7.2,18.6).curveTo(-6.2,19.3,-5.7,19).curveTo(-5.3,18.7,-5.8,17.9).curveTo(-6.6,16.4,-6.8,14.6).curveTo(-7,12.2,-5.4,10.8).curveTo(-6.3,14.1,-3,17.8).curveTo(0.9,22,7.3,20.2).curveTo(10.7,19.3,14.6,14.6).curveTo(17.7,10.8,18.3,8.8).curveTo(18.5,8.1,18,7.4).curveTo(17.6,6.9,16.8,6.9).curveTo(14.7,7,13.6,11.1).curveTo(12.3,10.3,12.6,8.1).curveTo(12.8,5.9,14.1,5.4).curveTo(8.3,4.5,4.3,6.6).curveTo(0.6,8.6,1,11.7).curveTo(1,15.3,4.4,15.7).curveTo(7.8,16.1,8.2,12.7).curveTo(8.4,10.9,6.4,10.7).curveTo(4.5,10.5,4,12.5).curveTo(3.7,10.6,4.8,9.6).curveTo(5.8,8.5,7.4,8.7).curveTo(9.1,8.7,10.1,9.8).curveTo(11.3,11,11.1,13.1).curveTo(10.9,16.6,7,18.1).curveTo(2.9,19.7,-0,16.9).curveTo(-3.2,13.8,-1.9,9.2).curveTo(-0.8,4.7,3.5,2.8).curveTo(7.6,1,13,1.5).curveTo(18.2,1.8,22.3,3.8).curveTo(20.9,11.9,14.6,17.2).curveTo(8.3,22.6,-0,22.6).curveTo(-8.7,22.6,-15.2,16.9).closePath().moveTo(1.1,-0.8).curveTo(0.6,-1.8,-0.9,-4.2).lineTo(-1.2,-4.5).curveTo(-4.2,-8.5,-4.1,-13).curveTo(-4,-15.5,-2.7,-18.8).curveTo(-6.2,-19.1,-8.9,-17.5).curveTo(-11.6,-15.8,-11,-13.6).curveTo(-10.4,-11.3,-8.2,-12.3).curveTo(-7.5,-12.7,-7.2,-13.4).curveTo(-7.1,-14.1,-8,-14.8).curveTo(-7,-15,-6.2,-14.3).curveTo(-5.5,-13.6,-5.3,-12.5).curveTo(-5,-9.7,-8.6,-9).curveTo(-10.8,-8.5,-12.5,-9.6).curveTo(-14,-10.6,-14.3,-12.5).curveTo(-14.8,-14.5,-14,-16.5).curveTo(-13.2,-18.6,-11.3,-19.7).lineTo(-10.8,-19.9).curveTo(-5.7,-22.6,-0,-22.6).curveTo(4.3,-22.6,8.2,-21.1).curveTo(3.3,-21.1,1,-15.9).curveTo(-1.4,-10.6,2,-6.8).curveTo(3.8,-4.7,6.5,-4.4).curveTo(9.1,-4.2,11.5,-5.5).curveTo(13.8,-6.8,14.5,-8.9).curveTo(15.4,-11.2,13.8,-13.7).curveTo(11.1,-17,8,-15).curveTo(6.7,-14.2,6.4,-12.9).curveTo(5.9,-11.5,6.7,-10.5).curveTo(8.1,-8.5,10,-9.3).curveTo(12.1,-10.1,11.6,-11.9).curveTo(13.1,-9.1,9.8,-7.7).curveTo(6.6,-6.3,4.4,-9).curveTo(2.9,-10.9,3.4,-13.3).curveTo(3.8,-15.6,5.8,-17.1).curveTo(7.8,-18.6,10.3,-18.7).curveTo(13.2,-18.6,15.5,-16.5).curveTo(19.1,-12.6,17.9,-8).curveTo(16.8,-4.3,13.7,-2.2).curveTo(12.1,-1,9.6,-0.5).curveTo(6.5,0,3.8,-0.9).curveTo(3.6,0.4,3.2,1).curveTo(2.7,2,1.6,2.5).curveTo(1.9,1,1.1,-0.8).closePath().moveTo(21.8,-1.4).curveTo(21.6,-1.8,21.1,-2.7).curveTo(20.5,-1.6,18.7,-0.9).curveTo(16.7,-0.2,14.5,-0.3).curveTo(17.4,-1.3,19,-4.6).curveTo(20.5,-8,19.7,-11.3).curveTo(22.6,-6,22.6,-0).lineTo(22.6,1.1).curveTo(22.3,-0.4,21.8,-1.4).closePath().moveTo(-20.6,-9.6).curveTo(-18.9,-13.1,-16.2,-15.9).curveTo(-16.8,-15.2,-17.5,-13.5).curveTo(-18.2,-11.8,-18.6,-9.9).curveTo(-17.8,-10.3,-16.8,-10.3).curveTo(-15.2,-10.4,-14.2,-9.3).curveTo(-16.4,-9,-18.7,-7.2).curveTo(-21.6,-5,-22.6,-1.8).curveTo(-22.4,-5.9,-20.6,-9.6).closePath();
	this.shape.setTransform(75.5,44.5);

	// Layer 7
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#748475").beginStroke().moveTo(-18.9,1.3).curveTo(-23.5,-1.8,-23.5,-4.3).lineTo(-23.5,-5.5).curveTo(-23.5,-5.4,-23.4,-5.4).curveTo(-23.4,-5.4,-23.4,-5.4).curveTo(-23.4,-5.4,-23.4,-5.5).curveTo(-23.3,-5.5,-23.3,-5.6).lineTo(-22.4,-5.6).curveTo(-21.3,-2.6,-18.2,-0.4).curveTo(-12.2,3.9,-0.6,4.2).curveTo(8.7,4.3,15.5,3.1).curveTo(17,2.9,19.3,0.8).curveTo(21.6,-1.3,22.5,-3.3).lineTo(23.2,-3.3).lineTo(23.5,-3.1).lineTo(23.5,-1.8).curveTo(23.5,-0.1,20.4,2.1).curveTo(17.7,4.1,15.8,4.5).curveTo(12.4,5.3,5.6,5.5).lineTo(-0.5,5.5).lineTo(-1.2,5.6).curveTo(-12.5,5.6,-18.9,1.3).closePath();
	this.shape_1.setTransform(72.7,13.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-18.9,10).curveTo(-28.7,7.5,-28.7,4.3).lineTo(-28.7,3.6).curveTo(-22.8,2.3,-8.8,2.4).curveTo(-6.8,2.6,-5.2,4.1).curveTo(-3.7,5.6,-3.7,7.4).lineTo(-4.2,7.9).curveTo(-5,8.5,-6.6,9.1).curveTo(-10,10.2,-16.3,10.6).lineTo(-18.9,10).closePath().moveTo(7.7,7.7).curveTo(6.4,6.2,5.3,1.7).curveTo(8.2,0.8,13.2,0.9).curveTo(20.4,0.9,24.5,2.7).curveTo(28,4.3,28.9,7).curveTo(29.2,7.7,25.9,8.2).curveTo(20.5,8.8,10.5,9.6).curveTo(8.5,8.7,7.7,7.7).closePath().moveTo(-28.7,-3.6).curveTo(-29.1,-6.1,-28.9,-8).curveTo(-27.4,-9.7,-24.3,-10.3).curveTo(-21.2,-10.9,-17,-10.2).curveTo(-16.3,-10,-14.3,-8.6).curveTo(-12.4,-7.3,-12.2,-7.3).lineTo(-12.1,-4.8).lineTo(-12,-2.9).lineTo(-13.2,-3).curveTo(-17.1,-3.4,-20.7,-3.1).curveTo(-22.5,-2.9,-24.1,-2.6).lineTo(-27.9,-0.9).curveTo(-28.3,-1.6,-28.7,-3.6).closePath().moveTo(16.3,-3.9).curveTo(10.7,-4.3,6.2,-3.4).lineTo(4.4,-3).lineTo(4.4,-4.3).curveTo(4.3,-6.6,6.7,-7.8).curveTo(14.5,-9.3,22.8,-8).curveTo(22.9,-6.7,22.4,-4).curveTo(22.4,-3.4,22.5,-2.8).curveTo(18.7,-3.7,16.3,-3.9).closePath();
	this.shape_2.setTransform(76.7,148.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#BABABA").beginStroke().moveTo(-16.8,4.1).curveTo(-22.1,3.6,-22.9,3.3).lineTo(-22,0.9).curveTo(-21.4,-0.6,-19.9,-1.4).lineTo(-18.5,-2.1).lineTo(-17.9,-2.2).lineTo(-16.3,-2.7).lineTo(-16.4,-2).curveTo(-16.4,1.3,-6.5,3.8).lineTo(-3.9,4.3).curveTo(-6.8,4.5,-10.3,4.5).curveTo(-12,4.5,-16.8,4.1).closePath().moveTo(13.1,1.4).curveTo(13.1,0,12.8,-1.2).curveTo(13.4,-1.2,13.9,-1.9).curveTo(14.7,-2.9,15.2,-3.3).curveTo(16.2,-4,17.7,-4.5).curveTo(18.7,-0,20.1,1.5).curveTo(20.9,2.5,22.9,3.3).lineTo(15.7,3.9).curveTo(13.6,4,13,4.1).curveTo(13.2,3.1,13.1,1.4).closePath();
	this.shape_3.setTransform(64.4,154.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#000000").beginStroke().moveTo(-30.1,80.9).curveTo(-36.2,79.9,-36.9,78.8).curveTo(-37.7,77.7,-37.7,75.5).curveTo(-37.7,70.4,-32.5,66.8).lineTo(-29.1,65.2).curveTo(-30.4,63.3,-30.6,59.6).lineTo(-30.8,57.9).curveTo(-30.8,56.2,-30.1,54).curveTo(-22.7,50.4,-16.3,51.1).curveTo(-12.9,51.4,-8.1,53.6).curveTo(-5.6,54.7,-5.3,56.7).curveTo(-5.2,57.9,-5.5,60.4).curveTo(-5.5,62.2,-5.8,63.5).curveTo(-3.3,64.4,-0.2,66).lineTo(-0.1,66).curveTo(0.6,64.9,2,64.1).lineTo(2,61.2).curveTo(2,57.5,2.3,56.8).curveTo(3,55.7,7.8,52.8).curveTo(17.6,51.1,26.9,52.8).curveTo(28,53.6,28.6,54.3).lineTo(29.4,54.4).curveTo(30.4,55.3,30.8,56.2).curveTo(31.3,57.2,31.3,58.6).curveTo(31.3,62.1,30.3,64.3).lineTo(31.7,65).curveTo(38,68.6,37.7,73.9).curveTo(37.6,75.4,37.1,75.9).curveTo(36.6,76.6,34.7,77.1).curveTo(33.3,77.4,17.3,79.4).curveTo(10.7,80.1,5.2,79.4).curveTo(3,79,2.9,78.3).lineTo(2.6,76.7).lineTo(2.2,77.3).curveTo(0.1,79.8,-9,80.9).curveTo(-14.8,81.3,-20,81.3).curveTo(-25.3,81.3,-30.1,80.9).closePath().moveTo(-26.5,68.9).lineTo(-28.1,69.3).lineTo(-28.7,69.5).lineTo(-30.1,70.1).curveTo(-31.6,70.9,-32.2,72.4).lineTo(-33,74.8).curveTo(-32.3,75.1,-27,75.6).curveTo(-22.1,76,-20.4,76).curveTo(-17,76,-14.1,75.9).curveTo(-7.8,75.5,-4.4,74.4).curveTo(-2.8,73.8,-2,73.2).lineTo(-1.5,72.7).curveTo(-1.5,70.9,-3,69.4).curveTo(-4.6,67.9,-6.6,67.7).lineTo(-8.5,67.6).curveTo(-21.1,67.6,-26.5,68.9).closePath().moveTo(7.5,67).curveTo(6,67.5,5,68.2).curveTo(4.5,68.6,3.7,69.6).curveTo(3.2,70.3,2.6,70.3).curveTo(3,71.6,3,72.9).curveTo(3,74.6,2.8,75.7).curveTo(3.5,75.5,5.5,75.4).lineTo(12.7,74.9).curveTo(22.7,74.1,28.1,73.5).curveTo(31.4,73,31.1,72.3).curveTo(30.2,69.6,26.7,68).curveTo(22.6,66.2,15.4,66.2).lineTo(14.8,66.2).curveTo(10.2,66.2,7.5,67).closePath().moveTo(-22.1,55).curveTo(-25.2,55.6,-26.7,57.3).curveTo(-26.9,59.2,-26.5,61.7).curveTo(-26.1,63.7,-25.7,64.4).lineTo(-21.9,62.7).curveTo(-20.3,62.4,-18.5,62.2).curveTo(-14.9,61.9,-11,62.3).lineTo(-9.9,62.4).lineTo(-9.9,60.5).lineTo(-10,58).curveTo(-10.2,58,-12.1,56.7).curveTo(-14.1,55.3,-14.8,55.1).curveTo(-17,54.7,-18.9,54.7).curveTo(-20.6,54.7,-22.1,55).closePath().moveTo(24.7,62.5).curveTo(24.5,61.9,24.6,61.3).curveTo(25.1,58.6,25,57.3).curveTo(16.7,56,8.9,57.5).curveTo(6.5,58.7,6.6,61).lineTo(6.6,62.3).lineTo(8.4,61.9).curveTo(12.9,61,18.5,61.4).curveTo(20.9,61.6,24.7,62.5).lineTo(24.7,62.5).closePath().moveTo(29.6,58.9).lineTo(29.8,58.4).lineTo(29.7,57.9).lineTo(29.6,58.9).closePath().moveTo(-11.1,-71.3).curveTo(-18.8,-73.8,-19.3,-76.1).curveTo(-19.7,-77.7,-16.4,-79.4).curveTo(-13.2,-81.1,-4.9,-81.2).curveTo(3.4,-81.4,11.2,-79.4).curveTo(18.9,-77.6,17.9,-75.5).curveTo(17.6,-74.8,17.2,-73.4).curveTo(16.5,-72,14.5,-70.9).curveTo(12.1,-69.7,3.1,-69.7).curveTo(-6.3,-69.7,-11.1,-71.3).closePath();
	this.shape_4.setTransform(74.5,83.2);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#748474").beginStroke().moveTo(31.2,32).lineTo(26.8,31.4).curveTo(19.5,30.4,7.2,29.1).lineTo(6.3,29).lineTo(2.7,28.5).lineTo(-4.9,27.4).lineTo(-4.9,27.4).curveTo(-7.2,26.1,-10.2,25.5).lineTo(-11.7,25.2).lineTo(-11,23.4).curveTo(-9.6,19.5,-8.8,13.7).lineTo(-7.4,5.5).curveTo(-6.9,2.9,-6.9,-0.2).lineTo(-6.9,-1.2).lineTo(-6.9,-1.3).lineTo(-7,-2.2).lineTo(-7,-2.9).curveTo(-6.4,-3.2,-5.7,-3.3).curveTo(-5.2,-3.3,-4.3,-2.1).curveTo(-3.5,-1,-3.1,0.1).curveTo(-2.8,0.8,-2,5.5).curveTo(-0.5,10.9,3.4,15.3).curveTo(10,22.7,21.9,26.2).lineTo(29.5,28.5).lineTo(30.7,29.1).curveTo(33.1,30.3,34,32.1).curveTo(34.1,32.2,33.5,32.2).lineTo(31.2,32).closePath().moveTo(-30.6,25.2).lineTo(-30.6,25.2).lineTo(-30.8,25.1).lineTo(-30.9,25).lineTo(-31.6,24.4).lineTo(-31.9,24.5).curveTo(-33.2,24.5,-33.8,17.3).lineTo(-34,10.1).lineTo(-34,3.7).curveTo(-33.8,3,-33.1,1.1).curveTo(-32.9,-4.2,-27.8,-15.1).curveTo(-26,-19,-24.8,-21.1).curveTo(-24.8,-22.5,-25,-23.4).curveTo(-23.2,-25.1,-21.1,-26.5).curveTo(-16.6,-29.7,-9.2,-32.2).curveTo(-11.3,-30.6,-14.8,-25.9).curveTo(-18.6,-20.8,-20.7,-16.2).curveTo(-21.3,-14.7,-21.8,-11.5).curveTo(-22,-10,-22.5,-9.6).lineTo(-22.5,-9.4).curveTo(-22.8,-6.6,-26.1,7.5).curveTo(-26.9,11.2,-27.2,16.1).curveTo(-27.3,18.5,-27.4,23.8).lineTo(-27,23.8).lineTo(-27,24.6).lineTo(-27,25.6).curveTo(-26.8,26.4,-25.8,26.5).curveTo(-28.9,26.3,-30.6,25.2).closePath();
	this.shape_5.setTransform(37.8,43.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#A5BCA5").beginStroke().moveTo(1.7,35.6).curveTo(0.8,33.8,-1.6,32.6).lineTo(-2.8,32).lineTo(-10.4,29.7).curveTo(-22.4,26.2,-29,18.8).curveTo(-32.9,14.4,-34.3,9).curveTo(-35.1,4.3,-35.4,3.6).curveTo(-35.9,2.5,-36.6,1.4).curveTo(-37.5,0.2,-38,0.2).curveTo(-38.7,0.3,-39.4,0.6).curveTo(-39.7,-1.3,-40.4,-1.8).curveTo(-41.4,-2.4,-42.9,-0.4).lineTo(-43.6,0.7).lineTo(-43.6,1.1).curveTo(-43.6,2.2,-44.3,4.9).lineTo(-44.6,5.9).lineTo(-44.5,8).curveTo(-44.5,8.9,-44.9,11.2).lineTo(-44.9,11.4).lineTo(-45.4,13.5).lineTo(-45.5,14.2).lineTo(-45.8,15.5).lineTo(-46.9,20.3).lineTo(-46.5,19.4).lineTo(-47.3,22.6).curveTo(-48.3,26.2,-48.3,27).lineTo(-48.2,28.4).lineTo(-48.3,29.1).lineTo(-48.3,29.3).curveTo(-51.9,30.1,-54.7,30).lineTo(-53.6,29.8).lineTo(-55.3,29.8).curveTo(-56.4,30,-57.3,30.1).lineTo(-57.6,30.1).lineTo(-58.1,30).curveTo(-59.1,29.9,-59.4,29.1).lineTo(-59.4,28.1).lineTo(-59.4,27.3).lineTo(-59.7,27.3).curveTo(-59.7,22,-59.5,19.6).curveTo(-59.2,14.7,-58.5,11).curveTo(-55.2,-3.1,-54.9,-5.9).lineTo(-54.9,-6.1).curveTo(-54.4,-6.5,-54.2,-8).curveTo(-53.7,-11.2,-53,-12.7).curveTo(-51,-17.3,-47.1,-22.4).curveTo(-43.7,-27.1,-41.6,-28.7).lineTo(-29.2,-32.9).curveTo(-25.5,-34.2,-23.9,-34.9).lineTo(-21.8,-35.3).lineTo(-19.3,-35.9).lineTo(11.2,-35.9).curveTo(20.7,-34.5,23.1,-34.5).curveTo(24.2,-34.5,24.6,-34.3).curveTo(29.1,-32.4,37.9,-27.5).curveTo(38.9,-27,41.5,-25.2).curveTo(48,-21.1,50,-19.1).curveTo(50.8,-18.2,54.5,-10.2).curveTo(60.9,18.6,59.5,24.1).curveTo(56.8,29.5,50.7,28.2).curveTo(50,28,50.1,27).curveTo(50.4,25.7,50.3,25.3).curveTo(50,19.1,50.1,18.5).curveTo(50.9,15,51.1,13).curveTo(51.3,10.6,51.2,8.4).lineTo(51,6.4).lineTo(50.9,4.9).lineTo(50.7,3.2).lineTo(50.6,2.9).curveTo(50.4,1.7,49.5,-1.5).curveTo(48.6,-4.7,47.5,-6.5).curveTo(46.2,-8.8,44.4,-11).curveTo(41.7,-14.2,39.7,-14.6).curveTo(37.6,-15,37.6,-11.7).curveTo(37.6,-11.2,41.8,-5.4).curveTo(46.1,0.5,46.6,6.8).curveTo(47.2,12.6,44.3,16.9).curveTo(42.9,19.1,41.4,20.4).curveTo(41.4,21.5,41.6,21.7).lineTo(41.8,22.1).curveTo(42.1,22.4,44,22.4).curveTo(44.6,22.4,45.4,22).lineTo(46.7,21.4).lineTo(46.9,27.2).curveTo(47.1,30.9,47.5,32).lineTo(47.7,32.3).lineTo(46.8,32.6).lineTo(45.3,33.1).curveTo(43.6,33.8,41.3,34.3).curveTo(34.9,35.7,18.5,35.9).lineTo(14.6,35.9).curveTo(7.2,35.9,1.7,35.6).closePath();
	this.shape_6.setTransform(70.2,39.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#396086").beginStroke().moveTo(4.2,15.4).curveTo(-1.1,-1.5,-2.8,-4.3).curveTo(-9.8,-15.8,-12.5,-28.8).lineTo(-11.8,-32.1).lineTo(-11.1,-35.8).curveTo(-8.2,-34.5,1.8,-32.9).lineTo(2.6,-32.8).lineTo(3,-32.7).lineTo(4.9,-32.5).curveTo(4.9,-20.3,7.8,-4.2).curveTo(8.2,-2,9.9,15).curveTo(11.4,31,12.6,35.8).curveTo(9.2,31.4,4.2,15.4).closePath();
	this.shape_7.setTransform(36.8,109.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#396087").beginStroke().moveTo(-9,33.6).curveTo(-11.1,33.5,-12.5,33.1).lineTo(-15.9,31.8).curveTo(-17.5,31.1,-18.3,30.4).lineTo(-18.9,29.9).curveTo(-28.9,-0.6,-29.9,-3.5).curveTo(-35,-17.3,-35,-31.7).lineTo(-34.7,-33.6).curveTo(-31.9,-20.6,-25,-9.1).curveTo(-23.3,-6.3,-18,10.6).curveTo(-13,26.6,-9.6,31).lineTo(-7.6,33.6).closePath().moveTo(22.3,32.9).lineTo(15.9,31.9).lineTo(15.9,31.2).curveTo(15.7,28.6,15.5,15.8).lineTo(15.1,1.7).curveTo(16.8,4.5,19.1,6.9).curveTo(20.2,8,26,21).curveTo(31.1,32.5,35,33.5).lineTo(33.4,33.5).curveTo(27.9,33.5,22.3,32.9).closePath();
	this.shape_8.setTransform(58.9,114.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#371E2D").beginStroke().moveTo(9.7,1.3).lineTo(2.3,0).lineTo(2.3,-1.3).lineTo(2.3,-1.5).lineTo(2.6,-1.5).lineTo(4.1,-1.2).curveTo(7.1,-0.6,9.4,0.7).lineTo(9.5,0.7).lineTo(9.5,0.8).lineTo(10.6,1.5).lineTo(9.7,1.3).closePath().moveTo(-10.6,-0.1).curveTo(-9.7,-0.2,-8.6,-0.4).lineTo(-6.9,-0.4).lineTo(-8,-0.2).lineTo(-10.2,-0.1).lineTo(-10.6,-0.1).closePath();
	this.shape_9.setTransform(23.5,70.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#4B7DAF").beginStroke().moveTo(-30.6,32.9).curveTo(-31.7,28.1,-33.3,12.2).curveTo(-34.9,-4.9,-35.3,-7).curveTo(-38.2,-23.1,-38.3,-35.3).curveTo(-31.4,-34.5,-15.1,-33).curveTo(-6.2,-32.2,10,-32.7).curveTo(27.2,-33.3,36,-34.9).lineTo(38.3,-35.5).lineTo(37.9,-19.6).curveTo(37.1,-9.5,34.3,-1).curveTo(32.6,3.8,29.4,12.2).curveTo(29,13.3,27.8,23.7).lineTo(26.5,34.6).lineTo(22,34.9).curveTo(17.2,35.4,14.1,35.5).curveTo(10.2,34.4,5,23).curveTo(-0.8,10,-1.9,8.9).curveTo(-4.2,6.5,-5.8,3.6).lineTo(-6.3,-5.1).curveTo(-6,-10.7,-6.2,-13.1).curveTo(-10,-12.8,-10.6,-8.8).curveTo(-10.9,-6.7,-10.4,-4.6).lineTo(-9.9,4.7).lineTo(-9.6,18.8).lineTo(-9.3,32.1).lineTo(-9.4,33.2).curveTo(-9.4,33.9,-11.5,34.2).lineTo(-28.5,35.5).closePath();
	this.shape_10.setTransform(79.9,112.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#000000").beginStroke().moveTo(14.5,75.2).curveTo(8.8,74.7,7.4,74).lineTo(5.7,73.2).lineTo(5.1,72.9).lineTo(5.1,72.9).lineTo(4,73.4).curveTo(-0.3,75.4,-7.7,75.8).curveTo(-11.5,76.3,-15.4,75.7).curveTo(-17.8,75.3,-25.5,73.4).curveTo(-28,72.8,-28.3,72.5).curveTo(-31.4,69.8,-38.5,47.8).curveTo(-46.5,22.9,-46.7,7.2).curveTo(-46.7,0.8,-46.2,-1.5).curveTo(-46,-2.5,-45.7,-3.3).lineTo(-49.8,-3.2).lineTo(-50.8,-3.2).lineTo(-52.7,-3.2).curveTo(-53.6,-3.2,-54.8,-3.4).curveTo(-56.4,-3.6,-58.6,-4.3).lineTo(-59.9,-4.7).lineTo(-60.4,-4.8).curveTo(-62.6,-5.5,-63.6,-6).lineTo(-64.3,-5.9).curveTo(-65.3,-6.4,-65.8,-9.5).curveTo(-66.3,-11.8,-66.3,-13.1).curveTo(-66.6,-15.4,-66.4,-18.5).curveTo(-65.9,-22.9,-65.9,-24.5).curveTo(-65.8,-33.6,-62.9,-43.6).lineTo(-62.7,-43.8).lineTo(-60.7,-47.9).lineTo(-59.3,-50.6).lineTo(-57.9,-53.9).curveTo(-57.3,-55.1,-55.6,-57.5).lineTo(-55.4,-57.7).lineTo(-55.3,-57.7).lineTo(-53,-61.4).curveTo(-52.9,-61.6,-51.4,-62.7).curveTo(-49.9,-63.8,-49.1,-64.3).curveTo(-37.8,-70.9,-20.3,-74.2).lineTo(-6.9,-76.1).lineTo(15.1,-76.1).curveTo(18.9,-75.6,24.4,-74.6).curveTo(30.1,-73.6,30.2,-73.3).lineTo(30.3,-73.3).lineTo(35.8,-70.7).curveTo(41.3,-68,43.6,-67).curveTo(47.7,-65.3,50.7,-63).curveTo(60.6,-55.5,62.4,-45).lineTo(65.2,-28.1).curveTo(66.3,-21,66.4,-18.8).curveTo(66.5,-13.8,66.4,-13.1).lineTo(66.5,-12).curveTo(66.5,-9,64.3,-6.8).curveTo(63,-5.5,61.2,-4.8).curveTo(60.2,-4.5,59.1,-4.3).curveTo(57.9,-4.1,56.5,-4.1).lineTo(55.2,-4.1).lineTo(55.6,-1.4).curveTo(56.1,2.8,56.1,4.1).curveTo(56.1,13.4,55.9,16.2).curveTo(55.2,24.5,52.7,32.3).curveTo(51.4,36.5,47.9,54.5).curveTo(44.9,69.9,43.1,72.3).curveTo(43.3,72.4,43,73).curveTo(42.6,73.5,42.1,73.6).curveTo(42.5,73.8,40.4,74.3).lineTo(36,75.2).curveTo(30.3,76.1,25.3,76.1).curveTo(20.2,76.1,14.5,75.2).closePath().moveTo(-41.6,1).lineTo(-42.3,4.3).lineTo(-42.6,6.1).curveTo(-42.6,20.5,-37.5,34.4).curveTo(-36.4,37.2,-26.5,67.8).lineTo(-25.9,68.3).curveTo(-25.1,68.9,-23.5,69.7).lineTo(-20.1,71).curveTo(-18.7,71.4,-16.5,71.4).lineTo(-15.1,71.4).lineTo(1.9,70.1).curveTo(4,69.8,4,69.1).lineTo(4.1,68).lineTo(3.8,54.7).lineTo(3.5,40.6).lineTo(3,31.3).curveTo(2.5,29.2,2.8,27.1).curveTo(3.4,23.1,7.2,22.8).curveTo(7.4,25.2,7.1,30.8).lineTo(7.6,39.5).lineTo(7.9,53.7).curveTo(8.2,66.4,8.4,69).lineTo(8.4,69.8).lineTo(14.8,70.8).curveTo(21.1,71.5,27.5,71.4).curveTo(30.6,71.3,35.4,70.8).lineTo(39.9,70.5).lineTo(41.2,59.6).curveTo(42.4,49.2,42.8,48.1).curveTo(46,39.7,47.7,34.9).curveTo(50.5,26.4,51.3,16.3).lineTo(51.7,0.4).lineTo(49.4,1).curveTo(40.6,2.6,23.4,3.2).curveTo(7.2,3.7,-1.7,2.9).curveTo(-18,1.4,-24.8,0.6).lineTo(-26.7,0.3).lineTo(-27.1,0.3).lineTo(-27.9,0.2).curveTo(-37.9,-1.5,-40.8,-2.7).lineTo(-41.6,1).closePath().moveTo(22.2,-1).curveTo(38.6,-1.2,45,-2.6).curveTo(47.3,-3.1,49,-3.7).lineTo(50.5,-4.3).lineTo(51.4,-4.5).lineTo(51.3,-4.8).curveTo(50.9,-6,50.6,-9.7).lineTo(50.4,-15.4).lineTo(49.1,-14.8).curveTo(48.3,-14.4,47.7,-14.4).curveTo(45.8,-14.4,45.5,-14.8).lineTo(45.3,-15.1).curveTo(45.1,-15.4,45.1,-16.5).curveTo(46.6,-17.8,48,-20).curveTo(50.9,-24.3,50.3,-30.1).curveTo(49.8,-36.4,45.5,-42.3).curveTo(41.3,-48,41.3,-48.5).curveTo(41.3,-51.9,43.4,-51.5).curveTo(45.4,-51.1,48.1,-47.8).curveTo(49.9,-45.6,51.2,-43.4).curveTo(52.3,-41.6,53.2,-38.4).curveTo(54.1,-35.2,54.3,-34).lineTo(54.6,-32).lineTo(54.7,-30.5).lineTo(54.9,-28.4).curveTo(55,-26.2,54.8,-23.9).curveTo(54.6,-21.8,53.8,-18.3).curveTo(53.7,-17.7,54,-11.6).curveTo(54.1,-11.1,53.8,-9.9).curveTo(53.7,-8.8,54.4,-8.7).curveTo(60.5,-7.3,63.2,-12.7).curveTo(64.6,-18.2,58.2,-47).curveTo(54.5,-55.1,53.7,-55.9).curveTo(51.7,-57.9,45.2,-62).curveTo(42.6,-63.9,41.7,-64.4).curveTo(32.8,-69.3,28.3,-71.2).curveTo(27.9,-71.4,26.8,-71.4).curveTo(24.4,-71.4,14.9,-72.8).lineTo(-15.6,-72.8).lineTo(-18.1,-72.1).lineTo(-20.1,-71.8).curveTo(-21.8,-71,-25.5,-69.7).lineTo(-37.9,-65.5).curveTo(-45.3,-63,-49.8,-59.9).curveTo(-51.8,-58.5,-53.6,-56.7).curveTo(-53.4,-55.9,-53.4,-54.4).curveTo(-54.6,-52.4,-56.5,-48.4).curveTo(-61.6,-37.6,-61.8,-32.2).curveTo(-62.5,-30.3,-62.6,-29.6).lineTo(-62.7,-23.2).lineTo(-62.4,-16).curveTo(-61.9,-8.9,-60.6,-8.9).lineTo(-60.2,-8.9).lineTo(-59.6,-8.3).lineTo(-59.5,-8.3).lineTo(-59.3,-8.2).lineTo(-59.2,-8.1).curveTo(-57.5,-7,-54.4,-6.8).lineTo(-53.9,-6.8).lineTo(-53.6,-6.8).lineTo(-53.1,-6.8).lineTo(-51,-6.9).curveTo(-48.2,-6.8,-44.6,-7.5).lineTo(-44.6,-7.7).lineTo(-44.5,-8.5).lineTo(-44.6,-9.8).curveTo(-44.6,-10.7,-43.6,-14.2).lineTo(-42.8,-17.5).lineTo(-43.2,-16.6).lineTo(-42.1,-21.3).lineTo(-41.8,-22.6).lineTo(-41.7,-23.3).lineTo(-41.2,-25.5).lineTo(-41.2,-25.6).curveTo(-40.8,-27.9,-40.8,-28.8).lineTo(-40.9,-30.9).lineTo(-40.6,-32).curveTo(-39.9,-34.7,-39.9,-35.8).lineTo(-39.9,-36.2).lineTo(-39.2,-37.2).curveTo(-37.7,-39.3,-36.7,-38.6).curveTo(-36,-38.1,-35.7,-36.3).lineTo(-35.6,-35.6).lineTo(-35.5,-34.7).lineTo(-35.5,-34.5).lineTo(-35.5,-33.5).curveTo(-35.5,-30.5,-36,-27.9).lineTo(-37.4,-19.7).curveTo(-38.3,-13.8,-39.6,-10).lineTo(-40.3,-8.1).lineTo(-40.6,-8.1).lineTo(-40.6,-7.9).lineTo(-40.6,-6.6).lineTo(-33.2,-5.4).lineTo(-32.3,-5.2).lineTo(-33.4,-5.9).lineTo(-33.5,-5.9).lineTo(-25.9,-4.8).lineTo(-22.3,-4.4).lineTo(-21.4,-4.3).curveTo(-9.2,-2.9,-1.9,-1.9).lineTo(2.6,-1.3).curveTo(5.5,-1,5.4,-1.3).curveTo(12.3,-0.9,22.2,-1).closePath().moveTo(-63.3,-7.1).lineTo(-63.2,-7.1).lineTo(-63.3,-7.1).closePath();
	this.shape_11.setTransform(66.5,76.8);

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0.7,133,163.8);


(lib.Girl02 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#9B4D00").beginStroke().moveTo(2.3,3).curveTo(-1.6,2.6,-6.5,2.6).curveTo(-8.7,2.7,-9.4,0.3).lineTo(-9.9,-2.9).curveTo(-10.3,-4.5,-0.2,-1.7).curveTo(9.9,1.1,9.9,2.1).lineTo(9.9,2.8).lineTo(9.7,3.1).lineTo(9.4,3.3).curveTo(9.1,3.5,8.2,3.5).curveTo(6.3,3.5,2.3,3).closePath();
	this.shape.setTransform(72.3,3.1);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#CC6600").beginStroke().moveTo(27.7,7.7).lineTo(27.7,6.6).curveTo(27.7,4,25.1,1.2).curveTo(21.9,-2.3,16.5,-3.8).lineTo(16.1,-4).lineTo(10.3,-6).lineTo(5.3,-7.6).lineTo(4.2,-8.1).curveTo(15.3,-6.9,22.3,-4.7).curveTo(25.5,-3.7,28.3,-2.2).lineTo(30.2,-1.1).curveTo(32,-0.1,32.1,1).curveTo(32.5,1.8,31.9,3.2).curveTo(31.8,3.5,31.4,5.1).lineTo(31,6.9).curveTo(30.8,7.6,30.3,8.4).lineTo(29.4,8.6).lineTo(29.1,8.6).curveTo(28.4,8.3,27.7,7.7).closePath().moveTo(-8.5,-1.1).curveTo(-9,-1.2,-9.4,-1.4).curveTo(-9.6,-1.3,-11.1,-1.7).lineTo(-16.3,-2.8).lineTo(-18,-3).lineTo(-19.9,-3.1).lineTo(-20.3,-3.1).lineTo(-20.4,-3.1).curveTo(-24.6,-3.6,-28.5,-4.4).curveTo(-32.2,-5.3,-32.2,-5.7).curveTo(-32.2,-7.3,-25.7,-8.2).curveTo(-19.2,-9.1,-15.4,-7.9).lineTo(-15.2,-7.9).lineTo(-14.5,-7.9).curveTo(-11.6,-7.9,-9.9,-7.2).curveTo(-8,-6.4,-6.9,-4.7).curveTo(-5.8,-3.8,-4.9,-1.7).lineTo(-4.7,-0.4).lineTo(-4.6,0.5).lineTo(-5.5,0.6).lineTo(-8.5,-1.1).closePath();
	this.shape_1.setTransform(62,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#5A986F").beginStroke().moveTo(-1.8,-0.8).lineTo(-1.6,-0.7).curveTo(0.7,-0,1.8,0.8).curveTo(0.1,-0.1,-1.8,-0.8).closePath();
	this.shape_2.setTransform(88.4,-0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-8,20.8).lineTo(-8.6,19.1).curveTo(-8.6,17.9,-7.7,17.5).curveTo(-8,16.5,-8.7,12.3).curveTo(-9.6,7.6,-10,5.9).lineTo(-12.5,-4.4).lineTo(-12.9,-4.3).curveTo(-13.8,-4.3,-15.7,-5.7).lineTo(-19.5,-8.5).curveTo(-25.3,-12.4,-30.4,-11.2).lineTo(-32.3,-12.7).curveTo(-32.3,-14.5,-29.7,-14.5).lineTo(-29.6,-14.5).lineTo(-29.2,-14.4).lineTo(-27.3,-14.4).lineTo(-25.6,-14.2).lineTo(-20.5,-13).curveTo(-19,-12.6,-18.7,-12.8).curveTo(-18.4,-12.5,-17.9,-12.4).lineTo(-14.9,-10.7).lineTo(-13.9,-10.2).lineTo(-13.8,-10.1).lineTo(-13.8,-10.2).lineTo(-13.9,-10.3).lineTo(-13.9,-10.5).lineTo(-13.9,-10.9).lineTo(-14,-11.8).lineTo(-14.3,-13).curveTo(-15.2,-15.2,-16.2,-16.1).curveTo(-17.3,-17.7,-19.2,-18.5).curveTo(-21,-19.2,-23.8,-19.3).lineTo(-24.6,-19.3).lineTo(-23.9,-19.9).curveTo(-23.4,-20.2,-23,-20.2).curveTo(-22.4,-21.1,-20.5,-21.1).lineTo(-18.5,-21).lineTo(-18.7,-21.3).curveTo(-18.7,-21.3,-18.7,-21.4).curveTo(-18.7,-21.5,-18.7,-21.5).curveTo(-18.7,-21.5,-18.6,-21.6).curveTo(-18.6,-21.6,-18.6,-21.6).curveTo(-16.1,-23.2,-5.5,-22.3).curveTo(6.2,-21.3,15.2,-17.8).curveTo(17.1,-17.1,18.8,-16.2).lineTo(19.3,-16).curveTo(26.3,-12.5,26.3,-8.8).curveTo(26.3,-6.3,25.6,-3.8).curveTo(24.9,-1.5,23.8,-0.5).curveTo(25.8,2,26,2.4).curveTo(27.2,4.1,28.1,6.5).curveTo(30.6,13.5,30.9,16.4).curveTo(32.3,16.5,32.3,18.9).curveTo(32.3,20.2,32.1,20.5).curveTo(31.5,21.6,29,21.6).lineTo(26.9,21.5).lineTo(26,21.3).lineTo(25.7,20.1).curveTo(26.5,18.9,27.6,17.8).curveTo(27,16.8,25,11.1).curveTo(22.7,4,21.2,0.5).curveTo(20.3,0.5,19.8,0.3).lineTo(19.4,0.2).curveTo(19.4,0.2,19.4,0.1).curveTo(19.4,0.1,19.4,0.1).curveTo(19.4,0,19.3,-0).curveTo(19.3,-0,19.2,-0.1).curveTo(19.1,-0.2,19.1,-1).lineTo(20.1,-2.8).lineTo(20.9,-3).curveTo(21.5,-3.7,21.7,-4.4).lineTo(22.1,-6.3).curveTo(22.4,-7.9,22.6,-8.1).curveTo(23.2,-9.5,22.7,-10.4).curveTo(22.7,-11.4,20.9,-12.4).lineTo(19,-13.5).curveTo(16.1,-15.1,12.9,-16.1).curveTo(6,-18.2,-5.2,-19.4).lineTo(-4,-19).lineTo(1,-17.3).lineTo(6.8,-15.3).lineTo(7.2,-15.1).curveTo(12.5,-13.6,15.7,-10.2).curveTo(18.4,-7.3,18.4,-4.7).lineTo(18.4,-3.7).lineTo(18.2,-1.6).lineTo(17.8,-1.4).curveTo(17.5,-1.2,16.5,-1.2).curveTo(15.1,-1.2,13.9,-4.5).lineTo(13.1,-7.2).lineTo(11.6,-7.1).curveTo(5.7,-5.9,2.8,-5.9).curveTo(-3.9,-5.9,-8.1,-7.9).curveTo(-7.6,-6.9,-7.4,-6.2).lineTo(-3.4,-3.5).curveTo(1.3,-0.2,1.3,-0).curveTo(1.3,0.8,0.4,1.2).curveTo(-0.2,1.5,-1.1,1.5).lineTo(-7.1,-2.1).curveTo(-6.7,0.1,-6.7,3).curveTo(-6.6,3.1,-5.3,8.6).curveTo(-4.6,11.8,-4,18.5).curveTo(-2.7,19.2,-2.7,20).curveTo(-2.7,21,-4.2,21.9).curveTo(-5.5,22.6,-6,22.6).curveTo(-7.1,22.6,-8,20.8).closePath().moveTo(-6.1,-11.1).curveTo(-4.1,-10.5,-2.8,-10.3).curveTo(-4.1,-11.1,-5,-11.4).lineTo(-6.1,-11.1).closePath().moveTo(-14.6,-19.9).lineTo(-14.6,-19.9).lineTo(-14.5,-20.2).lineTo(-15.6,-20.3).lineTo(-14.6,-19.9).closePath();
	this.shape_3.setTransform(71.4,16.9);

	// Layer 4
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#000000").beginStroke().moveTo(3.4,8.3).curveTo(-5.2,8.2,-9.7,5.7).curveTo(-16.8,1.9,-17.3,-1.8).curveTo(-17.6,-4.3,-13.4,-6.3).curveTo(-9.2,-8.3,-1,-8.3).curveTo(7.2,-8.3,12.7,-6.2).curveTo(18.1,-4.1,17.2,-0.9).curveTo(16.9,0.1,16.5,2.5).curveTo(15.9,4.6,14,6.3).curveTo(11.8,8.3,3.8,8.3).lineTo(3.4,8.3).closePath();
	this.shape_4.setTransform(77.8,11.5);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill().beginStroke("#4E2A3F").setStrokeStyle(1,1,1).moveTo(24.6,1.1).curveTo(22.1,0.5,19,0.2).curveTo(10.9,-1.1,5.2,-1.7).curveTo(-5.3,-2.8,-10,-1.6).curveTo(-12,-1.2,-17.2,-0.5).curveTo(-20.9,0,-22.5,0.9).curveTo(-23.7,1.6,-24.6,2.2);
	this.shape_5.setTransform(69.8,2.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#9B4D00").beginStroke().moveTo(26.8,31.5).curveTo(19.2,30.5,6.3,29).curveTo(0.4,28.3,-4.8,27.5).lineTo(-4.9,27.5).curveTo(-7.2,26.2,-10.2,25.5).lineTo(-11.7,25.3).lineTo(-11,23.4).curveTo(-9.6,19.6,-8.8,13.7).lineTo(-7.4,5.5).curveTo(-6.9,2.9,-6.9,-0.1).lineTo(-6.9,-1.1).lineTo(-6.9,-1.3).lineTo(-7,-2.2).lineTo(-7,-2.9).curveTo(-6.4,-3.1,-5.7,-3.2).curveTo(-5.2,-3.3,-4.3,-2).curveTo(-3.5,-1,-3.1,0.2).curveTo(-2.8,0.8,-2,5.6).curveTo(-0.5,10.9,3.4,15.3).curveTo(7.3,19.7,12.9,21.9).curveTo(16.1,23.2,22.3,24.3).curveTo(27.8,25.3,29.9,26.4).curveTo(33.2,28.2,34,32.1).curveTo(29.8,31.9,26.8,31.5).closePath().moveTo(-30.6,25.3).lineTo(-30.6,25.2).lineTo(-30.8,25.1).lineTo(-30.9,25.1).lineTo(-31.6,24.5).lineTo(-31.9,24.5).curveTo(-33.2,24.5,-33.8,17.4).lineTo(-34,10.2).lineTo(-34,3.8).curveTo(-33.8,3.1,-33.1,1.2).curveTo(-32.9,-4.2,-27.8,-15).curveTo(-26,-19,-24.8,-21).curveTo(-24.8,-22.5,-25,-23.3).curveTo(-23.2,-25.1,-21.1,-26.5).curveTo(-16.6,-29.6,-9.2,-32.1).curveTo(-11.3,-30.6,-14.8,-25.9).curveTo(-18.6,-20.7,-20.7,-16.1).curveTo(-21.3,-14.7,-21.8,-11.5).curveTo(-22,-9.9,-22.5,-9.6).lineTo(-22.5,-9.4).curveTo(-22.8,-6.6,-26.1,7.6).curveTo(-26.9,11.2,-27.2,16.2).curveTo(-27.3,18.5,-27.4,23.8).lineTo(-27,23.9).lineTo(-27,24.7).lineTo(-27,25.7).curveTo(-26.8,26.4,-25.8,26.6).curveTo(-28.9,26.4,-30.6,25.3).closePath();
	this.shape_6.setTransform(37.8,43.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#A55200").beginStroke().moveTo(-16.8,4.1).curveTo(-22.1,3.6,-22.9,3.3).lineTo(-22,0.9).curveTo(-21.4,-0.6,-19.9,-1.4).lineTo(-18.5,-2.1).lineTo(-17.9,-2.2).lineTo(-16.3,-2.7).lineTo(-16.4,-2).curveTo(-16.4,1.3,-6.5,3.8).lineTo(-3.9,4.3).curveTo(-6.8,4.5,-10.3,4.5).curveTo(-12,4.5,-16.8,4.1).closePath().moveTo(13.1,1.4).curveTo(13.1,0,12.8,-1.2).curveTo(13.4,-1.2,13.9,-1.9).curveTo(14.7,-2.9,15.2,-3.3).curveTo(16.2,-4,17.7,-4.5).curveTo(18.7,-0,20.1,1.5).curveTo(20.9,2.5,22.9,3.3).lineTo(15.7,3.9).curveTo(13.6,4,13,4.1).curveTo(13.2,3.1,13.1,1.4).closePath();
	this.shape_7.setTransform(64.4,154.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#CC6600").beginStroke().moveTo(-12.2,78.9).curveTo(-22.1,76.5,-22.1,73.2).lineTo(-22,72.5).curveTo(-16.1,71.2,-2.1,71.3).curveTo(-0.2,71.5,1.4,73.1).curveTo(3,74.6,2.9,76.3).lineTo(2.5,76.8).curveTo(1.7,77.5,0.1,78).curveTo(-3.4,79.2,-9.6,79.5).lineTo(-12.2,78.9).closePath().moveTo(14.4,76.7).curveTo(13,75.2,12,70.7).curveTo(14.8,69.8,19.9,69.8).curveTo(27,69.8,31.2,71.7).curveTo(34.7,73.3,35.6,75.9).curveTo(35.8,76.7,32.5,77.1).curveTo(27.1,77.7,17.2,78.5).curveTo(15.2,77.7,14.4,76.7).closePath().moveTo(1.8,-4).curveTo(1,-8,-2.3,-9.8).curveTo(-4.4,-10.8,-9.9,-11.8).curveTo(-16.1,-13,-19.4,-14.2).curveTo(-24.9,-16.4,-28.8,-20.8).curveTo(-32.8,-25.2,-34.2,-30.6).curveTo(-35,-35.3,-35.3,-36).curveTo(-35.8,-37.1,-36.5,-38.2).curveTo(-37.4,-39.4,-37.9,-39.4).curveTo(-38.6,-39.3,-39.3,-39).curveTo(-39.5,-40.9,-40.3,-41.4).curveTo(-41.3,-42,-42.8,-40).lineTo(-43.5,-38.9).lineTo(-43.5,-38.5).curveTo(-43.5,-37.4,-44.1,-34.7).lineTo(-44.5,-33.7).lineTo(-44.4,-31.6).curveTo(-44.4,-30.7,-44.8,-28.4).lineTo(-44.8,-28.2).lineTo(-45.2,-26.1).lineTo(-45.4,-25.4).lineTo(-45.7,-24.1).lineTo(-46.7,-19.3).lineTo(-46.4,-20.2).lineTo(-47.2,-17).curveTo(-48.2,-13.4,-48.2,-12.6).lineTo(-48.1,-11.2).lineTo(-48.2,-10.5).lineTo(-48.2,-10.3).curveTo(-51.7,-9.5,-54.6,-9.6).lineTo(-53.5,-9.8).lineTo(-55.1,-9.8).curveTo(-56.2,-9.6,-57.2,-9.5).lineTo(-57.5,-9.5).lineTo(-58,-9.6).curveTo(-59,-9.7,-59.2,-10.5).lineTo(-59.2,-11.5).lineTo(-59.2,-12.3).lineTo(-59.6,-12.3).curveTo(-59.6,-17.6,-59.4,-20).curveTo(-59.1,-24.9,-58.4,-28.6).curveTo(-55.1,-42.7,-54.7,-45.5).lineTo(-54.7,-45.7).curveTo(-54.3,-46.1,-54,-47.6).curveTo(-53.6,-50.8,-52.9,-52.3).curveTo(-50.9,-56.9,-47,-62).curveTo(-43.5,-66.7,-41.4,-68.3).lineTo(-29.1,-72.5).curveTo(-23.4,-74.5,-22.6,-75.1).curveTo(-22.3,-75.3,-22.4,-75.8).lineTo(-22.7,-76.4).curveTo(-21.2,-77.3,-17.5,-77.8).curveTo(-12.3,-78.5,-10.3,-78.9).curveTo(-5.6,-80.1,4.9,-79).curveTo(10.6,-78.4,18.8,-77.1).curveTo(21.8,-76.8,24.3,-76.2).curveTo(24.1,-75.9,24.1,-75.3).curveTo(24.1,-73.5,24.2,-74.5).curveTo(26.8,-71.8,38.1,-67.1).lineTo(41.6,-64.8).curveTo(48.1,-60.7,50.1,-58.7).curveTo(51.4,-57.4,53.5,-51).lineTo(54,-49.8).lineTo(54.1,-49.3).lineTo(54.1,-47.8).lineTo(54.1,-47.2).lineTo(54.1,-47.2).lineTo(57.4,-28).lineTo(58.6,-20.6).curveTo(59.1,-17,59.6,-15.5).curveTo(56.9,-10.1,50.8,-11.4).curveTo(50.1,-11.6,50.3,-12.6).curveTo(50.5,-13.9,50.5,-14.3).curveTo(50.1,-20.5,50.3,-21.1).curveTo(51,-24.6,51.2,-26.6).curveTo(51.4,-29,51.4,-31.2).lineTo(51.1,-33.2).lineTo(51,-34.7).lineTo(50.8,-36.4).lineTo(50.7,-36.7).curveTo(50.1,-38.2,49.3,-40.5).curveTo(47.7,-44.9,47.7,-46.1).curveTo(46.3,-48.4,44.5,-50.6).curveTo(41.8,-53.8,39.8,-54.2).curveTo(37.8,-54.6,37.8,-51.3).curveTo(37.8,-50.8,41.9,-45).curveTo(46.2,-39.1,46.8,-32.8).curveTo(47.3,-27,44.4,-22.7).curveTo(43.1,-20.5,41.5,-19.2).curveTo(41.5,-18.1,41.7,-17.9).lineTo(41.9,-17.5).curveTo(42.2,-17.2,44.2,-17.2).curveTo(44.7,-17.2,45.5,-17.6).lineTo(46.8,-18.2).lineTo(47,-12.4).curveTo(47.3,-8.1,47.8,-7.3).curveTo(47.1,-7.1,45.4,-6.5).curveTo(43.8,-5.8,41.5,-5.3).curveTo(35,-3.9,18.6,-3.7).lineTo(14.8,-3.7).curveTo(7.3,-3.7,1.8,-4).closePath();
	this.shape_8.setTransform(70.1,79.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#396086").beginStroke().moveTo(-2.8,4.7).curveTo(-9.8,-6.9,-12.5,-19.8).lineTo(-11.8,-23.1).lineTo(-11.1,-26.8).curveTo(-8.2,-25.6,1.8,-23.9).lineTo(2.6,-23.8).lineTo(3,-23.8).lineTo(4.9,-23.6).curveTo(4.9,-11.4,7.8,4.8).curveTo(9.5,14.3,12.6,26.8).curveTo(3.5,15.1,-2.8,4.7).closePath();
	this.shape_9.setTransform(36.8,100.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#396087").beginStroke().moveTo(-9,24.6).curveTo(-13.9,24.6,-18.5,22.9).curveTo(-22.5,21,-22.8,21).curveTo(-23.3,19.3,-24.3,16.8).curveTo(-27.1,9.7,-28.6,5.4).curveTo(-35,-11.9,-35,-22.8).lineTo(-34.7,-24.7).curveTo(-31.9,-11.7,-25,-0.2).curveTo(-18.7,10.2,-9.6,22).lineTo(-7.6,24.6).lineTo(-8,24.6).lineTo(-8.5,24.7).lineTo(-9,24.6).closePath().moveTo(22.3,24).lineTo(15.9,23).lineTo(15.9,22.2).lineTo(15.1,10.6).curveTo(16.8,13.4,19.1,15.8).curveTo(24.9,21.8,35,24.6).lineTo(33.4,24.6).curveTo(27.9,24.6,22.3,24).closePath();
	this.shape_10.setTransform(58.9,105.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#371E2D").beginStroke().moveTo(9.7,1.3).lineTo(2.3,0).lineTo(2.3,-1.3).lineTo(2.3,-1.5).lineTo(2.6,-1.5).lineTo(4.1,-1.2).curveTo(7.1,-0.6,9.4,0.7).lineTo(9.5,0.7).lineTo(9.5,0.8).lineTo(10.6,1.5).lineTo(9.7,1.3).closePath().moveTo(-10.6,-0.1).curveTo(-9.7,-0.2,-8.6,-0.4).lineTo(-6.9,-0.4).lineTo(-8,-0.2).lineTo(-10.2,-0.1).lineTo(-10.6,-0.1).closePath();
	this.shape_11.setTransform(23.5,70.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#4B7DAF").beginStroke().moveTo(-30.6,24).curveTo(-33.6,11.4,-35.3,1.9).curveTo(-38.2,-14.2,-38.3,-26.4).curveTo(-31.4,-25.6,-15.1,-24).curveTo(-6.2,-23.3,10,-23.7).curveTo(27.2,-24.4,36,-26).lineTo(38.3,-26.6).lineTo(37.9,-10.7).curveTo(37.1,-0.6,34.3,7.9).curveTo(32.6,12.7,29.4,21.1).lineTo(27.8,25.7).lineTo(24.5,25.8).lineTo(23.3,25.9).curveTo(18.7,26.4,14.1,26.6).curveTo(4,23.8,-1.9,17.8).curveTo(-4.2,15.4,-5.8,12.5).lineTo(-6.3,3.8).curveTo(-6,-1.8,-6.2,-4.1).curveTo(-10,-3.9,-10.6,0.1).curveTo(-10.9,2.2,-10.4,4.3).lineTo(-9.9,13.6).lineTo(-9.3,23.2).lineTo(-9.4,24.2).curveTo(-9.5,25,-11.5,25.3).curveTo(-15.5,25.5,-22.2,26).lineTo(-28.5,26.6).lineTo(-30.6,24).closePath();
	this.shape_12.setTransform(79.9,103.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.beginFill("#000000").beginStroke().moveTo(-22,80.6).curveTo(-28.1,79.7,-28.9,78.6).curveTo(-29.6,77.4,-29.6,75.2).curveTo(-29.6,70.1,-24.4,66.6).curveTo(-17.3,63,-13.8,62.4).curveTo(-12.2,62.1,-10.4,62).curveTo(-6.8,61.6,-3,62).curveTo(1.1,62.4,7.9,65.7).lineTo(8,65.8).curveTo(9.3,63.7,13.3,62.4).curveTo(14.8,61.9,16.5,61.6).curveTo(21,60.7,26.5,61.2).curveTo(30.5,61.4,37.9,63.8).lineTo(39.7,64.7).curveTo(46.1,68.3,45.7,73.6).curveTo(45.6,75.1,45.2,75.7).curveTo(44.6,76.3,42.8,76.8).curveTo(41.4,77.1,25.4,79.2).curveTo(18.8,79.8,13.3,79.2).curveTo(11.1,78.7,10.9,78).lineTo(10.6,76.5).lineTo(10.3,77).curveTo(8.1,79.5,-1,80.6).curveTo(-6.7,81,-12,81).curveTo(-17.2,81,-22,80.6).closePath().moveTo(-18.4,68.6).lineTo(-20,69).lineTo(-20.7,69.2).lineTo(-22,69.8).curveTo(-23.6,70.6,-24.1,72.2).lineTo(-25,74.6).curveTo(-24.3,74.9,-19,75.3).curveTo(-14.1,75.7,-12.4,75.8).curveTo(-8.9,75.7,-6,75.6).curveTo(0.2,75.2,3.7,74.1).curveTo(5.3,73.6,6,72.9).lineTo(6.5,72.4).curveTo(6.6,70.6,5,69.2).curveTo(3.4,67.6,1.5,67.4).lineTo(-0.5,67.4).curveTo(-13,67.4,-18.4,68.6).closePath().moveTo(15.6,66.8).curveTo(14,67.2,13.1,67.9).curveTo(12.6,68.4,11.8,69.4).curveTo(11.3,70,10.7,70.1).curveTo(11,71.3,11,72.6).curveTo(11,74.4,10.9,75.4).curveTo(11.5,75.2,13.5,75.1).lineTo(20.7,74.6).curveTo(30.7,73.8,36.1,73.2).curveTo(39.4,72.7,39.2,72).curveTo(38.3,69.3,34.8,67.7).curveTo(30.6,65.9,23.4,65.9).lineTo(22.9,65.9).curveTo(18.3,65.9,15.6,66.8).closePath().moveTo(14.5,50.6).curveTo(8.8,50.1,7.4,49.5).lineTo(5.7,48.7).lineTo(5.1,48.4).lineTo(5.1,48.3).lineTo(4,48.9).curveTo(-0.4,51,-7.7,51.3).curveTo(-15.5,52.4,-29.4,48.9).curveTo(-31.9,48.3,-32.2,48).curveTo(-35,45.6,-40.2,30.1).curveTo(-46.1,12.8,-46.7,0.5).curveTo(-46.9,-4.8,-46.2,-8.2).curveTo(-46,-9.2,-45.7,-9.9).lineTo(-49.8,-9.9).lineTo(-50.8,-9.9).lineTo(-52.7,-9.8).curveTo(-53.6,-9.8,-54.8,-10).curveTo(-56.9,-10.4,-59.9,-11.4).curveTo(-62.5,-12.1,-63.6,-12.6).lineTo(-64.3,-12.5).curveTo(-65.3,-13.1,-65.8,-16.2).curveTo(-66.3,-18.4,-66.3,-19.7).curveTo(-66.6,-22,-66.4,-25.2).curveTo(-65.9,-29.6,-65.9,-31.2).curveTo(-65.8,-40.3,-62.9,-50.2).lineTo(-62.7,-50.4).lineTo(-60.7,-54.6).lineTo(-59.3,-57.3).lineTo(-57.9,-60.6).curveTo(-57.3,-61.7,-55.6,-64.2).lineTo(-55.4,-64.4).lineTo(-55.3,-64.4).lineTo(-53,-68.1).curveTo(-52.9,-68.3,-51.4,-69.3).curveTo(-49.9,-70.5,-49.1,-71).curveTo(-36.4,-78.4,-19.5,-81).curveTo(-19.4,-81,-19.3,-80.5).lineTo(-19.2,-80.3).lineTo(-18.9,-79.7).curveTo(-18.7,-79.2,-19,-79).curveTo(-19.8,-78.4,-25.5,-76.4).lineTo(-37.9,-72.2).curveTo(-45.3,-69.7,-49.8,-66.5).curveTo(-51.8,-65.2,-53.6,-63.4).curveTo(-53.4,-62.6,-53.4,-61.1).curveTo(-54.6,-59,-56.5,-55.1).curveTo(-61.6,-44.3,-61.8,-38.9).curveTo(-62.5,-37,-62.6,-36.3).lineTo(-62.7,-29.9).lineTo(-62.4,-22.7).curveTo(-61.9,-15.6,-60.6,-15.6).lineTo(-60.2,-15.6).lineTo(-59.6,-15).lineTo(-59.5,-15).lineTo(-59.3,-14.9).lineTo(-59.2,-14.8).curveTo(-57.5,-13.7,-54.4,-13.5).lineTo(-53.9,-13.4).lineTo(-53.6,-13.4).lineTo(-53.1,-13.4).lineTo(-51,-13.5).curveTo(-48.2,-13.4,-44.6,-14.2).lineTo(-44.6,-14.4).lineTo(-44.5,-15.1).lineTo(-44.6,-16.5).curveTo(-44.6,-17.3,-43.6,-20.9).lineTo(-42.8,-24.1).lineTo(-43.2,-23.2).lineTo(-42.1,-28).lineTo(-41.8,-29.3).lineTo(-41.7,-30).lineTo(-41.2,-32.2).lineTo(-41.2,-32.3).curveTo(-40.8,-34.6,-40.8,-35.5).lineTo(-40.9,-37.6).lineTo(-40.6,-38.6).curveTo(-39.9,-41.4,-39.9,-42.4).lineTo(-39.9,-42.9).lineTo(-39.2,-43.9).curveTo(-37.7,-46,-36.7,-45.3).curveTo(-36,-44.8,-35.7,-42.9).lineTo(-35.6,-42.2).lineTo(-35.5,-41.3).lineTo(-35.5,-41.2).lineTo(-35.5,-40.2).curveTo(-35.5,-37.2,-36,-34.6).lineTo(-37.4,-26.4).curveTo(-38.3,-20.5,-39.6,-16.7).lineTo(-40.3,-14.8).lineTo(-40.6,-14.8).lineTo(-40.6,-14.6).lineTo(-40.6,-13.3).lineTo(-33.2,-12.1).lineTo(-32.3,-11.8).lineTo(-33.4,-12.5).lineTo(-33.5,-12.6).curveTo(-28.2,-11.7,-22.3,-11.1).curveTo(-9.4,-9.6,-1.9,-8.6).curveTo(1.2,-8.2,5.4,-7.9).curveTo(12.3,-7.6,22.2,-7.7).curveTo(38.6,-7.9,45,-9.3).curveTo(47.3,-9.8,49,-10.4).curveTo(50.6,-11.1,51.4,-11.2).curveTo(50.9,-12.1,50.6,-16.3).lineTo(50.4,-22.1).lineTo(49.1,-21.5).curveTo(48.3,-21.1,47.7,-21.1).curveTo(45.8,-21.1,45.5,-21.4).lineTo(45.3,-21.8).curveTo(45.1,-22,45.1,-23.2).curveTo(46.6,-24.4,48,-26.7).curveTo(50.9,-31,50.3,-36.7).curveTo(49.8,-43,45.5,-49).curveTo(41.3,-54.7,41.3,-55.2).curveTo(41.3,-58.6,43.4,-58.2).curveTo(45.4,-57.8,48.1,-54.5).curveTo(49.9,-52.3,51.2,-50.1).curveTo(51.3,-48.8,52.9,-44.4).curveTo(53.7,-42.1,54.3,-40.6).lineTo(54.6,-38.6).lineTo(54.7,-37.2).lineTo(54.9,-35.1).curveTo(55,-32.9,54.8,-30.5).curveTo(54.6,-28.5,53.8,-25).curveTo(53.7,-24.4,54,-18.3).curveTo(54.1,-17.8,53.8,-16.6).curveTo(53.7,-15.5,54.4,-15.3).curveTo(60.5,-14,63.2,-19.4).curveTo(62.7,-20.9,62.2,-24.5).lineTo(60.9,-32).lineTo(57.6,-51.1).lineTo(57.7,-51.1).lineTo(57.7,-51.7).lineTo(57.7,-53.2).lineTo(57.6,-53.7).lineTo(57.1,-54.9).curveTo(55,-61.3,53.7,-62.6).curveTo(51.7,-64.6,45.2,-68.7).lineTo(41.7,-71).curveTo(30.4,-75.7,27.7,-78.4).curveTo(27.6,-77.4,27.6,-79.2).curveTo(27.6,-79.8,27.9,-80.1).curveTo(28.3,-80.9,30.3,-79.9).lineTo(35.4,-77.4).lineTo(43.6,-73.7).curveTo(47.7,-71.9,50.7,-69.7).curveTo(61.8,-61.3,61.8,-51.7).lineTo(61.8,-51.3).curveTo(62.4,-46,64.7,-33.5).curveTo(66.5,-23.1,66.5,-21.3).lineTo(66.4,-19.8).lineTo(66.5,-18.7).curveTo(66.5,-15.6,64.3,-13.4).curveTo(61.6,-10.7,56.5,-10.7).lineTo(55.2,-10.8).lineTo(55.6,-8.1).curveTo(56.1,-3.9,56.1,-2.5).curveTo(56.1,6.7,55.9,9.5).curveTo(55.2,17.8,52.7,25.6).lineTo(48.5,39).curveTo(46.4,44.9,44.4,47.8).curveTo(44.6,47.9,44.3,48.5).curveTo(43.9,49,43.4,49.1).curveTo(43.8,49.2,41.7,49.8).curveTo(39.5,50.3,37.3,50.6).curveTo(31.6,51.6,25.9,51.6).curveTo(20.2,51.6,14.5,50.6).closePath().moveTo(-41.6,-5.7).lineTo(-42.3,-2.4).lineTo(-42.6,-0.6).curveTo(-42.6,10.3,-36.2,27.7).curveTo(-34.6,32,-31.8,39.1).curveTo(-30.9,41.5,-30.4,43.3).curveTo(-30.1,43.2,-26.1,45.2).curveTo(-21.5,46.8,-16.5,46.9).lineTo(-15.5,46.9).lineTo(-15.1,46.9).lineTo(-8.8,46.3).curveTo(-2.1,45.8,1.9,45.6).curveTo(3.9,45.3,4,44.5).lineTo(4.1,43.5).lineTo(3.5,33.9).lineTo(3,24.6).curveTo(2.5,22.5,2.8,20.4).curveTo(3.4,16.4,7.2,16.2).curveTo(7.4,18.5,7.1,24.1).lineTo(7.6,32.8).lineTo(8.4,44.5).lineTo(8.4,45.2).lineTo(14.8,46.2).curveTo(21.1,47,27.5,46.9).curveTo(32.1,46.7,36.7,46.2).lineTo(37.9,46.1).lineTo(41.2,46).lineTo(42.8,41.4).curveTo(46,33,47.7,28.2).curveTo(50.5,19.7,51.3,9.6).lineTo(51.7,-6.3).lineTo(49.4,-5.7).curveTo(40.6,-4.1,23.4,-3.4).curveTo(7.2,-3,-1.7,-3.7).curveTo(-18,-5.3,-24.8,-6.1).lineTo(-26.7,-6.3).lineTo(-27.1,-6.4).lineTo(-27.9,-6.5).curveTo(-37.9,-8.1,-40.8,-9.4).lineTo(-41.6,-5.7).closePath().moveTo(-63.3,-13.8).lineTo(-63.2,-13.8).lineTo(-63.3,-13.8).closePath().moveTo(-21.3,-79).curveTo(-20.4,-79.7,-19.2,-80.3).curveTo(-20.4,-79.7,-21.3,-79).closePath();
	this.shape_13.setTransform(66.5,83.5);

	// Layer 5
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.beginFill("#CC6600").beginStroke().moveTo(-31.5,12.9).curveTo(-34,11.5,-34.5,9.3).curveTo(-34.6,8.8,-34.6,5.7).curveTo(-34.6,-1.2,-28.5,-7.5).curveTo(-20.7,-15.4,-6.2,-16.9).lineTo(16.2,-16.8).lineTo(26.1,-13.6).curveTo(34.6,-10.8,34.6,-9.7).curveTo(34.6,-6.8,32.1,-2.5).curveTo(29.5,2.3,25.1,6.4).curveTo(13.7,16.9,-1.2,16.9).curveTo(-24.8,16.9,-31.5,12.9).closePath();
	this.shape_14.setTransform(40.4,14.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.beginFill("#000000").beginStroke().moveTo(-37.3,13.6).curveTo(-40.4,10.4,-40.4,5.1).curveTo(-40.4,-3.1,-32.8,-10).curveTo(-24,-17.8,-8.6,-20.7).curveTo(5,-21.4,16.6,-20.7).curveTo(17.9,-20.2,29.5,-17.2).curveTo(40.4,-14,40.4,-11.7).curveTo(40.4,-7.9,38.6,-3.3).curveTo(36.2,2.8,31.6,7.6).curveTo(18.3,21,-9,21).curveTo(-30.2,21,-37.3,13.6).closePath().moveTo(-29.1,-7.8).curveTo(-35.1,-1.6,-35.2,5.3).curveTo(-35.2,8.4,-35,8.9).curveTo(-34.6,11.1,-32,12.6).curveTo(-25.3,16.5,-1.7,16.5).curveTo(13.2,16.5,24.6,6).curveTo(28.9,1.9,31.6,-2.9).curveTo(34.1,-7.2,34.1,-10.1).curveTo(34.1,-11.2,25.6,-14).lineTo(15.7,-17.1).lineTo(-6.7,-17.3).curveTo(-21.3,-15.8,-29.1,-7.8).closePath();
	this.shape_15.setTransform(41,14.9);

	this.addChild(this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-6.1,133,170.6);


(lib.GHAIR = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#3A1B00").beginStroke().moveTo(59.3,78).curveTo(59.3,66.9,54.9,53.2).curveTo(53.4,48.5,49.5,42.5).curveTo(47.1,39.1,37.5,26.2).curveTo(34.7,22.5,29.5,17.5).curveTo(24.3,12.4,19.7,9).curveTo(5.7,-0.4,-4.6,-2.7).curveTo(-14.8,-5.1,-15.7,1).curveTo(-17.5,1.6,-19,2.7).curveTo(-36,-14.1,-50.6,-44.8).curveTo(-33.6,-52.7,-8.9,-53.6).lineTo(13.6,-53.6).curveTo(39.2,-47.1,53.7,-31.6).curveTo(65.6,-19,68.7,-1.9).curveTo(75.2,21.7,69.5,51.4).curveTo(69.5,60.5,59.2,84.4).lineTo(59.3,78).closePath().moveTo(-63.5,-52.6).curveTo(-70.7,-62.1,-71.9,-73.2).lineTo(-72.2,-75.4).curveTo(-65,-80.6,-54.1,-84.4).lineTo(-31.4,-84.4).curveTo(-17.5,-79.9,-14.9,-77.3).curveTo(-13.6,-76,-13.4,-74).lineTo(-13.4,-69.3).curveTo(-13.4,-64.4,-14.2,-62.1).curveTo(-14.8,-60.2,-16.8,-59.1).lineTo(-18.1,-58.2).curveTo(-38.9,-56.9,-53.9,-49.6).curveTo(-56.3,-48.5,-58.6,-47.2).lineTo(-63.5,-52.6).closePath();
	this.shape.setTransform(91.8,89.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#000000").beginStroke().moveTo(0.4,66.7).curveTo(-33.9,40.4,-33.9,8.6).curveTo(-33.9,-15.4,-21.8,-32.1).curveTo(-14,-42.8,-1.8,-49.4).curveTo(0.2,-50.6,2.4,-51.6).curveTo(17,-20.9,34,-4.1).curveTo(26,1.1,22.5,16.9).curveTo(19.8,28.7,19.8,48.4).lineTo(20,55.5).curveTo(20.4,63.9,21.4,70.9).curveTo(22.4,77.5,23.7,82.2).curveTo(8,72.6,0.4,66.7).closePath().moveTo(-33.1,-44.6).curveTo(-33.9,-46.2,-34,-48.6).lineTo(-33.9,-53.7).curveTo(-33.9,-65.6,-27.8,-73.9).curveTo(-24.5,-78.4,-19.2,-82.2).lineTo(-18.9,-80).curveTo(-17.6,-68.9,-10.4,-59.4).lineTo(-5.6,-54).curveTo(-16.9,-47.5,-24.7,-37).curveTo(-31.6,-41.5,-33.1,-44.6).closePath();
	this.shape_1.setTransform(38.8,96.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#020100").beginStroke().moveTo(52.7,105.1).curveTo(52.7,102.4,55.5,93.4).lineTo(58.3,84.4).curveTo(60.4,77.9,61.1,74.5).curveTo(62.2,69.3,62.2,59.6).curveTo(62.2,49.5,59.1,40.7).curveTo(55.4,28.6,45.5,15.8).curveTo(25.6,-9.7,-5.7,-13.7).lineTo(-6,-13.9).curveTo(-7.4,-12.8,-8.7,-11.5).lineTo(-10.1,-9.9).curveTo(-17.2,-1.8,-19,10.2).curveTo(-19.7,15.8,-20.3,25.9).curveTo(-20.8,33.9,-20.8,36.7).curveTo(-20.8,45.1,-15.6,63.2).curveTo(-13.1,71.8,-10.7,79.1).lineTo(-10.5,79.7).lineTo(-10.5,79.9).curveTo(-10.5,81.7,-10.9,81.3).curveTo(-10.9,81.3,-10.9,81.3).curveTo(-10.9,81.3,-11,81.3).curveTo(-11,81.4,-11,81.5).curveTo(-11.1,81.5,-11.1,81.6).curveTo(-11.5,82.1,-12.8,82.1).lineTo(-13.1,82.1).lineTo(-13.6,82.1).curveTo(-21,82,-43.7,64.2).curveTo(-63.9,48.4,-68,42.8).curveTo(-80.1,26.2,-82.4,19).curveTo(-83.6,15.1,-83.9,10).curveTo(-84,8.3,-84,-2.1).curveTo(-84,-26.1,-72.9,-43.7).curveTo(-80.8,-44.8,-83.2,-53.9).curveTo(-84,-57.2,-84.1,-61.2).lineTo(-84,-64.8).curveTo(-84,-79.7,-77.3,-88.4).curveTo(-73,-93.9,-65.2,-98.7).curveTo(-57.6,-103.5,-46.8,-107.7).lineTo(-23.1,-107.7).curveTo(-7.3,-104.1,-3.1,-98).curveTo(-1.4,-95.7,-1.1,-92.4).curveTo(-1,-91.6,-1,-86.8).curveTo(-1,-80.7,-1.6,-78.3).lineTo(-2.2,-76.4).lineTo(-1,-76.4).lineTo(-1,-76.9).lineTo(21.9,-76.9).curveTo(49.8,-70.1,65.5,-52.8).curveTo(77.2,-39.9,81.2,-22.2).curveTo(84.1,-9.8,84,5.4).curveTo(83.9,15.9,82,33.1).curveTo(80.3,48.3,70.7,72.9).curveTo(63.8,90.2,55.1,107.3).curveTo(54.5,107.7,54,107.7).curveTo(52.8,107.7,52.7,105.1).closePath().moveTo(-42.9,-62.8).curveTo(-45.1,-61.8,-47.1,-60.7).curveTo(-59.3,-54,-67.1,-43.3).curveTo(-79.2,-26.6,-79.2,-2.6).curveTo(-79.2,29.2,-44.8,55.4).curveTo(-37.3,61.3,-21.6,70.9).curveTo(-22.9,66.3,-23.9,59.7).curveTo(-24.9,52.7,-25.3,44.3).lineTo(-25.5,37.2).curveTo(-25.5,17.4,-22.8,5.7).curveTo(-19.3,-10.1,-11.3,-15.3).curveTo(-9.7,-16.4,-8,-17).curveTo(-7.1,-23.1,3.1,-20.7).curveTo(13.5,-18.4,27.4,-9).curveTo(32,-5.7,37.3,-0.6).curveTo(42.5,4.5,45.2,8.2).curveTo(54.9,21,57.2,24.5).curveTo(61.2,30.5,62.7,35.2).curveTo(67,48.8,67,59.9).lineTo(67,66.3).curveTo(77.2,42.4,77.2,33.4).curveTo(83,3.7,76.4,-20).curveTo(73.3,-37,61.4,-49.7).curveTo(46.9,-65.2,21.3,-71.7).lineTo(-1.2,-71.7).curveTo(-25.9,-70.7,-42.9,-62.8).closePath().moveTo(-64.4,-93.4).curveTo(-69.8,-89.7,-73.1,-85.2).curveTo(-79.2,-76.9,-79.2,-65).lineTo(-79.3,-59.9).curveTo(-79.2,-57.4,-78.4,-55.8).curveTo(-76.8,-52.8,-69.9,-48.3).curveTo(-62.2,-58.7,-50.8,-65.3).curveTo(-48.6,-66.5,-46.2,-67.7).curveTo(-31.1,-74.9,-10.4,-76.2).lineTo(-9.1,-77.1).curveTo(-7.1,-78.3,-6.5,-80.1).curveTo(-5.7,-82.5,-5.7,-87.3).lineTo(-5.7,-92.1).curveTo(-5.9,-94,-7.2,-95.3).curveTo(-9.7,-97.9,-23.7,-102.4).lineTo(-46.4,-102.4).curveTo(-57.2,-98.6,-64.4,-93.4).closePath();
	this.shape_2.setTransform(84.1,107.7);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.1,215.3);


(lib.FrankHead = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#CCCCCC").beginStroke().moveTo(-36.2,8.9).lineTo(-36.2,-7.7).lineTo(-11.2,-7.7).lineTo(-11.2,5.7).curveTo(-12.6,6.3,-17.2,7.1).curveTo(-22.6,8.1,-30.4,8.9).closePath().moveTo(13.8,7.7).lineTo(13.8,-7.4).lineTo(16.7,-8.7).lineTo(34.3,-8.9).curveTo(34.6,-8.3,35.5,-7.2).curveTo(36.2,-5.8,36.2,-3.9).curveTo(36.2,-1.5,35.3,1.9).curveTo(34.4,5.4,33.1,8).closePath();
	this.shape.setTransform(98.6,88.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#7A4D30").beginStroke().moveTo(21.2,57.9).curveTo(14,55,6.6,47.7).curveTo(3.7,44.8,1.7,40.4).lineTo(-1,34.7).lineTo(0.3,34).curveTo(2.2,32.9,2.2,30.5).curveTo(2.2,29.4,2,28.8).curveTo(1.4,27.4,-0.7,27.4).curveTo(-1.8,27.4,-5.1,28.5).curveTo(-8.4,29.6,-9.6,29.6).curveTo(-13.5,29.6,-17.2,27.6).curveTo(-22.5,24.7,-26,18.3).curveTo(-26.3,17.7,-26.3,16.7).lineTo(-26.2,14.8).curveTo(-26.2,11.2,-23.5,9.2).curveTo(-22.5,8.5,-21.9,7.7).curveTo(-18.8,8.5,-16.1,10).curveTo(-14.1,11.1,-11.7,11.1).curveTo(-10.6,11.1,-9.9,10.9).curveTo(-9,10.4,-9,9.1).curveTo(-5.4,11.4,-1.2,11.9).curveTo(3.3,12.3,2.4,9.5).curveTo(-1.8,-2.6,-3.5,-18.2).curveTo(-5.4,-34.4,-3.3,-44.8).curveTo(-1.6,-54,2.1,-59).lineTo(2.6,-59.5).curveTo(13.7,-44.6,15.9,-26.9).curveTo(17.3,-16.3,17.9,8.7).curveTo(19.8,24.2,22.3,37.3).curveTo(24.8,50.4,26.3,59.5).curveTo(23.5,58.7,21.2,57.9).closePath();
	this.shape_1.setTransform(30.8,89.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#5B371E").beginStroke().moveTo(9.6,42.2).curveTo(5.4,41.8,1.8,39.4).curveTo(1.8,37.8,-0.9,35.9).curveTo(-3.1,34.3,-6.7,33.3).curveTo(-8.5,32.8,-9.7,32.7).curveTo(-13.5,11,-13.5,10).curveTo(-13.5,2.9,-12,-5.7).curveTo(-9.9,-18.3,-5.2,-28).curveTo(-1,-36.6,4.8,-42.3).curveTo(9,-36.8,12.2,-31.4).lineTo(13.5,-29.3).lineTo(13.4,-29.1).lineTo(12.9,-28.6).curveTo(9.2,-23.7,7.5,-14.4).curveTo(5.4,-4.1,7.3,12.2).curveTo(9.1,27.8,13.2,39.9).curveTo(14,42.3,10.9,42.3).lineTo(9.6,42.2).closePath();
	this.shape_2.setTransform(20,58.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#804E29").beginStroke().moveTo(-29.1,6.7).curveTo(-33.4,5.6,-37.1,9.9).lineTo(-38.4,7.8).curveTo(-41.6,2.4,-45.8,-3.2).curveTo(-32.6,-16.4,-10.8,-14.3).lineTo(14.6,-14.3).curveTo(34.5,-10,45.9,-0).curveTo(42.4,14,21,14.6).lineTo(19,14.6).curveTo(1.9,14.6,-29.1,6.7).closePath();
	this.shape_3.setTransform(70.6,19.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#975F3C").beginStroke().moveTo(-26.1,62.8).curveTo(-27.6,53.7,-30.1,40.6).curveTo(-32.6,27.5,-34.5,12).curveTo(-35.1,-13,-36.5,-23.7).curveTo(-38.7,-41.3,-49.8,-56.2).lineTo(-49.7,-56.4).curveTo(-46.1,-60.6,-41.7,-59.5).curveTo(-8.8,-51.2,8.4,-51.7).curveTo(29.7,-52.3,33.2,-66.3).curveTo(39.3,-60.9,44.4,-48.9).curveTo(47,-42.9,48.3,-37.9).curveTo(50.6,-30.5,49.5,-15).lineTo(49.3,-12.2).curveTo(31.2,-14.7,25.9,-6.8).curveTo(24.6,-5,24.2,-3).lineTo(15.4,-2.8).lineTo(9.1,-2.6).lineTo(8.7,-5.3).curveTo(8.1,-9.5,6.6,-10.6).lineTo(-23.4,-10.6).curveTo(-25.1,-8.1,-25.6,-3.9).curveTo(-25.8,-2.1,-25.8,1.5).curveTo(-26.2,1.9,-26.1,3.2).curveTo(-25.9,4.4,-25.8,4.5).curveTo(-25.7,7.1,-25.2,9.7).curveTo(-24.2,15,-22.1,16.8).lineTo(-13.3,16.8).curveTo(3.5,16.9,7.7,10.5).curveTo(9.3,8.3,9.3,5).lineTo(9.2,1.7).lineTo(24,1.3).lineTo(24.2,2.5).lineTo(24.1,8.9).curveTo(24.4,13.8,26.7,15.6).lineTo(30.1,15.6).curveTo(29.5,16.3,29.5,18.4).curveTo(29.5,21.2,29.3,22.1).curveTo(28.8,23.6,27,24.3).lineTo(13.2,24.3).lineTo(7.9,21.8).curveTo(8,18.6,7.9,17.6).curveTo(7.9,16,6.3,15.8).curveTo(4.7,15.6,3.6,18.1).curveTo(2.6,20.2,2.6,21.8).lineTo(2.5,23.8).curveTo(2.6,25.4,3.8,26.5).curveTo(7.1,29.7,20.2,29.7).curveTo(29.8,29.7,32.9,26.5).curveTo(34.4,24.7,34.4,20.9).curveTo(34.4,17,31.9,15.6).lineTo(47.1,15.6).curveTo(46.2,19.7,44.4,24).lineTo(40.4,32.9).curveTo(39.3,35.3,38.6,35.6).curveTo(34.1,37.4,30.6,38.2).curveTo(27,39.2,22.3,39.9).curveTo(6.7,42.2,-8,39.9).curveTo(-13.2,39.2,-13.2,42.1).lineTo(-6.8,44.3).curveTo(2.1,45.8,7.8,46).curveTo(16.3,46.3,23.9,45).curveTo(27.1,44.5,31.5,43.4).curveTo(35.5,42.3,38.7,41.8).lineTo(38.7,42.1).curveTo(36.8,55.3,26.8,61.6).curveTo(19.4,66.2,9.2,66.3).lineTo(7.5,66.3).curveTo(-13.5,66.3,-26.1,62.8).closePath();
	this.shape_4.setTransform(83.2,85.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#000000").beginStroke().moveTo(-1.9,77.3).curveTo(-7.9,76.3,-12.3,75).curveTo(-22.9,71.8,-30.3,66.8).curveTo(-41.8,58.7,-45.4,45.8).lineTo(-45.4,45.7).lineTo(-48.7,45.8).curveTo(-58.7,45.8,-64.4,39.1).curveTo(-68.4,34.4,-68.6,29.8).lineTo(-69.5,29.2).curveTo(-69.8,27.2,-69.8,24.9).curveTo(-69.8,20.6,-69.3,18.7).curveTo(-68.3,14.6,-65.1,13.6).curveTo(-66.2,10.6,-67.2,-0).lineTo(-68,-10.3).curveTo(-68,-32.9,-59.3,-50.9).curveTo(-54.8,-60.3,-48.4,-66.4).curveTo(-34,-80.4,-10.1,-78.2).lineTo(-10.1,-78.6).lineTo(14.4,-78.6).curveTo(40.1,-74,53.5,-60).curveTo(59.4,-53.7,62.9,-43.9).curveTo(65,-37.9,66,-32.6).curveTo(65.7,-32.6,66.8,-28.3).curveTo(68.1,-23.5,68.2,-12.4).curveTo(68.2,-6.8,67.8,-2.5).curveTo(69.1,-1.3,69.6,-0.3).curveTo(69.8,0.5,69.8,4.9).curveTo(69.8,6.8,67.7,13.6).curveTo(66.6,17.1,65.6,19.3).curveTo(64.9,23.2,64.1,26.3).curveTo(62.3,33.4,58.7,41.7).curveTo(58.4,42.3,57.9,45).lineTo(57.4,48.6).curveTo(56.6,60.3,49.1,68.6).curveTo(41.6,77.2,30.5,78.3).lineTo(10,78.6).lineTo(-1.9,77.3).closePath().moveTo(-37.2,39).curveTo(-37,39.5,-37,40.7).curveTo(-37,43,-38.9,44.2).lineTo(-40.2,44.8).lineTo(-37.5,50.6).curveTo(-35.5,55,-32.6,57.9).curveTo(-25.2,65.2,-18,68).curveTo(-15.7,68.9,-12.9,69.6).curveTo(0.2,73.3,22.4,73.1).curveTo(32.6,73.1,40,68.5).curveTo(50,62.1,51.9,48.9).lineTo(51.9,48.7).curveTo(48.7,49.2,44.7,50.3).curveTo(40.3,51.3,37.1,51.9).curveTo(29.5,53.2,21,52.8).curveTo(15.3,52.6,6.4,51.2).lineTo(0,48.9).curveTo(0,46,5.2,46.8).curveTo(19.9,49,35.5,46.8).curveTo(40.2,46.1,43.8,45.1).curveTo(47.3,44.3,51.8,42.5).curveTo(52.5,42.2,53.6,39.8).lineTo(57.6,30.8).curveTo(59.4,26.6,60.3,22.5).lineTo(45.1,22.5).curveTo(47.6,23.9,47.6,27.8).curveTo(47.6,31.5,46.1,33.4).curveTo(43,36.6,33.4,36.6).curveTo(20.3,36.6,17,33.4).curveTo(15.8,32.2,15.7,30.7).lineTo(15.8,28.7).curveTo(15.8,27,16.8,24.9).curveTo(17.9,22.5,19.5,22.7).curveTo(21.1,22.9,21.1,24.5).curveTo(21.2,25.5,21.1,28.7).lineTo(26.4,31.2).lineTo(40.2,31.2).curveTo(42,30.5,42.5,29).curveTo(42.7,28.1,42.7,25.3).curveTo(42.7,23.2,43.3,22.5).lineTo(39.9,22.5).curveTo(37.6,20.7,37.3,15.7).lineTo(37.4,9.4).lineTo(37.2,8.2).lineTo(22.4,8.6).lineTo(22.5,11.9).curveTo(22.5,15.1,20.9,17.3).curveTo(16.7,23.8,-0.1,23.6).lineTo(-8.9,23.6).curveTo(-11,21.9,-12,16.5).curveTo(-12.5,13.9,-12.6,11.3).curveTo(-12.7,11.3,-12.9,10.1).curveTo(-13,8.8,-12.6,8.4).curveTo(-12.6,4.7,-12.4,3).curveTo(-11.9,-1.3,-10.2,-3.7).lineTo(19.8,-3.7).curveTo(21.3,-2.6,21.9,1.5).lineTo(22.3,4.2).lineTo(28.6,4.1).lineTo(37.4,3.8).curveTo(37.8,1.9,39.1,0.1).curveTo(44.4,-7.8,62.5,-5.3).lineTo(62.7,-8.1).curveTo(63.8,-23.6,61.5,-31).curveTo(60.2,-36,57.6,-42.1).curveTo(52.5,-54.1,46.4,-59.4).curveTo(35.1,-69.3,15.2,-73.7).lineTo(-10.2,-73.7).curveTo(-32,-75.8,-45.3,-62.6).curveTo(-51,-56.9,-55.2,-48.3).curveTo(-59.9,-38.5,-62,-25.9).curveTo(-63.5,-17.3,-63.5,-10.3).curveTo(-63.5,-9.2,-59.7,12.5).curveTo(-58.5,12.6,-56.7,13).curveTo(-53.2,14.1,-50.9,15.6).curveTo(-48.2,17.6,-48.2,19.2).curveTo(-48.2,20.5,-49.1,21).curveTo(-49.8,21.2,-50.9,21.2).curveTo(-53.3,21.2,-55.3,20.1).curveTo(-58,18.6,-61.1,17.9).curveTo(-61.7,18.6,-62.7,19.3).curveTo(-65.4,21.3,-65.4,24.9).lineTo(-65.5,26.9).curveTo(-65.5,27.8,-65.2,28.4).curveTo(-61.7,34.9,-56.4,37.8).curveTo(-52.7,39.7,-48.8,39.8).curveTo(-47.6,39.7,-44.3,38.7).curveTo(-41,37.5,-39.9,37.5).curveTo(-37.8,37.5,-37.2,39).closePath().moveTo(-7.6,18.1).lineTo(-1.8,18.1).curveTo(6,17.3,11.4,16.3).curveTo(15.9,15.5,17.3,14.9).lineTo(17.3,1.4).lineTo(-7.6,1.4).closePath().moveTo(45.2,0.5).lineTo(42.4,1.8).lineTo(42.4,16.8).lineTo(61.7,17.2).curveTo(63,14.6,63.9,11).curveTo(64.8,7.7,64.8,5.3).curveTo(64.8,3.3,64.1,2).curveTo(63.2,0.9,62.9,0.2).closePath();
	this.shape_5.setTransform(70,79);

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#000000").beginStroke().moveTo(-21.1,4.3).curveTo(-22.2,3.1,-22.2,1.9).curveTo(-22.2,1.4,-22.1,1).lineTo(-28.4,1.1).curveTo(-30.3,1.4,-32.8,2.3).curveTo(-34.8,2.9,-35.7,2.9).curveTo(-36.9,2.9,-37.8,2.7).lineTo(-38.3,2.6).lineTo(-38.7,0.8).lineTo(-38.2,-0.4).curveTo(-38,-1.1,-37.4,-1.5).curveTo(-35.6,-2.7,-28.3,-3.4).curveTo(-22.2,-5.1,-9.5,-3.8).curveTo(-7.3,-1.9,-7.3,-1.4).curveTo(-7.3,-1.2,-8.2,-0.4).curveTo(-8.9,0.4,-9.5,0.7).lineTo(-13.5,0.8).curveTo(-13.2,1.3,-13.2,2).curveTo(-13.2,4.1,-14.6,5.1).curveTo(-15.6,5.9,-17,5.9).curveTo(-19.6,5.9,-21.1,4.3).closePath().moveTo(25.6,2.5).curveTo(23.5,2.1,22.6,1.3).curveTo(22.4,1,22.2,0.7).lineTo(22.1,0.4).lineTo(22.2,-0.4).lineTo(21.8,-0.4).curveTo(20.7,-0.6,19.7,-1).lineTo(18.9,-1.3).lineTo(18.9,-1.6).lineTo(18.8,-1.7).curveTo(18.7,-2,18.9,-3).curveTo(19.4,-6.3,28.5,-5.9).curveTo(32.6,-5.7,38.3,-4.8).lineTo(38.4,-4.8).lineTo(38.5,-4.2).curveTo(38.7,-3.8,38.5,-2.7).curveTo(38.4,-1.9,32.9,-0.8).lineTo(30.8,-0.4).lineTo(30.5,-0.3).lineTo(30.5,-0.2).curveTo(30,2.7,27,2.7).lineTo(25.6,2.5).closePath();
	this.shape_6.setTransform(96.2,88.1);

	// Layer 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#EACFB7").beginStroke().moveTo(-19.6,4.8).lineTo(-20.7,4.7).curveTo(-20.7,4.5,-21,4.2).curveTo(-21.3,4,-21.3,2.2).lineTo(-33.5,3.2).curveTo(-34.9,3.3,-36,3.2).lineTo(-36.7,3).curveTo(-36.7,2.9,-36.7,2.9).curveTo(-36.7,2.9,-36.8,2.9).curveTo(-36.8,2.9,-36.9,2.9).curveTo(-36.9,3,-37,3).curveTo(-37.2,3.2,-37.3,1.8).curveTo(-37.4,0.4,-37,-0).curveTo(-36.2,-0.8,-33.3,-1.3).curveTo(-28.9,-2.1,-7.2,-3.8).lineTo(-2.9,-4.1).curveTo(-2.9,-4,4.5,-4.2).lineTo(19.2,-4.7).curveTo(24.2,-4.9,27.8,-4.8).curveTo(32.9,-4.6,35.1,-3.8).curveTo(37.3,-3,37.3,-1.3).curveTo(37.3,0.2,36.1,0.5).lineTo(34,0.5).lineTo(32.6,0.5).curveTo(32.5,2.9,32.2,3.6).lineTo(29.4,4.1).curveTo(24.9,4,23.7,2.4).curveTo(23.4,2,23.3,-0).lineTo(20,-0.4).curveTo(-6.2,0.6,-7.2,0.1).lineTo(-7.2,0.1).lineTo(-12.1,1).curveTo(-12.2,2.5,-13.4,3.5).curveTo(-14.7,4.7,-16.7,4.8).lineTo(-18.1,4.9).lineTo(-19.6,4.8).closePath();
	this.shape_7.setTransform(94.6,87.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#DEBBA4").beginStroke().moveTo(21.4,58.2).curveTo(14.1,55.4,6.6,48).curveTo(3.7,45.1,1.7,40.7).lineTo(-1,34.9).lineTo(0.3,34.2).curveTo(2.2,33.1,2.2,30.7).curveTo(2.2,29.6,2,29).curveTo(1.3,27.6,-0.7,27.6).curveTo(-1.8,27.6,-5.2,28.7).curveTo(-8.5,29.8,-9.7,29.8).curveTo(-13.6,29.8,-17.3,27.8).curveTo(-22.7,24.9,-26.2,18.4).curveTo(-26.5,17.8,-26.4,16.8).lineTo(-26.3,14.9).curveTo(-26.4,11.2,-23.7,9.3).curveTo(-22.7,8.6,-22,7.8).curveTo(-18.9,8.5,-16.2,10).curveTo(-14.2,11.2,-11.7,11.2).curveTo(-10.6,11.2,-10,10.9).curveTo(-9.1,10.5,-9.1,9.1).curveTo(-5.4,11.5,-1.1,11.9).curveTo(3.3,12.4,2.4,9.7).curveTo(-1.7,-2.6,-3.6,-18.3).curveTo(-5.4,-34.7,-3.4,-45.1).curveTo(-1.6,-54.4,2.2,-59.4).lineTo(2.6,-59.9).curveTo(13.9,-44.9,16,-27.2).curveTo(17.3,-16.4,18,8.8).curveTo(19.9,24.3,22.4,37.6).curveTo(24.9,50.7,26.5,59.9).curveTo(23.7,59.1,21.4,58.2).closePath();
	this.shape_8.setTransform(30.8,89.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#AA8462").beginStroke().moveTo(9.8,42.5).curveTo(5.5,42.1,1.8,39.8).lineTo(1.8,39.7).curveTo(1.8,38.1,-0.9,36.2).curveTo(-3.2,34.6,-6.8,33.5).curveTo(-8.5,33,-9.8,33).curveTo(-13.6,11.1,-13.6,10.1).curveTo(-13.6,2.9,-12.1,-5.7).curveTo(-9.9,-18.3,-5.2,-28.2).curveTo(-1,-36.8,4.8,-42.6).curveTo(9.1,-37,12.4,-31.6).lineTo(13.6,-29.4).lineTo(13.5,-29.3).lineTo(13.1,-28.7).curveTo(9.3,-23.8,7.5,-14.5).curveTo(5.5,-4,7.3,12.4).curveTo(9.2,28,13.3,40.3).curveTo(14.1,42.6,11,42.6).lineTo(9.8,42.5).closePath();
	this.shape_9.setTransform(19.9,58.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#CEA077").beginStroke().moveTo(-29.3,6.8).curveTo(-33.7,5.7,-37.3,10).lineTo(-38.6,7.8).curveTo(-41.8,2.4,-46.1,-3.2).curveTo(-32.8,-16.5,-10.9,-14.4).lineTo(14.7,-14.4).curveTo(34.8,-10,46.1,0).curveTo(42.7,14.1,21.1,14.7).lineTo(19.1,14.7).curveTo(2,14.7,-29.3,6.8).closePath();
	this.shape_10.setTransform(70.8,19.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#000000").beginStroke().moveTo(-1.1,77.7).curveTo(-7.2,76.8,-11.6,75.5).curveTo(-22.3,72.3,-29.7,67.2).curveTo(-41.3,59.1,-44.9,46.1).lineTo(-44.9,46).lineTo(-48.2,46.1).curveTo(-58.3,46.1,-64,39.4).curveTo(-68.1,34.6,-68.3,29.9).lineTo(-69.2,29.5).curveTo(-69.4,27.4,-69.4,25.1).curveTo(-69.4,20.7,-69,18.8).curveTo(-68,14.7,-64.7,13.7).curveTo(-65.9,10.7,-66.9,-0).lineTo(-67.7,-10.3).curveTo(-67.7,-33.1,-58.9,-51.3).curveTo(-54.4,-60.7,-48,-66.9).curveTo(-33.5,-81,-9.4,-78.7).lineTo(-9.4,-79.1).lineTo(15.3,-79.1).curveTo(41.2,-74.5,54.6,-60.4).curveTo(60.6,-54,64.1,-44.2).curveTo(66.3,-38.2,67.3,-32.8).curveTo(66.9,-32.9,68.1,-28.5).curveTo(69.4,-23.6,69.4,-12.5).curveTo(69.5,0.1,67.4,6.2).curveTo(67,7.3,67.2,11.2).curveTo(67.5,15.4,67.4,16.1).curveTo(66.5,21.9,65.3,26.5).curveTo(63.5,33.6,59.9,42).curveTo(59.6,42.6,59.1,45.3).lineTo(58.6,48.9).curveTo(57.7,60.7,50.3,69.1).curveTo(42.6,77.7,31.5,78.8).lineTo(10.8,79.1).curveTo(2.8,78.3,-1.1,77.7).closePath().moveTo(-36.7,39.2).curveTo(-36.5,39.8,-36.5,41).curveTo(-36.5,43.3,-38.4,44.5).lineTo(-39.7,45.1).lineTo(-37,50.9).curveTo(-35,55.4,-32.1,58.2).curveTo(-24.6,65.6,-17.3,68.4).curveTo(-15,69.3,-12.2,70.1).curveTo(1,73.8,23.3,73.7).curveTo(33.5,73.6,41.1,68.9).curveTo(51.2,62.6,53,49.3).lineTo(53,49.1).curveTo(49.8,49.5,45.8,50.6).curveTo(41.4,51.7,38.1,52.2).curveTo(30.5,53.5,21.9,53.2).curveTo(16.2,53,7.2,51.6).lineTo(0.8,49.3).curveTo(0.8,46.3,6,47).curveTo(20.8,49.4,36.6,47.1).curveTo(41.3,46.4,44.9,45.4).curveTo(48.4,44.5,52.9,42.8).curveTo(53.6,42.5,54.7,40.1).lineTo(58.8,31.1).curveTo(60.9,26,61.8,21.3).curveTo(62.5,17.1,62.5,11.3).curveTo(62.5,9.9,62.7,9).curveTo(62.5,8.4,62.5,7.7).curveTo(63.4,-0.2,64,-8.2).curveTo(65,-23.8,62.7,-31.2).curveTo(61.4,-36.3,58.8,-42.3).curveTo(53.7,-54.4,47.5,-59.8).curveTo(36.1,-69.8,16.1,-74.2).lineTo(-9.5,-74.2).curveTo(-31.4,-76.3,-44.8,-63).curveTo(-50.6,-57.2,-54.7,-48.6).curveTo(-59.5,-38.7,-61.7,-26.1).curveTo(-63.2,-17.5,-63.2,-10.3).curveTo(-63.2,-9.3,-59.3,12.6).curveTo(-58.1,12.6,-56.4,13.1).curveTo(-52.8,14.2,-50.5,15.8).curveTo(-47.8,17.7,-47.8,19.3).lineTo(-47.8,19.4).curveTo(-47.8,20.7,-48.7,21.1).curveTo(-49.3,21.4,-50.4,21.4).curveTo(-52.9,21.4,-54.9,20.3).curveTo(-57.6,18.8,-60.7,18).curveTo(-61.4,18.8,-62.4,19.5).curveTo(-65,21.4,-65,25.1).lineTo(-65.1,27).curveTo(-65.1,28,-64.9,28.6).curveTo(-61.3,35.1,-56,38).curveTo(-52.3,40,-48.3,40).curveTo(-47.1,40,-43.8,38.9).curveTo(-40.5,37.8,-39.3,37.8).curveTo(-37.3,37.8,-36.7,39.2).closePath().moveTo(17.8,33.5).curveTo(16.7,32.4,16.6,30.9).lineTo(16.7,28.9).curveTo(16.7,27.2,17.7,25.1).curveTo(18.9,22.7,20.4,22.8).curveTo(22,23,22.1,24.7).lineTo(22,28.9).lineTo(27.4,31.4).lineTo(41.2,31.4).curveTo(43,30.7,43.5,29.2).curveTo(43.8,28.3,43.8,25.4).curveTo(43.7,21.2,46.2,22.6).curveTo(48.8,24.1,48.8,27.9).curveTo(48.8,31.8,47.2,33.5).curveTo(44.1,36.8,34.4,36.8).curveTo(21.3,36.8,17.8,33.5).closePath();
	this.shape_11.setTransform(69.4,79.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-26.3,63.2).curveTo(-27.8,54,-30.3,40.8).curveTo(-32.8,27.6,-34.8,12.1).curveTo(-35.4,-13.1,-36.8,-23.9).curveTo(-38.9,-41.6,-50.2,-56.6).lineTo(-50.1,-56.8).curveTo(-46.4,-61.1,-42,-60).curveTo(-8.9,-51.5,8.4,-52).curveTo(29.9,-52.6,33.4,-66.7).curveTo(39.6,-61.3,44.7,-49.3).curveTo(47.3,-43.2,48.6,-38.2).curveTo(50.9,-30.8,49.9,-15.1).curveTo(49.3,-7.2,48.4,0.8).curveTo(48.4,1.4,48.6,2.1).curveTo(48.4,3,48.4,4.4).curveTo(48.4,10.1,47.7,14.3).curveTo(46.8,19.1,44.7,24.1).lineTo(40.6,33.1).curveTo(39.5,35.5,38.8,35.8).curveTo(34.3,37.6,30.8,38.5).curveTo(27.2,39.4,22.5,40.1).curveTo(6.7,42.5,-8.1,40.1).curveTo(-13.3,39.4,-13.3,42.3).lineTo(-6.9,44.6).curveTo(2.1,46,7.8,46.2).curveTo(16.4,46.5,24,45.3).curveTo(27.3,44.7,31.7,43.6).curveTo(35.7,42.6,38.9,42.1).lineTo(38.9,42.3).curveTo(37.1,55.6,27,62).curveTo(19.4,66.7,9.2,66.7).lineTo(8,66.7).curveTo(-13.4,66.7,-26.3,63.2).closePath().moveTo(3.6,18.2).curveTo(2.6,20.3,2.6,21.9).lineTo(2.5,23.9).curveTo(2.6,25.5,3.7,26.6).curveTo(7.2,29.9,20.3,29.9).curveTo(30,29.9,33.1,26.6).curveTo(34.7,24.8,34.7,21).curveTo(34.7,17.1,32.1,15.7).curveTo(29.6,14.2,29.7,18.5).curveTo(29.7,21.3,29.4,22.3).curveTo(28.9,23.8,27.1,24.4).lineTo(13.3,24.4).lineTo(7.9,21.9).lineTo(8,17.7).curveTo(7.9,16.1,6.3,15.9).lineTo(6.1,15.9).curveTo(4.7,15.9,3.6,18.2).closePath().moveTo(-10,5.6).curveTo(-9.6,5.9,-9.6,6.1).lineTo(-8.5,6.2).curveTo(-7.1,6.3,-5.6,6.2).curveTo(-3.6,6.1,-2.3,4.9).curveTo(-1.2,3.9,-1.1,2.4).lineTo(3.8,1.5).lineTo(3.9,1.5).curveTo(4.9,2,31.1,1).lineTo(34.4,1.4).curveTo(34.5,3.4,34.8,3.8).curveTo(36,5.4,40.4,5.5).lineTo(43.3,5).curveTo(43.5,4.3,43.6,1.9).lineTo(45,1.9).lineTo(47.2,1.9).curveTo(48.4,1.6,48.4,0.1).curveTo(48.4,-1.6,46.1,-2.4).curveTo(43.9,-3.2,38.9,-3.4).curveTo(35.3,-3.5,30.2,-3.3).lineTo(15.5,-2.8).curveTo(8.1,-2.6,8.1,-2.7).lineTo(3.9,-2.4).curveTo(-17.8,-0.7,-22.3,0.1).curveTo(-25.2,0.6,-25.9,1.4).curveTo(-26.4,1.8,-26.3,3.2).curveTo(-26.2,4.6,-25.9,4.4).curveTo(-25.9,4.4,-25.8,4.3).curveTo(-25.8,4.3,-25.7,4.3).curveTo(-25.7,4.3,-25.7,4.3).curveTo(-25.7,4.3,-25.7,4.4).lineTo(-24.9,4.6).curveTo(-23.9,4.7,-22.4,4.6).lineTo(-10.3,3.6).curveTo(-10.3,5.4,-10,5.6).closePath();
	this.shape_12.setTransform(83.5,86.1);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,139.8,158.3);


(lib.FinalScorecopy = function() {
	this.initialize();

	// Layer 2
	this.mc_Score = new cjs.Text("20465", "30px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_Score.name = "mc_Score";
	this.mc_Score.textAlign = "center";
	this.mc_Score.lineHeight = 32;
	this.mc_Score.setTransform(112.7,23);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(3,1,1).moveTo(117.1,27.1).lineTo(117.1,31.6).lineTo(112.3,31.6).lineTo(-112.6,31.6).lineTo(-117.1,31.6).lineTo(-117.1,27.1).lineTo(-117.1,-26.7).lineTo(-117.1,-31.7).lineTo(-112.6,-31.7).lineTo(112.3,-31.7).lineTo(117.1,-31.7).lineTo(117.1,-27.1).lineTo(117.1,-26.7).closePath();
	this.shape.setTransform(117.1,35.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#9E8C49").beginStroke().moveTo(112.3,29.4).lineTo(112.3,-25).lineTo(-112.7,-25).lineTo(-112.7,29.4).lineTo(-117.1,29.4).lineTo(-117.1,-24.4).lineTo(-117.1,-29.4).lineTo(-112.7,-29.4).lineTo(112.3,-29.4).lineTo(117.1,-29.4).lineTo(117.1,-24.8).lineTo(117.1,-24.4).lineTo(117.1,29.4).closePath();
	this.shape_1.setTransform(117.1,33.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#474747").beginStroke().moveTo(-108.1,12).lineTo(-108.1,4.2).lineTo(-108.1,-12).lineTo(108.1,-12).lineTo(108.1,12).closePath();
	this.shape_2.setTransform(117.1,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#9E8C48").beginStroke().moveTo(112.3,2.3).lineTo(-112.7,2.3).lineTo(-117.1,2.3).lineTo(-117.1,-2.3).lineTo(-112.7,-2.3).lineTo(112.3,-2.3).lineTo(117.1,-2.3).lineTo(117.1,2.3).closePath();
	this.shape_3.setTransform(117.1,64.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#666666").beginStroke().moveTo(-108.1,10.7).lineTo(-108.1,-10.7).lineTo(108.1,-10.7).lineTo(108.1,10.7).closePath();
	this.shape_4.setTransform(117.1,23.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#CCB55E").beginStroke().moveTo(-112.5,27.2).lineTo(-112.5,-27.2).lineTo(112.5,-27.2).lineTo(112.5,27.2).closePath().moveTo(-107.9,-1.3).lineTo(-107.9,14.9).lineTo(-107.9,22.7).lineTo(108.2,22.7).lineTo(108.2,-1.3).lineTo(108.2,-22.6).lineTo(-107.9,-22.6).closePath();
	this.shape_5.setTransform(117,35.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.mc_Score);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1.5,2.2,237.2,66.3);


(lib.FB_butn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-6.9,32.3).lineTo(-6.9,2.8).lineTo(-16.8,2.8).lineTo(-16.8,-8.6).lineTo(-6.9,-8.6).lineTo(-6.9,-17.1).curveTo(-6.9,-24.4,-2.8,-28.4).curveTo(1.2,-32.3,7.9,-32.3).curveTo(13.6,-32.3,16.8,-31.8).lineTo(16.8,-21.6).lineTo(10.7,-21.6).curveTo(7.4,-21.6,6,-20).curveTo(5,-18.8,5,-16).lineTo(5,-8.6).lineTo(16.4,-8.6).lineTo(14.9,2.8).lineTo(5,2.8).lineTo(5,32.3).closePath();
	this.shape.setTransform(25.8,21.4,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#3A5A94").beginStroke().moveTo(-14.1,19.1).curveTo(-19.1,19.1,-19.1,14.1).lineTo(-19.1,-14.1).curveTo(-19.1,-19.1,-14.1,-19.1).lineTo(14.1,-19.1).curveTo(19.1,-19.1,19.1,-14.1).lineTo(19.1,14.1).curveTo(19.1,19.1,14.1,19.1).closePath();
	this.shape_1.setTransform(19,19.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.1,0.1,38.2,38.2);


(lib.EndWhiteBoxcopy = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(350.5,-175.2).lineTo(-350.4,-175.2).curveTo(-364.6,-175.2,-364.6,-161.1).lineTo(-364.6,-88.8).lineTo(-364.6,-29.4).lineTo(-364.6,161.1).curveTo(-364.6,175.2,-350.4,175.2).lineTo(350.5,175.2).curveTo(364.6,175.2,364.6,161.1).lineTo(364.6,-29.4).lineTo(364.6,-88.8).lineTo(364.6,-161.1).curveTo(364.6,-175.2,350.5,-175.2).closePath();
	this.shape.setTransform(363.7,195.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#C1C1C1").beginStroke().moveTo(-364.6,72.9).lineTo(-364.6,13.5).lineTo(-364.6,-58.8).curveTo(-364.6,-72.9,-350.4,-72.9).lineTo(350.5,-72.9).curveTo(364.6,-72.9,364.6,-58.8).lineTo(364.6,13.5).lineTo(364.6,72.9).closePath();
	this.shape_1.setTransform(363.7,92.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-350.4,102.3).curveTo(-364.6,102.3,-364.6,88.1).lineTo(-364.6,-102.3).lineTo(364.6,-102.3).lineTo(364.6,88.1).curveTo(364.6,102.3,350.5,102.3).closePath();
	this.shape_2.setTransform(363.7,268.1);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2.9,18,733.3,354.5);


(lib.EndWhiteBox = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(350.5,-175.2).lineTo(-350.4,-175.2).curveTo(-364.6,-175.2,-364.6,-161.1).lineTo(-364.6,-88.8).lineTo(-364.6,-8.4).lineTo(-364.6,161.1).curveTo(-364.6,175.2,-350.4,175.2).lineTo(350.5,175.2).curveTo(364.6,175.2,364.6,161.1).lineTo(364.6,-8.4).lineTo(364.6,-88.8).lineTo(364.6,-161.1).curveTo(364.6,-175.2,350.5,-175.2).closePath();
	this.shape.setTransform(363.7,195.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#C1C1C1").beginStroke().moveTo(-364.6,83.4).lineTo(-364.6,3).lineTo(-364.6,-69.3).curveTo(-364.6,-83.4,-350.4,-83.4).lineTo(350.5,-83.4).curveTo(364.6,-83.4,364.6,-69.3).lineTo(364.6,3).lineTo(364.6,83.4).closePath();
	this.shape_1.setTransform(363.7,103.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-350.4,91.8).curveTo(-364.6,91.8,-364.6,77.6).lineTo(-364.6,-91.8).lineTo(364.6,-91.8).lineTo(364.6,77.6).curveTo(364.6,91.8,350.5,91.8).closePath();
	this.shape_2.setTransform(363.7,278.6);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2.9,18,733.3,354.5);


(lib.DarkOverlay = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("rgba(0,0,0,0.6)").beginStroke().moveTo(-487.2,274.1).lineTo(-487.2,-274.1).lineTo(487.2,-274.1).lineTo(487.2,274.1).closePath();
	this.shape.setTransform(487.2,274.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,974.5,548.2);


(lib.Conveyor_Line = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#333333").beginStroke().moveTo(0.2,44.6).curveTo(-1.8,43.3,-2.2,-0.1).curveTo(-2.6,-42.9,-1.9,-43.9).curveTo(-1.3,-45,-1.1,-45.1).lineTo(-1.1,-44.9).curveTo(0.5,-26,1.2,-4.6).lineTo(2.4,44.2).lineTo(1.3,44.8).lineTo(1.4,45.1).lineTo(1.3,45.1).curveTo(1,45.1,0.2,44.6).closePath();
	this.shape.setTransform(2.4,45.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,4.8,90.2);


(lib.ButtonSupply = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#834F9E").beginStroke().moveTo(52.8,58.5).curveTo(29.6,43.2,14.1,34.2).curveTo(-6.8,22.3,-23.2,11.3).curveTo(-44.2,-3.4,-61.9,-15.4).lineTo(-76.8,-25.4).curveTo(-75.7,-27.3,-75,-36.2).lineTo(-75.1,-44.5).curveTo(-74.8,-62.7,-74.7,-75.7).curveTo(-61.8,-67.6,-49,-59.4).curveTo(-36,-51.1,-8.7,-35.6).curveTo(16.9,-21,33.7,-12.4).lineTo(71.5,10.7).lineTo(75.5,13.2).lineTo(75.5,26.7).curveTo(76.9,42.3,76.8,58.6).curveTo(76.7,74.3,76.9,75.6).curveTo(69.5,69.6,52.8,58.5).closePath();
	this.shape.setTransform(76.9,75.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,153.7,151.3);


(lib.ButtonMarketing = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#527BCE").beginStroke().moveTo(52.5,56.8).curveTo(43.8,51.4,12.8,34.9).curveTo(-22,16.3,-35.1,8.9).lineTo(-75.3,-14).lineTo(-75.3,-48.4).curveTo(-74.8,-52.8,-75.1,-70).curveTo(-12,-40.2,75.3,0.6).lineTo(75.3,70.1).curveTo(58.8,60.6,52.5,56.8).closePath();
	this.shape.setTransform(75.4,70.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,150.7,140.1);


(lib.ButtonIT = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#E5B700").beginStroke().moveTo(13.3,30.7).curveTo(-54.8,-7.4,-59.7,-8.9).lineTo(-59.8,-8.9).curveTo(-59.7,-15.1,-59.3,-19.2).lineTo(-59.5,-56.7).curveTo(-24.5,-39,53.9,-1.8).lineTo(59.8,1).curveTo(60,19.3,59.6,23.4).lineTo(59.6,56.7).curveTo(36.8,43.8,13.3,30.7).closePath();
	this.shape.setTransform(59.8,56.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,119.7,113.4);


(lib.ButtonHR = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#BD3A24").beginStroke().moveTo(7.8,25.2).curveTo(2.9,21.7,-11.4,12.8).curveTo(-22.4,6,-29.2,0.3).curveTo(-42.3,-10.2,-48,-14.4).curveTo(-54.7,-19.5,-59.7,-21.7).curveTo(-59.4,-22.2,-59.4,-23.2).lineTo(-59.3,-25.5).curveTo(-59.2,-26.2,-59.8,-60.4).curveTo(-11.2,-32.4,10.2,-19.9).curveTo(35.1,-5.4,59.3,9.6).curveTo(59.4,23.3,59.8,44.7).curveTo(59.7,46.2,59,51.3).curveTo(58.1,56.2,58.5,59.5).lineTo(58.6,60.4).curveTo(35.6,44.6,7.8,25.2).closePath();
	this.shape.setTransform(59.8,60.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,119.5,120.8);


(lib.ButtonFinnance = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#4B8471").beginStroke().moveTo(35,35).curveTo(7.6,23.9,-10.1,17.5).lineTo(-50.5,-0.5).lineTo(-59.3,-4.3).curveTo(-59.5,-24.3,-59.6,-24.6).lineTo(-59.6,-45.2).curveTo(-54.7,-42.4,-43.9,-38.3).curveTo(-38.9,-36.4,-32.9,-34.3).curveTo(26.5,-13.7,40.5,-8.4).curveTo(45.9,-6.2,59.3,-1.8).lineTo(59.3,29.3).lineTo(59.6,45.2).curveTo(49.1,40.7,35,35).closePath();
	this.shape.setTransform(59.6,45.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,119.2,90.4);


(lib.ButtonCommercial = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#D67535").beginStroke().moveTo(24.7,35.3).curveTo(-19.2,16.9,-61.7,-3).curveTo(-67.2,-5.6,-75.1,-9).lineTo(-75.5,-26.6).lineTo(-75.3,-56.4).lineTo(-56.3,-50.2).curveTo(-18.7,-38.1,-4.4,-33.1).curveTo(33.9,-19.5,74.5,-5.5).lineTo(74.5,3.1).curveTo(74.5,8.6,75.5,56.4).lineTo(24.7,35.3).closePath();
	this.shape.setTransform(75.5,56.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,150.9,112.9);


(lib.BossAnims = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.Boss_01__000();
	this.instance.setTransform(400.9,0);

	this.instance_1 = new lib.Boss_01__001();
	this.instance_1.setTransform(400.9,0);

	this.instance_2 = new lib.Boss_01__002();
	this.instance_2.setTransform(400.9,0);

	this.instance_3 = new lib.Boss_01__003();
	this.instance_3.setTransform(400.9,0);

	this.instance_4 = new lib.Boss_01__004();
	this.instance_4.setTransform(400.9,0);

	this.instance_5 = new lib.Boss_01__005();
	this.instance_5.setTransform(400.9,0);

	this.instance_6 = new lib.Boss_01__006();
	this.instance_6.setTransform(400.9,0);

	this.instance_7 = new lib.Boss_01__007();
	this.instance_7.setTransform(400.9,0);

	this.instance_8 = new lib.Boss_01__008();
	this.instance_8.setTransform(400.9,0);

	this.instance_9 = new lib.Boss_01__009();
	this.instance_9.setTransform(400.9,0);

	this.instance_10 = new lib.Boss_01__010();
	this.instance_10.setTransform(400.9,0);

	this.instance_11 = new lib.Boss_01__011();
	this.instance_11.setTransform(400.9,0);

	this.instance_12 = new lib.Boss_01__012();
	this.instance_12.setTransform(400.9,0);

	this.instance_13 = new lib.Boss_01__013();
	this.instance_13.setTransform(400.9,0);

	this.instance_14 = new lib.Boss_01__014();
	this.instance_14.setTransform(400.9,0);

	this.instance_15 = new lib.Boss_01__015();
	this.instance_15.setTransform(400.9,0);

	this.instance_16 = new lib.Boss_01__016();
	this.instance_16.setTransform(400.9,0);

	this.instance_17 = new lib.Boss_01__017();
	this.instance_17.setTransform(400.9,0);

	this.instance_18 = new lib.Boss_01__018();
	this.instance_18.setTransform(400.9,0);

	this.instance_19 = new lib.Boss_01__019();
	this.instance_19.setTransform(400.9,0);

	this.instance_20 = new lib.Boss_01__020();
	this.instance_20.setTransform(400.9,0);

	this.instance_21 = new lib.Boss_01__021();
	this.instance_21.setTransform(400.9,0);

	this.instance_22 = new lib.Boss_01__022();
	this.instance_22.setTransform(400.9,0);

	this.instance_23 = new lib.Boss_01__023();
	this.instance_23.setTransform(400.9,0);

	this.instance_24 = new lib.Boss_01__024();
	this.instance_24.setTransform(400.9,0);

	this.instance_25 = new lib.Boss_01__025();
	this.instance_25.setTransform(400.9,0);

	this.instance_26 = new lib.Boss_01__026();
	this.instance_26.setTransform(400.9,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(400.9,0,400,472);


(lib.Boss_Bubble = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 33
	this.text = new cjs.Text("We have very large warehouses that hold our products and this department has to know what products and how many are in there at all times.", "27px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 33;
	this.text.lineWidth = 395;
	this.text.setTransform(135,-97.1,1.18,1.18);
	this.text._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text).wait(29).to({_off:false},0).wait(1));

	// Layer 32
	this.text_1 = new cjs.Text("We have 1000's of companies that supply products to us and managing these companies is a very imortant part of this department's role. ", "28px 'Laffayette Comic Pro'");
	this.text_1.textAlign = "center";
	this.text_1.lineHeight = 34;
	this.text_1.lineWidth = 395;
	this.text_1.setTransform(135,-97.1,1.18,1.18);
	this.text_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_1).wait(28).to({_off:false},0).to({_off:true},1).wait(1));

	// Layer 31
	this.text_2 = new cjs.Text("Planning where our products will be needed is a daily activity for the Supply Chain department.", "30px 'Laffayette Comic Pro'");
	this.text_2.textAlign = "center";
	this.text_2.lineHeight = 36;
	this.text_2.lineWidth = 395;
	this.text_2.setTransform(135,-97.1,1.18,1.18);
	this.text_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_2).wait(27).to({_off:false},0).to({_off:true},1).wait(2));

	// Layer 30
	this.text_3 = new cjs.Text("Managing the supply and demand of our products is a key responsibilty of this department.", "30px 'Laffayette Comic Pro'");
	this.text_3.textAlign = "center";
	this.text_3.lineHeight = 36;
	this.text_3.lineWidth = 395;
	this.text_3.setTransform(135,-81.2,1.18,1.18);
	this.text_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_3).wait(26).to({_off:false},0).to({_off:true},1).wait(3));

	// Layer 29
	this.text_4 = new cjs.Text("Our supply chain department make sure all our products are delivered to our outlets on time. ", "30px 'Laffayette Comic Pro'");
	this.text_4.textAlign = "center";
	this.text_4.lineHeight = 36;
	this.text_4.lineWidth = 395;
	this.text_4.setTransform(135,-97.1,1.18,1.18);
	this.text_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_4).wait(25).to({_off:false},0).to({_off:true},1).wait(4));

	// Layer 28
	this.text_5 = new cjs.Text("Writing a press release, dealing with the TV channels, organising and planning company events are just part of this functions responsibilities.", "28px 'Laffayette Comic Pro'");
	this.text_5.textAlign = "center";
	this.text_5.lineHeight = 34;
	this.text_5.lineWidth = 395;
	this.text_5.setTransform(135,-97.1,1.18,1.18);
	this.text_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_5).wait(24).to({_off:false},0).to({_off:true},1).wait(5));

	// Layer 27
	this.text_6 = new cjs.Text("Developing relationships with our customers and responding to their inquiries is a key part of the Marketing and Communications day to day job.", "25px 'Laffayette Comic Pro'");
	this.text_6.textAlign = "center";
	this.text_6.lineHeight = 31;
	this.text_6.lineWidth = 395;
	this.text_6.setTransform(135,-97.1,1.18,1.18);
	this.text_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_6).wait(23).to({_off:false},0).to({_off:true},1).wait(6));

	// Layer 26
	this.text_7 = new cjs.Text("Creating new ideas for products and services that we offer is important and Marketing support us with this.", "30px 'Laffayette Comic Pro'");
	this.text_7.textAlign = "center";
	this.text_7.lineHeight = 36;
	this.text_7.lineWidth = 395;
	this.text_7.setTransform(135,-97.1,1.18,1.18);
	this.text_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_7).wait(22).to({_off:false},0).to({_off:true},1).wait(7));

	// Layer 25
	this.text_8 = new cjs.Text("Understanding new trends with products is a very important part of what Marketing do.", "30px 'Laffayette Comic Pro'");
	this.text_8.textAlign = "center";
	this.text_8.lineHeight = 36;
	this.text_8.lineWidth = 395;
	this.text_8.setTransform(135,-65.1,1.18,1.18);
	this.text_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_8).wait(21).to({_off:false},0).to({_off:true},1).wait(8));

	// Layer 24
	this.text_9 = new cjs.Text("Our Marketing department consistently listen to our customer to make sure we are providing them with what they need.", "30px 'Laffayette Comic Pro'");
	this.text_9.textAlign = "center";
	this.text_9.lineHeight = 36;
	this.text_9.lineWidth = 395;
	this.text_9.setTransform(135,-97.1,1.18,1.18);
	this.text_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_9).wait(20).to({_off:false},0).to({_off:true},1).wait(9));

	// Layer 23
	this.text_10 = new cjs.Text("When our business buys other businesses then HR support with this.", "30px 'Laffayette Comic Pro'");
	this.text_10.textAlign = "center";
	this.text_10.lineHeight = 36;
	this.text_10.lineWidth = 395;
	this.text_10.setTransform(135,-67.1,1.18,1.18);
	this.text_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_10).wait(19).to({_off:false},0).to({_off:true},1).wait(10));

	// Layer 22
	this.text_11 = new cjs.Text("They offer colleagues a range of discounts off new computers, food, holidays and many more products.", "30px 'Laffayette Comic Pro'");
	this.text_11.textAlign = "center";
	this.text_11.lineHeight = 36;
	this.text_11.lineWidth = 395;
	this.text_11.setTransform(135,-97.1,1.18,1.18);
	this.text_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_11).wait(18).to({_off:false},0).to({_off:true},1).wait(11));

	// Layer 21
	this.text_12 = new cjs.Text("Along with other departments HR look after the health and welfare of our colleagues.", "30px 'Laffayette Comic Pro'");
	this.text_12.textAlign = "center";
	this.text_12.lineHeight = 36;
	this.text_12.lineWidth = 395;
	this.text_12.setTransform(135,-97.1,1.18,1.18);
	this.text_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_12).wait(17).to({_off:false},0).to({_off:true},1).wait(12));

	// Layer 20
	this.text_13 = new cjs.Text("HR provide training opportunities for people wishing to further develop their skills.", "30px 'Laffayette Comic Pro'");
	this.text_13.textAlign = "center";
	this.text_13.lineHeight = 36;
	this.text_13.lineWidth = 395;
	this.text_13.setTransform(135,-81.2,1.18,1.18);
	this.text_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_13).wait(16).to({_off:false},0).to({_off:true},1).wait(13));

	// Layer 19
	this.text_14 = new cjs.Text("Our HR teams support our colleagues in other departments in hiring new employees.", "30px 'Laffayette Comic Pro'");
	this.text_14.textAlign = "center";
	this.text_14.lineHeight = 36;
	this.text_14.lineWidth = 395;
	this.text_14.setTransform(135,-68.1,1.18,1.18);
	this.text_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_14).wait(15).to({_off:false},0).to({_off:true},1).wait(14));

	// Layer 18
	this.text_15 = new cjs.Text("Over 350 people are employed in our IT function.", "30px 'Laffayette Comic Pro'");
	this.text_15.textAlign = "center";
	this.text_15.lineHeight = 36;
	this.text_15.lineWidth = 395;
	this.text_15.setTransform(135,-72.7,1.18,1.18);
	this.text_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_15).wait(14).to({_off:false},0).to({_off:true},1).wait(15));

	// Layer 17
	this.text_16 = new cjs.Text("Some of our new starters will get their laptops set up by our IT function.", "30px 'Laffayette Comic Pro'");
	this.text_16.textAlign = "center";
	this.text_16.lineHeight = 36;
	this.text_16.lineWidth = 395;
	this.text_16.setTransform(135,-72.7,1.18,1.18);
	this.text_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_16).wait(13).to({_off:false},0).to({_off:true},1).wait(16));

	// Layer 16
	this.text_17 = new cjs.Text("When one of our computers isn't working the IT department will fix it for us.", "30px 'Laffayette Comic Pro'");
	this.text_17.textAlign = "center";
	this.text_17.lineHeight = 36;
	this.text_17.lineWidth = 395;
	this.text_17.setTransform(135,-72.7,1.18,1.18);
	this.text_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_17).wait(12).to({_off:false},0).to({_off:true},1).wait(17));

	// Layer 15
	this.text_18 = new cjs.Text("All of our electronic communications are looked after by our IT department.", "30px 'Laffayette Comic Pro'");
	this.text_18.textAlign = "center";
	this.text_18.lineHeight = 36;
	this.text_18.lineWidth = 395;
	this.text_18.setTransform(135,-72.7,1.18,1.18);
	this.text_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_18).wait(11).to({_off:false},0).to({_off:true},1).wait(18));

	// Layer 14
	this.text_19 = new cjs.Text("The IT department make sure all our computers are working correctly.", "30px 'Laffayette Comic Pro'");
	this.text_19.textAlign = "center";
	this.text_19.lineHeight = 36;
	this.text_19.lineWidth = 395;
	this.text_19.setTransform(135,-65.1,1.18,1.18);
	this.text_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_19).wait(10).to({_off:false},0).to({_off:true},1).wait(19));

	// Layer 13
	this.text_20 = new cjs.Text("Finance report how well we have done each year to our shareholders.", "30px 'Laffayette Comic Pro'");
	this.text_20.textAlign = "center";
	this.text_20.lineHeight = 36;
	this.text_20.lineWidth = 395;
	this.text_20.setTransform(135,-63.2,1.18,1.18);
	this.text_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_20).wait(9).to({_off:false},0).to({_off:true},1).wait(20));

	// Layer 12
	this.text_21 = new cjs.Text("We spend £175m per year with our number 1 supplier of the products we sell to our customers.", "30px 'Laffayette Comic Pro'");
	this.text_21.textAlign = "center";
	this.text_21.lineHeight = 36;
	this.text_21.lineWidth = 395;
	this.text_21.setTransform(135,-97.1,1.18,1.18);
	this.text_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_21).wait(8).to({_off:false},0).to({_off:true},1).wait(21));

	// Layer 11
	this.text_22 = new cjs.Text("We process 4.5m documents per year - receiving post is important to help us do this as quickly as possible. ", "30px 'Laffayette Comic Pro'");
	this.text_22.textAlign = "center";
	this.text_22.lineHeight = 36;
	this.text_22.lineWidth = 395;
	this.text_22.setTransform(135,-97.1,1.18,1.18);
	this.text_22._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_22).wait(7).to({_off:false},0).to({_off:true},1).wait(22));

	// Layer 10
	this.text_23 = new cjs.Text("They're responsible for things such as buying new businesses, making sure people pay us, ensuring we don't spend more money than we make, preparing statements to the city of London and preparing our budgets.", "22px 'Laffayette Comic Pro'");
	this.text_23.textAlign = "center";
	this.text_23.lineHeight = 28;
	this.text_23.lineWidth = 395;
	this.text_23.setTransform(135,-97.1,1.18,1.18);
	this.text_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_23).wait(6).to({_off:false},0).to({_off:true},1).wait(23));

	// Layer 9
	this.text_24 = new cjs.Text("Over 500 people work in our Finance function.", "30px 'Laffayette Comic Pro'");
	this.text_24.textAlign = "center";
	this.text_24.lineHeight = 36;
	this.text_24.lineWidth = 395;
	this.text_24.setTransform(135,-49.1,1.18,1.18);
	this.text_24._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_24).wait(5).to({_off:false},0).to({_off:true},1).wait(24));

	// Layer 8
	this.text_25 = new cjs.Text("We manufacture products ourselves for some of our brands.", "30px 'Laffayette Comic Pro'");
	this.text_25.textAlign = "center";
	this.text_25.lineHeight = 36;
	this.text_25.lineWidth = 395;
	this.text_25.setTransform(135,-72.7,1.18,1.18);
	this.text_25._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_25).wait(4).to({_off:false},0).to({_off:true},1).wait(25));

	// Layer 7
	this.text_26 = new cjs.Text("TP has over 1500 suppliers to their businesses.", "30px 'Laffayette Comic Pro'");
	this.text_26.textAlign = "center";
	this.text_26.lineHeight = 36;
	this.text_26.lineWidth = 390;
	this.text_26.setTransform(132,-68.1,1.18,1.18);
	this.text_26._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_26).wait(3).to({_off:false},0).to({_off:true},1).wait(26));

	// Layer 6
	this.text_27 = new cjs.Text("It is vital for us to get our post quickly, many of ouR products are sourced from China and take a long time to arrive. This mail could be really important.", "27px 'Laffayette Comic Pro'");
	this.text_27.textAlign = "center";
	this.text_27.lineHeight = 33;
	this.text_27.lineWidth = 395;
	this.text_27.setTransform(135,-97.1,1.18,1.18);
	this.text_27._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_27).wait(2).to({_off:false},0).to({_off:true},1).wait(27));

	// Layer 3
	this.text_28 = new cjs.Text("The Commercial team are responsible for choosing which products we sell and at what price. They also have to negotiate the prices with our suppliers.", "27px 'Laffayette Comic Pro'");
	this.text_28.textAlign = "center";
	this.text_28.lineHeight = 33;
	this.text_28.lineWidth = 395;
	this.text_28.setTransform(135,-97.1,1.18,1.18);
	this.text_28._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_28).wait(1).to({_off:false},0).to({_off:true},1).wait(28));

	// Layer 1
	this.text_29 = new cjs.Text("Over 200 people work in the commercial teams at Travis Perkins Group.", "30px 'Laffayette Comic Pro'");
	this.text_29.textAlign = "center";
	this.text_29.lineHeight = 36;
	this.text_29.lineWidth = 402;
	this.text_29.setTransform(139,-57.1,1.18,1.18);

	this.timeline.addTween(cjs.Tween.get(this.text_29).to({_off:true},1).wait(29));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(271.4,-157.7).lineTo(-212.5,-157.7).curveTo(-226.7,-157.7,-226.7,-143.6).lineTo(-226.7,-0.9).lineTo(-285.5,72.8).lineTo(-226.7,68.1).lineTo(-226.7,143.6).curveTo(-226.7,157.7,-212.5,157.7).lineTo(271.4,157.7).curveTo(285.5,157.7,285.5,143.6).lineTo(285.5,-143.6).curveTo(285.5,-157.7,271.4,-157.7).closePath();
	this.shape.setTransform(107.3,38.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#66CCFF").beginStroke().moveTo(-212.5,157.7).curveTo(-226.7,157.7,-226.7,143.6).lineTo(-226.7,68.1).lineTo(-285.5,72.8).lineTo(-226.7,-0.9).lineTo(-226.7,-143.6).curveTo(-226.7,-157.7,-212.5,-157.7).lineTo(271.4,-157.7).curveTo(285.5,-157.7,285.5,-143.6).lineTo(285.5,143.6).curveTo(285.5,157.7,271.4,157.7).closePath();
	this.shape_1.setTransform(107.3,38.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(30));

	// Layer 5
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("rgba(0,0,0,0.502)").beginStroke().moveTo(-217.9,157.7).curveTo(-232.1,157.7,-232.1,143.6).lineTo(-232.1,68.1).lineTo(-280.1,65.6).lineTo(-232.1,-0.9).lineTo(-232.1,-143.6).curveTo(-232.1,-157.7,-217.9,-157.7).lineTo(266,-157.7).curveTo(280.1,-157.7,280.1,-143.6).lineTo(280.1,143.6).curveTo(280.1,157.7,266,157.7).closePath();
	this.shape_2.setTransform(121.2,46.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(30));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-180.2,-121.4,581.6,326);


(lib.BGBox = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#E8E7E6").beginStroke().moveTo(-219.7,76.7).lineTo(-219.7,-76.7).lineTo(219.7,-76.7).lineTo(219.7,76.7).closePath();
	this.shape.setTransform(521.9,629.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(302.1,553.2,439.5,153.5);


(lib.Behind = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#5B371D").beginStroke().moveTo(-6.6,21.6).curveTo(-2.4,11.5,-2.4,4.8).curveTo(-2.4,-2.5,-3.2,-15.6).lineTo(-3.8,-26.8).curveTo(-2.2,-23.3,-0.8,-20.9).curveTo(3.9,-12.9,5.9,-8).curveTo(8.7,-1.1,8.7,4.8).curveTo(8.7,12.2,3.4,18.2).curveTo(-0.9,23.2,-8.7,26.8).lineTo(-6.6,21.6).closePath();
	this.shape.setTransform(28.3,48.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#352011").beginStroke().moveTo(-8.1,40.5).curveTo(-10.5,38.6,-11.1,30.5).curveTo(-11.3,27.9,-11.3,18.9).curveTo(-11.9,8.1,-12.1,0.9).curveTo(-11.2,-3.6,-9.5,-9.6).curveTo(-8.7,-12.7,-9.2,-18.8).lineTo(-9.9,-26.3).curveTo(-8.9,-30.4,-7.6,-34.7).curveTo(-6.9,-36.8,-4.5,-38.3).curveTo(-3.2,-39.1,0.1,-40.5).curveTo(1.8,-37.3,5.4,-28.4).curveTo(9.4,-18.7,10.7,-16).lineTo(11.3,-4.7).curveTo(12.1,8.4,12.1,15.7).curveTo(12.1,22.4,7.9,32.5).lineTo(5.8,37.7).curveTo(2.5,39.2,-1.4,40.5).closePath();
	this.shape_1.setTransform(13.8,38);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-15.3,43.2).curveTo(-19.2,39.7,-20.2,31.6).curveTo(-20.4,29,-20.4,18.9).curveTo(-20.4,11.3,-19.5,5.2).lineTo(-18.7,0.8).curveTo(-18.5,8,-17.9,18.8).curveTo(-17.9,27.9,-17.7,30.5).curveTo(-17.2,38.5,-14.7,40.4).lineTo(-8,40.4).curveTo(-4.1,39.1,-0.8,37.6).curveTo(7,34,11.3,29).curveTo(16.6,23,16.5,15.6).curveTo(16.5,9.7,13.8,2.8).curveTo(11.8,-2.1,7.1,-10.1).curveTo(5.7,-12.5,4,-16).curveTo(2.8,-18.7,-1.2,-28.5).curveTo(-4.9,-37.4,-6.5,-40.6).curveTo(-9.8,-39.2,-11.2,-38.4).curveTo(-13.5,-36.9,-14.2,-34.8).curveTo(-15.6,-30.5,-16.6,-26.4).lineTo(-16.7,-29.1).curveTo(-17.1,-41.3,-8,-43.1).curveTo(-3.4,-44.1,3.1,-29.5).curveTo(6.5,-22.1,9.8,-12.8).curveTo(12.3,-7,17,1.9).curveTo(20.5,9.3,20.4,15.4).curveTo(20.5,31.7,-2.2,41.2).curveTo(-4.8,42.2,-7.7,43.2).closePath();
	this.shape_2.setTransform(20.5,38);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-5.1,40.9,86.4);


(lib.Avatars = function() {
	this.initialize();

}).prototype = p = new cjs.Container();
p.nominalBounds = null;


(lib.Arrow_Butncopy = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(6.8,18.6).lineTo(-0,9).lineTo(-6.9,18.6).lineTo(-24.9,18.6).lineTo(-8.3,-0.9).lineTo(-22.8,-18.6).lineTo(-5.6,-18.6).lineTo(-0,-10.5).lineTo(5.6,-18.6).lineTo(22.8,-18.6).lineTo(8.3,-0.9).lineTo(24.9,18.6).closePath();
	this.shape.setTransform(33.9,36);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(23.8,32.1).curveTo(22.5,32.6,21.1,33.1).curveTo(16.4,34.4,11,34.4).curveTo(-3.4,34.4,-13.6,24.2).curveTo(-23.8,14,-23.8,-0.4).curveTo(-23.8,-14.9,-13.6,-25.1).curveTo(-6.5,-32.2,2.7,-34.4).curveTo(2.9,-34.4,3,-34.4);
	this.shape_1.setTransform(23.8,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill().beginStroke("#333333").setStrokeStyle(4,1,1).moveTo(-21.4,-32.9).curveTo(-17.6,-33.7,-13.4,-33.7).curveTo(1,-33.7,11.2,-23.5).curveTo(21.4,-13.3,21.4,1.1).curveTo(21.4,15.6,11.2,25.8).curveTo(5.9,31.1,-0.6,33.7);
	this.shape_2.setTransform(48.3,33.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#F4D96F").beginStroke().moveTo(-16.3,23.4).curveTo(-25,16.1,-27.8,6.4).curveTo(-30.6,-3.2,-27.1,-13.5).curveTo(-23.5,-23.9,-14.2,-33.2).lineTo(-13.9,-33.3).curveTo(-10,-34.2,-5.9,-34.2).curveTo(8.6,-34.2,18.8,-24).curveTo(29,-13.8,29,0.7).curveTo(29,15.1,18.8,25.3).curveTo(13.5,30.7,6.9,33.2).lineTo(4.2,34.2).curveTo(-8.1,30.3,-16.3,23.4).closePath();
	this.shape_3.setTransform(40.7,34.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB55E").beginStroke().moveTo(-12.3,24.2).curveTo(-22.5,14,-22.5,-0.5).curveTo(-22.5,-14.9,-12.3,-25.1).curveTo(-5.1,-32.2,4.1,-34.4).curveTo(-5.3,-25,-8.8,-14.7).curveTo(-12.3,-4.4,-9.5,5.3).curveTo(-6.7,15,2,22.3).curveTo(10.2,29.2,22.5,33).curveTo(17.7,34.4,12.4,34.4).curveTo(-2.1,34.4,-12.3,24.2).closePath();
	this.shape_4.setTransform(22.5,35.3);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.ARR = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(-5.3,-0.8).lineTo(3,-7).curveTo(2.7,-1.2,4.5,4.7).lineTo(5.3,7).closePath();
	this.shape.setTransform(-3,26);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#333333").beginStroke().moveTo(-20.4,7.8).lineTo(-21.2,5.5).curveTo(-22.9,-0.3,-22.6,-6.1).lineTo(3,-25.1).lineTo(3,-7.6).lineTo(22.6,-7.6).lineTo(22.6,7.7).lineTo(3,7.7).lineTo(3,25.1).closePath();
	this.shape_1.setTransform(22.7,25.1);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-8.2,0,53.6,50.2);


(lib._50Percent_white = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("rgba(255,255,255,0.502)").beginStroke().moveTo(-500,285.9).lineTo(-500,-285.9).lineTo(500,-285.9).lineTo(500,285.9).closePath();
	this.shape.setTransform(500,286);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1000,571.9);


(lib.YesBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Yes_Btn();
	this.instance.setTransform(99,31.7,1,1,0,0,0,99,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(-84.5,30.7).lineTo(-84.5,-30.7).lineTo(84.5,-30.7).lineTo(84.5,30.7).closePath();
	this.shape.setTransform(81.5,30.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1,x:99}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85,x:99.1}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.WinnersCup = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(272.5,380.2,0.48,0.48,0,0,0,291.4,34.5);

	this.text = new cjs.Text("young grafter", "58px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 61;
	this.text.lineWidth = 318;
	this.text.setTransform(266.8,86.6,1.18,1.18,-8);

	// Layer 6
	this.instance_1 = new lib.Handle();
	this.instance_1.setTransform(82.5,164.8,1,1,0,0,180,82.8,103.8);

	this.instance_2 = new lib.Handlecopy();
	this.instance_2.setTransform(458.1,164.8,1,1,0,0,0,82.8,103.8);

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-166.7,-51.1).lineTo(-93.2,-61.2).lineTo(130.5,-92.1).moveTo(-66,92).lineTo(-24,86.1).lineTo(166.7,59.1);
	this.shape.setTransform(242.1,147.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#7A6C38").beginStroke().moveTo(-12.5,44.2).curveTo(-28.3,28.2,-40,7.6).curveTo(-60.1,-27.7,-68.6,-53.1).curveTo(-70.2,-57.7,-71.3,-66.5).lineTo(2.1,-76.6).curveTo(5.7,-64.6,8.1,-57).curveTo(34.7,25.5,71.3,70.7).lineTo(29.4,76.6).curveTo(4.4,61.2,-12.5,44.2).closePath();
	this.shape_1.setTransform(146.8,163.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#9E8C49").beginStroke().moveTo(-21.7,110.7).curveTo(-64.8,97,-96.5,77.4).lineTo(-54.5,71.4).curveTo(-91.1,26.2,-117.7,-56.2).curveTo(-120.2,-63.8,-123.7,-75.9).lineTo(100,-106.7).curveTo(143.6,-103.2,199.9,-97.1).curveTo(200.3,-97.1,201.5,-97.3).curveTo(202.3,-97.5,202.2,-97.2).curveTo(200.8,-84.7,196.2,-67.8).curveTo(193.8,-58.9,190.5,-48.3).curveTo(187,-35.8,178.4,-18.6).curveTo(174.1,-10,163.2,9.4).curveTo(152.6,28.3,136.2,44.4).lineTo(-54.5,71.4).lineTo(-53.1,73.1).curveTo(-41.1,87.6,-29.1,95.4).lineTo(-15.6,103.7).curveTo(-10.5,107,-5.2,110.7).closePath().moveTo(-197.4,-67.6).curveTo(-199.2,-73.8,-200,-78).curveTo(-200.4,-81.4,-200.7,-82.6).curveTo(-200.7,-86.6,-201.3,-91.2).curveTo(-202,-97,-202.2,-100.3).lineTo(-179.3,-104.9).curveTo(-161.7,-108.2,-149.8,-110).curveTo(-146.5,-110.5,-141.6,-110.6).lineTo(-133.7,-110.7).curveTo(-127.9,-90.2,-123.7,-75.9).lineTo(-197.2,-65.7).curveTo(-197.2,-67.1,-197.4,-67.6).closePath();
	this.shape_2.setTransform(272.6,162.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#CCB55E").beginStroke().moveTo(-6.4,111.6).curveTo(-11.7,108,-16.9,104.7).lineTo(-30.3,96.3).curveTo(-42.4,88.5,-54.4,74.1).lineTo(-55.8,72.4).lineTo(135,45.3).curveTo(108.8,71.1,85.5,85.4).curveTo(50.4,107,10.6,111.6).closePath().moveTo(-135,-109.8).curveTo(-34,-113.9,56.3,-108.7).curveTo(75.8,-107.6,98.8,-105.8).lineTo(-125,-75).curveTo(-129.2,-89.3,-135,-109.8).closePath();
	this.shape_3.setTransform(273.9,161.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#000000").beginStroke().moveTo(-22.3,119.4).curveTo(-68.2,109.1,-106.1,82.3).lineTo(-110.1,79.4).curveTo(-140.9,56.7,-164.1,24.3).curveTo(-185.8,-6,-197.8,-40.2).curveTo(-208.7,-71.3,-208.7,-96.8).lineTo(-208.6,-100.2).lineTo(-209.3,-100.5).curveTo(-209.3,-100.5,-209.4,-100.5).curveTo(-209.4,-100.5,-209.4,-100.5).curveTo(-209.4,-100.5,-209.5,-100.5).curveTo(-209.6,-100.5,-209.6,-100.5).curveTo(-210,-100.4,-210,-102.4).curveTo(-210,-107.3,-183.6,-111.7).curveTo(-177.6,-112.7,-163.1,-114.7).curveTo(-151.8,-116.2,-148.7,-117).lineTo(-135.9,-117.5).curveTo(-40.6,-121.6,57.3,-117).curveTo(69.1,-115.2,139.5,-109.9).curveTo(148.3,-109.4,172.9,-106.6).curveTo(203.3,-103.1,206.1,-101.3).curveTo(207.9,-101.1,208.6,-100.7).curveTo(209.5,-100.1,209.8,-98.6).curveTo(210.2,-96.6,209.8,-94.3).curveTo(209.8,-72.4,197.3,-40.5).curveTo(183.6,-5.1,160.2,26.5).curveTo(153.9,35.1,147.1,43).curveTo(125.1,68.7,98.7,86.5).curveTo(58.8,113.5,13,119.4).closePath().moveTo(-133.6,-110.6).lineTo(-141.5,-110.5).curveTo(-146.4,-110.4,-149.7,-109.9).curveTo(-161.6,-108.1,-179.2,-104.8).lineTo(-202.1,-100.2).curveTo(-201.9,-96.9,-201.2,-91.1).curveTo(-200.6,-86.5,-200.6,-82.5).curveTo(-200.3,-81.3,-199.9,-77.9).curveTo(-199.1,-73.7,-197.3,-67.6).curveTo(-197.1,-67,-197.1,-65.7).curveTo(-196,-56.9,-194.4,-52.2).curveTo(-185.8,-26.8,-165.8,8.5).curveTo(-154,29.1,-138.3,45).curveTo(-121.4,62.1,-96.4,77.5).curveTo(-64.7,97.1,-21.6,110.8).lineTo(-5.1,110.8).lineTo(12,110.8).curveTo(51.7,106.1,86.8,84.6).curveTo(110.2,70.2,136.3,44.5).curveTo(152.7,28.4,163.3,9.5).curveTo(174.2,-9.9,178.5,-18.5).curveTo(187.1,-35.7,190.6,-48.2).curveTo(193.9,-58.8,196.3,-67.7).curveTo(200.9,-84.7,202.3,-97.1).curveTo(202.4,-97.4,201.6,-97.2).curveTo(200.4,-97,200,-97.1).curveTo(143.7,-103.1,100.1,-106.6).curveTo(77.1,-108.4,57.7,-109.6).curveTo(7.4,-112.4,-46.1,-112.4).curveTo(-88.8,-112.4,-133.6,-110.6).closePath();
	this.shape_4.setTransform(272.5,162.2);

	// Layer 7
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#7A6C38").beginStroke().moveTo(-28.8,29.7).curveTo(-42.1,27.7,-45,27.1).curveTo(-44.3,23.1,-35.2,20.2).curveTo(-30.4,18.7,-14.9,15.6).curveTo(0.2,12.6,7.8,10.1).curveTo(19.5,6.2,24.2,0.6).curveTo(26,-4.8,30.6,-16.3).curveTo(35.1,-28.1,36.4,-31.9).curveTo(41,-32,45.1,-31.9).curveTo(43.9,-5.3,36.8,6.8).curveTo(30.1,18.1,16.4,24.1).curveTo(10.1,26.9,-9.8,31.8).lineTo(-10.4,32).curveTo(-19.8,31,-28.8,29.7).closePath();
	this.shape_5.setTransform(223,302.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#9E8C49").beginStroke().moveTo(-23.9,33.5).curveTo(-53.8,33.2,-80.8,30.4).lineTo(-80.3,30.3).curveTo(-60.4,25.3,-54,22.5).curveTo(-40.4,16.6,-33.7,5.2).curveTo(-26.5,-6.9,-25.4,-33.5).curveTo(-7.4,-33,-2.5,-26.7).curveTo(-0.2,-23.8,0.3,-18.5).curveTo(0.6,-15.4,1,-8.7).curveTo(2.5,4,15.6,10.5).curveTo(34.3,19.7,80.8,21).lineTo(80.8,22).lineTo(80.2,24).curveTo(78.7,26.9,76,27.6).curveTo(68.9,29.5,66.4,30).curveTo(61.8,31,56.5,31.2).curveTo(10.4,33.5,-18.3,33.5).lineTo(-23.9,33.5).closePath();
	this.shape_6.setTransform(293.4,303.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#000000").beginStroke().moveTo(-7.6,42).curveTo(-45.9,41.3,-79,36.3).lineTo(-80.6,36).curveTo(-96.4,33.5,-102.4,31.1).curveTo(-107.6,28.9,-107.6,25.4).curveTo(-107.6,21.4,-99,16.4).curveTo(-90.4,11.5,-79,8.8).curveTo(-69,6.5,-51.3,2.6).curveTo(-37.9,-0.8,-34.3,-4.8).curveTo(-31,-8.6,-29.7,-15.1).lineTo(-28.1,-26.6).curveTo(-27.2,-33.2,-25.2,-36.5).curveTo(-22.4,-40.9,-16.4,-42).curveTo(-11,-41.7,-6.3,-41.2).curveTo(21.2,-38.2,26.6,-28.1).curveTo(28.5,-24.6,28.5,-17.6).curveTo(28.6,-10.3,29.9,-7.4).curveTo(34.3,2.6,47.3,7.5).curveTo(56.7,11,73.1,12.4).lineTo(96.7,14.4).curveTo(107.1,16.1,107.5,20.7).curveTo(107.8,23.2,107,25.5).curveTo(105.9,28.3,102.7,30.2).curveTo(93.8,35.2,68.8,38.2).curveTo(36.6,42,1.9,42).lineTo(-7.6,42).closePath().moveTo(-15.1,-33.8).curveTo(-16.4,-30,-21,-18.2).curveTo(-25.5,-6.7,-27.3,-1.3).curveTo(-32,4.3,-43.7,8.2).curveTo(-51.3,10.7,-66.4,13.7).curveTo(-81.9,16.8,-86.8,18.3).curveTo(-95.8,21.2,-96.6,25.2).curveTo(-93.6,25.8,-80.3,27.8).curveTo(-71.3,29.1,-61.9,30.1).curveTo(-34.9,32.9,-5,33.1).curveTo(24.7,33.4,75.4,30.9).curveTo(80.7,30.6,85.3,29.7).curveTo(87.8,29.2,94.9,27.3).curveTo(97.6,26.6,99.1,23.6).lineTo(99.7,21.7).lineTo(99.7,20.7).curveTo(53.2,19.4,34.5,10.1).curveTo(21.4,3.6,19.9,-9.1).curveTo(19.5,-15.8,19.2,-18.8).curveTo(18.7,-24.2,16.4,-27).curveTo(11.5,-33.3,-6.5,-33.8).lineTo(-9.9,-33.9).lineTo(-15.1,-33.8).closePath();
	this.shape_7.setTransform(274.5,304);

	// Layer 8
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#9E8C49").beginStroke().moveTo(-141.7,7).lineTo(-130.6,0.4).curveTo(-122.2,-4.7,-116.8,-7).lineTo(-75.4,-4.9).curveTo(-73.9,-4.5,-45.2,-2.9).lineTo(-16.7,-1.5).lineTo(-14.4,-0.8).lineTo(113.5,-2.9).curveTo(120.5,-1.9,129.3,1.4).curveTo(136.5,4.1,141.7,7).closePath();
	this.shape_8.setTransform(272.5,332);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#CCB55E").beginStroke().moveTo(-151.6,30.6).lineTo(-152.6,23.5).lineTo(-153.9,-4.3).curveTo(-155.2,-30.4,-155.2,-32.1).curveTo(-155.2,-34.2,-153.1,-36.8).curveTo(-150.6,-39.5,-149.7,-40.5).lineTo(151.2,-40.2).curveTo(151.6,-39.8,152.3,-38.1).curveTo(153,-36.3,153.2,-36).curveTo(157.2,1,153.2,36.9).curveTo(142.8,36.9,142.8,37.5).curveTo(139,37.7,45,37.9).curveTo(42.7,38.6,-16.2,39.2).curveTo(-18.4,39.9,-151,40.5).curveTo(-150.7,36.9,-151.6,30.6).closePath();
	this.shape_9.setTransform(270.4,385.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#000000").beginStroke().moveTo(-158.4,51.8).curveTo(-158.4,50.1,-157.6,49).lineTo(-157.1,48.5).curveTo(-157,48.5,-157,48.4).curveTo(-156.9,48.4,-156.9,48.3).curveTo(-156.8,48.3,-156.8,48.2).curveTo(-156.8,48.2,-156.8,48.2).curveTo(-159.4,31.1,-160.3,12.5).curveTo(-161,-0.9,-161,-23.4).curveTo(-161,-28.7,-160.9,-29.6).curveTo(-160,-34.8,-156.4,-36).curveTo(-155.7,-36.6,-154.5,-37.2).lineTo(-152.8,-37.2).curveTo(-148.3,-40.8,-139,-45.5).curveTo(-127.7,-51.4,-116.8,-55.5).curveTo(-72.4,-55.5,-70.5,-54.9).curveTo(-15,-54.2,-12.8,-53.5).lineTo(114.4,-53.5).curveTo(142.4,-46.2,157.3,-37.2).lineTo(157,-37.2).curveTo(160.9,-30.7,160.9,3.3).lineTo(161,28.4).curveTo(160.7,41,159.7,46.4).curveTo(160.3,47,159.9,48.4).curveTo(159.4,49.8,158.8,49.7).curveTo(157.9,51.6,157,51.6).curveTo(153.8,51.6,153.6,51.2).lineTo(146.1,50.9).curveTo(139.7,50.6,132.3,51.1).lineTo(89.3,51.5).curveTo(46.1,52,44.9,52.4).lineTo(13.8,52.8).curveTo(-17.5,53.3,-18.9,53.7).lineTo(-154.5,55.5).curveTo(-158.4,51.9,-158.4,51.8).closePath().moveTo(-153.2,-27.7).curveTo(-155.3,-25.1,-155.3,-23).curveTo(-155.3,-21.3,-154,4.8).lineTo(-152.7,32.6).lineTo(-151.7,39.6).curveTo(-150.8,45.9,-151.2,49.6).curveTo(-18.5,49,-16.3,48.3).curveTo(42.6,47.7,44.9,47).curveTo(138.9,46.8,142.7,46.5).curveTo(142.7,46,153.1,45.9).curveTo(157.1,10.1,153.1,-27).curveTo(152.8,-27.3,152.1,-29).curveTo(151.5,-30.7,151.1,-31.1).lineTo(-149.8,-31.5).curveTo(-150.7,-30.4,-153.2,-27.7).closePath().moveTo(-128.7,-43.7).lineTo(-139.7,-37.2).lineTo(143.7,-37.2).curveTo(138.5,-40,131.3,-42.7).curveTo(122.4,-46,115.4,-47.1).lineTo(-12.5,-45).lineTo(-14.7,-45.6).lineTo(-43.2,-47.1).curveTo(-72,-48.6,-73.4,-49).lineTo(-114.9,-51.1).curveTo(-120.3,-48.8,-128.7,-43.7).closePath();
	this.shape_10.setTransform(270.5,376.1);

	this.addChild(this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance_2,this.instance_1,this.text,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.4,42.8,541.4,388.8);


(lib.WindmillLoader = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_11 = function() {
		this.gotoAndPlay(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(11).call(this.frame_11).wait(1));

	// Layer 1
	this.instance = new lib.WindmillSpin("synched",0);
	this.instance.setTransform(55.5,64.7,1,1,0,0,0,64.7,64.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regY:64.8,rotation:30,y:64.8},0).wait(1).to({rotation:60},0).wait(1).to({rotation:90},0).wait(1).to({rotation:120},0).wait(1).to({rotation:150},0).wait(1).to({rotation:180},0).wait(1).to({regX:64.8,rotation:210,x:55.4,y:64.7},0).wait(1).to({rotation:240},0).wait(1).to({rotation:270},0).wait(1).to({rotation:300},0).wait(1).to({rotation:330},0).wait(1));

	// Dark Overlay
	this.mc_bg = new lib.DarkOverlay();
	this.mc_bg.setTransform(55,73.9,1.013,1.076,0,0,0,487.2,274.2);

	this.timeline.addTween(cjs.Tween.get(this.mc_bg).wait(12));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-438.5,-221.2,987,590);


(lib.TheCounter = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("the\ncounter", "30px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 35;
	this.text.lineWidth = 395;
	this.text.setTransform(220.8,199.7,1.18,1.18,-8);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#7A6C38").beginStroke().moveTo(-14.4,52.5).lineTo(-24.4,33.2).lineTo(-25.4,31.1).lineTo(-27.8,26.5).lineTo(-28.3,25.6).curveTo(-34.4,13.4,-39.7,0.8).lineTo(-40,0.2).lineTo(-41.4,-3.1).curveTo(-50.7,-25.6,-57.5,-48.9).lineTo(2.3,-56.8).lineTo(18.6,-59).curveTo(34.6,-0.9,57.5,49.9).lineTo(39.9,52.2).lineTo(-10.9,59).lineTo(-14.4,52.5).closePath();
	this.shape.setTransform(80.9,262.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#9E8C49").beginStroke().moveTo(-25.8,165.2).curveTo(-72.3,143.3,-103.6,120.2).curveTo(-124.9,104.5,-139.8,81.9).curveTo(-142.3,78.3,-144.8,74).lineTo(-94.5,67.4).lineTo(-77,65).curveTo(-50.5,122.4,-15.1,170.1).lineTo(-25.8,165.2).closePath().moveTo(-118.4,-49.4).lineTo(141.6,-83.9).curveTo(145.2,-84.6,152.1,-85.4).curveTo(157.5,-86,163.7,-87.4).lineTo(211.7,-95.4).curveTo(204.8,-51.2,191.6,-12.8).lineTo(189.4,-6.5).curveTo(183.5,9.9,176.4,25.2).lineTo(170.3,26.2).lineTo(-79.5,59.4).curveTo(-102.4,8.7,-118.4,-49.4).closePath().moveTo(-203,-73.9).lineTo(-203.1,-74.3).lineTo(-204.2,-79.6).curveTo(-206.3,-90.9,-208,-102.4).curveTo(-211.3,-126.4,-211.7,-148.2).lineTo(-211.5,-165.3).lineTo(-201.1,-165.3).lineTo(-197.2,-166.2).lineTo(-169.8,-166.5).lineTo(-153.6,-168.7).lineTo(-143.1,-170.1).curveTo(-134.5,-109.5,-119.9,-55.2).lineTo(-136.1,-53).lineTo(-196.1,-45.1).curveTo(-200,-59.3,-203,-73.9).closePath();
	this.shape_1.setTransform(217.9,252.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#CCB55E").beginStroke().moveTo(-50.3,205.2).lineTo(-52.5,204.2).curveTo(-87.9,156.4,-114.4,99.1).lineTo(133.3,66.3).lineTo(135.9,65.8).curveTo(133.2,71.3,130.4,76.8).curveTo(123.9,89.5,120.4,96.1).curveTo(114.4,107.5,108.9,115.9).curveTo(93.9,138.5,72.7,154.3).curveTo(41.3,177.4,-5.2,199.3).curveTo(-21.2,206.7,-34.2,212.2).curveTo(-41.7,209.1,-50.3,205.2).closePath().moveTo(-180.5,-136.1).curveTo(-161.9,-138.8,-153.5,-140.9).curveTo(-134,-145.7,-104.4,-160.2).curveTo(-84.4,-169.7,-62,-185.6).curveTo(-37.8,-202.6,-34.2,-212.2).curveTo(-30.5,-202.6,-6.3,-185.6).curveTo(16.1,-169.7,36,-160.2).curveTo(65.7,-145.7,85.1,-140.9).curveTo(98.6,-137.5,138.8,-132.5).lineTo(166.2,-132.1).lineTo(170.2,-131.3).lineTo(180.5,-131.3).curveTo(180.7,-118.8,179.5,-103.7).lineTo(179.4,-102.9).lineTo(179.1,-98.8).lineTo(179,-97.9).lineTo(178.9,-97).lineTo(175.3,-67.3).lineTo(175.3,-67.2).lineTo(126.7,-59.5).lineTo(103.9,-56).lineTo(-157.3,-21.1).curveTo(-171.9,-75.5,-180.5,-136.1).closePath();
	this.shape_2.setTransform(255.2,218.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#1C180C").beginStroke().moveTo(-160.1,82.5).lineTo(-109.3,75.7).lineTo(-91.8,73.4).lineTo(158.1,40.1).lineTo(164.2,39.2).lineTo(161.1,45.7).lineTo(158.4,46.2).lineTo(-89.2,79).lineTo(-106.7,81.3).lineTo(-157,88).lineTo(-160.1,82.5).closePath().moveTo(-208.3,-31.1).lineTo(-148.3,-39.1).lineTo(-132.2,-41.2).lineTo(129.1,-76).lineTo(151.9,-79.5).lineTo(200.4,-87.3).lineTo(200.3,-86.4).lineTo(199.9,-84.1).lineTo(199.5,-81.4).lineTo(151.4,-73.5).curveTo(145.3,-72.1,139.9,-71.5).curveTo(133,-70.7,129.4,-70).lineTo(-130.6,-35.5).lineTo(-146.9,-33.3).lineTo(-206.7,-25.4).lineTo(-208.3,-31.1).closePath().moveTo(208.3,-88).lineTo(208.3,-87.9).lineTo(208,-83.8).lineTo(207.9,-82.8).lineTo(207.5,-82.8).lineTo(208.3,-88).closePath();
	this.shape_3.setTransform(230.1,238.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#000000").beginStroke().moveTo(-5.4,217.5).lineTo(-6.4,217.1).curveTo(-9.6,216,-9.1,215.8).lineTo(-9.4,215.7).curveTo(-34.6,207,-65.2,188.7).curveTo(-92.2,172.6,-124.9,147.8).curveTo(-139.1,137.1,-155,110).lineTo(-155.6,108.9).lineTo(-158.7,103.4).curveTo(-166.2,90,-172.5,75.5).lineTo(-174.9,70).lineTo(-177.7,63.2).curveTo(-193.8,22.8,-196.8,14.8).lineTo(-200.1,6.1).curveTo(-206.6,-11.5,-210.3,-24.7).curveTo(-212.3,-31.7,-213.9,-38.8).lineTo(-215.1,-44.3).curveTo(-221,-73.4,-221,-106.4).curveTo(-221.2,-119.1,-220.5,-129).lineTo(-220.1,-133.6).lineTo(-220.4,-133.9).curveTo(-221,-134.4,-221,-134.7).curveTo(-221,-136.1,-220.6,-136.5).curveTo(-220.4,-136.6,-219.7,-136.7).curveTo(-219.1,-139.6,-212,-139.5).curveTo(-201.4,-139.2,-199.1,-139.8).lineTo(-171.4,-140.1).lineTo(-157.5,-142.2).lineTo(-147.4,-143.7).lineTo(-141.3,-144.6).curveTo(-126.5,-146.8,-118.1,-148.7).curveTo(-96,-153.8,-67,-169.4).curveTo(-50.1,-178.4,-28.9,-195.9).curveTo(-5.1,-215.6,-0.1,-218.8).lineTo(0,-218.3).lineTo(0.2,-218.8).curveTo(5.2,-215.6,29,-195.9).curveTo(50.2,-178.4,67.1,-169.4).curveTo(96.1,-153.8,118.1,-148.7).curveTo(126.6,-146.8,141.4,-144.6).curveTo(164.7,-141.2,171.4,-140.1).lineTo(199.1,-139.8).curveTo(201.4,-139.2,212,-139.5).curveTo(219.2,-139.6,219.7,-136.7).curveTo(220.5,-136.6,220.7,-136.5).curveTo(221,-136.1,221,-134.7).curveTo(221,-134.4,220.4,-133.9).lineTo(220.2,-133.6).curveTo(221.3,-123.2,221,-106.4).lineTo(220.9,-105).lineTo(220.6,-99.3).curveTo(219.5,-83.4,217.4,-68.4).lineTo(217.4,-68.2).lineTo(217.4,-68.1).lineTo(216.6,-62.9).curveTo(213.8,-44.9,209.7,-28.2).curveTo(204.4,-7.1,194.8,20.3).lineTo(192.6,26.5).curveTo(187.3,41.3,180.7,58).lineTo(178.7,63.2).lineTo(178.1,64.6).curveTo(168.8,87.5,155.3,109.9).curveTo(139.1,136.7,125,147.8).curveTo(62.5,197.4,9.2,215.8).curveTo(4.5,217.4,0,218.7).lineTo(-0.4,218.8).curveTo(-1.8,218.8,-5.4,217.5).closePath().moveTo(-214.9,-114.3).curveTo(-214.5,-92.5,-211.1,-68.5).curveTo(-209.5,-57.1,-207.4,-45.8).lineTo(-206.3,-40.4).lineTo(-206.2,-40).curveTo(-203.2,-25.4,-199.3,-11.2).lineTo(-197.7,-5.5).curveTo(-190.9,17.8,-181.6,40.4).lineTo(-180.2,43.6).lineTo(-179.9,44.3).curveTo(-174.6,56.8,-168.4,69.1).lineTo(-168,69.9).lineTo(-165.6,74.6).lineTo(-164.6,76.6).lineTo(-154.6,96).lineTo(-151.1,102.4).lineTo(-148,107.9).curveTo(-145.5,112.2,-143,115.8).curveTo(-128.1,138.4,-106.8,154.1).curveTo(-75.4,177.2,-28.9,199.1).lineTo(-18.3,204).lineTo(-16.1,205).curveTo(-7.5,208.9,0,212.1).curveTo(13,206.6,29,199.1).curveTo(75.5,177.2,106.9,154.1).curveTo(128.1,138.4,143.1,115.8).curveTo(148.6,107.4,154.6,96).curveTo(158.1,89.4,164.6,76.6).curveTo(167.4,71.2,170.1,65.6).lineTo(173.2,59.1).curveTo(180.3,43.8,186.2,27.4).lineTo(188.4,21.1).curveTo(201.6,-17.3,208.6,-61.5).lineTo(209,-64.1).lineTo(209.3,-66.5).lineTo(209.5,-67.4).lineTo(209.5,-67.4).lineTo(213.1,-97.2).lineTo(213.2,-98.1).lineTo(213.3,-99).lineTo(213.6,-103.1).lineTo(213.7,-103.8).curveTo(214.9,-118.9,214.7,-131.5).lineTo(204.4,-131.5).lineTo(200.4,-132.3).lineTo(173,-132.6).curveTo(132.8,-137.7,119.3,-141.1).curveTo(99.9,-145.9,70.2,-160.3).curveTo(50.3,-169.9,27.9,-185.7).curveTo(3.7,-202.7,0,-212.4).curveTo(-3.6,-202.7,-27.8,-185.7).curveTo(-50.2,-169.9,-70.2,-160.3).curveTo(-99.8,-145.9,-119.3,-141.1).curveTo(-127.7,-139,-146.3,-136.2).lineTo(-156.7,-134.8).lineTo(-173,-132.6).lineTo(-200.4,-132.3).lineTo(-204.3,-131.5).lineTo(-214.7,-131.5).closePath();
	this.shape_4.setTransform(221,218.8);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("rgba(0,0,0,0.502)").beginStroke().moveTo(-5.4,217.5).lineTo(-6.4,217.2).curveTo(-9.6,215.9,-9.1,215.8).lineTo(-9.3,215.7).curveTo(-34.6,207,-65.2,188.7).curveTo(-92.2,172.5,-124.9,147.9).curveTo(-139.1,137.2,-155,110.1).lineTo(-155.6,109).lineTo(-158.7,103.5).curveTo(-166.2,90.1,-172.5,75.5).lineTo(-174.9,69.9).lineTo(-177.7,63.2).curveTo(-193.8,22.8,-196.8,14.8).lineTo(-200.1,6.1).curveTo(-206.5,-11.5,-210.3,-24.8).curveTo(-212.3,-31.7,-213.9,-38.9).lineTo(-215.1,-44.4).curveTo(-221,-73.5,-221,-106.4).curveTo(-221.2,-119.1,-220.5,-129).lineTo(-220.1,-133.7).lineTo(-220.4,-133.8).curveTo(-221,-134.4,-221,-134.7).curveTo(-221,-136.2,-220.6,-136.5).curveTo(-220.4,-136.6,-219.7,-136.6).curveTo(-219.1,-139.6,-212,-139.5).curveTo(-201.4,-139.3,-199.1,-139.8).lineTo(-171.4,-140.1).curveTo(-167.4,-140.8,-157.5,-142.2).lineTo(-147.3,-143.7).lineTo(-141.3,-144.6).curveTo(-126.5,-146.8,-118.1,-148.8).curveTo(-96,-153.8,-67,-169.4).curveTo(-50.1,-178.5,-28.9,-195.9).curveTo(-5.1,-215.5,-0.1,-218.7).lineTo(0,-218.3).lineTo(0.2,-218.7).curveTo(5.2,-215.5,29,-195.9).curveTo(50.2,-178.5,67.1,-169.4).curveTo(96.1,-153.8,118.1,-148.8).curveTo(126.6,-146.8,141.4,-144.6).curveTo(164.7,-141.2,171.4,-140.1).lineTo(199.1,-139.8).curveTo(201.4,-139.3,212,-139.5).curveTo(219.2,-139.6,219.7,-136.6).curveTo(220.5,-136.6,220.7,-136.5).curveTo(221,-136.2,221,-134.7).curveTo(221,-134.4,220.4,-133.8).lineTo(220.2,-133.7).curveTo(221.3,-123.2,221,-106.4).lineTo(221,-105).lineTo(220.6,-99.3).curveTo(219.5,-83.3,217.4,-68.3).lineTo(217.4,-68.2).lineTo(217.4,-68.1).lineTo(217.4,-68).lineTo(217,-63.8).lineTo(216.9,-62.9).lineTo(216.6,-62.8).curveTo(213.9,-44.9,209.7,-28.2).curveTo(204.4,-7.2,194.8,20.3).lineTo(192.6,26.5).curveTo(187.3,41.3,180.8,58).lineTo(178.7,63.2).lineTo(178.1,64.6).curveTo(168.9,87.5,155.3,109.9).curveTo(139.1,136.7,125,147.9).curveTo(62.5,197.4,9.2,215.8).curveTo(4.5,217.4,0,218.7).lineTo(-0.4,218.7).curveTo(-1.8,218.8,-5.4,217.5).closePath();
	this.shape_5.setTransform(230.5,228.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-10.2,0,484.3,447);


(lib.Table = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#A38E36").beginStroke().moveTo(31.6,61).curveTo(31,59.4,30.2,58.3).lineTo(30.2,59.1).curveTo(28,54.3,22.6,46.1).curveTo(17.4,38.6,15.6,35.7).lineTo(15.2,35.1).curveTo(13.3,32,7,20.7).curveTo(1.6,11,-1.6,6.1).curveTo(-6,0.3,-25,-29.9).curveTo(-30.7,-39,-33.9,-42.9).curveTo(-33.1,-46.3,-33.1,-53.9).curveTo(-33.1,-59.4,-33.5,-62.5).lineTo(-33.4,-62.5).lineTo(21.3,27.6).lineTo(32.9,46.6).lineTo(33.2,46.6).lineTo(33,46.4).curveTo(32.8,46.2,33,46.3).lineTo(33.4,46.4).lineTo(33.9,62.6).curveTo(32.3,62.1,31.6,61).closePath();
	this.shape.setTransform(37.8,68);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#C1A841").beginStroke().moveTo(-23.2,10.5).curveTo(-27.7,9.3,-111.8,8.9).lineTo(-194.9,8.8).curveTo(-196.8,8.1,-198.5,7.9).lineTo(-198.4,7).lineTo(-198.5,6.5).lineTo(-198.6,6.5).lineTo(-199.1,-9.7).lineTo(-175.9,-9.8).lineTo(-103.5,-10.2).curveTo(-16.9,-10.6,-4.2,-10.1).lineTo(177.9,-10.1).lineTo(179.3,-10.5).lineTo(195.8,-10.5).lineTo(199.1,-7.9).lineTo(199.1,-6.1).curveTo(199.2,-5.2,198.9,-4.8).lineTo(198.6,-2.3).curveTo(198.3,0,198.3,1.5).lineTo(198.2,4.8).curveTo(198.1,8,198.7,10.1).lineTo(169,10.1).lineTo(167.7,10.5).closePath();
	this.shape_1.setTransform(270.3,124.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#26210C").beginStroke().moveTo(10.2,69.9).curveTo(4.5,68.8,-79.3,68.4).lineTo(-162.1,68.3).curveTo(-163.9,68.2,-165.2,67.7).curveTo(-166.3,67.3,-166.9,66.8).curveTo(-167.4,66.4,-168.6,64.7).curveTo(-170,62.7,-170.7,61.9).curveTo(-172.8,59.4,-185,39.5).lineTo(-185.4,38.8).curveTo(-199.7,15.4,-204.1,9).curveTo(-212.7,-3.9,-223.3,-21.1).curveTo(-229.7,-31.5,-233.1,-37.7).lineTo(-234,-39.3).curveTo(-237.1,-45,-237.1,-46.3).curveTo(-237.1,-47.5,-236.6,-47.6).lineTo(-236.9,-49.3).curveTo(-237.1,-50.8,-237.1,-55.6).curveTo(-237.1,-63.4,-236.2,-66.5).lineTo(-236.3,-66.6).curveTo(-236.3,-67.6,-235.6,-68.1).curveTo(-235.2,-68.6,-234.7,-68.6).lineTo(-234.4,-68.6).curveTo(-230.2,-69.7,-216,-68.8).lineTo(-89.1,-68.8).lineTo(-87.8,-69.2).lineTo(-19.8,-69.2).lineTo(-18.6,-68.8).lineTo(9.4,-68.6).lineTo(14.2,-69.2).lineTo(48.2,-69.4).lineTo(52.9,-69.8).curveTo(59.9,-69.9,68,-69.5).lineTo(88.5,-68.4).lineTo(88.8,-68.2).curveTo(93.3,-69.4,118.2,-50.3).curveTo(125.4,-44.7,142.9,-30.6).curveTo(157.3,-19,162.1,-15.5).curveTo(165,-12.9,177.7,-3.7).lineTo(202.4,14.3).curveTo(235.4,38.8,235.4,43.9).curveTo(235.4,44.5,236.2,46.3).curveTo(238,55.5,236.2,64.7).curveTo(236.2,65.8,235.4,66.9).curveTo(235.3,67.3,234.5,68).curveTo(233.8,68.7,233.1,69.1).lineTo(217.7,69.3).curveTo(202.2,69.6,201.4,69.9).closePath().moveTo(-167.7,59.2).curveTo(-167,60.3,-165.4,60.7).lineTo(-165.3,60.8).lineTo(-165.2,61.3).lineTo(-165.3,62.2).curveTo(-163.6,62.4,-161.7,63.1).lineTo(-78.6,63.2).curveTo(5.5,63.5,10,64.8).lineTo(200.9,64.8).lineTo(202.2,64.4).lineTo(231.9,64.4).curveTo(231.3,62.3,231.4,59.1).lineTo(231.5,55.8).curveTo(231.5,54.3,231.8,52).lineTo(232.1,49.5).curveTo(232.4,49.1,232.3,48.1).lineTo(232.3,46.4).lineTo(229,43.8).lineTo(218.2,34.9).curveTo(202.1,21.6,196.9,18.1).curveTo(189.4,12.8,177.9,3.9).lineTo(159.6,-10.5).lineTo(120.2,-40.5).curveTo(95.7,-59.2,89.7,-64.5).lineTo(88.5,-63.7).lineTo(-215.8,-63.7).curveTo(-217,-64,-232.7,-64.4).lineTo(-232.8,-64.4).curveTo(-232.4,-61.2,-232.4,-55.7).curveTo(-232.4,-48.1,-233.2,-44.7).curveTo(-230,-40.8,-224.3,-31.8).curveTo(-205.3,-1.5,-200.9,4.3).curveTo(-197.7,9.2,-192.3,18.8).curveTo(-186,30.2,-184.1,33.3).lineTo(-183.7,33.9).curveTo(-181.9,36.8,-176.7,44.3).curveTo(-171.3,52.5,-169.1,57.2).lineTo(-169.1,56.5).curveTo(-168.3,57.6,-167.7,59.2).closePath();
	this.shape_2.setTransform(237.1,69.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#E5C74D").beginStroke().moveTo(-164.6,54.6).lineTo(-176.1,35.6).lineTo(-230.9,-54.5).curveTo(-215.2,-54.1,-213.9,-53.8).lineTo(90.4,-53.8).lineTo(91.5,-54.6).curveTo(97.5,-49.3,122,-30.6).lineTo(161.5,-0.6).lineTo(179.8,13.8).curveTo(191.2,22.7,198.8,28).curveTo(203.9,31.5,220,44.8).lineTo(230.9,53.7).lineTo(214.4,53.7).lineTo(213,54.1).lineTo(30.9,54.1).curveTo(18.1,53.6,-68.5,54).lineTo(-140.8,54.4).lineTo(-164.1,54.5).lineTo(-164.5,54.4).curveTo(-164.6,54.3,-164.5,54.4).lineTo(-164.2,54.6).lineTo(-164.6,54.6).closePath();
	this.shape_3.setTransform(235.2,59.9);

	// Layer 2
	this.instance = new lib.TableLeg();
	this.instance.setTransform(446.1,163.1,1,1,0,0,0,20,46.8);

	this.instance_1 = new lib.TableLeg();
	this.instance_1.setTransform(91.2,163.6,1,1,0,0,0,20,46.8);

	this.instance_2 = new lib.TableLeg();
	this.instance_2.setTransform(22.4,84.3,1,1,0,0,0,20,46.8);

	// Layer 4
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#7F7F7F").beginStroke().moveTo(67.9,82).curveTo(18.4,65.7,-8.4,57).curveTo(-55.9,41.6,-89.6,31.6).curveTo(-128.4,20.1,-145,15.7).curveTo(-153.8,13.4,-161.2,11.6).lineTo(-161.4,11).curveTo(-161.6,10.4,-161.6,9.2).curveTo(-161.6,6.7,-145,10.4).lineTo(-145,4.4).curveTo(-167,-1.5,-177.8,-3.9).lineTo(-178,-4.2).curveTo(-178.2,-4.5,-178.2,-5.7).curveTo(-178.2,-10.5,-146.8,-1.3).lineTo(-145,-0.8).lineTo(-145,-15.6).curveTo(-167.1,-23.1,-167.1,-22.9).curveTo(-167.1,-25.3,-145,-18.9).lineTo(-175.6,-101.2).curveTo(-192.2,-106.4,-192.2,-106.9).curveTo(-192.2,-110.3,-178.2,-107).lineTo(-175.6,-106.3).curveTo(-164.7,-103.6,-141.6,-96.3).lineTo(-90.8,-80.1).curveTo(-58.4,-70.1,-34,-64).curveTo(-26.7,-62,-1.3,-53.8).curveTo(27.5,-44.4,39.1,-41).curveTo(56.1,-36,79.6,-28.4).lineTo(87.1,-26).curveTo(115.1,-16.9,123.6,-12.9).curveTo(136.4,-6.8,165.1,41.2).curveTo(175.3,58.3,184,74.8).curveTo(192,89.9,192.2,91.9).curveTo(192.8,98.8,188.3,102.7).curveTo(181.6,108.6,163,108.6).curveTo(149.6,108.6,67.9,82).closePath();
	this.shape_4.setTransform(-161.7,108.6);

	// Layer 3
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#7F7F7F").beginStroke().moveTo(-172.8,57.4).lineTo(-184.9,37.4).lineTo(-242.4,-57.2).curveTo(-225.9,-56.8,-224.6,-56.5).lineTo(94.9,-56.5).lineTo(96.1,-57.4).curveTo(102.4,-51.7,128.1,-32.2).lineTo(169.6,-0.7).curveTo(184.5,11.2,188.8,14.5).curveTo(200.8,23.9,208.7,29.4).curveTo(214.1,33.1,231,47).lineTo(242.4,56.4).lineTo(225.1,56.4).lineTo(223.6,56.8).lineTo(32.4,56.8).curveTo(19,56.2,-71.9,56.7).lineTo(-147.9,57.1).lineTo(-172.3,57.2).lineTo(-172.7,57.1).curveTo(-172.8,57,-172.7,57.1).lineTo(-172.4,57.4).lineTo(-172.8,57.4).closePath();
	this.shape_5.setTransform(238.8,164.3);

	this.addChild(this.shape_5,this.shape_4,this.instance_2,this.instance_1,this.instance,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-353.9,0,835.2,221.7);


(lib.SteveHoodie = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape.setTransform(25.6,90.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_1.setTransform(9.8,86.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-1.6,27.3).curveTo(-2.5,20.5,-2.5,17.8).lineTo(-2.5,16.6).lineTo(-4,16).curveTo(-4.5,16.3,-5.5,16.3).curveTo(-7.3,16.3,-7.7,13.1).lineTo(-7.8,9.7).curveTo(-7.8,6.8,-7.4,5.1).lineTo(-12.4,3.8).lineTo(-15.6,2.8).curveTo(-20,1.4,-21.9,-0.2).curveTo(-24.2,-2.2,-24.2,-5.4).curveTo(-24.2,-7.9,-22.6,-11.7).curveTo(-23.6,-12.4,-24.1,-13.2).curveTo(-24.9,-14.4,-24.9,-19.1).curveTo(-24.9,-22.8,-24.3,-27.4).lineTo(-19.9,-27).curveTo(-20.4,-23.2,-20.4,-19.9).curveTo(-20.4,-17.4,-19.9,-16.5).curveTo(-17.7,-12.8,-11.8,-13.3).lineTo(16.2,-12.9).lineTo(16.2,-16.9).lineTo(16.2,-18.9).curveTo(18.8,-16.8,20.7,-14.2).lineTo(20.5,-10.8).lineTo(20.6,-10.6).curveTo(20.6,-10.3,20.4,-9.9).lineTo(20.4,-9.2).lineTo(20,-8.8).curveTo(19.5,-8.4,18.4,-8.4).lineTo(17.6,-8.4).lineTo(17.6,-8.3).curveTo(17.9,-6.3,17.6,-4.2).curveTo(17.6,-1.4,16.9,2.2).curveTo(15.7,8.2,13.2,9.6).lineTo(6.5,9.6).curveTo(2.1,7.8,-3.4,6.2).curveTo(-3.3,7.6,-3.3,9.6).lineTo(-3.3,11.8).lineTo(-2.9,11.8).curveTo(-1.7,11.8,14.2,15).lineTo(16.5,15.5).lineTo(24.9,17.1).curveTo(24.5,18.2,23.9,19.1).curveTo(22.9,20.8,21.4,22.1).lineTo(17.5,21.4).curveTo(13.4,20.7,9.2,19.7).curveTo(4.1,18.6,0.8,17.7).lineTo(1.2,19.3).curveTo(2.3,23.3,3.1,27.4).lineTo(2,27.4).lineTo(-1.6,27.3).closePath().moveTo(-19.7,-4.9).curveTo(-19.7,-4.3,-11.6,-1.4).lineTo(-6.6,0.3).curveTo(6.7,4.7,9.7,4.7).curveTo(12.9,4.7,13,-2).lineTo(12.7,-8.4).lineTo(-10.7,-8.4).lineTo(-12.9,-8.4).curveTo(-15.5,-8.9,-17.5,-9.4).lineTo(-19.7,-4.9).closePath();
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt04();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#497C59").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape_3.setTransform(18,46.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#5A996E").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_4.setTransform(85.2,58);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_5.setTransform(86,143.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_6.setTransform(67.5,124.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_7.setTransform(9.8,86.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_8.setTransform(86.9,128);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_9.setTransform(94.8,140.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-70.6,9.4).lineTo(-75.5,7.6).lineTo(-75.4,6.9).curveTo(-74.7,2.8,-74.6,0.7).lineTo(-51.3,0.7).lineTo(-51,7.1).curveTo(-51.1,13.8,-54.3,13.8).curveTo(-57.3,13.8,-70.6,9.4).closePath().moveTo(59.2,-1.5).lineTo(57.8,-7.9).curveTo(61,-9,66,-12.1).lineTo(68.5,-13.8).curveTo(69.3,-12.4,71.8,-9.1).curveTo(75.5,-4.2,75.5,0.3).curveTo(75.5,2.7,66.6,3.5).curveTo(62.5,3.8,60.3,4.4).lineTo(59.2,-1.5).closePath();
	this.shape_10.setTransform(88.9,83);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_11.setTransform(84.4,77.7);

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-3.8,-6.1,172.7,161.6);


(lib.SoundIconRoundBtncopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1));

	// Layer 3
	this.txt_Sound = new cjs.Text("Sound on", "32px 'Arial'", "#FFFFFF");
	this.txt_Sound.name = "txt_Sound";
	this.txt_Sound.lineHeight = 38;
	this.txt_Sound.lineWidth = 214;
	this.txt_Sound.setTransform(51.4,17.9,0.59,0.59);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(0.9,8.7).curveTo(0.6,8.4,0.6,8).curveTo(0.6,7.6,0.9,7.4).curveTo(3.9,4.3,3.9,0).curveTo(3.9,-4.3,0.9,-7.3).curveTo(0.6,-7.6,0.6,-8).curveTo(0.6,-8.4,0.9,-8.7).curveTo(1.2,-9,1.6,-9).curveTo(1.9,-9,2.2,-8.7).curveTo(5.9,-5.1,5.8,0).curveTo(5.9,5.1,2.2,8.7).curveTo(1.9,9,1.6,9).curveTo(1.2,9,0.9,8.7).closePath().moveTo(-2.3,6.2).curveTo(-2.6,5.9,-2.6,5.5).curveTo(-2.6,5.1,-2.3,4.8).curveTo(-0.4,2.8,-0.4,0).curveTo(-0.4,-2.8,-2.3,-4.8).curveTo(-2.6,-5.1,-2.6,-5.5).curveTo(-2.6,-5.9,-2.3,-6.1).curveTo(-2.1,-6.4,-1.6,-6.4).curveTo(-1.3,-6.4,-0.9,-6.1).curveTo(1.6,-3.6,1.6,0).curveTo(1.6,3.6,-0.9,6.2).curveTo(-1.3,6.4,-1.6,6.4).curveTo(-2.1,6.4,-2.3,6.2).closePath().moveTo(-5.6,3.6).curveTo(-5.9,3.3,-5.9,2.9).curveTo(-5.9,2.5,-5.6,2.3).curveTo(-4.7,1.3,-4.7,0).curveTo(-4.7,-1.3,-5.6,-2.2).curveTo(-5.9,-2.5,-5.9,-2.9).curveTo(-5.9,-3.3,-5.6,-3.6).curveTo(-5.3,-3.9,-4.9,-3.9).curveTo(-4.5,-3.9,-4.2,-3.6).curveTo(-2.7,-2.1,-2.7,0).curveTo(-2.7,2.1,-4.2,3.6).curveTo(-4.5,3.9,-4.9,3.9).curveTo(-5.3,3.9,-5.6,3.6).closePath();
	this.shape.setTransform(37.5,25.9);

	this.instance = new lib.S_off();
	this.instance.setTransform(38.6,26.4,1,1,0,0,0,6.5,6.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.txt_Sound,p:{text:"Sound on",lineWidth:214}}]}).to({state:[{t:this.txt_Sound,p:{text:"Sound off",lineWidth:243}},{t:this.instance}]},1).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#FFFFFF").beginStroke().moveTo(5.1,11.4).lineTo(-2.1,4.1).lineTo(-5.6,4.1).curveTo(-5.7,4.1,-5.7,4.1).curveTo(-5.8,4.1,-5.9,4.1).curveTo(-5.9,4,-6,4).curveTo(-6,4,-6.1,4).curveTo(-6.3,3.8,-6.3,3.4).lineTo(-6.3,-3.4).curveTo(-6.3,-3.8,-6.1,-4).curveTo(-6,-4,-6,-4).curveTo(-5.9,-4.1,-5.9,-4.1).curveTo(-5.8,-4.1,-5.7,-4.1).curveTo(-5.7,-4.1,-5.6,-4.1).lineTo(-2.1,-4.1).lineTo(5.1,-11.4).curveTo(5.3,-11.6,5.6,-11.6).lineTo(5.9,-11.6).curveTo(6.3,-11.5,6.3,-11).lineTo(6.3,11).curveTo(6.3,11.5,5.9,11.6).lineTo(5.6,11.6).curveTo(5.3,11.6,5.1,11.4).closePath();
	this.shape_1.setTransform(23.2,26.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(2));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#4F2A3F").beginStroke().moveTo(-103.4,19.1).curveTo(-108.4,19.1,-108.4,14.1).lineTo(-108.4,-14.1).curveTo(-108.4,-19.1,-103.4,-19.1).lineTo(103.4,-19.1).curveTo(108.4,-19.1,108.4,-14.1).lineTo(108.4,14.1).curveTo(108.4,19.1,103.4,19.1).closePath();
	this.shape_2.setTransform(118.9,25.9);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(10.5,6.8,216.8,38.2);


(lib.shirt2 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(4,39.5).curveTo(3.6,32,3.6,12.5).curveTo(3,1.9,4.1,-6.9).curveTo(2.5,-8.5,-0.9,-13.5).lineTo(-2.3,-15.6).lineTo(-2.6,-15.4).curveTo(-4.5,-17.8,-6.5,-19.5).curveTo(-10.1,-22.8,-16.8,-22.8).curveTo(-17.8,-22.8,-20,-21.8).curveTo(-22.2,-20.9,-23.2,-20.9).curveTo(-24.5,-20.9,-25.1,-21).lineTo(-25.5,-21.2).lineTo(-25.7,-21.5).lineTo(-25.6,-22.1).curveTo(-25.6,-22.6,-25.9,-23).curveTo(-29.4,-26,-31.5,-32.3).curveTo(-32.9,-36.6,-32.9,-38.4).curveTo(-32.9,-45.4,-29.2,-51.8).curveTo(-24.9,-59.5,-18.2,-59.6).curveTo(-5.3,-61.8,12.9,-59.6).curveTo(24,-57.2,29.1,-52.3).curveTo(32.9,-48.6,32.9,-43.6).curveTo(32.9,-35.8,32.7,-34.8).curveTo(32.3,-33.4,28,-24.3).curveTo(24.8,-22.4,24.5,-23.2).lineTo(24.5,-23.2).curveTo(19,-22.6,14.8,-19.5).curveTo(8.2,-14.7,8.2,-5.7).lineTo(8.2,12.9).curveTo(9.8,52.3,9.8,54.3).curveTo(9.8,59,7.5,60.7).curveTo(4.7,57.2,4,39.5).closePath().moveTo(-13.1,-27.4).curveTo(-7.9,-26.7,-4.3,-24.6).curveTo(-2.7,-23.6,-1.7,-22.5).curveTo(-0.6,-21.6,0.9,-20).curveTo(3.3,-17.4,5.2,-14.3).curveTo(6.6,-20.7,12.7,-24.6).curveTo(18.9,-28.6,24.6,-26.4).lineTo(26.2,-31.6).curveTo(28.2,-37.3,28.2,-43.3).curveTo(28.2,-46.8,24.6,-49.7).curveTo(20.2,-53.1,11.5,-54.5).curveTo(-4.2,-57.1,-17.8,-54.5).curveTo(-22.4,-52.7,-25.8,-45.2).curveTo(-28.3,-39.8,-28.3,-37.7).curveTo(-28.3,-34.8,-24.4,-30.7).curveTo(-22.4,-28.6,-21.4,-27.2).curveTo(-19.4,-27.7,-17.1,-27.7).curveTo(-15.2,-27.7,-13.1,-27.4).closePath();
	this.shape.setTransform(82.6,51.9);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#333333").beginStroke().moveTo(23.6,14.7).lineTo(23.6,12.3).lineTo(23.7,8.6).curveTo(23.9,6.3,24.5,4.9).lineTo(24.5,3).lineTo(24.2,4.7).curveTo(23.7,6.3,22.7,6.3).lineTo(22,6.2).lineTo(21.8,6.1).lineTo(21.7,5.5).lineTo(22.4,3.7).lineTo(22.3,3.1).lineTo(22.3,2.4).curveTo(21.6,3.7,21.2,4.8).curveTo(20.7,5.3,19.7,5.5).lineTo(18,3.5).curveTo(17.6,2.8,17.6,2.3).curveTo(17.6,1.9,17.9,1.1).lineTo(16.1,-0.2).lineTo(14.3,-1.5).lineTo(12.6,-2.5).curveTo(9.7,-4.4,5.9,-5.4).curveTo(3,-6.2,-1.2,-6.6).curveTo(-7.9,-7.4,-14.3,-2.1).curveTo(-14.7,-1.8,-15.2,-2.1).curveTo(-15.6,-2.4,-15.8,-2.8).lineTo(-16.1,-2.7).lineTo(-16.3,-2.7).curveTo(-17.5,-0.1,-18.1,0.4).curveTo(-18.8,-0.4,-19.2,-1.2).curveTo(-19.3,-0.6,-19.6,-0.3).curveTo(-20.1,0.4,-21.7,0.4).curveTo(-21.9,0.4,-29.8,-6.8).lineTo(-30.1,-7.1).curveTo(-28.9,-7.9,-27.2,-9.4).lineTo(-22.8,-13.5).curveTo(-20.3,-15.6,-14.4,-16.2).curveTo(-12.3,-16.4,-11.1,-17).curveTo(-4.8,-16.7,2.3,-16.2).curveTo(11.2,-15.5,17.5,-14.7).curveTo(17.9,-14.3,18.7,-14).curveTo(23.6,-12.5,25.1,-11.7).curveTo(25.6,-11.5,27.9,-8.6).curveTo(28.7,-7.6,29.5,-7.1).lineTo(28.1,-5.2).curveTo(30.1,-2.5,30.1,2.8).curveTo(30.1,6.8,28.9,11.1).curveTo(27.3,17,24.5,17).curveTo(23.8,17,23.6,14.7).closePath().moveTo(-20.3,-4.9).lineTo(-20.1,-5.1).lineTo(-20.1,-5.2).lineTo(-20.4,-5.2).lineTo(-20.3,-4.9).closePath();
	this.shape_1.setTransform(82.4,8.7);

	// Layer 4
	this.instance = new lib.TP_logoWhite();
	this.instance.setTransform(111.2,46.8,0.048,0.048,-3.5,0,0,292.3,34.4);

	// Layer 5
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-7.3,6.4).curveTo(-16.3,-0.6,-16.6,-4.1).curveTo(-16.9,-7.5,-12.9,-10.2).curveTo(-8.8,-13,-0.9,-13).curveTo(7,-13.1,12.2,-10.2).curveTo(17.3,-7.3,16.5,-2.8).curveTo(15.6,1.6,11.5,7.1).curveTo(7.3,12.7,4.5,13).lineTo(4.2,13).curveTo(1.2,13,-7.3,6.4).closePath();
	this.shape_2.setTransform(82.1,16.2);

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#282828").beginStroke().moveTo(2,44).lineTo(1.7,44).lineTo(1.7,41.8).lineTo(1.7,41.7).lineTo(1.7,41.6).lineTo(1.7,41.4).lineTo(1.7,40.6).lineTo(1.6,38.4).lineTo(1.5,38.4).lineTo(1.5,38.4).lineTo(1.5,31.1).lineTo(1.5,23.8).lineTo(15.5,23.8).lineTo(15,46).lineTo(13.7,46.1).curveTo(3,44,2,44).closePath().moveTo(-15,15.7).curveTo(-15.5,14.8,-15.5,12.3).curveTo(-15.5,5.4,-13,-4.4).lineTo(-13,-4.6).curveTo(-11.7,-9.2,-10.2,-13.6).curveTo(-7.9,-19.9,-4.9,-25.7).curveTo(2.1,-38.8,11.5,-46.1).curveTo(5,-36.9,0.1,-23.2).curveTo(-1.5,-18.8,-2.7,-14.5).lineTo(-2.6,-14.5).lineTo(-3.1,-12.9).lineTo(-6.5,10.6).lineTo(-6.5,11.8).lineTo(-6,18.9).lineTo(-6.9,18.9).lineTo(-8.3,19).curveTo(-13.1,19,-15,15.7).closePath();
	this.shape_3.setTransform(20,59.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#1E1E1E").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_4.setTransform(86,143.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#333333").beginStroke().moveTo(-16,72.9).lineTo(-22,71.8).curveTo(-35.5,69,-40,65.3).curveTo(-32.7,64.6,-30.8,64.6).curveTo(-21.4,64.6,-9.2,67.8).lineTo(0.1,70.1).curveTo(-2.5,71.9,-4.3,72.9).closePath().moveTo(40.6,63.4).curveTo(27.7,61.8,23.8,58.6).curveTo(32.8,54.2,39.1,53.4).curveTo(52.2,51.9,59.1,63.9).curveTo(56.3,64.1,54.5,64.4).lineTo(54.1,64.5).curveTo(43.2,63.7,40.6,63.4).closePath().moveTo(-59.1,33.7).lineTo(-59.5,32).curveTo(-56.2,33,-51.1,34.1).curveTo(-46.9,35,-42.8,35.8).lineTo(-40.9,46.5).curveTo(-39.4,55.9,-39.1,60.1).lineTo(-44.5,60.6).curveTo(-53.4,61.3,-55.3,61.8).curveTo(-55.3,47.3,-59.1,33.7).closePath().moveTo(13.9,48).curveTo(13.2,45,12.3,43.2).curveTo(12.6,40.5,12.6,38.6).lineTo(12.6,37.6).lineTo(21.2,37.5).lineTo(21.2,38.2).curveTo(21.2,39.8,22.8,46.9).lineTo(24.1,52.3).curveTo(22.3,53,20.7,53.8).curveTo(17.4,55.4,15.5,57.4).curveTo(15.1,52.8,13.9,48).closePath().moveTo(-37.7,31).lineTo(-43.8,29.8).lineTo(-46.1,29.4).lineTo(-51.6,28.3).lineTo(-50.2,28.1).lineTo(-49.7,5.9).lineTo(-47.6,5.9).lineTo(-47.6,6.2).curveTo(-45,5.8,-42.7,5.9).lineTo(-41.9,5.9).curveTo(-40.8,5.9,-40.3,5.6).lineTo(-39.9,5.2).lineTo(-39.9,4.4).curveTo(-39.7,4.1,-39.7,3.8).lineTo(-39.8,3.5).curveTo(-39.5,-1.9,-38.6,-12.9).lineTo(-38.4,-15).curveTo(-37.9,-20.4,-37.7,-25).curveTo(-37.4,-30,-37.4,-34.1).curveTo(-37.4,-43.5,-38.5,-43.8).curveTo(-39.4,-44.1,-40.7,-37.5).curveTo(-41.7,-32.7,-42.4,-27.7).lineTo(-42.4,-27.7).lineTo(-42.8,-25.1).lineTo(-44.1,-6.1).lineTo(-44.1,-5.7).lineTo(-44.1,-5.4).lineTo(-44.1,-5).lineTo(-44.1,-4.6).lineTo(-44.1,-3.6).lineTo(-44.1,-3.1).lineTo(-44.1,-2.6).lineTo(-44.1,1.4).lineTo(-71.2,1.1).lineTo(-71.7,-6).lineTo(-71.7,-7.2).lineTo(-68.3,-30.7).lineTo(-67.9,-32.4).lineTo(-67.9,-32.4).curveTo(-66.7,-36.7,-65.1,-41.1).curveTo(-60.3,-54.7,-53.8,-64).curveTo(-44.3,-71.4,-32.5,-72.9).lineTo(-4.3,-72.9).curveTo(24,-68.4,36.4,-60.9).curveTo(41.3,-57.9,45.5,-53.5).lineTo(52.5,-45.2).curveTo(64.1,-31.7,66,-29).curveTo(71.2,-21.5,71.7,-15.2).curveTo(68.4,-14.2,64.2,-11.9).curveTo(61.4,-10.5,59.6,-9.1).lineTo(58.3,-12.9).curveTo(56.8,-16.8,54.4,-21.4).curveTo(52.2,-25.8,50.1,-29).curveTo(49.2,-30.9,48.6,-33.7).lineTo(47.7,-38.2).curveTo(46.4,-42.8,42,-42.8).curveTo(41.8,-41.1,41.8,-38.9).curveTo(41.8,-33.6,48,-21.4).curveTo(54.9,-7.9,55.3,-5.8).curveTo(56.6,0,58.2,11.2).curveTo(59.4,17.9,61.1,23.1).curveTo(54.7,24.2,47.5,27.4).curveTo(37,31.9,21.1,31.7).lineTo(-22.6,33.2).curveTo(-27.6,32.9,-37.7,31).closePath();
	this.shape_5.setTransform(85.2,77.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#666666").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_6.setTransform(86.9,128);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#000000").beginStroke().moveTo(-22.1,77.7).curveTo(-41.7,74.4,-47.4,72.4).curveTo(-53.4,70.3,-53.5,66.6).curveTo(-58.7,38.4,-58.7,32.2).lineTo(-58.7,31).lineTo(-60.2,30.4).curveTo(-60.7,30.7,-61.7,30.7).curveTo(-63.2,30.7,-63.7,28.5).lineTo(-63.9,27.5).lineTo(-63.9,27).lineTo(-63.9,24.1).lineTo(-63.9,23.9).lineTo(-63.8,21.5).lineTo(-63.7,19.4).lineTo(-62.8,6).lineTo(-69.1,6).lineTo(-69.4,5.9).curveTo(-71.8,5.5,-78.8,2.7).curveTo(-79.8,2,-80.3,1.2).curveTo(-81.1,0,-81.1,-4.7).curveTo(-81.1,-11.8,-78.9,-21.4).lineTo(-78.8,-22.3).curveTo(-77.9,-26.1,-76.8,-29.7).curveTo(-74.2,-38.1,-70.5,-45.5).curveTo(-55.8,-74.7,-28.8,-77.3).lineTo(-0.4,-77.7).curveTo(29.9,-73,42.9,-65.3).curveTo(48.3,-62.2,52.6,-57.6).lineTo(60.2,-48.8).lineTo(71.5,-32.5).curveTo(80.3,-23,80.4,-16).lineTo(80.5,-16).lineTo(80.8,-15.5).curveTo(81.1,-15.2,81.1,-14.1).curveTo(81.1,-12.9,79.6,-11.3).curveTo(75.1,-7.7,73.8,-6.8).curveTo(68.8,-3.7,65.6,-2.6).lineTo(67,3.8).lineTo(68.1,9.7).curveTo(69.2,17.8,69.2,21.3).curveTo(69.2,26.3,67,27.7).lineTo(66.7,27).curveTo(64,28.8,57.4,31.4).lineTo(56.2,34.1).curveTo(54.4,38.4,54.2,41.9).curveTo(54,43.8,54.5,47.8).curveTo(54.5,49.4,54,50.7).curveTo(57.5,51.4,60.4,52.8).curveTo(68.5,56.8,68.5,64.4).curveTo(68.5,66.2,66.7,67.5).curveTo(63.8,69.7,57.2,69.5).lineTo(25.7,69.5).curveTo(21.2,69.6,18.6,67.3).curveTo(16.8,65.6,16.7,63.7).curveTo(15.9,61.5,14.7,56).lineTo(14.5,54.8).lineTo(14.4,55.2).curveTo(12.4,64.6,10.5,68.4).lineTo(10.8,68.8).lineTo(10.8,68.9).lineTo(10.9,68.9).lineTo(10.9,69.3).lineTo(10.9,70.1).curveTo(10.9,72.5,8,74.7).curveTo(4.9,77.1,0.3,77.7).closePath().moveTo(-48,67.2).curveTo(-46.9,68.5,-38,70).curveTo(-23.4,72.5,-21.6,72.9).lineTo(-11.9,72.9).lineTo(-0.2,72.9).curveTo(1.6,72,4.2,70.2).lineTo(-5.1,67.8).curveTo(-17.3,64.7,-26.6,64.7).curveTo(-28.6,64.7,-35.8,65.3).lineTo(-36.2,65.3).curveTo(-44.4,66.1,-45.9,66.6).lineTo(-48.4,66.6).lineTo(-48,67.2).closePath().moveTo(1.9,64.3).curveTo(5,65.5,7.3,66.7).curveTo(7.8,63.1,9.7,52.2).curveTo(11.5,42.8,12.1,37.7).lineTo(-18.3,38.2).curveTo(-27.9,37.7,-38.6,35.8).curveTo(-42.7,35.1,-47,34.1).curveTo(-52.1,33,-55.4,32.1).lineTo(-54.9,33.7).curveTo(-51.2,47.4,-51.2,61.9).curveTo(-49.3,61.4,-40.4,60.6).lineTo(-35,60.2).curveTo(-29.9,59.8,-26.9,59.8).curveTo(-10.4,59.8,1.9,64.3).closePath().moveTo(43.3,53.5).curveTo(36.9,54.2,27.9,58.7).curveTo(24.8,60.2,21.4,62.1).curveTo(22.2,62.6,22.1,62.9).curveTo(22.6,63.3,26.9,64.7).lineTo(57.3,64.7).lineTo(58.3,64.5).lineTo(58.6,64.5).curveTo(60.4,64.2,63.2,63.9).curveTo(57.1,53.3,46.2,53.3).curveTo(44.7,53.3,43.3,53.5).closePath().moveTo(31.8,37.4).lineTo(25.4,37.5).lineTo(16.7,37.7).lineTo(16.7,38.6).curveTo(16.7,40.6,16.5,43.2).curveTo(17.3,45.1,18.1,48.1).curveTo(19.3,52.8,19.6,57.4).curveTo(21.5,55.5,24.8,53.8).curveTo(26.5,53,28.2,52.4).curveTo(34.5,50,42.6,49.8).curveTo(46.6,49.6,50.1,50).lineTo(49.8,48.8).curveTo(49.7,47.7,49.7,44.3).curveTo(49.7,36.6,50.2,33.8).curveTo(42.5,35.9,31.8,37.4).closePath().moveTo(-59.6,13.3).lineTo(-59.6,20.6).lineTo(-59.6,20.6).lineTo(-59.5,20.6).lineTo(-59.4,22.8).lineTo(-59.4,23.6).lineTo(-59.4,23.7).lineTo(-59.4,23.8).lineTo(-59.4,24).lineTo(-59.4,26.2).lineTo(-59.1,26.2).curveTo(-58.1,26.2,-47.4,28.3).lineTo(-42,29.4).lineTo(-39.6,29.9).lineTo(-33.6,31).curveTo(-23.5,32.9,-18.5,33.3).lineTo(25.3,31.8).curveTo(41.1,32,51.6,27.4).curveTo(58.9,24.3,65.2,23.2).curveTo(63.5,18,62.4,11.3).curveTo(60.7,0.1,59.5,-5.7).curveTo(59,-7.9,52.2,-21.3).curveTo(46,-33.5,46,-38.8).curveTo(46,-41.1,46.1,-42.8).curveTo(50.6,-42.7,51.8,-38.1).lineTo(52.7,-33.7).curveTo(53.3,-30.8,54.2,-29).curveTo(56.3,-25.7,58.6,-21.4).curveTo(60.9,-16.8,62.5,-12.8).lineTo(63.7,-9.1).curveTo(65.6,-10.4,68.3,-11.9).curveTo(72.5,-14.2,75.9,-15.2).curveTo(75.3,-21.5,70.2,-28.9).curveTo(68.2,-31.7,56.6,-45.2).lineTo(49.6,-53.5).curveTo(45.4,-57.9,40.6,-60.8).curveTo(28.1,-68.4,-0.2,-72.8).lineTo(-28.4,-72.8).curveTo(-40.2,-71.3,-49.6,-63.9).curveTo(-59,-56.6,-66,-43.5).curveTo(-69,-37.7,-71.3,-31.4).curveTo(-72.8,-27,-74.1,-22.4).lineTo(-74.1,-22.2).curveTo(-76.6,-12.4,-76.6,-5.5).curveTo(-76.6,-3,-76.1,-2.1).curveTo(-73.9,1.6,-68,1.1).lineTo(-67.1,1.1).lineTo(-40,1.5).lineTo(-40,-2.5).lineTo(-40,-3.1).lineTo(-40,-3.5).lineTo(-40,-4.5).lineTo(-40,-4.9).lineTo(-40,-5.3).lineTo(-40,-5.7).lineTo(-40,-6.1).lineTo(-38.6,-25).lineTo(-38.3,-27.6).lineTo(-38.3,-27.6).curveTo(-37.6,-32.6,-36.6,-37.4).curveTo(-35.2,-44.1,-34.4,-43.8).curveTo(-33.3,-43.4,-33.3,-34).curveTo(-33.3,-30,-33.6,-25).curveTo(-33.8,-20.4,-34.2,-15).lineTo(-34.4,-12.8).curveTo(-35.4,-1.9,-35.7,3.6).lineTo(-35.6,3.8).curveTo(-35.6,4.1,-35.7,4.5).lineTo(-35.8,5.2).lineTo(-36.2,5.6).curveTo(-36.6,6,-37.8,6).lineTo(-38.5,6).curveTo(-40.9,5.9,-43.5,6.2).lineTo(-43.5,6).lineTo(-45.6,6).lineTo(-59.6,6).lineTo(-59.6,13.3).closePath();
	this.shape_7.setTransform(81.1,77.7);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.instance,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-8.8,162.2,164.3);


(lib.shirt = function() {
	this.initialize();

	// Layer 6
	this.instance = new lib.TP_logoWhite();
	this.instance.setTransform(111.7,49.3,0.048,0.048,-3.5,0,0,292.3,34.4);

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(-7.3,6.4).curveTo(-16.3,-0.6,-16.6,-4.1).curveTo(-16.9,-7.5,-12.9,-10.2).curveTo(-8.8,-13,-0.9,-13).curveTo(7,-13.1,12.2,-10.2).curveTo(17.3,-7.3,16.5,-2.8).curveTo(15.6,1.6,11.5,7.1).curveTo(7.3,12.7,4.5,13).lineTo(4.2,13).curveTo(1.2,13,-7.3,6.4).closePath();
	this.shape.setTransform(82.1,16.2);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#282828").beginStroke().moveTo(12.5,46).curveTo(1.8,43.9,0.9,43.8).lineTo(0.5,43.8).lineTo(0.5,41.6).lineTo(0.5,38.3).lineTo(0.3,38.2).lineTo(1.2,4.7).lineTo(15.2,4.7).lineTo(13.9,46.3).lineTo(12.5,46).closePath().moveTo(-15.2,-3.3).curveTo(-15.4,-3.6,-14.8,-4.9).curveTo(-13.3,-8.3,-11.3,-13.7).curveTo(-9.1,-20,-6,-25.8).curveTo(1,-38.9,10.3,-46.3).curveTo(3.8,-37,-1.1,-23.3).curveTo(-2.6,-18.9,-3.8,-14.7).curveTo(-5.5,-8.3,-6,-5).curveTo(-6.4,-2.4,-6.3,-0.1).lineTo(-7.2,-0.1).lineTo(-8.5,-0).curveTo(-13.3,-0,-15.2,-3.3).closePath();
	this.shape_1.setTransform(21.2,60.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#333333").beginStroke().moveTo(-41.7,49.7).lineTo(-44.1,49.2).lineTo(-48.2,48.4).lineTo(-46.8,6.8).lineTo(-44.7,6.8).lineTo(-44.7,7.1).curveTo(-42.4,6.8,-39.8,6.8).lineTo(-39,6.8).curveTo(-37.9,6.8,-37.4,6.5).lineTo(-37,6.1).lineTo(-37,5.3).curveTo(-36.8,5,-36.8,4.7).lineTo(-36.9,4.4).curveTo(-35.4,-11.5,-35.4,-14.2).curveTo(-35.4,-23.7,-36.5,-24).curveTo(-37.4,-24.3,-38.7,-17.6).curveTo(-39.7,-12.8,-40.4,-7.8).curveTo(-41,-2.4,-41.1,-0.7).lineTo(-41.2,2.3).lineTo(-68.3,2).curveTo(-68.5,-0.3,-68.1,-2.9).curveTo(-67.6,-6.2,-65.9,-12.6).curveTo(-64.7,-16.8,-63.1,-21.2).curveTo(-58.3,-34.9,-51.7,-44.2).curveTo(-43.4,-50.7,-33.3,-52.6).curveTo(-32.9,-50.5,-30.5,-44.4).curveTo(-27.3,-36.2,-24.8,-33.7).curveTo(-22.5,-31.4,-14.4,-22.1).curveTo(-7,-14.1,-5.5,-14.1).curveTo(-4.5,-14.1,-0.3,-22.1).lineTo(-0.2,-22.2).curveTo(0.6,-16.6,2.7,-7.6).curveTo(3.9,-2.8,5.6,1.3).lineTo(9.9,1.3).curveTo(9,-4.2,8.3,-7.3).curveTo(7.7,-12,5.2,-21).lineTo(3,-29).lineTo(10.1,-24.4).curveTo(20,-17.8,20.3,-17.8).curveTo(23.2,-17.8,27.9,-29.8).curveTo(32.1,-40.5,32.1,-43.5).lineTo(32.1,-44.3).curveTo(35.7,-42.7,38.4,-41).curveTo(43.3,-38.1,47.5,-33.7).lineTo(54.5,-25.4).curveTo(60.8,-18.1,62.8,-15.3).lineTo(68.3,-6.2).curveTo(61.3,-3.9,56.2,-0.1).lineTo(54.9,-3.9).curveTo(52.6,-9.9,50,-14.9).curveTo(46.3,-22,44,-23).curveTo(43.9,-22.2,43.6,-21.1).curveTo(43.5,-20.1,43.8,-19.1).lineTo(51.9,3.3).curveTo(54,9,56.2,15.8).curveTo(59.5,26.2,60.2,31.1).curveTo(61.4,37.7,63.1,42.9).curveTo(56.7,44.1,49.5,47.2).curveTo(39,51.8,23.1,51.6).lineTo(8.9,52).lineTo(8.7,52).lineTo(-20.6,53).curveTo(-27,52.6,-41.7,49.7).closePath().moveTo(-14,-30).curveTo(-20.1,-35.9,-22.4,-39.2).curveTo(-27.6,-49.8,-29.4,-53.1).lineTo(-2.3,-53.1).curveTo(16.6,-50.1,28.5,-45.7).curveTo(27.7,-43.7,26.9,-40.6).curveTo(25.6,-35.4,24.5,-32.6).curveTo(22.5,-27.8,19.5,-25.5).curveTo(18.4,-26.5,12.2,-29.9).curveTo(5.5,-33.5,3.5,-33.5).lineTo(3.4,-33.5).curveTo(2.8,-34.3,1.6,-34.3).curveTo(-1.4,-34.3,-3.5,-28.5).curveTo(-4.3,-26.4,-5.5,-21).curveTo(-7.6,-23.7,-14,-30).closePath();
	this.shape_2.setTransform(83.2,58);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#1A2D3A").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_3.setTransform(67.5,124.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#2B4B60").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_4.setTransform(86.9,128);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#3F1E11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_5.setTransform(86,143.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_6.setTransform(94.8,140.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#000000").beginStroke().moveTo(-20.1,77.7).curveTo(-39.8,74.4,-45.4,72.4).curveTo(-51.4,70.3,-51.5,66.6).curveTo(-56.7,38.4,-56.7,32.2).lineTo(-56.7,31).lineTo(-58.2,30.4).curveTo(-58.7,30.7,-59.7,30.7).curveTo(-61.2,30.7,-61.7,28.5).lineTo(-61.9,27.5).lineTo(-62,24.1).curveTo(-62,22.4,-61.3,8.7).curveTo(-60.5,-6,-60,-12.9).lineTo(-66.2,-12.9).curveTo(-68.6,-13.3,-75.9,-16.2).curveTo(-77,-17,-77.4,-17.7).curveTo(-77.9,-18.3,-77.5,-20).curveTo(-77.1,-22.2,-74.8,-29.7).curveTo(-69.4,-47.7,-58.7,-60).curveTo(-45.4,-75.1,-26.8,-77.3).lineTo(1.6,-77.7).curveTo(28.8,-73.8,43.1,-65.8).curveTo(52.1,-60.7,62.2,-48.8).lineTo(69.2,-39.1).curveTo(76.3,-29,76.9,-26.8).lineTo(77.1,-26.8).lineTo(77.4,-26.3).curveTo(77.7,-26,77.7,-24.9).curveTo(77.7,-24,76.2,-22.1).curveTo(71.9,-18.6,70.4,-17.6).curveTo(65.5,-14.5,62.2,-13.4).curveTo(62.4,-12.6,65.6,-4.9).curveTo(68.8,2.6,69,3.8).curveTo(69.7,6.9,70.7,13.5).curveTo(71.2,17.8,71.2,21.3).curveTo(71.2,26.3,68.9,27.7).lineTo(68.6,27).curveTo(65.9,28.8,59.4,31.4).lineTo(58.2,34.1).curveTo(56.4,38.4,56.2,41.9).curveTo(56,43.8,56.5,47.8).curveTo(56.5,49.4,55.9,50.7).curveTo(59.4,51.4,62.3,52.8).curveTo(70.4,56.8,70.4,64.4).curveTo(70.4,66.2,68.6,67.5).curveTo(65.7,69.7,59.2,69.5).lineTo(27.7,69.5).curveTo(23.1,69.6,20.6,67.3).curveTo(18.7,65.6,18.7,63.7).curveTo(17.9,61.5,16.7,56).lineTo(16.5,54.8).lineTo(16.4,55.2).curveTo(14.4,64.6,12.5,68.4).lineTo(12.7,68.8).lineTo(12.7,68.9).lineTo(12.8,68.9).lineTo(12.8,69.3).lineTo(12.8,70.1).curveTo(12.8,72.5,10,74.7).curveTo(6.9,77.1,2.3,77.7).closePath().moveTo(-46,67.2).curveTo(-44.9,68.5,-36,70).curveTo(-21.4,72.5,-19.7,72.9).lineTo(-9.9,72.9).lineTo(1.8,72.9).curveTo(3.6,72,6.2,70.2).lineTo(-3.1,67.8).curveTo(-15.3,64.7,-24.7,64.7).curveTo(-26.6,64.7,-33.9,65.3).lineTo(-34.2,65.3).curveTo(-42.5,66.1,-44,66.6).lineTo(-46.4,66.6).lineTo(-46,67.2).closePath().moveTo(3.9,64.3).curveTo(7,65.5,9.2,66.7).curveTo(9.7,63.1,11.7,52.2).curveTo(13.4,42.8,14.1,37.7).lineTo(-16.3,38.2).curveTo(-25.9,37.7,-36.7,35.8).curveTo(-40.8,35.1,-45,34.1).curveTo(-50.1,33,-53.4,32.1).lineTo(-53,33.7).curveTo(-49.2,47.4,-49.2,61.9).curveTo(-47.3,61.4,-38.4,60.6).lineTo(-33,60.2).curveTo(-27.9,59.8,-24.9,59.8).curveTo(-8.4,59.8,3.9,64.3).closePath().moveTo(45.2,53.5).curveTo(38.9,54.2,29.9,58.7).curveTo(26.8,60.2,23.4,62.1).curveTo(24.2,62.6,24.1,62.9).curveTo(24.5,63.3,28.9,64.7).lineTo(59.3,64.7).lineTo(60.3,64.5).lineTo(60.6,64.5).curveTo(62.4,64.2,65.2,63.9).curveTo(59.1,53.3,48.1,53.3).curveTo(46.7,53.3,45.2,53.5).closePath().moveTo(33.7,37.4).lineTo(27.3,37.5).lineTo(18.7,37.7).lineTo(18.7,38.6).curveTo(18.7,40.6,18.4,43.2).curveTo(19.3,45.1,20,48.1).curveTo(21.2,52.8,21.6,57.4).curveTo(23.5,55.5,26.8,53.8).curveTo(28.4,53,30.2,52.4).curveTo(36.5,50,44.6,49.8).curveTo(48.6,49.6,52.1,50).lineTo(51.8,48.8).curveTo(51.7,47.7,51.7,44.3).curveTo(51.7,36.6,52.2,33.8).curveTo(44.5,35.9,33.7,37.4).closePath().moveTo(-57.6,20.6).lineTo(-57.5,20.6).lineTo(-57.5,24).lineTo(-57.5,26.2).lineTo(-57.1,26.2).curveTo(-56.1,26.2,-45.5,28.3).lineTo(-44.1,28.6).lineTo(-40,29.4).lineTo(-37.6,29.9).curveTo(-22.9,32.8,-16.5,33.3).lineTo(12.8,32.3).lineTo(13,32.3).lineTo(27.2,31.8).curveTo(43.1,32,53.6,27.4).curveTo(60.8,24.3,67.2,23.2).curveTo(65.5,18,64.3,11.3).curveTo(63.6,6.5,60.3,-4).curveTo(58.1,-10.8,56,-16.5).lineTo(47.9,-38.8).curveTo(47.6,-39.9,47.7,-40.8).curveTo(48,-41.9,48.1,-42.8).curveTo(50.4,-41.8,54.1,-34.7).curveTo(56.7,-29.7,59,-23.6).lineTo(60.3,-19.9).curveTo(65.4,-23.7,72.4,-26).lineTo(66.9,-35).curveTo(64.9,-37.9,58.6,-45.2).lineTo(51.6,-53.5).curveTo(47.4,-57.9,42.5,-60.8).curveTo(39.7,-62.5,36.2,-64.1).lineTo(36.2,-63.2).curveTo(36.2,-60.3,32,-49.5).curveTo(27.3,-37.6,24.4,-37.6).curveTo(24.1,-37.6,14.2,-44.1).lineTo(7.1,-48.8).lineTo(9.3,-40.7).curveTo(11.8,-31.8,12.4,-27).curveTo(13.1,-24,14,-18.5).lineTo(9.7,-18.5).curveTo(7.9,-22.6,6.8,-27.3).curveTo(4.7,-36.4,3.9,-42).lineTo(3.8,-41.9).curveTo(-0.4,-33.9,-1.4,-33.9).curveTo(-2.9,-33.9,-10.3,-41.9).curveTo(-18.4,-51.1,-20.7,-53.5).curveTo(-23.2,-56,-26.4,-64.1).curveTo(-28.9,-70.3,-29.2,-72.4).curveTo(-39.4,-70.4,-47.6,-63.9).curveTo(-57,-56.6,-64,-43.5).curveTo(-67,-37.7,-69.3,-31.4).curveTo(-71.3,-25.9,-72.8,-22.6).curveTo(-73.3,-21.3,-73.2,-21).curveTo(-71,-17.3,-65.1,-17.8).lineTo(-64.2,-17.8).lineTo(-37.1,-17.4).lineTo(-37,-20.5).curveTo(-36.9,-22.2,-36.3,-27.6).curveTo(-35.6,-32.6,-34.6,-37.4).curveTo(-33.3,-44.1,-32.4,-43.8).curveTo(-31.3,-43.4,-31.3,-34).curveTo(-31.3,-31.3,-32.8,-15.3).lineTo(-32.7,-15.1).curveTo(-32.7,-14.8,-32.9,-14.4).lineTo(-32.9,-13.7).lineTo(-33.3,-13.3).curveTo(-33.8,-12.9,-34.9,-12.9).lineTo(-35.7,-12.9).curveTo(-38.3,-13,-40.6,-12.7).lineTo(-40.6,-12.9).lineTo(-42.7,-12.9).lineTo(-56.7,-12.9).closePath().moveTo(-18.3,-59).curveTo(-16,-55.7,-9.9,-49.8).curveTo(-3.5,-43.5,-1.4,-40.7).curveTo(-0.2,-46.2,0.6,-48.3).curveTo(2.7,-54,5.7,-54).curveTo(6.9,-54,7.5,-53.3).lineTo(7.6,-53.3).curveTo(9.6,-53.3,16.3,-49.6).curveTo(22.5,-46.3,23.6,-45.2).curveTo(26.6,-47.6,28.6,-52.4).curveTo(29.7,-55.2,31,-60.3).curveTo(31.8,-63.5,32.6,-65.5).curveTo(20.7,-69.8,1.8,-72.8).lineTo(-25.3,-72.8).curveTo(-23.5,-69.6,-18.3,-59).closePath();
	this.shape_7.setTransform(79.1,77.7);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(1.5,0,155.4,155.5);


(lib.ShareTwitter = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("Share on Twitter", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(87.1,20.9,0.59,0.59);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-25.1,15.8).curveTo(-23.9,16,-22.7,15.9).curveTo(-15.4,15.9,-9.9,11.5).curveTo(-13.2,11.4,-15.9,9.5).curveTo(-18.5,7.5,-19.5,4.3).curveTo(-18.7,4.5,-17.5,4.5).curveTo(-16.3,4.5,-14.8,4.2).curveTo(-18.4,3.5,-20.7,0.6).curveTo(-23.1,-2.2,-23.1,-5.9).lineTo(-23.1,-6).curveTo(-20.9,-4.8,-18.4,-4.8).curveTo(-20.5,-6.2,-21.8,-8.4).curveTo(-23,-10.7,-23,-13.3).curveTo(-23,-16.2,-21.6,-18.5).curveTo(-17.7,-13.8,-12.3,-11).curveTo(-6.7,-8.1,-0.4,-7.7).curveTo(-0.6,-9,-0.6,-10.1).curveTo(-0.6,-14.3,2.4,-17.4).curveTo(5.4,-20.4,9.7,-20.4).curveTo(14.2,-20.4,17.2,-17.2).curveTo(20.5,-17.8,23.7,-19.6).curveTo(22.6,-16.1,19.2,-13.9).curveTo(22.4,-14.4,25.1,-15.6).curveTo(23,-12.5,20,-10.3).lineTo(20,-8.9).curveTo(20,2.3,12.5,10.8).curveTo(4,20.4,-9.3,20.4).curveTo(-17.9,20.4,-25.1,15.8).closePath();
	this.shape.setTransform(67.5,30,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#23AADB").beginStroke().moveTo(-103.4,19.1).curveTo(-108.4,19.1,-108.4,14.1).lineTo(-108.4,-14.1).curveTo(-108.4,-19.1,-103.4,-19.1).lineTo(103.4,-19.1).curveTo(108.4,-19.1,108.4,-14.1).lineTo(108.4,14.1).curveTo(108.4,19.1,103.4,19.1).closePath();
	this.shape_1.setTransform(156.4,29.1);

	this.instance = new lib.Share_Twitter();
	this.instance.setTransform(167.2,31.7,0.85,0.85,0,0,0,166.3,30.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#3A5A94").beginStroke().moveTo(-161.9,30.6).curveTo(-166.9,30.6,-166.9,25.6).lineTo(-166.9,-25.6).curveTo(-166.9,-30.6,-161.9,-30.6).lineTo(161.9,-30.6).curveTo(166.9,-30.6,166.9,-25.6).lineTo(166.9,25.6).curveTo(166.9,30.6,161.9,30.6).closePath();
	this.shape_2.setTransform(167.8,31.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).to({state:[{t:this.instance}]},2).to({state:[{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(48,10,216.8,38.2);


(lib.ShareFB = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("Share on Facebook", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(87,21.3,0.59,0.59);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-6.9,32.3).lineTo(-6.9,2.8).lineTo(-16.8,2.8).lineTo(-16.8,-8.6).lineTo(-6.9,-8.6).lineTo(-6.9,-17.1).curveTo(-6.9,-24.4,-2.8,-28.4).curveTo(1.2,-32.3,7.9,-32.3).curveTo(13.6,-32.3,16.8,-31.8).lineTo(16.8,-21.6).lineTo(10.7,-21.6).curveTo(7.4,-21.6,6,-20).curveTo(5,-18.8,5,-16).lineTo(5,-8.6).lineTo(16.4,-8.6).lineTo(14.9,2.8).lineTo(5,2.8).lineTo(5,32.3).closePath();
	this.shape.setTransform(64.1,29.5,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#395994").beginStroke().moveTo(-103.4,19.1).curveTo(-108.4,19.1,-108.4,14.1).lineTo(-108.4,-14.1).curveTo(-108.4,-19.1,-103.4,-19.1).lineTo(103.4,-19.1).curveTo(108.4,-19.1,108.4,-14.1).lineTo(108.4,14.1).curveTo(108.4,19.1,103.4,19.1).closePath();
	this.shape_1.setTransform(156.4,29.1);

	this.instance = new lib.Share_FB();
	this.instance.setTransform(167.2,31.7,0.85,0.85,0,0,0,166.3,30.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#3A5A94").beginStroke().moveTo(-161.9,30.6).curveTo(-166.9,30.6,-166.9,25.6).lineTo(-166.9,-25.6).curveTo(-166.9,-30.6,-161.9,-30.6).lineTo(161.9,-30.6).curveTo(166.9,-30.6,166.9,-25.6).lineTo(166.9,25.6).curveTo(166.9,30.6,161.9,30.6).closePath();
	this.shape_2.setTransform(167.8,31.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).to({state:[{t:this.instance}]},2).to({state:[{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(48,10,216.8,38.2);


(lib.PostRoomLeaderBoard = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("SCORE", "49px 'VAGRounded'");
	this.text.lineHeight = 60;
	this.text.lineWidth = 163;
	this.text.setTransform(581.4,3.3,0.75,0.75);

	this.text_1 = new cjs.Text("NAME", "49px 'VAGRounded'");
	this.text_1.lineHeight = 60;
	this.text_1.lineWidth = 163;
	this.text_1.setTransform(44.5,0.4,0.75,0.75);

	this.text_2 = new cjs.Text("10,564\n9,324\n8,361\n7,183\n6,753\n6,650\n5,921", "40px 'Laffayette Comic Pro'");
	this.text_2.textAlign = "right";
	this.text_2.lineHeight = 54;
	this.text_2.lineWidth = 187;
	this.text_2.setTransform(689.2,74.8,0.88,0.88);

	this.text_3 = new cjs.Text("COOLJ\nMARCO_B\n259_BRYCE\nDAVESYKES32\nTOM93\nPAT_JONES\nJO_KINGSLEY", "40px 'Laffayette Comic Pro'");
	this.text_3.lineHeight = 54;
	this.text_3.lineWidth = 398;
	this.text_3.setTransform(45.4,74.8,0.88,0.88);

	// Layer 3
	this.instance = new lib.ARR();
	this.instance.setTransform(765.5,89.7,0.75,0.68,0,-90,90,22.6,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 193, 193, 193, 0)];
	this.instance.cache(-10,-2,58,54);

	this.instance_1 = new lib.ARR();
	this.instance_1.setTransform(765.5,360.7,0.75,0.68,-90,0,0,22.6,25.1);
	this.instance_1.filters = [new cjs.ColorFilter(0, 0, 0, 1, 193, 193, 193, 0)];
	this.instance_1.cache(-10,-2,58,54);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#C1C1C1").beginStroke().moveTo(-5.2,58.3).lineTo(-5.2,-58.3).lineTo(5.2,-58.3).lineTo(5.2,58.3).closePath();
	this.shape.setTransform(765.5,176.1);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-394.2,-187.3).lineTo(-394.2,-148.1).lineTo(-394.2,187.3).curveTo(-394,193.5,-391,196.8).curveTo(-388.9,199.1,-385.3,200).curveTo(-382.6,200.6,-380.1,200.6).lineTo(378.7,200.6).lineTo(380.1,200.6).curveTo(382.9,200.6,385.4,200).curveTo(388.9,199.1,391,196.8).curveTo(391.9,195.7,392.6,194.5).curveTo(392.9,193.8,393.2,193.1).curveTo(394,191.1,394.2,188).curveTo(394.2,187.3,394.2,186.5).lineTo(394.2,-114.1).lineTo(394.2,-148.1).lineTo(394.2,-152.3).lineTo(394.2,-186.4).curveTo(394.2,-191.3,392.6,-194.5).curveTo(389.6,-200.3,380.9,-200.6).lineTo(-380.9,-200.6).curveTo(-393.8,-200.2,-394.2,-187.3).closePath();
	this.shape_1.setTransform(394.3,200.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#C1C1C1").beginStroke().moveTo(-394.2,26.2).lineTo(-394.2,-13).curveTo(-393.8,-25.8,-380.9,-26.2).lineTo(380.9,-26.2).curveTo(389.6,-26,392.6,-20.1).curveTo(394.2,-16.9,394.2,-12.1).lineTo(394.2,22).lineTo(394.2,26.2).closePath();
	this.shape_2.setTransform(394.3,26.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#FFFFFF").beginStroke().moveTo(378.7,174.4).lineTo(-380,174.4).curveTo(-382.7,174.4,-385.3,173.8).curveTo(-388.9,172.9,-391,170.5).curveTo(-394,167.2,-394.2,161.1).lineTo(-394.2,-174.4).lineTo(394.2,-174.4).lineTo(394.2,-140.4).lineTo(394.2,160.2).lineTo(394.2,161.8).curveTo(394,164.8,393.2,166.9).lineTo(392.6,168.2).curveTo(391.9,169.5,391,170.5).curveTo(388.9,172.9,385.3,173.8).curveTo(382.9,174.4,380.1,174.4).closePath().moveTo(391,170.5).curveTo(391.9,169.5,392.6,168.2).lineTo(392.6,170.5).closePath().moveTo(392.6,168.2).closePath();
	this.shape_3.setTransform(394.3,226.8);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance_1,this.instance,this.text_3,this.text_2,this.text_1,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,792.5,429.7);


(lib.OptionsBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Options_btn();
	this.instance.setTransform(34.9,34.9,1,1,0,0,0,34.9,34.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FF0000").beginStroke().moveTo(-26.9,26.9).curveTo(-38,15.8,-38,0).curveTo(-38,-15.7,-26.9,-26.9).curveTo(-19,-34.8,-8.7,-37).curveTo(-4.6,-38,0,-38).curveTo(15.8,-38,26.9,-26.9).curveTo(38,-15.7,38,0).curveTo(38,15.8,26.9,26.9).curveTo(21,32.7,14,35.5).curveTo(7.5,38,0,38).curveTo(-15.7,38,-26.9,26.9).closePath();
	this.shape.setTransform(34.8,34.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regX:34.9,regY:34.9,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regX:34.8,regY:34.8,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.NoBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.No_Btn();
	this.instance.setTransform(99,31.7,1,1,0,0,0,99,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(-84,30.2).lineTo(-84,-30.2).lineTo(84,-30.2).lineTo(84,30.2).closePath();
	this.shape.setTransform(80,30.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1,x:99}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85,x:99.1}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.MotifGreg = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#BC9A72").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#D5AE81").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-1.6,27.3).curveTo(-2.5,20.5,-2.5,17.8).lineTo(-2.5,16.6).lineTo(-4,16).curveTo(-4.5,16.3,-5.5,16.3).curveTo(-7.3,16.3,-7.7,13.1).lineTo(-7.8,9.7).curveTo(-7.8,6.8,-7.4,5.1).lineTo(-12.4,3.8).lineTo(-15.6,2.8).curveTo(-20,1.4,-21.9,-0.2).curveTo(-24.2,-2.2,-24.2,-5.4).curveTo(-24.2,-7.9,-22.6,-11.7).curveTo(-23.6,-12.4,-24.1,-13.2).curveTo(-24.9,-14.4,-24.9,-19.1).curveTo(-24.9,-22.8,-24.3,-27.4).lineTo(-19.9,-27).curveTo(-20.4,-23.2,-20.4,-19.9).curveTo(-20.4,-17.4,-19.9,-16.5).curveTo(-17.7,-12.8,-11.8,-13.3).lineTo(16.2,-12.9).lineTo(16.2,-16.9).lineTo(16.2,-18.9).curveTo(18.8,-16.8,20.7,-14.2).lineTo(20.5,-10.8).lineTo(20.6,-10.6).curveTo(20.6,-10.3,20.4,-9.9).lineTo(20.4,-9.2).lineTo(20,-8.8).curveTo(19.5,-8.4,18.4,-8.4).lineTo(17.6,-8.4).lineTo(17.6,-8.3).curveTo(17.9,-6.3,17.6,-4.2).curveTo(17.6,-1.4,16.9,2.2).curveTo(15.7,8.2,13.2,9.6).lineTo(6.5,9.6).curveTo(2.1,7.8,-3.4,6.2).curveTo(-3.3,7.6,-3.3,9.6).lineTo(-3.3,11.8).lineTo(-2.9,11.8).curveTo(-1.7,11.8,14.2,15).lineTo(16.5,15.5).lineTo(24.9,17.1).curveTo(24.5,18.2,23.9,19.1).curveTo(22.9,20.8,21.4,22.1).lineTo(17.5,21.4).curveTo(13.4,20.7,9.2,19.7).curveTo(4.1,18.6,0.8,17.7).lineTo(1.2,19.3).curveTo(2.3,23.3,3.1,27.4).lineTo(2,27.4).lineTo(-1.6,27.3).closePath().moveTo(-19.7,-4.9).curveTo(-19.7,-4.3,-11.6,-1.4).lineTo(-6.6,0.3).curveTo(6.7,4.7,9.7,4.7).curveTo(12.9,4.7,13,-2).lineTo(12.7,-8.4).lineTo(-10.7,-8.4).lineTo(-12.9,-8.4).curveTo(-15.5,-8.9,-17.5,-9.4).lineTo(-19.7,-4.9).closePath();
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt03();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#D5AE81").beginStroke().moveTo(-7.4,3.2).lineTo(-8.9,-3.2).curveTo(-5.6,-4.3,-0.7,-7.4).lineTo(1.8,-9.1).curveTo(2.6,-7.7,5.1,-4.4).curveTo(8.9,0.5,8.9,5).curveTo(8.9,7.4,-0.1,8.2).curveTo(-4.1,8.5,-6.3,9.1).lineTo(-7.4,3.2).closePath();
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#497C59").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#5A996E").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_5.setTransform(85.2,58);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_6.setTransform(86,143.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_7.setTransform(67.5,124.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_8.setTransform(9.8,86.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_11.setTransform(25.6,90.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_12.setTransform(84.4,77.7);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.MotifFrank = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#7A4D30").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#975F3C").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-1.6,27.3).curveTo(-2.5,20.5,-2.5,17.8).lineTo(-2.5,16.6).lineTo(-4,16).curveTo(-4.5,16.3,-5.5,16.3).curveTo(-7.3,16.3,-7.7,13.1).lineTo(-7.8,9.7).curveTo(-7.8,6.8,-7.4,5.1).lineTo(-12.4,3.8).lineTo(-15.6,2.8).curveTo(-20,1.4,-21.9,-0.2).curveTo(-24.2,-2.2,-24.2,-5.4).curveTo(-24.2,-7.9,-22.6,-11.7).curveTo(-23.6,-12.4,-24.1,-13.2).curveTo(-24.9,-14.4,-24.9,-19.1).curveTo(-24.9,-22.8,-24.3,-27.4).lineTo(-19.9,-27).curveTo(-20.4,-23.2,-20.4,-19.9).curveTo(-20.4,-17.4,-19.9,-16.5).curveTo(-17.7,-12.8,-11.8,-13.3).lineTo(16.2,-12.9).lineTo(16.2,-16.9).lineTo(16.2,-18.9).curveTo(18.8,-16.8,20.7,-14.2).lineTo(20.5,-10.8).lineTo(20.6,-10.6).curveTo(20.6,-10.3,20.4,-9.9).lineTo(20.4,-9.2).lineTo(20,-8.8).curveTo(19.5,-8.4,18.4,-8.4).lineTo(17.6,-8.4).lineTo(17.6,-8.3).curveTo(17.9,-6.3,17.6,-4.2).curveTo(17.6,-1.4,16.9,2.2).curveTo(15.7,8.2,13.2,9.6).lineTo(6.5,9.6).curveTo(2.1,7.8,-3.4,6.2).curveTo(-3.3,7.6,-3.3,9.6).lineTo(-3.3,11.8).lineTo(-2.9,11.8).curveTo(-1.7,11.8,14.2,15).lineTo(16.5,15.5).lineTo(24.9,17.1).curveTo(24.5,18.2,23.9,19.1).curveTo(22.9,20.8,21.4,22.1).lineTo(17.5,21.4).curveTo(13.4,20.7,9.2,19.7).curveTo(4.1,18.6,0.8,17.7).lineTo(1.2,19.3).curveTo(2.3,23.3,3.1,27.4).lineTo(2,27.4).lineTo(-1.6,27.3).closePath().moveTo(-19.7,-4.9).curveTo(-19.7,-4.3,-11.6,-1.4).lineTo(-6.6,0.3).curveTo(6.7,4.7,9.7,4.7).curveTo(12.9,4.7,13,-2).lineTo(12.7,-8.4).lineTo(-10.7,-8.4).lineTo(-12.9,-8.4).curveTo(-15.5,-8.9,-17.5,-9.4).lineTo(-19.7,-4.9).closePath();
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt03();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#975F3C").beginStroke().moveTo(-7.4,3.2).lineTo(-8.9,-3.2).curveTo(-5.6,-4.3,-0.7,-7.4).lineTo(1.8,-9.1).curveTo(2.6,-7.7,5.1,-4.4).curveTo(8.9,0.5,8.9,5).curveTo(8.9,7.4,-0.1,8.2).curveTo(-4.1,8.5,-6.3,9.1).lineTo(-7.4,3.2).closePath();
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#497C59").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#5A996E").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_5.setTransform(85.2,58);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_6.setTransform(86,143.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_7.setTransform(67.5,124.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_8.setTransform(9.8,86.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_11.setTransform(25.6,90.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_12.setTransform(84.4,77.7);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.Motif = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape.setTransform(25.6,90.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_1.setTransform(9.8,86.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-1.6,27.3).curveTo(-2.5,20.5,-2.5,17.8).lineTo(-2.5,16.6).lineTo(-4,16).curveTo(-4.5,16.3,-5.5,16.3).curveTo(-7.3,16.3,-7.7,13.1).lineTo(-7.8,9.7).curveTo(-7.8,6.8,-7.4,5.1).lineTo(-12.4,3.8).lineTo(-15.6,2.8).curveTo(-20,1.4,-21.9,-0.2).curveTo(-24.2,-2.2,-24.2,-5.4).curveTo(-24.2,-7.9,-22.6,-11.7).curveTo(-23.6,-12.4,-24.1,-13.2).curveTo(-24.9,-14.4,-24.9,-19.1).curveTo(-24.9,-22.8,-24.3,-27.4).lineTo(-19.9,-27).curveTo(-20.4,-23.2,-20.4,-19.9).curveTo(-20.4,-17.4,-19.9,-16.5).curveTo(-17.7,-12.8,-11.8,-13.3).lineTo(16.2,-12.9).lineTo(16.2,-16.9).lineTo(16.2,-18.9).curveTo(18.8,-16.8,20.7,-14.2).lineTo(20.5,-10.8).lineTo(20.6,-10.6).curveTo(20.6,-10.3,20.4,-9.9).lineTo(20.4,-9.2).lineTo(20,-8.8).curveTo(19.5,-8.4,18.4,-8.4).lineTo(17.6,-8.4).lineTo(17.6,-8.3).curveTo(17.9,-6.3,17.6,-4.2).curveTo(17.6,-1.4,16.9,2.2).curveTo(15.7,8.2,13.2,9.6).lineTo(6.5,9.6).curveTo(2.1,7.8,-3.4,6.2).curveTo(-3.3,7.6,-3.3,9.6).lineTo(-3.3,11.8).lineTo(-2.9,11.8).curveTo(-1.7,11.8,14.2,15).lineTo(16.5,15.5).lineTo(24.9,17.1).curveTo(24.5,18.2,23.9,19.1).curveTo(22.9,20.8,21.4,22.1).lineTo(17.5,21.4).curveTo(13.4,20.7,9.2,19.7).curveTo(4.1,18.6,0.8,17.7).lineTo(1.2,19.3).curveTo(2.3,23.3,3.1,27.4).lineTo(2,27.4).lineTo(-1.6,27.3).closePath().moveTo(-19.7,-4.9).curveTo(-19.7,-4.3,-11.6,-1.4).lineTo(-6.6,0.3).curveTo(6.7,4.7,9.7,4.7).curveTo(12.9,4.7,13,-2).lineTo(12.7,-8.4).lineTo(-10.7,-8.4).lineTo(-12.9,-8.4).curveTo(-15.5,-8.9,-17.5,-9.4).lineTo(-19.7,-4.9).closePath();
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt03();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#497C59").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape_3.setTransform(18,46.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#5A996E").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_4.setTransform(85.2,58);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_5.setTransform(86,143.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_6.setTransform(67.5,124.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_7.setTransform(9.8,86.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_8.setTransform(86.9,128);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_9.setTransform(94.8,140.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-70.6,9.4).lineTo(-75.5,7.6).lineTo(-75.4,6.9).curveTo(-74.7,2.8,-74.6,0.7).lineTo(-51.3,0.7).lineTo(-51,7.1).curveTo(-51.1,13.8,-54.3,13.8).curveTo(-57.3,13.8,-70.6,9.4).closePath().moveTo(59.2,-1.5).lineTo(57.8,-7.9).curveTo(61,-9,66,-12.1).lineTo(68.5,-13.8).curveTo(69.3,-12.4,71.8,-9.1).curveTo(75.5,-4.2,75.5,0.3).curveTo(75.5,2.7,66.6,3.5).curveTo(62.5,3.8,60.3,4.4).lineTo(59.2,-1.5).closePath();
	this.shape_10.setTransform(88.9,83);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_11.setTransform(84.4,77.7);

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.LogOutRoundBtncopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		this.stop()
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1));

	// Layer 4
	this.text = new cjs.Text("Logout", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(35,-4.2,0.59,0.59);

	this.instance = new lib.ARR();
	this.instance.setTransform(9.6,5.4,0.348,0.348,0,0,0,22.7,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 255, 255, 0)];
	this.instance.cache(-10,-2,58,54);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{skewY:0}},{t:this.text,p:{text:"Logout",lineWidth:84}}]}).to({state:[{t:this.instance,p:{skewY:180}},{t:this.text,p:{text:"Login",lineWidth:67}}]},1).wait(1));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-9.6,-12.4).lineTo(9.6,-12.4).lineTo(9.6,12.4).lineTo(-9.6,12.4);
	this.shape.setTransform(15.2,4.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#4F2A3F").beginStroke().moveTo(-103.4,19.1).curveTo(-108.4,19.1,-108.4,14.1).lineTo(-108.4,-14.1).curveTo(-108.4,-19.1,-103.4,-19.1).lineTo(103.4,-19.1).curveTo(108.4,-19.1,108.4,-14.1).lineTo(108.4,14.1).curveTo(108.4,19.1,103.4,19.1).closePath();
	this.shape_1.setTransform(102.5,4);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.9,-15.1,216.8,38.2);


(lib.Letter_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{Level1:0,Level2:70,Level3:118,Level4:152,Level5:177,PickUpFinance:195,PickUpIT:216,PickUpHR:237,PickUpCommercial:258,PickUpMarketingComms:279,PickUpSupplyChain:300});

	// timeline functions:
	this.frame_0 = function() {
		this.visible=true;
	}
	this.frame_69 = function() {
		this.stop();
		gamePost.letterFinishedRoller(this);
	}
	this.frame_70 = function() {
		this.visible=true;
	}
	this.frame_117 = function() {
		this.stop();
		gamePost.letterFinishedRoller(this);
	}
	this.frame_118 = function() {
		this.visible=true;
	}
	this.frame_151 = function() {
		this.stop();
		gamePost.letterFinishedRoller(this);
	}
	this.frame_152 = function() {
		this.visible=true;
	}
	this.frame_176 = function() {
		this.stop();
		gamePost.letterFinishedRoller(this);
	}
	this.frame_177 = function() {
		this.visible=true;
	}
	this.frame_194 = function() {
		this.stop();
		gamePost.letterFinishedRoller(this);
	}
	this.frame_213 = function() {
		this.stop();
		gamePost.letterFinishPickup();
	}
	this.frame_234 = function() {
		this.stop();
		gamePost.letterFinishPickup();
	}
	this.frame_255 = function() {
		this.stop();
		gamePost.letterFinishPickup();
	}
	this.frame_276 = function() {
		this.stop();
		gamePost.letterFinishPickup();
	}
	this.frame_297 = function() {
		this.stop();
		gamePost.letterFinishPickup();
	}
	this.frame_318 = function() {
		this.stop();
		gamePost.letterFinishPickup();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(69).call(this.frame_69).wait(1).call(this.frame_70).wait(47).call(this.frame_117).wait(1).call(this.frame_118).wait(33).call(this.frame_151).wait(1).call(this.frame_152).wait(24).call(this.frame_176).wait(1).call(this.frame_177).wait(17).call(this.frame_194).wait(19).call(this.frame_213).wait(21).call(this.frame_234).wait(21).call(this.frame_255).wait(21).call(this.frame_276).wait(21).call(this.frame_297).wait(21).call(this.frame_318).wait(3));

	// Layer 5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_205 = new cjs.Graphics().moveTo(70.2,116).lineTo(56.9,108.2).curveTo(55.4,109.6,53.6,110.9).curveTo(40.4,120,16.1,120).curveTo(3.9,120,-8.3,117.2).curveTo(-21.9,114.2,-35.5,107.7).curveTo(-55,98.4,-68.1,85.9).curveTo(-81.3,73.1,-83.2,64.8).curveTo(-84,61.4,-83.8,55.8).lineTo(-83.4,44.1).curveTo(-83.4,33.7,-82.5,29.5).curveTo(-80,16.9,-79,9.5).curveTo(-78.1,3.3,-74.2,-2.9).curveTo(-76.3,-9.8,-77.3,-16.8).curveTo(-77.3,-28.1,-73.7,-36.5).curveTo(-73,-38.2,-62.5,-57.6).curveTo(-61.1,-60.2,-50.3,-67.3).curveTo(-39,-74.6,-37,-77.6).curveTo(-36.7,-78,-36.5,-79.2).lineTo(-36.5,-82.4).lineTo(-36,-82.2).lineTo(-36,-82.9).curveTo(-35.7,-85.4,-35.4,-86.6).lineTo(-36.5,-86.8).lineTo(-36.5,-101.6).curveTo(-34.7,-100.4,-31.4,-99.1).lineTo(-24.5,-96.6).curveTo(-16,-93.6,15,-84.1).curveTo(49.8,-73.5,56.6,-72.4).lineTo(83.8,-63.8).lineTo(83.4,-63.8).curveTo(82.4,-57.1,82.4,-44.9).lineTo(82.4,-44.4).lineTo(80.5,-45).curveTo(80.4,-42.8,79.8,-39.9).lineTo(82.4,-39).lineTo(82.4,-7.9).lineTo(82.7,7.9).lineTo(69.4,2.4).lineTo(69.4,8).lineTo(82.9,13.6).lineTo(83.1,26.4).lineTo(83.3,36).lineTo(69.4,29.4).lineTo(69.4,34.8).lineTo(77.5,38.7).lineTo(83.4,41.5).curveTo(83.6,59.7,83.2,63.9).lineTo(83.2,97.2).lineTo(68,88.6).curveTo(67.4,91.4,66.4,94).lineTo(83.2,103.4).lineTo(83.2,105.5).curveTo(82.8,109.1,82.8,123.4).lineTo(70.2,116).closePath().moveTo(-47.6,-123.2).lineTo(-47.3,-123.4).lineTo(-47,-123.3).lineTo(-47.5,-123.2).lineTo(-47.6,-123.2).closePath();
	var mask_graphics_225 = new cjs.Graphics().moveTo(-9.9,163.7).curveTo(-38.1,159.7,-55.5,143.7).curveTo(-61.8,138,-67.7,129.8).lineTo(-76.4,116.7).curveTo(-89,97.7,-90.9,88.8).curveTo(-91.7,84.8,-91.7,78.6).curveTo(-91.4,70.4,-91.4,64.6).curveTo(-91.4,44.3,-87.9,31.4).curveTo(-85,20.8,-73,-6).curveTo(-69,-15.1,-62.2,-24.1).curveTo(-55,-33.6,-46.1,-41.5).curveTo(-42,-45,-38.6,-50.3).lineTo(-33,-58.8).curveTo(-30.2,-62.5,-27.8,-64.7).lineTo(-27.9,-76.4).curveTo(-10.9,-68.4,23.3,-54.1).lineTo(67.2,-36.6).curveTo(80.9,-31.1,91.1,-26.8).lineTo(91.4,-14).lineTo(91.6,1.1).curveTo(91.8,19.4,91.5,23.5).lineTo(91.5,65.1).curveTo(91.1,68.7,91.1,83).curveTo(91.1,101.9,91.5,123.4).curveTo(91.5,124.9,90.7,129.9).curveTo(89.9,134.9,90.2,138.2).lineTo(90.3,139.1).lineTo(67.9,123.6).lineTo(67.6,124.5).curveTo(64.1,133.6,58.6,141.8).curveTo(43.6,163.7,24.5,163.7).closePath().moveTo(-39.3,-163.6).lineTo(-39,-163.8).lineTo(-38.8,-163.7).lineTo(-39.2,-163.6).lineTo(-39.3,-163.6).closePath();
	var mask_graphics_244 = new cjs.Graphics().moveTo(11.5,198.5).curveTo(-27.5,194.9,-40.4,181.4).curveTo(-44.6,177,-61.6,170.3).curveTo(-78.5,163.5,-95.8,158.8).curveTo(-113.1,154,-103.3,133.9).curveTo(-93.5,113.8,-89.7,95.2).curveTo(-86,76.7,-67.6,53.1).curveTo(-49.2,29.5,-34.4,9.5).curveTo(-22.7,-6.3,-12.6,-13.2).lineTo(-12.7,-16.5).lineTo(-12.9,-27.9).curveTo(-12.9,-34.4,-12.7,-39.8).curveTo(21.7,-19.6,65.7,5.1).lineTo(106.8,28.3).lineTo(106.8,30.3).curveTo(106.4,34,106.4,53.5).curveTo(106.5,67.2,106.8,88.7).curveTo(106.8,90.1,106,95.1).curveTo(105.2,100.1,105.6,103.4).lineTo(105.7,104.3).lineTo(98.7,99.5).curveTo(97.5,105.7,95.5,112.9).curveTo(90.4,131,84.2,141.5).curveTo(81,146.9,76.8,157).curveTo(72.6,167.3,69.8,171.9).curveTo(60,188.6,36.4,198.5).closePath().moveTo(-24,-198.4).lineTo(-23.7,-198.5).lineTo(-23.4,-198.4).lineTo(-23.9,-198.4).lineTo(-24,-198.4).closePath();
	var mask_graphics_267 = new cjs.Graphics().moveTo(120.4,140.4).curveTo(111.6,135,80.7,118.5).lineTo(57.8,106.2).curveTo(53.1,112.5,48.5,117.7).curveTo(28.4,140.4,14.1,136.9).lineTo(-16.7,136.9).curveTo(-30.6,137.7,-45.2,127.3).curveTo(-57.4,118.5,-69,102.8).curveTo(-78.7,89.7,-85.3,75.4).curveTo(-91.5,62.3,-91.4,56.8).curveTo(-91.5,29.8,-76.9,-1.2).curveTo(-64.9,-27.2,-45.1,-51.3).curveTo(-27.9,-72.2,-13.3,-82.2).curveTo(-9.6,-84.8,-6.8,-86).curveTo(-6.4,-91.7,-7.5,-92.8).lineTo(82.1,-64.2).curveTo(118.3,-52.6,125.4,-50.5).curveTo(140.2,-45.8,142.4,-45.8).lineTo(142,-40.7).curveTo(141.6,-34.9,141.6,-31).lineTo(141.6,-8.1).curveTo(141.6,-2.6,142.8,51.8).lineTo(143.2,73.2).lineTo(143.2,153.7).curveTo(126.6,144.2,120.4,140.4).closePath().moveTo(-143.2,-153.5).lineTo(-142.9,-153.7).lineTo(-142.6,-153.6).lineTo(-143.1,-153.5).lineTo(-143.2,-153.5).closePath();
	var mask_graphics_290 = new cjs.Graphics().moveTo(119.8,186.3).curveTo(96.6,171,81.1,162).lineTo(81,162).curveTo(71.9,165.4,64.1,168.6).lineTo(1.4,168.6).curveTo(-84.1,157.4,-116,119.6).curveTo(-128.4,104.9,-133.2,85.6).curveTo(-136.7,71.9,-136.7,52.6).curveTo(-136.7,37.5,-132.1,20.8).curveTo(-127,2.3,-118.5,-9.1).curveTo(-110.7,-19.4,-105.7,-23.9).curveTo(-97.3,-31.7,-81.7,-39.9).curveTo(-73.5,-44.3,-58.6,-48.4).curveTo(-44.5,-52.3,-29.9,-54.4).curveTo(-17,-56.3,-8.3,-56.2).lineTo(-8.5,-64.4).curveTo(1.1,-60.3,6.9,-57.4).curveTo(17.1,-52.5,52.7,-37.6).curveTo(89.9,-22,96.1,-19.1).curveTo(123.4,-6.3,142.1,2).lineTo(142.5,23.4).lineTo(142.5,154.4).curveTo(143.9,170.1,143.8,186.4).curveTo(143.7,202,143.8,203.4).curveTo(136.5,197.4,119.8,186.3).closePath().moveTo(-143.8,-203.3).lineTo(-143.6,-203.4).lineTo(-143.3,-203.4).lineTo(-143.7,-203.3).lineTo(-143.8,-203.3).closePath();
	var mask_graphics_310 = new cjs.Graphics().moveTo(-134.3,192.4).curveTo(-157.3,180,-169.6,162.6).curveTo(-182.3,144.6,-182.3,122.4).curveTo(-182.3,72.5,-147.7,45.3).curveTo(-121.3,24.6,-67.3,13).lineTo(30.3,13.8).lineTo(30.3,12.2).curveTo(50.3,23.4,68.6,33.4).curveTo(141.2,72.8,157.8,82.2).lineTo(181,96.8).lineTo(181,140.6).curveTo(182.3,156.3,182.3,172.6).curveTo(182.1,188.2,182.3,189.6).curveTo(176.2,184.5,163.5,176).curveTo(158.9,181.9,149.4,187.5).curveTo(144.5,190.5,136.3,194.4).curveTo(128.2,198.2,126.6,199.2).curveTo(117.7,204.6,83.5,210.1).curveTo(39.6,217.2,-12.9,217.2).curveTo(-88.1,217.2,-134.3,192.4).closePath().moveTo(-105.4,-217.1).lineTo(-105.1,-217.3).lineTo(-104.8,-217.2).lineTo(-105.3,-217.1).lineTo(-105.4,-217.1).closePath();

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(205).to({graphics:mask_graphics_205,x:623.8,y:-43.7}).wait(11).to({graphics:null,x:0,y:0}).wait(9).to({graphics:mask_graphics_225,x:615.6,y:-3.3}).wait(12).to({graphics:null,x:0,y:0}).wait(7).to({graphics:mask_graphics_244,x:600.2,y:31.4}).wait(14).to({graphics:null,x:0,y:0}).wait(9).to({graphics:mask_graphics_267,x:719.5,y:-13.4}).wait(12).to({graphics:null,x:0,y:0}).wait(11).to({graphics:mask_graphics_290,x:720.1,y:36.3}).wait(10).to({graphics:null,x:0,y:0}).wait(10).to({graphics:mask_graphics_310,x:681.7,y:50.2}).wait(11));

	// Layer 1
	this.mc_LetterInside = new lib.Letter();
	this.mc_LetterInside.setTransform(-103.1,33.3,1,0.91,0,24.8,16,125.7,39.6);

	this.mc_LetterInside.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.mc_LetterInside).to({regY:39.5,scaleX:1,scaleY:0.96,skewX:17.5,skewY:13,x:357.4,y:172.6},42).to({regY:39.6,scaleX:1,scaleY:0.98,skewX:2.6,skewY:0.5,x:469.6,y:172},15).to({scaleY:1,skewX:0,skewY:0,x:529.4,y:169.1},12,cjs.Ease.get(1)).to({scaleX:1,scaleY:0.91,skewX:24.8,skewY:16,x:-103.1,y:33.3},1).to({regY:39.5,scaleX:1,scaleY:0.96,skewX:17.5,skewY:13,x:357.4,y:172.6},32).to({regY:39.6,scaleX:1,scaleY:0.98,skewX:2.6,skewY:0.5,x:469.6,y:172},9).to({scaleY:1,skewX:0,skewY:0,x:529.4,y:169.1},6,cjs.Ease.get(1)).to({scaleX:1,scaleY:0.91,skewX:24.8,skewY:16,x:-103.1,y:33.3},1).to({regY:39.5,scaleX:1,scaleY:0.96,skewX:17.5,skewY:13,x:357.4,y:172.6},22).to({regY:39.6,scaleX:1,scaleY:0.98,skewX:2.6,skewY:0.5,x:469.6,y:172},6).to({scaleY:1,skewX:0,skewY:0,x:529.4,y:169.1},5,cjs.Ease.get(1)).to({scaleX:1,scaleY:0.91,skewX:24.8,skewY:16,x:-103.1,y:33.3},1).to({regY:39.5,scaleX:1,scaleY:0.96,skewX:17.5,skewY:13,x:357.4,y:172.6},17).to({regY:39.6,scaleX:1,scaleY:0.98,skewX:2.6,skewY:0.5,x:469.6,y:172},4).to({scaleY:1,skewX:0,skewY:0,x:529.4,y:169.1},3,cjs.Ease.get(1)).to({scaleX:1,scaleY:0.91,skewX:24.8,skewY:16,x:-103.1,y:33.3},1).to({regY:39.5,scaleX:1,scaleY:0.96,skewX:17.5,skewY:13,x:357.4,y:172.6},12).to({regY:39.6,scaleX:1,scaleY:0.98,skewX:2.6,skewY:0.5,x:469.6,y:172},3).to({scaleY:1,skewX:0,skewY:0,x:529.4,y:169.1},2,cjs.Ease.get(1)).to({regY:39.5,skewX:0.1,skewY:-0.2,x:530.8,y:169},1).to({regY:39.6,scaleX:0.96,scaleY:1.12,skewX:-4.7,skewY:-20.5,x:628,y:139.3},4).to({regX:125.5,scaleX:0.95,scaleY:1.15,skewX:-10.3,skewY:-30.1,x:646.8,y:115.1},1).to({scaleX:0.94,scaleY:1.18,skewX:-23,skewY:-46.8,x:647.3,y:85.8},1).to({regX:125.6,regY:39.5,scaleX:0.93,scaleY:1.21,skewX:-32,skewY:-59.5,x:648.6,y:63.4},1).to({regX:125.8,regY:39.6,scaleX:0.91,scaleY:1.29,skewX:-26.4,skewY:-65.5,x:634.3,y:-55.6},3).to({regX:125.7,scaleX:0.91,scaleY:1.14,skewX:-26.3,skewY:-42.4,x:692.2,y:-142.7},3).to({scaleY:1.04,skewY:-26.8,x:732.6,y:-154},2).to({regX:125.9,scaleX:0.91,scaleY:0.9,skewX:-26.2,skewY:-3.8,x:823.8,y:-132.7},3).wait(3).to({regX:125.7,regY:39.5,scaleX:1,scaleY:1,skewX:0.1,skewY:-0.2,x:530.8,y:169},0).to({regX:125.8,scaleX:0.98,scaleY:1.06,skewX:-7.8,skewY:-16,x:576.7,y:158.9},2).to({regX:125.7,scaleX:0.97,scaleY:1.09,skewX:-12,skewY:-24.1,x:599.5,y:146.2},1).to({scaleX:0.96,scaleY:1.12,skewX:-16,skewY:-32.1,x:622.5,y:129},1).to({regX:125.6,scaleX:0.95,scaleY:1.15,skewX:-20.3,skewY:-40,x:645.4,y:99.9},1).to({scaleX:0.94,scaleY:1.18,skewX:-21.3,skewY:-45.1,x:643.2,y:89.2},1).to({scaleX:0.93,scaleY:1.2,skewX:-27.3,skewY:-55.1,x:647.7,y:70.4},1).to({regX:125.7,scaleX:0.93,scaleY:1.23,skewX:-32,skewY:-63.6,x:638.7,y:55.8},1).to({regX:125.8,regY:39.6,scaleX:0.91,scaleY:1.29,skewX:-26.4,skewY:-65.5,x:634.3,y:-13.1},2).to({regX:125.7,scaleX:0.91,scaleY:1.14,skewX:-26.3,skewY:-42.4,x:692.2,y:-66.2},3).to({scaleY:1.04,skewY:-26.8,x:732.6,y:-69},2).to({regX:125.9,scaleX:0.91,scaleY:0.9,skewX:-26.2,skewY:-3.8,x:823.8,y:-39.2},3).wait(3).to({regX:125.7,regY:39.5,scaleX:1,scaleY:1,skewX:0.1,skewY:-0.2,x:530.8,y:169},0).to({scaleX:0.96,scaleY:1.12,skewX:-11.8,skewY:-27.8,x:596.7,y:163.4},4).to({regX:125.6,scaleX:0.93,scaleY:1.21,skewX:-21,skewY:-48.6,x:620.5,y:119.9},3).to({regX:125.8,regY:39.6,scaleX:0.91,scaleY:1.29,skewX:-26.4,skewY:-65.5,x:634.3,y:88.9},3).to({regX:125.7,scaleX:0.91,scaleY:1.14,skewX:-26.3,skewY:-42.4,x:685.4,y:35},3).to({scaleY:1.04,skewY:-26.8,x:732.6,y:16.8},2).to({regX:125.9,scaleX:0.91,scaleY:0.9,skewX:-26.2,skewY:-3.8,x:823.8,y:45},3).wait(3).to({regX:125.7,regY:39.5,scaleX:1,scaleY:1,skewX:0.1,skewY:-0.2,x:530.8,y:169},0).to({scaleX:0.96,scaleY:1.12,skewX:3.8,skewY:-12,x:610.5,y:153.2},4).to({regX:125.6,scaleX:0.95,scaleY:1.15,skewX:4.7,skewY:-15.1,x:630.3,y:142.8},1).to({regX:125.5,scaleX:0.94,scaleY:1.18,skewX:-8,skewY:-31.8,x:647.3,y:113.6},1).to({regX:125.6,scaleX:0.93,scaleY:1.21,skewX:-21,skewY:-48.6,x:664.4,y:63.5},1).to({regX:125.8,regY:39.6,scaleX:0.91,scaleY:1.29,skewX:-26.4,skewY:-65.5,x:778.8,y:-55.6},3).to({regX:125.7,scaleX:0.91,scaleY:1.14,skewX:-26.3,skewY:-42.4,x:840.1,y:-117.2},3).to({scaleY:1.04,skewY:-26.8,x:886.4,y:-137},2).to({regX:125.9,scaleX:0.91,scaleY:0.9,skewX:-26.2,skewY:-3.8,x:949.6,y:-98.7},3).wait(3).to({regX:125.7,regY:39.5,scaleX:1,scaleY:1,skewX:0.1,skewY:-0.2,x:530.8,y:169},0).to({scaleX:0.96,scaleY:1.12,skewX:5.2,skewY:-10.3,x:574,y:168.2},4).to({regY:39.4,scaleX:0.94,scaleY:1.21,skewX:0.6,skewY:-26.8,x:646,y:125.6},3).to({regX:125.8,regY:39.6,scaleX:0.91,scaleY:1.29,skewX:7.3,skewY:-31.8,x:700.1,y:75.6},3).to({regX:125.7,scaleX:0.91,scaleY:1.04,skewX:-26.3,skewY:-26.8,x:870.5,y:-20.5},4).to({regX:125.9,scaleX:0.91,scaleY:0.9,skewX:-26.2,skewY:-3.8,x:953.2,y:0.9},4).wait(3).to({regX:125.7,regY:39.5,scaleX:1,scaleY:1,skewX:0.1,skewY:-0.2,x:530.8,y:169},0).to({scaleX:0.96,scaleY:1.12,skewX:5.2,skewY:-10.3,x:566.3,y:168.2},4).to({regY:39.6,scaleX:0.94,scaleY:1.21,skewX:9.3,skewY:-18,x:618.2,y:149.3},3).to({regX:125.8,scaleX:0.91,scaleY:1.29,skewX:13.5,skewY:-25.6,x:670.1,y:127.7},3).to({regX:125.7,regY:39.8,scaleX:0.91,scaleY:1.04,skewX:-15,skewY:-15.5,x:870.6,y:62},4).to({regX:125.9,regY:39.6,scaleX:0.91,scaleY:0.9,skewX:-26.2,skewY:-3.8,x:953.2,y:111.4},4).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-260.7,-40.3,315.3,147.1);


(lib.LeaderboardButton = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Lead();
	this.instance.setTransform(89,19.5,1,1,0,0,0,89,19.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FF0000").beginStroke().moveTo(-89,19.5).lineTo(-89,-19.5).lineTo(89,-19.5).lineTo(89,19.5).closePath();
	this.shape.setTransform(89,19.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regY:19.5,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regY:19.4,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,178.1,40.8);


(lib.HS_Bubble = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 1
	this.text = new cjs.Text("lorem ipsum dolor sit amet, consecte tur adip iscing elit Mauris id Lorem ipsum dolor ", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 395;
	this.text.setTransform(135,-97.1,1.18,1.18);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer 4
	this.stretch_mc = new lib.hns_stretch_bubble_mc();
	this.stretch_mc.setTransform(139.8,-10,1,1,0,0,0,258.9,88);

	this.head_mc = new lib.hns_head_bubble_mc();
	this.head_mc.setTransform(139.8,-111.4,1,1,0,0,0,258.9,13.4);

	this.base_mc = new lib.hns_base_bubble_ms();
	this.base_mc.setTransform(156.1,123.2,1,1,0,0,0,275.2,45.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.base_mc},{t:this.head_mc},{t:this.stretch_mc}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-98.1,-97.1,471.2,184);


(lib.Highlight = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.HighlightCircle("synched",0);
	this.instance.setTransform(24.8,24.8,0.839,0.839,0,0,0,24.8,24.8);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({_off:false},0).to({scaleX:0.98,scaleY:0.98,alpha:1},2).to({scaleX:1.63,scaleY:1.63},9).to({scaleX:1.85,scaleY:1.85,alpha:0},3).to({_off:true},1).wait(1));

	// Layer 2
	this.instance_1 = new lib.HighlightCircle("synched",0);
	this.instance_1.setTransform(24.8,24.8,0.839,0.839,0,0,0,24.8,24.8);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:0.98,scaleY:0.98,alpha:1},2).to({scaleX:1.63,scaleY:1.63},9).to({scaleX:1.85,scaleY:1.85,alpha:0},3).to({_off:true},1).wait(5));

	// Layer 1
	this.instance_2 = new lib.HighlightCircle("synched",0);
	this.instance_2.setTransform(24.8,24.8,0.839,0.839,0,0,0,24.8,24.8);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX:0.98,scaleY:0.98,alpha:1},2).to({scaleX:1.63,scaleY:1.63},9).to({scaleX:1.85,scaleY:1.85,alpha:0},3).to({_off:true},1).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(2.8,2.8,44.1,44.1);


(lib.Health_and_Safety_Advisor = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{Default:0,StartTalking:8,TalkLoop:13,StopTalking:30});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_45 = function() {
		gamePost.healthAndSafetyDoneTalking();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(45).call(this.frame_45).wait(1));

	// Layer 4
	this.instance = new lib.LadyBoss("single",0);
	this.instance.setTransform(68.7,98.7,0.455,0.455,0,0,0,216.5,210.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({mode:"synched",loop:false},0).wait(22).to({startPosition:21},0).wait(16));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(3,1,1).moveTo(98.2,-0).curveTo(98.2,40.6,69.5,69.4).curveTo(40.6,98.2,0,98.2).curveTo(-40.6,98.2,-69.4,69.4).curveTo(-98.2,40.6,-98.2,-0).curveTo(-98.2,-40.7,-69.4,-69.4).curveTo(-40.6,-98.2,0,-98.2).curveTo(40.6,-98.2,69.5,-69.4).curveTo(98.2,-40.7,98.2,-0).closePath();
	this.shape.setTransform(90.1,90.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-63.7,63.6).curveTo(-90.1,37.3,-90.1,-0).curveTo(-90.1,-37.4,-63.7,-63.7).curveTo(-37.3,-90.1,0,-90.1).curveTo(37.3,-90.1,63.7,-63.7).curveTo(90.1,-37.4,90.1,-0).curveTo(90.1,37.3,63.7,63.6).curveTo(37.3,90.1,0,90.1).curveTo(-37.3,90.1,-63.7,63.6).closePath();
	this.shape_1.setTransform(90.1,90.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#CCB55E").beginStroke().moveTo(-69.4,69.4).curveTo(-98.2,40.6,-98.2,-0).curveTo(-98.2,-40.7,-69.4,-69.4).curveTo(-40.7,-98.2,0,-98.2).curveTo(40.7,-98.2,69.4,-69.4).curveTo(98.2,-40.7,98.2,-0).curveTo(98.2,40.6,69.4,69.4).curveTo(40.7,98.2,0,98.2).curveTo(-40.7,98.2,-69.4,69.4).closePath().moveTo(-63.7,-63.7).curveTo(-90.1,-37.4,-90.1,-0).curveTo(-90.1,37.3,-63.7,63.6).curveTo(-37.3,90.1,0,90.1).curveTo(37.3,90.1,63.7,63.6).curveTo(90.1,37.3,90.1,-0).curveTo(90.1,-37.4,63.7,-63.7).curveTo(37.3,-90.1,0,-90.1).curveTo(-37.3,-90.1,-63.7,-63.7).closePath();
	this.shape_2.setTransform(90.1,90.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(46));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.6,-9.6,199.4,199.4);


(lib.GregHoodie = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#D5AE81").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape.setTransform(25.6,90.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#BC9A72").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_1.setTransform(9.8,86.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-1.6,27.3).curveTo(-2.5,20.5,-2.5,17.8).lineTo(-2.5,16.6).lineTo(-4,16).curveTo(-4.5,16.3,-5.5,16.3).curveTo(-7.3,16.3,-7.7,13.1).lineTo(-7.8,9.7).curveTo(-7.8,6.8,-7.4,5.1).lineTo(-12.4,3.8).lineTo(-15.6,2.8).curveTo(-20,1.4,-21.9,-0.2).curveTo(-24.2,-2.2,-24.2,-5.4).curveTo(-24.2,-7.9,-22.6,-11.7).curveTo(-23.6,-12.4,-24.1,-13.2).curveTo(-24.9,-14.4,-24.9,-19.1).curveTo(-24.9,-22.8,-24.3,-27.4).lineTo(-19.9,-27).curveTo(-20.4,-23.2,-20.4,-19.9).curveTo(-20.4,-17.4,-19.9,-16.5).curveTo(-17.7,-12.8,-11.8,-13.3).lineTo(16.2,-12.9).lineTo(16.2,-16.9).lineTo(16.2,-18.9).curveTo(18.8,-16.8,20.7,-14.2).lineTo(20.5,-10.8).lineTo(20.6,-10.6).curveTo(20.6,-10.3,20.4,-9.9).lineTo(20.4,-9.2).lineTo(20,-8.8).curveTo(19.5,-8.4,18.4,-8.4).lineTo(17.6,-8.4).lineTo(17.6,-8.3).curveTo(17.9,-6.3,17.6,-4.2).curveTo(17.6,-1.4,16.9,2.2).curveTo(15.7,8.2,13.2,9.6).lineTo(6.5,9.6).curveTo(2.1,7.8,-3.4,6.2).curveTo(-3.3,7.6,-3.3,9.6).lineTo(-3.3,11.8).lineTo(-2.9,11.8).curveTo(-1.7,11.8,14.2,15).lineTo(16.5,15.5).lineTo(24.9,17.1).curveTo(24.5,18.2,23.9,19.1).curveTo(22.9,20.8,21.4,22.1).lineTo(17.5,21.4).curveTo(13.4,20.7,9.2,19.7).curveTo(4.1,18.6,0.8,17.7).lineTo(1.2,19.3).curveTo(2.3,23.3,3.1,27.4).lineTo(2,27.4).lineTo(-1.6,27.3).closePath().moveTo(-19.7,-4.9).curveTo(-19.7,-4.3,-11.6,-1.4).lineTo(-6.6,0.3).curveTo(6.7,4.7,9.7,4.7).curveTo(12.9,4.7,13,-2).lineTo(12.7,-8.4).lineTo(-10.7,-8.4).lineTo(-12.9,-8.4).curveTo(-15.5,-8.9,-17.5,-9.4).lineTo(-19.7,-4.9).closePath();
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt04Greg();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#D5AE81").beginStroke().moveTo(-7.4,3.2).lineTo(-8.9,-3.2).curveTo(-5.6,-4.3,-0.7,-7.4).lineTo(1.8,-9.1).curveTo(2.6,-7.7,5.1,-4.4).curveTo(8.9,0.5,8.9,5).curveTo(8.9,7.4,-0.1,8.2).curveTo(-4.1,8.5,-6.3,9.1).lineTo(-7.4,3.2).closePath();
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#497C59").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#5A996E").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_5.setTransform(85.2,58);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_6.setTransform(86,143.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_7.setTransform(67.5,124.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_8.setTransform(9.8,86.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_11.setTransform(25.6,90.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_12.setTransform(84.4,77.7);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-3.8,-6.1,172.7,161.6);


(lib.GotItButton = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.GotIt();
	this.instance.setTransform(99,31.7,1,1,0,0,0,99,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#000000").beginStroke().moveTo(-100.1,32.7).lineTo(-100.1,-32.7).lineTo(100.1,-32.7).lineTo(100.1,32.7).closePath();
	this.shape.setTransform(100.1,32.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1,x:99}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85,x:99.1}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,202.2,67.5);


(lib.Gliss = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_34 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(34).call(this.frame_34).wait(1));

	// Layer 1
	this.instance = new lib.glissTurn();
	this.instance.setTransform(38.3,43.8,0.225,0.225,-60,0,0,38.4,43.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:38.1,regY:43.8,scaleX:1,scaleY:1,rotation:360,x:38.1},4).to({rotation:720},25).to({regX:38.2,scaleX:0.17,scaleY:0.17,rotation:756.7,x:38.2,y:43.7},4).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(25.5,30.1,26.4,26.1);


(lib.GirlOutfit04 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.TP_logoWhite();
	this.instance.setTransform(84.8,46.6,0.046,0.046,-3.5,0,0,292.4,34);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#282828").beginStroke().moveTo(14.4,34.2).curveTo(4.9,32.7,-3.7,31.8).lineTo(-7.3,31.3).curveTo(-10.4,30.9,-13.2,30.1).lineTo(-18.6,28.9).curveTo(-18,28,-17.4,26.2).curveTo(-16,22.3,-15.2,16.5).lineTo(-13.8,8.3).curveTo(-13.3,5.6,-13.3,2.6).lineTo(-13.3,1.6).lineTo(-13.3,1.4).lineTo(-13.4,0.5).lineTo(-13.4,-0.2).curveTo(-12.8,-0.4,-12.1,-0.5).curveTo(-11.6,-0.6,-10.7,0.7).curveTo(-9.9,1.8,-9.5,2.9).curveTo(-9.2,3.6,-10.2,8.3).curveTo(-10.5,13.7,-6.6,18.1).curveTo(-2.7,22.4,2.5,24.7).curveTo(5.5,25.9,11.1,27.1).curveTo(16,28,17.9,29.1).curveTo(20.8,30.9,21.6,34.8).curveTo(16.6,34.5,14.4,34.2).closePath().moveTo(-29.3,-6.8).lineTo(-35,-8.8).curveTo(-37.5,-9.7,-37.7,-10.6).curveTo(-38,-13.3,-34.2,-17.7).curveTo(-31.1,-21.3,-27.5,-23.8).curveTo(-23,-26.9,-15.6,-29.4).curveTo(-17.7,-27.9,-21.2,-23.1).curveTo(-25,-18,-27.1,-13.4).curveTo(-27.7,-12,-28.2,-8.7).curveTo(-28.5,-6.8,-29.2,-6.8).lineTo(-29.3,-6.8).closePath().moveTo(32.3,-17).curveTo(31.2,-19.7,29.8,-21.2).lineTo(29.1,-21.8).lineTo(33.9,-21.8).lineTo(34.7,-22.2).lineTo(37.7,-15.8).curveTo(36,-13.5,34.6,-12.1).curveTo(33.8,-13.4,32.3,-17).closePath().moveTo(7.5,-14.2).curveTo(6.3,-14.6,5,-16).lineTo(2.2,-18.7).curveTo(-0,-20.9,-1.3,-23).curveTo(-2.7,-25.2,-3.6,-26.8).lineTo(-5.1,-29.9).lineTo(-5.8,-32.2).curveTo(-6.2,-33.4,-6.9,-33.6).lineTo(-6.9,-33.6).lineTo(-3.4,-34.8).curveTo(-1.3,-31.5,2.8,-27.3).curveTo(3.7,-26.4,6.7,-22.3).curveTo(9.6,-18.4,12.8,-15.2).curveTo(11,-13.9,10,-13.9).curveTo(8.3,-13.9,7.5,-14.2).closePath();
	this.shape.setTransform(41.3,44.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#494949").beginStroke().moveTo(4.7,21).lineTo(-1.6,19).curveTo(-2.2,16.8,-4.3,13.6).curveTo(-6.3,10.6,-7,7.3).curveTo(-9.5,-3.2,-10.1,-6.5).curveTo(-10.9,-11.2,-10.9,-16).curveTo(-10.9,-17.2,-10.6,-19.5).curveTo(-10.4,-21.5,-10.4,-22.4).lineTo(-6.4,-20.4).curveTo(-3.8,-19.4,0.8,-18.8).lineTo(2,-18.6).curveTo(2.1,-13.2,2.9,-8.4).lineTo(5.1,3.4).curveTo(6.1,8.6,7.8,13.5).lineTo(10.9,22.4).curveTo(7.8,21.9,4.7,21).closePath();
	this.shape_1.setTransform(32,99.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#666666").beginStroke().moveTo(-16.2,20.9).curveTo(-19.6,20.6,-25.1,19.7).lineTo(-28.2,10.8).curveTo(-29.8,5.9,-30.8,0.7).lineTo(-33.1,-11.1).curveTo(-33.9,-15.9,-34,-21.3).lineTo(-25.9,-20.2).curveTo(-19.6,-19.5,-12.2,-18.8).curveTo(-3.4,-18,9.8,-18.5).curveTo(23.9,-19.1,32.8,-20.8).lineTo(33.4,-20.9).curveTo(33.3,-20.3,33.8,-15.9).curveTo(34.3,-11,33.5,-8).curveTo(30.3,4.3,27.1,12.6).lineTo(24.7,18.8).lineTo(24.5,18.8).curveTo(22.6,19.3,10.4,20.3).curveTo(-2.5,21.3,-7.6,21.3).curveTo(-12.1,21.3,-16.2,20.9).closePath();
	this.shape_2.setTransform(68,102.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#333333").beginStroke().moveTo(11.1,35.3).curveTo(11.5,33,11.5,30.3).lineTo(11.5,19.9).curveTo(11.7,2.7,10.7,-5.5).curveTo(10.2,-9.9,8.8,-15.4).lineTo(10.2,-13.6).lineTo(13.6,-8.6).curveTo(14.7,-6.9,16,-6.9).curveTo(21.1,-6.9,26.1,-16.7).curveTo(30.3,-24.9,30.3,-28.6).lineTo(30.3,-30.3).curveTo(31.9,-29.7,36.3,-26.3).curveTo(41.2,-22.5,43.2,-19.7).curveTo(46,-15.5,47.6,-8.1).curveTo(47.7,-7.6,45.7,-6.7).lineTo(43,-5.4).curveTo(39.8,-11.7,36,-14.4).curveTo(32.1,-17.1,32.1,-12.3).curveTo(32.1,-11.8,36.3,-6).curveTo(40.6,-0.1,41.1,6.2).curveTo(41.7,12,38.8,16.3).curveTo(35.9,19.5,35.9,19.8).curveTo(35.9,20.9,36.1,21.1).lineTo(36.3,21.5).lineTo(36.7,21.7).curveTo(37.3,21.8,38.5,21.8).curveTo(39.1,21.8,39.9,21.4).lineTo(41.2,20.8).curveTo(41.3,22.7,41.9,26.6).curveTo(42.7,30.8,43.2,31.7).lineTo(40.3,32.5).curveTo(38.1,33.2,35.8,33.7).curveTo(28.5,35.3,13.7,35.3).lineTo(11.1,35.3).closePath().moveTo(5.1,35.1).lineTo(3.4,35).curveTo(2.6,31,-0.4,29.2).curveTo(-2.2,28.1,-7.2,27.2).curveTo(-12.8,26,-15.8,24.8).curveTo(-20.9,22.6,-24.9,18.2).curveTo(-28.7,13.8,-28.4,8.4).curveTo(-27.4,3.7,-27.7,3).curveTo(-28.2,1.9,-28.9,0.8).curveTo(-29.8,-0.4,-30.3,-0.4).curveTo(-31,-0.3,-31.7,-0).curveTo(-32,-1.9,-32.7,-2.4).curveTo(-33.7,-3,-35.2,-1).lineTo(-36.2,0.5).lineTo(-37.7,0.7).lineTo(-37.8,0.5).lineTo(-41.9,-3.6).curveTo(-43,-4.7,-44.1,-5.2).lineTo(-47.6,-6.7).curveTo(-46.8,-6.5,-46.5,-8.6).curveTo(-46,-11.8,-45.3,-13.3).curveTo(-43.3,-17.9,-39.4,-23).curveTo(-36,-27.7,-33.9,-29.3).curveTo(-32.8,-29.7,-31.1,-30.9).curveTo(-29.4,-32,-27.1,-32.8).lineTo(-27.1,-31.7).curveTo(-27.1,-30.8,-25.9,-27.5).curveTo(-24.3,-23.4,-22.2,-19.8).curveTo(-16,-9.2,-8.3,-9.2).curveTo(-5.2,-9.2,0.8,-14.4).curveTo(2.3,-15.7,2.7,-17.4).curveTo(3.3,-19.4,3.4,-19.6).curveTo(5,-13.4,5.9,-7.3).curveTo(6.1,-6,7.1,3.6).lineTo(7.1,35.2).lineTo(5.1,35.1).closePath().moveTo(-11.5,-22.2).curveTo(-14.5,-26.3,-15.4,-27.2).curveTo(-19.6,-31.3,-21.6,-34.7).lineTo(-20.2,-35.3).curveTo(-15,-29.2,-13.6,-28).curveTo(-10.2,-25.3,-8.3,-24.3).curveTo(-5.2,-22.6,-0.9,-21.7).lineTo(1.9,-21.7).lineTo(-1.3,-18.6).curveTo(-3.7,-16.3,-5.4,-15.1).curveTo(-8.6,-18.2,-11.5,-22.2).closePath().moveTo(16.5,-22.1).curveTo(18.1,-23.1,21.9,-26.7).curveTo(25,-29.6,26.1,-31.2).curveTo(26.3,-31.3,27,-31.2).curveTo(25.2,-28.3,23.9,-24.2).curveTo(23,-21.4,20.8,-17.8).lineTo(19.4,-15.7).closePath();
	this.shape_3.setTransform(59.5,44.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#070707").beginStroke().moveTo(-16.9,4).curveTo(-22.2,3.6,-22.8,3.3).lineTo(-22,0.9).curveTo(-21.4,-0.6,-19.9,-1.5).lineTo(-18.6,-2).lineTo(-16.3,-2.7).lineTo(-16.3,-1.9).curveTo(-16.3,1.2,-6.5,3.7).lineTo(-3.9,4.4).curveTo(-6.7,4.5,-10.2,4.5).curveTo(-12,4.5,-16.9,4).closePath().moveTo(13.2,1.4).curveTo(13.2,0,12.8,-1.2).curveTo(13.4,-1.2,13.9,-1.9).curveTo(14.9,-3.1,15.2,-3.3).curveTo(16.2,-4,17.7,-4.5).curveTo(18.7,-0,20,1.5).curveTo(21,2.5,22.8,3.4).lineTo(15.6,3.9).curveTo(13.8,3.9,13,4.2).curveTo(13.2,3.1,13.2,1.4).closePath();
	this.shape_4.setTransform(59,148);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#262626").beginStroke().moveTo(-19,4.2).curveTo(-28.8,1.7,-28.8,-1.4).lineTo(-28.8,-2.2).curveTo(-22.9,-3.4,-8.9,-3.3).curveTo(-6.8,-3.1,-5.3,-1.6).curveTo(-3.7,-0.1,-3.8,1.6).curveTo(-4.1,2.3,-5.7,3).curveTo(-9.1,4.4,-16.4,4.9).closePath().moveTo(7.6,2).curveTo(6.3,0.5,5.2,-4).curveTo(8.1,-4.8,13.1,-4.9).curveTo(20.3,-4.9,24.4,-3).curveTo(27.9,-1.4,28.8,1.3).curveTo(29.1,2,25.8,2.4).curveTo(19,3.2,10.4,3.9).curveTo(8.5,3,7.6,2).closePath();
	this.shape_5.setTransform(71.5,147.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#000000").beginStroke().moveTo(-16.1,75.4).curveTo(-22.2,74.3,-23,73.3).curveTo(-23.7,72.2,-23.7,69.9).curveTo(-23.7,64.8,-18.5,61.2).curveTo(-11.3,57.7,-7.9,57.1).curveTo(-6.3,56.8,-4.5,56.6).curveTo(-0.8,56.3,2.9,56.7).curveTo(7.2,57.1,13.8,60.4).lineTo(13.9,60.4).curveTo(15.2,58.4,19.2,57.1).curveTo(20.7,56.6,22.4,56.3).curveTo(26.9,55.4,32.5,55.8).curveTo(36.3,56,39.7,57).curveTo(44.8,58.9,45.7,59.4).curveTo(52,63,51.6,68.3).curveTo(51.5,69.8,51.1,70.3).curveTo(50.6,71.1,48.7,71.5).curveTo(47.3,71.8,31.3,73.8).curveTo(24.7,74.6,19.2,73.8).curveTo(17,73.4,16.9,72.7).curveTo(16.6,72,16.5,71.1).lineTo(16.2,71.8).curveTo(14.1,74.3,4.9,75.4).curveTo(-0.8,75.7,-6.1,75.7).curveTo(-11.3,75.7,-16.1,75.4).closePath().moveTo(-12.5,63.3).lineTo(-14.8,63.9).lineTo(-16.1,64.5).curveTo(-17.6,65.3,-18.2,66.8).lineTo(-19,69.2).curveTo(-18.4,69.5,-13.1,70).curveTo(-8.2,70.5,-6.4,70.4).curveTo(-2.9,70.4,-0.1,70.3).curveTo(7.2,69.8,10.6,68.4).curveTo(12.2,67.8,12.5,67.1).curveTo(12.5,65.3,11,63.8).curveTo(9.4,62.4,7.4,62.1).lineTo(6.3,62.1).curveTo(-6.9,62.1,-12.5,63.3).closePath().moveTo(21.5,61.4).curveTo(20,61.9,19,62.7).curveTo(18.7,62.8,17.7,64).curveTo(17.2,64.8,16.6,64.7).curveTo(17,66,17,67.3).curveTo(17,69,16.8,70.1).curveTo(17.6,69.9,19.4,69.9).lineTo(26.6,69.3).curveTo(35.3,68.7,42.1,67.9).curveTo(45.4,67.4,45.1,66.7).curveTo(44.2,64.1,40.7,62.5).curveTo(36.5,60.6,29.3,60.6).curveTo(24.4,60.6,21.5,61.4).closePath().moveTo(-10.7,45.3).lineTo(-12.8,45).curveTo(-17.9,44.2,-23.4,43).lineTo(-23.4,43).lineTo(-23.5,43).curveTo(-26,42.4,-26.1,41.9).lineTo(-25.8,41.4).curveTo(-26.7,41.1,-28.1,39.5).curveTo(-29.5,38.1,-29.6,37.4).curveTo(-31.7,34.2,-32.9,31.7).curveTo(-35.1,27.4,-36.1,20.6).curveTo(-37.3,13.6,-38,7.2).lineTo(-38.5,2.3).curveTo(-38.5,-2.4,-37.1,-8.2).lineTo(-37.1,-8.3).lineTo(-37,-8.3).curveTo(-36.8,-9.7,-36.2,-10.3).lineTo(-36.3,-11.2).curveTo(-35.5,-16.4,-35.7,-16).curveTo(-34.9,-20.3,-34,-26.4).curveTo(-34.4,-33.4,-34.1,-34).lineTo(-35.3,-34.9).curveTo(-39.9,-38.4,-42.6,-39.4).lineTo(-44.5,-40.1).curveTo(-45.9,-40.7,-47,-40.8).curveTo(-47.5,-40.9,-48.4,-41.3).lineTo(-49.1,-41.5).curveTo(-50.5,-42.8,-53.2,-43.6).lineTo(-53.2,-44.1).curveTo(-54.2,-44.4,-54.7,-45.1).curveTo(-55.1,-45.8,-55.2,-47.1).lineTo(-55.1,-48.5).lineTo(-55.1,-48.6).curveTo(-54.3,-57.8,-40.7,-65.7).curveTo(-33.5,-69.9,-30.3,-71.4).curveTo(-24.8,-73.9,-18.2,-75.1).lineTo(-14.8,-75.7).curveTo(-14.7,-75.7,-14.5,-75.1).lineTo(-14.4,-75).lineTo(-14.3,-74.8).curveTo(-11.8,-73.9,-7.2,-70.4).lineTo(-1.4,-66).curveTo(1,-64.3,3.3,-64).lineTo(18.8,-64).curveTo(21.2,-65.4,23.8,-69.4).curveTo(25.6,-72.2,27.9,-72.5).curveTo(28.9,-73.8,32,-73).curveTo(32.8,-72.9,33.1,-72.2).curveTo(33.5,-71.6,34.4,-71.1).curveTo(37.1,-69.8,39.7,-68).curveTo(40.8,-67.6,41.9,-66.6).lineTo(44.5,-64.4).curveTo(55.2,-56.4,55.2,-46).lineTo(55.1,-45.2).lineTo(55.2,-44.8).lineTo(54.7,-43.7).lineTo(53.6,-43.3).curveTo(49.2,-40.2,48.6,-39.9).curveTo(49.3,-37.7,49.9,-31).curveTo(50.3,-28.3,49.9,-25.3).curveTo(49.7,-23.2,48.9,-19.7).curveTo(48.9,-19.4,49.4,-15.6).curveTo(49.8,-12,50,-11).lineTo(51.3,-4).curveTo(51.3,-2.8,50.6,-1.9).lineTo(50.7,0).curveTo(50.7,6.1,48.2,20.1).curveTo(45.9,33.8,44.2,37.5).lineTo(43.3,39.5).curveTo(42.3,41.4,41,42.7).curveTo(33.8,44.4,26.8,45.1).lineTo(15.1,46.1).lineTo(9.1,46.5).curveTo(-1.5,46.5,-10.7,45.3).closePath().moveTo(-33.8,-1.8).curveTo(-34,0.4,-34.1,1.7).curveTo(-34.1,6.5,-33.2,11.2).curveTo(-32.7,14.5,-30.2,25).curveTo(-29.5,28.3,-27.5,31.3).curveTo(-25.4,34.5,-24.8,36.7).lineTo(-18.5,38.6).curveTo(-15.4,39.6,-12.3,40.1).curveTo(-6.8,41,-3.4,41.2).curveTo(0.7,41.6,5.2,41.6).curveTo(10.3,41.7,23.2,40.6).curveTo(35.4,39.7,37.2,39.2).lineTo(37.4,39.2).lineTo(39.9,33).curveTo(43.1,24.6,46.2,12.4).curveTo(47,9.3,46.6,4.4).curveTo(46.1,0.1,46.1,-0.5).lineTo(45.5,-0.4).curveTo(36.7,1.3,22.6,1.8).curveTo(9.4,2.3,0.6,1.6).curveTo(-6.9,0.8,-13.1,0.1).lineTo(-21.2,-1).lineTo(-22.4,-1.1).curveTo(-26.9,-1.7,-29.6,-2.8).lineTo(-33.6,-4.7).curveTo(-33.6,-3.8,-33.8,-1.8).closePath().moveTo(15.1,-43.2).curveTo(16,-34.9,15.8,-17.8).lineTo(15.8,-7.4).curveTo(15.8,-4.6,15.5,-2.3).curveTo(32.2,-2.2,40.1,-3.9).curveTo(42.5,-4.5,44.6,-5.1).lineTo(47.6,-5.9).curveTo(47,-6.9,46.2,-11).curveTo(45.6,-14.9,45.5,-16.8).lineTo(44.2,-16.2).curveTo(43.4,-15.8,42.8,-15.8).curveTo(41.7,-15.8,41,-16).lineTo(40.6,-16.2).lineTo(40.4,-16.5).curveTo(40.2,-16.8,40.2,-17.9).curveTo(40.2,-18.1,43.2,-21.4).curveTo(46,-25.6,45.4,-31.5).curveTo(44.9,-37.7,40.6,-43.6).curveTo(36.4,-49.4,36.4,-49.9).curveTo(36.4,-54.7,40.4,-52).curveTo(44.1,-49.4,47.3,-43).lineTo(50,-44.3).curveTo(52,-45.2,51.9,-45.8).curveTo(50.3,-53.1,47.5,-57.3).curveTo(45.6,-60.1,40.6,-64).curveTo(36.2,-67.3,34.6,-67.9).lineTo(34.6,-66.2).curveTo(34.6,-62.5,30.5,-54.3).curveTo(25.5,-44.5,20.3,-44.5).curveTo(19.1,-44.5,18,-46.2).lineTo(14.5,-51.3).lineTo(13.1,-53).curveTo(14.5,-47.5,15.1,-43.2).closePath().moveTo(-28.4,-40).curveTo(-27.6,-39.5,-27.4,-37.7).lineTo(-27.3,-37).lineTo(-27.2,-36.1).lineTo(-27.2,-35.9).lineTo(-27.2,-34.9).curveTo(-27.2,-31.9,-27.7,-29.2).lineTo(-29.1,-21).curveTo(-30,-15.2,-31.3,-11.3).curveTo(-31.9,-9.5,-32.5,-8.6).lineTo(-27.1,-7.4).curveTo(-24.3,-6.6,-21.2,-6.2).lineTo(-17.6,-5.7).curveTo(-9,-4.8,0.4,-3.3).curveTo(2.6,-3,7.7,-2.7).lineTo(9.4,-2.5).lineTo(11.4,-2.4).lineTo(11.4,-34).curveTo(10.4,-43.6,10.2,-45).curveTo(9.4,-51,7.7,-57.2).curveTo(7.6,-57.1,7.1,-55).curveTo(6.6,-53.4,5.1,-52.1).curveTo(-0.9,-46.8,-4,-46.8).curveTo(-11.6,-46.8,-17.9,-57.5).curveTo(-20,-61,-21.5,-65.1).curveTo(-22.8,-68.5,-22.8,-69.3).lineTo(-22.7,-70.4).curveTo(-25.1,-69.7,-26.8,-68.5).curveTo(-28.4,-67.3,-29.6,-66.9).curveTo(-36.9,-64.4,-41.4,-61.3).curveTo(-45.1,-58.8,-48.1,-55.2).curveTo(-51.9,-50.8,-51.6,-48.1).curveTo(-51.5,-47.2,-48.9,-46.3).lineTo(-43.2,-44.3).lineTo(-39.7,-42.8).curveTo(-38.7,-42.3,-37.6,-41.2).lineTo(-33.5,-37.1).lineTo(-33.4,-36.9).lineTo(-31.9,-37.1).lineTo(-30.9,-38.6).curveTo(-29.7,-40.2,-28.9,-40.2).curveTo(-28.6,-40.2,-28.4,-40).closePath().moveTo(15.8,-58.7).curveTo(17.3,-57.2,18.4,-54.5).curveTo(19.9,-50.9,20.7,-49.6).curveTo(22.1,-51,23.7,-53.3).lineTo(25.1,-55.4).curveTo(27.3,-59.1,28.2,-61.8).curveTo(29.5,-66,31.4,-68.8).curveTo(30.6,-69,30.4,-68.8).curveTo(29.3,-67.2,26.2,-64.3).curveTo(22.5,-60.7,20.8,-59.7).lineTo(20,-59.3).lineTo(15.2,-59.3).closePath().moveTo(-17.3,-72.3).lineTo(-20.8,-71.1).lineTo(-20.8,-71.1).curveTo(-20.1,-70.9,-19.7,-69.7).lineTo(-19,-67.4).lineTo(-17.5,-64.3).curveTo(-16.6,-62.7,-15.2,-60.5).curveTo(-13.9,-58.4,-11.7,-56.2).lineTo(-8.9,-53.5).curveTo(-7.6,-52.1,-6.4,-51.7).curveTo(-5.6,-51.4,-4,-51.4).curveTo(-2.9,-51.4,-1.1,-52.7).curveTo(0.6,-54,3,-56.2).lineTo(6.3,-59.3).lineTo(3.5,-59.3).curveTo(-0.9,-60.2,-4,-61.9).curveTo(-5.9,-63,-9.2,-65.7).curveTo(-10.7,-66.8,-15.9,-72.9).closePath();
	this.shape_6.setTransform(55.2,82.1);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,6.4,110.4,151.4);


(lib.GirlOutfit01 = function() {
	this.initialize();

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#282828").beginStroke().moveTo(-22.8,1.1).curveTo(-25.9,-3.1,-27.4,-7).curveTo(-28.9,-10.8,-28,-11.7).lineTo(-27.5,-12.1).curveTo(-18.1,7.1,-2.8,10.4).lineTo(-3,10.6).curveTo(-4.1,12.1,-5.4,12.1).curveTo(-14.6,12.1,-22.8,1.1).closePath().moveTo(10.7,9.1).curveTo(9.2,7.3,7.8,7).lineTo(9.1,6).lineTo(9.4,6).curveTo(9.9,6,11.9,6.4).lineTo(14.3,6.7).curveTo(16.7,6.7,24.4,2).lineTo(28.3,-0.5).curveTo(26.7,1.7,24.2,4.1).curveTo(17.7,10.3,12.3,12).curveTo(12.2,10.8,10.7,9.1).closePath();
	this.shape.setTransform(72.5,15);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#333333").beginStroke().moveTo(-29.4,-10.7).curveTo(-25.7,-13.6,-21.5,-8.7).curveTo(-19,-5.9,-16.2,-1.1).curveTo(-12.5,2.9,-9.6,5.4).curveTo(-6.8,7.7,-3.3,9.8).lineTo(-4.7,11.8).curveTo(-20,8.5,-29.4,-10.7).closePath().moveTo(10,7.8).curveTo(8,7.4,7.5,7.4).lineTo(7.2,7.4).lineTo(17.8,-1.8).curveTo(20.4,-4.2,23.6,-8.9).curveTo(26.2,-11.6,29.3,-7.7).curveTo(29.8,-3.7,26.4,0.9).lineTo(22.5,3.4).curveTo(14.8,8.1,12.4,8.1).lineTo(10,7.8).closePath();
	this.shape_1.setTransform(74.4,13.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-0.7,17.7).lineTo(-0.7,11).curveTo(-1.2,5.8,-1.3,3).curveTo(-3.4,4.7,-7,7.4).curveTo(-18.4,7.4,-27.4,-5.6).curveTo(-30.9,-10.8,-32.1,-15.5).curveTo(-33.3,-20.3,-31.3,-22).lineTo(-30.6,-22.6).curveTo(-26.5,-25.6,-23,-23.3).curveTo(-21.5,-22.3,-14.7,-14.5).curveTo(-5.7,-4.2,-0.8,-1.3).curveTo(-0.2,-2.5,1,-2.5).lineTo(1.6,-2.3).curveTo(3.6,-4,7.2,-6.9).curveTo(14.5,-12.5,15.7,-13.2).curveTo(17.1,-14.1,18.6,-16.8).curveTo(20.4,-20.1,20.8,-20.5).curveTo(23.9,-23.9,28.4,-21.4).curveTo(32.7,-19.1,32.5,-16.5).curveTo(32.5,-15.3,31.8,-13.6).curveTo(30.3,-9.9,25.7,-4).curveTo(18,5.9,9.9,7.9).curveTo(8.7,8.2,7.2,5.3).curveTo(5.4,1.9,3.2,1.2).lineTo(3.2,2.7).curveTo(3.1,5.5,2.5,10.9).lineTo(2.5,18.2).curveTo(2.3,24.3,1,24.3).curveTo(-0.4,24.3,-0.7,17.7).closePath().moveTo(-29.4,-20.2).lineTo(-29.9,-19.8).curveTo(-30.8,-18.9,-29.3,-15.1).curveTo(-27.8,-11.2,-24.7,-7.1).curveTo(-16.5,3.9,-7.3,3.9).curveTo(-6,3.9,-4.9,2.5).lineTo(-4.7,2.2).lineTo(-3.3,0.3).curveTo(-6.8,-1.9,-9.6,-4.2).curveTo(-12.5,-6.6,-16.2,-10.6).curveTo(-19.1,-15.5,-21.5,-18.2).curveTo(-24.2,-21.3,-26.6,-21.3).curveTo(-28.1,-21.3,-29.4,-20.2).closePath().moveTo(23.6,-18.4).curveTo(20.4,-13.7,17.8,-11.4).lineTo(7.1,-2.2).lineTo(5.9,-1.1).curveTo(7.3,-0.8,8.8,1).curveTo(10.3,2.7,10.4,3.9).curveTo(15.8,2.1,22.3,-4.1).curveTo(24.8,-6.4,26.4,-8.6).curveTo(29.8,-13.3,29.3,-17.2).curveTo(27.5,-19.5,25.8,-19.5).curveTo(24.7,-19.5,23.6,-18.4).closePath();
	this.shape_2.setTransform(74.4,23.1);

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#543126").beginStroke().moveTo(-16.9,4.1).curveTo(-22.1,3.6,-22.8,3.3).lineTo(-22,0.9).curveTo(-21.5,-0.6,-19.9,-1.4).lineTo(-18.5,-2).lineTo(-17.9,-2.2).lineTo(-16.3,-2.7).lineTo(-16.3,-1.9).curveTo(-16.3,1.3,-6.5,3.7).lineTo(-3.9,4.3).curveTo(-6.8,4.5,-10.2,4.5).curveTo(-11.9,4.5,-16.9,4.1).closePath().moveTo(13.2,1.4).curveTo(13.1,0,12.8,-1.2).curveTo(13.4,-1.2,13.9,-1.9).curveTo(14.7,-2.9,15.2,-3.3).curveTo(16.1,-4.1,17.7,-4.5).curveTo(18.7,-0,20,1.5).curveTo(20.9,2.5,22.9,3.4).lineTo(15.7,3.9).curveTo(13.7,4,13,4.2).curveTo(13.1,3.1,13.2,1.4).closePath();
	this.shape_3.setTransform(65.9,152.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#774736").beginStroke().moveTo(-19,4.3).curveTo(-28.8,1.8,-28.8,-1.4).lineTo(-28.8,-2.1).curveTo(-22.9,-3.4,-8.9,-3.3).curveTo(-6.9,-3.1,-5.3,-1.6).curveTo(-3.8,-0.1,-3.8,1.7).lineTo(-4.3,2.2).curveTo(-5.1,2.8,-6.7,3.4).curveTo(-10.1,4.5,-16.4,4.9).lineTo(-19,4.3).closePath().moveTo(7.6,2).curveTo(6.3,0.5,5.2,-4).curveTo(8.1,-4.9,13.1,-4.8).curveTo(20.3,-4.8,24.4,-3).curveTo(27.9,-1.4,28.8,1.3).curveTo(29.1,2,25.8,2.5).curveTo(20.4,3.1,10.4,3.9).curveTo(8.4,3,7.6,2).closePath();
	this.shape_4.setTransform(78.3,151.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#000000").beginStroke().moveTo(-30.1,9.6).curveTo(-36.2,8.6,-36.9,7.6).curveTo(-37.7,6.4,-37.7,4.2).curveTo(-37.7,-0.9,-32.5,-4.5).curveTo(-25.4,-8,-21.9,-8.6).curveTo(-20.3,-8.9,-18.5,-9.1).curveTo(-14.9,-9.3,-11,-9).curveTo(-6.9,-8.6,-0.2,-5.3).lineTo(-0.1,-5.2).curveTo(1.2,-7.3,5.2,-8.6).curveTo(6.8,-9.1,8.4,-9.4).curveTo(12.9,-10.3,18.5,-9.9).curveTo(22.4,-9.6,29.8,-7.3).lineTo(31.7,-6.3).curveTo(38,-2.7,37.7,2.6).curveTo(37.6,4.1,37.1,4.7).curveTo(36.6,5.3,34.7,5.8).curveTo(33.3,6.2,17.3,8.1).curveTo(10.7,8.9,5.2,8.1).curveTo(3,7.7,2.9,7).lineTo(2.6,5.4).lineTo(2.2,6).curveTo(0.1,8.5,-9,9.6).curveTo(-14.8,10,-20,10).curveTo(-25.3,10,-30.1,9.6).closePath().moveTo(-26.5,-2.4).lineTo(-28.1,-2).lineTo(-28.7,-1.8).lineTo(-30.1,-1.2).curveTo(-31.6,-0.4,-32.2,1.2).lineTo(-33,3.5).curveTo(-32.3,3.9,-27,4.3).curveTo(-22.1,4.8,-20.4,4.8).curveTo(-17,4.8,-14.1,4.6).curveTo(-7.8,4.3,-4.4,3.1).curveTo(-2.8,2.5,-2,1.9).lineTo(-1.5,1.4).curveTo(-1.5,-0.4,-3,-1.9).curveTo(-4.6,-3.4,-6.6,-3.6).lineTo(-8.5,-3.6).curveTo(-21.1,-3.6,-26.5,-2.4).closePath().moveTo(7.5,-4.2).curveTo(6,-3.8,5,-3.1).curveTo(4.5,-2.7,3.7,-1.6).curveTo(3.2,-1,2.6,-1).curveTo(3,0.3,3,1.6).curveTo(3,3.3,2.8,4.4).curveTo(3.5,4.3,5.5,4.1).lineTo(12.7,3.6).curveTo(22.7,2.8,28.1,2.2).curveTo(31.4,1.8,31.1,1).curveTo(30.2,-1.7,26.7,-3.2).curveTo(22.6,-5.1,15.4,-5.1).lineTo(14.8,-5.1).curveTo(10.2,-5.1,7.5,-4.2).closePath();
	this.shape_5.setTransform(76,152);

	// Layer 4
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#192D3A").beginStroke().moveTo(-9,37.1).curveTo(-11.1,37,-12.5,36.6).lineTo(-15.9,35.3).curveTo(-17.5,34.6,-18.3,33.9).lineTo(-18.9,33.4).curveTo(-28.9,2.9,-29.9,0).curveTo(-35,-13.8,-35,-28.2).lineTo(-34.7,-30.1).lineTo(-34,-33.4).lineTo(-33.3,-37.1).curveTo(-30.3,-35.8,-20.4,-34.2).lineTo(-19.6,-34.1).lineTo(-19.1,-34).lineTo(-17.3,-33.8).curveTo(-17.2,-21.6,-14.4,-5.5).curveTo(-14,-3.3,-12.3,13.7).curveTo(-10.8,29.7,-9.6,34.5).lineTo(-7.6,37.1).closePath().moveTo(22.3,36.4).lineTo(15.9,35.4).lineTo(15.9,34.7).curveTo(15.7,32.1,15.5,19.3).lineTo(15.1,5.2).curveTo(16.8,8,19.1,10.4).curveTo(20.2,11.5,26,24.5).curveTo(31.1,36,35,37).lineTo(33.4,37).curveTo(27.9,37,22.3,36.4).closePath();
	this.shape_6.setTransform(58.9,111.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#2B4A60").beginStroke().moveTo(-30.8,33.1).curveTo(-32,28.3,-33.5,12.3).curveTo(-35.2,-4.7,-35.6,-6.9).curveTo(-38.4,-23,-38.5,-35.2).curveTo(-31.7,-34.3,-15.4,-32.8).curveTo(-6.5,-32,9.7,-32.5).curveTo(27,-33.1,35.7,-34.8).curveTo(36.1,-34.8,37.1,-35.5).curveTo(37.9,-35.9,38,-35.4).curveTo(38.9,-28.8,38.1,-20.2).curveTo(37.1,-10.1,34,-0.9).curveTo(33.5,0.7,32.7,5.1).curveTo(31.7,10.8,30.7,15.3).lineTo(26.2,34.7).lineTo(21.8,35).curveTo(17,35.6,13.8,35.6).curveTo(9.9,34.6,4.8,23.1).curveTo(-1,10.1,-2.1,9).curveTo(-4.4,6.6,-6.1,3.8).lineTo(-6.6,-5).curveTo(-6.2,-10.5,-6.4,-12.9).curveTo(-10.3,-12.6,-10.8,-8.6).curveTo(-11.1,-6.6,-10.6,-4.5).lineTo(-10.2,4.9).lineTo(-9.9,18.9).lineTo(-9.6,32.3).lineTo(-9.6,33.3).curveTo(-9.7,34.1,-11.8,34.4).lineTo(-28.8,35.7).closePath();
	this.shape_7.setTransform(80.1,112.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#000000").beginStroke().moveTo(9.5,39.5).curveTo(3.9,39,2.4,38.3).lineTo(0.7,37.5).lineTo(0.2,37.3).lineTo(0.1,37.2).lineTo(-0.9,37.7).curveTo(-5.3,39.8,-12.7,40.2).curveTo(-16.5,40.7,-20.4,40.1).curveTo(-22.7,39.7,-30.5,37.7).curveTo(-33,37.2,-33.3,36.9).curveTo(-36.4,34.1,-43.4,12.2).curveTo(-51.5,-12.7,-51.6,-28.4).curveTo(-51.6,-31.7,-51,-35.5).curveTo(-50.4,-39.1,-49.7,-40.5).lineTo(-30.9,-40.5).lineTo(-27.3,-40).lineTo(-26.4,-39.9).curveTo(-14.1,-38.5,-6.8,-37.6).lineTo(-2.4,-37).curveTo(0.6,-36.6,0.4,-36.9).curveTo(7.3,-36.5,17.2,-36.6).curveTo(33.6,-36.8,40.1,-38.2).curveTo(42.4,-38.7,44.1,-39.4).lineTo(45.5,-39.9).lineTo(46.4,-40.2).lineTo(46.3,-40.5).curveTo(52.5,-39.7,51.5,-39.7).lineTo(50.3,-39.8).lineTo(50.6,-37.1).curveTo(51.1,-32.9,51.1,-31.5).curveTo(51.1,-22.3,50.9,-19.4).curveTo(50.3,-11.1,47.8,-3.4).curveTo(46.4,0.9,42.9,18.9).curveTo(39.9,34.2,38.2,36.6).curveTo(38.3,36.8,38,37.3).curveTo(37.7,37.9,37.1,38).curveTo(37.5,38.1,35.4,38.7).lineTo(31.1,39.5).curveTo(25.4,40.5,20.3,40.5).curveTo(15.3,40.5,9.5,39.5).closePath().moveTo(-46.5,-34.7).lineTo(-47.2,-31.4).lineTo(-47.5,-29.5).curveTo(-47.5,-15.1,-42.4,-1.3).curveTo(-41.4,1.6,-31.4,32.1).lineTo(-30.8,32.6).curveTo(-30,33.3,-28.4,34).lineTo(-25,35.3).curveTo(-23.6,35.7,-21.5,35.8).lineTo(-20.1,35.8).lineTo(-3.1,34.5).curveTo(-1,34.2,-0.9,33.4).lineTo(-0.9,32.4).lineTo(-1.2,19).lineTo(-1.5,5).lineTo(-1.9,-4.4).curveTo(-2.4,-6.5,-2.1,-8.5).curveTo(-1.6,-12.5,2.3,-12.8).curveTo(2.5,-10.4,2.1,-4.9).lineTo(2.6,3.9).lineTo(3,18).curveTo(3.2,30.8,3.4,33.4).lineTo(3.4,34.1).lineTo(9.8,35.1).curveTo(16.2,35.8,22.5,35.7).curveTo(25.7,35.7,30.5,35.1).lineTo(34.9,34.8).lineTo(39.4,15.4).curveTo(40.4,10.9,41.4,5.2).curveTo(42.2,0.8,42.7,-0.8).curveTo(45.8,-10,46.8,-20.1).curveTo(47.6,-28.7,46.7,-35.3).curveTo(46.6,-35.8,45.8,-35.4).curveTo(44.8,-34.7,44.4,-34.7).curveTo(35.7,-33,18.4,-32.4).curveTo(2.2,-31.9,-6.7,-32.7).curveTo(-23,-34.2,-29.8,-35.1).lineTo(-31.6,-35.3).lineTo(-32.1,-35.4).lineTo(-32.9,-35.5).curveTo(-42.8,-37.1,-45.8,-38.4).lineTo(-46.5,-34.7).closePath();
	this.shape_8.setTransform(71.4,112.4);

	// Layer 1
	this.instance = new lib.TP_logoWhite();
	this.instance.setTransform(97.2,38.2,0.048,0.048,-3.5,0,0,292.3,34.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#282828").beginStroke().moveTo(27.6,34.6).curveTo(17.2,33.2,7.2,32.1).curveTo(-3.1,30.9,-11.3,29.2).curveTo(-10.8,28.4,-10.1,26.5).curveTo(-9.3,24.3,-7.9,14.2).lineTo(-6.5,6).curveTo(-6,3.4,-6,0.4).lineTo(-6,-0.6).lineTo(-6,-0.8).lineTo(-6.1,-1.7).lineTo(-6.2,-2.4).curveTo(-5.5,-2.6,-4.8,-2.7).curveTo(-4.3,-2.8,-3.4,-1.5).curveTo(-2.7,-0.5,-2.2,0.7).curveTo(-1.9,1.3,-1.1,6.1).curveTo(0.3,11.4,4.2,15.8).curveTo(8.2,20.2,13.7,22.8).curveTo(16.8,24.3,23.1,26.1).curveTo(28.6,27.7,30.8,29.1).curveTo(34.1,31.2,34.9,35.2).curveTo(29.8,34.9,27.6,34.6).closePath().moveTo(-28.3,11.9).curveTo(-32.4,10.4,-34.4,8.8).curveTo(-35.6,7.7,-33.4,-0.4).curveTo(-31.5,-7.9,-30.1,-10.2).lineTo(-27.9,-14.1).curveTo(-26.6,-16.5,-25.4,-18).lineTo(-22.8,-21.8).curveTo(-19.9,-25.7,-18.2,-27.2).curveTo(-7.7,-35.9,3.2,-35.1).lineTo(-2.3,-33.2).lineTo(-8,-31.1).curveTo(-12,-28.1,-14.2,-23.4).lineTo(-16,-20.1).curveTo(-17.5,-17.7,-18.2,-15.6).curveTo(-18.7,-14.4,-19.8,-5.8).curveTo(-21.1,3.6,-23.9,13.2).curveTo(-25.4,13,-28.3,11.9).closePath();
	this.shape_9.setTransform(38.5,40.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#333333").beginStroke().moveTo(-0.7,35.1).curveTo(-1.5,31.1,-4.8,29).curveTo(-7,27.6,-12.4,26).curveTo(-18.8,24.2,-21.8,22.7).curveTo(-27.4,20.1,-31.3,15.7).curveTo(-35.2,11.3,-36.7,6).curveTo(-37.5,1.2,-37.8,0.6).curveTo(-38.2,-0.6,-39,-1.6).curveTo(-39.9,-2.9,-40.4,-2.8).curveTo(-41.1,-2.7,-41.7,-2.5).curveTo(-42,-4.3,-42.7,-4.8).curveTo(-43.1,-5.1,-43.5,-4.9).curveTo(-44,-7.9,-46.3,-4.5).lineTo(-47.9,-1.7).curveTo(-48.2,-1.2,-49.7,4.7).curveTo(-51.1,10.7,-52.6,13.3).curveTo(-52.9,13.8,-54.6,13.7).curveTo(-56.5,13.7,-59.4,13.1).curveTo(-56.6,3.5,-55.4,-5.9).curveTo(-54.2,-14.5,-53.8,-15.7).curveTo(-53,-17.8,-51.6,-20.2).lineTo(-49.7,-23.5).curveTo(-47.6,-28.2,-43.5,-31.2).lineTo(-37.9,-33.3).lineTo(-32.3,-35.2).lineTo(-30.2,-35).curveTo(-29.4,-35.1,-28.9,-35.4).lineTo(-28.8,-35).curveTo(-28.3,-35.1,-27.6,-35.1).curveTo(-25.9,-35.2,-24.5,-34.1).curveTo(-23.6,-33.4,-22.9,-32.3).lineTo(-20.3,-29.2).curveTo(-18.5,-27.1,-17.8,-25.9).curveTo(-15.4,-22.3,-10.8,-19.4).curveTo(-5,-15.8,1.3,-16).lineTo(8.5,-16).curveTo(12.8,-17.2,16.1,-20.1).curveTo(18,-21.8,20.9,-25).curveTo(23.4,-27.2,24.5,-31.3).curveTo(25,-33.1,26.4,-33.7).curveTo(27.2,-34,28.2,-34).curveTo(29.6,-34,30.5,-33.5).lineTo(30.5,-33.8).lineTo(33.4,-30.7).lineTo(44.8,-22.2).curveTo(47.9,-19.9,50.2,-16).lineTo(51.3,-13.8).lineTo(53.1,-9.7).lineTo(53.6,-8.2).lineTo(54.9,-4.1).lineTo(56.8,3.1).curveTo(57.8,7.5,59.4,10.8).curveTo(57.8,10.9,56.3,11.3).curveTo(54.3,11.8,52.4,12.7).curveTo(48.7,14.4,48,16.4).curveTo(47.7,15.7,47.8,15.5).curveTo(48.5,12,48.7,9.9).curveTo(49.1,6.9,48.8,4.2).curveTo(48.1,-2.5,47.4,-4.8).curveTo(44.6,-11,42,-14).curveTo(39.3,-17.3,37.4,-17.7).curveTo(35.3,-18.1,35.3,-14.7).curveTo(35.3,-14.2,39.4,-8.5).curveTo(43.7,-2.6,44.3,3.7).curveTo(44.8,9.5,42,13.8).curveTo(40.6,16,39,17.3).curveTo(39,18.4,39.2,18.7).lineTo(39.4,19).curveTo(39.8,19.4,41.7,19.4).curveTo(42.2,19.4,43.1,19).lineTo(44.3,18.4).curveTo(44.4,19.7,44.8,20.7).lineTo(45.9,23.7).curveTo(46.4,25.2,45.9,27.9).curveTo(45.4,30.3,44.7,31.1).curveTo(43.8,32.4,43,32.8).curveTo(42.1,33.1,39,33.8).curveTo(33.8,35,21.3,35.3).lineTo(12.8,35.4).curveTo(5.8,35.4,-0.7,35.1).closePath();
	this.shape_10.setTransform(74,40.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#262626").beginStroke().moveTo(0.4,15.8).lineTo(-4.6,-0.9).lineTo(-6.6,-10.6).curveTo(-7.8,-16.2,-8.2,-19.8).lineTo(-8.3,-20.9).lineTo(-7.8,-21.9).curveTo(-3.8,-19.4,7.9,-17.8).lineTo(8.3,-17.7).lineTo(7,-11.2).curveTo(5.9,-5.3,4.9,1.2).curveTo(2.8,14.4,2.1,21.9).lineTo(0.4,15.8).closePath();
	this.shape_11.setTransform(33,95);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#424242").beginStroke().moveTo(-40.7,21).lineTo(-42.5,14.2).curveTo(-41.7,6.7,-39.7,-6.6).curveTo(-38.7,-13,-37.5,-18.9).lineTo(-36.3,-25.5).curveTo(-28.8,-24.4,-11.3,-22.9).curveTo(-2.4,-22,15.3,-22.5).curveTo(33.7,-23.1,42.5,-24.8).curveTo(40.4,-21.1,40.4,-18).curveTo(40.4,-17.4,40.8,-16.5).curveTo(41.2,-15.6,41.2,-15).curveTo(41.2,-12.7,39.6,-6.3).curveTo(37.6,1.4,36.8,6.7).lineTo(35.1,18.4).lineTo(34.7,22.3).lineTo(33.8,22.6).curveTo(28.4,23.5,13.6,24.5).curveTo(-1.5,25.5,-6.7,25.5).curveTo(-23,25.5,-40.7,21).closePath();
	this.shape_12.setTransform(77.6,102.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.beginFill("#000000").beginStroke().moveTo(-18.7,65).curveTo(-21.2,64.6,-28.3,63.1).lineTo(-31.1,62.4).lineTo(-32.2,57.2).curveTo(-14.5,61.7,1.8,61.7).curveTo(7,61.7,22.1,60.7).curveTo(36.9,59.7,42.3,58.8).lineTo(43.3,58.6).lineTo(43,62.6).lineTo(42.4,62.8).curveTo(35.3,64.5,26.9,65.2).lineTo(14,66.1).curveTo(9.8,66.4,5.7,66.5).curveTo(-8.9,66.5,-18.7,65).closePath().moveTo(-2.8,13.4).curveTo(-20.3,11.8,-27.8,10.8).lineTo(-28.2,10.7).curveTo(-39.9,9.1,-43.9,6.6).lineTo(-44.4,7.6).curveTo(-44.6,6.7,-45.4,6.3).curveTo(-46.5,5.9,-47.3,5.5).curveTo(-47,5.2,-46.3,3.8).curveTo(-45.6,2.7,-45.4,2.6).lineTo(-45.6,2.1).lineTo(-45.7,-2).curveTo(-45.7,-2.8,-44.7,-6.4).curveTo(-43.6,-10.5,-43.2,-13.2).lineTo(-42.4,-18.8).lineTo(-43.4,-13.3).curveTo(-44.6,-8,-45.8,-7.7).curveTo(-47,-7,-47.7,-7.1).curveTo(-49.8,-7.2,-52.4,-7.6).curveTo(-57.5,-8.5,-61.2,-10.2).curveTo(-64.9,-12,-66.9,-13.6).curveTo(-68.4,-14.9,-68,-14.9).lineTo(-68.4,-15.5).curveTo(-68.9,-16.3,-68.9,-17.6).curveTo(-68.9,-21,-64.6,-34.9).curveTo(-63.4,-38.8,-60.4,-44).curveTo(-56.7,-50.4,-52.4,-54.6).curveTo(-49.7,-57.3,-47.5,-58.8).curveTo(-44.8,-60.7,-41.2,-62.1).curveTo(-35.2,-64.5,-30.7,-65.3).curveTo(-25.4,-66.5,-21,-66.5).lineTo(-20.8,-65.9).curveTo(-20.5,-65.7,-20.5,-64.5).lineTo(-20.4,-63.5).lineTo(-19.5,-60).curveTo(-20.9,-61.1,-22.6,-61.1).curveTo(-23.3,-61.1,-23.9,-60.9).lineTo(-24,-61.4).curveTo(-24.5,-61.1,-25.2,-60.9).lineTo(-27.4,-61.2).curveTo(-38.3,-62,-48.8,-53.2).curveTo(-50.5,-51.8,-53.4,-47.8).lineTo(-56,-44.1).curveTo(-57.2,-42.6,-58.5,-40.2).lineTo(-60.7,-36.2).curveTo(-62.1,-34,-64,-26.5).curveTo(-66.2,-18.3,-65,-17.3).curveTo(-63,-15.6,-58.9,-14.1).curveTo(-56,-13.1,-54.5,-12.8).curveTo(-51.5,-12.2,-49.6,-12.2).curveTo(-47.9,-12.2,-47.6,-12.7).curveTo(-46.2,-15.3,-44.7,-21.2).curveTo(-43.3,-27.1,-43,-27.6).lineTo(-41.4,-30.5).curveTo(-39.1,-33.9,-38.6,-30.9).curveTo(-38.1,-31,-37.8,-30.8).curveTo(-37.1,-30.3,-36.8,-28.4).lineTo(-36.7,-27.7).lineTo(-36.6,-26.8).lineTo(-36.6,-26.7).lineTo(-36.6,-25.7).curveTo(-36.6,-22.6,-37.1,-20).lineTo(-38.5,-11.8).curveTo(-39.9,-1.7,-40.7,0.5).curveTo(-41.4,2.4,-41.9,3.2).curveTo(-33.7,4.8,-23.4,6.1).curveTo(-13.4,7.2,-3,8.5).curveTo(-0.8,8.8,4.3,9.2).curveTo(14.6,9.7,26.2,9.4).curveTo(38.7,9,43.9,7.9).curveTo(47.1,7.2,47.9,6.8).curveTo(48.8,6.4,49.7,5.2).curveTo(50.3,4.3,50.8,2).curveTo(51.4,-0.7,50.9,-2.3).lineTo(49.8,-5.2).curveTo(49.3,-6.3,49.3,-7.6).lineTo(48,-7).curveTo(47.2,-6.6,46.6,-6.6).curveTo(44.7,-6.6,44.4,-6.9).lineTo(44.2,-7.3).curveTo(44,-7.5,44,-8.6).curveTo(45.5,-9.9,46.9,-12.1).curveTo(49.8,-16.4,49.2,-22.2).curveTo(48.7,-28.5,44.4,-34.4).curveTo(40.2,-40.2,40.2,-40.7).curveTo(40.2,-44,42.3,-43.6).curveTo(44.3,-43.2,47,-40).curveTo(49.5,-36.9,52.4,-30.7).curveTo(53,-28.4,53.7,-21.8).curveTo(54,-19,53.7,-16).curveTo(53.5,-14,52.7,-10.5).curveTo(52.7,-10.3,53,-9.5).curveTo(53.7,-11.5,57.4,-13.3).curveTo(59.2,-14.2,61.2,-14.6).curveTo(62.8,-15,64.4,-15.1).curveTo(62.8,-18.5,61.7,-22.9).lineTo(59.8,-30.1).lineTo(58.6,-34.1).lineTo(58,-35.6).lineTo(56.3,-39.7).lineTo(55.1,-41.9).curveTo(52.9,-45.9,49.8,-48.2).lineTo(38.4,-56.6).lineTo(35.5,-59.7).lineTo(35.4,-59.4).curveTo(34.5,-59.9,33.1,-59.9).curveTo(32.1,-59.9,31.4,-59.6).lineTo(31.6,-61.8).curveTo(31.7,-62.7,32.3,-62.8).curveTo(32.8,-62.9,33.6,-62.6).curveTo(34.3,-62.3,34.9,-62.4).curveTo(35.4,-62.6,37.5,-62.1).lineTo(37.2,-62.1).lineTo(38.2,-61.9).curveTo(41,-61.2,44.7,-58.9).curveTo(48.8,-56.3,52.1,-53.2).curveTo(56.5,-48.9,59.4,-43.5).lineTo(60.5,-41.4).curveTo(64.9,-31.9,66.3,-27.8).curveTo(68.9,-20.5,68.9,-14.2).lineTo(68.9,-13.5).lineTo(68.9,-13).lineTo(68.7,-12.5).lineTo(68.6,-12.3).lineTo(68,-11.3).lineTo(66.7,-11).lineTo(66.7,-11).lineTo(63.1,-9.4).curveTo(60,-8.1,55.3,-6.6).lineTo(54.2,-6.6).curveTo(54.3,-6.4,54.4,-3.9).curveTo(54.4,-1,54.6,0.3).curveTo(54.8,1.3,54.7,3.1).curveTo(54.7,5.1,54.4,5.7).curveTo(53.3,8.3,52.7,9.2).lineTo(51.3,11).lineTo(51,11.5).curveTo(42.3,13.1,23.8,13.7).curveTo(17,13.8,11.5,13.8).curveTo(2.7,13.8,-2.8,13.4).closePath();
	this.shape_13.setTransform(69.1,66.5);

	// Layer 2
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.beginFill("#0F0F0F").beginStroke().moveTo(-19.2,8.6).curveTo(-22.2,6.7,-24.2,4.4).curveTo(-25.7,2.6,-25.7,1.9).curveTo(-25.7,-0.3,-24.4,-1.3).curveTo(-23.7,-1.9,-22.6,-2.3).lineTo(-23.7,-2.8).curveTo(-31.6,-7,-31.6,-9.2).curveTo(-31.6,-11.1,-31.4,-10.9).lineTo(-30.9,-11.2).curveTo(-30.6,-11.4,-30.1,-11.4).curveTo(-30.9,-12.7,-30.9,-13.4).curveTo(-30.9,-14.9,-28.7,-13.8).lineTo(-21.8,-10).curveTo(-12.7,-5.3,-2.6,-3.7).curveTo(5.3,-2.9,9.5,-3.7).curveTo(13.7,-4.6,15.1,-5.2).curveTo(16.4,-5.8,23.9,-10.7).curveTo(30.9,-15.2,30.9,-9.6).curveTo(30.9,-9.3,30.7,-8.8).curveTo(31.6,-7.4,31.6,-4).curveTo(31.6,1.5,27.2,6.2).curveTo(19.8,14.3,2.6,14.3).curveTo(-10.5,14.3,-19.2,8.6).closePath();
	this.shape_14.setTransform(74.2,17.3);

	// Layer 3
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.beginFill("#0F0F0F").beginStroke().moveTo(11.8,4.6).lineTo(-4.1,4.6).lineTo(-4.8,4.6).curveTo(-10.3,3.7,-15.3,1.8).curveTo(-17.8,0.7,-20.5,-1.4).curveTo(-22,-2.5,-22.5,-3).lineTo(-22.5,-3).lineTo(-22.6,-3).lineTo(-22.8,-3.1).lineTo(-23,-3.2).curveTo(-23.9,-3.2,-24.5,-3.6).curveTo(-25.1,-4,-25.1,-4.3).curveTo(-25.1,-4.3,-25,-4.4).curveTo(-25,-4.4,-25,-4.5).curveTo(-25,-4.5,-24.9,-4.5).curveTo(-24.9,-4.5,-24.9,-4.5).lineTo(-24.7,-4.6).lineTo(-23.1,-4.6).lineTo(-22.5,-4.5).lineTo(-22.2,-4.5).curveTo(-21.1,-4.6,-18,-3.7).curveTo(-14.8,-2.9,-8.6,-1).lineTo(10.4,-1).curveTo(20.2,-2.8,22.9,-2.8).curveTo(24.4,-2.8,24.9,-2).lineTo(25,-1.7).lineTo(25,-1.1).curveTo(25,1.3,19.6,1.2).curveTo(20.6,1.4,20.6,2.7).curveTo(20.6,3.9,18.8,4.6).closePath();
	this.shape_15.setTransform(73.7,9.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.beginFill("#223825").beginStroke().moveTo(-3.4,-0.2).curveTo(-3.4,-0.3,-3.4,-0.4).curveTo(-3.4,-0.5,-3.4,-0.5).curveTo(-3.4,-0.6,-3.4,-0.6).curveTo(-3.3,-0.7,-3.3,-0.7).curveTo(-3.1,-1,-2.2,-1).lineTo(1.6,-1).curveTo(2.2,-1,2.8,-0.7).curveTo(3.4,-0.5,3.4,-0.4).curveTo(3.4,0.1,3.1,0.1).curveTo(2.8,0,1.9,-0.4).lineTo(-2.2,-0.4).lineTo(-2.8,-0.2).curveTo(-2.7,0.1,-1,0.2).curveTo(0.7,0.3,0.7,0.8).curveTo(0.7,1,0.2,1).curveTo(-3.4,1.1,-3.4,-0.2).closePath();
	this.shape_16.setTransform(44.9,6.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.beginFill("#000000").beginStroke().moveTo(-3.4,7.7).lineTo(-5.5,7).lineTo(10.4,7).curveTo(10.2,7.2,9.8,7.2).curveTo(6.4,7.4,4.7,7.7).closePath().moveTo(-10,1.3).curveTo(-16.2,-0.5,-19.4,-1.4).curveTo(-22.5,-2.2,-23.5,-2.2).lineTo(-23.9,-2.2).lineTo(-24.4,-2.3).lineTo(-26.1,-2.3).curveTo(-26.8,-2.5,-27.3,-2.4).curveTo(-27.4,-2.4,-27.5,-2.4).curveTo(-27.5,-2.4,-27.6,-2.4).curveTo(-27.6,-2.4,-27.7,-2.4).curveTo(-27.7,-2.4,-27.7,-2.5).curveTo(-27.7,-3.5,-26.8,-3.6).curveTo(-28.6,-4.7,-28.6,-5.4).curveTo(-28.6,-5.9,-27.9,-6).lineTo(-27.9,-6.2).curveTo(-28,-9.2,-24.5,-6.3).lineTo(-23.6,-5.5).lineTo(-20.4,-4.6).curveTo(-9.9,-1.4,-6,-1.4).lineTo(8,-1.6).curveTo(11.2,-1.7,24.4,-4).lineTo(25.3,-4).curveTo(26.2,-4.4,28.6,-3.4).curveTo(28.6,-3,27.2,-2.2).curveTo(26.4,-1,23.6,0.7).lineTo(23.5,0.4).curveTo(23,-0.4,21.6,-0.5).curveTo(18.8,-0.4,9.1,1.3).closePath().moveTo(-24,-0.7).lineTo(-23.9,-0.7).lineTo(-23.9,-0.6).lineTo(-23.9,-0.5).closePath();
	this.shape_17.setTransform(75.1,7.5);

	this.addChild(this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.instance,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.2,-1.1,137.8,163.2);


(lib.FrankShirtandTie = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#BC9A72").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#D5AE81").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-1.6,27.3).curveTo(-2.5,20.5,-2.5,17.8).lineTo(-2.5,16.6).lineTo(-4,16).curveTo(-4.5,16.3,-5.5,16.3).curveTo(-7.3,16.3,-7.7,13.1).lineTo(-7.8,9.7).curveTo(-7.8,6.8,-7.4,5.1).lineTo(-12.4,3.8).lineTo(-15.6,2.8).curveTo(-20,1.4,-21.9,-0.2).curveTo(-24.2,-2.2,-24.2,-5.4).curveTo(-24.2,-7.9,-22.6,-11.7).curveTo(-23.6,-12.4,-24.1,-13.2).curveTo(-24.9,-14.4,-24.9,-19.1).curveTo(-24.9,-22.8,-24.3,-27.4).lineTo(-19.9,-27).curveTo(-20.4,-23.2,-20.4,-19.9).curveTo(-20.4,-17.4,-19.9,-16.5).curveTo(-17.7,-12.8,-11.8,-13.3).lineTo(16.2,-12.9).lineTo(16.2,-16.9).lineTo(16.2,-18.9).curveTo(18.8,-16.8,20.7,-14.2).lineTo(20.5,-10.8).lineTo(20.6,-10.6).curveTo(20.6,-10.3,20.4,-9.9).lineTo(20.4,-9.2).lineTo(20,-8.8).curveTo(19.5,-8.4,18.4,-8.4).lineTo(17.6,-8.4).lineTo(17.6,-8.3).curveTo(17.9,-6.3,17.6,-4.2).curveTo(17.6,-1.4,16.9,2.2).curveTo(15.7,8.2,13.2,9.6).lineTo(6.5,9.6).curveTo(2.1,7.8,-3.4,6.2).curveTo(-3.3,7.6,-3.3,9.6).lineTo(-3.3,11.8).lineTo(-2.9,11.8).curveTo(-1.7,11.8,14.2,15).lineTo(16.5,15.5).lineTo(24.9,17.1).curveTo(24.5,18.2,23.9,19.1).curveTo(22.9,20.8,21.4,22.1).lineTo(17.5,21.4).curveTo(13.4,20.7,9.2,19.7).curveTo(4.1,18.6,0.8,17.7).lineTo(1.2,19.3).curveTo(2.3,23.3,3.1,27.4).lineTo(2,27.4).lineTo(-1.6,27.3).closePath().moveTo(-19.7,-4.9).curveTo(-19.7,-4.3,-11.6,-1.4).lineTo(-6.6,0.3).curveTo(6.7,4.7,9.7,4.7).curveTo(12.9,4.7,13,-2).lineTo(12.7,-8.4).lineTo(-10.7,-8.4).lineTo(-12.9,-8.4).curveTo(-15.5,-8.9,-17.5,-9.4).lineTo(-19.7,-4.9).closePath();
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt2();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#D5AE81").beginStroke().moveTo(-7.4,3.2).lineTo(-8.9,-3.2).curveTo(-5.6,-4.3,-0.7,-7.4).lineTo(1.8,-9.1).curveTo(2.6,-7.7,5.1,-4.4).curveTo(8.9,0.5,8.9,5).curveTo(8.9,7.4,-0.1,8.2).curveTo(-4.1,8.5,-6.3,9.1).lineTo(-7.4,3.2).closePath();
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#497C59").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#5A996E").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_5.setTransform(85.2,58);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_6.setTransform(86,143.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_7.setTransform(67.5,124.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_8.setTransform(9.8,86.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_11.setTransform(25.6,90.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_12.setTransform(84.4,77.7);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-8.8,168.9,164.3);


(lib.FrankShirt = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#BC9A72").beginStroke().moveTo(-7.2,9.4).lineTo(-7.2,9.3).lineTo(-7,8.7).curveTo(-7,6.4,-6.1,4.6).curveTo(-4.5,2,-3.3,-0.7).curveTo(-2.2,-3.2,1.1,-4.8).lineTo(1.3,-6.4).lineTo(1.4,-11.1).lineTo(1.4,-12.8).lineTo(7.2,-12.9).lineTo(6.5,-9.6).curveTo(5.8,-6.5,5.2,-4.9).curveTo(5.5,-4.5,3.8,-2).curveTo(2.8,0.5,2.4,4.4).lineTo(2.2,5.9).lineTo(1.8,5.9).lineTo(1.7,7.8).lineTo(1.3,10.8).lineTo(1.1,11.7).lineTo(1.1,11.8).lineTo(1.1,12.1).lineTo(0.9,12.9).curveTo(-7.2,10,-7.2,9.4).closePath();
	this.shape.setTransform(12.4,77.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#D5AE81").beginStroke().moveTo(-7.3,11.6).lineTo(-12.3,9.9).lineTo(-12.1,9.2).lineTo(-12.1,8.8).lineTo(-12.1,8.7).lineTo(-11.9,7.8).lineTo(-11.5,4.8).lineTo(-11.4,2.9).lineTo(-11,2.9).lineTo(-10.8,1.4).curveTo(-10.4,-2.5,-9.4,-5).curveTo(-7.7,-7.5,-8,-7.9).curveTo(-7.4,-9.5,-6.7,-12.6).lineTo(-6,-15.9).lineTo(9,-16.1).lineTo(9.7,-12).curveTo(10,-9.3,11.4,-1.6).lineTo(11.8,0.5).lineTo(12.2,2.7).lineTo(12.2,2.7).curveTo(12.2,3.7,12,4.4).lineTo(12,4.4).lineTo(12.3,9.4).curveTo(12.2,16.1,9,16.1).curveTo(6,16.1,-7.3,11.6).closePath();
	this.shape_1.setTransform(25.6,80.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-4.5,19.8).lineTo(-4.6,18.3).curveTo(-4.6,15.5,-4.2,13.7).lineTo(-9.2,12.4).lineTo(-12.4,11.5).curveTo(-16.8,10.1,-18.7,8.4).curveTo(-21,6.5,-21,3.2).curveTo(-21,2.5,-19.8,-0.4).curveTo(-18.7,-3.2,-18.5,-3.4).lineTo(-17.8,-4.5).lineTo(-17.7,-4.7).lineTo(-17.2,-5.5).curveTo(-15.1,-9,-13.6,-12.5).lineTo(-13.2,-15.2).curveTo(-13.1,-17.1,-12.4,-19.8).lineTo(-8,-18.5).lineTo(-7.9,-18.5).lineTo(-7.9,-16.8).lineTo(-8,-12.1).lineTo(-8.2,-10.5).curveTo(-11.5,-8.9,-12.6,-6.4).curveTo(-13.8,-3.7,-15.4,-1.1).curveTo(-16.3,0.7,-16.3,3).lineTo(-16.4,3.7).lineTo(-16.5,3.7).curveTo(-16.5,4.3,-8.3,7.2).lineTo(-3.4,8.9).curveTo(9.9,13.4,12.9,13.4).curveTo(16.1,13.4,16.2,6.7).lineTo(15.9,1.7).lineTo(15.9,1.7).curveTo(16.1,1,16.1,0).lineTo(16.1,0).lineTo(15.7,-2.2).lineTo(15.3,-4.3).curveTo(13.9,-12,13.6,-14.7).lineTo(13,-18.8).lineTo(16.4,-18.8).lineTo(16.7,-16.7).curveTo(16.9,-15.3,18.1,-9.9).curveTo(18.8,-6.8,19,-4.3).lineTo(20.8,0.2).lineTo(20.8,0.4).curveTo(21.1,2.3,20.8,4.5).curveTo(20.8,7.2,20.1,10.8).curveTo(18.9,16.8,16.4,18.2).lineTo(9.7,18.2).curveTo(5.3,16.5,-0.2,14.8).curveTo(-0.1,16.3,-0.1,18.2).lineTo(-0.1,19.8).closePath();
	this.shape_2.setTransform(21.7,83.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_3.setTransform(25.6,90.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_4.setTransform(9.8,86.7);

	// Layer 1
	this.instance = new lib.shirt();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#D5AE81").beginStroke().moveTo(-7.4,2.1).curveTo(-11.2,-10.1,-11.5,-11.4).curveTo(-9.6,-12,-8.6,-13).lineTo(-5.6,-15.5).lineTo(-3.2,-11.4).curveTo(-0.6,-7.4,1.9,-4.2).curveTo(2.6,-3.2,4.3,-2.3).lineTo(6.1,-1.3).curveTo(8.9,0.5,10.4,5).curveTo(11.5,8.3,11.5,11.4).curveTo(11.5,13.8,2.6,14.6).curveTo(-1.5,14.9,-3.7,15.5).curveTo(-4.1,13.2,-7.4,2.1).closePath();
	this.shape_5.setTransform(153,71.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#EACFB7").beginStroke().moveTo(-3,15).curveTo(-2.7,12.5,-2.7,11.7).lineTo(-2.7,11.7).curveTo(-4.4,12.7,-4.7,12.8).lineTo(-6.1,12.7).curveTo(-7.4,13.9,-8,13.9).curveTo(-9.4,13.9,-9.8,12.9).lineTo(-9.9,12.9).curveTo(-11.4,12.9,-11.7,11.5).lineTo(-11.7,10.7).curveTo(-11.4,9.8,-11.3,8.5).curveTo(-10.9,4.6,-9.9,2.1).curveTo(-8.2,-0.3,-8.5,-0.8).curveTo(-7.9,-2.3,-7.2,-5.4).curveTo(-6.1,-9.9,-6.1,-12.3).lineTo(-6.2,-13.3).lineTo(-6.1,-14).lineTo(-6.2,-14.6).lineTo(-5.9,-14.9).curveTo(-5,-16.1,-0.4,-16.2).curveTo(2.7,-16.2,4.1,-15.6).curveTo(4.2,-16.5,5.1,-16.9).curveTo(6.2,-17.4,7.4,-16.3).lineTo(7.6,-16).lineTo(7.7,-15.7).curveTo(8,-13.7,8.1,-12.3).curveTo(8.3,-9.9,9.2,-4.9).curveTo(9.5,-2.2,10.9,5.5).lineTo(11.3,7.7).lineTo(11.7,9.9).lineTo(11.7,9.9).curveTo(11.7,10.9,11.5,11.5).curveTo(11.1,13,9.6,13).curveTo(8.3,13,8,12.1).lineTo(7.7,12.1).curveTo(7.3,12.1,7,11.8).curveTo(6.1,13,4,13).curveTo(3.4,13,3,12.9).curveTo(2.3,14.7,1.1,14.7).lineTo(0.9,14.7).curveTo(0.2,17.1,-0.9,17.1).curveTo(-2.9,17.1,-3,15).closePath();
	this.shape_6.setTransform(26.1,73.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_7.setTransform(86,143.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_8.setTransform(67.5,124.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#CCB59F").beginStroke().moveTo(-7.4,13.2).lineTo(-7.2,12.5).curveTo(-7.2,10.2,-6.3,8.4).curveTo(-4.7,5.8,-3.5,3.1).curveTo(-2.4,0.6,0.9,-1).lineTo(1.1,-2.6).lineTo(1.2,-7.3).curveTo(1.2,-10.3,1,-11.5).lineTo(1.3,-13.6).curveTo(1.3,-14.4,1.1,-14.9).curveTo(2.1,-15.9,4,-16.5).curveTo(7,-17.4,7.3,-15).lineTo(7.4,-14.3).lineTo(7.3,-13.6).lineTo(7.4,-12.7).curveTo(7.4,-10.3,6.3,-5.8).curveTo(5.6,-2.7,5,-1.1).curveTo(5.3,-0.7,3.6,1.8).curveTo(2.6,4.3,2.2,8.2).curveTo(2.1,9.5,1.8,10.4).lineTo(1.6,11).curveTo(1.4,12.9,0.9,16).lineTo(0.7,16.7).curveTo(-7.4,13.8,-7.4,13.2).closePath();
	this.shape_9.setTransform(12.6,74);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_10.setTransform(86.9,128);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_11.setTransform(94.8,140.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,1.8).lineTo(-12.3,0.1).lineTo(-12.1,-0.6).curveTo(-11.6,-3.7,-11.4,-5.7).lineTo(-11.2,-6.3).lineTo(-11.2,-5.4).curveTo(-10.9,-4.1,-9.4,-4.1).lineTo(-9.3,-4.1).curveTo(-8.9,-3,-7.5,-3).curveTo(-6.9,-3,-5.6,-4.2).lineTo(-4.2,-4.2).curveTo(-3.9,-4.2,-2.2,-5.3).lineTo(-2.2,-5.2).curveTo(-2.2,-4.5,-2.5,-2).curveTo(-2.4,0.1,-0.4,0.1).curveTo(0.7,0.1,1.4,-2.3).lineTo(1.6,-2.3).curveTo(2.8,-2.3,3.5,-4.1).curveTo(3.9,-3.9,4.5,-3.9).curveTo(6.6,-3.9,7.5,-5.1).curveTo(7.8,-4.8,8.2,-4.8).lineTo(8.5,-4.8).curveTo(8.8,-3.9,10.1,-3.9).curveTo(11.6,-3.9,12,-5.4).lineTo(12.3,-0.4).curveTo(12.2,6.3,9,6.3).curveTo(6,6.3,-7.3,1.8).closePath();
	this.shape_12.setTransform(25.6,90.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.beginFill("#000000").beginStroke().moveTo(-25.8,55.1).curveTo(-45.4,51.7,-51.1,49.7).curveTo(-57.1,47.7,-57.2,43.9).curveTo(-62.4,15.8,-62.4,9.6).lineTo(-62.4,8.4).lineTo(-63.9,7.8).curveTo(-64.4,8,-65.4,8).curveTo(-67.2,8,-67.6,4.8).lineTo(-67.6,2.9).lineTo(-67.6,1.4).curveTo(-67.6,-1.4,-67.3,-3.2).lineTo(-72.3,-4.5).lineTo(-75.4,-5.4).curveTo(-79.9,-6.8,-81.8,-8.5).curveTo(-84,-10.4,-84.1,-13.7).curveTo(-84.1,-14.4,-82.9,-17.3).curveTo(-81.8,-20.1,-81.6,-20.3).lineTo(-80.9,-21.4).lineTo(-80.8,-21.6).lineTo(-80.3,-22.4).curveTo(-78.2,-25.9,-76.6,-29.4).lineTo(-76.3,-32.1).curveTo(-76.1,-34.3,-75.2,-37.6).curveTo(-75.2,-38.8,-74.5,-39.6).curveTo(-73.8,-41.3,-72.5,-41.7).curveTo(-71.5,-42,-71.1,-41.3).curveTo(-70.9,-40.8,-70.9,-40).lineTo(-71.1,-37.9).curveTo(-71,-36.7,-71,-33.7).lineTo(-71,-29).lineTo(-71.3,-27.4).curveTo(-74.5,-25.8,-75.7,-23.3).curveTo(-76.9,-20.6,-78.4,-18).curveTo(-79.3,-16.2,-79.3,-13.9).lineTo(-79.6,-13.2).curveTo(-79.5,-12.6,-71.4,-9.7).lineTo(-66.4,-8).curveTo(-53.1,-3.5,-50.2,-3.5).curveTo(-47,-3.5,-46.9,-10.2).lineTo(-47.2,-15.2).curveTo(-47,-15.9,-46.9,-16.8).lineTo(-46.9,-16.9).lineTo(-47.4,-19.1).lineTo(-47.7,-21.2).curveTo(-49.1,-28.9,-49.5,-31.6).curveTo(-50.3,-36.7,-50.5,-39).curveTo(-50.6,-40.4,-51,-42.4).lineTo(-51,-42.8).lineTo(-51.3,-43).curveTo(-51,-45.5,-49.3,-44).curveTo(-47.3,-42.2,-47.3,-40.3).lineTo(-46.4,-33.6).curveTo(-46.2,-32.2,-44.9,-26.8).curveTo(-44.3,-23.7,-44.1,-21.2).lineTo(-42.2,-16.7).lineTo(-42.2,-16.5).curveTo(-42,-14.6,-42.2,-12.4).curveTo(-42.2,-9.7,-43,-6.1).curveTo(-44.1,-0.1,-46.6,1.3).lineTo(-53.4,1.3).curveTo(-57.8,-0.4,-63.3,-2.1).curveTo(-63.1,-0.6,-63.1,1.3).lineTo(-63.1,2.9).lineTo(-63.1,3.5).lineTo(-62.8,3.5).curveTo(-61.6,3.5,-45.7,6.8).lineTo(-43.3,7.2).curveTo(-28.6,10.2,-22.2,10.6).lineTo(21.6,9.1).curveTo(28.7,9.2,36,7.9).curveTo(41.5,6.9,47.9,4.8).curveTo(51.4,3.6,55.3,2.5).curveTo(58.4,1.6,61.5,0.5).curveTo(59.8,-4.7,58.7,-11.4).curveTo(57,-23,55.8,-28.4).curveTo(55.4,-29.7,52.4,-36.2).lineTo(53.3,-47.5).lineTo(54.9,-44.2).lineTo(56.7,-39.8).curveTo(56.9,-38.5,60.7,-26.4).curveTo(64.1,-15.2,64.4,-12.9).curveTo(66.7,-13.5,70.7,-13.9).curveTo(79.6,-14.7,79.6,-17).curveTo(79.7,-20.1,78.5,-23.5).curveTo(77,-27.9,74.2,-29.8).lineTo(72.4,-30.8).curveTo(70.7,-31.7,70.1,-32.6).curveTo(67.6,-35.8,65,-39.9).lineTo(62.5,-44).lineTo(59.6,-41.4).curveTo(58.5,-40.5,56.7,-39.8).curveTo(58.2,-41,59.1,-45.7).curveTo(60,-49.9,62.1,-50.6).curveTo(62,-52.7,61.2,-55.1).lineTo(66,-55.1).curveTo(66.6,-53,66.6,-51.5).lineTo(66.8,-51.5).lineTo(67,-51).curveTo(67.3,-50.6,67.4,-49.5).curveTo(67.4,-48.6,65.9,-46.8).curveTo(66,-46.6,68.8,-41.1).curveTo(71.6,-35.8,72.3,-35.5).curveTo(77.6,-33.5,81.1,-26.7).curveTo(84.1,-21,84,-16.6).curveTo(84,-13,83.6,-12.3).curveTo(82.4,-10.7,76.7,-9.2).lineTo(65,-9.2).curveTo(65.5,-4.8,65.5,-1.4).curveTo(65.5,3.7,63.3,5).lineTo(63,4.3).curveTo(60.3,6.2,53.7,8.8).lineTo(52.5,11.4).curveTo(50.7,15.8,50.5,19.3).curveTo(50.3,21.2,50.8,25.1).curveTo(50.8,26.7,50.3,28).curveTo(53.8,28.8,56.7,30.2).curveTo(64.8,34.1,64.8,41.8).curveTo(64.8,43.5,63,44.9).curveTo(60.1,47.1,53.5,46.8).lineTo(22,46.8).curveTo(17.5,47,14.9,44.6).curveTo(13.1,42.9,13,41.1).curveTo(12.2,38.8,11,33.4).lineTo(10.8,32.2).lineTo(10.7,32.6).curveTo(8.7,42,6.8,45.8).lineTo(7.1,46.2).lineTo(7.1,46.2).lineTo(7.2,46.3).lineTo(7.2,46.7).lineTo(7.2,47.5).curveTo(7.2,49.9,4.3,52.1).curveTo(1.2,54.5,-3.4,55.1).closePath().moveTo(-51.7,44.6).curveTo(-50.6,45.8,-41.7,47.4).curveTo(-27.1,49.9,-25.3,50.3).lineTo(-15.6,50.3).lineTo(-3.9,50.3).curveTo(-2.1,49.3,0.5,47.5).lineTo(-8.8,45.2).curveTo(-21,42,-30.3,42).curveTo(-32.3,42,-39.5,42.7).lineTo(-39.9,42.7).curveTo(-48.1,43.4,-49.6,43.9).lineTo(-52.1,43.9).lineTo(-51.7,44.6).closePath().moveTo(-1.8,41.7).curveTo(1.3,42.8,3.6,44).curveTo(4.1,40.4,6,29.5).curveTo(7.8,20.1,8.4,15.1).lineTo(-22,15.5).curveTo(-31.6,15.1,-42.3,13.2).curveTo(-46.4,12.4,-50.7,11.5).curveTo(-55.8,10.4,-59.1,9.4).lineTo(-58.6,11.1).curveTo(-54.9,24.7,-54.9,39.2).curveTo(-53,38.7,-44.1,38).lineTo(-38.7,37.5).curveTo(-33.6,37.2,-30.6,37.2).curveTo(-14.1,37.2,-1.8,41.7).closePath().moveTo(39.6,30.8).curveTo(33.2,31.6,24.2,36).curveTo(21.1,37.5,17.7,39.4).curveTo(18.5,40,18.4,40.3).curveTo(18.9,40.6,23.2,42).lineTo(53.6,42).lineTo(54.6,41.9).lineTo(54.9,41.8).curveTo(56.7,41.5,59.5,41.3).curveTo(53.4,30.6,42.5,30.6).curveTo(41,30.6,39.6,30.8).closePath().moveTo(28.1,14.8).lineTo(21.7,14.9).lineTo(13,15).lineTo(13,16).curveTo(13,17.9,12.8,20.6).curveTo(13.6,22.4,14.4,25.4).curveTo(15.6,30.2,15.9,34.8).curveTo(17.8,32.8,21.1,31.2).curveTo(22.8,30.4,24.5,29.7).curveTo(30.8,27.4,38.9,27.1).curveTo(42.9,27,46.4,27.4).lineTo(46.1,26.2).curveTo(46,25,46,21.7).curveTo(46,13.9,46.5,11.2).curveTo(38.8,13.3,28.1,14.8).closePath().moveTo(56.7,-39.8).lineTo(56.7,-39.8).closePath();
	this.shape_13.setTransform(84.8,100.4);

	this.addChild(this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.instance,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.8,0,168.1,155.5);


(lib.FrankHoodie = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#7A4D30").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#975F3C").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-1.6,27.3).curveTo(-2.5,20.5,-2.5,17.8).lineTo(-2.5,16.6).lineTo(-4,16).curveTo(-4.5,16.3,-5.5,16.3).curveTo(-7.3,16.3,-7.7,13.1).lineTo(-7.8,9.7).curveTo(-7.8,6.8,-7.4,5.1).lineTo(-12.4,3.8).lineTo(-15.6,2.8).curveTo(-20,1.4,-21.9,-0.2).curveTo(-24.2,-2.2,-24.2,-5.4).curveTo(-24.2,-7.9,-22.6,-11.7).curveTo(-23.6,-12.4,-24.1,-13.2).curveTo(-24.9,-14.4,-24.9,-19.1).curveTo(-24.9,-22.8,-24.3,-27.4).lineTo(-19.9,-27).curveTo(-20.4,-23.2,-20.4,-19.9).curveTo(-20.4,-17.4,-19.9,-16.5).curveTo(-17.7,-12.8,-11.8,-13.3).lineTo(16.2,-12.9).lineTo(16.2,-16.9).lineTo(16.2,-18.9).curveTo(18.8,-16.8,20.7,-14.2).lineTo(20.5,-10.8).lineTo(20.6,-10.6).curveTo(20.6,-10.3,20.4,-9.9).lineTo(20.4,-9.2).lineTo(20,-8.8).curveTo(19.5,-8.4,18.4,-8.4).lineTo(17.6,-8.4).lineTo(17.6,-8.3).curveTo(17.9,-6.3,17.6,-4.2).curveTo(17.6,-1.4,16.9,2.2).curveTo(15.7,8.2,13.2,9.6).lineTo(6.5,9.6).curveTo(2.1,7.8,-3.4,6.2).curveTo(-3.3,7.6,-3.3,9.6).lineTo(-3.3,11.8).lineTo(-2.9,11.8).curveTo(-1.7,11.8,14.2,15).lineTo(16.5,15.5).lineTo(24.9,17.1).curveTo(24.5,18.2,23.9,19.1).curveTo(22.9,20.8,21.4,22.1).lineTo(17.5,21.4).curveTo(13.4,20.7,9.2,19.7).curveTo(4.1,18.6,0.8,17.7).lineTo(1.2,19.3).curveTo(2.3,23.3,3.1,27.4).lineTo(2,27.4).lineTo(-1.6,27.3).closePath().moveTo(-19.7,-4.9).curveTo(-19.7,-4.3,-11.6,-1.4).lineTo(-6.6,0.3).curveTo(6.7,4.7,9.7,4.7).curveTo(12.9,4.7,13,-2).lineTo(12.7,-8.4).lineTo(-10.7,-8.4).lineTo(-12.9,-8.4).curveTo(-15.5,-8.9,-17.5,-9.4).lineTo(-19.7,-4.9).closePath();
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt04();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#975F3C").beginStroke().moveTo(-7.4,3.2).lineTo(-8.9,-3.2).curveTo(-5.6,-4.3,-0.7,-7.4).lineTo(1.8,-9.1).curveTo(2.6,-7.7,5.1,-4.4).curveTo(8.9,0.5,8.9,5).curveTo(8.9,7.4,-0.1,8.2).curveTo(-4.1,8.5,-6.3,9.1).lineTo(-7.4,3.2).closePath();
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#497C59").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#5A996E").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_5.setTransform(85.2,58);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_6.setTransform(86,143.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_7.setTransform(67.5,124.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_8.setTransform(9.8,86.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_11.setTransform(25.6,90.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_12.setTransform(84.4,77.7);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-3.8,-6.1,172.7,161.6);


(lib.Frank_ShirtandTie = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#7A4D30").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#975F3C").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-1.6,27.3).curveTo(-2.5,20.5,-2.5,17.8).lineTo(-2.5,16.6).lineTo(-4,16).curveTo(-4.5,16.3,-5.5,16.3).curveTo(-7.3,16.3,-7.7,13.1).lineTo(-7.8,9.7).curveTo(-7.8,6.8,-7.4,5.1).lineTo(-12.4,3.8).lineTo(-15.6,2.8).curveTo(-20,1.4,-21.9,-0.2).curveTo(-24.2,-2.2,-24.2,-5.4).curveTo(-24.2,-7.9,-22.6,-11.7).curveTo(-23.6,-12.4,-24.1,-13.2).curveTo(-24.9,-14.4,-24.9,-19.1).curveTo(-24.9,-22.8,-24.3,-27.4).lineTo(-19.9,-27).curveTo(-20.4,-23.2,-20.4,-19.9).curveTo(-20.4,-17.4,-19.9,-16.5).curveTo(-17.7,-12.8,-11.8,-13.3).lineTo(16.2,-12.9).lineTo(16.2,-16.9).lineTo(16.2,-18.9).curveTo(18.8,-16.8,20.7,-14.2).lineTo(20.5,-10.8).lineTo(20.6,-10.6).curveTo(20.6,-10.3,20.4,-9.9).lineTo(20.4,-9.2).lineTo(20,-8.8).curveTo(19.5,-8.4,18.4,-8.4).lineTo(17.6,-8.4).lineTo(17.6,-8.3).curveTo(17.9,-6.3,17.6,-4.2).curveTo(17.6,-1.4,16.9,2.2).curveTo(15.7,8.2,13.2,9.6).lineTo(6.5,9.6).curveTo(2.1,7.8,-3.4,6.2).curveTo(-3.3,7.6,-3.3,9.6).lineTo(-3.3,11.8).lineTo(-2.9,11.8).curveTo(-1.7,11.8,14.2,15).lineTo(16.5,15.5).lineTo(24.9,17.1).curveTo(24.5,18.2,23.9,19.1).curveTo(22.9,20.8,21.4,22.1).lineTo(17.5,21.4).curveTo(13.4,20.7,9.2,19.7).curveTo(4.1,18.6,0.8,17.7).lineTo(1.2,19.3).curveTo(2.3,23.3,3.1,27.4).lineTo(2,27.4).lineTo(-1.6,27.3).closePath().moveTo(-19.7,-4.9).curveTo(-19.7,-4.3,-11.6,-1.4).lineTo(-6.6,0.3).curveTo(6.7,4.7,9.7,4.7).curveTo(12.9,4.7,13,-2).lineTo(12.7,-8.4).lineTo(-10.7,-8.4).lineTo(-12.9,-8.4).curveTo(-15.5,-8.9,-17.5,-9.4).lineTo(-19.7,-4.9).closePath();
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt2();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#975F3C").beginStroke().moveTo(-7.4,3.2).lineTo(-8.9,-3.2).curveTo(-5.6,-4.3,-0.7,-7.4).lineTo(1.8,-9.1).curveTo(2.6,-7.7,5.1,-4.4).curveTo(8.9,0.5,8.9,5).curveTo(8.9,7.4,-0.1,8.2).curveTo(-4.1,8.5,-6.3,9.1).lineTo(-7.4,3.2).closePath();
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#497C59").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#5A996E").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_5.setTransform(85.2,58);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_6.setTransform(86,143.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_7.setTransform(67.5,124.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_8.setTransform(9.8,86.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_11.setTransform(25.6,90.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_12.setTransform(84.4,77.7);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-8.8,168.9,164.3);


(lib.Frank_Shirt = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#7A4D30").beginStroke().moveTo(-7.2,9.4).lineTo(-7.2,9.3).lineTo(-7,8.7).curveTo(-7,6.4,-6.1,4.6).curveTo(-4.5,2,-3.3,-0.7).curveTo(-2.2,-3.2,1.1,-4.8).lineTo(1.3,-6.4).lineTo(1.4,-11.1).lineTo(1.4,-12.8).lineTo(7.2,-12.9).lineTo(6.5,-9.6).curveTo(5.8,-6.5,5.2,-4.9).curveTo(5.5,-4.5,3.8,-2).curveTo(2.8,0.5,2.4,4.4).lineTo(2.2,5.9).lineTo(1.8,5.9).lineTo(1.7,7.8).lineTo(1.3,10.8).lineTo(1.1,11.7).lineTo(1.1,11.8).lineTo(1.1,12.1).lineTo(0.9,12.9).curveTo(-7.2,10,-7.2,9.4).closePath();
	this.shape.setTransform(12.4,77.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#975F3C").beginStroke().moveTo(-7.3,11.6).lineTo(-12.3,9.9).lineTo(-12.1,9.2).lineTo(-12.1,8.8).lineTo(-12.1,8.7).lineTo(-11.9,7.8).lineTo(-11.5,4.8).lineTo(-11.4,2.9).lineTo(-11,2.9).lineTo(-10.8,1.4).curveTo(-10.4,-2.5,-9.4,-5).curveTo(-7.7,-7.5,-8,-7.9).curveTo(-7.4,-9.5,-6.7,-12.6).lineTo(-6,-15.9).lineTo(9,-16.1).lineTo(9.7,-12).curveTo(10,-9.3,11.4,-1.6).lineTo(11.8,0.5).lineTo(12.2,2.7).lineTo(12.2,2.7).curveTo(12.2,3.7,12,4.4).lineTo(12,4.4).lineTo(12.3,9.4).curveTo(12.2,16.1,9,16.1).curveTo(6,16.1,-7.3,11.6).closePath();
	this.shape_1.setTransform(25.6,80.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-4.5,19.8).lineTo(-4.6,18.3).curveTo(-4.6,15.5,-4.2,13.7).lineTo(-9.2,12.4).lineTo(-12.4,11.5).curveTo(-16.8,10.1,-18.7,8.4).curveTo(-21,6.5,-21,3.2).curveTo(-21,2.5,-19.8,-0.4).curveTo(-18.7,-3.2,-18.5,-3.4).lineTo(-17.8,-4.5).lineTo(-17.7,-4.7).lineTo(-17.2,-5.5).curveTo(-15.1,-9,-13.6,-12.5).lineTo(-13.2,-15.2).curveTo(-13.1,-17.1,-12.4,-19.8).lineTo(-8,-18.5).lineTo(-7.9,-18.5).lineTo(-7.9,-16.8).lineTo(-8,-12.1).lineTo(-8.2,-10.5).curveTo(-11.5,-8.9,-12.6,-6.4).curveTo(-13.8,-3.7,-15.4,-1.1).curveTo(-16.3,0.7,-16.3,3).lineTo(-16.4,3.7).lineTo(-16.5,3.7).curveTo(-16.5,4.3,-8.3,7.2).lineTo(-3.4,8.9).curveTo(9.9,13.4,12.9,13.4).curveTo(16.1,13.4,16.2,6.7).lineTo(15.9,1.7).lineTo(15.9,1.7).curveTo(16.1,1,16.1,0).lineTo(16.1,0).lineTo(15.7,-2.2).lineTo(15.3,-4.3).curveTo(13.9,-12,13.6,-14.7).lineTo(13,-18.8).lineTo(16.4,-18.8).lineTo(16.7,-16.7).curveTo(16.9,-15.3,18.1,-9.9).curveTo(18.8,-6.8,19,-4.3).lineTo(20.8,0.2).lineTo(20.8,0.4).curveTo(21.1,2.3,20.8,4.5).curveTo(20.8,7.2,20.1,10.8).curveTo(18.9,16.8,16.4,18.2).lineTo(9.7,18.2).curveTo(5.3,16.5,-0.2,14.8).curveTo(-0.1,16.3,-0.1,18.2).lineTo(-0.1,19.8).closePath();
	this.shape_2.setTransform(21.7,83.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_3.setTransform(25.6,90.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_4.setTransform(9.8,86.7);

	// Layer 1
	this.instance = new lib.shirt();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#7A4D30").beginStroke().moveTo(-7.4,13.2).lineTo(-7.2,12.5).curveTo(-7.2,10.2,-6.3,8.4).curveTo(-4.7,5.8,-3.5,3.1).curveTo(-2.4,0.6,0.9,-1).lineTo(1.1,-2.6).lineTo(1.2,-7.3).curveTo(1.2,-10.3,1,-11.5).lineTo(1.3,-13.6).curveTo(1.3,-14.4,1.1,-14.9).curveTo(2.1,-15.9,4,-16.5).curveTo(7,-17.4,7.3,-15).lineTo(7.4,-14.3).lineTo(7.3,-13.6).lineTo(7.4,-12.7).curveTo(7.4,-10.3,6.3,-5.8).curveTo(5.6,-2.7,5,-1.1).curveTo(5.3,-0.7,3.6,1.8).curveTo(2.6,4.3,2.2,8.2).curveTo(2.1,9.5,1.8,10.4).lineTo(1.6,11).curveTo(1.4,12.9,0.9,16).lineTo(0.7,16.7).curveTo(-7.4,13.8,-7.4,13.2).closePath();
	this.shape_5.setTransform(12.6,74);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#975F3C").beginStroke().moveTo(-66.3,15).curveTo(-66,12.6,-66,11.8).lineTo(-66,11.8).curveTo(-67.7,12.8,-68,12.9).lineTo(-69.4,12.8).curveTo(-70.7,14,-71.3,14).curveTo(-72.7,14,-73.1,12.9).lineTo(-73.2,12.9).curveTo(-74.7,12.9,-75,11.6).lineTo(-75,10.8).curveTo(-74.7,9.9,-74.6,8.6).curveTo(-74.2,4.7,-73.2,2.2).curveTo(-71.5,-0.2,-71.8,-0.7).curveTo(-71.2,-2.2,-70.5,-5.3).curveTo(-69.4,-9.9,-69.4,-12.3).lineTo(-69.5,-13.2).lineTo(-69.4,-13.9).lineTo(-69.5,-14.5).lineTo(-69.2,-14.9).curveTo(-68.3,-16.1,-63.7,-16.1).curveTo(-60.6,-16.1,-59.2,-15.5).curveTo(-59.1,-16.4,-58.2,-16.8).curveTo(-57.1,-17.3,-55.9,-16.2).lineTo(-55.7,-16).lineTo(-55.6,-15.6).curveTo(-55.3,-13.6,-55.2,-12.2).curveTo(-55,-9.9,-54.1,-4.8).curveTo(-53.8,-2.1,-52.4,5.6).lineTo(-52,7.7).lineTo(-51.6,10).lineTo(-51.6,10).curveTo(-51.6,11,-51.8,11.6).curveTo(-52.2,13.1,-53.7,13.1).curveTo(-55,13.1,-55.3,12.2).lineTo(-55.6,12.2).curveTo(-56,12.2,-56.3,11.9).curveTo(-57.2,13.1,-59.3,13.1).curveTo(-59.9,13.1,-60.3,12.9).curveTo(-61,14.7,-62.2,14.7).lineTo(-62.4,14.7).curveTo(-63.1,17.2,-64.2,17.2).curveTo(-66.2,17.2,-66.3,15).closePath().moveTo(56.1,0.4).curveTo(52.3,-11.7,52,-13).curveTo(53.9,-13.7,55,-14.6).lineTo(57.9,-17.1).lineTo(60.4,-13.1).curveTo(63,-9,65.4,-5.8).curveTo(66.1,-4.9,67.8,-4).lineTo(69.6,-3).curveTo(72.4,-1.1,73.9,3.3).curveTo(75,6.7,75,9.8).curveTo(75,12.2,66.1,12.9).curveTo(62,13.3,59.8,13.9).curveTo(59.5,11.6,56.1,0.4).closePath();
	this.shape_6.setTransform(89.4,73.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_7.setTransform(86,143.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_8.setTransform(67.5,124.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,1.8).lineTo(-12.3,0.1).lineTo(-12.1,-0.6).curveTo(-11.6,-3.7,-11.4,-5.7).lineTo(-11.2,-6.3).lineTo(-11.2,-5.4).curveTo(-10.9,-4.1,-9.4,-4.1).lineTo(-9.3,-4.1).curveTo(-8.9,-3,-7.5,-3).curveTo(-6.9,-3,-5.6,-4.2).lineTo(-4.2,-4.2).curveTo(-3.9,-4.2,-2.2,-5.3).lineTo(-2.2,-5.2).curveTo(-2.2,-4.5,-2.5,-2).curveTo(-2.4,0.1,-0.4,0.1).curveTo(0.7,0.1,1.4,-2.3).lineTo(1.6,-2.3).curveTo(2.8,-2.3,3.5,-4.1).curveTo(3.9,-3.9,4.5,-3.9).curveTo(6.6,-3.9,7.5,-5.1).curveTo(7.8,-4.8,8.2,-4.8).lineTo(8.5,-4.8).curveTo(8.8,-3.9,10.1,-3.9).curveTo(11.6,-3.9,12,-5.4).lineTo(12.3,-0.4).curveTo(12.2,6.3,9,6.3).curveTo(6,6.3,-7.3,1.8).closePath();
	this.shape_11.setTransform(25.6,90.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#000000").beginStroke().moveTo(-25.8,55.1).curveTo(-45.4,51.7,-51.1,49.7).curveTo(-57.1,47.7,-57.2,43.9).curveTo(-62.4,15.8,-62.4,9.6).lineTo(-62.4,8.4).lineTo(-63.9,7.8).curveTo(-64.4,8,-65.4,8).curveTo(-67.2,8,-67.6,4.8).lineTo(-67.6,2.9).lineTo(-67.6,1.4).curveTo(-67.6,-1.4,-67.3,-3.2).lineTo(-72.3,-4.5).lineTo(-75.4,-5.4).curveTo(-79.9,-6.8,-81.8,-8.5).curveTo(-84,-10.4,-84.1,-13.7).curveTo(-84.1,-14.4,-82.9,-17.3).curveTo(-81.8,-20.1,-81.6,-20.3).lineTo(-80.9,-21.4).lineTo(-80.8,-21.6).lineTo(-80.3,-22.4).curveTo(-78.2,-25.9,-76.6,-29.4).lineTo(-76.3,-32.1).curveTo(-76.1,-34.3,-75.2,-37.6).curveTo(-75.2,-38.8,-74.5,-39.6).curveTo(-73.8,-41.3,-72.5,-41.7).curveTo(-71.5,-42,-71.1,-41.3).curveTo(-70.9,-40.8,-70.9,-40).lineTo(-71.1,-37.9).curveTo(-71,-36.7,-71,-33.7).lineTo(-71,-29).lineTo(-71.3,-27.4).curveTo(-74.5,-25.8,-75.7,-23.3).curveTo(-76.9,-20.6,-78.4,-18).curveTo(-79.3,-16.2,-79.3,-13.9).lineTo(-79.6,-13.2).curveTo(-79.5,-12.6,-71.4,-9.7).lineTo(-66.4,-8).curveTo(-53.1,-3.5,-50.2,-3.5).curveTo(-47,-3.5,-46.9,-10.2).lineTo(-47.2,-15.2).curveTo(-47,-15.9,-46.9,-16.8).lineTo(-46.9,-16.9).lineTo(-47.4,-19.1).lineTo(-47.7,-21.2).curveTo(-49.1,-28.9,-49.5,-31.6).curveTo(-50.3,-36.7,-50.5,-39).curveTo(-50.6,-40.4,-51,-42.4).lineTo(-51,-42.8).lineTo(-51.3,-43).curveTo(-51,-45.5,-49.3,-44).curveTo(-47.3,-42.2,-47.3,-40.3).lineTo(-46.4,-33.6).curveTo(-46.2,-32.2,-44.9,-26.8).curveTo(-44.3,-23.7,-44.1,-21.2).lineTo(-42.2,-16.7).lineTo(-42.2,-16.5).curveTo(-42,-14.6,-42.2,-12.4).curveTo(-42.2,-9.7,-43,-6.1).curveTo(-44.1,-0.1,-46.6,1.3).lineTo(-53.4,1.3).curveTo(-57.8,-0.4,-63.3,-2.1).curveTo(-63.1,-0.6,-63.1,1.3).lineTo(-63.1,2.9).lineTo(-63.1,3.5).lineTo(-62.8,3.5).curveTo(-61.6,3.5,-45.7,6.8).lineTo(-43.3,7.2).curveTo(-28.6,10.2,-22.2,10.6).lineTo(21.6,9.1).curveTo(28.7,9.2,36,7.9).curveTo(41.5,6.9,47.9,4.8).curveTo(51.4,3.6,55.3,2.5).curveTo(58.4,1.6,61.5,0.5).curveTo(59.8,-4.7,58.7,-11.4).curveTo(57,-23,55.8,-28.4).curveTo(55.4,-29.7,52.4,-36.2).lineTo(53.3,-47.5).lineTo(54.9,-44.2).lineTo(56.7,-39.8).curveTo(56.9,-38.5,60.7,-26.4).curveTo(64.1,-15.2,64.4,-12.9).curveTo(66.7,-13.5,70.7,-13.9).curveTo(79.6,-14.7,79.6,-17).curveTo(79.7,-20.1,78.5,-23.5).curveTo(77,-27.9,74.2,-29.8).lineTo(72.4,-30.8).curveTo(70.7,-31.7,70.1,-32.6).curveTo(67.6,-35.8,65,-39.9).lineTo(62.5,-44).lineTo(59.6,-41.4).curveTo(58.5,-40.5,56.7,-39.8).curveTo(58.2,-41,59.1,-45.7).curveTo(60,-49.9,62.1,-50.6).curveTo(62,-52.7,61.2,-55.1).lineTo(66,-55.1).curveTo(66.6,-53,66.6,-51.5).lineTo(66.8,-51.5).lineTo(67,-51).curveTo(67.3,-50.6,67.4,-49.5).curveTo(67.4,-48.6,65.9,-46.8).curveTo(66,-46.6,68.8,-41.1).curveTo(71.6,-35.8,72.3,-35.5).curveTo(77.6,-33.5,81.1,-26.7).curveTo(84.1,-21,84,-16.6).curveTo(84,-13,83.6,-12.3).curveTo(82.4,-10.7,76.7,-9.2).lineTo(65,-9.2).curveTo(65.5,-4.8,65.5,-1.4).curveTo(65.5,3.7,63.3,5).lineTo(63,4.3).curveTo(60.3,6.2,53.7,8.8).lineTo(52.5,11.4).curveTo(50.7,15.8,50.5,19.3).curveTo(50.3,21.2,50.8,25.1).curveTo(50.8,26.7,50.3,28).curveTo(53.8,28.8,56.7,30.2).curveTo(64.8,34.1,64.8,41.8).curveTo(64.8,43.5,63,44.9).curveTo(60.1,47.1,53.5,46.8).lineTo(22,46.8).curveTo(17.5,47,14.9,44.6).curveTo(13.1,42.9,13,41.1).curveTo(12.2,38.8,11,33.4).lineTo(10.8,32.2).lineTo(10.7,32.6).curveTo(8.7,42,6.8,45.8).lineTo(7.1,46.2).lineTo(7.1,46.2).lineTo(7.2,46.3).lineTo(7.2,46.7).lineTo(7.2,47.5).curveTo(7.2,49.9,4.3,52.1).curveTo(1.2,54.5,-3.4,55.1).closePath().moveTo(-51.7,44.6).curveTo(-50.6,45.8,-41.7,47.4).curveTo(-27.1,49.9,-25.3,50.3).lineTo(-15.6,50.3).lineTo(-3.9,50.3).curveTo(-2.1,49.3,0.5,47.5).lineTo(-8.8,45.2).curveTo(-21,42,-30.3,42).curveTo(-32.3,42,-39.5,42.7).lineTo(-39.9,42.7).curveTo(-48.1,43.4,-49.6,43.9).lineTo(-52.1,43.9).lineTo(-51.7,44.6).closePath().moveTo(-1.8,41.7).curveTo(1.3,42.8,3.6,44).curveTo(4.1,40.4,6,29.5).curveTo(7.8,20.1,8.4,15.1).lineTo(-22,15.5).curveTo(-31.6,15.1,-42.3,13.2).curveTo(-46.4,12.4,-50.7,11.5).curveTo(-55.8,10.4,-59.1,9.4).lineTo(-58.6,11.1).curveTo(-54.9,24.7,-54.9,39.2).curveTo(-53,38.7,-44.1,38).lineTo(-38.7,37.5).curveTo(-33.6,37.2,-30.6,37.2).curveTo(-14.1,37.2,-1.8,41.7).closePath().moveTo(39.6,30.8).curveTo(33.2,31.6,24.2,36).curveTo(21.1,37.5,17.7,39.4).curveTo(18.5,40,18.4,40.3).curveTo(18.9,40.6,23.2,42).lineTo(53.6,42).lineTo(54.6,41.9).lineTo(54.9,41.8).curveTo(56.7,41.5,59.5,41.3).curveTo(53.4,30.6,42.5,30.6).curveTo(41,30.6,39.6,30.8).closePath().moveTo(28.1,14.8).lineTo(21.7,14.9).lineTo(13,15).lineTo(13,16).curveTo(13,17.9,12.8,20.6).curveTo(13.6,22.4,14.4,25.4).curveTo(15.6,30.2,15.9,34.8).curveTo(17.8,32.8,21.1,31.2).curveTo(22.8,30.4,24.5,29.7).curveTo(30.8,27.4,38.9,27.1).curveTo(42.9,27,46.4,27.4).lineTo(46.1,26.2).curveTo(46,25,46,21.7).curveTo(46,13.9,46.5,11.2).curveTo(38.8,13.3,28.1,14.8).closePath().moveTo(56.7,-39.8).lineTo(56.7,-39.8).closePath();
	this.shape_12.setTransform(84.8,100.4);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.instance,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.8,0,168.1,155.5);


(lib.Frank_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		this.stop();
	}
	this.frame_2 = function() {
		this.stop();
	}
	this.frame_3 = function() {
		this.stop();
	}
	this.frame_4 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1).call(this.frame_2).wait(1).call(this.frame_3).wait(1).call(this.frame_4).wait(1));

	// Layer 2
	this.instance = new lib.FrankHead();
	this.instance.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 1
	this.instance_1 = new lib.Male_Frank_Front();
	this.instance_1.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_2 = new lib.Frank_Shirt();
	this.instance_2.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_3 = new lib.FrankHoodie();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.MotifFrank();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.Frank_ShirtandTie();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.FemaleTrousersPeppa = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#D5AE81").beginStroke().moveTo(-5.3,9.4).curveTo(-7.9,7.8,-9.1,6.8).curveTo(-10.4,5.8,-11.4,4.8).lineTo(-14.1,1.9).curveTo(-15.2,0.7,-17.2,-2.6).curveTo(-19.8,-6.5,-19.8,-7.7).curveTo(-19.9,-8.6,-19.1,-9.3).curveTo(-18.2,-10.1,-16.9,-9.1).curveTo(-16.4,-8.8,-16.1,-7.8).lineTo(-15.8,-7.1).curveTo(-14.5,-7.5,-12,-7).curveTo(-11.5,-7,-5.1,-5.3).lineTo(8,-5.3).curveTo(8.3,-5.7,10.8,-6.2).lineTo(13.2,-6.6).curveTo(14,-6.6,14.4,-6.3).lineTo(15.4,-8).curveTo(16.9,-11.2,18.4,-11.1).curveTo(19.8,-11,19.9,-9.3).curveTo(19.9,-7.9,11.2,1.3).curveTo(9.4,3.1,4.4,7).lineTo(1.2,9.3).lineTo(-0.7,10.6).curveTo(-1.2,11.2,-2.1,11.2).curveTo(-2.6,11.2,-5.3,9.4).closePath();
	this.shape.setTransform(84.4,14.1);

	// Layer 3
	this.instance = new lib.GirlOutfit01();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 2
	this.instance_1 = new lib.GirlOutfit01();
	this.instance_1.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#BA986F").beginStroke().moveTo(-2,27.7).curveTo(-6.2,23.6,-7.7,17.1).curveTo(-8,15.9,-6.9,13.8).curveTo(-5.2,11.1,-4.2,9.1).curveTo(0.4,0.8,1.2,-10.3).curveTo(1.4,-12.8,3.3,-21.3).lineTo(5.3,-30.6).lineTo(6.5,-30.2).curveTo(7.3,-29.8,7.8,-29.8).curveTo(7.1,-25.6,5.9,-11.8).curveTo(4.6,3.9,4.5,4.2).curveTo(3.1,8.6,0.2,13.1).curveTo(-1.5,16.8,-0,23).lineTo(1.1,26.8).lineTo(2.2,30.6).curveTo(0,29.7,-2,27.7).closePath();
	this.shape_1.setTransform(12,71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#D5AE81").beginStroke().moveTo(-63.1,30.7).lineTo(-64.6,30.5).curveTo(-65.7,30.4,-66.7,30.1).lineTo(-68.1,29.6).lineTo(-69.2,25.8).lineTo(-70.3,22).curveTo(-71.8,15.8,-70.1,12.1).curveTo(-67.2,7.6,-65.8,3.2).curveTo(-65.7,2.9,-64.4,-12.8).curveTo(-63.2,-26.6,-62.5,-30.8).curveTo(-61.4,-30.6,-60,-30.1).lineTo(-58.1,-29.3).curveTo(-55.4,-28.3,-50.8,-24.8).lineTo(-49.6,-23.9).lineTo(-50.4,-22).curveTo(-53,-15.6,-55.3,-4.7).curveTo(-57.7,6.5,-57.4,8.5).lineTo(-57.4,8.7).curveTo(-57.2,9.5,-54.5,14.4).lineTo(-53.6,16.2).curveTo(-50.3,22.4,-50,23.5).lineTo(-49.9,23.6).curveTo(-49.4,25.7,-50.9,27.5).lineTo(-51.6,28.2).curveTo(-54.3,30.8,-60.6,30.8).lineTo(-63.1,30.7).closePath().moveTo(51.2,17.5).curveTo(49.1,13,50.3,6.7).curveTo(50.5,6.4,49.5,4.9).curveTo(48.5,3.4,48.1,3.4).lineTo(48,3.4).lineTo(47.9,3.4).lineTo(47.6,2.7).curveTo(47.4,2.2,48.4,-0.2).curveTo(49.4,-2.5,48.7,-4.4).lineTo(47.1,-8.1).curveTo(46.6,-9.4,46.7,-9.6).curveTo(47.4,-13.1,47.6,-15.2).curveTo(48,-18.2,47.7,-20.9).curveTo(47,-27.6,46.3,-29.9).lineTo(48.6,-30.8).curveTo(49.3,-27.5,53.7,-17.4).curveTo(56,-11.9,56.7,-9.7).curveTo(57.1,-8.5,57.3,-7.1).curveTo(58.3,-3.3,60.8,-0.3).curveTo(61.8,1,65.3,4.5).curveTo(71,10,71,16.8).curveTo(71,21.2,65.4,22.7).curveTo(62.1,23.6,61.5,23.8).curveTo(58.4,24.1,58.2,24.3).curveTo(53.2,21.8,51.2,17.5).closePath();
	this.shape_2.setTransform(82.3,72);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-57,36.6).curveTo(-59.5,36.5,-61.6,36.2).curveTo(-70.7,34,-73.1,32.1).curveTo(-77.4,28.6,-78.5,21).curveTo(-79.3,16.1,-78.4,13.2).curveTo(-77.8,11.5,-75.8,8.6).curveTo(-70.8,1.6,-69.3,-9.5).curveTo(-68.6,-15.1,-67.6,-19.7).lineTo(-65.4,-29.7).lineTo(-65.3,-31.8).lineTo(-65,-32.4).curveTo(-64.2,-34,-63.9,-34.2).curveTo(-61.7,-34.1,-60.8,-34.5).lineTo(-55.3,-32.6).lineTo(-51.8,-31.2).curveTo(-50.8,-30.6,-49.7,-29.5).lineTo(-45.6,-25.5).lineTo(-45.5,-25.2).lineTo(-43.9,-25.5).curveTo(-41.8,-24.5,-46.7,-6.9).lineTo(-47.2,-5.7).curveTo(-49.6,-0.2,-49.9,4.2).curveTo(-50.3,8.3,-48.7,11.3).curveTo(-47.8,13.2,-45.8,16.8).curveTo(-45,18.3,-43.9,21.7).curveTo(-42.3,26.6,-42.8,28.8).curveTo(-43.3,30.7,-45.7,32.8).curveTo(-47.4,34.3,-48.4,34.7).lineTo(-49.4,35.3).curveTo(-52.4,36.6,-56.1,36.6).lineTo(-57,36.6).closePath().moveTo(-63.6,-20.7).curveTo(-65.5,-12.2,-65.7,-9.7).curveTo(-66.5,1.4,-71.1,9.7).curveTo(-72.1,11.7,-73.8,14.4).curveTo(-74.9,16.5,-74.6,17.7).curveTo(-73.1,24.2,-68.9,28.3).curveTo(-66.9,30.3,-64.7,31.2).lineTo(-63.3,31.7).curveTo(-62.3,32,-61.2,32.1).lineTo(-59.7,32.3).curveTo(-51.4,32.8,-48.2,29.8).lineTo(-47.5,29.1).curveTo(-46,27.3,-46.5,25.2).lineTo(-46.6,25.1).curveTo(-46.9,24,-50.2,17.8).lineTo(-51.1,16).curveTo(-53.8,11.1,-54,10.3).lineTo(-54,10.1).curveTo(-54.3,8.1,-51.9,-3.1).curveTo(-49.6,-14,-47,-20.4).lineTo(-46.2,-22.3).lineTo(-47.4,-23.2).curveTo(-52,-26.7,-54.7,-27.7).lineTo(-56.6,-28.5).curveTo(-58,-29,-59.1,-29.2).curveTo(-59.6,-29.2,-60.4,-29.6).lineTo(-61.6,-30).lineTo(-63.6,-20.7).closePath().moveTo(63.2,29.7).lineTo(57.5,29.3).curveTo(46.2,20.4,50.5,10.1).lineTo(47.3,11).curveTo(46.9,8.5,45.5,6.2).lineTo(47.6,5.8).curveTo(47.2,5.1,47.9,3.3).curveTo(48.6,1.4,48.2,0.2).lineTo(47.1,-2.8).curveTo(46.7,-3.8,46.6,-5.1).lineTo(45.4,-4.5).curveTo(44.5,-4.1,44,-4.1).curveTo(42.1,-4.1,41.7,-4.5).lineTo(41.5,-4.8).curveTo(41.3,-5.1,41.3,-6.2).curveTo(42.9,-7.5,44.3,-9.7).curveTo(47.1,-14,46.6,-19.8).curveTo(46.1,-25.2,42.9,-30.3).lineTo(45.9,-32.3).lineTo(47.3,-33.4).lineTo(48.4,-31.3).curveTo(49.8,-32.2,52.3,-33.4).curveTo(53.6,-34.1,54.7,-34.5).lineTo(53.9,-36.3).curveTo(56,-36.3,57.9,-36.6).lineTo(58,-34.7).lineTo(58,-34).lineTo(58,-33.5).lineTo(57.9,-32.9).lineTo(57.8,-32.8).lineTo(57.6,-32.5).lineTo(57.5,-32.4).lineTo(56.4,-32).curveTo(55.6,-31.7,55.8,-31.2).curveTo(58.2,-24.8,61.1,-18.6).curveTo(63.2,-14,64.7,-9).curveTo(66.6,-2.5,69.3,1.1).curveTo(70.7,2.9,74,5.4).curveTo(76.5,7.4,77.5,9.5).curveTo(78.9,12.6,78.9,19).curveTo(78.9,22.3,75.9,25.5).curveTo(72.2,29.4,66.2,29.8).lineTo(65.9,29.8).lineTo(63.2,29.7).closePath().moveTo(52.9,6.5).curveTo(53.9,8,53.7,8.3).curveTo(52.5,14.6,54.6,19.1).curveTo(56.6,23.4,61.6,25.9).curveTo(61.8,25.7,64.9,25.4).curveTo(65.5,25.2,68.8,24.3).curveTo(74.4,22.8,74.4,18.4).curveTo(74.4,11.6,68.7,6.1).curveTo(65.2,2.6,64.2,1.3).curveTo(61.7,-1.7,60.7,-5.5).curveTo(60.5,-6.9,60.1,-8.1).curveTo(59.4,-10.3,57.1,-15.8).curveTo(52.7,-25.9,52,-29.2).lineTo(49.7,-28.3).curveTo(50.4,-26,51.1,-19.3).curveTo(51.4,-16.6,51,-13.6).curveTo(50.8,-11.5,50.1,-8).curveTo(50,-7.8,50.5,-6.5).lineTo(52.1,-2.8).curveTo(52.8,-0.9,51.8,1.4).curveTo(50.8,3.8,51,4.3).lineTo(51.3,5).lineTo(51.4,5).lineTo(51.5,5).curveTo(51.9,5,52.9,6.5).closePath();
	this.shape_3.setTransform(78.9,70.4);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.instance_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,2.4,157.8,163.2);


(lib.FemaleTrousersJane = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#975F3C").beginStroke().moveTo(-5.3,9.4).curveTo(-7.9,7.8,-9.1,6.8).curveTo(-10.4,5.8,-11.4,4.8).lineTo(-14.1,1.9).curveTo(-15.2,0.7,-17.2,-2.6).curveTo(-19.8,-6.5,-19.8,-7.7).curveTo(-19.9,-8.6,-19.1,-9.3).curveTo(-18.2,-10.1,-16.9,-9.1).curveTo(-16.4,-8.8,-16.1,-7.8).lineTo(-15.8,-7.1).curveTo(-14.5,-7.5,-12,-7).curveTo(-11.5,-7,-5.1,-5.3).lineTo(8,-5.3).curveTo(8.3,-5.7,10.8,-6.2).lineTo(13.2,-6.6).curveTo(14,-6.6,14.4,-6.3).lineTo(15.4,-8).curveTo(16.9,-11.2,18.4,-11.1).curveTo(19.8,-11,19.9,-9.3).curveTo(19.9,-7.9,11.2,1.3).curveTo(9.4,3.1,4.4,7).lineTo(1.2,9.3).lineTo(-0.7,10.6).curveTo(-1.2,11.2,-2.1,11.2).curveTo(-2.6,11.2,-5.3,9.4).closePath();
	this.shape.setTransform(84.4,14.1);

	// Layer 3
	this.instance = new lib.GirlOutfit01();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 2
	this.instance_1 = new lib.GirlOutfit01();
	this.instance_1.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#7A4D30").beginStroke().moveTo(-2,27.7).curveTo(-6.2,23.6,-7.7,17.1).curveTo(-8,15.9,-6.9,13.8).curveTo(-5.2,11.1,-4.2,9.1).curveTo(0.4,0.8,1.2,-10.3).curveTo(1.4,-12.8,3.3,-21.3).lineTo(5.3,-30.6).lineTo(6.5,-30.2).curveTo(7.3,-29.8,7.8,-29.8).curveTo(7.1,-25.6,5.9,-11.8).curveTo(4.6,3.9,4.5,4.2).curveTo(3.1,8.6,0.2,13.1).curveTo(-1.5,16.8,-0,23).lineTo(1.1,26.8).lineTo(2.2,30.6).curveTo(0,29.7,-2,27.7).closePath();
	this.shape_1.setTransform(12,71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#975F3C").beginStroke().moveTo(-63.1,30.7).lineTo(-64.6,30.5).curveTo(-65.7,30.4,-66.7,30.1).lineTo(-68.1,29.6).lineTo(-69.2,25.8).lineTo(-70.3,22).curveTo(-71.8,15.8,-70.1,12.1).curveTo(-67.2,7.6,-65.8,3.2).curveTo(-65.7,2.9,-64.4,-12.8).curveTo(-63.2,-26.6,-62.5,-30.8).curveTo(-61.4,-30.6,-60,-30.1).lineTo(-58.1,-29.3).curveTo(-55.4,-28.3,-50.8,-24.8).lineTo(-49.6,-23.9).lineTo(-50.4,-22).curveTo(-53,-15.6,-55.3,-4.7).curveTo(-57.7,6.5,-57.4,8.5).lineTo(-57.4,8.7).curveTo(-57.2,9.5,-54.5,14.4).lineTo(-53.6,16.2).curveTo(-50.3,22.4,-50,23.5).lineTo(-49.9,23.6).curveTo(-49.4,25.7,-50.9,27.5).lineTo(-51.6,28.2).curveTo(-54.3,30.8,-60.6,30.8).lineTo(-63.1,30.7).closePath().moveTo(51.2,17.5).curveTo(49.1,13,50.3,6.7).curveTo(50.5,6.4,49.5,4.9).curveTo(48.5,3.4,48.1,3.4).lineTo(48,3.4).lineTo(47.9,3.4).lineTo(47.6,2.7).curveTo(47.4,2.2,48.4,-0.2).curveTo(49.4,-2.5,48.7,-4.4).lineTo(47.1,-8.1).curveTo(46.6,-9.4,46.7,-9.6).curveTo(47.4,-13.1,47.6,-15.2).curveTo(48,-18.2,47.7,-20.9).curveTo(47,-27.6,46.3,-29.9).lineTo(48.6,-30.8).curveTo(49.3,-27.5,53.7,-17.4).curveTo(56,-11.9,56.7,-9.7).curveTo(57.1,-8.5,57.3,-7.1).curveTo(58.3,-3.3,60.8,-0.3).curveTo(61.8,1,65.3,4.5).curveTo(71,10,71,16.8).curveTo(71,21.2,65.4,22.7).curveTo(62.1,23.6,61.5,23.8).curveTo(58.4,24.1,58.2,24.3).curveTo(53.2,21.8,51.2,17.5).closePath();
	this.shape_2.setTransform(82.3,72);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-57,36.6).curveTo(-59.5,36.5,-61.6,36.2).curveTo(-70.7,34,-73.1,32.1).curveTo(-77.4,28.6,-78.5,21).curveTo(-79.3,16.1,-78.4,13.2).curveTo(-77.8,11.5,-75.8,8.6).curveTo(-70.8,1.6,-69.3,-9.5).curveTo(-68.6,-15.1,-67.6,-19.7).lineTo(-65.4,-29.7).lineTo(-65.3,-31.8).lineTo(-65,-32.4).curveTo(-64.2,-34,-63.9,-34.2).curveTo(-61.7,-34.1,-60.8,-34.5).lineTo(-55.3,-32.6).lineTo(-51.8,-31.2).curveTo(-50.8,-30.6,-49.7,-29.5).lineTo(-45.6,-25.5).lineTo(-45.5,-25.2).lineTo(-43.9,-25.5).curveTo(-41.8,-24.5,-46.7,-6.9).lineTo(-47.2,-5.7).curveTo(-49.6,-0.2,-49.9,4.2).curveTo(-50.3,8.3,-48.7,11.3).curveTo(-47.8,13.2,-45.8,16.8).curveTo(-45,18.3,-43.9,21.7).curveTo(-42.3,26.6,-42.8,28.8).curveTo(-43.3,30.7,-45.7,32.8).curveTo(-47.4,34.3,-48.4,34.7).lineTo(-49.4,35.3).curveTo(-52.4,36.6,-56.1,36.6).lineTo(-57,36.6).closePath().moveTo(-63.6,-20.7).curveTo(-65.5,-12.2,-65.7,-9.7).curveTo(-66.5,1.4,-71.1,9.7).curveTo(-72.1,11.7,-73.8,14.4).curveTo(-74.9,16.5,-74.6,17.7).curveTo(-73.1,24.2,-68.9,28.3).curveTo(-66.9,30.3,-64.7,31.2).lineTo(-63.3,31.7).curveTo(-62.3,32,-61.2,32.1).lineTo(-59.7,32.3).curveTo(-51.4,32.8,-48.2,29.8).lineTo(-47.5,29.1).curveTo(-46,27.3,-46.5,25.2).lineTo(-46.6,25.1).curveTo(-46.9,24,-50.2,17.8).lineTo(-51.1,16).curveTo(-53.8,11.1,-54,10.3).lineTo(-54,10.1).curveTo(-54.3,8.1,-51.9,-3.1).curveTo(-49.6,-14,-47,-20.4).lineTo(-46.2,-22.3).lineTo(-47.4,-23.2).curveTo(-52,-26.7,-54.7,-27.7).lineTo(-56.6,-28.5).curveTo(-58,-29,-59.1,-29.2).curveTo(-59.6,-29.2,-60.4,-29.6).lineTo(-61.6,-30).lineTo(-63.6,-20.7).closePath().moveTo(63.2,29.7).lineTo(57.5,29.3).curveTo(46.2,20.4,50.5,10.1).lineTo(47.3,11).curveTo(46.9,8.5,45.5,6.2).lineTo(47.6,5.8).curveTo(47.2,5.1,47.9,3.3).curveTo(48.6,1.4,48.2,0.2).lineTo(47.1,-2.8).curveTo(46.7,-3.8,46.6,-5.1).lineTo(45.4,-4.5).curveTo(44.5,-4.1,44,-4.1).curveTo(42.1,-4.1,41.7,-4.5).lineTo(41.5,-4.8).curveTo(41.3,-5.1,41.3,-6.2).curveTo(42.9,-7.5,44.3,-9.7).curveTo(47.1,-14,46.6,-19.8).curveTo(46.1,-25.2,42.9,-30.3).lineTo(45.9,-32.3).lineTo(47.3,-33.4).lineTo(48.4,-31.3).curveTo(49.8,-32.2,52.3,-33.4).curveTo(53.6,-34.1,54.7,-34.5).lineTo(53.9,-36.3).curveTo(56,-36.3,57.9,-36.6).lineTo(58,-34.7).lineTo(58,-34).lineTo(58,-33.5).lineTo(57.9,-32.9).lineTo(57.8,-32.8).lineTo(57.6,-32.5).lineTo(57.5,-32.4).lineTo(56.4,-32).curveTo(55.6,-31.7,55.8,-31.2).curveTo(58.2,-24.8,61.1,-18.6).curveTo(63.2,-14,64.7,-9).curveTo(66.6,-2.5,69.3,1.1).curveTo(70.7,2.9,74,5.4).curveTo(76.5,7.4,77.5,9.5).curveTo(78.9,12.6,78.9,19).curveTo(78.9,22.3,75.9,25.5).curveTo(72.2,29.4,66.2,29.8).lineTo(65.9,29.8).lineTo(63.2,29.7).closePath().moveTo(52.9,6.5).curveTo(53.9,8,53.7,8.3).curveTo(52.5,14.6,54.6,19.1).curveTo(56.6,23.4,61.6,25.9).curveTo(61.8,25.7,64.9,25.4).curveTo(65.5,25.2,68.8,24.3).curveTo(74.4,22.8,74.4,18.4).curveTo(74.4,11.6,68.7,6.1).curveTo(65.2,2.6,64.2,1.3).curveTo(61.7,-1.7,60.7,-5.5).curveTo(60.5,-6.9,60.1,-8.1).curveTo(59.4,-10.3,57.1,-15.8).curveTo(52.7,-25.9,52,-29.2).lineTo(49.7,-28.3).curveTo(50.4,-26,51.1,-19.3).curveTo(51.4,-16.6,51,-13.6).curveTo(50.8,-11.5,50.1,-8).curveTo(50,-7.8,50.5,-6.5).lineTo(52.1,-2.8).curveTo(52.8,-0.9,51.8,1.4).curveTo(50.8,3.8,51,4.3).lineTo(51.3,5).lineTo(51.4,5).lineTo(51.5,5).curveTo(51.9,5,52.9,6.5).closePath();
	this.shape_3.setTransform(78.9,70.4);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.instance_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,2.4,157.8,163.2);


(lib.FemaleTrousers = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#EACFB7").beginStroke().moveTo(-5.3,9.4).curveTo(-7.9,7.8,-9.1,6.8).curveTo(-10.4,5.8,-11.4,4.8).lineTo(-14.1,1.9).curveTo(-15.2,0.7,-17.2,-2.6).curveTo(-19.8,-6.5,-19.8,-7.7).curveTo(-19.9,-8.6,-19.1,-9.3).curveTo(-18.2,-10.1,-16.9,-9.1).curveTo(-16.4,-8.8,-16.1,-7.8).lineTo(-15.8,-7.1).curveTo(-14.5,-7.5,-12,-7).curveTo(-11.5,-7,-5.1,-5.3).lineTo(8,-5.3).curveTo(8.3,-5.7,10.8,-6.2).lineTo(13.2,-6.6).curveTo(14,-6.6,14.4,-6.3).lineTo(15.4,-8).curveTo(16.9,-11.2,18.4,-11.1).curveTo(19.8,-11,19.9,-9.3).curveTo(19.9,-7.9,11.2,1.3).curveTo(9.4,3.1,4.4,7).lineTo(1.2,9.3).lineTo(-0.7,10.6).curveTo(-1.2,11.2,-2.1,11.2).curveTo(-2.6,11.2,-5.3,9.4).closePath();
	this.shape.setTransform(84.4,14.1);

	// Layer 3
	this.instance = new lib.GirlOutfit01();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 2
	this.instance_1 = new lib.GirlOutfit01();
	this.instance_1.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#EACFB7").beginStroke().moveTo(-63.1,30.7).lineTo(-64.6,30.5).curveTo(-65.7,30.4,-66.7,30.1).lineTo(-68.1,29.6).lineTo(-69.2,25.8).lineTo(-70.3,22).curveTo(-71.8,15.8,-70.1,12.1).curveTo(-67.2,7.6,-65.8,3.2).curveTo(-65.7,2.9,-64.4,-12.8).curveTo(-63.2,-26.6,-62.5,-30.8).curveTo(-61.4,-30.6,-60,-30.1).lineTo(-58.1,-29.3).curveTo(-55.4,-28.3,-50.8,-24.8).lineTo(-49.6,-23.9).lineTo(-50.4,-22).curveTo(-53,-15.6,-55.3,-4.7).curveTo(-57.7,6.5,-57.4,8.5).lineTo(-57.4,8.7).curveTo(-57.2,9.5,-54.5,14.4).lineTo(-53.6,16.2).curveTo(-50.3,22.4,-50,23.5).lineTo(-49.9,23.6).curveTo(-49.4,25.7,-50.9,27.5).lineTo(-51.6,28.2).curveTo(-54.3,30.8,-60.6,30.8).lineTo(-63.1,30.7).closePath().moveTo(51.2,17.5).curveTo(49.1,13,50.3,6.7).curveTo(50.5,6.4,49.5,4.9).curveTo(48.5,3.4,48.1,3.4).lineTo(48,3.4).lineTo(47.9,3.4).lineTo(47.6,2.7).curveTo(47.4,2.2,48.4,-0.2).curveTo(49.4,-2.5,48.7,-4.4).lineTo(47.1,-8.1).curveTo(46.6,-9.4,46.7,-9.6).curveTo(47.4,-13.1,47.6,-15.2).curveTo(48,-18.2,47.7,-20.9).curveTo(47,-27.6,46.3,-29.9).lineTo(48.6,-30.8).curveTo(49.3,-27.5,53.7,-17.4).curveTo(56,-11.9,56.7,-9.7).curveTo(57.1,-8.5,57.3,-7.1).curveTo(58.3,-3.3,60.8,-0.3).curveTo(61.8,1,65.3,4.5).curveTo(71,10,71,16.8).curveTo(71,21.2,65.4,22.7).curveTo(62.1,23.6,61.5,23.8).curveTo(58.4,24.1,58.2,24.3).curveTo(53.2,21.8,51.2,17.5).closePath();
	this.shape_1.setTransform(82.3,72);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#CCB59F").beginStroke().moveTo(-2,27.7).curveTo(-6.2,23.6,-7.7,17.1).curveTo(-8,15.9,-6.9,13.8).curveTo(-5.2,11.1,-4.2,9.1).curveTo(0.4,0.8,1.2,-10.3).curveTo(1.4,-12.8,3.3,-21.3).lineTo(5.3,-30.6).lineTo(6.5,-30.2).curveTo(7.3,-29.8,7.8,-29.8).curveTo(7.1,-25.6,5.9,-11.8).curveTo(4.6,3.9,4.5,4.2).curveTo(3.1,8.6,0.2,13.1).curveTo(-1.5,16.8,-0,23).lineTo(1.1,26.8).lineTo(2.2,30.6).curveTo(0,29.7,-2,27.7).closePath();
	this.shape_2.setTransform(12,71);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-57,36.6).curveTo(-59.5,36.5,-61.6,36.2).curveTo(-70.7,34,-73.1,32.1).curveTo(-77.4,28.6,-78.5,21).curveTo(-79.3,16.1,-78.4,13.2).curveTo(-77.8,11.5,-75.8,8.6).curveTo(-70.8,1.6,-69.3,-9.5).curveTo(-68.6,-15.1,-67.6,-19.7).lineTo(-65.4,-29.7).lineTo(-65.3,-31.8).lineTo(-65,-32.4).curveTo(-64.2,-34,-63.9,-34.2).curveTo(-61.7,-34.1,-60.8,-34.5).lineTo(-55.3,-32.6).lineTo(-51.8,-31.2).curveTo(-50.8,-30.6,-49.7,-29.5).lineTo(-45.6,-25.5).lineTo(-45.5,-25.2).lineTo(-43.9,-25.5).curveTo(-41.8,-24.5,-46.7,-6.9).lineTo(-47.2,-5.7).curveTo(-49.6,-0.2,-49.9,4.2).curveTo(-50.3,8.3,-48.7,11.3).curveTo(-47.8,13.2,-45.8,16.8).curveTo(-45,18.3,-43.9,21.7).curveTo(-42.3,26.6,-42.8,28.8).curveTo(-43.3,30.7,-45.7,32.8).curveTo(-47.4,34.3,-48.4,34.7).lineTo(-49.4,35.3).curveTo(-52.4,36.6,-56.1,36.6).lineTo(-57,36.6).closePath().moveTo(-63.6,-20.7).curveTo(-65.5,-12.2,-65.7,-9.7).curveTo(-66.5,1.4,-71.1,9.7).curveTo(-72.1,11.7,-73.8,14.4).curveTo(-74.9,16.5,-74.6,17.7).curveTo(-73.1,24.2,-68.9,28.3).curveTo(-66.9,30.3,-64.7,31.2).lineTo(-63.3,31.7).curveTo(-62.3,32,-61.2,32.1).lineTo(-59.7,32.3).curveTo(-51.4,32.8,-48.2,29.8).lineTo(-47.5,29.1).curveTo(-46,27.3,-46.5,25.2).lineTo(-46.6,25.1).curveTo(-46.9,24,-50.2,17.8).lineTo(-51.1,16).curveTo(-53.8,11.1,-54,10.3).lineTo(-54,10.1).curveTo(-54.3,8.1,-51.9,-3.1).curveTo(-49.6,-14,-47,-20.4).lineTo(-46.2,-22.3).lineTo(-47.4,-23.2).curveTo(-52,-26.7,-54.7,-27.7).lineTo(-56.6,-28.5).curveTo(-58,-29,-59.1,-29.2).curveTo(-59.6,-29.2,-60.4,-29.6).lineTo(-61.6,-30).lineTo(-63.6,-20.7).closePath().moveTo(63.2,29.7).lineTo(57.5,29.3).curveTo(46.2,20.4,50.5,10.1).lineTo(47.3,11).curveTo(46.9,8.5,45.5,6.2).lineTo(47.6,5.8).curveTo(47.2,5.1,47.9,3.3).curveTo(48.6,1.4,48.2,0.2).lineTo(47.1,-2.8).curveTo(46.7,-3.8,46.6,-5.1).lineTo(45.4,-4.5).curveTo(44.5,-4.1,44,-4.1).curveTo(42.1,-4.1,41.7,-4.5).lineTo(41.5,-4.8).curveTo(41.3,-5.1,41.3,-6.2).curveTo(42.9,-7.5,44.3,-9.7).curveTo(47.1,-14,46.6,-19.8).curveTo(46.1,-25.2,42.9,-30.3).lineTo(45.9,-32.3).lineTo(47.3,-33.4).lineTo(48.4,-31.3).curveTo(49.8,-32.2,52.3,-33.4).curveTo(53.6,-34.1,54.7,-34.5).lineTo(53.9,-36.3).curveTo(56,-36.3,57.9,-36.6).lineTo(58,-34.7).lineTo(58,-34).lineTo(58,-33.5).lineTo(57.9,-32.9).lineTo(57.8,-32.8).lineTo(57.6,-32.5).lineTo(57.5,-32.4).lineTo(56.4,-32).curveTo(55.6,-31.7,55.8,-31.2).curveTo(58.2,-24.8,61.1,-18.6).curveTo(63.2,-14,64.7,-9).curveTo(66.6,-2.5,69.3,1.1).curveTo(70.7,2.9,74,5.4).curveTo(76.5,7.4,77.5,9.5).curveTo(78.9,12.6,78.9,19).curveTo(78.9,22.3,75.9,25.5).curveTo(72.2,29.4,66.2,29.8).lineTo(65.9,29.8).lineTo(63.2,29.7).closePath().moveTo(52.9,6.5).curveTo(53.9,8,53.7,8.3).curveTo(52.5,14.6,54.6,19.1).curveTo(56.6,23.4,61.6,25.9).curveTo(61.8,25.7,64.9,25.4).curveTo(65.5,25.2,68.8,24.3).curveTo(74.4,22.8,74.4,18.4).curveTo(74.4,11.6,68.7,6.1).curveTo(65.2,2.6,64.2,1.3).curveTo(61.7,-1.7,60.7,-5.5).curveTo(60.5,-6.9,60.1,-8.1).curveTo(59.4,-10.3,57.1,-15.8).curveTo(52.7,-25.9,52,-29.2).lineTo(49.7,-28.3).curveTo(50.4,-26,51.1,-19.3).curveTo(51.4,-16.6,51,-13.6).curveTo(50.8,-11.5,50.1,-8).curveTo(50,-7.8,50.5,-6.5).lineTo(52.1,-2.8).curveTo(52.8,-0.9,51.8,1.4).curveTo(50.8,3.8,51,4.3).lineTo(51.3,5).lineTo(51.4,5).lineTo(51.5,5).curveTo(51.9,5,52.9,6.5).closePath();
	this.shape_3.setTransform(78.9,70.4);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.instance_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,2.4,157.8,163.2);


(lib.FemaleMotifPeppa = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.Girl03();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#BA986F").beginStroke().moveTo(-2,27.7).curveTo(-6.2,23.6,-7.7,17.1).curveTo(-8,15.9,-6.9,13.8).curveTo(-5.2,11.1,-4.2,9.1).curveTo(-1.4,4,0,-2.1).curveTo(4.1,-5.2,5.1,-17.8).curveTo(5.5,-23.1,5.3,-29.7).lineTo(5.3,-30.4).lineTo(5.3,-30.6).lineTo(6.5,-30.2).curveTo(7.3,-29.8,7.8,-29.8).curveTo(7.1,-25.6,5.9,-11.8).curveTo(4.6,3.9,4.5,4.2).curveTo(3.1,8.6,0.2,13.1).curveTo(-1.5,16.8,-0,23).lineTo(1.1,26.8).lineTo(2.2,30.6).curveTo(0,29.7,-2,27.7).closePath();
	this.shape.setTransform(15,71);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#D5AE81").beginStroke().moveTo(-56,31.3).lineTo(-57.5,31.1).curveTo(-58.6,31,-59.6,30.7).lineTo(-61,30.2).lineTo(-62.1,26.4).lineTo(-63.2,22.6).curveTo(-64.7,16.4,-63,12.7).curveTo(-60.1,8.2,-58.7,3.8).curveTo(-58.6,3.5,-57.3,-12.2).curveTo(-56.1,-26,-55.4,-30.2).curveTo(-54.3,-30,-52.9,-29.5).lineTo(-51,-28.7).curveTo(-48.3,-27.7,-43.7,-24.2).lineTo(-42.5,-23.3).lineTo(-43.3,-21.4).curveTo(-45.9,-15,-48.2,-4.1).curveTo(-50.6,7.1,-50.3,9.1).lineTo(-50.3,9.3).curveTo(-50.1,10.1,-47.4,15).lineTo(-46.5,16.8).curveTo(-43.2,23,-42.9,24.1).lineTo(-42.8,24.2).curveTo(-42.3,26.3,-43.8,28.1).lineTo(-44.5,28.8).curveTo(-47.2,31.3,-53.5,31.3).lineTo(-56,31.3).closePath().moveTo(48.8,5.9).curveTo(49,5.7,48.5,4).curveTo(47.9,2.2,47.5,2.1).lineTo(47.4,2.2).lineTo(47.3,2.1).lineTo(47.2,1.3).lineTo(47.3,-1.6).curveTo(47.4,-4.1,47.2,-6).lineTo(46.7,-10).curveTo(46.5,-11.4,46.6,-11.6).curveTo(48.2,-14.8,48.9,-16.7).curveTo(50.1,-19.5,50.5,-22.2).curveTo(51.3,-27.1,51.4,-29.7).lineTo(51.5,-31.2).lineTo(52.6,-31.3).lineTo(53.9,-31.1).lineTo(54,-29.9).curveTo(54.1,-26,55.3,-17.2).curveTo(56.2,-11.4,56.3,-9.1).curveTo(56.4,-7.7,56.2,-6.4).curveTo(56.2,-2.5,57.8,1.1).curveTo(58.4,2.6,60.9,6.9).curveTo(65.1,13.6,63.3,20.2).curveTo(62.2,24.5,56.3,24.5).curveTo(52.9,24.5,52.3,24.6).lineTo(51.4,24.8).curveTo(50.5,12.4,48.8,5.9).closePath();
	this.shape_1.setTransform(78.2,71.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-50.1,37.9).curveTo(-52.5,37.8,-54.6,37.5).curveTo(-63.7,35.3,-66.1,33.4).curveTo(-70.4,29.9,-71.6,22.3).curveTo(-72.3,17.4,-71.5,14.5).curveTo(-70.9,12.8,-68.9,9.9).curveTo(-66,5.9,-64.3,0.6).curveTo(-63.6,0.8,-62.9,0.8).curveTo(-61.3,0.8,-59.9,-0.2).curveTo(-61.3,5.9,-64.1,11).curveTo(-65.2,13,-66.9,15.7).curveTo(-68,17.8,-67.7,19).curveTo(-66.2,25.5,-62,29.6).curveTo(-59.9,31.6,-57.8,32.5).lineTo(-56.4,33).curveTo(-55.4,33.3,-54.3,33.4).lineTo(-52.7,33.6).curveTo(-44.4,34.1,-41.2,31.1).lineTo(-40.5,30.4).curveTo(-39.1,28.6,-39.6,26.5).lineTo(-39.6,26.4).curveTo(-40,25.3,-43.3,19.1).lineTo(-44.2,17.3).curveTo(-46.8,12.4,-47,11.6).lineTo(-47,11.4).curveTo(-47.3,9.4,-45,-1.8).curveTo(-42.6,-12.7,-40.1,-19.1).lineTo(-39.3,-21).lineTo(-40.5,-21.9).curveTo(-45,-25.4,-47.7,-26.4).lineTo(-49.6,-27.2).curveTo(-51,-27.7,-52.2,-27.9).curveTo(-52.6,-27.9,-53.5,-28.3).lineTo(-54.7,-28.7).lineTo(-54.7,-28.5).lineTo(-54.9,-33.7).lineTo(-54,-33.3).lineTo(-48.4,-31.3).lineTo(-44.9,-29.9).curveTo(-43.8,-29.3,-42.7,-28.2).lineTo(-38.6,-24.2).lineTo(-38.5,-23.9).lineTo(-37,-24.2).lineTo(-36,-25.6).curveTo(-34.5,-27.7,-33.5,-27).curveTo(-32.8,-26.5,-32.5,-24.7).lineTo(-32.4,-24).lineTo(-32.3,-23.1).lineTo(-32.3,-22.9).lineTo(-32.3,-21.9).curveTo(-32.3,-18.9,-32.8,-16.3).lineTo(-34.2,-8.1).curveTo(-35.1,-2.2,-36.4,1.6).curveTo(-37.1,3.5,-37.6,4.3).curveTo(-33.1,5.2,-28.1,6).lineTo(-27.5,11.3).curveTo(-36.2,9.9,-39.6,7.7).curveTo(-40.2,9.4,-41.8,12.6).curveTo(-40.9,14.5,-38.9,18.1).curveTo(-38,19.6,-36.9,23).curveTo(-35.3,27.9,-35.9,30.1).curveTo(-36.4,32,-38.8,34.1).curveTo(-40.4,35.6,-41.5,36).lineTo(-42.4,36.6).curveTo(-45.4,37.9,-49.1,37.9).lineTo(-50.1,37.9).closePath().moveTo(-40.2,-4.4).curveTo(-42.6,1.1,-43,5.5).curveTo(-41.9,4.2,-41.1,3.7).lineTo(-41.3,3.3).curveTo(-41.4,3,-41.4,1.8).curveTo(-41.4,0.9,-40.4,-2.6).lineTo(-39.6,-5.9).lineTo(-40.2,-4.4).closePath().moveTo(55.6,31.5).lineTo(49.9,31.6).lineTo(49.9,31.5).lineTo(48.5,9.1).curveTo(46.7,9.2,44.2,9).lineTo(43.7,4.1).lineTo(46.8,4.2).curveTo(46.1,1.5,45.9,-1.8).lineTo(45.6,-5).curveTo(45.5,-6.1,45.7,-7.3).lineTo(44.4,-7.1).lineTo(43.4,-7).curveTo(43.5,-9.4,43.8,-11.6).lineTo(44.7,-12.4).curveTo(48.5,-15.8,49.5,-21.5).curveTo(50,-24,49.8,-26.5).curveTo(51.2,-28,52.9,-29.4).lineTo(53.2,-29.5).lineTo(53.5,-29.5).lineTo(55.4,-31.7).lineTo(56.2,-32.1).curveTo(57.6,-32.8,58.7,-33.2).lineTo(58,-34.6).lineTo(59.1,-35.6).lineTo(61.4,-37.9).curveTo(62,-35.7,62,-33.4).lineTo(61.9,-32.7).lineTo(62,-32.2).lineTo(61.8,-31.6).lineTo(61.8,-31.5).lineTo(61.5,-31.2).lineTo(61.4,-31.1).lineTo(60.4,-30.7).curveTo(59.6,-30.4,59.8,-29.9).lineTo(60.5,-28).lineTo(61.6,-27.7).curveTo(62.2,-22.2,63.2,-16.6).curveTo(64.1,-11.6,64.2,-6.4).curveTo(64.4,0.4,66.1,4.5).curveTo(67,6.6,69.5,9.8).curveTo(71.4,12.4,71.8,14.8).curveTo(72.3,18.1,70.7,24.3).curveTo(69.8,27.5,66.2,29.8).curveTo(62.9,31.8,58.8,31.8).curveTo(57.3,31.8,55.6,31.5).closePath().moveTo(51.7,6.3).curveTo(52.3,8,52.1,8.2).curveTo(53.7,14.7,54.6,27.1).lineTo(55.5,26.9).curveTo(56.1,26.8,59.6,26.8).curveTo(65.4,26.8,66.6,22.5).curveTo(68.3,15.9,64.2,9.2).curveTo(61.7,4.9,61.1,3.4).curveTo(59.5,-0.2,59.5,-4.1).curveTo(59.6,-5.4,59.6,-6.8).curveTo(59.5,-9.1,58.6,-14.9).curveTo(57.3,-23.7,57.2,-27.6).lineTo(57.2,-28.8).lineTo(55.9,-29).lineTo(54.7,-28.9).lineTo(54.7,-27.4).curveTo(54.5,-24.8,53.7,-19.9).curveTo(53.3,-17.2,52.2,-14.4).curveTo(51.5,-12.5,49.8,-9.3).curveTo(49.7,-9.1,49.9,-7.7).lineTo(50.4,-3.7).curveTo(50.6,-1.8,50.5,0.7).lineTo(50.5,3.6).lineTo(50.6,4.4).lineTo(50.6,4.5).lineTo(50.7,4.4).curveTo(51.1,4.5,51.7,6.3).closePath();
	this.shape_2.setTransform(74.9,69.1);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,4.2,143.9,163.8);


(lib.FemaleMotifJane = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.Girl03();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#7A4D30").beginStroke().moveTo(-2,27.7).curveTo(-6.2,23.6,-7.7,17.1).curveTo(-8,15.9,-6.9,13.8).curveTo(-5.2,11.1,-4.2,9.1).curveTo(-1.4,4,0,-2.1).curveTo(4.1,-5.2,5.1,-17.8).curveTo(5.5,-23.1,5.3,-29.7).lineTo(5.3,-30.4).lineTo(5.3,-30.6).lineTo(6.5,-30.2).curveTo(7.3,-29.8,7.8,-29.8).curveTo(7.1,-25.6,5.9,-11.8).curveTo(4.6,3.9,4.5,4.2).curveTo(3.1,8.6,0.2,13.1).curveTo(-1.5,16.8,-0,23).lineTo(1.1,26.8).lineTo(2.2,30.6).curveTo(0,29.7,-2,27.7).closePath();
	this.shape.setTransform(15,71);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#975F3C").beginStroke().moveTo(-56,31.3).lineTo(-57.5,31.1).curveTo(-58.6,31,-59.6,30.7).lineTo(-61,30.2).lineTo(-62.1,26.4).lineTo(-63.2,22.6).curveTo(-64.7,16.4,-63,12.7).curveTo(-60.1,8.2,-58.7,3.8).curveTo(-58.6,3.5,-57.3,-12.2).curveTo(-56.1,-26,-55.4,-30.2).curveTo(-54.3,-30,-52.9,-29.5).lineTo(-51,-28.7).curveTo(-48.3,-27.7,-43.7,-24.2).lineTo(-42.5,-23.3).lineTo(-43.3,-21.4).curveTo(-45.9,-15,-48.2,-4.1).curveTo(-50.6,7.1,-50.3,9.1).lineTo(-50.3,9.3).curveTo(-50.1,10.1,-47.4,15).lineTo(-46.5,16.8).curveTo(-43.2,23,-42.9,24.1).lineTo(-42.8,24.2).curveTo(-42.3,26.3,-43.8,28.1).lineTo(-44.5,28.8).curveTo(-47.2,31.3,-53.5,31.3).lineTo(-56,31.3).closePath().moveTo(48.8,5.9).curveTo(49,5.7,48.5,4).curveTo(47.9,2.2,47.5,2.1).lineTo(47.4,2.2).lineTo(47.3,2.1).lineTo(47.2,1.3).lineTo(47.3,-1.6).curveTo(47.4,-4.1,47.2,-6).lineTo(46.7,-10).curveTo(46.5,-11.4,46.6,-11.6).curveTo(48.2,-14.8,48.9,-16.7).curveTo(50.1,-19.5,50.5,-22.2).curveTo(51.3,-27.1,51.4,-29.7).lineTo(51.5,-31.2).lineTo(52.6,-31.3).lineTo(53.9,-31.1).lineTo(54,-29.9).curveTo(54.1,-26,55.3,-17.2).curveTo(56.2,-11.4,56.3,-9.1).curveTo(56.4,-7.7,56.2,-6.4).curveTo(56.2,-2.5,57.8,1.1).curveTo(58.4,2.6,60.9,6.9).curveTo(65.1,13.6,63.3,20.2).curveTo(62.2,24.5,56.3,24.5).curveTo(52.9,24.5,52.3,24.6).lineTo(51.4,24.8).curveTo(50.5,12.4,48.8,5.9).closePath();
	this.shape_1.setTransform(78.2,71.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-50.1,37.9).curveTo(-52.5,37.8,-54.6,37.5).curveTo(-63.7,35.3,-66.1,33.4).curveTo(-70.4,29.9,-71.6,22.3).curveTo(-72.3,17.4,-71.5,14.5).curveTo(-70.9,12.8,-68.9,9.9).curveTo(-66,5.9,-64.3,0.6).curveTo(-63.6,0.8,-62.9,0.8).curveTo(-61.3,0.8,-59.9,-0.2).curveTo(-61.3,5.9,-64.1,11).curveTo(-65.2,13,-66.9,15.7).curveTo(-68,17.8,-67.7,19).curveTo(-66.2,25.5,-62,29.6).curveTo(-59.9,31.6,-57.8,32.5).lineTo(-56.4,33).curveTo(-55.4,33.3,-54.3,33.4).lineTo(-52.7,33.6).curveTo(-44.4,34.1,-41.2,31.1).lineTo(-40.5,30.4).curveTo(-39.1,28.6,-39.6,26.5).lineTo(-39.6,26.4).curveTo(-40,25.3,-43.3,19.1).lineTo(-44.2,17.3).curveTo(-46.8,12.4,-47,11.6).lineTo(-47,11.4).curveTo(-47.3,9.4,-45,-1.8).curveTo(-42.6,-12.7,-40.1,-19.1).lineTo(-39.3,-21).lineTo(-40.5,-21.9).curveTo(-45,-25.4,-47.7,-26.4).lineTo(-49.6,-27.2).curveTo(-51,-27.7,-52.2,-27.9).curveTo(-52.6,-27.9,-53.5,-28.3).lineTo(-54.7,-28.7).lineTo(-54.7,-28.5).lineTo(-54.9,-33.7).lineTo(-54,-33.3).lineTo(-48.4,-31.3).lineTo(-44.9,-29.9).curveTo(-43.8,-29.3,-42.7,-28.2).lineTo(-38.6,-24.2).lineTo(-38.5,-23.9).lineTo(-37,-24.2).lineTo(-36,-25.6).curveTo(-34.5,-27.7,-33.5,-27).curveTo(-32.8,-26.5,-32.5,-24.7).lineTo(-32.4,-24).lineTo(-32.3,-23.1).lineTo(-32.3,-22.9).lineTo(-32.3,-21.9).curveTo(-32.3,-18.9,-32.8,-16.3).lineTo(-34.2,-8.1).curveTo(-35.1,-2.2,-36.4,1.6).curveTo(-37.1,3.5,-37.6,4.3).curveTo(-33.1,5.2,-28.1,6).lineTo(-27.5,11.3).curveTo(-36.2,9.9,-39.6,7.7).curveTo(-40.2,9.4,-41.8,12.6).curveTo(-40.9,14.5,-38.9,18.1).curveTo(-38,19.6,-36.9,23).curveTo(-35.3,27.9,-35.9,30.1).curveTo(-36.4,32,-38.8,34.1).curveTo(-40.4,35.6,-41.5,36).lineTo(-42.4,36.6).curveTo(-45.4,37.9,-49.1,37.9).lineTo(-50.1,37.9).closePath().moveTo(-40.2,-4.4).curveTo(-42.6,1.1,-43,5.5).curveTo(-41.9,4.2,-41.1,3.7).lineTo(-41.3,3.3).curveTo(-41.4,3,-41.4,1.8).curveTo(-41.4,0.9,-40.4,-2.6).lineTo(-39.6,-5.9).lineTo(-40.2,-4.4).closePath().moveTo(55.6,31.5).lineTo(49.9,31.6).lineTo(49.9,31.5).lineTo(48.5,9.1).curveTo(46.7,9.2,44.2,9).lineTo(43.7,4.1).lineTo(46.8,4.2).curveTo(46.1,1.5,45.9,-1.8).lineTo(45.6,-5).curveTo(45.5,-6.1,45.7,-7.3).lineTo(44.4,-7.1).lineTo(43.4,-7).curveTo(43.5,-9.4,43.8,-11.6).lineTo(44.7,-12.4).curveTo(48.5,-15.8,49.5,-21.5).curveTo(50,-24,49.8,-26.5).curveTo(51.2,-28,52.9,-29.4).lineTo(53.2,-29.5).lineTo(53.5,-29.5).lineTo(55.4,-31.7).lineTo(56.2,-32.1).curveTo(57.6,-32.8,58.7,-33.2).lineTo(58,-34.6).lineTo(59.1,-35.6).lineTo(61.4,-37.9).curveTo(62,-35.7,62,-33.4).lineTo(61.9,-32.7).lineTo(62,-32.2).lineTo(61.8,-31.6).lineTo(61.8,-31.5).lineTo(61.5,-31.2).lineTo(61.4,-31.1).lineTo(60.4,-30.7).curveTo(59.6,-30.4,59.8,-29.9).lineTo(60.5,-28).lineTo(61.6,-27.7).curveTo(62.2,-22.2,63.2,-16.6).curveTo(64.1,-11.6,64.2,-6.4).curveTo(64.4,0.4,66.1,4.5).curveTo(67,6.6,69.5,9.8).curveTo(71.4,12.4,71.8,14.8).curveTo(72.3,18.1,70.7,24.3).curveTo(69.8,27.5,66.2,29.8).curveTo(62.9,31.8,58.8,31.8).curveTo(57.3,31.8,55.6,31.5).closePath().moveTo(51.7,6.3).curveTo(52.3,8,52.1,8.2).curveTo(53.7,14.7,54.6,27.1).lineTo(55.5,26.9).curveTo(56.1,26.8,59.6,26.8).curveTo(65.4,26.8,66.6,22.5).curveTo(68.3,15.9,64.2,9.2).curveTo(61.7,4.9,61.1,3.4).curveTo(59.5,-0.2,59.5,-4.1).curveTo(59.6,-5.4,59.6,-6.8).curveTo(59.5,-9.1,58.6,-14.9).curveTo(57.3,-23.7,57.2,-27.6).lineTo(57.2,-28.8).lineTo(55.9,-29).lineTo(54.7,-28.9).lineTo(54.7,-27.4).curveTo(54.5,-24.8,53.7,-19.9).curveTo(53.3,-17.2,52.2,-14.4).curveTo(51.5,-12.5,49.8,-9.3).curveTo(49.7,-9.1,49.9,-7.7).lineTo(50.4,-3.7).curveTo(50.6,-1.8,50.5,0.7).lineTo(50.5,3.6).lineTo(50.6,4.4).lineTo(50.6,4.5).lineTo(50.7,4.4).curveTo(51.1,4.5,51.7,6.3).closePath();
	this.shape_2.setTransform(74.9,69.1);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,4.2,143.9,163.8);


(lib.FemaleMotif = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.Girl03();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#CCB59F").beginStroke().moveTo(-2,27.7).curveTo(-6.2,23.6,-7.7,17.1).curveTo(-8,15.9,-6.9,13.8).curveTo(-5.2,11.1,-4.2,9.1).curveTo(-1.4,4,0,-2.1).curveTo(4.1,-5.2,5.1,-17.8).curveTo(5.5,-23.1,5.3,-29.7).lineTo(5.3,-30.4).lineTo(5.3,-30.6).lineTo(6.5,-30.2).curveTo(7.3,-29.8,7.8,-29.8).curveTo(7.1,-25.6,5.9,-11.8).curveTo(4.6,3.9,4.5,4.2).curveTo(3.1,8.6,0.2,13.1).curveTo(-1.5,16.8,-0,23).lineTo(1.1,26.8).lineTo(2.2,30.6).curveTo(0,29.7,-2,27.7).closePath();
	this.shape.setTransform(15,71);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#EACFB7").beginStroke().moveTo(-56,31.3).lineTo(-57.5,31.1).curveTo(-58.6,31,-59.6,30.7).lineTo(-61,30.2).lineTo(-62.1,26.4).lineTo(-63.2,22.6).curveTo(-64.7,16.4,-63,12.7).curveTo(-60.1,8.2,-58.7,3.8).curveTo(-58.6,3.5,-57.3,-12.2).curveTo(-56.1,-26,-55.4,-30.2).curveTo(-54.3,-30,-52.9,-29.5).lineTo(-51,-28.7).curveTo(-48.3,-27.7,-43.7,-24.2).lineTo(-42.5,-23.3).lineTo(-43.3,-21.4).curveTo(-45.9,-15,-48.2,-4.1).curveTo(-50.6,7.1,-50.3,9.1).lineTo(-50.3,9.3).curveTo(-50.1,10.1,-47.4,15).lineTo(-46.5,16.8).curveTo(-43.2,23,-42.9,24.1).lineTo(-42.8,24.2).curveTo(-42.3,26.3,-43.8,28.1).lineTo(-44.5,28.8).curveTo(-47.2,31.3,-53.5,31.3).lineTo(-56,31.3).closePath().moveTo(48.8,5.9).curveTo(49,5.7,48.5,4).curveTo(47.9,2.2,47.5,2.1).lineTo(47.4,2.2).lineTo(47.3,2.1).lineTo(47.2,1.3).lineTo(47.3,-1.6).curveTo(47.4,-4.1,47.2,-6).lineTo(46.7,-10).curveTo(46.5,-11.4,46.6,-11.6).curveTo(48.2,-14.8,48.9,-16.7).curveTo(50.1,-19.5,50.5,-22.2).curveTo(51.3,-27.1,51.4,-29.7).lineTo(51.5,-31.2).lineTo(52.6,-31.3).lineTo(53.9,-31.1).lineTo(54,-29.9).curveTo(54.1,-26,55.3,-17.2).curveTo(56.2,-11.4,56.3,-9.1).curveTo(56.4,-7.7,56.2,-6.4).curveTo(56.2,-2.5,57.8,1.1).curveTo(58.4,2.6,60.9,6.9).curveTo(65.1,13.6,63.3,20.2).curveTo(62.2,24.5,56.3,24.5).curveTo(52.9,24.5,52.3,24.6).lineTo(51.4,24.8).curveTo(50.5,12.4,48.8,5.9).closePath();
	this.shape_1.setTransform(78.2,71.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-50.1,37.9).curveTo(-52.5,37.8,-54.6,37.5).curveTo(-63.7,35.3,-66.1,33.4).curveTo(-70.4,29.9,-71.6,22.3).curveTo(-72.3,17.4,-71.5,14.5).curveTo(-70.9,12.8,-68.9,9.9).curveTo(-66,5.9,-64.3,0.6).curveTo(-63.6,0.8,-62.9,0.8).curveTo(-61.3,0.8,-59.9,-0.2).curveTo(-61.3,5.9,-64.1,11).curveTo(-65.2,13,-66.9,15.7).curveTo(-68,17.8,-67.7,19).curveTo(-66.2,25.5,-62,29.6).curveTo(-59.9,31.6,-57.8,32.5).lineTo(-56.4,33).curveTo(-55.4,33.3,-54.3,33.4).lineTo(-52.7,33.6).curveTo(-44.4,34.1,-41.2,31.1).lineTo(-40.5,30.4).curveTo(-39.1,28.6,-39.6,26.5).lineTo(-39.6,26.4).curveTo(-40,25.3,-43.3,19.1).lineTo(-44.2,17.3).curveTo(-46.8,12.4,-47,11.6).lineTo(-47,11.4).curveTo(-47.3,9.4,-45,-1.8).curveTo(-42.6,-12.7,-40.1,-19.1).lineTo(-39.3,-21).lineTo(-40.5,-21.9).curveTo(-45,-25.4,-47.7,-26.4).lineTo(-49.6,-27.2).curveTo(-51,-27.7,-52.2,-27.9).curveTo(-52.6,-27.9,-53.5,-28.3).lineTo(-54.7,-28.7).lineTo(-54.7,-28.5).lineTo(-54.9,-33.7).lineTo(-54,-33.3).lineTo(-48.4,-31.3).lineTo(-44.9,-29.9).curveTo(-43.8,-29.3,-42.7,-28.2).lineTo(-38.6,-24.2).lineTo(-38.5,-23.9).lineTo(-37,-24.2).lineTo(-36,-25.6).curveTo(-34.5,-27.7,-33.5,-27).curveTo(-32.8,-26.5,-32.5,-24.7).lineTo(-32.4,-24).lineTo(-32.3,-23.1).lineTo(-32.3,-22.9).lineTo(-32.3,-21.9).curveTo(-32.3,-18.9,-32.8,-16.3).lineTo(-34.2,-8.1).curveTo(-35.1,-2.2,-36.4,1.6).curveTo(-37.1,3.5,-37.6,4.3).curveTo(-33.1,5.2,-28.1,6).lineTo(-27.5,11.3).curveTo(-36.2,9.9,-39.6,7.7).curveTo(-40.2,9.4,-41.8,12.6).curveTo(-40.9,14.5,-38.9,18.1).curveTo(-38,19.6,-36.9,23).curveTo(-35.3,27.9,-35.9,30.1).curveTo(-36.4,32,-38.8,34.1).curveTo(-40.4,35.6,-41.5,36).lineTo(-42.4,36.6).curveTo(-45.4,37.9,-49.1,37.9).lineTo(-50.1,37.9).closePath().moveTo(-40.2,-4.4).curveTo(-42.6,1.1,-43,5.5).curveTo(-41.9,4.2,-41.1,3.7).lineTo(-41.3,3.3).curveTo(-41.4,3,-41.4,1.8).curveTo(-41.4,0.9,-40.4,-2.6).lineTo(-39.6,-5.9).lineTo(-40.2,-4.4).closePath().moveTo(55.6,31.5).lineTo(49.9,31.6).lineTo(49.9,31.5).lineTo(48.5,9.1).curveTo(46.7,9.2,44.2,9).lineTo(43.7,4.1).lineTo(46.8,4.2).curveTo(46.1,1.5,45.9,-1.8).lineTo(45.6,-5).curveTo(45.5,-6.1,45.7,-7.3).lineTo(44.4,-7.1).lineTo(43.4,-7).curveTo(43.5,-9.4,43.8,-11.6).lineTo(44.7,-12.4).curveTo(48.5,-15.8,49.5,-21.5).curveTo(50,-24,49.8,-26.5).curveTo(51.2,-28,52.9,-29.4).lineTo(53.2,-29.5).lineTo(53.5,-29.5).lineTo(55.4,-31.7).lineTo(56.2,-32.1).curveTo(57.6,-32.8,58.7,-33.2).lineTo(58,-34.6).lineTo(59.1,-35.6).lineTo(61.4,-37.9).curveTo(62,-35.7,62,-33.4).lineTo(61.9,-32.7).lineTo(62,-32.2).lineTo(61.8,-31.6).lineTo(61.8,-31.5).lineTo(61.5,-31.2).lineTo(61.4,-31.1).lineTo(60.4,-30.7).curveTo(59.6,-30.4,59.8,-29.9).lineTo(60.5,-28).lineTo(61.6,-27.7).curveTo(62.2,-22.2,63.2,-16.6).curveTo(64.1,-11.6,64.2,-6.4).curveTo(64.4,0.4,66.1,4.5).curveTo(67,6.6,69.5,9.8).curveTo(71.4,12.4,71.8,14.8).curveTo(72.3,18.1,70.7,24.3).curveTo(69.8,27.5,66.2,29.8).curveTo(62.9,31.8,58.8,31.8).curveTo(57.3,31.8,55.6,31.5).closePath().moveTo(51.7,6.3).curveTo(52.3,8,52.1,8.2).curveTo(53.7,14.7,54.6,27.1).lineTo(55.5,26.9).curveTo(56.1,26.8,59.6,26.8).curveTo(65.4,26.8,66.6,22.5).curveTo(68.3,15.9,64.2,9.2).curveTo(61.7,4.9,61.1,3.4).curveTo(59.5,-0.2,59.5,-4.1).curveTo(59.6,-5.4,59.6,-6.8).curveTo(59.5,-9.1,58.6,-14.9).curveTo(57.3,-23.7,57.2,-27.6).lineTo(57.2,-28.8).lineTo(55.9,-29).lineTo(54.7,-28.9).lineTo(54.7,-27.4).curveTo(54.5,-24.8,53.7,-19.9).curveTo(53.3,-17.2,52.2,-14.4).curveTo(51.5,-12.5,49.8,-9.3).curveTo(49.7,-9.1,49.9,-7.7).lineTo(50.4,-3.7).curveTo(50.6,-1.8,50.5,0.7).lineTo(50.5,3.6).lineTo(50.6,4.4).lineTo(50.6,4.5).lineTo(50.7,4.4).curveTo(51.1,4.5,51.7,6.3).closePath();
	this.shape_2.setTransform(74.9,69.1);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,4.2,143.9,163.8);


(lib.FemaleHoodiePeppa = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#CC6600").beginStroke().moveTo(-11.3,3.4).curveTo(-13.8,0.2,-13.8,-2.7).curveTo(-13.8,-5.1,-13.7,-5.9).curveTo(-13.2,-8,-11.6,-8.9).curveTo(5.1,-9,10.9,-5.6).curveTo(13.8,-3.9,13.8,-0.6).curveTo(13.8,1.5,10.7,4.8).curveTo(6.8,8.9,2,8.9).curveTo(-7.1,8.9,-11.3,3.4).closePath();
	this.shape.setTransform(86.4,21.8);

	// Layer 2
	this.instance = new lib.Girl02();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#BA986F").beginStroke().moveTo(-2,27.7).curveTo(-6.2,23.6,-7.7,17.1).curveTo(-8,15.9,-6.9,13.8).curveTo(-5.2,11.1,-4.2,9.1).curveTo(-1.4,4,0,-2.1).curveTo(4.1,-5.2,5.1,-17.8).curveTo(5.5,-23.1,5.3,-29.7).lineTo(5.3,-30.4).lineTo(5.3,-30.6).lineTo(6.5,-30.2).curveTo(7.3,-29.8,7.8,-29.8).curveTo(7.1,-25.6,5.9,-11.8).curveTo(4.6,3.9,4.5,4.2).curveTo(3.1,8.6,0.2,13.1).curveTo(-1.5,16.8,-0,23).lineTo(1.1,26.8).lineTo(2.2,30.6).curveTo(0,29.7,-2,27.7).closePath();
	this.shape_1.setTransform(15,71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#D5AE81").beginStroke().moveTo(-56,31.3).lineTo(-57.5,31.1).curveTo(-58.6,31,-59.6,30.7).lineTo(-61,30.2).lineTo(-62.1,26.4).lineTo(-63.2,22.6).curveTo(-64.7,16.4,-63,12.7).curveTo(-60.1,8.2,-58.7,3.8).curveTo(-58.6,3.5,-57.3,-12.2).curveTo(-56.1,-26,-55.4,-30.2).curveTo(-54.3,-30,-52.9,-29.5).lineTo(-51,-28.7).curveTo(-48.3,-27.7,-43.7,-24.2).lineTo(-42.5,-23.3).lineTo(-43.3,-21.4).curveTo(-45.9,-15,-48.2,-4.1).curveTo(-50.6,7.1,-50.3,9.1).lineTo(-50.3,9.3).curveTo(-50.1,10.1,-47.4,15).lineTo(-46.5,16.8).curveTo(-43.2,23,-42.9,24.1).lineTo(-42.8,24.2).curveTo(-42.3,26.3,-43.8,28.1).lineTo(-44.5,28.8).curveTo(-47.2,31.3,-53.5,31.3).lineTo(-56,31.3).closePath().moveTo(48.8,5.9).curveTo(49,5.7,48.5,4).curveTo(47.9,2.2,47.5,2.1).lineTo(47.4,2.2).lineTo(47.3,2.1).lineTo(47.2,1.3).lineTo(47.3,-1.6).curveTo(47.4,-4.1,47.2,-6).lineTo(46.7,-10).curveTo(46.5,-11.4,46.6,-11.6).curveTo(48.2,-14.8,48.9,-16.7).curveTo(50.1,-19.5,50.5,-22.2).curveTo(51.3,-27.1,51.4,-29.7).lineTo(51.5,-31.2).lineTo(52.6,-31.3).lineTo(53.9,-31.1).lineTo(54,-29.9).curveTo(54.1,-26,55.3,-17.2).curveTo(56.2,-11.4,56.3,-9.1).curveTo(56.4,-7.7,56.2,-6.4).curveTo(56.2,-2.5,57.8,1.1).curveTo(58.4,2.6,60.9,6.9).curveTo(65.1,13.6,63.3,20.2).curveTo(62.2,24.5,56.3,24.5).curveTo(52.9,24.5,52.3,24.6).lineTo(51.4,24.8).curveTo(50.5,12.4,48.8,5.9).closePath();
	this.shape_2.setTransform(78.2,71.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-50.1,37.9).curveTo(-52.5,37.8,-54.6,37.5).curveTo(-63.7,35.3,-66.1,33.4).curveTo(-70.4,29.9,-71.6,22.3).curveTo(-72.3,17.4,-71.5,14.5).curveTo(-70.9,12.8,-68.9,9.9).curveTo(-66,5.9,-64.3,0.6).curveTo(-63.6,0.8,-62.9,0.8).curveTo(-61.3,0.8,-59.9,-0.2).curveTo(-61.3,5.9,-64.1,11).curveTo(-65.2,13,-66.9,15.7).curveTo(-68,17.8,-67.7,19).curveTo(-66.2,25.5,-62,29.6).curveTo(-59.9,31.6,-57.8,32.5).lineTo(-56.4,33).curveTo(-55.4,33.3,-54.3,33.4).lineTo(-52.7,33.6).curveTo(-44.4,34.1,-41.2,31.1).lineTo(-40.5,30.4).curveTo(-39.1,28.6,-39.6,26.5).lineTo(-39.6,26.4).curveTo(-40,25.3,-43.3,19.1).lineTo(-44.2,17.3).curveTo(-46.8,12.4,-47,11.6).lineTo(-47,11.4).curveTo(-47.3,9.4,-45,-1.8).curveTo(-42.6,-12.7,-40.1,-19.1).lineTo(-39.3,-21).lineTo(-40.5,-21.9).curveTo(-45,-25.4,-47.7,-26.4).lineTo(-49.6,-27.2).curveTo(-51,-27.7,-52.2,-27.9).curveTo(-52.6,-27.9,-53.5,-28.3).lineTo(-54.7,-28.7).lineTo(-54.7,-28.5).lineTo(-54.9,-33.7).lineTo(-54,-33.3).lineTo(-48.4,-31.3).lineTo(-44.9,-29.9).curveTo(-43.8,-29.3,-42.7,-28.2).lineTo(-38.6,-24.2).lineTo(-38.5,-23.9).lineTo(-37,-24.2).lineTo(-36,-25.6).curveTo(-34.5,-27.7,-33.5,-27).curveTo(-32.8,-26.5,-32.5,-24.7).lineTo(-32.4,-24).lineTo(-32.3,-23.1).lineTo(-32.3,-22.9).lineTo(-32.3,-21.9).curveTo(-32.3,-18.9,-32.8,-16.3).lineTo(-34.2,-8.1).curveTo(-35.1,-2.2,-36.4,1.6).curveTo(-37.1,3.5,-37.6,4.3).curveTo(-33.1,5.2,-28.1,6).lineTo(-27.5,11.3).curveTo(-36.2,9.9,-39.6,7.7).curveTo(-40.2,9.4,-41.8,12.6).curveTo(-40.9,14.5,-38.9,18.1).curveTo(-38,19.6,-36.9,23).curveTo(-35.3,27.9,-35.9,30.1).curveTo(-36.4,32,-38.8,34.1).curveTo(-40.4,35.6,-41.5,36).lineTo(-42.4,36.6).curveTo(-45.4,37.9,-49.1,37.9).lineTo(-50.1,37.9).closePath().moveTo(-40.2,-4.4).curveTo(-42.6,1.1,-43,5.5).curveTo(-41.9,4.2,-41.1,3.7).lineTo(-41.3,3.3).curveTo(-41.4,3,-41.4,1.8).curveTo(-41.4,0.9,-40.4,-2.6).lineTo(-39.6,-5.9).lineTo(-40.2,-4.4).closePath().moveTo(55.6,31.5).lineTo(49.9,31.6).lineTo(49.9,31.5).lineTo(48.5,9.1).curveTo(46.7,9.2,44.2,9).lineTo(43.7,4.1).lineTo(46.8,4.2).curveTo(46.1,1.5,45.9,-1.8).lineTo(45.6,-5).curveTo(45.5,-6.1,45.7,-7.3).lineTo(44.4,-7.1).lineTo(43.4,-7).curveTo(43.5,-9.4,43.8,-11.6).lineTo(44.7,-12.4).curveTo(48.5,-15.8,49.5,-21.5).curveTo(50,-24,49.8,-26.5).curveTo(51.2,-28,52.9,-29.4).lineTo(53.2,-29.5).lineTo(53.5,-29.5).lineTo(55.4,-31.7).lineTo(56.2,-32.1).curveTo(57.6,-32.8,58.7,-33.2).lineTo(58,-34.6).lineTo(59.1,-35.6).lineTo(61.4,-37.9).curveTo(62,-35.7,62,-33.4).lineTo(61.9,-32.7).lineTo(62,-32.2).lineTo(61.8,-31.6).lineTo(61.8,-31.5).lineTo(61.5,-31.2).lineTo(61.4,-31.1).lineTo(60.4,-30.7).curveTo(59.6,-30.4,59.8,-29.9).lineTo(60.5,-28).lineTo(61.6,-27.7).curveTo(62.2,-22.2,63.2,-16.6).curveTo(64.1,-11.6,64.2,-6.4).curveTo(64.4,0.4,66.1,4.5).curveTo(67,6.6,69.5,9.8).curveTo(71.4,12.4,71.8,14.8).curveTo(72.3,18.1,70.7,24.3).curveTo(69.8,27.5,66.2,29.8).curveTo(62.9,31.8,58.8,31.8).curveTo(57.3,31.8,55.6,31.5).closePath().moveTo(51.7,6.3).curveTo(52.3,8,52.1,8.2).curveTo(53.7,14.7,54.6,27.1).lineTo(55.5,26.9).curveTo(56.1,26.8,59.6,26.8).curveTo(65.4,26.8,66.6,22.5).curveTo(68.3,15.9,64.2,9.2).curveTo(61.7,4.9,61.1,3.4).curveTo(59.5,-0.2,59.5,-4.1).curveTo(59.6,-5.4,59.6,-6.8).curveTo(59.5,-9.1,58.6,-14.9).curveTo(57.3,-23.7,57.2,-27.6).lineTo(57.2,-28.8).lineTo(55.9,-29).lineTo(54.7,-28.9).lineTo(54.7,-27.4).curveTo(54.5,-24.8,53.7,-19.9).curveTo(53.3,-17.2,52.2,-14.4).curveTo(51.5,-12.5,49.8,-9.3).curveTo(49.7,-9.1,49.9,-7.7).lineTo(50.4,-3.7).curveTo(50.6,-1.8,50.5,0.7).lineTo(50.5,3.6).lineTo(50.6,4.4).lineTo(50.6,4.5).lineTo(50.7,4.4).curveTo(51.1,4.5,51.7,6.3).closePath();
	this.shape_3.setTransform(74.9,69.1);

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#BA986F").beginStroke().moveTo(18.4,14.3).curveTo(16.2,12.8,15.6,8.8).curveTo(15.4,7.2,15.4,2.2).curveTo(15.4,-1.4,15.6,-2.4).curveTo(15.9,-3.3,16.9,-4.2).curveTo(19.7,-6.6,22.6,-10.9).lineTo(20.4,14.3).closePath().moveTo(-17.8,1.1).curveTo(-20.2,-8.6,-22.6,-13.2).curveTo(-18,-14,-14.5,-14.3).lineTo(-7.8,13.7).lineTo(-8.6,13.8).curveTo(-15,9.6,-17.8,1.1).closePath();
	this.shape_4.setTransform(72.4,141.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#D5AE81").beginStroke().moveTo(8.6,14.8).lineTo(10.8,-10.4).curveTo(12.1,-12.4,13.4,-14.7).curveTo(19.7,-14.6,22.3,-14.4).lineTo(26.3,-13.8).curveTo(25.5,-11.8,22.7,0.4).curveTo(20.8,8.7,15.8,14.8).closePath().moveTo(-26.3,-13.8).curveTo(-21.7,-14.1,-19.1,-13.3).lineTo(-15.4,-11.9).curveTo(-13.1,-11,-10.4,-10.6).lineTo(-10.4,1.9).curveTo(-11.2,3.8,-11.6,8.5).curveTo(-12.4,12.5,-16.2,14.1).lineTo(-19.6,14.2).closePath();
	this.shape_5.setTransform(84.2,140.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#000000").beginStroke().moveTo(3.7,12.4).curveTo(3.1,9.6,3.2,5.8).lineTo(3.5,2.3).lineTo(3.5,-5.7).curveTo(4,-7.7,6.1,-9.2).lineTo(10.3,-12.2).curveTo(11,-12.8,12.6,-16.4).curveTo(13.8,-19.1,15.4,-19.2).lineTo(15.5,-19.2).curveTo(31.4,-19.9,34.4,-17.2).curveTo(35.5,-16.3,35,-14.8).curveTo(34.8,-14,34.2,-12.7).lineTo(32.4,-4.4).curveTo(31.2,1.3,29.8,4.9).curveTo(25.5,15.9,15.1,18.8).curveTo(13.4,19.2,12,19.3).lineTo(11.2,19.3).curveTo(5.3,19.3,3.7,12.4).closePath().moveTo(14.6,-10.7).curveTo(11.7,-6.4,8.9,-4).curveTo(7.9,-3.1,7.7,-2.2).curveTo(7.4,-1.2,7.4,2.4).curveTo(7.4,7.4,7.7,9).curveTo(8.2,13,10.4,14.5).lineTo(12.5,14.5).lineTo(19.6,14.5).curveTo(24.6,8.5,26.5,0.1).curveTo(29.3,-12,30.2,-14.1).lineTo(26.2,-14.6).curveTo(23.6,-14.9,17.3,-15).curveTo(15.9,-12.7,14.6,-10.7).closePath().moveTo(-14.7,18.1).curveTo(-18.8,18.1,-21.1,16.8).curveTo(-23.4,15.5,-25.1,15.5).curveTo(-26.1,15.5,-26.5,15.2).lineTo(-27.2,14.6).curveTo(-27.2,11.6,-31.1,0.5).curveTo(-35.1,-10.6,-35.1,-14.4).curveTo(-35.1,-15.8,-33.7,-16.3).curveTo(-32.5,-16.7,-25.1,-18.1).lineTo(-23.4,-18).curveTo(-7.9,-17.6,-3.9,-13.1).curveTo(-2.5,-11.4,-2.4,-8.9).curveTo(-2.3,-7.5,-2.6,-5.1).curveTo(-2.6,18.1,-14.5,18.1).lineTo(-14.7,18.1).closePath().moveTo(-22.5,-14.1).curveTo(-25.9,-13.8,-30.5,-13).curveTo(-28.2,-8.4,-25.8,1.3).curveTo(-22.9,9.8,-16.5,14).lineTo(-15.7,13.9).lineTo(-12.3,13.8).curveTo(-8.6,12.2,-7.7,8.2).curveTo(-7.3,3.6,-6.5,1.6).lineTo(-6.5,-10.9).curveTo(-9.3,-11.2,-11.6,-12.2).lineTo(-15.2,-13.6).curveTo(-17.1,-14.2,-20,-14.2).lineTo(-22.5,-14.1).closePath();
	this.shape_6.setTransform(80.4,140.9);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,-2.6,143.9,170.6);


(lib.FemaleHoodieJane = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#CC6600").beginStroke().moveTo(-11.3,3.4).curveTo(-13.8,0.2,-13.8,-2.7).curveTo(-13.8,-5.1,-13.7,-5.9).curveTo(-13.2,-8,-11.6,-8.9).curveTo(5.1,-9,10.9,-5.6).curveTo(13.8,-3.9,13.8,-0.6).curveTo(13.8,1.5,10.7,4.8).curveTo(6.8,8.9,2,8.9).curveTo(-7.1,8.9,-11.3,3.4).closePath();
	this.shape.setTransform(86.4,21.8);

	// Layer 2
	this.instance = new lib.Girl02();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#7A4D30").beginStroke().moveTo(-2,27.7).curveTo(-6.2,23.6,-7.7,17.1).curveTo(-8,15.9,-6.9,13.8).curveTo(-5.2,11.1,-4.2,9.1).curveTo(-1.4,4,0,-2.1).curveTo(4.1,-5.2,5.1,-17.8).curveTo(5.5,-23.1,5.3,-29.7).lineTo(5.3,-30.4).lineTo(5.3,-30.6).lineTo(6.5,-30.2).curveTo(7.3,-29.8,7.8,-29.8).curveTo(7.1,-25.6,5.9,-11.8).curveTo(4.6,3.9,4.5,4.2).curveTo(3.1,8.6,0.2,13.1).curveTo(-1.5,16.8,-0,23).lineTo(1.1,26.8).lineTo(2.2,30.6).curveTo(0,29.7,-2,27.7).closePath();
	this.shape_1.setTransform(15,71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#975F3C").beginStroke().moveTo(-56,31.3).lineTo(-57.5,31.1).curveTo(-58.6,31,-59.6,30.7).lineTo(-61,30.2).lineTo(-62.1,26.4).lineTo(-63.2,22.6).curveTo(-64.7,16.4,-63,12.7).curveTo(-60.1,8.2,-58.7,3.8).curveTo(-58.6,3.5,-57.3,-12.2).curveTo(-56.1,-26,-55.4,-30.2).curveTo(-54.3,-30,-52.9,-29.5).lineTo(-51,-28.7).curveTo(-48.3,-27.7,-43.7,-24.2).lineTo(-42.5,-23.3).lineTo(-43.3,-21.4).curveTo(-45.9,-15,-48.2,-4.1).curveTo(-50.6,7.1,-50.3,9.1).lineTo(-50.3,9.3).curveTo(-50.1,10.1,-47.4,15).lineTo(-46.5,16.8).curveTo(-43.2,23,-42.9,24.1).lineTo(-42.8,24.2).curveTo(-42.3,26.3,-43.8,28.1).lineTo(-44.5,28.8).curveTo(-47.2,31.3,-53.5,31.3).lineTo(-56,31.3).closePath().moveTo(48.8,5.9).curveTo(49,5.7,48.5,4).curveTo(47.9,2.2,47.5,2.1).lineTo(47.4,2.2).lineTo(47.3,2.1).lineTo(47.2,1.3).lineTo(47.3,-1.6).curveTo(47.4,-4.1,47.2,-6).lineTo(46.7,-10).curveTo(46.5,-11.4,46.6,-11.6).curveTo(48.2,-14.8,48.9,-16.7).curveTo(50.1,-19.5,50.5,-22.2).curveTo(51.3,-27.1,51.4,-29.7).lineTo(51.5,-31.2).lineTo(52.6,-31.3).lineTo(53.9,-31.1).lineTo(54,-29.9).curveTo(54.1,-26,55.3,-17.2).curveTo(56.2,-11.4,56.3,-9.1).curveTo(56.4,-7.7,56.2,-6.4).curveTo(56.2,-2.5,57.8,1.1).curveTo(58.4,2.6,60.9,6.9).curveTo(65.1,13.6,63.3,20.2).curveTo(62.2,24.5,56.3,24.5).curveTo(52.9,24.5,52.3,24.6).lineTo(51.4,24.8).curveTo(50.5,12.4,48.8,5.9).closePath();
	this.shape_2.setTransform(78.2,71.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-50.1,37.9).curveTo(-52.5,37.8,-54.6,37.5).curveTo(-63.7,35.3,-66.1,33.4).curveTo(-70.4,29.9,-71.6,22.3).curveTo(-72.3,17.4,-71.5,14.5).curveTo(-70.9,12.8,-68.9,9.9).curveTo(-66,5.9,-64.3,0.6).curveTo(-63.6,0.8,-62.9,0.8).curveTo(-61.3,0.8,-59.9,-0.2).curveTo(-61.3,5.9,-64.1,11).curveTo(-65.2,13,-66.9,15.7).curveTo(-68,17.8,-67.7,19).curveTo(-66.2,25.5,-62,29.6).curveTo(-59.9,31.6,-57.8,32.5).lineTo(-56.4,33).curveTo(-55.4,33.3,-54.3,33.4).lineTo(-52.7,33.6).curveTo(-44.4,34.1,-41.2,31.1).lineTo(-40.5,30.4).curveTo(-39.1,28.6,-39.6,26.5).lineTo(-39.6,26.4).curveTo(-40,25.3,-43.3,19.1).lineTo(-44.2,17.3).curveTo(-46.8,12.4,-47,11.6).lineTo(-47,11.4).curveTo(-47.3,9.4,-45,-1.8).curveTo(-42.6,-12.7,-40.1,-19.1).lineTo(-39.3,-21).lineTo(-40.5,-21.9).curveTo(-45,-25.4,-47.7,-26.4).lineTo(-49.6,-27.2).curveTo(-51,-27.7,-52.2,-27.9).curveTo(-52.6,-27.9,-53.5,-28.3).lineTo(-54.7,-28.7).lineTo(-54.7,-28.5).lineTo(-54.9,-33.7).lineTo(-54,-33.3).lineTo(-48.4,-31.3).lineTo(-44.9,-29.9).curveTo(-43.8,-29.3,-42.7,-28.2).lineTo(-38.6,-24.2).lineTo(-38.5,-23.9).lineTo(-37,-24.2).lineTo(-36,-25.6).curveTo(-34.5,-27.7,-33.5,-27).curveTo(-32.8,-26.5,-32.5,-24.7).lineTo(-32.4,-24).lineTo(-32.3,-23.1).lineTo(-32.3,-22.9).lineTo(-32.3,-21.9).curveTo(-32.3,-18.9,-32.8,-16.3).lineTo(-34.2,-8.1).curveTo(-35.1,-2.2,-36.4,1.6).curveTo(-37.1,3.5,-37.6,4.3).curveTo(-33.1,5.2,-28.1,6).lineTo(-27.5,11.3).curveTo(-36.2,9.9,-39.6,7.7).curveTo(-40.2,9.4,-41.8,12.6).curveTo(-40.9,14.5,-38.9,18.1).curveTo(-38,19.6,-36.9,23).curveTo(-35.3,27.9,-35.9,30.1).curveTo(-36.4,32,-38.8,34.1).curveTo(-40.4,35.6,-41.5,36).lineTo(-42.4,36.6).curveTo(-45.4,37.9,-49.1,37.9).lineTo(-50.1,37.9).closePath().moveTo(-40.2,-4.4).curveTo(-42.6,1.1,-43,5.5).curveTo(-41.9,4.2,-41.1,3.7).lineTo(-41.3,3.3).curveTo(-41.4,3,-41.4,1.8).curveTo(-41.4,0.9,-40.4,-2.6).lineTo(-39.6,-5.9).lineTo(-40.2,-4.4).closePath().moveTo(55.6,31.5).lineTo(49.9,31.6).lineTo(49.9,31.5).lineTo(48.5,9.1).curveTo(46.7,9.2,44.2,9).lineTo(43.7,4.1).lineTo(46.8,4.2).curveTo(46.1,1.5,45.9,-1.8).lineTo(45.6,-5).curveTo(45.5,-6.1,45.7,-7.3).lineTo(44.4,-7.1).lineTo(43.4,-7).curveTo(43.5,-9.4,43.8,-11.6).lineTo(44.7,-12.4).curveTo(48.5,-15.8,49.5,-21.5).curveTo(50,-24,49.8,-26.5).curveTo(51.2,-28,52.9,-29.4).lineTo(53.2,-29.5).lineTo(53.5,-29.5).lineTo(55.4,-31.7).lineTo(56.2,-32.1).curveTo(57.6,-32.8,58.7,-33.2).lineTo(58,-34.6).lineTo(59.1,-35.6).lineTo(61.4,-37.9).curveTo(62,-35.7,62,-33.4).lineTo(61.9,-32.7).lineTo(62,-32.2).lineTo(61.8,-31.6).lineTo(61.8,-31.5).lineTo(61.5,-31.2).lineTo(61.4,-31.1).lineTo(60.4,-30.7).curveTo(59.6,-30.4,59.8,-29.9).lineTo(60.5,-28).lineTo(61.6,-27.7).curveTo(62.2,-22.2,63.2,-16.6).curveTo(64.1,-11.6,64.2,-6.4).curveTo(64.4,0.4,66.1,4.5).curveTo(67,6.6,69.5,9.8).curveTo(71.4,12.4,71.8,14.8).curveTo(72.3,18.1,70.7,24.3).curveTo(69.8,27.5,66.2,29.8).curveTo(62.9,31.8,58.8,31.8).curveTo(57.3,31.8,55.6,31.5).closePath().moveTo(51.7,6.3).curveTo(52.3,8,52.1,8.2).curveTo(53.7,14.7,54.6,27.1).lineTo(55.5,26.9).curveTo(56.1,26.8,59.6,26.8).curveTo(65.4,26.8,66.6,22.5).curveTo(68.3,15.9,64.2,9.2).curveTo(61.7,4.9,61.1,3.4).curveTo(59.5,-0.2,59.5,-4.1).curveTo(59.6,-5.4,59.6,-6.8).curveTo(59.5,-9.1,58.6,-14.9).curveTo(57.3,-23.7,57.2,-27.6).lineTo(57.2,-28.8).lineTo(55.9,-29).lineTo(54.7,-28.9).lineTo(54.7,-27.4).curveTo(54.5,-24.8,53.7,-19.9).curveTo(53.3,-17.2,52.2,-14.4).curveTo(51.5,-12.5,49.8,-9.3).curveTo(49.7,-9.1,49.9,-7.7).lineTo(50.4,-3.7).curveTo(50.6,-1.8,50.5,0.7).lineTo(50.5,3.6).lineTo(50.6,4.4).lineTo(50.6,4.5).lineTo(50.7,4.4).curveTo(51.1,4.5,51.7,6.3).closePath();
	this.shape_3.setTransform(74.9,69.1);

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#7A4D30").beginStroke().moveTo(18.4,14.3).curveTo(16.2,12.8,15.6,8.8).curveTo(15.4,7.2,15.4,2.2).curveTo(15.4,-1.4,15.6,-2.4).curveTo(15.9,-3.3,16.9,-4.2).curveTo(19.7,-6.6,22.6,-10.9).lineTo(20.4,14.3).closePath().moveTo(-17.8,1.1).curveTo(-20.2,-8.6,-22.6,-13.2).curveTo(-18,-14,-14.5,-14.3).lineTo(-7.8,13.7).lineTo(-8.6,13.8).curveTo(-15,9.6,-17.8,1.1).closePath();
	this.shape_4.setTransform(72.4,141.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#975F3C").beginStroke().moveTo(8.6,14.8).lineTo(10.8,-10.4).curveTo(12.1,-12.4,13.4,-14.7).curveTo(19.7,-14.6,22.3,-14.4).lineTo(26.3,-13.8).curveTo(25.5,-11.8,22.7,0.4).curveTo(20.8,8.7,15.8,14.8).closePath().moveTo(-26.3,-13.8).curveTo(-21.7,-14.1,-19.1,-13.3).lineTo(-15.4,-11.9).curveTo(-13.1,-11,-10.4,-10.6).lineTo(-10.4,1.9).curveTo(-11.2,3.8,-11.6,8.5).curveTo(-12.4,12.5,-16.2,14.1).lineTo(-19.6,14.2).closePath();
	this.shape_5.setTransform(84.2,140.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#000000").beginStroke().moveTo(3.7,12.4).curveTo(3.1,9.6,3.2,5.8).lineTo(3.5,2.3).lineTo(3.5,-5.7).curveTo(4,-7.7,6.1,-9.2).lineTo(10.3,-12.2).curveTo(11,-12.8,12.6,-16.4).curveTo(13.8,-19.1,15.4,-19.2).lineTo(15.5,-19.2).curveTo(31.4,-19.9,34.4,-17.2).curveTo(35.5,-16.3,35,-14.8).curveTo(34.8,-14,34.2,-12.7).lineTo(32.4,-4.4).curveTo(31.2,1.3,29.8,4.9).curveTo(25.5,15.9,15.1,18.8).curveTo(13.4,19.2,12,19.3).lineTo(11.2,19.3).curveTo(5.3,19.3,3.7,12.4).closePath().moveTo(14.6,-10.7).curveTo(11.7,-6.4,8.9,-4).curveTo(7.9,-3.1,7.7,-2.2).curveTo(7.4,-1.2,7.4,2.4).curveTo(7.4,7.4,7.7,9).curveTo(8.2,13,10.4,14.5).lineTo(12.5,14.5).lineTo(19.6,14.5).curveTo(24.6,8.5,26.5,0.1).curveTo(29.3,-12,30.2,-14.1).lineTo(26.2,-14.6).curveTo(23.6,-14.9,17.3,-15).curveTo(15.9,-12.7,14.6,-10.7).closePath().moveTo(-14.7,18.1).curveTo(-18.8,18.1,-21.1,16.8).curveTo(-23.4,15.5,-25.1,15.5).curveTo(-26.1,15.5,-26.5,15.2).lineTo(-27.2,14.6).curveTo(-27.2,11.6,-31.1,0.5).curveTo(-35.1,-10.6,-35.1,-14.4).curveTo(-35.1,-15.8,-33.7,-16.3).curveTo(-32.5,-16.7,-25.1,-18.1).lineTo(-23.4,-18).curveTo(-7.9,-17.6,-3.9,-13.1).curveTo(-2.5,-11.4,-2.4,-8.9).curveTo(-2.3,-7.5,-2.6,-5.1).curveTo(-2.6,18.1,-14.5,18.1).lineTo(-14.7,18.1).closePath().moveTo(-22.5,-14.1).curveTo(-25.9,-13.8,-30.5,-13).curveTo(-28.2,-8.4,-25.8,1.3).curveTo(-22.9,9.8,-16.5,14).lineTo(-15.7,13.9).lineTo(-12.3,13.8).curveTo(-8.6,12.2,-7.7,8.2).curveTo(-7.3,3.6,-6.5,1.6).lineTo(-6.5,-10.9).curveTo(-9.3,-11.2,-11.6,-12.2).lineTo(-15.2,-13.6).curveTo(-17.1,-14.2,-20,-14.2).lineTo(-22.5,-14.1).closePath();
	this.shape_6.setTransform(80.4,140.9);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,-2.6,143.9,170.6);


(lib.FemaleHoodie = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#CC6600").beginStroke().moveTo(-11.3,3.4).curveTo(-13.8,0.2,-13.8,-2.7).curveTo(-13.8,-5.1,-13.7,-5.9).curveTo(-13.2,-8,-11.6,-8.9).curveTo(5.1,-9,10.9,-5.6).curveTo(13.8,-3.9,13.8,-0.6).curveTo(13.8,1.5,10.7,4.8).curveTo(6.8,8.9,2,8.9).curveTo(-7.1,8.9,-11.3,3.4).closePath();
	this.shape.setTransform(86.4,21.8);

	// Layer 2
	this.instance = new lib.Girl02();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#CCB59F").beginStroke().moveTo(-2,27.7).curveTo(-6.2,23.6,-7.7,17.1).curveTo(-8,15.9,-6.9,13.8).curveTo(-5.2,11.1,-4.2,9.1).curveTo(-1.4,4,0,-2.1).curveTo(4.1,-5.2,5.1,-17.8).curveTo(5.5,-23.1,5.3,-29.7).lineTo(5.3,-30.4).lineTo(5.3,-30.6).lineTo(6.5,-30.2).curveTo(7.3,-29.8,7.8,-29.8).curveTo(7.1,-25.6,5.9,-11.8).curveTo(4.6,3.9,4.5,4.2).curveTo(3.1,8.6,0.2,13.1).curveTo(-1.5,16.8,-0,23).lineTo(1.1,26.8).lineTo(2.2,30.6).curveTo(0,29.7,-2,27.7).closePath();
	this.shape_1.setTransform(15,71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#EACFB7").beginStroke().moveTo(-56,31.3).lineTo(-57.5,31.1).curveTo(-58.6,31,-59.6,30.7).lineTo(-61,30.2).lineTo(-62.1,26.4).lineTo(-63.2,22.6).curveTo(-64.7,16.4,-63,12.7).curveTo(-60.1,8.2,-58.7,3.8).curveTo(-58.6,3.5,-57.3,-12.2).curveTo(-56.1,-26,-55.4,-30.2).curveTo(-54.3,-30,-52.9,-29.5).lineTo(-51,-28.7).curveTo(-48.3,-27.7,-43.7,-24.2).lineTo(-42.5,-23.3).lineTo(-43.3,-21.4).curveTo(-45.9,-15,-48.2,-4.1).curveTo(-50.6,7.1,-50.3,9.1).lineTo(-50.3,9.3).curveTo(-50.1,10.1,-47.4,15).lineTo(-46.5,16.8).curveTo(-43.2,23,-42.9,24.1).lineTo(-42.8,24.2).curveTo(-42.3,26.3,-43.8,28.1).lineTo(-44.5,28.8).curveTo(-47.2,31.3,-53.5,31.3).lineTo(-56,31.3).closePath().moveTo(48.8,5.9).curveTo(49,5.7,48.5,4).curveTo(47.9,2.2,47.5,2.1).lineTo(47.4,2.2).lineTo(47.3,2.1).lineTo(47.2,1.3).lineTo(47.3,-1.6).curveTo(47.4,-4.1,47.2,-6).lineTo(46.7,-10).curveTo(46.5,-11.4,46.6,-11.6).curveTo(48.2,-14.8,48.9,-16.7).curveTo(50.1,-19.5,50.5,-22.2).curveTo(51.3,-27.1,51.4,-29.7).lineTo(51.5,-31.2).lineTo(52.6,-31.3).lineTo(53.9,-31.1).lineTo(54,-29.9).curveTo(54.1,-26,55.3,-17.2).curveTo(56.2,-11.4,56.3,-9.1).curveTo(56.4,-7.7,56.2,-6.4).curveTo(56.2,-2.5,57.8,1.1).curveTo(58.4,2.6,60.9,6.9).curveTo(65.1,13.6,63.3,20.2).curveTo(62.2,24.5,56.3,24.5).curveTo(52.9,24.5,52.3,24.6).lineTo(51.4,24.8).curveTo(50.5,12.4,48.8,5.9).closePath();
	this.shape_2.setTransform(78.2,71.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-50.1,37.9).curveTo(-52.5,37.8,-54.6,37.5).curveTo(-63.7,35.3,-66.1,33.4).curveTo(-70.4,29.9,-71.6,22.3).curveTo(-72.3,17.4,-71.5,14.5).curveTo(-70.9,12.8,-68.9,9.9).curveTo(-66,5.9,-64.3,0.6).curveTo(-63.6,0.8,-62.9,0.8).curveTo(-61.3,0.8,-59.9,-0.2).curveTo(-61.3,5.9,-64.1,11).curveTo(-65.2,13,-66.9,15.7).curveTo(-68,17.8,-67.7,19).curveTo(-66.2,25.5,-62,29.6).curveTo(-59.9,31.6,-57.8,32.5).lineTo(-56.4,33).curveTo(-55.4,33.3,-54.3,33.4).lineTo(-52.7,33.6).curveTo(-44.4,34.1,-41.2,31.1).lineTo(-40.5,30.4).curveTo(-39.1,28.6,-39.6,26.5).lineTo(-39.6,26.4).curveTo(-40,25.3,-43.3,19.1).lineTo(-44.2,17.3).curveTo(-46.8,12.4,-47,11.6).lineTo(-47,11.4).curveTo(-47.3,9.4,-45,-1.8).curveTo(-42.6,-12.7,-40.1,-19.1).lineTo(-39.3,-21).lineTo(-40.5,-21.9).curveTo(-45,-25.4,-47.7,-26.4).lineTo(-49.6,-27.2).curveTo(-51,-27.7,-52.2,-27.9).curveTo(-52.6,-27.9,-53.5,-28.3).lineTo(-54.7,-28.7).lineTo(-54.7,-28.5).lineTo(-54.9,-33.7).lineTo(-54,-33.3).lineTo(-48.4,-31.3).lineTo(-44.9,-29.9).curveTo(-43.8,-29.3,-42.7,-28.2).lineTo(-38.6,-24.2).lineTo(-38.5,-23.9).lineTo(-37,-24.2).lineTo(-36,-25.6).curveTo(-34.5,-27.7,-33.5,-27).curveTo(-32.8,-26.5,-32.5,-24.7).lineTo(-32.4,-24).lineTo(-32.3,-23.1).lineTo(-32.3,-22.9).lineTo(-32.3,-21.9).curveTo(-32.3,-18.9,-32.8,-16.3).lineTo(-34.2,-8.1).curveTo(-35.1,-2.2,-36.4,1.6).curveTo(-37.1,3.5,-37.6,4.3).curveTo(-33.1,5.2,-28.1,6).lineTo(-27.5,11.3).curveTo(-36.2,9.9,-39.6,7.7).curveTo(-40.2,9.4,-41.8,12.6).curveTo(-40.9,14.5,-38.9,18.1).curveTo(-38,19.6,-36.9,23).curveTo(-35.3,27.9,-35.9,30.1).curveTo(-36.4,32,-38.8,34.1).curveTo(-40.4,35.6,-41.5,36).lineTo(-42.4,36.6).curveTo(-45.4,37.9,-49.1,37.9).lineTo(-50.1,37.9).closePath().moveTo(-40.2,-4.4).curveTo(-42.6,1.1,-43,5.5).curveTo(-41.9,4.2,-41.1,3.7).lineTo(-41.3,3.3).curveTo(-41.4,3,-41.4,1.8).curveTo(-41.4,0.9,-40.4,-2.6).lineTo(-39.6,-5.9).lineTo(-40.2,-4.4).closePath().moveTo(55.6,31.5).lineTo(49.9,31.6).lineTo(49.9,31.5).lineTo(48.5,9.1).curveTo(46.7,9.2,44.2,9).lineTo(43.7,4.1).lineTo(46.8,4.2).curveTo(46.1,1.5,45.9,-1.8).lineTo(45.6,-5).curveTo(45.5,-6.1,45.7,-7.3).lineTo(44.4,-7.1).lineTo(43.4,-7).curveTo(43.5,-9.4,43.8,-11.6).lineTo(44.7,-12.4).curveTo(48.5,-15.8,49.5,-21.5).curveTo(50,-24,49.8,-26.5).curveTo(51.2,-28,52.9,-29.4).lineTo(53.2,-29.5).lineTo(53.5,-29.5).lineTo(55.4,-31.7).lineTo(56.2,-32.1).curveTo(57.6,-32.8,58.7,-33.2).lineTo(58,-34.6).lineTo(59.1,-35.6).lineTo(61.4,-37.9).curveTo(62,-35.7,62,-33.4).lineTo(61.9,-32.7).lineTo(62,-32.2).lineTo(61.8,-31.6).lineTo(61.8,-31.5).lineTo(61.5,-31.2).lineTo(61.4,-31.1).lineTo(60.4,-30.7).curveTo(59.6,-30.4,59.8,-29.9).lineTo(60.5,-28).lineTo(61.6,-27.7).curveTo(62.2,-22.2,63.2,-16.6).curveTo(64.1,-11.6,64.2,-6.4).curveTo(64.4,0.4,66.1,4.5).curveTo(67,6.6,69.5,9.8).curveTo(71.4,12.4,71.8,14.8).curveTo(72.3,18.1,70.7,24.3).curveTo(69.8,27.5,66.2,29.8).curveTo(62.9,31.8,58.8,31.8).curveTo(57.3,31.8,55.6,31.5).closePath().moveTo(51.7,6.3).curveTo(52.3,8,52.1,8.2).curveTo(53.7,14.7,54.6,27.1).lineTo(55.5,26.9).curveTo(56.1,26.8,59.6,26.8).curveTo(65.4,26.8,66.6,22.5).curveTo(68.3,15.9,64.2,9.2).curveTo(61.7,4.9,61.1,3.4).curveTo(59.5,-0.2,59.5,-4.1).curveTo(59.6,-5.4,59.6,-6.8).curveTo(59.5,-9.1,58.6,-14.9).curveTo(57.3,-23.7,57.2,-27.6).lineTo(57.2,-28.8).lineTo(55.9,-29).lineTo(54.7,-28.9).lineTo(54.7,-27.4).curveTo(54.5,-24.8,53.7,-19.9).curveTo(53.3,-17.2,52.2,-14.4).curveTo(51.5,-12.5,49.8,-9.3).curveTo(49.7,-9.1,49.9,-7.7).lineTo(50.4,-3.7).curveTo(50.6,-1.8,50.5,0.7).lineTo(50.5,3.6).lineTo(50.6,4.4).lineTo(50.6,4.5).lineTo(50.7,4.4).curveTo(51.1,4.5,51.7,6.3).closePath();
	this.shape_3.setTransform(74.9,69.1);

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#CCB59F").beginStroke().moveTo(18.4,14.3).curveTo(16.2,12.8,15.6,8.8).curveTo(15.4,7.2,15.4,2.2).curveTo(15.4,-1.4,15.6,-2.4).curveTo(15.9,-3.3,16.9,-4.2).curveTo(19.7,-6.6,22.6,-10.9).lineTo(20.4,14.3).closePath().moveTo(-17.8,1.1).curveTo(-20.2,-8.6,-22.6,-13.2).curveTo(-18,-14,-14.5,-14.3).lineTo(-7.8,13.7).lineTo(-8.6,13.8).curveTo(-15,9.6,-17.8,1.1).closePath();
	this.shape_4.setTransform(72.4,141.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#EACFB7").beginStroke().moveTo(8.6,14.8).lineTo(10.8,-10.4).curveTo(12.1,-12.4,13.4,-14.7).curveTo(19.7,-14.6,22.3,-14.4).lineTo(26.3,-13.8).curveTo(25.5,-11.8,22.7,0.4).curveTo(20.8,8.7,15.8,14.8).closePath().moveTo(-26.3,-13.8).curveTo(-21.7,-14.1,-19.1,-13.3).lineTo(-15.4,-11.9).curveTo(-13.1,-11,-10.4,-10.6).lineTo(-10.4,1.9).curveTo(-11.2,3.8,-11.6,8.5).curveTo(-12.4,12.5,-16.2,14.1).lineTo(-19.6,14.2).closePath();
	this.shape_5.setTransform(84.2,140.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#000000").beginStroke().moveTo(3.7,12.4).curveTo(3.1,9.6,3.2,5.8).lineTo(3.5,2.3).lineTo(3.5,-5.7).curveTo(4,-7.7,6.1,-9.2).lineTo(10.3,-12.2).curveTo(11,-12.8,12.6,-16.4).curveTo(13.8,-19.1,15.4,-19.2).lineTo(15.5,-19.2).curveTo(31.4,-19.9,34.4,-17.2).curveTo(35.5,-16.3,35,-14.8).curveTo(34.8,-14,34.2,-12.7).lineTo(32.4,-4.4).curveTo(31.2,1.3,29.8,4.9).curveTo(25.5,15.9,15.1,18.8).curveTo(13.4,19.2,12,19.3).lineTo(11.2,19.3).curveTo(5.3,19.3,3.7,12.4).closePath().moveTo(14.6,-10.7).curveTo(11.7,-6.4,8.9,-4).curveTo(7.9,-3.1,7.7,-2.2).curveTo(7.4,-1.2,7.4,2.4).curveTo(7.4,7.4,7.7,9).curveTo(8.2,13,10.4,14.5).lineTo(12.5,14.5).lineTo(19.6,14.5).curveTo(24.6,8.5,26.5,0.1).curveTo(29.3,-12,30.2,-14.1).lineTo(26.2,-14.6).curveTo(23.6,-14.9,17.3,-15).curveTo(15.9,-12.7,14.6,-10.7).closePath().moveTo(-14.7,18.1).curveTo(-18.8,18.1,-21.1,16.8).curveTo(-23.4,15.5,-25.1,15.5).curveTo(-26.1,15.5,-26.5,15.2).lineTo(-27.2,14.6).curveTo(-27.2,11.6,-31.1,0.5).curveTo(-35.1,-10.6,-35.1,-14.4).curveTo(-35.1,-15.8,-33.7,-16.3).curveTo(-32.5,-16.7,-25.1,-18.1).lineTo(-23.4,-18).curveTo(-7.9,-17.6,-3.9,-13.1).curveTo(-2.5,-11.4,-2.4,-8.9).curveTo(-2.3,-7.5,-2.6,-5.1).curveTo(-2.6,18.1,-14.5,18.1).lineTo(-14.7,18.1).closePath().moveTo(-22.5,-14.1).curveTo(-25.9,-13.8,-30.5,-13).curveTo(-28.2,-8.4,-25.8,1.3).curveTo(-22.9,9.8,-16.5,14).lineTo(-15.7,13.9).lineTo(-12.3,13.8).curveTo(-8.6,12.2,-7.7,8.2).curveTo(-7.3,3.6,-6.5,1.6).lineTo(-6.5,-10.9).curveTo(-9.3,-11.2,-11.6,-12.2).lineTo(-15.2,-13.6).curveTo(-17.1,-14.2,-20,-14.2).lineTo(-22.5,-14.1).closePath();
	this.shape_6.setTransform(80.4,140.9);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,-2.6,143.9,170.6);


(lib.DidntGetBadge = function() {
	this.initialize();

	// Layer 1
	this.mc_FinalScoreNotEnoughtPts = new lib.FinalScorecopy();
	this.mc_FinalScoreNotEnoughtPts.setTransform(608.7,193.7,1.38,1.21,0,0,0,91.1,33.5);

	this.text = new cjs.Text("FINAL SCORE", "49px 'VAGRounded BT'");
	this.text.textAlign = "center";
	this.text.lineHeight = 60;
	this.text.lineWidth = 317;
	this.text.setTransform(298.7,167.6,0.97,0.97);

	this.mc_Twitter = new lib.TW_butn();
	this.mc_Twitter.setTransform(833,424.7,1,1,0,0,0,19.2,19.2);
	new cjs.ButtonHelper(this.mc_Twitter, 0, 1, 1);

	this.mc_Facebook = new lib.FB_butn();
	this.mc_Facebook.setTransform(832.9,370,1,1,0,0,0,19.1,19.1);
	new cjs.ButtonHelper(this.mc_Facebook, 0, 1, 1);

	this.text_1 = new cjs.Text("you haven't gained enough points to earn a badge. better luck next time.", "49px 'Laffayette Comic Pro'");
	this.text_1.textAlign = "center";
	this.text_1.lineHeight = 60;
	this.text_1.lineWidth = 794;
	this.text_1.setTransform(460,312.1,0.86,0.86);

	this.instance = new lib.EndWhiteBoxcopy();
	this.instance.setTransform(482.4,288.3,1.082,1.05,0,0,0,365.2,185.9);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.addChild(this.instance,this.text_1,this.mc_Facebook,this.mc_Twitter,this.text,this.mc_FinalScoreNotEnoughtPts);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(83.1,111,799,379);


(lib.ConveyorBelt = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Conveyor_Line
	this.instance = new lib.Conveyor_Line();
	this.instance.setTransform(31,49.2,1,0.67,-3,0,0,2.3,45.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleY:0.69,rotation:-6,x:58,y:57.7},3).wait(1));

	// Conveyor_Line
	this.instance_1 = new lib.Conveyor_Line();
	this.instance_1.setTransform(58,57.7,1,0.69,-6,0,0,2.3,45.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regX:2.4,regY:45.3,scaleY:0.72,rotation:-10,x:89.1,y:66.7},3).wait(1));

	// Conveyor_Line
	this.instance_2 = new lib.Conveyor_Line();
	this.instance_2.setTransform(89.1,66.7,1,0.72,-10,0,0,2.4,45.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:2.3,scaleY:0.75,rotation:-14,x:123.5,y:77.7},3).wait(1));

	// Conveyor_Line
	this.instance_3 = new lib.Conveyor_Line();
	this.instance_3.setTransform(123.5,77.7,1,0.75,-14,0,0,2.3,45.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regY:45.2,scaleX:1,scaleY:0.8,rotation:-19,x:160,y:87.7},3).wait(1));

	// Conveyor_Line
	this.instance_4 = new lib.Conveyor_Line();
	this.instance_4.setTransform(160,87.7,0.999,0.799,-19,0,0,2.3,45.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({scaleX:1,scaleY:0.82,rotation:-22,x:197.6,y:100.3},3).wait(1));

	// Conveyor_Line
	this.instance_5 = new lib.Conveyor_Line();
	this.instance_5.setTransform(197.6,100.3,1,0.82,-22,0,0,2.3,45.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({regX:2.4,scaleY:0.85,rotation:-25,x:234.2,y:111.1},3).wait(1));

	// Conveyor_Line
	this.instance_6 = new lib.Conveyor_Line();
	this.instance_6.setTransform(234.2,111.1,1,0.85,-25,0,0,2.4,45.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({regY:45.1,scaleY:0.93,rotation:-29,x:273.6,y:122.6},3).wait(1));

	// Conveyor_Line
	this.instance_7 = new lib.Conveyor_Line();
	this.instance_7.setTransform(273.6,122.6,1,0.93,-29,0,0,2.4,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({scaleY:1,rotation:-32.2,x:317.8,y:136.8},3).wait(1));

	// Conveyor_Line
	this.instance_8 = new lib.Conveyor_Line();
	this.instance_8.setTransform(317.8,136.8,1,1,-32.2,0,0,2.4,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({x:350.2,y:147.8},3).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#333333").beginStroke().moveTo(145.2,56.2).lineTo(138.7,54.6).curveTo(113.5,48.4,63.9,31.7).curveTo(6.8,12,-19.2,3.3).lineTo(-60.3,-9.6).curveTo(-100.4,-22.3,-110.4,-26.1).curveTo(-149.5,-38.3,-161.9,-42).lineTo(-161.9,-56.8).lineTo(-101.8,-36.4).curveTo(-72.6,-25.1,-16.4,-9.6).lineTo(-9.7,-7.8).curveTo(13.6,-1.4,79.7,18.8).lineTo(95.2,23.2).curveTo(110.8,27.7,123.1,31.8).curveTo(161.9,44.5,161.9,50).curveTo(161.9,54.6,153,56.5).curveTo(151.8,56.8,150.3,56.8).curveTo(148.1,56.8,145.2,56.2).closePath();
	this.shape.setTransform(209.1,149.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#000000").beginStroke().moveTo(67.9,82).curveTo(18.4,65.7,-8.4,57).curveTo(-55.9,41.6,-89.6,31.6).curveTo(-128.4,20.1,-145,15.7).curveTo(-153.8,13.4,-161.2,11.6).lineTo(-161.4,11).curveTo(-161.6,10.4,-161.6,9.2).curveTo(-161.6,6.7,-145,10.4).curveTo(-125.8,14.7,-71.4,31.6).curveTo(-47.7,39,-20.5,47.8).curveTo(-7,52.1,71.5,78.8).curveTo(146.6,103.3,162.5,103.5).curveTo(178,103.7,184.1,98.7).curveTo(187.5,95.9,187.5,92).curveTo(187.5,85.2,158.3,40.3).curveTo(129.5,-4.1,123.5,-7.8).curveTo(117.9,-11.3,80.6,-23.1).lineTo(70.9,-26).lineTo(36.1,-36.7).lineTo(-4.4,-49.7).curveTo(-30.5,-58.1,-36.7,-59.9).curveTo(-141.4,-90.5,-175.6,-101.2).curveTo(-192.2,-106.4,-192.2,-106.9).curveTo(-192.2,-110.3,-178.2,-107).lineTo(-175.6,-106.3).curveTo(-164.7,-103.6,-141.6,-96.3).lineTo(-90.8,-80.1).curveTo(-58.4,-70.1,-34,-64).curveTo(-26.7,-62,-1.3,-53.8).curveTo(27.5,-44.4,39.1,-41).curveTo(56.1,-36,79.6,-28.4).lineTo(87.1,-26).curveTo(115.1,-16.9,123.6,-12.9).curveTo(136.4,-6.8,165.1,41.2).curveTo(175.3,58.3,184,74.8).curveTo(192,89.9,192.2,91.9).curveTo(192.8,98.8,188.3,102.7).curveTo(181.6,108.6,163,108.6).curveTo(149.6,108.6,67.9,82).closePath().moveTo(158.9,100.4).curveTo(154.4,99.5,134.5,93.6).curveTo(109.7,86.3,84.8,78.1).lineTo(37.9,62.9).curveTo(11.3,54.2,-5.5,48).curveTo(-25,40.8,-53.8,31.6).curveTo(-75.8,24.5,-101.5,16.9).curveTo(-126.6,9.4,-145,4.4).curveTo(-167,-1.5,-177.8,-3.9).lineTo(-178,-4.2).curveTo(-178.2,-4.5,-178.2,-5.7).curveTo(-178.2,-10.5,-146.8,-1.3).lineTo(-145,-0.8).curveTo(-132.6,2.9,-93.5,15.1).curveTo(-83.6,18.9,-43.4,31.6).lineTo(-2.3,44.5).curveTo(23.7,53.2,80.7,72.9).curveTo(130.3,89.6,155.5,95.8).lineTo(162.1,97.4).curveTo(166.9,98.4,169.8,97.7).curveTo(178.8,95.8,178.8,91.2).curveTo(178.8,85.7,140,73).curveTo(127.7,68.9,112,64.4).lineTo(96.6,60).curveTo(30.5,39.8,7.2,33.4).lineTo(0.4,31.6).curveTo(-55.7,16.1,-84.9,4.8).lineTo(-145,-15.6).curveTo(-167.1,-23.1,-167.1,-22.9).curveTo(-167.1,-25.3,-145,-18.9).curveTo(-124.9,-13.4,-62.6,6.9).curveTo(-4,25.9,9.1,29.1).lineTo(18.7,31.6).curveTo(37.7,36.5,59.9,43.5).lineTo(111.7,60).lineTo(133.1,66.2).curveTo(148,70.5,156.7,73.6).curveTo(183.6,83,183.6,91.2).curveTo(183.6,95.4,181.7,97.2).curveTo(179,99.7,170.4,101.3).curveTo(169.2,101.5,167.7,101.5).curveTo(164.3,101.5,158.9,100.4).closePath();
	this.shape_1.setTransform(192.2,108.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#999999").beginStroke().moveTo(156.5,102.3).curveTo(140.6,102.1,65.6,77.6).curveTo(-13,50.9,-26.4,46.6).curveTo(-53.6,37.8,-77.4,30.4).curveTo(-131.7,13.5,-151,9.2).lineTo(-151,3.2).curveTo(-132.5,8.2,-107.4,15.7).curveTo(-81.8,23.3,-59.7,30.4).curveTo(-30.9,39.6,-11.5,46.8).curveTo(5.3,53,32,61.7).lineTo(78.9,76.9).curveTo(103.8,85.1,128.6,92.4).curveTo(148.5,98.3,153,99.2).curveTo(160.7,100.8,164.5,100.1).curveTo(173,98.5,175.7,96).curveTo(177.7,94.2,177.7,90).curveTo(177.7,81.8,150.8,72.4).curveTo(142.1,69.3,127.1,65).lineTo(105.8,58.8).lineTo(54,42.3).curveTo(31.8,35.3,12.8,30.4).lineTo(3.2,27.9).curveTo(-10,24.7,-68.5,5.7).curveTo(-130.9,-14.6,-151,-20.1).lineTo(-181.6,-102.4).curveTo(-147.3,-91.7,-42.6,-61.1).curveTo(-36.4,-59.3,-10.3,-50.9).lineTo(30.2,-37.9).lineTo(64.9,-27.2).lineTo(74.6,-24.3).curveTo(112,-12.5,117.6,-9).curveTo(123.6,-5.3,152.3,39.1).curveTo(181.6,84,181.6,90.8).curveTo(181.6,94.7,178.1,97.5).curveTo(172.3,102.4,157.7,102.4).lineTo(156.5,102.3).closePath();
	this.shape_2.setTransform(198.2,109.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,384.5,217.3);


(lib.CloseX = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Arrow_Butncopy();
	this.instance.setTransform(34.9,34.9,1,1,0,0,0,34.9,34.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FF0000").beginStroke().moveTo(-26.9,26.9).curveTo(-38,15.8,-38,0).curveTo(-38,-15.7,-26.9,-26.9).curveTo(-19,-34.8,-8.7,-37).curveTo(-4.6,-38,0,-38).curveTo(15.8,-38,26.9,-26.9).curveTo(38,-15.7,38,0).curveTo(38,15.8,26.9,26.9).curveTo(21,32.7,14,35.5).curveTo(7.5,38,0,38).curveTo(-15.7,38,-26.9,26.9).closePath();
	this.shape.setTransform(34.8,34.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regX:34.9,regY:34.9,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regX:34.8,regY:34.8,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.BossMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"Default":0,"StartTalking":4,"TalkLoop":9,"StopTalking":26});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_4 = function() {
		this.mc_BossBubble.removeAllEventListeners("tick");
		this.mc_BossBubble.addEventListener("tick", function(){
			if (gamePost.bossSpeechFrame==null){
				exportRoot.mc_Boss.mc_BossBubble.stop();
			}else{
				exportRoot.mc_Boss.mc_BossBubble.gotoAndStop(gamePost.bossSpeechFrame);
			} 
		});
	}
	this.frame_24 = function() {
		this.stop();
	}
	this.frame_34 = function() {
		this.stop();
		gamePost.bossTalkDone();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4).call(this.frame_4).wait(20).call(this.frame_24).wait(10).call(this.frame_34).wait(1));

	// Layer 3
	this.instance = new lib.BossAnims("single",0);
	this.instance.setTransform(-244.7,154,1,1,0,0,0,232.8,228.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({mode:"synched",loop:false},0).wait(5).to({startPosition:5},0).wait(15).to({startPosition:20},0).wait(2).to({startPosition:21},0).wait(8).to({startPosition:26},0).wait(1));

	// GotItButton
	this.mc_BossGotItBtn = new lib.GotItButton();
	this.mc_BossGotItBtn.setTransform(259.4,150.6,0.114,0.114,0,0,0,99.2,32);
	this.mc_BossGotItBtn._off = true;
	new cjs.ButtonHelper(this.mc_BossGotItBtn, 0, 1, 2, false, new lib.GotItButton(), 3);

	this.timeline.addTween(cjs.Tween.get(this.mc_BossGotItBtn).wait(4).to({_off:false},0).to({regX:99,regY:31.7,scaleX:1,scaleY:1,x:535.4,y:222.4},4).wait(18).to({regX:99.2,regY:32,scaleX:0.11,scaleY:0.11,x:259.4,y:150.6},4).to({_off:true},1).wait(4));

	// Boss_Bubble
	this.mc_BossBubble = new lib.Boss_Bubble();
	this.mc_BossBubble.setTransform(223.9,141.3,0.114,0.114,0,0,0,-176.1,115);
	this.mc_BossBubble._off = true;

	this.timeline.addTween(cjs.Tween.get(this.mc_BossBubble).wait(4).to({_off:false},0).to({regX:-176.3,scaleX:1,scaleY:1},4).wait(18).to({regX:-176.1,scaleX:0.11,scaleY:0.11},4).to({_off:true},1).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-76.6,-74.9,400,472);


(lib.BadgeYard = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("the\nyard", "30px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 35;
	this.text.lineWidth = 395;
	this.text.setTransform(220.8,199.7,1.18,1.18,-8);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#7A6C38").beginStroke().moveTo(-14.4,52.5).lineTo(-24.4,33.2).lineTo(-25.4,31.1).lineTo(-27.8,26.5).lineTo(-28.3,25.6).curveTo(-34.4,13.4,-39.7,0.8).lineTo(-40,0.2).lineTo(-41.4,-3.1).curveTo(-50.7,-25.6,-57.5,-48.9).lineTo(2.3,-56.8).lineTo(18.6,-59).curveTo(34.6,-0.9,57.5,49.9).lineTo(39.9,52.2).lineTo(-10.9,59).lineTo(-14.4,52.5).closePath();
	this.shape.setTransform(80.9,262.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#9E8C49").beginStroke().moveTo(-25.8,165.2).curveTo(-72.3,143.3,-103.6,120.2).curveTo(-124.9,104.5,-139.8,81.9).curveTo(-142.3,78.3,-144.8,74).lineTo(-94.5,67.4).lineTo(-77,65).curveTo(-50.5,122.4,-15.1,170.1).lineTo(-25.8,165.2).closePath().moveTo(-118.4,-49.4).lineTo(141.6,-83.9).curveTo(145.2,-84.6,152.1,-85.4).curveTo(157.5,-86,163.7,-87.4).lineTo(211.7,-95.4).curveTo(204.8,-51.2,191.6,-12.8).lineTo(189.4,-6.5).curveTo(183.5,9.9,176.4,25.2).lineTo(170.3,26.2).lineTo(-79.5,59.4).curveTo(-102.4,8.7,-118.4,-49.4).closePath().moveTo(-203,-73.9).lineTo(-203.1,-74.3).lineTo(-204.2,-79.6).curveTo(-206.3,-90.9,-208,-102.4).curveTo(-211.3,-126.4,-211.7,-148.2).lineTo(-211.5,-165.3).lineTo(-201.1,-165.3).lineTo(-197.2,-166.2).lineTo(-169.8,-166.5).lineTo(-153.6,-168.7).lineTo(-143.1,-170.1).curveTo(-134.5,-109.5,-119.9,-55.2).lineTo(-136.1,-53).lineTo(-196.1,-45.1).curveTo(-200,-59.3,-203,-73.9).closePath();
	this.shape_1.setTransform(217.9,252.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#CCB55E").beginStroke().moveTo(-50.3,205.2).lineTo(-52.5,204.2).curveTo(-87.9,156.4,-114.4,99.1).lineTo(133.3,66.3).lineTo(135.9,65.8).curveTo(133.2,71.3,130.4,76.8).curveTo(123.9,89.5,120.4,96.1).curveTo(114.4,107.5,108.9,115.9).curveTo(93.9,138.5,72.7,154.3).curveTo(41.3,177.4,-5.2,199.3).curveTo(-21.2,206.7,-34.2,212.2).curveTo(-41.7,209.1,-50.3,205.2).closePath().moveTo(-180.5,-136.1).curveTo(-161.9,-138.8,-153.5,-140.9).curveTo(-134,-145.7,-104.4,-160.2).curveTo(-84.4,-169.7,-62,-185.6).curveTo(-37.8,-202.6,-34.2,-212.2).curveTo(-30.5,-202.6,-6.3,-185.6).curveTo(16.1,-169.7,36,-160.2).curveTo(65.7,-145.7,85.1,-140.9).curveTo(98.6,-137.5,138.8,-132.5).lineTo(166.2,-132.1).lineTo(170.2,-131.3).lineTo(180.5,-131.3).curveTo(180.7,-118.8,179.5,-103.7).lineTo(179.4,-102.9).lineTo(179.1,-98.8).lineTo(179,-97.9).lineTo(178.9,-97).lineTo(175.3,-67.3).lineTo(175.3,-67.2).lineTo(126.7,-59.5).lineTo(103.9,-56).lineTo(-157.3,-21.1).curveTo(-171.9,-75.5,-180.5,-136.1).closePath();
	this.shape_2.setTransform(255.2,218.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#1C180C").beginStroke().moveTo(-160.1,82.5).lineTo(-109.3,75.7).lineTo(-91.8,73.4).lineTo(158.1,40.1).lineTo(164.2,39.2).lineTo(161.1,45.7).lineTo(158.4,46.2).lineTo(-89.2,79).lineTo(-106.7,81.3).lineTo(-157,88).lineTo(-160.1,82.5).closePath().moveTo(-208.3,-31.1).lineTo(-148.3,-39.1).lineTo(-132.2,-41.2).lineTo(129.1,-76).lineTo(151.9,-79.5).lineTo(200.4,-87.3).lineTo(200.3,-86.4).lineTo(199.9,-84.1).lineTo(199.5,-81.4).lineTo(151.4,-73.5).curveTo(145.3,-72.1,139.9,-71.5).curveTo(133,-70.7,129.4,-70).lineTo(-130.6,-35.5).lineTo(-146.9,-33.3).lineTo(-206.7,-25.4).lineTo(-208.3,-31.1).closePath().moveTo(208.3,-88).lineTo(208.3,-87.9).lineTo(208,-83.8).lineTo(207.9,-82.8).lineTo(207.5,-82.8).lineTo(208.3,-88).closePath();
	this.shape_3.setTransform(230.1,238.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#000000").beginStroke().moveTo(-5.4,217.5).lineTo(-6.4,217.1).curveTo(-9.6,216,-9.1,215.8).lineTo(-9.4,215.7).curveTo(-34.6,207,-65.2,188.7).curveTo(-92.2,172.6,-124.9,147.8).curveTo(-139.1,137.1,-155,110).lineTo(-155.6,108.9).lineTo(-158.7,103.4).curveTo(-166.2,90,-172.5,75.5).lineTo(-174.9,70).lineTo(-177.7,63.2).curveTo(-193.8,22.8,-196.8,14.8).lineTo(-200.1,6.1).curveTo(-206.6,-11.5,-210.3,-24.7).curveTo(-212.3,-31.7,-213.9,-38.8).lineTo(-215.1,-44.3).curveTo(-221,-73.4,-221,-106.4).curveTo(-221.2,-119.1,-220.5,-129).lineTo(-220.1,-133.6).lineTo(-220.4,-133.9).curveTo(-221,-134.4,-221,-134.7).curveTo(-221,-136.1,-220.6,-136.5).curveTo(-220.4,-136.6,-219.7,-136.7).curveTo(-219.1,-139.6,-212,-139.5).curveTo(-201.4,-139.2,-199.1,-139.8).lineTo(-171.4,-140.1).lineTo(-157.5,-142.2).lineTo(-147.4,-143.7).lineTo(-141.3,-144.6).curveTo(-126.5,-146.8,-118.1,-148.7).curveTo(-96,-153.8,-67,-169.4).curveTo(-50.1,-178.4,-28.9,-195.9).curveTo(-5.1,-215.6,-0.1,-218.8).lineTo(0,-218.3).lineTo(0.2,-218.8).curveTo(5.2,-215.6,29,-195.9).curveTo(50.2,-178.4,67.1,-169.4).curveTo(96.1,-153.8,118.1,-148.7).curveTo(126.6,-146.8,141.4,-144.6).curveTo(164.7,-141.2,171.4,-140.1).lineTo(199.1,-139.8).curveTo(201.4,-139.2,212,-139.5).curveTo(219.2,-139.6,219.7,-136.7).curveTo(220.5,-136.6,220.7,-136.5).curveTo(221,-136.1,221,-134.7).curveTo(221,-134.4,220.4,-133.9).lineTo(220.2,-133.6).curveTo(221.3,-123.2,221,-106.4).lineTo(220.9,-105).lineTo(220.6,-99.3).curveTo(219.5,-83.4,217.4,-68.4).lineTo(217.4,-68.2).lineTo(217.4,-68.1).lineTo(216.6,-62.9).curveTo(213.8,-44.9,209.7,-28.2).curveTo(204.4,-7.1,194.8,20.3).lineTo(192.6,26.5).curveTo(187.3,41.3,180.7,58).lineTo(178.7,63.2).lineTo(178.1,64.6).curveTo(168.8,87.5,155.3,109.9).curveTo(139.1,136.7,125,147.8).curveTo(62.5,197.4,9.2,215.8).curveTo(4.5,217.4,0,218.7).lineTo(-0.4,218.8).curveTo(-1.8,218.8,-5.4,217.5).closePath().moveTo(-214.9,-114.3).curveTo(-214.5,-92.5,-211.1,-68.5).curveTo(-209.5,-57.1,-207.4,-45.8).lineTo(-206.3,-40.4).lineTo(-206.2,-40).curveTo(-203.2,-25.4,-199.3,-11.2).lineTo(-197.7,-5.5).curveTo(-190.9,17.8,-181.6,40.4).lineTo(-180.2,43.6).lineTo(-179.9,44.3).curveTo(-174.6,56.8,-168.4,69.1).lineTo(-168,69.9).lineTo(-165.6,74.6).lineTo(-164.6,76.6).lineTo(-154.6,96).lineTo(-151.1,102.4).lineTo(-148,107.9).curveTo(-145.5,112.2,-143,115.8).curveTo(-128.1,138.4,-106.8,154.1).curveTo(-75.4,177.2,-28.9,199.1).lineTo(-18.3,204).lineTo(-16.1,205).curveTo(-7.5,208.9,0,212.1).curveTo(13,206.6,29,199.1).curveTo(75.5,177.2,106.9,154.1).curveTo(128.1,138.4,143.1,115.8).curveTo(148.6,107.4,154.6,96).curveTo(158.1,89.4,164.6,76.6).curveTo(167.4,71.2,170.1,65.6).lineTo(173.2,59.1).curveTo(180.3,43.8,186.2,27.4).lineTo(188.4,21.1).curveTo(201.6,-17.3,208.6,-61.5).lineTo(209,-64.1).lineTo(209.3,-66.5).lineTo(209.5,-67.4).lineTo(209.5,-67.4).lineTo(213.1,-97.2).lineTo(213.2,-98.1).lineTo(213.3,-99).lineTo(213.6,-103.1).lineTo(213.7,-103.8).curveTo(214.9,-118.9,214.7,-131.5).lineTo(204.4,-131.5).lineTo(200.4,-132.3).lineTo(173,-132.6).curveTo(132.8,-137.7,119.3,-141.1).curveTo(99.9,-145.9,70.2,-160.3).curveTo(50.3,-169.9,27.9,-185.7).curveTo(3.7,-202.7,0,-212.4).curveTo(-3.6,-202.7,-27.8,-185.7).curveTo(-50.2,-169.9,-70.2,-160.3).curveTo(-99.8,-145.9,-119.3,-141.1).curveTo(-127.7,-139,-146.3,-136.2).lineTo(-156.7,-134.8).lineTo(-173,-132.6).lineTo(-200.4,-132.3).lineTo(-204.3,-131.5).lineTo(-214.7,-131.5).closePath();
	this.shape_4.setTransform(221,218.8);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("rgba(0,0,0,0.502)").beginStroke().moveTo(-5.4,217.5).lineTo(-6.4,217.2).curveTo(-9.6,215.9,-9.1,215.8).lineTo(-9.3,215.7).curveTo(-34.6,207,-65.2,188.7).curveTo(-92.2,172.5,-124.9,147.9).curveTo(-139.1,137.2,-155,110.1).lineTo(-155.6,109).lineTo(-158.7,103.5).curveTo(-166.2,90.1,-172.5,75.5).lineTo(-174.9,69.9).lineTo(-177.7,63.2).curveTo(-193.8,22.8,-196.8,14.8).lineTo(-200.1,6.1).curveTo(-206.5,-11.5,-210.3,-24.8).curveTo(-212.3,-31.7,-213.9,-38.9).lineTo(-215.1,-44.4).curveTo(-221,-73.5,-221,-106.4).curveTo(-221.2,-119.1,-220.5,-129).lineTo(-220.1,-133.7).lineTo(-220.4,-133.8).curveTo(-221,-134.4,-221,-134.7).curveTo(-221,-136.2,-220.6,-136.5).curveTo(-220.4,-136.6,-219.7,-136.6).curveTo(-219.1,-139.6,-212,-139.5).curveTo(-201.4,-139.3,-199.1,-139.8).lineTo(-171.4,-140.1).curveTo(-167.4,-140.8,-157.5,-142.2).lineTo(-147.3,-143.7).lineTo(-141.3,-144.6).curveTo(-126.5,-146.8,-118.1,-148.8).curveTo(-96,-153.8,-67,-169.4).curveTo(-50.1,-178.5,-28.9,-195.9).curveTo(-5.1,-215.5,-0.1,-218.7).lineTo(0,-218.3).lineTo(0.2,-218.7).curveTo(5.2,-215.5,29,-195.9).curveTo(50.2,-178.5,67.1,-169.4).curveTo(96.1,-153.8,118.1,-148.8).curveTo(126.6,-146.8,141.4,-144.6).curveTo(164.7,-141.2,171.4,-140.1).lineTo(199.1,-139.8).curveTo(201.4,-139.3,212,-139.5).curveTo(219.2,-139.6,219.7,-136.6).curveTo(220.5,-136.6,220.7,-136.5).curveTo(221,-136.2,221,-134.7).curveTo(221,-134.4,220.4,-133.8).lineTo(220.2,-133.7).curveTo(221.3,-123.2,221,-106.4).lineTo(221,-105).lineTo(220.6,-99.3).curveTo(219.5,-83.3,217.4,-68.3).lineTo(217.4,-68.2).lineTo(217.4,-68.1).lineTo(217.4,-68).lineTo(217,-63.8).lineTo(216.9,-62.9).lineTo(216.6,-62.8).curveTo(213.9,-44.9,209.7,-28.2).curveTo(204.4,-7.2,194.8,20.3).lineTo(192.6,26.5).curveTo(187.3,41.3,180.8,58).lineTo(178.7,63.2).lineTo(178.1,64.6).curveTo(168.9,87.5,155.3,109.9).curveTo(139.1,136.7,125,147.9).curveTo(62.5,197.4,9.2,215.8).curveTo(4.5,217.4,0,218.7).lineTo(-0.4,218.7).curveTo(-1.8,218.8,-5.4,217.5).closePath();
	this.shape_5.setTransform(230.5,228.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-10.2,0,484.3,447);


(lib.BadgeDeliveryTruck = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("the", "30px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 112;
	this.text.setTransform(62.3,219.6,1,1,-21);

	this.text_1 = new cjs.Text("delivery truck", "49px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text_1.textAlign = "center";
	this.text_1.lineHeight = 49;
	this.text_1.lineWidth = 332;
	this.text_1.setTransform(220,193.8,1.18,1.18,-8);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#7A6C38").beginStroke().moveTo(-14.4,52.5).lineTo(-24.4,33.2).lineTo(-25.4,31.1).lineTo(-27.8,26.5).lineTo(-28.3,25.6).curveTo(-34.4,13.4,-39.7,0.8).lineTo(-40,0.2).lineTo(-41.4,-3.1).curveTo(-50.7,-25.6,-57.5,-48.9).lineTo(2.3,-56.8).lineTo(18.6,-59).curveTo(34.6,-0.9,57.5,49.9).lineTo(39.9,52.2).lineTo(-10.9,59).lineTo(-14.4,52.5).closePath();
	this.shape.setTransform(80.9,262.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#9E8C49").beginStroke().moveTo(-25.8,165.2).curveTo(-72.3,143.3,-103.6,120.2).curveTo(-124.9,104.5,-139.8,81.9).curveTo(-142.3,78.3,-144.8,74).lineTo(-94.5,67.4).lineTo(-77,65).curveTo(-50.5,122.4,-15.1,170.1).lineTo(-25.8,165.2).closePath().moveTo(-118.4,-49.4).lineTo(141.6,-83.9).curveTo(145.2,-84.6,152.1,-85.4).curveTo(157.5,-86,163.7,-87.4).lineTo(211.7,-95.4).curveTo(204.8,-51.2,191.6,-12.8).lineTo(189.4,-6.5).curveTo(183.5,9.9,176.4,25.2).lineTo(170.3,26.2).lineTo(-79.5,59.4).curveTo(-102.4,8.7,-118.4,-49.4).closePath().moveTo(-203,-73.9).lineTo(-203.1,-74.3).lineTo(-204.2,-79.6).curveTo(-206.3,-90.9,-208,-102.4).curveTo(-211.3,-126.4,-211.7,-148.2).lineTo(-211.5,-165.3).lineTo(-201.1,-165.3).lineTo(-197.2,-166.2).lineTo(-169.8,-166.5).lineTo(-153.6,-168.7).lineTo(-143.1,-170.1).curveTo(-134.5,-109.5,-119.9,-55.2).lineTo(-136.1,-53).lineTo(-196.1,-45.1).curveTo(-200,-59.3,-203,-73.9).closePath();
	this.shape_1.setTransform(217.9,252.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#CCB55E").beginStroke().moveTo(-50.3,205.2).lineTo(-52.5,204.2).curveTo(-87.9,156.4,-114.4,99.1).lineTo(133.3,66.3).lineTo(135.9,65.8).curveTo(133.2,71.3,130.4,76.8).curveTo(123.9,89.5,120.4,96.1).curveTo(114.4,107.5,108.9,115.9).curveTo(93.9,138.5,72.7,154.3).curveTo(41.3,177.4,-5.2,199.3).curveTo(-21.2,206.7,-34.2,212.2).curveTo(-41.7,209.1,-50.3,205.2).closePath().moveTo(-180.5,-136.1).curveTo(-161.9,-138.8,-153.5,-140.9).curveTo(-134,-145.7,-104.4,-160.2).curveTo(-84.4,-169.7,-62,-185.6).curveTo(-37.8,-202.6,-34.2,-212.2).curveTo(-30.5,-202.6,-6.3,-185.6).curveTo(16.1,-169.7,36,-160.2).curveTo(65.7,-145.7,85.1,-140.9).curveTo(98.6,-137.5,138.8,-132.5).lineTo(166.2,-132.1).lineTo(170.2,-131.3).lineTo(180.5,-131.3).curveTo(180.7,-118.8,179.5,-103.7).lineTo(179.4,-102.9).lineTo(179.1,-98.8).lineTo(179,-97.9).lineTo(178.9,-97).lineTo(175.3,-67.3).lineTo(175.3,-67.2).lineTo(126.7,-59.5).lineTo(103.9,-56).lineTo(-157.3,-21.1).curveTo(-171.9,-75.5,-180.5,-136.1).closePath();
	this.shape_2.setTransform(255.2,218.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#1C180C").beginStroke().moveTo(-160.1,82.5).lineTo(-109.3,75.7).lineTo(-91.8,73.4).lineTo(158.1,40.1).lineTo(164.2,39.2).lineTo(161.1,45.7).lineTo(158.4,46.2).lineTo(-89.2,79).lineTo(-106.7,81.3).lineTo(-157,88).lineTo(-160.1,82.5).closePath().moveTo(-208.3,-31.1).lineTo(-148.3,-39.1).lineTo(-132.2,-41.2).lineTo(129.1,-76).lineTo(151.9,-79.5).lineTo(200.4,-87.3).lineTo(200.3,-86.4).lineTo(199.9,-84.1).lineTo(199.5,-81.4).lineTo(151.4,-73.5).curveTo(145.3,-72.1,139.9,-71.5).curveTo(133,-70.7,129.4,-70).lineTo(-130.6,-35.5).lineTo(-146.9,-33.3).lineTo(-206.7,-25.4).lineTo(-208.3,-31.1).closePath().moveTo(208.3,-88).lineTo(208.3,-87.9).lineTo(208,-83.8).lineTo(207.9,-82.8).lineTo(207.5,-82.8).lineTo(208.3,-88).closePath();
	this.shape_3.setTransform(230.1,238.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#000000").beginStroke().moveTo(-5.4,217.5).lineTo(-6.4,217.1).curveTo(-9.6,216,-9.1,215.8).lineTo(-9.4,215.7).curveTo(-34.6,207,-65.2,188.7).curveTo(-92.2,172.6,-124.9,147.8).curveTo(-139.1,137.1,-155,110).lineTo(-155.6,108.9).lineTo(-158.7,103.4).curveTo(-166.2,90,-172.5,75.5).lineTo(-174.9,70).lineTo(-177.7,63.2).curveTo(-193.8,22.8,-196.8,14.8).lineTo(-200.1,6.1).curveTo(-206.6,-11.5,-210.3,-24.7).curveTo(-212.3,-31.7,-213.9,-38.8).lineTo(-215.1,-44.3).curveTo(-221,-73.4,-221,-106.4).curveTo(-221.2,-119.1,-220.5,-129).lineTo(-220.1,-133.6).lineTo(-220.4,-133.9).curveTo(-221,-134.4,-221,-134.7).curveTo(-221,-136.1,-220.6,-136.5).curveTo(-220.4,-136.6,-219.7,-136.7).curveTo(-219.1,-139.6,-212,-139.5).curveTo(-201.4,-139.2,-199.1,-139.8).lineTo(-171.4,-140.1).lineTo(-157.5,-142.2).lineTo(-147.4,-143.7).lineTo(-141.3,-144.6).curveTo(-126.5,-146.8,-118.1,-148.7).curveTo(-96,-153.8,-67,-169.4).curveTo(-50.1,-178.4,-28.9,-195.9).curveTo(-5.1,-215.6,-0.1,-218.8).lineTo(0,-218.3).lineTo(0.2,-218.8).curveTo(5.2,-215.6,29,-195.9).curveTo(50.2,-178.4,67.1,-169.4).curveTo(96.1,-153.8,118.1,-148.7).curveTo(126.6,-146.8,141.4,-144.6).curveTo(164.7,-141.2,171.4,-140.1).lineTo(199.1,-139.8).curveTo(201.4,-139.2,212,-139.5).curveTo(219.2,-139.6,219.7,-136.7).curveTo(220.5,-136.6,220.7,-136.5).curveTo(221,-136.1,221,-134.7).curveTo(221,-134.4,220.4,-133.9).lineTo(220.2,-133.6).curveTo(221.3,-123.2,221,-106.4).lineTo(220.9,-105).lineTo(220.6,-99.3).curveTo(219.5,-83.4,217.4,-68.4).lineTo(217.4,-68.2).lineTo(217.4,-68.1).lineTo(216.6,-62.9).curveTo(213.8,-44.9,209.7,-28.2).curveTo(204.4,-7.1,194.8,20.3).lineTo(192.6,26.5).curveTo(187.3,41.3,180.7,58).lineTo(178.7,63.2).lineTo(178.1,64.6).curveTo(168.8,87.5,155.3,109.9).curveTo(139.1,136.7,125,147.8).curveTo(62.5,197.4,9.2,215.8).curveTo(4.5,217.4,0,218.7).lineTo(-0.4,218.8).curveTo(-1.8,218.8,-5.4,217.5).closePath().moveTo(-214.9,-114.3).curveTo(-214.5,-92.5,-211.1,-68.5).curveTo(-209.5,-57.1,-207.4,-45.8).lineTo(-206.3,-40.4).lineTo(-206.2,-40).curveTo(-203.2,-25.4,-199.3,-11.2).lineTo(-197.7,-5.5).curveTo(-190.9,17.8,-181.6,40.4).lineTo(-180.2,43.6).lineTo(-179.9,44.3).curveTo(-174.6,56.8,-168.4,69.1).lineTo(-168,69.9).lineTo(-165.6,74.6).lineTo(-164.6,76.6).lineTo(-154.6,96).lineTo(-151.1,102.4).lineTo(-148,107.9).curveTo(-145.5,112.2,-143,115.8).curveTo(-128.1,138.4,-106.8,154.1).curveTo(-75.4,177.2,-28.9,199.1).lineTo(-18.3,204).lineTo(-16.1,205).curveTo(-7.5,208.9,0,212.1).curveTo(13,206.6,29,199.1).curveTo(75.5,177.2,106.9,154.1).curveTo(128.1,138.4,143.1,115.8).curveTo(148.6,107.4,154.6,96).curveTo(158.1,89.4,164.6,76.6).curveTo(167.4,71.2,170.1,65.6).lineTo(173.2,59.1).curveTo(180.3,43.8,186.2,27.4).lineTo(188.4,21.1).curveTo(201.6,-17.3,208.6,-61.5).lineTo(209,-64.1).lineTo(209.3,-66.5).lineTo(209.5,-67.4).lineTo(209.5,-67.4).lineTo(213.1,-97.2).lineTo(213.2,-98.1).lineTo(213.3,-99).lineTo(213.6,-103.1).lineTo(213.7,-103.8).curveTo(214.9,-118.9,214.7,-131.5).lineTo(204.4,-131.5).lineTo(200.4,-132.3).lineTo(173,-132.6).curveTo(132.8,-137.7,119.3,-141.1).curveTo(99.9,-145.9,70.2,-160.3).curveTo(50.3,-169.9,27.9,-185.7).curveTo(3.7,-202.7,0,-212.4).curveTo(-3.6,-202.7,-27.8,-185.7).curveTo(-50.2,-169.9,-70.2,-160.3).curveTo(-99.8,-145.9,-119.3,-141.1).curveTo(-127.7,-139,-146.3,-136.2).lineTo(-156.7,-134.8).lineTo(-173,-132.6).lineTo(-200.4,-132.3).lineTo(-204.3,-131.5).lineTo(-214.7,-131.5).closePath();
	this.shape_4.setTransform(221,218.8);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("rgba(0,0,0,0.502)").beginStroke().moveTo(-5.4,217.5).lineTo(-6.4,217.2).curveTo(-9.6,215.9,-9.1,215.8).lineTo(-9.3,215.7).curveTo(-34.6,207,-65.2,188.7).curveTo(-92.2,172.5,-124.9,147.9).curveTo(-139.1,137.2,-155,110.1).lineTo(-155.6,109).lineTo(-158.7,103.5).curveTo(-166.2,90.1,-172.5,75.5).lineTo(-174.9,69.9).lineTo(-177.7,63.2).curveTo(-193.8,22.8,-196.8,14.8).lineTo(-200.1,6.1).curveTo(-206.5,-11.5,-210.3,-24.8).curveTo(-212.3,-31.7,-213.9,-38.9).lineTo(-215.1,-44.4).curveTo(-221,-73.5,-221,-106.4).curveTo(-221.2,-119.1,-220.5,-129).lineTo(-220.1,-133.7).lineTo(-220.4,-133.8).curveTo(-221,-134.4,-221,-134.7).curveTo(-221,-136.2,-220.6,-136.5).curveTo(-220.4,-136.6,-219.7,-136.6).curveTo(-219.1,-139.6,-212,-139.5).curveTo(-201.4,-139.3,-199.1,-139.8).lineTo(-171.4,-140.1).curveTo(-167.4,-140.8,-157.5,-142.2).lineTo(-147.3,-143.7).lineTo(-141.3,-144.6).curveTo(-126.5,-146.8,-118.1,-148.8).curveTo(-96,-153.8,-67,-169.4).curveTo(-50.1,-178.5,-28.9,-195.9).curveTo(-5.1,-215.5,-0.1,-218.7).lineTo(0,-218.3).lineTo(0.2,-218.7).curveTo(5.2,-215.5,29,-195.9).curveTo(50.2,-178.5,67.1,-169.4).curveTo(96.1,-153.8,118.1,-148.8).curveTo(126.6,-146.8,141.4,-144.6).curveTo(164.7,-141.2,171.4,-140.1).lineTo(199.1,-139.8).curveTo(201.4,-139.3,212,-139.5).curveTo(219.2,-139.6,219.7,-136.6).curveTo(220.5,-136.6,220.7,-136.5).curveTo(221,-136.2,221,-134.7).curveTo(221,-134.4,220.4,-133.8).lineTo(220.2,-133.7).curveTo(221.3,-123.2,221,-106.4).lineTo(221,-105).lineTo(220.6,-99.3).curveTo(219.5,-83.3,217.4,-68.3).lineTo(217.4,-68.2).lineTo(217.4,-68.1).lineTo(217.4,-68).lineTo(217,-63.8).lineTo(216.9,-62.9).lineTo(216.6,-62.8).curveTo(213.9,-44.9,209.7,-28.2).curveTo(204.4,-7.2,194.8,20.3).lineTo(192.6,26.5).curveTo(187.3,41.3,180.8,58).lineTo(178.7,63.2).lineTo(178.1,64.6).curveTo(168.9,87.5,155.3,109.9).curveTo(139.1,136.7,125,147.9).curveTo(62.5,197.4,9.2,215.8).curveTo(4.5,217.4,0,218.7).lineTo(-0.4,218.7).curveTo(-1.8,218.8,-5.4,217.5).closePath();
	this.shape_5.setTransform(230.5,228.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance,this.text_1,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,451.6,447);


(lib.Badge = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("the\npost room", "30px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 395;
	this.text.setTransform(220.8,199.7,1.18,1.18,-8);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#7A6C38").beginStroke().moveTo(-14.4,52.5).lineTo(-24.4,33.2).lineTo(-25.4,31.1).lineTo(-27.8,26.5).lineTo(-28.3,25.6).curveTo(-34.4,13.4,-39.7,0.8).lineTo(-40,0.2).lineTo(-41.4,-3.1).curveTo(-50.7,-25.6,-57.5,-48.9).lineTo(2.3,-56.8).lineTo(18.6,-59).curveTo(34.6,-0.9,57.5,49.9).lineTo(39.9,52.2).lineTo(-10.9,59).lineTo(-14.4,52.5).closePath();
	this.shape.setTransform(80.9,262.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#9E8C49").beginStroke().moveTo(-25.8,165.2).curveTo(-72.3,143.3,-103.6,120.2).curveTo(-124.9,104.5,-139.8,81.9).curveTo(-142.3,78.3,-144.8,74).lineTo(-94.5,67.4).lineTo(-77,65).curveTo(-50.5,122.4,-15.1,170.1).lineTo(-25.8,165.2).closePath().moveTo(-118.4,-49.4).lineTo(141.6,-83.9).curveTo(145.2,-84.6,152.1,-85.4).curveTo(157.5,-86,163.7,-87.4).lineTo(211.7,-95.4).curveTo(204.8,-51.2,191.6,-12.8).lineTo(189.4,-6.5).curveTo(183.5,9.9,176.4,25.2).lineTo(170.3,26.2).lineTo(-79.5,59.4).curveTo(-102.4,8.7,-118.4,-49.4).closePath().moveTo(-203,-73.9).lineTo(-203.1,-74.3).lineTo(-204.2,-79.6).curveTo(-206.3,-90.9,-208,-102.4).curveTo(-211.3,-126.4,-211.7,-148.2).lineTo(-211.5,-165.3).lineTo(-201.1,-165.3).lineTo(-197.2,-166.2).lineTo(-169.8,-166.5).lineTo(-153.6,-168.7).lineTo(-143.1,-170.1).curveTo(-134.5,-109.5,-119.9,-55.2).lineTo(-136.1,-53).lineTo(-196.1,-45.1).curveTo(-200,-59.3,-203,-73.9).closePath();
	this.shape_1.setTransform(217.9,252.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#CCB55E").beginStroke().moveTo(-50.3,205.2).lineTo(-52.5,204.2).curveTo(-87.9,156.4,-114.4,99.1).lineTo(133.3,66.3).lineTo(135.9,65.8).curveTo(133.2,71.3,130.4,76.8).curveTo(123.9,89.5,120.4,96.1).curveTo(114.4,107.5,108.9,115.9).curveTo(93.9,138.5,72.7,154.3).curveTo(41.3,177.4,-5.2,199.3).curveTo(-21.2,206.7,-34.2,212.2).curveTo(-41.7,209.1,-50.3,205.2).closePath().moveTo(-180.5,-136.1).curveTo(-161.9,-138.8,-153.5,-140.9).curveTo(-134,-145.7,-104.4,-160.2).curveTo(-84.4,-169.7,-62,-185.6).curveTo(-37.8,-202.6,-34.2,-212.2).curveTo(-30.5,-202.6,-6.3,-185.6).curveTo(16.1,-169.7,36,-160.2).curveTo(65.7,-145.7,85.1,-140.9).curveTo(98.6,-137.5,138.8,-132.5).lineTo(166.2,-132.1).lineTo(170.2,-131.3).lineTo(180.5,-131.3).curveTo(180.7,-118.8,179.5,-103.7).lineTo(179.4,-102.9).lineTo(179.1,-98.8).lineTo(179,-97.9).lineTo(178.9,-97).lineTo(175.3,-67.3).lineTo(175.3,-67.2).lineTo(126.7,-59.5).lineTo(103.9,-56).lineTo(-157.3,-21.1).curveTo(-171.9,-75.5,-180.5,-136.1).closePath();
	this.shape_2.setTransform(255.2,218.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#1C180C").beginStroke().moveTo(-160.1,82.5).lineTo(-109.3,75.7).lineTo(-91.8,73.4).lineTo(158.1,40.1).lineTo(164.2,39.2).lineTo(161.1,45.7).lineTo(158.4,46.2).lineTo(-89.2,79).lineTo(-106.7,81.3).lineTo(-157,88).lineTo(-160.1,82.5).closePath().moveTo(-208.3,-31.1).lineTo(-148.3,-39.1).lineTo(-132.2,-41.2).lineTo(129.1,-76).lineTo(151.9,-79.5).lineTo(200.4,-87.3).lineTo(200.3,-86.4).lineTo(199.9,-84.1).lineTo(199.5,-81.4).lineTo(151.4,-73.5).curveTo(145.3,-72.1,139.9,-71.5).curveTo(133,-70.7,129.4,-70).lineTo(-130.6,-35.5).lineTo(-146.9,-33.3).lineTo(-206.7,-25.4).lineTo(-208.3,-31.1).closePath().moveTo(208.3,-88).lineTo(208.3,-87.9).lineTo(208,-83.8).lineTo(207.9,-82.8).lineTo(207.5,-82.8).lineTo(208.3,-88).closePath();
	this.shape_3.setTransform(230.1,238.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#000000").beginStroke().moveTo(-5.4,217.5).lineTo(-6.4,217.1).curveTo(-9.6,216,-9.1,215.8).lineTo(-9.4,215.7).curveTo(-34.6,207,-65.2,188.7).curveTo(-92.2,172.6,-124.9,147.8).curveTo(-139.1,137.1,-155,110).lineTo(-155.6,108.9).lineTo(-158.7,103.4).curveTo(-166.2,90,-172.5,75.5).lineTo(-174.9,70).lineTo(-177.7,63.2).curveTo(-193.8,22.8,-196.8,14.8).lineTo(-200.1,6.1).curveTo(-206.6,-11.5,-210.3,-24.7).curveTo(-212.3,-31.7,-213.9,-38.8).lineTo(-215.1,-44.3).curveTo(-221,-73.4,-221,-106.4).curveTo(-221.2,-119.1,-220.5,-129).lineTo(-220.1,-133.6).lineTo(-220.4,-133.9).curveTo(-221,-134.4,-221,-134.7).curveTo(-221,-136.1,-220.6,-136.5).curveTo(-220.4,-136.6,-219.7,-136.7).curveTo(-219.1,-139.6,-212,-139.5).curveTo(-201.4,-139.2,-199.1,-139.8).lineTo(-171.4,-140.1).lineTo(-157.5,-142.2).lineTo(-147.4,-143.7).lineTo(-141.3,-144.6).curveTo(-126.5,-146.8,-118.1,-148.7).curveTo(-96,-153.8,-67,-169.4).curveTo(-50.1,-178.4,-28.9,-195.9).curveTo(-5.1,-215.6,-0.1,-218.8).lineTo(0,-218.3).lineTo(0.2,-218.8).curveTo(5.2,-215.6,29,-195.9).curveTo(50.2,-178.4,67.1,-169.4).curveTo(96.1,-153.8,118.1,-148.7).curveTo(126.6,-146.8,141.4,-144.6).curveTo(164.7,-141.2,171.4,-140.1).lineTo(199.1,-139.8).curveTo(201.4,-139.2,212,-139.5).curveTo(219.2,-139.6,219.7,-136.7).curveTo(220.5,-136.6,220.7,-136.5).curveTo(221,-136.1,221,-134.7).curveTo(221,-134.4,220.4,-133.9).lineTo(220.2,-133.6).curveTo(221.3,-123.2,221,-106.4).lineTo(220.9,-105).lineTo(220.6,-99.3).curveTo(219.5,-83.4,217.4,-68.4).lineTo(217.4,-68.2).lineTo(217.4,-68.1).lineTo(216.6,-62.9).curveTo(213.8,-44.9,209.7,-28.2).curveTo(204.4,-7.1,194.8,20.3).lineTo(192.6,26.5).curveTo(187.3,41.3,180.7,58).lineTo(178.7,63.2).lineTo(178.1,64.6).curveTo(168.8,87.5,155.3,109.9).curveTo(139.1,136.7,125,147.8).curveTo(62.5,197.4,9.2,215.8).curveTo(4.5,217.4,0,218.7).lineTo(-0.4,218.8).curveTo(-1.8,218.8,-5.4,217.5).closePath().moveTo(-214.9,-114.3).curveTo(-214.5,-92.5,-211.1,-68.5).curveTo(-209.5,-57.1,-207.4,-45.8).lineTo(-206.3,-40.4).lineTo(-206.2,-40).curveTo(-203.2,-25.4,-199.3,-11.2).lineTo(-197.7,-5.5).curveTo(-190.9,17.8,-181.6,40.4).lineTo(-180.2,43.6).lineTo(-179.9,44.3).curveTo(-174.6,56.8,-168.4,69.1).lineTo(-168,69.9).lineTo(-165.6,74.6).lineTo(-164.6,76.6).lineTo(-154.6,96).lineTo(-151.1,102.4).lineTo(-148,107.9).curveTo(-145.5,112.2,-143,115.8).curveTo(-128.1,138.4,-106.8,154.1).curveTo(-75.4,177.2,-28.9,199.1).lineTo(-18.3,204).lineTo(-16.1,205).curveTo(-7.5,208.9,0,212.1).curveTo(13,206.6,29,199.1).curveTo(75.5,177.2,106.9,154.1).curveTo(128.1,138.4,143.1,115.8).curveTo(148.6,107.4,154.6,96).curveTo(158.1,89.4,164.6,76.6).curveTo(167.4,71.2,170.1,65.6).lineTo(173.2,59.1).curveTo(180.3,43.8,186.2,27.4).lineTo(188.4,21.1).curveTo(201.6,-17.3,208.6,-61.5).lineTo(209,-64.1).lineTo(209.3,-66.5).lineTo(209.5,-67.4).lineTo(209.5,-67.4).lineTo(213.1,-97.2).lineTo(213.2,-98.1).lineTo(213.3,-99).lineTo(213.6,-103.1).lineTo(213.7,-103.8).curveTo(214.9,-118.9,214.7,-131.5).lineTo(204.4,-131.5).lineTo(200.4,-132.3).lineTo(173,-132.6).curveTo(132.8,-137.7,119.3,-141.1).curveTo(99.9,-145.9,70.2,-160.3).curveTo(50.3,-169.9,27.9,-185.7).curveTo(3.7,-202.7,0,-212.4).curveTo(-3.6,-202.7,-27.8,-185.7).curveTo(-50.2,-169.9,-70.2,-160.3).curveTo(-99.8,-145.9,-119.3,-141.1).curveTo(-127.7,-139,-146.3,-136.2).lineTo(-156.7,-134.8).lineTo(-173,-132.6).lineTo(-200.4,-132.3).lineTo(-204.3,-131.5).lineTo(-214.7,-131.5).closePath();
	this.shape_4.setTransform(221,218.8);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("rgba(0,0,0,0.502)").beginStroke().moveTo(-5.4,217.5).lineTo(-6.4,217.2).curveTo(-9.6,215.9,-9.1,215.8).lineTo(-9.3,215.7).curveTo(-34.6,207,-65.2,188.7).curveTo(-92.2,172.5,-124.9,147.9).curveTo(-139.1,137.2,-155,110.1).lineTo(-155.6,109).lineTo(-158.7,103.5).curveTo(-166.2,90.1,-172.5,75.5).lineTo(-174.9,69.9).lineTo(-177.7,63.2).curveTo(-193.8,22.8,-196.8,14.8).lineTo(-200.1,6.1).curveTo(-206.5,-11.5,-210.3,-24.8).curveTo(-212.3,-31.7,-213.9,-38.9).lineTo(-215.1,-44.4).curveTo(-221,-73.5,-221,-106.4).curveTo(-221.2,-119.1,-220.5,-129).lineTo(-220.1,-133.7).lineTo(-220.4,-133.8).curveTo(-221,-134.4,-221,-134.7).curveTo(-221,-136.2,-220.6,-136.5).curveTo(-220.4,-136.6,-219.7,-136.6).curveTo(-219.1,-139.6,-212,-139.5).curveTo(-201.4,-139.3,-199.1,-139.8).lineTo(-171.4,-140.1).curveTo(-167.4,-140.8,-157.5,-142.2).lineTo(-147.3,-143.7).lineTo(-141.3,-144.6).curveTo(-126.5,-146.8,-118.1,-148.8).curveTo(-96,-153.8,-67,-169.4).curveTo(-50.1,-178.5,-28.9,-195.9).curveTo(-5.1,-215.5,-0.1,-218.7).lineTo(0,-218.3).lineTo(0.2,-218.7).curveTo(5.2,-215.5,29,-195.9).curveTo(50.2,-178.5,67.1,-169.4).curveTo(96.1,-153.8,118.1,-148.8).curveTo(126.6,-146.8,141.4,-144.6).curveTo(164.7,-141.2,171.4,-140.1).lineTo(199.1,-139.8).curveTo(201.4,-139.3,212,-139.5).curveTo(219.2,-139.6,219.7,-136.6).curveTo(220.5,-136.6,220.7,-136.5).curveTo(221,-136.2,221,-134.7).curveTo(221,-134.4,220.4,-133.8).lineTo(220.2,-133.7).curveTo(221.3,-123.2,221,-106.4).lineTo(221,-105).lineTo(220.6,-99.3).curveTo(219.5,-83.3,217.4,-68.3).lineTo(217.4,-68.2).lineTo(217.4,-68.1).lineTo(217.4,-68).lineTo(217,-63.8).lineTo(216.9,-62.9).lineTo(216.6,-62.8).curveTo(213.9,-44.9,209.7,-28.2).curveTo(204.4,-7.2,194.8,20.3).lineTo(192.6,26.5).curveTo(187.3,41.3,180.8,58).lineTo(178.7,63.2).lineTo(178.1,64.6).curveTo(168.9,87.5,155.3,109.9).curveTo(139.1,136.7,125,147.9).curveTo(62.5,197.4,9.2,215.8).curveTo(4.5,217.4,0,218.7).lineTo(-0.4,218.7).curveTo(-1.8,218.8,-5.4,217.5).closePath();
	this.shape_5.setTransform(230.5,228.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-10.2,0,482.8,447);


(lib.Arrow_Butncopy2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.ARR();
	this.instance.setTransform(37.3,34.5,1,1,0,0,0,22.7,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 0, 0, 0, 0)];
	this.instance.cache(-10,-2,58,54);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(23.8,32.1).curveTo(22.5,32.6,21.1,33.1).curveTo(16.3,34.4,11,34.4).curveTo(-3.4,34.4,-13.6,24.2).curveTo(-23.8,14,-23.8,-0.4).curveTo(-23.8,-14.9,-13.6,-25.1).curveTo(-6.5,-32.2,2.7,-34.3).curveTo(2.9,-34.4,3,-34.4);
	this.shape.setTransform(23.8,35.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill().beginStroke("#333333").setStrokeStyle(4,1,1).moveTo(-21.4,-32.9).curveTo(-17.6,-33.7,-13.4,-33.7).curveTo(1,-33.7,11.2,-23.5).curveTo(21.4,-13.3,21.4,1.1).curveTo(21.4,15.6,11.2,25.8).curveTo(5.9,31.1,-0.6,33.7);
	this.shape_1.setTransform(48.3,33.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#F4D96F").beginStroke().moveTo(-16.3,23.4).curveTo(-25,16.1,-27.8,6.4).curveTo(-30.6,-3.2,-27.1,-13.5).curveTo(-23.6,-23.9,-14.2,-33.2).lineTo(-13.9,-33.3).curveTo(-10.1,-34.2,-5.9,-34.2).curveTo(8.6,-34.2,18.8,-24).curveTo(29,-13.8,29,0.7).curveTo(29,15.1,18.8,25.3).curveTo(13.4,30.7,6.9,33.2).lineTo(4.2,34.2).curveTo(-8.1,30.3,-16.3,23.4).closePath();
	this.shape_2.setTransform(40.7,34.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#CCB55E").beginStroke().moveTo(-12.3,24.2).curveTo(-22.5,14,-22.5,-0.5).curveTo(-22.5,-14.9,-12.3,-25.1).curveTo(-5.1,-32.3,4.1,-34.4).curveTo(-5.3,-25,-8.8,-14.7).curveTo(-12.3,-4.4,-9.5,5.3).curveTo(-6.7,15,2,22.3).curveTo(10.2,29.2,22.5,33).curveTo(17.7,34.4,12.4,34.4).curveTo(-2.1,34.4,-12.3,24.2).closePath();
	this.shape_3.setTransform(22.5,35.3);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.Arrow_Butn = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.ARR();
	this.instance.setTransform(37.3,34.5,1,1,0,0,0,22.7,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 0, 0, 0, 0)];
	this.instance.cache(-10,-2,58,54);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-23.8,32.1).curveTo(-22.5,32.6,-21.1,33.1).curveTo(-16.3,34.4,-11,34.4).curveTo(3.4,34.4,13.6,24.2).curveTo(23.8,14,23.8,-0.4).curveTo(23.8,-14.9,13.6,-25.1).curveTo(6.5,-32.2,-2.7,-34.3).curveTo(-2.9,-34.4,-3,-34.4);
	this.shape.setTransform(45.9,35.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill().beginStroke("#333333").setStrokeStyle(4,1,1).moveTo(21.4,-32.9).curveTo(17.6,-33.7,13.4,-33.7).curveTo(-1,-33.7,-11.2,-23.5).curveTo(-21.4,-13.3,-21.4,1.1).curveTo(-21.4,15.6,-11.2,25.8).curveTo(-5.9,31.1,0.6,33.7);
	this.shape_1.setTransform(21.4,33.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#F4D96F").beginStroke().moveTo(-6.9,33.2).curveTo(-13.4,30.7,-18.8,25.3).curveTo(-29,15.1,-29,0.7).curveTo(-29,-13.8,-18.8,-24).curveTo(-8.6,-34.2,5.9,-34.2).curveTo(10.1,-34.2,13.9,-33.3).lineTo(14.2,-33.2).curveTo(23.6,-23.9,27.1,-13.5).curveTo(30.6,-3.2,27.8,6.4).curveTo(25,16.1,16.3,23.4).curveTo(8.1,30.3,-4.2,34.2).lineTo(-6.9,33.2).closePath();
	this.shape_2.setTransform(29,34.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#CCB55E").beginStroke().moveTo(-22.5,33).curveTo(-10.2,29.2,-2,22.3).curveTo(6.7,15,9.5,5.3).curveTo(12.3,-4.4,8.8,-14.7).curveTo(5.3,-25,-4.1,-34.4).curveTo(5.1,-32.3,12.3,-25.1).curveTo(22.5,-14.9,22.5,-0.5).curveTo(22.5,14,12.3,24.2).curveTo(2.1,34.4,-12.4,34.4).curveTo(-17.7,34.4,-22.5,33).closePath();
	this.shape_3.setTransform(47.2,35.3);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.WinnersCupMC = function() {
	this.initialize();

	// Layer 1
	this.mc_Close = new lib.Arrow_Butncopy();
	this.mc_Close.setTransform(837.7,149,0.712,0.712,0,0,180,34.9,34.9);

	this.instance = new lib.TheCounter();
	this.instance.setTransform(155.4,283.6,0.151,0.151,10.8,0,0,221.5,219);

	this.instance_1 = new lib.BadgeYard();
	this.instance_1.setTransform(390.4,283.6,0.151,0.151,10.8,0,0,221.2,219.1);

	this.instance_2 = new lib.BadgeDeliveryTruck();
	this.instance_2.setTransform(312.7,283.7,0.151,0.151,10.8,0,0,221.5,218.7);

	this.instance_3 = new lib.Badge();
	this.instance_3.setTransform(233.7,283.5,0.151,0.151,10.8,0,0,220.8,218.5);

	this.mc_Twitter = new lib.TW_butn();
	this.mc_Twitter.setTransform(833,445,1,1,0,0,0,19.2,19.2);
	new cjs.ButtonHelper(this.mc_Twitter, 0, 1, 1);

	this.mc_Facebook = new lib.FB_butn();
	this.mc_Facebook.setTransform(832.9,394,1,1,0,0,0,19.1,19.1);
	new cjs.ButtonHelper(this.mc_Facebook, 0, 1, 1);

	this.text = new cjs.Text("you've earned a young grafters winner's cup!", "45px 'Laffayette Comic Pro'");
	this.text.lineHeight = 62;
	this.text.lineWidth = 416;
	this.text.setTransform(126.4,338.8,0.77,0.77);

	this.text_1 = new cjs.Text("YOU HAVE ALL FOUR BADGES", "49px 'VAGRounded BT'");
	this.text_1.lineHeight = 43;
	this.text_1.lineWidth = 358;
	this.text_1.setTransform(126.4,156.6,0.75,0.75);

	this.instance_4 = new lib.WinnersCup();
	this.instance_4.setTransform(601.4,297,0.78,0.78,3,0,0,221.1,218.8);

	this.instance_5 = new lib.EndWhiteBox();
	this.instance_5.setTransform(482.4,288.3,1.082,1.05,0,0,0,365.2,185.9);
	this.instance_5.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.addChild(this.instance_5,this.instance_4,this.text_1,this.text,this.mc_Facebook,this.mc_Twitter,this.instance_3,this.instance_2,this.instance_1,this.instance,this.mc_Close);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(83.1,111,799,379);


(lib.Sure_you_wanna_quit = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_23 = function() {
		this.stop();
	}
	this.frame_29 = function() {
		gamePost.confirmedQuitGame();
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(23).call(this.frame_23).wait(6).call(this.frame_29).wait(1));

	// NoBtn
	this.btn_No = new lib.NoBtn();
	this.btn_No.setTransform(609.6,94.4,0.17,0.17,0,0,0,85.2,31.8);
	new cjs.ButtonHelper(this.btn_No, 0, 1, 2, false, new lib.NoBtn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.btn_No).to({regX:85,regY:31.7,scaleX:1,scaleY:1,x:236.3,y:250.3},4).wait(20).to({regX:85.2,regY:31.8,scaleX:0.17,scaleY:0.17,x:609.6,y:94.4},4).to({_off:true},1).wait(1));

	// YesBtn
	this.btn_Yes = new lib.YesBtn();
	this.btn_Yes.setTransform(647.3,94.4,0.17,0.17,0,0,0,85.2,31.8);
	new cjs.ButtonHelper(this.btn_Yes, 0, 1, 2, false, new lib.YesBtn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.btn_Yes).to({regX:85,regY:31.7,scaleX:1,scaleY:1,x:458.3,y:250.3},4).wait(20).to({regX:85.2,regY:31.8,scaleX:0.17,scaleY:0.17,x:647.3,y:94.4},4).to({_off:true},1).wait(1));

	// Sure
	this.instance = new lib.Sure();
	this.instance.setTransform(685.9,62.5,0.17,0.17,0,0,0,481.6,-167.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:481.8,scaleX:1,scaleY:1},4).wait(20).to({regX:481.6,scaleX:0.17,scaleY:0.17},4).to({_off:true},1).wait(1));

	// Layer 2
	this.mc_bg = new lib._50Percent_white();
	this.mc_bg.setTransform(281,263.4,1,1,0,0,0,500,285.9);

	this.timeline.addTween(cjs.Tween.get(this.mc_bg).wait(30));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-219,-22.5,1000,571.9);


(lib.SteveShirtandTie = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape.setTransform(25.6,90.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_1.setTransform(9.8,86.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-1.6,27.3).curveTo(-2.5,20.5,-2.5,17.8).lineTo(-2.5,16.6).lineTo(-4,16).curveTo(-4.5,16.3,-5.5,16.3).curveTo(-7.3,16.3,-7.7,13.1).lineTo(-7.8,9.7).curveTo(-7.8,6.8,-7.4,5.1).lineTo(-12.4,3.8).lineTo(-15.6,2.8).curveTo(-20,1.4,-21.9,-0.2).curveTo(-24.2,-2.2,-24.2,-5.4).curveTo(-24.2,-7.9,-22.6,-11.7).curveTo(-23.6,-12.4,-24.1,-13.2).curveTo(-24.9,-14.4,-24.9,-19.1).curveTo(-24.9,-22.8,-24.3,-27.4).lineTo(-19.9,-27).curveTo(-20.4,-23.2,-20.4,-19.9).curveTo(-20.4,-17.4,-19.9,-16.5).curveTo(-17.7,-12.8,-11.8,-13.3).lineTo(16.2,-12.9).lineTo(16.2,-16.9).lineTo(16.2,-18.9).curveTo(18.8,-16.8,20.7,-14.2).lineTo(20.5,-10.8).lineTo(20.6,-10.6).curveTo(20.6,-10.3,20.4,-9.9).lineTo(20.4,-9.2).lineTo(20,-8.8).curveTo(19.5,-8.4,18.4,-8.4).lineTo(17.6,-8.4).lineTo(17.6,-8.3).curveTo(17.9,-6.3,17.6,-4.2).curveTo(17.6,-1.4,16.9,2.2).curveTo(15.7,8.2,13.2,9.6).lineTo(6.5,9.6).curveTo(2.1,7.8,-3.4,6.2).curveTo(-3.3,7.6,-3.3,9.6).lineTo(-3.3,11.8).lineTo(-2.9,11.8).curveTo(-1.7,11.8,14.2,15).lineTo(16.5,15.5).lineTo(24.9,17.1).curveTo(24.5,18.2,23.9,19.1).curveTo(22.9,20.8,21.4,22.1).lineTo(17.5,21.4).curveTo(13.4,20.7,9.2,19.7).curveTo(4.1,18.6,0.8,17.7).lineTo(1.2,19.3).curveTo(2.3,23.3,3.1,27.4).lineTo(2,27.4).lineTo(-1.6,27.3).closePath().moveTo(-19.7,-4.9).curveTo(-19.7,-4.3,-11.6,-1.4).lineTo(-6.6,0.3).curveTo(6.7,4.7,9.7,4.7).curveTo(12.9,4.7,13,-2).lineTo(12.7,-8.4).lineTo(-10.7,-8.4).lineTo(-12.9,-8.4).curveTo(-15.5,-8.9,-17.5,-9.4).lineTo(-19.7,-4.9).closePath();
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt2();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#497C59").beginStroke().moveTo(-12.9,29.3).curveTo(-13.5,28.4,-13.5,25.9).curveTo(-13.5,19,-11,9.2).curveTo(-8,-2.2,-2.8,-12.1).curveTo(4.1,-25.2,13.5,-32.6).curveTo(7,-23.3,2.1,-9.6).curveTo(-4.5,8.9,-4.5,25.4).lineTo(-4,32.5).lineTo(-4.9,32.5).lineTo(-6.2,32.6).curveTo(-11,32.6,-12.9,29.3).closePath();
	this.shape_3.setTransform(18,46.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#5A996E").beginStroke().moveTo(-43.8,49.7).lineTo(-46.1,49.2).curveTo(-62,45.9,-63.2,45.9).lineTo(-63.6,45.9).lineTo(-63.6,43.7).curveTo(-63.6,41.8,-63.7,40.3).curveTo(-58.2,42,-53.8,43.7).lineTo(-47.1,43.7).curveTo(-44.6,42.3,-43.4,36.4).curveTo(-42.7,32.7,-42.7,30).curveTo(-42.4,27.9,-42.7,25.9).lineTo(-42.7,25.7).lineTo(-41.9,25.7).curveTo(-40.8,25.7,-40.3,25.4).lineTo(-39.9,25).lineTo(-39.9,24.2).curveTo(-39.7,23.9,-39.7,23.6).lineTo(-39.8,23.3).curveTo(-39.5,17.4,-38.4,4.8).curveTo(-37.4,-6.4,-37.4,-14.2).curveTo(-37.4,-23.7,-38.5,-24).curveTo(-39.4,-24.3,-40.7,-17.6).curveTo(-44.1,-1.3,-44.1,17.3).lineTo(-44.1,21.2).lineTo(-71.2,20.9).lineTo(-71.7,13.8).curveTo(-71.7,-2.7,-65.1,-21.2).curveTo(-60.3,-34.9,-53.8,-44.2).curveTo(-44.3,-51.5,-32.5,-53.1).lineTo(-4.3,-53.1).curveTo(24,-48.6,36.4,-41).curveTo(41.3,-38.1,45.5,-33.7).lineTo(52.5,-25.4).curveTo(64.1,-11.9,66,-9.2).curveTo(71.2,-1.7,71.7,4.6).curveTo(68.4,5.6,64.2,7.9).curveTo(61.4,9.4,59.6,10.7).lineTo(58.3,6.9).curveTo(56.8,3,54.4,-1.6).curveTo(52.2,-6,50.1,-9.2).curveTo(49.2,-11,48.6,-13.9).lineTo(47.7,-18.4).curveTo(46.4,-22.9,42,-23).curveTo(41.8,-21.3,41.8,-19.1).curveTo(41.8,-13.7,48,-1.5).curveTo(54.9,11.9,55.3,14).curveTo(56.6,19.9,58.2,31.1).curveTo(59.4,37.7,61.1,42.9).curveTo(54.7,44.1,47.5,47.2).curveTo(37,51.8,21.1,51.6).lineTo(-22.6,53).curveTo(-29,52.6,-43.8,49.7).closePath();
	this.shape_4.setTransform(85.2,58);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_5.setTransform(86,143.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_6.setTransform(67.5,124.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_7.setTransform(9.8,86.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_8.setTransform(86.9,128);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_9.setTransform(94.8,140.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-70.6,9.4).lineTo(-75.5,7.6).lineTo(-75.4,6.9).curveTo(-74.7,2.8,-74.6,0.7).lineTo(-51.3,0.7).lineTo(-51,7.1).curveTo(-51.1,13.8,-54.3,13.8).curveTo(-57.3,13.8,-70.6,9.4).closePath().moveTo(59.2,-1.5).lineTo(57.8,-7.9).curveTo(61,-9,66,-12.1).lineTo(68.5,-13.8).curveTo(69.3,-12.4,71.8,-9.1).curveTo(75.5,-4.2,75.5,0.3).curveTo(75.5,2.7,66.6,3.5).curveTo(62.5,3.8,60.3,4.4).lineTo(59.2,-1.5).closePath();
	this.shape_10.setTransform(88.9,83);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#000000").beginStroke().moveTo(-25.4,77.7).curveTo(-45.1,74.4,-50.7,72.4).curveTo(-56.7,70.3,-56.8,66.6).curveTo(-62,38.4,-62,32.2).lineTo(-62,31).lineTo(-63.5,30.4).curveTo(-64,30.7,-65,30.7).curveTo(-66.8,30.7,-67.2,27.5).lineTo(-67.3,24.1).curveTo(-67.3,21.2,-66.9,19.5).lineTo(-71.9,18.2).lineTo(-75.1,17.2).curveTo(-79.5,15.8,-81.4,14.2).curveTo(-83.7,12.2,-83.7,9).curveTo(-83.7,6.5,-82.1,2.7).curveTo(-83.1,2,-83.6,1.2).curveTo(-84.4,0,-84.4,-4.7).curveTo(-84.4,-12.1,-82.1,-22.3).curveTo(-79.2,-34.9,-73.8,-45.5).curveTo(-59.1,-74.7,-32.1,-77.3).lineTo(-32.1,-77.7).lineTo(-3.7,-77.7).curveTo(26.5,-73,39.6,-65.3).curveTo(44.9,-62.2,49.3,-57.6).lineTo(56.9,-48.8).lineTo(68.2,-32.5).curveTo(77,-23,77,-16).lineTo(77.2,-16).lineTo(77.5,-15.5).curveTo(77.8,-15.2,77.8,-14.1).curveTo(77.8,-12.9,76.3,-11.3).curveTo(77.3,-10.4,78.5,-8.8).curveTo(84.4,-0.9,84.4,6.1).curveTo(84.4,9.7,84,10.3).curveTo(82.8,12,77,13.5).lineTo(65.4,13.5).curveTo(65.9,17.8,65.9,21.3).curveTo(65.9,26.3,63.6,27.7).lineTo(63.3,27).curveTo(60.6,28.8,54.1,31.4).lineTo(52.9,34.1).curveTo(51.1,38.4,50.9,41.9).curveTo(50.7,43.8,51.2,47.8).curveTo(51.2,49.4,50.6,50.7).curveTo(54.1,51.4,57,52.8).curveTo(65.1,56.8,65.1,64.4).curveTo(65.1,66.2,63.3,67.5).curveTo(60.4,69.7,53.9,69.5).lineTo(22.4,69.5).curveTo(17.8,69.6,15.3,67.3).curveTo(13.4,65.6,13.4,63.7).curveTo(12.6,61.5,11.4,56).lineTo(11.2,54.8).lineTo(11.1,55.2).curveTo(9.1,64.6,7.2,68.4).lineTo(7.4,68.8).lineTo(7.4,68.9).lineTo(7.5,68.9).lineTo(7.5,69.3).lineTo(7.5,70.1).curveTo(7.5,72.5,4.7,74.7).curveTo(1.6,77.1,-3,77.7).closePath().moveTo(-51.3,67.2).curveTo(-50.2,68.5,-41.3,70).curveTo(-26.7,72.5,-25,72.9).lineTo(-15.2,72.9).lineTo(-3.5,72.9).curveTo(-1.7,72,0.9,70.2).lineTo(-8.4,67.8).curveTo(-20.6,64.7,-30,64.7).curveTo(-31.9,64.7,-39.2,65.3).lineTo(-39.5,65.3).curveTo(-47.8,66.1,-49.3,66.6).lineTo(-51.7,66.6).lineTo(-51.3,67.2).closePath().moveTo(-1.4,64.3).curveTo(1.7,65.5,3.9,66.7).curveTo(4.4,63.1,6.4,52.2).curveTo(8.1,42.8,8.8,37.7).lineTo(-21.6,38.2).curveTo(-31.2,37.7,-42,35.8).curveTo(-46.1,35.1,-50.3,34.1).curveTo(-55.4,33,-58.7,32.1).lineTo(-58.3,33.7).curveTo(-54.5,47.4,-54.5,61.9).curveTo(-52.6,61.4,-43.7,60.6).lineTo(-38.3,60.2).curveTo(-33.2,59.8,-30.2,59.8).curveTo(-13.7,59.8,-1.4,64.3).closePath().moveTo(39.9,53.5).curveTo(33.6,54.2,24.6,58.7).curveTo(21.5,60.2,18.1,62.1).curveTo(18.9,62.6,18.8,62.9).curveTo(19.2,63.3,23.6,64.7).lineTo(54,64.7).lineTo(54.9,64.5).lineTo(55.3,64.5).curveTo(57.1,64.2,59.9,63.9).curveTo(53.8,53.3,42.8,53.3).curveTo(41.4,53.3,39.9,53.5).closePath().moveTo(28.4,37.4).lineTo(22,37.5).lineTo(13.4,37.7).lineTo(13.4,38.6).curveTo(13.4,40.6,13.1,43.2).curveTo(14,45.1,14.7,48.1).curveTo(15.9,52.8,16.3,57.4).curveTo(18.2,55.5,21.5,53.8).curveTo(23.1,53,24.9,52.4).curveTo(31.2,50,39.3,49.8).curveTo(43.3,49.6,46.8,50).lineTo(46.5,48.8).curveTo(46.4,47.7,46.4,44.3).curveTo(46.4,36.6,46.9,33.8).curveTo(39.2,35.9,28.4,37.4).closePath().moveTo(-62.8,24).lineTo(-62.8,26.2).lineTo(-62.4,26.2).curveTo(-61.2,26.2,-45.3,29.4).lineTo(-43,29.9).curveTo(-28.2,32.8,-21.8,33.3).lineTo(21.9,31.8).curveTo(37.8,32,48.3,27.4).curveTo(55.5,24.3,61.9,23.2).curveTo(60.2,18,59,11.3).curveTo(57.4,0.1,56.1,-5.7).curveTo(55.7,-7.9,48.8,-21.3).curveTo(42.6,-33.5,42.6,-38.8).curveTo(42.6,-41.1,42.8,-42.8).curveTo(47.2,-42.7,48.5,-38.1).lineTo(49.4,-33.7).curveTo(50,-30.8,50.9,-29).curveTo(53,-25.7,55.2,-21.4).curveTo(57.6,-16.8,59.1,-12.8).lineTo(60.4,-9.1).curveTo(62.2,-10.4,65,-11.9).curveTo(69.2,-14.2,72.5,-15.2).curveTo(72,-21.5,66.8,-28.9).curveTo(64.9,-31.7,53.3,-45.2).lineTo(46.3,-53.5).curveTo(42.1,-57.9,37.2,-60.8).curveTo(24.8,-68.4,-3.5,-72.8).lineTo(-31.7,-72.8).curveTo(-43.5,-71.3,-53,-63.9).curveTo(-62.3,-56.6,-69.3,-43.5).curveTo(-74.5,-33.6,-77.4,-22.2).curveTo(-79.9,-12.4,-79.9,-5.5).curveTo(-79.9,-3,-79.4,-2.1).curveTo(-77.2,1.6,-71.3,1.1).lineTo(-70.4,1.1).lineTo(-43.3,1.5).lineTo(-43.3,-2.5).curveTo(-43.3,-21.1,-39.9,-37.4).curveTo(-38.6,-44.1,-37.7,-43.8).curveTo(-36.6,-43.4,-36.6,-34).curveTo(-36.6,-26.2,-37.6,-15).curveTo(-38.7,-2.4,-39,3.6).lineTo(-38.9,3.8).curveTo(-38.9,4.1,-39.1,4.5).lineTo(-39.1,5.2).lineTo(-39.5,5.6).curveTo(-40,6,-41.1,6).lineTo(-41.9,6).lineTo(-41.9,6.1).curveTo(-41.6,8.1,-41.9,10.2).curveTo(-41.9,13,-42.6,16.6).curveTo(-43.8,22.6,-46.3,24).lineTo(-53,24).curveTo(-57.4,22.2,-62.9,20.6).curveTo(-62.8,22,-62.8,24).closePath().moveTo(-79.2,9.5).curveTo(-79.2,10.1,-71.1,12.9).lineTo(-66.1,14.7).curveTo(-52.8,19.1,-49.8,19.1).curveTo(-46.6,19.1,-46.5,12.4).lineTo(-46.8,6).lineTo(-70.2,6).lineTo(-72.4,6).curveTo(-75,5.5,-77,5).lineTo(-79.2,9.5).closePath().moveTo(70.5,-6.8).curveTo(65.5,-3.7,62.3,-2.6).lineTo(63.7,3.8).lineTo(64.8,9.7).curveTo(67,9.1,71.1,8.8).curveTo(80,8,80,5.6).curveTo(80,1.1,76.3,-3.8).curveTo(73.8,-7.1,73,-8.5).lineTo(70.5,-6.8).closePath();
	this.shape_11.setTransform(84.4,77.7);

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-8.8,168.9,164.3);


(lib.SteveShirt = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).lineTo(-12.1,-0.7).lineTo(-12.1,-0.8).lineTo(-11.9,-1.7).lineTo(-11.5,-4.7).lineTo(-11.4,-6.6).lineTo(-11,-6.6).lineTo(12,-6.6).lineTo(12,-5.1).lineTo(12,-5.1).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape.setTransform(25.6,90.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#CCB59F").beginStroke().moveTo(-7.2,9.4).lineTo(-7.2,9.3).lineTo(-7,8.7).curveTo(-7,6.4,-6.1,4.6).curveTo(-4.5,2,-3.3,-0.7).curveTo(-2.2,-3.2,1.1,-4.8).lineTo(1.3,-6.4).lineTo(1.4,-11.1).lineTo(1.4,-12.8).lineTo(7.2,-12.9).lineTo(6.5,-9.6).curveTo(5.8,-6.5,5.2,-4.9).curveTo(5.5,-4.5,3.8,-2).curveTo(2.8,0.5,2.4,4.4).lineTo(2.2,5.9).lineTo(1.8,5.9).lineTo(1.7,7.8).lineTo(1.3,10.8).lineTo(1.1,11.7).lineTo(1.1,11.8).lineTo(1.1,12.1).lineTo(0.9,12.9).curveTo(-7.2,10,-7.2,9.4).closePath();
	this.shape_1.setTransform(12.4,77.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#000000").beginStroke().moveTo(-4.5,19.8).lineTo(-4.6,18.3).curveTo(-4.6,15.5,-4.2,13.7).lineTo(-9.2,12.4).lineTo(-12.4,11.5).curveTo(-16.8,10.1,-18.7,8.4).curveTo(-21,6.5,-21,3.2).curveTo(-21,2.5,-19.8,-0.4).curveTo(-18.7,-3.2,-18.5,-3.4).lineTo(-17.8,-4.5).lineTo(-17.7,-4.7).lineTo(-17.2,-5.5).curveTo(-15.1,-9,-13.6,-12.5).lineTo(-13.2,-15.2).curveTo(-13.1,-17.1,-12.4,-19.8).lineTo(-8,-18.5).lineTo(-7.9,-18.5).lineTo(-7.9,-16.8).lineTo(-8,-12.1).lineTo(-8.2,-10.5).curveTo(-11.5,-8.9,-12.6,-6.4).curveTo(-13.8,-3.7,-15.4,-1.1).curveTo(-16.3,0.7,-16.3,3).lineTo(-16.4,3.7).lineTo(-16.5,3.7).curveTo(-16.5,4.3,-8.3,7.2).lineTo(-3.4,8.9).curveTo(9.9,13.4,12.9,13.4).curveTo(16.1,13.4,16.2,6.7).lineTo(15.9,1.7).lineTo(15.9,1.7).curveTo(16.1,1,16.1,0).lineTo(16.1,0).lineTo(15.7,-2.2).lineTo(15.3,-4.3).curveTo(13.9,-12,13.6,-14.7).lineTo(13,-18.8).lineTo(16.4,-18.8).lineTo(16.7,-16.7).curveTo(16.9,-15.3,18.1,-9.9).curveTo(18.8,-6.8,19,-4.3).lineTo(20.8,0.2).lineTo(20.8,0.4).curveTo(21.1,2.3,20.8,4.5).curveTo(20.8,7.2,20.1,10.8).curveTo(18.9,16.8,16.4,18.2).lineTo(9.7,18.2).curveTo(5.3,16.5,-0.2,14.8).curveTo(-0.1,16.3,-0.1,18.2).lineTo(-0.1,19.8).closePath();
	this.shape_2.setTransform(21.7,83.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#EACFB7").beginStroke().moveTo(11.4,8.7).lineTo(-11.6,8.7).lineTo(-11.4,7.2).curveTo(-11,3.3,-10,0.8).curveTo(-8.3,-1.6,-8.6,-2.1).curveTo(-8,-3.6,-7.3,-6.7).lineTo(-6.6,-10).lineTo(8.5,-10.2).lineTo(9.1,-6.2).curveTo(9.4,-3.5,10.8,4.2).lineTo(11.2,6.3).lineTo(11.6,8.6).lineTo(11.6,8.6).curveTo(11.6,9.6,11.4,10.2).lineTo(11.4,8.7).closePath();
	this.shape_3.setTransform(26.2,75);

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-7.3,2.1).lineTo(-12.3,0.4).lineTo(-12.1,-0.3).curveTo(-11.4,-4.4,-11.4,-6.6).lineTo(12,-6.6).lineTo(12.3,-0.1).curveTo(12.2,6.6,9,6.6).curveTo(6,6.6,-7.3,2.1).closePath();
	this.shape_4.setTransform(25.6,90.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#CCB59F").beginStroke().moveTo(-4.5,0.5).lineTo(-2.3,-4).curveTo(-0.3,-3.4,2.2,-3).lineTo(4.5,-3).curveTo(4.4,-0.8,3.7,3.3).lineTo(3.6,4).curveTo(-4.5,1.1,-4.5,0.5).closePath();
	this.shape_5.setTransform(9.8,86.7);

	// Layer 1
	this.instance = new lib.shirt();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#EACFB7").beginStroke().moveTo(-3,15).curveTo(-2.7,12.5,-2.7,11.7).lineTo(-2.7,11.7).curveTo(-4.4,12.7,-4.7,12.8).lineTo(-6.1,12.7).curveTo(-7.4,13.9,-8,13.9).curveTo(-9.4,13.9,-9.8,12.9).lineTo(-9.9,12.9).curveTo(-11.4,12.9,-11.7,11.5).lineTo(-11.7,10.7).curveTo(-11.4,9.8,-11.3,8.5).curveTo(-10.9,4.6,-9.9,2.1).curveTo(-8.2,-0.3,-8.5,-0.8).curveTo(-7.9,-2.3,-7.2,-5.4).curveTo(-6.1,-9.9,-6.1,-12.3).lineTo(-6.2,-13.3).lineTo(-6.1,-14).lineTo(-6.2,-14.6).lineTo(-5.9,-14.9).curveTo(-5,-16.1,-0.4,-16.2).curveTo(2.7,-16.2,4.1,-15.6).curveTo(4.2,-16.5,5.1,-16.9).curveTo(6.2,-17.4,7.4,-16.3).lineTo(7.6,-16).lineTo(7.7,-15.7).curveTo(8,-13.7,8.1,-12.3).curveTo(8.3,-9.9,9.2,-4.9).curveTo(9.5,-2.2,10.9,5.5).lineTo(11.3,7.7).lineTo(11.7,9.9).lineTo(11.7,9.9).curveTo(11.7,10.9,11.5,11.5).curveTo(11.1,13,9.6,13).curveTo(8.3,13,8,12.1).lineTo(7.7,12.1).curveTo(7.3,12.1,7,11.8).curveTo(6.1,13,4,13).curveTo(3.4,13,3,12.9).curveTo(2.3,14.7,1.1,14.7).lineTo(0.9,14.7).curveTo(0.2,17.1,-0.9,17.1).curveTo(-2.9,17.1,-3,15).closePath();
	this.shape_6.setTransform(26.1,73.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#421F11").beginStroke().moveTo(-26.6,7.1).curveTo(-28.3,6.7,-42.9,4.2).curveTo(-51.8,2.7,-52.9,1.4).lineTo(-53.3,0.8).lineTo(-50.9,0.8).curveTo(-49.4,0.3,-41.1,-0.5).lineTo(-40.8,-0.5).curveTo(-36.3,3.3,-22.8,6).lineTo(-16.8,7.1).closePath().moveTo(22,-1.1).curveTo(17.6,-2.5,17.2,-2.9).curveTo(17.3,-3.2,16.5,-3.7).curveTo(19.9,-5.6,23,-7.1).curveTo(26.9,-4,39.8,-2.4).curveTo(42.4,-2,53.3,-1.3).lineTo(52.4,-1.1).closePath();
	this.shape_7.setTransform(86,143.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#39637F").beginStroke().moveTo(-41.3,-13.3).lineTo(-41.8,-14.9).curveTo(-38.5,-13.9,-33.4,-12.8).curveTo(-29.1,-11.9,-25,-11.2).lineTo(-23.2,-0.4).curveTo(-21.7,9,-21.4,13.2).lineTo(-26.8,13.6).curveTo(-35.7,14.4,-37.6,14.9).curveTo(-37.6,0.4,-41.3,-13.3).closePath().moveTo(31.7,1.1).curveTo(30.9,-1.9,30.1,-3.7).curveTo(30.3,-6.4,30.3,-8.3).lineTo(30.3,-9.3).lineTo(39,-9.4).lineTo(39,-8.7).curveTo(38.9,-7.1,40.6,-0).lineTo(41.8,5.4).curveTo(40.1,6,38.4,6.9).curveTo(35.1,8.5,33.2,10.5).curveTo(32.9,5.8,31.7,1.1).closePath();
	this.shape_8.setTransform(67.5,124.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#CCB59F").beginStroke().moveTo(-7.4,13.2).lineTo(-7.2,12.5).curveTo(-7.2,10.2,-6.3,8.4).curveTo(-4.7,5.8,-3.5,3.1).curveTo(-2.4,0.6,0.9,-1).lineTo(1.1,-2.6).lineTo(1.2,-7.3).curveTo(1.2,-10.3,1,-11.5).lineTo(1.3,-13.6).curveTo(1.3,-14.4,1.1,-14.9).curveTo(2.1,-15.9,4,-16.5).curveTo(7,-17.4,7.3,-15).lineTo(7.4,-14.3).lineTo(7.3,-13.6).lineTo(7.4,-12.7).curveTo(7.4,-10.3,6.3,-5.8).curveTo(5.6,-2.7,5,-1.1).curveTo(5.3,-0.7,3.6,1.8).curveTo(2.6,4.3,2.2,8.2).curveTo(2.1,9.5,1.8,10.4).lineTo(1.6,11).curveTo(1.4,12.9,0.9,16).lineTo(0.7,16.7).curveTo(-7.4,13.8,-7.4,13.2).closePath();
	this.shape_9.setTransform(12.6,74);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#477A9D").beginStroke().moveTo(-3.9,14.1).curveTo(-16.1,9.6,-32.7,9.6).curveTo(-35.7,9.6,-40.8,9.9).curveTo(-41,5.7,-42.6,-3.7).lineTo(-44.4,-14.4).curveTo(-33.6,-12.5,-24.1,-12.1).lineTo(6.3,-12.5).curveTo(5.7,-7.5,4,1.9).curveTo(2,12.8,1.5,16.4).curveTo(-0.8,15.2,-3.9,14.1).closePath().moveTo(21.2,-3.3).curveTo(19.6,-10.4,19.6,-12).lineTo(19.6,-12.7).lineTo(26,-12.8).curveTo(36.8,-14.3,44.4,-16.4).curveTo(43.9,-13.7,43.9,-5.9).curveTo(43.9,-2.6,44.1,-1.4).lineTo(44.3,-0.2).curveTo(40.8,-0.6,36.8,-0.5).curveTo(28.8,-0.2,22.4,2.1).lineTo(21.2,-3.3).closePath();
	this.shape_10.setTransform(86.9,128);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#65301B").beginStroke().moveTo(-25.5,9.8).lineTo(-31.5,8.7).curveTo(-45,5.9,-49.5,2.2).curveTo(-42.2,1.6,-40.3,1.6).curveTo(-31,1.6,-18.7,4.7).lineTo(-9.4,7.1).curveTo(-12,8.9,-13.9,9.8).closePath().moveTo(31.1,0.3).curveTo(18.2,-1.3,14.2,-4.5).curveTo(23.3,-8.9,29.6,-9.6).curveTo(42.7,-11.2,49.5,0.8).curveTo(46.7,1.1,44.9,1.4).lineTo(44.6,1.4).curveTo(33.7,0.6,31.1,0.3).closePath();
	this.shape_11.setTransform(94.8,140.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#EAD0B7").beginStroke().moveTo(-70.6,15.8).lineTo(-75.5,14).lineTo(-75.4,13.3).curveTo(-74.9,10.3,-74.7,8.3).lineTo(-74.5,7.7).lineTo(-74.5,8.5).curveTo(-74.2,9.9,-72.7,9.9).lineTo(-72.6,9.9).curveTo(-72.2,10.9,-70.8,10.9).curveTo(-70.2,10.9,-68.9,9.7).lineTo(-67.5,9.8).curveTo(-67.2,9.7,-65.5,8.7).lineTo(-65.5,8.7).curveTo(-65.5,9.5,-65.8,12).curveTo(-65.7,14.1,-63.7,14.1).curveTo(-62.5,14.1,-61.9,11.7).lineTo(-61.7,11.7).curveTo(-60.5,11.7,-59.7,9.9).curveTo(-59.4,10,-58.8,10).curveTo(-56.7,10,-55.8,8.8).curveTo(-55.4,9.1,-55.1,9.1).lineTo(-54.8,9.1).curveTo(-54.5,10,-53.2,10).curveTo(-51.7,10,-51.3,8.6).lineTo(-51,13.5).curveTo(-51.1,20.2,-54.3,20.2).curveTo(-57.3,20.2,-70.6,15.8).closePath().moveTo(56.6,-2.6).curveTo(52.8,-14.8,52.5,-16.1).curveTo(54.4,-16.7,55.5,-17.7).lineTo(58.4,-20.2).lineTo(60.9,-16.1).curveTo(63.5,-12.1,65.9,-8.9).curveTo(66.6,-7.9,68.3,-7).lineTo(70.1,-6).curveTo(72.9,-4.2,74.4,0.3).curveTo(75.5,3.6,75.5,6.7).curveTo(75.5,9.1,66.6,9.9).curveTo(62.5,10.2,60.3,10.8).curveTo(60,8.5,56.6,-2.6).closePath();
	this.shape_12.setTransform(88.9,76.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.beginFill("#000000").beginStroke().moveTo(-25.8,55.1).curveTo(-45.4,51.7,-51.1,49.7).curveTo(-57.1,47.7,-57.2,43.9).curveTo(-62.4,15.8,-62.4,9.6).lineTo(-62.4,8.4).lineTo(-63.9,7.8).curveTo(-64.4,8,-65.4,8).curveTo(-67.2,8,-67.6,4.8).lineTo(-67.6,2.9).lineTo(-67.6,1.4).curveTo(-67.6,-1.4,-67.3,-3.2).lineTo(-72.3,-4.5).lineTo(-75.4,-5.4).curveTo(-79.9,-6.8,-81.8,-8.5).curveTo(-84,-10.4,-84.1,-13.7).curveTo(-84.1,-14.4,-82.9,-17.3).curveTo(-81.8,-20.1,-81.6,-20.3).lineTo(-80.9,-21.4).lineTo(-80.8,-21.6).lineTo(-80.3,-22.4).curveTo(-78.2,-25.9,-76.6,-29.4).lineTo(-76.3,-32.1).curveTo(-76.1,-34.3,-75.2,-37.6).curveTo(-75.2,-38.8,-74.5,-39.6).curveTo(-73.8,-41.3,-72.5,-41.7).curveTo(-71.5,-42,-71.1,-41.3).curveTo(-70.9,-40.8,-70.9,-40).lineTo(-71.1,-37.9).curveTo(-71,-36.7,-71,-33.7).lineTo(-71,-29).lineTo(-71.3,-27.4).curveTo(-74.5,-25.8,-75.7,-23.3).curveTo(-76.9,-20.6,-78.4,-18).curveTo(-79.3,-16.2,-79.3,-13.9).lineTo(-79.6,-13.2).curveTo(-79.5,-12.6,-71.4,-9.7).lineTo(-66.4,-8).curveTo(-53.1,-3.5,-50.2,-3.5).curveTo(-47,-3.5,-46.9,-10.2).lineTo(-47.2,-15.2).curveTo(-47,-15.9,-46.9,-16.8).lineTo(-46.9,-16.9).lineTo(-47.4,-19.1).lineTo(-47.7,-21.2).curveTo(-49.1,-28.9,-49.5,-31.6).curveTo(-50.3,-36.7,-50.5,-39).curveTo(-50.6,-40.4,-51,-42.4).lineTo(-51,-42.8).lineTo(-51.3,-43).curveTo(-51,-45.5,-49.3,-44).curveTo(-47.3,-42.2,-47.3,-40.3).lineTo(-46.4,-33.6).curveTo(-46.2,-32.2,-44.9,-26.8).curveTo(-44.3,-23.7,-44.1,-21.2).lineTo(-42.2,-16.7).lineTo(-42.2,-16.5).curveTo(-42,-14.6,-42.2,-12.4).curveTo(-42.2,-9.7,-43,-6.1).curveTo(-44.1,-0.1,-46.6,1.3).lineTo(-53.4,1.3).curveTo(-57.8,-0.4,-63.3,-2.1).curveTo(-63.1,-0.6,-63.1,1.3).lineTo(-63.1,2.9).lineTo(-63.1,3.5).lineTo(-62.8,3.5).curveTo(-61.6,3.5,-45.7,6.8).lineTo(-43.3,7.2).curveTo(-28.6,10.2,-22.2,10.6).lineTo(21.6,9.1).curveTo(28.7,9.2,36,7.9).curveTo(41.5,6.9,47.9,4.8).curveTo(51.4,3.6,55.3,2.5).curveTo(58.4,1.6,61.5,0.5).curveTo(59.8,-4.7,58.7,-11.4).curveTo(57,-23,55.8,-28.4).curveTo(55.4,-29.7,52.4,-36.2).lineTo(53.3,-47.5).lineTo(54.9,-44.2).lineTo(56.7,-39.8).curveTo(56.9,-38.5,60.7,-26.4).curveTo(64.1,-15.2,64.4,-12.9).curveTo(66.7,-13.5,70.7,-13.9).curveTo(79.6,-14.7,79.6,-17).curveTo(79.7,-20.1,78.5,-23.5).curveTo(77,-27.9,74.2,-29.8).lineTo(72.4,-30.8).curveTo(70.7,-31.7,70.1,-32.6).curveTo(67.6,-35.8,65,-39.9).lineTo(62.5,-44).lineTo(59.6,-41.4).curveTo(58.5,-40.5,56.7,-39.8).curveTo(58.2,-41,59.1,-45.7).curveTo(60,-49.9,62.1,-50.6).curveTo(62,-52.7,61.2,-55.1).lineTo(66,-55.1).curveTo(66.6,-53,66.6,-51.5).lineTo(66.8,-51.5).lineTo(67,-51).curveTo(67.3,-50.6,67.4,-49.5).curveTo(67.4,-48.6,65.9,-46.8).curveTo(66,-46.6,68.8,-41.1).curveTo(71.6,-35.8,72.3,-35.5).curveTo(77.6,-33.5,81.1,-26.7).curveTo(84.1,-21,84,-16.6).curveTo(84,-13,83.6,-12.3).curveTo(82.4,-10.7,76.7,-9.2).lineTo(65,-9.2).curveTo(65.5,-4.8,65.5,-1.4).curveTo(65.5,3.7,63.3,5).lineTo(63,4.3).curveTo(60.3,6.2,53.7,8.8).lineTo(52.5,11.4).curveTo(50.7,15.8,50.5,19.3).curveTo(50.3,21.2,50.8,25.1).curveTo(50.8,26.7,50.3,28).curveTo(53.8,28.8,56.7,30.2).curveTo(64.8,34.1,64.8,41.8).curveTo(64.8,43.5,63,44.9).curveTo(60.1,47.1,53.5,46.8).lineTo(22,46.8).curveTo(17.5,47,14.9,44.6).curveTo(13.1,42.9,13,41.1).curveTo(12.2,38.8,11,33.4).lineTo(10.8,32.2).lineTo(10.7,32.6).curveTo(8.7,42,6.8,45.8).lineTo(7.1,46.2).lineTo(7.1,46.2).lineTo(7.2,46.3).lineTo(7.2,46.7).lineTo(7.2,47.5).curveTo(7.2,49.9,4.3,52.1).curveTo(1.2,54.5,-3.4,55.1).closePath().moveTo(-51.7,44.6).curveTo(-50.6,45.8,-41.7,47.4).curveTo(-27.1,49.9,-25.3,50.3).lineTo(-15.6,50.3).lineTo(-3.9,50.3).curveTo(-2.1,49.3,0.5,47.5).lineTo(-8.8,45.2).curveTo(-21,42,-30.3,42).curveTo(-32.3,42,-39.5,42.7).lineTo(-39.9,42.7).curveTo(-48.1,43.4,-49.6,43.9).lineTo(-52.1,43.9).lineTo(-51.7,44.6).closePath().moveTo(-1.8,41.7).curveTo(1.3,42.8,3.6,44).curveTo(4.1,40.4,6,29.5).curveTo(7.8,20.1,8.4,15.1).lineTo(-22,15.5).curveTo(-31.6,15.1,-42.3,13.2).curveTo(-46.4,12.4,-50.7,11.5).curveTo(-55.8,10.4,-59.1,9.4).lineTo(-58.6,11.1).curveTo(-54.9,24.7,-54.9,39.2).curveTo(-53,38.7,-44.1,38).lineTo(-38.7,37.5).curveTo(-33.6,37.2,-30.6,37.2).curveTo(-14.1,37.2,-1.8,41.7).closePath().moveTo(39.6,30.8).curveTo(33.2,31.6,24.2,36).curveTo(21.1,37.5,17.7,39.4).curveTo(18.5,40,18.4,40.3).curveTo(18.9,40.6,23.2,42).lineTo(53.6,42).lineTo(54.6,41.9).lineTo(54.9,41.8).curveTo(56.7,41.5,59.5,41.3).curveTo(53.4,30.6,42.5,30.6).curveTo(41,30.6,39.6,30.8).closePath().moveTo(28.1,14.8).lineTo(21.7,14.9).lineTo(13,15).lineTo(13,16).curveTo(13,17.9,12.8,20.6).curveTo(13.6,22.4,14.4,25.4).curveTo(15.6,30.2,15.9,34.8).curveTo(17.8,32.8,21.1,31.2).curveTo(22.8,30.4,24.5,29.7).curveTo(30.8,27.4,38.9,27.1).curveTo(42.9,27,46.4,27.4).lineTo(46.1,26.2).curveTo(46,25,46,21.7).curveTo(46,13.9,46.5,11.2).curveTo(38.8,13.3,28.1,14.8).closePath().moveTo(56.7,-39.8).lineTo(56.7,-39.8).closePath();
	this.shape_13.setTransform(84.8,100.4);

	this.addChild(this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.instance,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.8,0,168.1,155.5);


(lib.Steve_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 2
	this.instance = new lib.Male_Head_01_Front();
	this.instance.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 1
	this.instance_1 = new lib.Male_Body_02_Front();
	this.instance_1.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_2 = new lib.SteveShirt();
	this.instance_2.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_3 = new lib.SteveHoodie();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.Motif();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.SteveShirtandTie();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.PostRoomBadgeAlreadyGot = function() {
	this.initialize();

	// Layer 1
	this.mc_Leaderboard = new lib.LeaderboardButton();
	this.mc_Leaderboard.setTransform(122.6,413.2);
	new cjs.ButtonHelper(this.mc_Leaderboard, 0, 1, 2, false, new lib.LeaderboardButton(), 3);

	this.mc_Twitter = new lib.TW_butn();
	this.mc_Twitter.setTransform(833,445,1,1,0,0,0,19.2,19.2);
	new cjs.ButtonHelper(this.mc_Twitter, 0, 1, 1);

	this.mc_Facebook = new lib.FB_butn();
	this.mc_Facebook.setTransform(832.9,394,1,1,0,0,0,19.1,19.1);
	new cjs.ButtonHelper(this.mc_Facebook, 0, 1, 1);

	this.mc_FinalScore = new lib.FinalScorecopy();
	this.mc_FinalScore.setTransform(246.7,224.7,1.38,1.21,0,0,0,91.1,33.5);

	this.text = new cjs.Text("You've already earned a post room badge", "40px 'Laffayette Comic Pro'");
	this.text.lineHeight = 53;
	this.text.lineWidth = 496;
	this.text.setTransform(118.9,319.8,0.77,0.77);

	this.text_1 = new cjs.Text("FINAL SCORE", "49px 'VAGRounded BT'");
	this.text_1.lineHeight = 60;
	this.text_1.lineWidth = 552;
	this.text_1.setTransform(121,134.1,0.75,0.75);

	this.instance = new lib.Badge();
	this.instance.setTransform(652.7,290.8,0.78,0.78,10.8,0,0,221.1,218.7);

	this.instance_1 = new lib.EndWhiteBox();
	this.instance_1.setTransform(482.4,288.3,1.082,1.05,0,0,0,365.2,185.9);
	this.instance_1.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.addChild(this.instance_1,this.instance,this.text_1,this.text,this.mc_FinalScore,this.mc_Facebook,this.mc_Twitter,this.mc_Leaderboard);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(83.1,91,799,408.4);


(lib.PostRoomBadge = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.Gliss();
	this.instance.setTransform(781.7,277,0.78,0.78,0,0,0,41.6,43.7);

	// Layer 1
	this.mc_Leaderboard = new lib.LeaderboardButton();
	this.mc_Leaderboard.setTransform(122.6,413.2);
	new cjs.ButtonHelper(this.mc_Leaderboard, 0, 1, 2, false, new lib.LeaderboardButton(), 3);

	this.mc_Twitter = new lib.TW_butn();
	this.mc_Twitter.setTransform(833,445,1,1,0,0,0,19.2,19.2);
	new cjs.ButtonHelper(this.mc_Twitter, 0, 1, 1);

	this.mc_Facebook = new lib.FB_butn();
	this.mc_Facebook.setTransform(832.9,394,1,1,0,0,0,19.1,19.1);
	new cjs.ButtonHelper(this.mc_Facebook, 0, 1, 1);

	this.mc_FinalScore = new lib.FinalScorecopy();
	this.mc_FinalScore.setTransform(246.7,224.7,1.38,1.21,0,0,0,91.1,33.5);

	this.text = new cjs.Text("You've earned a post room badge", "49px 'Laffayette Comic Pro'");
	this.text.lineHeight = 62;
	this.text.lineWidth = 483;
	this.text.setTransform(118.9,313.6,0.77,0.77);

	this.text_1 = new cjs.Text("FINAL SCORE", "49px 'VAGRounded BT'");
	this.text_1.lineHeight = 60;
	this.text_1.lineWidth = 552;
	this.text_1.setTransform(121,134.1,0.75,0.75);

	this.instance_1 = new lib.Badge();
	this.instance_1.setTransform(652.7,290.8,0.78,0.78,10.8,0,0,221.1,218.7);

	this.instance_2 = new lib.EndWhiteBox();
	this.instance_2.setTransform(482.4,288.3,1.082,1.05,0,0,0,365.2,185.9);
	this.instance_2.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.addChild(this.instance_2,this.instance_1,this.text_1,this.text,this.mc_FinalScore,this.mc_Facebook,this.mc_Twitter,this.mc_Leaderboard,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(83.1,91,799,408.4);


(lib.PeppaSkirt = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.GirlOutfit04();
	this.instance.setTransform(82.1,89.6,1.12,1.12,0,0,0,62.6,83.9);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#4E2A3F").setStrokeStyle(1,1,1).moveTo(31.2,6.8).curveTo(31,6.6,30.9,6.4).curveTo(25,-3.1,15.4,-5.9).curveTo(12.3,-6.8,5.6,-6.8).curveTo(-5.5,-6.8,-16.6,-4.1).curveTo(-24.8,-2.1,-29.1,0.2).curveTo(-30.3,0.9,-31.2,1.5);
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#BA986F").beginStroke().moveTo(8,56.3).curveTo(8.2,55.7,8.2,53.5).curveTo(8.6,49.5,8.1,46.3).curveTo(7.7,44.1,6.9,42.6).curveTo(11.1,42.5,15.3,42.2).lineTo(16.6,52.2).curveTo(15,52.5,13.4,53).curveTo(9.4,54.3,8.1,56.4).lineTo(8,56.3).closePath().moveTo(-23.6,45.5).curveTo(-26.2,38.1,-28,35.2).curveTo(-26.3,35.6,-22.7,38.2).curveTo(-19.3,40.8,-17.4,41.1).lineTo(-13.7,53).curveTo(-17.2,53.6,-20.1,54.9).lineTo(-23.6,45.5).closePath().moveTo(-65,1.9).curveTo(-69.2,-2.1,-70.7,-8.6).curveTo(-71,-9.9,-69.9,-11.9).curveTo(-68.2,-14.7,-67.2,-16.7).curveTo(-62.6,-24.9,-61.8,-36).curveTo(-61.6,-38.6,-59.7,-47.1).lineTo(-57.7,-56.4).lineTo(-56.5,-55.9).curveTo(-55.7,-55.6,-55.2,-55.5).curveTo(-55.9,-51.3,-57.1,-37.5).curveTo(-58.4,-21.8,-58.5,-21.6).curveTo(-59.9,-17.1,-62.8,-12.7).curveTo(-64.5,-9,-63,-2.8).lineTo(-61.9,1.1).lineTo(-60.8,4.9).curveTo(-63,3.9,-65,1.9).closePath().moveTo(64.1,-0.1).curveTo(61.1,-2.4,59.6,-5.9).curveTo(59.1,-7.1,58.6,-9.5).curveTo(58.4,-11.4,58.4,-13.8).curveTo(57.4,-20.1,54.4,-25.5).lineTo(53.8,-26.5).curveTo(52.7,-28.2,52.1,-28.6).lineTo(51.3,-31.7).curveTo(49.9,-37.2,49.4,-40.3).lineTo(47.9,-47.4).curveTo(53.7,-36.7,57.5,-28).curveTo(60.9,-19.7,61.2,-16.6).curveTo(61.7,-11.5,63.8,-7.4).curveTo(66.5,-2.5,70.8,-1.5).lineTo(68.8,-0.9).curveTo(68.3,-0.7,68,-0.4).lineTo(68,-0.5).lineTo(66.9,-0.1).closePath();
	this.shape_1.setTransform(75,96.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#D5AE81").beginStroke().moveTo(-10.1,53.4).curveTo(-14,53.1,-17.6,53.4).curveTo(-19.4,53.6,-21,53.9).lineTo(-24.7,42).curveTo(-15,43.5,-5,43.5).curveTo(-5,44.4,-4.8,45.2).lineTo(-4.1,50.3).lineTo(-3.6,54.7).curveTo(-6,53.8,-10.1,53.4).closePath().moveTo(19.4,52.6).curveTo(13.8,52.2,9.3,53).lineTo(8,43.1).curveTo(19.6,42.1,31.1,39.5).curveTo(30.6,40.7,30.3,41.9).curveTo(30.1,43,28.2,48.2).curveTo(26.8,51.8,26.6,53.8).curveTo(23.3,52.8,19.4,52.6).closePath().moveTo(-63.1,6.8).lineTo(-64.6,6.6).curveTo(-65.7,6.5,-66.7,6.2).lineTo(-68.1,5.7).lineTo(-69.2,1.9).lineTo(-70.3,-1.9).curveTo(-71.8,-8.2,-70.1,-11.8).curveTo(-67.2,-16.3,-65.8,-20.7).curveTo(-65.7,-21,-64.4,-36.7).curveTo(-63.2,-50.5,-62.5,-54.7).curveTo(-61.4,-54.6,-60,-54).lineTo(-58.1,-53.2).curveTo(-55.4,-52.2,-50.8,-48.7).lineTo(-49.6,-47.8).lineTo(-50.4,-45.9).curveTo(-53,-39.5,-55.3,-28.6).curveTo(-57.7,-17.4,-57.4,-15.4).lineTo(-57.4,-15.3).curveTo(-57.2,-14.4,-54.5,-9.5).lineTo(-53.6,-7.8).curveTo(-50.3,-1.5,-50,-0.4).lineTo(-49.9,-0.3).curveTo(-49.4,1.8,-50.9,3.6).lineTo(-51.6,4.3).curveTo(-54.3,6.8,-60.6,6.8).lineTo(-63.1,6.8).closePath().moveTo(56.5,-6.5).curveTo(54.4,-10.7,53.9,-15.7).curveTo(53.6,-18.9,50.2,-27.1).curveTo(46.4,-35.9,40.5,-46.6).curveTo(39.6,-51.1,39.4,-52.9).lineTo(40.7,-53.3).lineTo(40.9,-53.3).lineTo(41.6,-53.4).curveTo(43.7,-53.9,46,-53.9).lineTo(46.4,-53.8).lineTo(48.6,-54.7).curveTo(49.3,-51.5,53.7,-41.3).curveTo(56,-35.8,56.7,-33.6).curveTo(57.1,-32.4,57.3,-31).curveTo(58.3,-27.3,60.8,-24.2).curveTo(61.8,-22.9,65.3,-19.5).curveTo(71,-13.9,71,-7.2).curveTo(71,-2.8,65.4,-1.2).lineTo(63.5,-0.7).curveTo(59.1,-1.7,56.5,-6.5).closePath();
	this.shape_2.setTransform(82.3,95.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#371E2D").beginStroke().moveTo(-16.8,4).curveTo(-22.1,3.6,-22.9,3.3).lineTo(-22,0.9).curveTo(-21.5,-0.6,-19.9,-1.5).lineTo(-18.5,-2.1).lineTo(-17.9,-2.3).lineTo(-16.3,-2.6).lineTo(-16.4,-1.9).curveTo(-16.3,1.3,-6.5,3.8).lineTo(-3.9,4.4).curveTo(-6.8,4.5,-10.3,4.5).curveTo(-11.9,4.5,-16.8,4).closePath().moveTo(13.1,1.4).curveTo(13.2,0.1,12.8,-1.2).curveTo(13.4,-1.2,13.9,-1.9).curveTo(14.7,-2.9,15.2,-3.3).curveTo(16.2,-4,17.7,-4.5).curveTo(18.8,0,20.1,1.5).curveTo(20.9,2.5,22.8,3.3).lineTo(15.7,3.9).curveTo(13.7,4,13,4.1).curveTo(13.2,3.1,13.1,1.4).closePath();
	this.shape_3.setTransform(73,158.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#4D2A3E").beginStroke().moveTo(-19,4.3).curveTo(-28.8,1.8,-28.8,-1.4).lineTo(-28.8,-2.1).curveTo(-22.9,-3.4,-8.9,-3.3).curveTo(-6.9,-3.1,-5.3,-1.6).curveTo(-3.8,-0.1,-3.8,1.7).lineTo(-4.3,2.2).curveTo(-5.1,2.8,-6.7,3.4).curveTo(-10.1,4.5,-16.4,4.9).lineTo(-19,4.3).closePath().moveTo(7.6,2).curveTo(6.3,0.5,5.2,-4).curveTo(8.1,-4.9,13.1,-4.8).curveTo(20.3,-4.8,24.4,-3).curveTo(27.9,-1.4,28.8,1.3).curveTo(29.1,2,25.8,2.5).curveTo(20.4,3.1,10.4,3.9).curveTo(8.4,3,7.6,2).closePath();
	this.shape_4.setTransform(85.5,158.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#381E2D").beginStroke().moveTo(25.4,31.5).curveTo(17.9,30.5,5,29).curveTo(-5.1,27.8,-13.5,26.1).curveTo(-13,25.3,-12.3,23.4).curveTo(-11,19.6,-10.1,13.7).lineTo(-8.7,5.5).curveTo(-8.2,2.9,-8.2,-0.1).lineTo(-8.2,-1.1).lineTo(-8.2,-1.3).lineTo(-8.3,-2.2).lineTo(-8.4,-2.9).curveTo(-7.7,-3.1,-7,-3.2).curveTo(-6.5,-3.3,-5.6,-2).curveTo(-4.9,-1,-4.4,0.2).curveTo(-4.1,0.8,-3.3,5.6).curveTo(-1.9,10.9,2,15.3).curveTo(6,19.7,11.5,21.9).curveTo(14.7,23.2,20.9,24.3).curveTo(26.5,25.3,28.6,26.4).curveTo(31.9,28.2,32.7,32.1).curveTo(28.5,31.9,25.4,31.5).closePath().moveTo(-24.3,-9.5).lineTo(-29.9,-11.5).curveTo(-32.5,-12.5,-32.6,-13.4).curveTo(-33,-16.1,-29.1,-20.5).curveTo(-26.1,-24,-22.5,-26.5).curveTo(-18,-29.6,-10.6,-32.1).curveTo(-12.7,-30.6,-16.1,-25.9).curveTo(-20,-20.7,-22,-16.1).curveTo(-22.7,-14.7,-23.2,-11.5).curveTo(-23.5,-9.5,-24.2,-9.5).lineTo(-24.3,-9.5).closePath();
	this.shape_5.setTransform(47.8,47.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#4D2A3F").beginStroke().moveTo(1.9,39.5).curveTo(1.1,35.6,-2.2,33.8).curveTo(-4.3,32.7,-9.8,31.7).curveTo(-16,30.6,-19.2,29.3).curveTo(-24.8,27.1,-28.7,22.7).curveTo(-32.6,18.3,-34.1,13).curveTo(-34.9,8.2,-35.2,7.6).curveTo(-35.6,6.4,-36.4,5.4).curveTo(-37.3,4.1,-37.8,4.2).curveTo(-38.5,4.3,-39.1,4.5).curveTo(-39.3,3.5,-39.6,2.9).curveTo(-39.8,2.4,-40.1,2.2).curveTo(-41.1,1.5,-42.7,3.6).lineTo(-43.6,5).lineTo(-45.2,5.3).lineTo(-45.3,5).lineTo(-49.4,1).curveTo(-50.5,-0.1,-51.5,-0.7).lineTo(-55,-2.1).curveTo(-54.2,-2,-53.9,-4.1).curveTo(-53.4,-7.3,-52.8,-8.7).curveTo(-50.7,-13.3,-46.9,-18.5).curveTo(-43.4,-23.2,-41.3,-24.7).lineTo(-29,-28.9).curveTo(-23.3,-30.9,-22.5,-31.5).curveTo(-22.2,-31.7,-22.3,-32.2).lineTo(-22.6,-32.8).curveTo(-18.3,-35.2,-10.2,-37.2).curveTo(1,-39.9,12.1,-39.9).curveTo(18.8,-39.9,21.9,-39).curveTo(31.4,-36.2,37.4,-26.7).curveTo(36.2,-26.6,36.2,-25.4).curveTo(36.2,-24.7,41.7,-21.2).curveTo(48.2,-17.1,50.2,-15.1).curveTo(51.5,-13.8,53.7,-7.4).curveTo(54.4,-5.3,55,-4).curveTo(53.9,-3.6,52.6,-2.9).curveTo(50.1,-1.7,48.7,-0.8).curveTo(47.2,-4,44.6,-7).curveTo(41.9,-10.3,40,-10.7).curveTo(37.9,-11.1,37.9,-7.7).curveTo(37.9,-7.2,42,-1.5).lineTo(43.2,0.3).lineTo(44.2,2).lineTo(44.6,2.7).lineTo(44.4,2.8).lineTo(43.1,3.2).curveTo(41.3,3.9,40.9,5).curveTo(40.4,6.4,39.8,8.9).curveTo(39.1,11.9,39,13).lineTo(38.9,16.6).curveTo(38.9,21.6,40.8,26.4).curveTo(41.9,29.5,45.8,36.7).lineTo(46.1,37.3).lineTo(41.6,38.2).curveTo(35.1,39.6,18.7,39.8).lineTo(14.9,39.9).curveTo(7.4,39.9,1.9,39.5).closePath();
	this.shape_6.setTransform(78.6,39.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#000000").beginStroke().moveTo(-25.8,80.7).curveTo(-31.9,79.6,-32.6,78.5).curveTo(-33.4,77.4,-33.4,75.2).curveTo(-33.4,70.1,-28.2,66.5).curveTo(-30.4,63.3,-34.4,53).lineTo(-37.9,43.5).lineTo(-37.9,43).lineTo(-31.9,44.6).curveTo(-30.1,47.5,-27.5,54.8).lineTo(-24,64.2).curveTo(-21.1,63,-17.6,62.4).curveTo(-16,62.1,-14.2,62).curveTo(-10.6,61.7,-6.7,62).curveTo(-2.6,62.4,-0.2,63.2).lineTo(-0.7,58.9).lineTo(-1.4,53.8).curveTo(-1.6,53,-1.6,52.1).lineTo(3,52).curveTo(3.8,53.4,4.2,55.7).curveTo(4.7,58.9,4.3,62.9).curveTo(4.3,65,4.1,65.7).lineTo(4.2,65.7).curveTo(5.5,63.7,9.5,62.4).curveTo(11.1,61.9,12.7,61.6).curveTo(17.2,60.7,22.8,61.2).curveTo(26.7,61.4,30,62.3).curveTo(30.2,60.4,31.6,56.7).curveTo(33.5,51.5,33.7,50.4).curveTo(34,49.2,34.5,48.1).lineTo(39.7,46.8).curveTo(39.6,49.3,37.9,55.2).curveTo(36.2,61.1,34.1,63.8).lineTo(36,64.7).curveTo(42.3,68.3,42,73.6).curveTo(41.9,75.1,41.4,75.6).curveTo(40.9,76.4,39,76.8).curveTo(37.6,77.2,21.6,79.2).curveTo(15,79.9,9.5,79.2).curveTo(7.3,78.7,7.2,78).lineTo(6.9,76.5).lineTo(6.5,77.1).curveTo(4.4,79.5,-4.7,80.7).curveTo(-10.5,81,-15.8,81).curveTo(-21,81,-25.8,80.7).closePath().moveTo(-22.2,68.6).lineTo(-23.8,69).lineTo(-24.4,69.2).lineTo(-25.8,69.8).curveTo(-27.3,70.7,-27.9,72.1).lineTo(-28.7,74.6).curveTo(-28,74.9,-22.7,75.3).curveTo(-17.8,75.8,-16.1,75.7).curveTo(-12.7,75.8,-9.8,75.6).curveTo(-3.5,75.2,-0.1,74.1).curveTo(1.5,73.5,2.3,72.9).lineTo(2.8,72.4).curveTo(2.8,70.7,1.3,69.2).curveTo(-0.3,67.6,-2.3,67.4).lineTo(-4.3,67.4).curveTo(-16.8,67.4,-22.2,68.6).closePath().moveTo(11.8,66.7).curveTo(10.3,67.2,9.3,68).curveTo(8.8,68.3,8,69.3).curveTo(7.5,70.1,6.9,70.1).curveTo(7.3,71.3,7.3,72.7).curveTo(7.3,74.4,7.1,75.4).curveTo(7.8,75.3,9.8,75.2).lineTo(17,74.6).curveTo(27,73.8,32.4,73.2).curveTo(35.7,72.8,35.4,72).curveTo(34.5,69.4,31,67.7).curveTo(26.9,65.9,19.7,65.9).lineTo(19.1,65.9).curveTo(14.5,65.9,11.8,66.7).closePath().moveTo(-57,19.6).curveTo(-59.5,19.5,-61.6,19.2).lineTo(-66.8,18.1).curveTo(-70.7,17,-73.1,15.1).curveTo(-77.4,11.6,-78.5,4).curveTo(-79.3,-0.9,-78.4,-3.7).curveTo(-77.8,-5.5,-75.8,-8.3).curveTo(-70.8,-15.4,-69.3,-26.5).curveTo(-68.6,-32,-67.6,-36.7).lineTo(-65.4,-46.7).lineTo(-65.3,-49.4).curveTo(-66.2,-49.7,-66.7,-50.4).curveTo(-67.2,-51.1,-67.3,-52.4).lineTo(-67.2,-53.8).lineTo(-67.2,-53.9).curveTo(-66.4,-63.1,-52.8,-71).curveTo(-40.1,-78.5,-23.3,-81).curveTo(-23.2,-81,-23,-80.4).lineTo(-22.9,-80.3).curveTo(-24.1,-79.7,-25,-79).curveTo(-24.1,-79.7,-22.9,-80.3).lineTo(-22.6,-79.7).curveTo(-22.5,-79.2,-22.8,-79).curveTo(-23.6,-78.4,-29.3,-76.4).lineTo(-41.6,-72.2).curveTo(-49,-69.7,-53.5,-66.6).curveTo(-57.1,-64.1,-60.2,-60.5).curveTo(-64,-56.1,-63.7,-53.4).curveTo(-63.5,-52.5,-61,-51.6).lineTo(-55.3,-49.6).lineTo(-51.8,-48.1).curveTo(-50.8,-47.6,-49.7,-46.5).lineTo(-45.6,-42.4).lineTo(-45.5,-42.2).lineTo(-43.9,-42.4).lineTo(-43,-43.9).curveTo(-41.4,-46,-40.4,-45.3).curveTo(-40.2,-45.1,-39.9,-44.6).lineTo(-47.2,-22.7).curveTo(-49.6,-17.2,-49.9,-12.8).curveTo(-50.6,-8,-48.7,-5.7).curveTo(-47.8,-3.8,-45.8,-0.1).curveTo(-45,1.4,-43.9,4.7).curveTo(-42.3,9.6,-42.8,11.8).curveTo(-43.3,13.7,-45.7,15.9).curveTo(-47.4,17.3,-48.4,17.8).lineTo(-49.4,18.3).curveTo(-52.4,19.6,-56.1,19.6).lineTo(-57,19.6).closePath().moveTo(-63.6,-37.7).curveTo(-65.5,-29.2,-65.7,-26.7).curveTo(-66.5,-15.5,-71.1,-7.3).curveTo(-72.1,-5.3,-73.8,-2.6).curveTo(-74.9,-0.5,-74.6,0.8).curveTo(-73.1,7.3,-68.9,11.3).curveTo(-66.9,13.3,-64.7,14.3).lineTo(-63.3,14.7).curveTo(-62.3,15.1,-61.2,15.2).lineTo(-59.7,15.3).curveTo(-51.4,15.9,-48.2,12.8).lineTo(-47.5,12.2).curveTo(-46,10.3,-46.5,8.2).lineTo(-46.6,8.1).curveTo(-46.9,7,-50.2,0.8).lineTo(-51.1,-0.9).curveTo(-53.8,-5.9,-54,-6.7).lineTo(-54,-6.9).curveTo(-54.3,-8.9,-51.9,-20).curveTo(-49.6,-31,-47,-37.3).lineTo(-46.2,-39.3).lineTo(-47.4,-40.2).curveTo(-52,-43.7,-54.7,-44.7).lineTo(-56.6,-45.4).curveTo(-58,-46,-59.1,-46.1).curveTo(-59.6,-46.2,-60.4,-46.6).lineTo(-61.6,-47).lineTo(-63.6,-37.7).closePath().moveTo(60.1,12.7).lineTo(60.1,12.7).lineTo(59.8,12.7).curveTo(58,12.7,56.6,11).curveTo(54.6,9.2,53,4.4).curveTo(51.1,-1.2,51.1,-3.7).lineTo(51.2,-5.4).curveTo(51.1,-7.1,49.6,-11).curveTo(47.4,-16.7,47.4,-17).lineTo(47.4,-17.9).lineTo(47.4,-17.9).lineTo(47.4,-18).curveTo(47.4,-19.7,48.2,-19.2).curveTo(48.8,-18.8,49.9,-17.1).lineTo(50.5,-16.1).curveTo(53.5,-10.7,54.5,-4.4).curveTo(54.5,-2.1,54.7,-0.1).curveTo(55.2,2.3,55.7,3.5).curveTo(57.1,7,60.2,9.3).lineTo(63,9.3).lineTo(64,8.9).lineTo(64.1,8.9).curveTo(64.4,8.7,64.9,8.4).lineTo(66.9,7.9).lineTo(68.8,7.3).curveTo(74.4,5.8,74.4,1.4).curveTo(74.4,-5.4,68.7,-10.9).curveTo(65.2,-14.3,64.2,-15.6).curveTo(61.7,-18.7,60.7,-22.5).curveTo(60.5,-23.8,60.1,-25.1).curveTo(59.4,-27.3,57.1,-32.7).curveTo(52.7,-42.9,52,-46.1).lineTo(49.8,-45.3).lineTo(49.4,-45.3).curveTo(47.1,-45.3,45,-44.9).lineTo(44.3,-44.8).lineTo(43.9,-45.4).lineTo(42.9,-47.2).lineTo(41.7,-48.9).curveTo(37.6,-54.7,37.6,-55.2).curveTo(37.6,-58.5,39.7,-58.2).curveTo(41.6,-57.7,44.3,-54.5).curveTo(46.9,-51.4,48.4,-48.3).curveTo(49.8,-49.2,52.3,-50.4).curveTo(53.6,-51.1,54.7,-51.4).curveTo(54.1,-52.8,53.4,-54.9).curveTo(51.2,-61.3,49.9,-62.6).curveTo(47.9,-64.6,41.4,-68.7).curveTo(35.9,-72.2,35.9,-72.9).curveTo(35.9,-74.1,37.1,-74.2).curveTo(37.9,-74.3,39.4,-73.8).curveTo(42.8,-72.8,47,-69.7).curveTo(58,-61.4,58,-51.7).lineTo(58,-51).lineTo(58,-50.5).lineTo(57.9,-49.9).lineTo(57.8,-49.7).lineTo(57.6,-49.5).lineTo(57.5,-49.4).lineTo(56.4,-48.9).curveTo(55.6,-48.7,55.8,-48.1).curveTo(58.2,-41.8,61.1,-35.5).curveTo(63.2,-31,64.7,-26).curveTo(66.6,-19.4,69.3,-15.9).curveTo(70.7,-14,74,-11.6).curveTo(76.5,-9.6,77.5,-7.5).curveTo(78.9,-4.3,78.9,2).curveTo(78.9,5.3,75.9,8.5).curveTo(72.2,12.4,66.2,12.8).lineTo(65.6,12.9).lineTo(60.1,12.7).closePath().moveTo(37.3,-73.8).lineTo(37.1,-74.2).lineTo(37.3,-73.8).closePath();
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,173.4);


(lib.PeppaOptions = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#491616").beginStroke().moveTo(-7.1,46.5).curveTo(-18.5,39,-21.3,37.6).lineTo(-23.2,24).curveTo(-24.4,15.9,-24.4,12.6).curveTo(-24.3,4,-23.6,-0.2).curveTo(-23,-4.3,-20.8,-11).curveTo(-11.5,-38.4,-0,-51.3).curveTo(2.8,-54.5,6,-57).curveTo(9.5,-43.3,15.7,-25.2).curveTo(20.6,-10.2,24.4,-0.3).lineTo(20.2,-0.3).curveTo(18.9,0.5,17.8,2.1).curveTo(10.8,4.5,9.7,19.9).curveTo(9.2,25.7,9.6,33.3).lineTo(10.2,41.5).lineTo(10.5,49.3).curveTo(10.7,55.1,11.3,57).curveTo(3.9,53.6,-7.1,46.5).closePath();
	this.shape.setTransform(41.4,113.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#330F0F").beginStroke().moveTo(7.7,18.3).lineTo(7.5,18.3).curveTo(5.1,18.3,5.3,11.7).curveTo(5.3,10.7,5.4,9.9).curveTo(4.6,4.3,3.7,-5).lineTo(2.6,-8.9).lineTo(2.5,-9.3).curveTo(-0.7,-1.7,-1.5,1).lineTo(-4.2,11.3).curveTo(-6.3,17.8,-8.5,18).curveTo(-9.1,18.3,-9.7,18.3).lineTo(-9.7,18.3).curveTo(-9.8,18.3,-9.8,18.2).curveTo(-9.8,18.1,-9.8,18.1).curveTo(-9.7,18,-9.7,18).curveTo(-9.7,18,-9.7,17.9).curveTo(-9.6,16.4,-9.1,14.4).lineTo(-9,13.5).lineTo(-8.8,12.8).lineTo(-7.8,8.8).curveTo(-7.4,6.7,-6.7,4.5).curveTo(-5.6,0.5,-4.3,-3).curveTo(-3.2,-6,-2,-8.6).lineTo(-1.9,-8.9).curveTo(-0.5,-12,1.1,-14.5).curveTo(1.5,-15.5,2.1,-16.2).lineTo(3.2,-17.7).lineTo(3.6,-18.3).lineTo(3.8,-18.6).lineTo(3.9,-18.6).lineTo(4.4,-17.5).lineTo(5.2,-15.7).lineTo(5.5,-14.7).curveTo(6.1,-12.9,6.5,-10.9).curveTo(6.9,-8.9,7.2,-6.6).lineTo(7.5,-3.3).lineTo(7.5,-2.7).curveTo(7.9,4.7,8.5,9.5).curveTo(8.7,11.3,9,12.8).curveTo(9.2,14.2,9.5,15.3).lineTo(9.5,18.5).lineTo(9.8,18.5).lineTo(8.7,18.6).curveTo(8.1,18.6,7.7,18.3).closePath();
	this.shape_1.setTransform(83,100.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#6B2121").beginStroke().moveTo(24.7,34.9).curveTo(13.7,32.9,9.8,32.7).lineTo(-13.2,32.7).curveTo(-13.5,31.2,-13.7,29.4).curveTo(-14.3,24.6,-14.7,17.2).lineTo(-14.7,16.6).lineTo(-15,13.2).curveTo(-15.3,11,-15.7,9).curveTo(-16.1,7,-16.7,5.2).lineTo(-17,4.2).lineTo(-17.8,2.4).lineTo(-18.3,1.3).lineTo(-18.4,1.3).lineTo(-18.6,1.5).lineTo(-19,2.2).lineTo(-20.1,3.7).curveTo(-20.7,4.4,-21.1,5.4).curveTo(-22.7,7.9,-24.1,11).lineTo(-24.2,11.3).curveTo(-25.4,13.9,-26.5,16.8).curveTo(-27.8,20.4,-28.9,24.4).curveTo(-29.6,26.6,-30,28.7).lineTo(-31,32.7).lineTo(-39.4,32.7).curveTo(-43.2,22.8,-48.1,7.8).curveTo(-54.3,-10.2,-57.8,-24).curveTo(-41.5,-37.8,-16.7,-37.7).curveTo(-0.1,-37.7,19.4,-28.8).curveTo(40,-19.3,48.9,-5.9).curveTo(51.9,-1.4,54.2,7.3).curveTo(56.5,16.1,57.4,26.3).curveTo(57.4,27.4,57.2,32.1).curveTo(57,35.4,57.8,37.7).curveTo(31.1,36.1,24.7,34.9).closePath();
	this.shape_2.setTransform(105.2,80.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#000000").beginStroke().moveTo(-35.7,68.6).curveTo(-42.4,65.7,-49.7,61).curveTo(-58,55.1,-62.8,52).curveTo(-64,51.2,-67.2,50.1).curveTo(-69.6,49.2,-70.9,47.7).lineTo(-72.2,46.2).lineTo(-73.2,46.2).lineTo(-73.6,45.8).curveTo(-74,45.4,-74.4,43.5).curveTo(-76.6,33.1,-77.2,23.3).curveTo(-78.3,5.4,-73.9,-10.8).curveTo(-66.3,-39.6,-43.2,-56.3).curveTo(-29.4,-66.1,-10.9,-68.3).curveTo(-5.7,-68.9,-0.4,-68.9).curveTo(11.6,-68.9,23.3,-65.7).curveTo(39.5,-61.3,52.1,-51.5).curveTo(64.9,-41.7,71.4,-28.1).curveTo(78.1,-14.2,77.3,1.8).lineTo(76.8,10.4).lineTo(76.5,10.2).lineTo(76.4,11.1).lineTo(76.2,12.5).curveTo(76.6,13,76.9,13.8).lineTo(75.8,16.4).lineTo(61.9,16.4).lineTo(43.8,14.1).curveTo(28.8,12.2,24.2,11.9).lineTo(3.7,11.9).lineTo(3.4,11.9).lineTo(3.4,8.7).curveTo(3.1,7.7,2.9,6.3).lineTo(25.9,6.3).curveTo(29.8,6.5,40.8,8.5).curveTo(47.2,9.6,73.9,11.3).curveTo(73.1,9,73.3,5.7).curveTo(73.5,1,73.5,-0.1).curveTo(72.6,-10.4,70.3,-19.2).curveTo(68,-27.8,65,-32.4).curveTo(56.1,-45.7,35.5,-55.2).curveTo(16,-64.1,-0.6,-64.1).curveTo(-25.4,-64.2,-41.7,-50.4).curveTo(-44.9,-47.9,-47.7,-44.6).curveTo(-59.2,-31.8,-68.5,-4.4).curveTo(-70.7,2.3,-71.3,6.4).curveTo(-72,10.6,-72.1,19.2).curveTo(-72.1,22.5,-70.9,30.6).lineTo(-69,44.2).curveTo(-66.2,45.6,-54.8,53.1).curveTo(-43.8,60.3,-36.4,63.7).curveTo(-37,61.7,-37.2,55.9).lineTo(-37.5,48.1).lineTo(-38.1,39.9).curveTo(-38.5,32.4,-38,26.6).curveTo(-36.9,11.1,-29.9,8.7).curveTo(-28.8,7.1,-27.5,6.3).lineTo(-23.3,6.3).lineTo(-14.9,6.3).lineTo(-15.1,6.9).lineTo(-15.2,7.8).curveTo(-15.7,9.9,-15.8,11.4).curveTo(-15.8,11.4,-15.8,11.5).curveTo(-15.8,11.5,-15.8,11.6).curveTo(-15.8,11.6,-15.8,11.7).curveTo(-15.8,11.7,-15.8,11.8).lineTo(-15.8,11.9).lineTo(-27,11.9).curveTo(-31.5,21.3,-32.5,34.9).curveTo(-32.7,38.4,-32.4,65.5).lineTo(-32.2,66.4).curveTo(-32.7,67.8,-33,68.3).curveTo(-33.6,68.9,-34.4,68.9).curveTo(-35,68.9,-35.7,68.6).closePath();
	this.shape_3.setTransform(89.1,107.2);

	this.instance = new lib.PeppaHead();
	this.instance.setTransform(91.2,146.6,1,1,0,0,0,69.8,62);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#6B2121").beginStroke().moveTo(-1.3,29.8).curveTo(-4.1,29.9,-6.1,29.7).curveTo(-9.3,17.2,-10,13.8).curveTo(-10.7,9.5,-9.8,1.3).curveTo(-9.4,-4,-9,-7.2).curveTo(-8.2,-13,-6.7,-17.1).curveTo(-2.3,-28.7,9.4,-29.9).lineTo(10.3,-8.1).curveTo(10.4,-0.4,8.9,12.4).curveTo(8.1,19.6,7.8,21.6).curveTo(6.7,27.3,5,29.9).lineTo(-1.3,29.8).closePath();
	this.shape_4.setTransform(151.2,142.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#000000").beginStroke().moveTo(-0.9,34.3).lineTo(-6.8,34.1).curveTo(-13.2,20.7,-13.8,16.6).curveTo(-14.2,14.1,-12.8,1.8).lineTo(-11.8,-8).curveTo(-11.1,-13.4,-9.8,-17.2).curveTo(-6.4,-27.1,4.4,-34.3).lineTo(11.3,-33.6).curveTo(13.3,-28,13.7,-17.6).curveTo(14.3,-5.7,12.9,13.8).curveTo(12.7,15.9,11.4,26.3).curveTo(10.9,31.1,10,32.1).curveTo(7.9,34.3,0.9,34.3).lineTo(-0.9,34.3).closePath().moveTo(5.6,30.6).curveTo(7.3,28.1,8.3,22.4).curveTo(8.7,20.4,9.5,13.2).curveTo(10.9,0.4,10.8,-7.3).lineTo(9.9,-29.1).curveTo(-1.8,-28,-6.2,-16.3).curveTo(-7.6,-12.2,-8.4,-6.4).curveTo(-8.8,-3.2,-9.3,2).curveTo(-10.2,10.2,-9.4,14.5).curveTo(-8.7,17.9,-5.5,30.5).curveTo(-3.5,30.6,-0.7,30.6).lineTo(5.6,30.6).closePath();
	this.shape_5.setTransform(150.7,141.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(5));

	// Layer 2
	this.instance_1 = new lib.PeppaClothes();
	this.instance_1.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_2 = new lib.FemaleTrousersPeppa();
	this.instance_2.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_3 = new lib.FemaleHoodiePeppa();
	this.instance_3.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_4 = new lib.FemaleMotifPeppa();
	this.instance_4.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_5 = new lib.PeppaSkirt();
	this.instance_5.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(11.7,38.3,159.9,318.6);


(lib.OptionsMenu = function() {
	this.initialize();

	// Layer 2
	this.btn_Leaderboard = new lib.Symbol1();
	this.btn_Leaderboard.setTransform(36.1,100.1,1.26,1.26,0,0,0,24.1,24.1);

	this.btn_Twitter = new lib.ShareTwitter();
	this.btn_Twitter.setTransform(163,343.4,1.26,1.26,0,0,0,167.8,31.7);
	new cjs.ButtonHelper(this.btn_Twitter, 0, 1, 2, false, new lib.ShareTwitter(), 3);

	this.btn_Facebook = new lib.ShareFB();
	this.btn_Facebook.setTransform(163,283.3,1.26,1.26,0,0,0,167.8,31.7);
	new cjs.ButtonHelper(this.btn_Facebook, 0, 1, 2, false, new lib.ShareFB(), 3);

	this.btn_Sound = new lib.SoundIconRoundBtncopy();
	this.btn_Sound.setTransform(49.1,232,1.26,1.26,0,0,0,39.9,35.3);

	this.btn_Logout = new lib.LogOutRoundBtncopy();
	this.btn_Logout.setTransform(36.8,171.3,1.26,1.26,0,0,0,13.7,12.8);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-139.6,-189.5).lineTo(139.6,-189.5).curveTo(149.6,-189.5,149.6,-179.5).lineTo(149.6,179.5).curveTo(149.6,189.5,139.6,189.5).lineTo(-139.6,189.5).curveTo(-149.6,189.5,-149.6,179.5).lineTo(-149.6,-179.5).curveTo(-149.6,-189.5,-139.6,-189.5).closePath();
	this.shape.setTransform(149.6,189.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#F4D96F").beginStroke().moveTo(-139.6,189.5).curveTo(-149.6,189.5,-149.6,179.5).lineTo(-149.6,-179.5).curveTo(-149.6,-189.5,-139.6,-189.5).lineTo(139.6,-189.5).curveTo(149.6,-189.5,149.6,-179.5).lineTo(149.6,179.5).curveTo(149.6,189.5,139.6,189.5).closePath();
	this.shape_1.setTransform(149.6,189.5);

	// Layer 3
	this.mc_bg = new lib._50Percent_white();
	this.mc_bg.setTransform(480,270.4,1,1,0,0,0,500,285.9);

	this.addChild(this.mc_bg,this.shape_1,this.shape,this.btn_Logout,this.btn_Sound,this.btn_Facebook,this.btn_Twitter,this.btn_Leaderboard);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-20,-15.5,1000,571.9);


(lib.LadyBossMC2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"Default":0,"StartTalking":8,"TalkLoop":13,"StopTalking":30,PressGotIt:52});

	// timeline functions:
	this.frame_51 = function() {
		this.stop();
		this.stop();
		this.stop();
	}
	this.frame_63 = function() {
		this.gotoAndStop(0);
		gamePost.doGotItWrongWearPressedDone();
		//this.gotoAndStop("ChooseSteveWorkwear");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(51).call(this.frame_51).wait(12).call(this.frame_63).wait(2));

	// Layer 3
	this.instance = new lib.LadyBoss("single",0);
	this.instance.setTransform(743.4,131.3,0.94,0.94,0,0,0,216.6,210.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({mode:"synched",loop:false},0).wait(22).to({startPosition:21},0).wait(21).to({startPosition:26},0).wait(4).to({startPosition:26},0).to({y:551.3},8).to({_off:true},1).wait(1));

	// Layer 1
	this.instance_1 = new lib.GotItButton();
	this.instance_1.setTransform(631.1,123.4,0.142,0.142,0,0,0,99,31.7);
	this.instance_1._off = true;
	new cjs.ButtonHelper(this.instance_1, 0, 1, 2, false, new lib.GotItButton(), 3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,scaleY:1,x:414.3,y:253.6},4,cjs.Ease.get(1)).wait(44).to({scaleX:0.14,scaleY:0.14,x:631.1,y:123.4},4).to({_off:true},1).wait(8));

	// Layer 2
	this.instance_2 = new lib.LadyBoss_Bubble2();
	this.instance_2.setTransform(667.1,101.9,0.142,0.142,0,0,0,394.4,13.4);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(4).to({_off:false},0).to({regY:13.3,scaleX:1,scaleY:1,x:668.2,y:101.7},4,cjs.Ease.get(1)).wait(44).to({regY:13.4,scaleX:0.14,scaleY:0.14,x:667.1,y:101.9},4).to({_off:true},1).wait(8));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(604.2,-66.8,244.4,346.9);


(lib.KateSkirtJane = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.GirlOutfit04();
	this.instance.setTransform(82.1,89.6,1.12,1.12,0,0,0,62.6,83.9);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#4E2A3F").setStrokeStyle(1,1,1).moveTo(31.2,6.8).curveTo(31,6.6,30.9,6.4).curveTo(25,-3.1,15.4,-5.9).curveTo(12.3,-6.8,5.6,-6.8).curveTo(-5.5,-6.8,-16.6,-4.1).curveTo(-24.8,-2.1,-29.1,0.2).curveTo(-30.3,0.9,-31.2,1.5);
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#7A4D30").beginStroke().moveTo(8,56.3).curveTo(8.2,55.7,8.2,53.5).curveTo(8.6,49.5,8.1,46.3).curveTo(7.7,44.1,6.9,42.6).curveTo(11.1,42.5,15.3,42.2).lineTo(16.6,52.2).curveTo(15,52.5,13.4,53).curveTo(9.4,54.3,8.1,56.4).lineTo(8,56.3).closePath().moveTo(-23.6,45.5).curveTo(-26.2,38.1,-28,35.2).curveTo(-26.3,35.6,-22.7,38.2).curveTo(-19.3,40.8,-17.4,41.1).lineTo(-13.7,53).curveTo(-17.2,53.6,-20.1,54.9).lineTo(-23.6,45.5).closePath().moveTo(-65,1.9).curveTo(-69.2,-2.1,-70.7,-8.6).curveTo(-71,-9.9,-69.9,-11.9).curveTo(-68.2,-14.7,-67.2,-16.7).curveTo(-62.6,-24.9,-61.8,-36).curveTo(-61.6,-38.6,-59.7,-47.1).lineTo(-57.7,-56.4).lineTo(-56.5,-55.9).curveTo(-55.7,-55.6,-55.2,-55.5).curveTo(-55.9,-51.3,-57.1,-37.5).curveTo(-58.4,-21.8,-58.5,-21.6).curveTo(-59.9,-17.1,-62.8,-12.7).curveTo(-64.5,-9,-63,-2.8).lineTo(-61.9,1.1).lineTo(-60.8,4.9).curveTo(-63,3.9,-65,1.9).closePath().moveTo(64.1,-0.1).curveTo(61.1,-2.4,59.6,-5.9).curveTo(59.1,-7.1,58.6,-9.5).curveTo(58.4,-11.4,58.4,-13.8).curveTo(57.4,-20.1,54.4,-25.5).lineTo(53.8,-26.5).curveTo(52.7,-28.2,52.1,-28.6).lineTo(51.3,-31.7).curveTo(49.9,-37.2,49.4,-40.3).lineTo(47.9,-47.4).curveTo(53.7,-36.7,57.5,-28).curveTo(60.9,-19.7,61.2,-16.6).curveTo(61.7,-11.5,63.8,-7.4).curveTo(66.5,-2.5,70.8,-1.5).lineTo(68.8,-0.9).curveTo(68.3,-0.7,68,-0.4).lineTo(68,-0.5).lineTo(66.9,-0.1).closePath();
	this.shape_1.setTransform(75,96.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#975F3C").beginStroke().moveTo(-10.1,74).curveTo(-14,73.7,-17.6,74).curveTo(-19.4,74.2,-21,74.5).lineTo(-24.7,62.6).curveTo(-15,64.1,-5,64.1).curveTo(-5,65,-4.8,65.8).lineTo(-4.1,71).lineTo(-3.6,75.3).curveTo(-6,74.4,-10.1,74).closePath().moveTo(19.4,73.2).curveTo(13.8,72.8,9.3,73.6).lineTo(8,63.7).curveTo(19.6,62.7,31.1,60.1).curveTo(30.6,61.3,30.3,62.5).curveTo(30.1,63.6,28.2,68.8).curveTo(26.8,72.4,26.6,74.4).curveTo(23.3,73.4,19.4,73.2).closePath().moveTo(-63.1,27.4).lineTo(-64.6,27.2).curveTo(-65.7,27.1,-66.7,26.8).lineTo(-68.1,26.3).lineTo(-69.2,22.5).lineTo(-70.3,18.7).curveTo(-71.8,12.5,-70.1,8.8).curveTo(-67.2,4.3,-65.8,-0.1).curveTo(-65.7,-0.4,-64.4,-16.1).curveTo(-63.2,-29.9,-62.5,-34.1).curveTo(-61.4,-34,-60,-33.4).lineTo(-58.1,-32.6).curveTo(-55.4,-31.6,-50.8,-28.1).lineTo(-49.6,-27.2).lineTo(-50.4,-25.3).curveTo(-53,-18.9,-55.3,-8).curveTo(-57.7,3.2,-57.4,5.2).lineTo(-57.4,5.3).curveTo(-57.2,6.2,-54.5,11.1).lineTo(-53.6,12.8).curveTo(-50.3,19.1,-50,20.2).lineTo(-49.9,20.3).curveTo(-49.4,22.4,-50.9,24.2).lineTo(-51.6,24.9).curveTo(-54.3,27.4,-60.6,27.4).lineTo(-63.1,27.4).closePath().moveTo(56.5,14.1).curveTo(54.4,9.9,53.9,4.9).curveTo(53.6,1.7,50.2,-6.5).curveTo(46.4,-15.3,40.5,-26).curveTo(39.6,-30.5,39.4,-32.3).curveTo(37.6,-31.6,37.2,-30.5).curveTo(36.7,-29.1,36.1,-26.5).curveTo(35.4,-23.6,35.3,-22.5).lineTo(35.2,-18.9).curveTo(35.2,-13.9,37.1,-9).curveTo(38.2,-5.9,42.1,1.3).lineTo(42.4,1.8).lineTo(37.9,2.8).curveTo(31.4,4.2,15,4.4).curveTo(5.1,4.5,-1.8,4.1).curveTo(-2.6,0.2,-5.9,-1.7).curveTo(-8,-2.7,-13.5,-3.7).curveTo(-19.7,-4.8,-22.9,-6.1).curveTo(-28.5,-8.3,-32.4,-12.7).curveTo(-36.3,-17.1,-37.8,-22.5).curveTo(-38.6,-27.2,-38.9,-27.9).curveTo(-39.3,-29,-40.1,-30).curveTo(-41,-31.3,-41.5,-31.3).curveTo(-42.2,-31.1,-42.8,-30.9).curveTo(-43,-32,-43.3,-32.6).curveTo(-43.5,-33,-43.8,-33.3).curveTo(-44.8,-33.9,-46.4,-31.8).lineTo(-47.3,-30.4).lineTo(-48.9,-30.1).lineTo(-49,-30.4).lineTo(-53.1,-34.4).curveTo(-54.2,-35.6,-55.2,-36.1).lineTo(-58.7,-37.6).curveTo(-57.9,-37.4,-57.6,-39.5).curveTo(-57.1,-42.7,-56.5,-44.2).curveTo(-54.4,-48.7,-50.6,-53.9).curveTo(-47.1,-58.6,-45,-60.2).lineTo(-32.7,-64.3).curveTo(-27,-66.4,-26.2,-66.9).curveTo(-25.9,-67.2,-26,-67.6).lineTo(-26.3,-68.3).curveTo(-22,-70.6,-13.9,-72.6).curveTo(-2.7,-75.3,8.4,-75.3).curveTo(15.1,-75.3,18.2,-74.4).curveTo(27.7,-71.7,33.7,-62.2).curveTo(32.5,-62,32.5,-60.8).curveTo(32.5,-60.1,38,-56.7).curveTo(44.5,-52.5,46.5,-50.5).curveTo(47.8,-49.2,50,-42.8).curveTo(50.7,-40.8,51.3,-39.4).curveTo(50.2,-39.1,48.9,-38.3).curveTo(46.4,-37.2,45,-36.2).curveTo(43.5,-39.4,40.9,-42.5).curveTo(38.2,-45.7,36.3,-46.1).curveTo(34.2,-46.5,34.2,-43.2).curveTo(34.2,-42.7,38.3,-36.9).lineTo(39.5,-35.2).lineTo(40.5,-33.4).lineTo(40.9,-32.7).lineTo(41.6,-32.8).curveTo(43.7,-33.3,46,-33.3).lineTo(46.4,-33.2).lineTo(48.6,-34.1).curveTo(49.3,-30.9,53.7,-20.7).curveTo(56,-15.2,56.7,-13).curveTo(57.1,-11.8,57.3,-10.4).curveTo(58.3,-6.7,60.8,-3.6).curveTo(61.8,-2.3,65.3,1.1).curveTo(71,6.7,71,13.4).curveTo(71,17.8,65.4,19.4).lineTo(63.5,19.9).curveTo(59.1,18.9,56.5,14.1).closePath();
	this.shape_2.setTransform(82.3,75.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#371E2D").beginStroke().moveTo(-16.8,4).curveTo(-22.1,3.6,-22.9,3.3).lineTo(-22,0.9).curveTo(-21.5,-0.6,-19.9,-1.5).lineTo(-18.5,-2.1).lineTo(-17.9,-2.3).lineTo(-16.3,-2.6).lineTo(-16.4,-1.9).curveTo(-16.3,1.3,-6.5,3.8).lineTo(-3.9,4.4).curveTo(-6.8,4.5,-10.3,4.5).curveTo(-11.9,4.5,-16.8,4).closePath().moveTo(13.1,1.4).curveTo(13.2,0.1,12.8,-1.2).curveTo(13.4,-1.2,13.9,-1.9).curveTo(14.7,-2.9,15.2,-3.3).curveTo(16.2,-4,17.7,-4.5).curveTo(18.8,0,20.1,1.5).curveTo(20.9,2.5,22.8,3.3).lineTo(15.7,3.9).curveTo(13.7,4,13,4.1).curveTo(13.2,3.1,13.1,1.4).closePath();
	this.shape_3.setTransform(73,158.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#4D2A3E").beginStroke().moveTo(-19,4.3).curveTo(-28.8,1.8,-28.8,-1.4).lineTo(-28.8,-2.1).curveTo(-22.9,-3.4,-8.9,-3.3).curveTo(-6.9,-3.1,-5.3,-1.6).curveTo(-3.8,-0.1,-3.8,1.7).lineTo(-4.3,2.2).curveTo(-5.1,2.8,-6.7,3.4).curveTo(-10.1,4.5,-16.4,4.9).lineTo(-19,4.3).closePath().moveTo(7.6,2).curveTo(6.3,0.5,5.2,-4).curveTo(8.1,-4.9,13.1,-4.8).curveTo(20.3,-4.8,24.4,-3).curveTo(27.9,-1.4,28.8,1.3).curveTo(29.1,2,25.8,2.5).curveTo(20.4,3.1,10.4,3.9).curveTo(8.4,3,7.6,2).closePath();
	this.shape_4.setTransform(85.5,158.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#381E2D").beginStroke().moveTo(25.4,31.5).curveTo(17.9,30.5,5,29).curveTo(-5.1,27.8,-13.5,26.1).curveTo(-13,25.3,-12.3,23.4).curveTo(-11,19.6,-10.1,13.7).lineTo(-8.7,5.5).curveTo(-8.2,2.9,-8.2,-0.1).lineTo(-8.2,-1.1).lineTo(-8.2,-1.3).lineTo(-8.3,-2.2).lineTo(-8.4,-2.9).curveTo(-7.7,-3.1,-7,-3.2).curveTo(-6.5,-3.3,-5.6,-2).curveTo(-4.9,-1,-4.4,0.2).curveTo(-4.1,0.8,-3.3,5.6).curveTo(-1.9,10.9,2,15.3).curveTo(6,19.7,11.5,21.9).curveTo(14.7,23.2,20.9,24.3).curveTo(26.5,25.3,28.6,26.4).curveTo(31.9,28.2,32.7,32.1).curveTo(28.5,31.9,25.4,31.5).closePath().moveTo(-24.3,-9.5).lineTo(-29.9,-11.5).curveTo(-32.5,-12.5,-32.6,-13.4).curveTo(-33,-16.1,-29.1,-20.5).curveTo(-26.1,-24,-22.5,-26.5).curveTo(-18,-29.6,-10.6,-32.1).curveTo(-12.7,-30.6,-16.1,-25.9).curveTo(-20,-20.7,-22,-16.1).curveTo(-22.7,-14.7,-23.2,-11.5).curveTo(-23.5,-9.5,-24.2,-9.5).lineTo(-24.3,-9.5).closePath();
	this.shape_5.setTransform(47.8,47.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#000000").beginStroke().moveTo(-25.8,80.7).curveTo(-31.9,79.6,-32.6,78.5).curveTo(-33.4,77.4,-33.4,75.2).curveTo(-33.4,70.1,-28.2,66.5).curveTo(-30.4,63.3,-34.4,53).lineTo(-37.9,43.5).lineTo(-37.9,43).lineTo(-31.9,44.6).curveTo(-30.1,47.5,-27.5,54.8).lineTo(-24,64.2).curveTo(-21.1,63,-17.6,62.4).curveTo(-16,62.1,-14.2,62).curveTo(-10.6,61.7,-6.7,62).curveTo(-2.6,62.4,-0.2,63.2).lineTo(-0.7,58.9).lineTo(-1.4,53.8).curveTo(-1.6,53,-1.6,52.1).lineTo(3,52).curveTo(3.8,53.4,4.2,55.7).curveTo(4.7,58.9,4.3,62.9).curveTo(4.3,65,4.1,65.7).lineTo(4.2,65.7).curveTo(5.5,63.7,9.5,62.4).curveTo(11.1,61.9,12.7,61.6).curveTo(17.2,60.7,22.8,61.2).curveTo(26.7,61.4,30,62.3).curveTo(30.2,60.4,31.6,56.7).curveTo(33.5,51.5,33.7,50.4).curveTo(34,49.2,34.5,48.1).lineTo(39.7,46.8).curveTo(39.6,49.3,37.9,55.2).curveTo(36.2,61.1,34.1,63.8).lineTo(36,64.7).curveTo(42.3,68.3,42,73.6).curveTo(41.9,75.1,41.4,75.6).curveTo(40.9,76.4,39,76.8).curveTo(37.6,77.2,21.6,79.2).curveTo(15,79.9,9.5,79.2).curveTo(7.3,78.7,7.2,78).lineTo(6.9,76.5).lineTo(6.5,77.1).curveTo(4.4,79.5,-4.7,80.7).curveTo(-10.5,81,-15.8,81).curveTo(-21,81,-25.8,80.7).closePath().moveTo(-22.2,68.6).lineTo(-23.8,69).lineTo(-24.4,69.2).lineTo(-25.8,69.8).curveTo(-27.3,70.7,-27.9,72.1).lineTo(-28.7,74.6).curveTo(-28,74.9,-22.7,75.3).curveTo(-17.8,75.8,-16.1,75.7).curveTo(-12.7,75.8,-9.8,75.6).curveTo(-3.5,75.2,-0.1,74.1).curveTo(1.5,73.5,2.3,72.9).lineTo(2.8,72.4).curveTo(2.8,70.7,1.3,69.2).curveTo(-0.3,67.6,-2.3,67.4).lineTo(-4.3,67.4).curveTo(-16.8,67.4,-22.2,68.6).closePath().moveTo(11.8,66.7).curveTo(10.3,67.2,9.3,68).curveTo(8.8,68.3,8,69.3).curveTo(7.5,70.1,6.9,70.1).curveTo(7.3,71.3,7.3,72.7).curveTo(7.3,74.4,7.1,75.4).curveTo(7.8,75.3,9.8,75.2).lineTo(17,74.6).curveTo(27,73.8,32.4,73.2).curveTo(35.7,72.8,35.4,72).curveTo(34.5,69.4,31,67.7).curveTo(26.9,65.9,19.7,65.9).lineTo(19.1,65.9).curveTo(14.5,65.9,11.8,66.7).closePath().moveTo(-57,19.6).curveTo(-59.5,19.5,-61.6,19.2).lineTo(-66.8,18.1).curveTo(-70.7,17,-73.1,15.1).curveTo(-77.4,11.6,-78.5,4).curveTo(-79.3,-0.9,-78.4,-3.7).curveTo(-77.8,-5.5,-75.8,-8.3).curveTo(-70.8,-15.4,-69.3,-26.5).curveTo(-68.6,-32,-67.6,-36.7).lineTo(-65.4,-46.7).lineTo(-65.3,-49.4).curveTo(-66.2,-49.7,-66.7,-50.4).curveTo(-67.2,-51.1,-67.3,-52.4).lineTo(-67.2,-53.8).lineTo(-67.2,-53.9).curveTo(-66.4,-63.1,-52.8,-71).curveTo(-40.1,-78.5,-23.3,-81).curveTo(-23.2,-81,-23,-80.4).lineTo(-22.9,-80.3).curveTo(-24.1,-79.7,-25,-79).curveTo(-24.1,-79.7,-22.9,-80.3).lineTo(-22.6,-79.7).curveTo(-22.5,-79.2,-22.8,-79).curveTo(-23.6,-78.4,-29.3,-76.4).lineTo(-41.6,-72.2).curveTo(-49,-69.7,-53.5,-66.6).curveTo(-57.1,-64.1,-60.2,-60.5).curveTo(-64,-56.1,-63.7,-53.4).curveTo(-63.5,-52.5,-61,-51.6).lineTo(-55.3,-49.6).lineTo(-51.8,-48.1).curveTo(-50.8,-47.6,-49.7,-46.5).lineTo(-45.6,-42.4).lineTo(-45.5,-42.2).lineTo(-43.9,-42.4).lineTo(-43,-43.9).curveTo(-41.4,-46,-40.4,-45.3).curveTo(-40.2,-45.1,-39.9,-44.6).lineTo(-47.2,-22.7).curveTo(-49.6,-17.2,-49.9,-12.8).curveTo(-50.6,-8,-48.7,-5.7).curveTo(-47.8,-3.8,-45.8,-0.1).curveTo(-45,1.4,-43.9,4.7).curveTo(-42.3,9.6,-42.8,11.8).curveTo(-43.3,13.7,-45.7,15.9).curveTo(-47.4,17.3,-48.4,17.8).lineTo(-49.4,18.3).curveTo(-52.4,19.6,-56.1,19.6).lineTo(-57,19.6).closePath().moveTo(-63.6,-37.7).curveTo(-65.5,-29.2,-65.7,-26.7).curveTo(-66.5,-15.5,-71.1,-7.3).curveTo(-72.1,-5.3,-73.8,-2.6).curveTo(-74.9,-0.5,-74.6,0.8).curveTo(-73.1,7.3,-68.9,11.3).curveTo(-66.9,13.3,-64.7,14.3).lineTo(-63.3,14.7).curveTo(-62.3,15.1,-61.2,15.2).lineTo(-59.7,15.3).curveTo(-51.4,15.9,-48.2,12.8).lineTo(-47.5,12.2).curveTo(-46,10.3,-46.5,8.2).lineTo(-46.6,8.1).curveTo(-46.9,7,-50.2,0.8).lineTo(-51.1,-0.9).curveTo(-53.8,-5.9,-54,-6.7).lineTo(-54,-6.9).curveTo(-54.3,-8.9,-51.9,-20).curveTo(-49.6,-31,-47,-37.3).lineTo(-46.2,-39.3).lineTo(-47.4,-40.2).curveTo(-52,-43.7,-54.7,-44.7).lineTo(-56.6,-45.4).curveTo(-58,-46,-59.1,-46.1).curveTo(-59.6,-46.2,-60.4,-46.6).lineTo(-61.6,-47).lineTo(-63.6,-37.7).closePath().moveTo(60.1,12.7).lineTo(60.1,12.7).lineTo(59.8,12.7).curveTo(58,12.7,56.6,11).curveTo(54.6,9.2,53,4.4).curveTo(51.1,-1.2,51.1,-3.7).lineTo(51.2,-5.4).curveTo(51.1,-7.1,49.6,-11).curveTo(47.4,-16.7,47.4,-17).lineTo(47.4,-17.9).lineTo(47.4,-17.9).lineTo(47.4,-18).curveTo(47.4,-19.7,48.2,-19.2).curveTo(48.8,-18.8,49.9,-17.1).lineTo(50.5,-16.1).curveTo(53.5,-10.7,54.5,-4.4).curveTo(54.5,-2.1,54.7,-0.1).curveTo(55.2,2.3,55.7,3.5).curveTo(57.1,7,60.2,9.3).lineTo(63,9.3).lineTo(64,8.9).lineTo(64.1,8.9).curveTo(64.4,8.7,64.9,8.4).lineTo(66.9,7.9).lineTo(68.8,7.3).curveTo(74.4,5.8,74.4,1.4).curveTo(74.4,-5.4,68.7,-10.9).curveTo(65.2,-14.3,64.2,-15.6).curveTo(61.7,-18.7,60.7,-22.5).curveTo(60.5,-23.8,60.1,-25.1).curveTo(59.4,-27.3,57.1,-32.7).curveTo(52.7,-42.9,52,-46.1).lineTo(49.8,-45.3).lineTo(49.4,-45.3).curveTo(47.1,-45.3,45,-44.9).lineTo(44.3,-44.8).lineTo(43.9,-45.4).lineTo(42.9,-47.2).lineTo(41.7,-48.9).curveTo(37.6,-54.7,37.6,-55.2).curveTo(37.6,-58.5,39.7,-58.2).curveTo(41.6,-57.7,44.3,-54.5).curveTo(46.9,-51.4,48.4,-48.3).curveTo(49.8,-49.2,52.3,-50.4).curveTo(53.6,-51.1,54.7,-51.4).curveTo(54.1,-52.8,53.4,-54.9).curveTo(51.2,-61.3,49.9,-62.6).curveTo(47.9,-64.6,41.4,-68.7).curveTo(35.9,-72.2,35.9,-72.9).curveTo(35.9,-74.1,37.1,-74.2).curveTo(37.9,-74.3,39.4,-73.8).curveTo(42.8,-72.8,47,-69.7).curveTo(58,-61.4,58,-51.7).lineTo(58,-51).lineTo(58,-50.5).lineTo(57.9,-49.9).lineTo(57.8,-49.7).lineTo(57.6,-49.5).lineTo(57.5,-49.4).lineTo(56.4,-48.9).curveTo(55.6,-48.7,55.8,-48.1).curveTo(58.2,-41.8,61.1,-35.5).curveTo(63.2,-31,64.7,-26).curveTo(66.6,-19.4,69.3,-15.9).curveTo(70.7,-14,74,-11.6).curveTo(76.5,-9.6,77.5,-7.5).curveTo(78.9,-4.3,78.9,2).curveTo(78.9,5.3,75.9,8.5).curveTo(72.2,12.4,66.2,12.8).lineTo(65.6,12.9).lineTo(60.1,12.7).closePath().moveTo(37.3,-73.8).lineTo(37.1,-74.2).lineTo(37.3,-73.8).closePath();
	this.shape_6.setTransform(78.9,87.4);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,173.4);


(lib.KateSkirt = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.GirlOutfit04();
	this.instance.setTransform(82.1,89.6,1.12,1.12,0,0,0,62.6,83.9);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#4E2A3F").setStrokeStyle(1,1,1).moveTo(31.2,6.8).curveTo(31,6.6,30.9,6.4).curveTo(25,-3.1,15.4,-5.9).curveTo(12.3,-6.8,5.6,-6.8).curveTo(-5.5,-6.8,-16.6,-4.1).curveTo(-24.8,-2.1,-29.1,0.2).curveTo(-30.3,0.9,-31.2,1.5);
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#371E2D").beginStroke().moveTo(-16.8,4).curveTo(-22.1,3.6,-22.9,3.3).lineTo(-22,0.9).curveTo(-21.5,-0.6,-19.9,-1.5).lineTo(-18.5,-2.1).lineTo(-17.9,-2.3).lineTo(-16.3,-2.6).lineTo(-16.4,-1.9).curveTo(-16.3,1.3,-6.5,3.8).lineTo(-3.9,4.4).curveTo(-6.8,4.5,-10.3,4.5).curveTo(-11.9,4.5,-16.8,4).closePath().moveTo(13.1,1.4).curveTo(13.2,0.1,12.8,-1.2).curveTo(13.4,-1.2,13.9,-1.9).curveTo(14.7,-2.9,15.2,-3.3).curveTo(16.2,-4,17.7,-4.5).curveTo(18.8,0,20.1,1.5).curveTo(20.9,2.5,22.8,3.3).lineTo(15.7,3.9).curveTo(13.7,4,13,4.1).curveTo(13.2,3.1,13.1,1.4).closePath();
	this.shape_1.setTransform(73,158.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("#4D2A3E").beginStroke().moveTo(-19,4.3).curveTo(-28.8,1.8,-28.8,-1.4).lineTo(-28.8,-2.1).curveTo(-22.9,-3.4,-8.9,-3.3).curveTo(-6.9,-3.1,-5.3,-1.6).curveTo(-3.8,-0.1,-3.8,1.7).lineTo(-4.3,2.2).curveTo(-5.1,2.8,-6.7,3.4).curveTo(-10.1,4.5,-16.4,4.9).lineTo(-19,4.3).closePath().moveTo(7.6,2).curveTo(6.3,0.5,5.2,-4).curveTo(8.1,-4.9,13.1,-4.8).curveTo(20.3,-4.8,24.4,-3).curveTo(27.9,-1.4,28.8,1.3).curveTo(29.1,2,25.8,2.5).curveTo(20.4,3.1,10.4,3.9).curveTo(8.4,3,7.6,2).closePath();
	this.shape_2.setTransform(85.5,158.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#CCB59F").beginStroke().moveTo(8,56.3).curveTo(8.2,55.7,8.2,53.5).curveTo(8.6,49.5,8.1,46.3).curveTo(7.7,44.1,6.9,42.6).curveTo(11.1,42.5,15.3,42.2).lineTo(16.6,52.2).curveTo(15,52.5,13.4,53).curveTo(9.4,54.3,8.1,56.4).lineTo(8,56.3).closePath().moveTo(-23.6,45.5).curveTo(-26.2,38.1,-28,35.2).curveTo(-26.3,35.6,-22.7,38.2).curveTo(-19.3,40.8,-17.4,41.1).lineTo(-13.7,53).curveTo(-17.2,53.6,-20.1,54.9).lineTo(-23.6,45.5).closePath().moveTo(-65,1.9).curveTo(-69.2,-2.1,-70.7,-8.6).curveTo(-71,-9.9,-69.9,-11.9).curveTo(-68.2,-14.7,-67.2,-16.7).curveTo(-62.6,-24.9,-61.8,-36).curveTo(-61.6,-38.6,-59.7,-47.1).lineTo(-57.7,-56.4).lineTo(-56.5,-55.9).curveTo(-55.7,-55.6,-55.2,-55.5).curveTo(-55.9,-51.3,-57.1,-37.5).curveTo(-58.4,-21.8,-58.5,-21.6).curveTo(-59.9,-17.1,-62.8,-12.7).curveTo(-64.5,-9,-63,-2.8).lineTo(-61.9,1.1).lineTo(-60.8,4.9).curveTo(-63,3.9,-65,1.9).closePath().moveTo(64.1,-0.1).curveTo(61.1,-2.4,59.6,-5.9).curveTo(59.1,-7.1,58.6,-9.5).curveTo(58.4,-11.4,58.4,-13.8).curveTo(57.4,-20.1,54.4,-25.5).lineTo(53.8,-26.5).curveTo(52.7,-28.2,52.1,-28.6).lineTo(51.3,-31.7).curveTo(49.9,-37.2,49.4,-40.3).lineTo(47.9,-47.4).curveTo(53.7,-36.7,57.5,-28).curveTo(60.9,-19.7,61.2,-16.6).curveTo(61.7,-11.5,63.8,-7.4).curveTo(66.5,-2.5,70.8,-1.5).lineTo(68.8,-0.9).curveTo(68.3,-0.7,68,-0.4).lineTo(68,-0.5).lineTo(66.9,-0.1).closePath();
	this.shape_3.setTransform(75,96.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#381E2D").beginStroke().moveTo(25.4,31.5).curveTo(17.9,30.5,5,29).curveTo(-5.1,27.8,-13.5,26.1).curveTo(-13,25.3,-12.3,23.4).curveTo(-11,19.6,-10.1,13.7).lineTo(-8.7,5.5).curveTo(-8.2,2.9,-8.2,-0.1).lineTo(-8.2,-1.1).lineTo(-8.2,-1.3).lineTo(-8.3,-2.2).lineTo(-8.4,-2.9).curveTo(-7.7,-3.1,-7,-3.2).curveTo(-6.5,-3.3,-5.6,-2).curveTo(-4.9,-1,-4.4,0.2).curveTo(-4.1,0.8,-3.3,5.6).curveTo(-1.9,10.9,2,15.3).curveTo(6,19.7,11.5,21.9).curveTo(14.7,23.2,20.9,24.3).curveTo(26.5,25.3,28.6,26.4).curveTo(31.9,28.2,32.7,32.1).curveTo(28.5,31.9,25.4,31.5).closePath().moveTo(-24.3,-9.5).lineTo(-29.9,-11.5).curveTo(-32.5,-12.5,-32.6,-13.4).curveTo(-33,-16.1,-29.1,-20.5).curveTo(-26.1,-24,-22.5,-26.5).curveTo(-18,-29.6,-10.6,-32.1).curveTo(-12.7,-30.6,-16.1,-25.9).curveTo(-20,-20.7,-22,-16.1).curveTo(-22.7,-14.7,-23.2,-11.5).curveTo(-23.5,-9.5,-24.2,-9.5).lineTo(-24.3,-9.5).closePath();
	this.shape_4.setTransform(47.8,47.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#EACFB7").beginStroke().moveTo(-10.1,53.4).curveTo(-14,53.1,-17.6,53.4).curveTo(-19.4,53.6,-21,53.9).lineTo(-24.7,42).curveTo(-15,43.5,-5,43.5).curveTo(-5,44.4,-4.8,45.2).lineTo(-4.1,50.3).lineTo(-3.6,54.7).curveTo(-6,53.8,-10.1,53.4).closePath().moveTo(19.4,52.6).curveTo(13.8,52.2,9.3,53).lineTo(8,43.1).curveTo(19.6,42.1,31.1,39.5).curveTo(30.6,40.7,30.3,41.9).curveTo(30.1,43,28.2,48.2).curveTo(26.8,51.8,26.6,53.8).curveTo(23.3,52.8,19.4,52.6).closePath().moveTo(-63.1,6.8).lineTo(-64.6,6.6).curveTo(-65.7,6.5,-66.7,6.2).lineTo(-68.1,5.7).lineTo(-69.2,1.9).lineTo(-70.3,-1.9).curveTo(-71.8,-8.2,-70.1,-11.8).curveTo(-67.2,-16.3,-65.8,-20.7).curveTo(-65.7,-21,-64.4,-36.7).curveTo(-63.2,-50.5,-62.5,-54.7).curveTo(-61.4,-54.6,-60,-54).lineTo(-58.1,-53.2).curveTo(-55.4,-52.2,-50.8,-48.7).lineTo(-49.6,-47.8).lineTo(-50.4,-45.9).curveTo(-53,-39.5,-55.3,-28.6).curveTo(-57.7,-17.4,-57.4,-15.4).lineTo(-57.4,-15.3).curveTo(-57.2,-14.4,-54.5,-9.5).lineTo(-53.6,-7.8).curveTo(-50.3,-1.5,-50,-0.4).lineTo(-49.9,-0.3).curveTo(-49.4,1.8,-50.9,3.6).lineTo(-51.6,4.3).curveTo(-54.3,6.8,-60.6,6.8).lineTo(-63.1,6.8).closePath().moveTo(56.5,-6.5).curveTo(54.4,-10.7,53.9,-15.7).curveTo(53.6,-18.9,50.2,-27.1).curveTo(46.4,-35.9,40.5,-46.6).curveTo(39.6,-51.1,39.4,-52.9).lineTo(40.7,-53.3).lineTo(40.9,-53.3).lineTo(41.6,-53.4).curveTo(43.7,-53.9,46,-53.9).lineTo(46.4,-53.8).lineTo(48.6,-54.7).curveTo(49.3,-51.5,53.7,-41.3).curveTo(56,-35.8,56.7,-33.6).curveTo(57.1,-32.4,57.3,-31).curveTo(58.3,-27.3,60.8,-24.2).curveTo(61.8,-22.9,65.3,-19.5).curveTo(71,-13.9,71,-7.2).curveTo(71,-2.8,65.4,-1.2).lineTo(63.5,-0.7).curveTo(59.1,-1.7,56.5,-6.5).closePath();
	this.shape_5.setTransform(82.3,95.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#4D2A3F").beginStroke().moveTo(1.9,39.5).curveTo(1.1,35.6,-2.2,33.8).curveTo(-4.3,32.7,-9.8,31.7).curveTo(-16,30.6,-19.2,29.3).curveTo(-24.8,27.1,-28.7,22.7).curveTo(-32.6,18.3,-34.1,13).curveTo(-34.9,8.2,-35.2,7.6).curveTo(-35.6,6.4,-36.4,5.4).curveTo(-37.3,4.1,-37.8,4.2).curveTo(-38.5,4.3,-39.1,4.5).curveTo(-39.3,3.5,-39.6,2.9).curveTo(-39.8,2.4,-40.1,2.2).curveTo(-41.1,1.5,-42.7,3.6).lineTo(-43.6,5).lineTo(-45.2,5.3).lineTo(-45.3,5).lineTo(-49.4,1).curveTo(-50.5,-0.1,-51.5,-0.7).lineTo(-55,-2.1).curveTo(-54.2,-2,-53.9,-4.1).curveTo(-53.4,-7.3,-52.8,-8.7).curveTo(-50.7,-13.3,-46.9,-18.5).curveTo(-43.4,-23.2,-41.3,-24.7).lineTo(-29,-28.9).curveTo(-23.3,-30.9,-22.5,-31.5).curveTo(-22.2,-31.7,-22.3,-32.2).lineTo(-22.6,-32.8).curveTo(-18.3,-35.2,-10.2,-37.2).curveTo(1,-39.9,12.1,-39.9).curveTo(18.8,-39.9,21.9,-39).curveTo(31.4,-36.2,37.4,-26.7).curveTo(36.2,-26.6,36.2,-25.4).curveTo(36.2,-24.7,41.7,-21.2).curveTo(48.2,-17.1,50.2,-15.1).curveTo(51.5,-13.8,53.7,-7.4).curveTo(54.4,-5.3,55,-4).curveTo(53.9,-3.6,52.6,-2.9).curveTo(50.1,-1.7,48.7,-0.8).curveTo(47.2,-4,44.6,-7).curveTo(41.9,-10.3,40,-10.7).curveTo(37.9,-11.1,37.9,-7.7).curveTo(37.9,-7.2,42,-1.5).lineTo(43.2,0.3).lineTo(44.2,2).lineTo(44.6,2.7).lineTo(44.4,2.8).lineTo(43.1,3.2).curveTo(41.3,3.9,40.9,5).curveTo(40.4,6.4,39.8,8.9).curveTo(39.1,11.9,39,13).lineTo(38.9,16.6).curveTo(38.9,21.6,40.8,26.4).curveTo(41.9,29.5,45.8,36.7).lineTo(46.1,37.3).lineTo(41.6,38.2).curveTo(35.1,39.6,18.7,39.8).lineTo(14.9,39.9).curveTo(7.4,39.9,1.9,39.5).closePath();
	this.shape_6.setTransform(78.6,39.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#000000").beginStroke().moveTo(-25.8,80.7).curveTo(-31.9,79.6,-32.6,78.5).curveTo(-33.4,77.4,-33.4,75.2).curveTo(-33.4,70.1,-28.2,66.5).curveTo(-30.4,63.3,-34.4,53).lineTo(-37.9,43.5).lineTo(-37.9,43).lineTo(-31.9,44.6).curveTo(-30.1,47.5,-27.5,54.8).lineTo(-24,64.2).curveTo(-21.1,63,-17.6,62.4).curveTo(-16,62.1,-14.2,62).curveTo(-10.6,61.7,-6.7,62).curveTo(-2.6,62.4,-0.2,63.2).lineTo(-0.7,58.9).lineTo(-1.4,53.8).curveTo(-1.6,53,-1.6,52.1).lineTo(3,52).curveTo(3.8,53.4,4.2,55.7).curveTo(4.7,58.9,4.3,62.9).curveTo(4.3,65,4.1,65.7).lineTo(4.2,65.7).curveTo(5.5,63.7,9.5,62.4).curveTo(11.1,61.9,12.7,61.6).curveTo(17.2,60.7,22.8,61.2).curveTo(26.7,61.4,30,62.3).curveTo(30.2,60.4,31.6,56.7).curveTo(33.5,51.5,33.7,50.4).curveTo(34,49.2,34.5,48.1).lineTo(39.7,46.8).curveTo(39.6,49.3,37.9,55.2).curveTo(36.2,61.1,34.1,63.8).lineTo(36,64.7).curveTo(42.3,68.3,42,73.6).curveTo(41.9,75.1,41.4,75.6).curveTo(40.9,76.4,39,76.8).curveTo(37.6,77.2,21.6,79.2).curveTo(15,79.9,9.5,79.2).curveTo(7.3,78.7,7.2,78).lineTo(6.9,76.5).lineTo(6.5,77.1).curveTo(4.4,79.5,-4.7,80.7).curveTo(-10.5,81,-15.8,81).curveTo(-21,81,-25.8,80.7).closePath().moveTo(-22.2,68.6).lineTo(-23.8,69).lineTo(-24.4,69.2).lineTo(-25.8,69.8).curveTo(-27.3,70.7,-27.9,72.1).lineTo(-28.7,74.6).curveTo(-28,74.9,-22.7,75.3).curveTo(-17.8,75.8,-16.1,75.7).curveTo(-12.7,75.8,-9.8,75.6).curveTo(-3.5,75.2,-0.1,74.1).curveTo(1.5,73.5,2.3,72.9).lineTo(2.8,72.4).curveTo(2.8,70.7,1.3,69.2).curveTo(-0.3,67.6,-2.3,67.4).lineTo(-4.3,67.4).curveTo(-16.8,67.4,-22.2,68.6).closePath().moveTo(11.8,66.7).curveTo(10.3,67.2,9.3,68).curveTo(8.8,68.3,8,69.3).curveTo(7.5,70.1,6.9,70.1).curveTo(7.3,71.3,7.3,72.7).curveTo(7.3,74.4,7.1,75.4).curveTo(7.8,75.3,9.8,75.2).lineTo(17,74.6).curveTo(27,73.8,32.4,73.2).curveTo(35.7,72.8,35.4,72).curveTo(34.5,69.4,31,67.7).curveTo(26.9,65.9,19.7,65.9).lineTo(19.1,65.9).curveTo(14.5,65.9,11.8,66.7).closePath().moveTo(-57,19.6).curveTo(-59.5,19.5,-61.6,19.2).lineTo(-66.8,18.1).curveTo(-70.7,17,-73.1,15.1).curveTo(-77.4,11.6,-78.5,4).curveTo(-79.3,-0.9,-78.4,-3.7).curveTo(-77.8,-5.5,-75.8,-8.3).curveTo(-70.8,-15.4,-69.3,-26.5).curveTo(-68.6,-32,-67.6,-36.7).lineTo(-65.4,-46.7).lineTo(-65.3,-49.4).curveTo(-66.2,-49.7,-66.7,-50.4).curveTo(-67.2,-51.1,-67.3,-52.4).lineTo(-67.2,-53.8).lineTo(-67.2,-53.9).curveTo(-66.4,-63.1,-52.8,-71).curveTo(-40.1,-78.5,-23.3,-81).curveTo(-23.2,-81,-23,-80.4).lineTo(-22.9,-80.3).curveTo(-24.1,-79.7,-25,-79).curveTo(-24.1,-79.7,-22.9,-80.3).lineTo(-22.6,-79.7).curveTo(-22.5,-79.2,-22.8,-79).curveTo(-23.6,-78.4,-29.3,-76.4).lineTo(-41.6,-72.2).curveTo(-49,-69.7,-53.5,-66.6).curveTo(-57.1,-64.1,-60.2,-60.5).curveTo(-64,-56.1,-63.7,-53.4).curveTo(-63.5,-52.5,-61,-51.6).lineTo(-55.3,-49.6).lineTo(-51.8,-48.1).curveTo(-50.8,-47.6,-49.7,-46.5).lineTo(-45.6,-42.4).lineTo(-45.5,-42.2).lineTo(-43.9,-42.4).lineTo(-43,-43.9).curveTo(-41.4,-46,-40.4,-45.3).curveTo(-40.2,-45.1,-39.9,-44.6).lineTo(-47.2,-22.7).curveTo(-49.6,-17.2,-49.9,-12.8).curveTo(-50.6,-8,-48.7,-5.7).curveTo(-47.8,-3.8,-45.8,-0.1).curveTo(-45,1.4,-43.9,4.7).curveTo(-42.3,9.6,-42.8,11.8).curveTo(-43.3,13.7,-45.7,15.9).curveTo(-47.4,17.3,-48.4,17.8).lineTo(-49.4,18.3).curveTo(-52.4,19.6,-56.1,19.6).lineTo(-57,19.6).closePath().moveTo(-63.6,-37.7).curveTo(-65.5,-29.2,-65.7,-26.7).curveTo(-66.5,-15.5,-71.1,-7.3).curveTo(-72.1,-5.3,-73.8,-2.6).curveTo(-74.9,-0.5,-74.6,0.8).curveTo(-73.1,7.3,-68.9,11.3).curveTo(-66.9,13.3,-64.7,14.3).lineTo(-63.3,14.7).curveTo(-62.3,15.1,-61.2,15.2).lineTo(-59.7,15.3).curveTo(-51.4,15.9,-48.2,12.8).lineTo(-47.5,12.2).curveTo(-46,10.3,-46.5,8.2).lineTo(-46.6,8.1).curveTo(-46.9,7,-50.2,0.8).lineTo(-51.1,-0.9).curveTo(-53.8,-5.9,-54,-6.7).lineTo(-54,-6.9).curveTo(-54.3,-8.9,-51.9,-20).curveTo(-49.6,-31,-47,-37.3).lineTo(-46.2,-39.3).lineTo(-47.4,-40.2).curveTo(-52,-43.7,-54.7,-44.7).lineTo(-56.6,-45.4).curveTo(-58,-46,-59.1,-46.1).curveTo(-59.6,-46.2,-60.4,-46.6).lineTo(-61.6,-47).lineTo(-63.6,-37.7).closePath().moveTo(60.1,12.7).lineTo(60.1,12.7).lineTo(59.8,12.7).curveTo(58,12.7,56.6,11).curveTo(54.6,9.2,53,4.4).curveTo(51.1,-1.2,51.1,-3.7).lineTo(51.2,-5.4).curveTo(51.1,-7.1,49.6,-11).curveTo(47.4,-16.7,47.4,-17).lineTo(47.4,-17.9).lineTo(47.4,-17.9).lineTo(47.4,-18).curveTo(47.4,-19.7,48.2,-19.2).curveTo(48.8,-18.8,49.9,-17.1).lineTo(50.5,-16.1).curveTo(53.5,-10.7,54.5,-4.4).curveTo(54.5,-2.1,54.7,-0.1).curveTo(55.2,2.3,55.7,3.5).curveTo(57.1,7,60.2,9.3).lineTo(63,9.3).lineTo(64,8.9).lineTo(64.1,8.9).curveTo(64.4,8.7,64.9,8.4).lineTo(66.9,7.9).lineTo(68.8,7.3).curveTo(74.4,5.8,74.4,1.4).curveTo(74.4,-5.4,68.7,-10.9).curveTo(65.2,-14.3,64.2,-15.6).curveTo(61.7,-18.7,60.7,-22.5).curveTo(60.5,-23.8,60.1,-25.1).curveTo(59.4,-27.3,57.1,-32.7).curveTo(52.7,-42.9,52,-46.1).lineTo(49.8,-45.3).lineTo(49.4,-45.3).curveTo(47.1,-45.3,45,-44.9).lineTo(44.3,-44.8).lineTo(43.9,-45.4).lineTo(42.9,-47.2).lineTo(41.7,-48.9).curveTo(37.6,-54.7,37.6,-55.2).curveTo(37.6,-58.5,39.7,-58.2).curveTo(41.6,-57.7,44.3,-54.5).curveTo(46.9,-51.4,48.4,-48.3).curveTo(49.8,-49.2,52.3,-50.4).curveTo(53.6,-51.1,54.7,-51.4).curveTo(54.1,-52.8,53.4,-54.9).curveTo(51.2,-61.3,49.9,-62.6).curveTo(47.9,-64.6,41.4,-68.7).curveTo(35.9,-72.2,35.9,-72.9).curveTo(35.9,-74.1,37.1,-74.2).curveTo(37.9,-74.3,39.4,-73.8).curveTo(42.8,-72.8,47,-69.7).curveTo(58,-61.4,58,-51.7).lineTo(58,-51).lineTo(58,-50.5).lineTo(57.9,-49.9).lineTo(57.8,-49.7).lineTo(57.6,-49.5).lineTo(57.5,-49.4).lineTo(56.4,-48.9).curveTo(55.6,-48.7,55.8,-48.1).curveTo(58.2,-41.8,61.1,-35.5).curveTo(63.2,-31,64.7,-26).curveTo(66.6,-19.4,69.3,-15.9).curveTo(70.7,-14,74,-11.6).curveTo(76.5,-9.6,77.5,-7.5).curveTo(78.9,-4.3,78.9,2).curveTo(78.9,5.3,75.9,8.5).curveTo(72.2,12.4,66.2,12.8).lineTo(65.6,12.9).lineTo(60.1,12.7).closePath().moveTo(37.3,-73.8).lineTo(37.1,-74.2).lineTo(37.3,-73.8).closePath();
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,173.4);


(lib.KateOptions = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 1
	this.instance = new lib.GHAIR();
	this.instance.setTransform(84,107.7,1,1,0,0,0,84,107.7);

	this.instance_1 = new lib.HeadG();
	this.instance_1.setTransform(90.5,146.6,1,1,0,0,0,69.8,62);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(5));

	// Layer 2
	this.instance_2 = new lib.KateClothes();
	this.instance_2.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_3 = new lib.FemaleTrousers();
	this.instance_3.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_4 = new lib.FemaleHoodie();
	this.instance_4.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_5 = new lib.FemaleMotif();
	this.instance_5.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_6 = new lib.KateSkirt();
	this.instance_6.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,171.6,356.9);


(lib.Jason_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 2
	this.instance = new lib.Jason();
	this.instance.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 1
	this.instance_1 = new lib.Male_Body_Frank_Front();
	this.instance_1.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_2 = new lib.SteveShirt();
	this.instance_2.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_3 = new lib.SteveHoodie();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.Motif();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.SteveShirtandTie();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.JaneOptions = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 1
	this.instance = new lib.HInfront();
	this.instance.setTransform(87.1,123,0.97,1.07,0,0,0,84.5,80.2);

	this.instance_1 = new lib.JaneHead();
	this.instance_1.setTransform(90,146.6,1,1,0,0,0,69.8,62);

	this.instance_2 = new lib.Behind();
	this.instance_2.setTransform(155.1,157.5,1.07,1.07,8.5,0,0,20.5,40.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(5));

	// Layer 2
	this.instance_3 = new lib.JaneClothes();
	this.instance_3.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_4 = new lib.FemaleTrousersJane();
	this.instance_4.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_5 = new lib.FemaleHoodieJane();
	this.instance_5.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_6 = new lib.FemaleMotifJane();
	this.instance_6.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_7 = new lib.KateSkirtJane();
	this.instance_7.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3}]}).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(5.2,37.2,178.8,319.7);


(lib.InstructionsBit = function() {
	this.initialize();

	// Layer 1
	this.mc_InstructionGotIt = new lib.GotItButton();
	this.mc_InstructionGotIt.setTransform(340.2,273.1,1,1,0,0,0,99,31.7);
	new cjs.ButtonHelper(this.mc_InstructionGotIt, 0, 1, 2, false, new lib.GotItButton(), 3);

	this.text = new cjs.Text("select the appropriate pigeon hole for each letter before the time runs out. if you make a mistake, the manager will appear and slow you down.", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 565;
	this.text.setTransform(342.8,26.1,1.18,1.18);

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill().beginStroke("#000000").setStrokeStyle(4,1,1).moveTo(-344.9,122.9).lineTo(-344.9,-123.1).curveTo(-344.5,-136.4,-330.7,-136.4).lineTo(330.7,-136.4).curveTo(344.5,-136.4,344.9,-123.1).lineTo(344.9,123.2).curveTo(344.4,136.4,330.7,136.4).lineTo(-330.7,136.4).curveTo(-344.6,136.4,-344.9,122.9).closePath();
	this.shape.setTransform(344.9,136.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#EDED5E").beginStroke().moveTo(-330.7,136.4).curveTo(-344.6,136.4,-344.9,122.9).lineTo(-344.9,-123.1).curveTo(-344.5,-136.4,-330.7,-136.4).lineTo(330.7,-136.4).curveTo(344.5,-136.4,344.9,-123.1).lineTo(344.9,123.2).curveTo(344.4,136.4,330.7,136.4).closePath();
	this.shape_1.setTransform(344.9,136.4);

	// Layer 3
	this.mc_InstructionBitGotIt = new lib.GotItInstructionBit();
	this.mc_InstructionBitGotIt.setTransform(353.9,145.4,1,1,0,0,0,344.9,136.4);
	new cjs.ButtonHelper(this.mc_InstructionBitGotIt, 0, 1, 1);

	this.addChild(this.mc_InstructionBitGotIt,this.shape_1,this.shape,this.text,this.mc_InstructionGotIt);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,700.8,308.8);


(lib.Highlighter = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_36 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(36).call(this.frame_36).wait(1));

	// HIGHLIGHTER
	this.instance = new lib.Highlight("synched",0,false);
	this.instance.setTransform(24.8,24.8,1,1,0,0,0,24.8,24.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(36).to({startPosition:23},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(2.8,2.8,44.1,44.1);


(lib.Health_and_Safety_Bubble = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_4 = function() {
		this.stop();
	}
	this.frame_29 = function() {
		gamePost.HnSMessageDone();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(4).call(this.frame_4).wait(25).call(this.frame_29).wait(1));

	// Layer 2
	this.btn_HnSGotIt = new lib.GotItButton();
	this.btn_HnSGotIt.setTransform(589.4,384.6,0.117,0.117,0,0,0,99,31.7);
	new cjs.ButtonHelper(this.btn_HnSGotIt, 0, 1, 2, false, new lib.GotItButton(), 3);

	this.timeline.addTween(cjs.Tween.get(this.btn_HnSGotIt).to({scaleX:1,scaleY:1,x:339.3,y:336.4},4).wait(20).to({scaleX:0.12,scaleY:0.12,x:589.4,y:384.6},4).to({_off:true},1).wait(1));

	// Layer 1
	this.instance = new lib.HS_Bubble();
	this.instance.setTransform(622.4,390.9,0.117,0.117,0,0,0,418.4,161.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:161.1,scaleX:1,scaleY:1,x:622.5},4).wait(20).to({regY:161.2,scaleX:0.12,scaleY:0.12,x:622.4},4).to({_off:true},1).wait(1));

	// Layer 4
	this.mc_bg = new lib._50Percent_white();
	this.mc_bg.setTransform(276,177,1,1,0,0,0,500,285.9);

	this.timeline.addTween(cjs.Tween.get(this.mc_bg).wait(30));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-224,-108.9,1000,571.9);


(lib.Greg_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 2
	this.instance = new lib.GregHead();
	this.instance.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5));

	// Layer 1
	this.instance_1 = new lib.Male_Greg_Front();
	this.instance_1.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_2 = new lib.FrankShirt();
	this.instance_2.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_3 = new lib.GregHoodie();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.MotifGreg();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.FrankShirtandTie();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.BackArrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Arrow_Butn();
	this.instance.setTransform(34.9,34.9,1,1,0,0,0,34.9,34.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FF0000").beginStroke().moveTo(-26.9,26.9).curveTo(-38,15.8,-38,0).curveTo(-38,-15.7,-26.9,-26.9).curveTo(-19,-34.8,-8.7,-37).curveTo(-4.6,-38,0,-38).curveTo(15.8,-38,26.9,-26.9).curveTo(38,-15.7,38,0).curveTo(38,15.8,26.9,26.9).curveTo(21,32.7,14,35.5).curveTo(7.5,38,0,38).curveTo(-15.7,38,-26.9,26.9).closePath();
	this.shape.setTransform(34.8,34.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regX:34.9,regY:34.9,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regX:34.8,regY:34.8,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.LucyOptions = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 1
	this.instance = new lib.LucyHair();
	this.instance.setTransform(83.8,107.7,1,1,0,0,0,84,107.7);

	this.instance_1 = new lib.LucyHead();
	this.instance_1.setTransform(90.3,146.6,1,1,0,0,0,69.8,62);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#E5C45A").beginStroke().moveTo(-19.1,20.2).curveTo(-20.7,18.4,-21.9,16.2).curveTo(-25.4,9.6,-25.4,0.2).curveTo(-25.4,-10.5,-24.9,-13.1).curveTo(-23.5,-19.7,-17.3,-26.8).curveTo(9.2,-30.8,19.8,-17.1).curveTo(25.4,-9.8,25.5,-0.1).curveTo(25.5,7.6,23,13.8).curveTo(21.7,17.1,19.6,20.2).curveTo(17.1,23.9,13.4,27.4).lineTo(0.2,27.7).curveTo(-12.6,27.7,-19.1,20.2).closePath();
	this.shape.setTransform(30,164.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#000000").beginStroke().moveTo(-27.6,20.8).lineTo(-28.3,19.7).curveTo(-31,14.8,-31.5,8.3).lineTo(-31.5,0.6).curveTo(-31.5,-18.4,-21.6,-27.7).curveTo(-13,-35.9,-0,-34.4).curveTo(12.6,-33,21.7,-23.6).curveTo(31.5,-13.6,31.5,0.4).curveTo(31.5,10,27.8,19.2).lineTo(27.1,20.8).curveTo(20.7,34.7,7.6,34.7).curveTo(-18.9,34.7,-27.6,20.8).closePath().moveTo(-17.2,-26.2).curveTo(-23.5,-19.1,-24.9,-12.5).curveTo(-25.4,-9.9,-25.4,0.8).curveTo(-25.4,10.3,-21.9,16.8).curveTo(-20.7,19,-19.1,20.8).curveTo(-12.5,28.3,0.2,28.3).lineTo(13.5,28.1).curveTo(17.2,24.6,19.7,20.8).curveTo(21.7,17.7,23,14.4).curveTo(25.5,8.3,25.5,0.6).curveTo(25.5,-9.2,19.9,-16.5).curveTo(11.7,-27.1,-6.1,-27.1).curveTo(-11.3,-27.1,-17.2,-26.2).closePath();
	this.shape_1.setTransform(29.9,163.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance_1},{t:this.instance}]}).wait(5));

	// Layer 2
	this.instance_2 = new lib.LucyClothes();
	this.instance_2.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_3 = new lib.FemaleTrousers();
	this.instance_3.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_4 = new lib.FemaleHoodie();
	this.instance_4.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_5 = new lib.FemaleMotif();
	this.instance_5.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_6 = new lib.KateSkirt();
	this.instance_6.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.6,30,175.4,326.9);


// stage content:
(lib.The_Post_Room_18_CC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{ChooseSteveWorkwear:0,NotSuitableSteve:1,ChooseKateWorkwear:14,NotSuitableKate:15,Instructions:28,Game:34,GameQuit:36,Boss:38,NotEnoughForBadge:50,AlreadyGotBadge:51,EarnedBadge:52,CloseEarnedBadge:61,PostRoomLeaderboard:71});

	// timeline functions:
	this.frame_5 = function() {
		this.stop();
	}
	this.frame_14 = function() {
		this.stop();
	}
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_32 = function() {
		this.stop();
	}
	this.frame_34 = function() {
		this.stop();
	}
	this.frame_35 = function() {
		this.stop();
	}
	this.frame_36 = function() {
		this.stop();
	}
	this.frame_42 = function() {
		gamePost.bossDoneShowing();
		this.stop();
	}
	this.frame_50 = function() {
		this.stop();
	}
	this.frame_51 = function() {
		this.stop();
	}
	this.frame_60 = function() {
		this.stop();
	}
	this.frame_67 = function() {
		this.gotoAndStop("EarnedBadge");
	}
	this.frame_71 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(5).call(this.frame_5).wait(9).call(this.frame_14).wait(5).call(this.frame_19).wait(13).call(this.frame_32).wait(2).call(this.frame_34).wait(1).call(this.frame_35).wait(1).call(this.frame_36).wait(6).call(this.frame_42).wait(8).call(this.frame_50).wait(1).call(this.frame_51).wait(9).call(this.frame_60).wait(7).call(this.frame_67).wait(4).call(this.frame_71).wait(1));

	// Close Game
	this.mc_ConfirmQuitGame = new lib.Sure_you_wanna_quit();
	this.mc_ConfirmQuitGame.setTransform(500.7,180.7,1,1,0,0,0,301.7,173.7);
	this.mc_ConfirmQuitGame.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.mc_ConfirmQuitGame).wait(72));

	// HnS Message
	this.mc_HealtySafetyMessage = new lib.Health_and_Safety_Bubble();
	this.mc_HealtySafetyMessage.setTransform(507.3,268.8,1,1,0,0,0,301.7,173.7);
	this.mc_HealtySafetyMessage.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.mc_HealtySafetyMessage).wait(72));

	// Loader
	this.mc_loader = new lib.WindmillLoader();
	this.mc_loader.setTransform(481.7,286,1,1,0,0,0,64.7,64.7);
	this.mc_loader.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.mc_loader).wait(72));

	// Menu
	this.mc_GameMenuOn = new lib.Options_btn();
	this.mc_GameMenuOn.setTransform(45,35.4,0.712,0.712,0,0,0,34.9,34.9);
	this.mc_GameMenuOn.visible = false;

	this.mc_MainMenu = new lib.OptionsMenu();
	this.mc_MainMenu.setTransform(139.6,185.5,1,1,0,0,0,139.6,185.5);
	this.mc_MainMenu.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);
	this.mc_MainMenu.visible = false;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_MainMenu},{t:this.mc_GameMenuOn}]}).wait(72));

	// Layer6
	this.mc_GameMenu = new lib.Options_btn();
	this.mc_GameMenu.setTransform(45,35.4,0.712,0.712,0,0,0,34.9,34.9);
	this.mc_GameMenu._off = true;

	this.timeline.addTween(cjs.Tween.get(this.mc_GameMenu).wait(34).to({_off:false},0).to({_off:true},16).wait(22));

	// Layer5
	this.mc_PostGameMenu = new lib.OptionsMenu();
	this.mc_PostGameMenu.setTransform(151.7,189.4,1,1,0,0,0,139.6,185.5);
	this.mc_PostGameMenu.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);

	this.mc_QuitConfirmInit = new lib.Sure_you_wanna_quit();
	this.mc_QuitConfirmInit.setTransform(501.4,180.7,1,1,0,0,0,301.7,173.7);

	this.mc_ReplayGame = new lib.replaybtn();
	this.mc_ReplayGame.setTransform(114.9,35.4,0.712,0.712,0,0,0,34.9,34.9);

	this.mc_QuitGame = new lib.Arrow_Butncopy();
	this.mc_QuitGame.setTransform(916.3,37.4,0.712,0.712,0,0,180,34.9,34.9);

	this.mc_GameMenu_1 = new lib.Options_btn();
	this.mc_GameMenu_1.setTransform(45,35.4,0.712,0.712,0,0,0,34.9,34.9);

	this.mc_TimeUpTitle = new cjs.Text("TIME'S UP!", "37px 'VAGRounded BT'", "#FFFFFF");
	this.mc_TimeUpTitle.name = "mc_TimeUpTitle";
	this.mc_TimeUpTitle.textAlign = "center";
	this.mc_TimeUpTitle.lineHeight = 43;
	this.mc_TimeUpTitle.lineWidth = 593;
	this.mc_TimeUpTitle.setTransform(469.4,7.8,1.29,1.29);

	this.shape = new cjs.Shape();
	this.shape.graphics.beginLinearGradientFill(["#4F3F5B","#4F2A40"],[0,1],0,-35,0,35.1).beginStroke().moveTo(-485,37.5).lineTo(-485,-37.5).lineTo(485,-37.5).lineTo(485,37.5).closePath();
	this.shape.setTransform(480.5,36.3);

	this.mc_LBGameMenu = new lib.OptionsBtn();
	this.mc_LBGameMenu.setTransform(45,35.4,0.712,0.712,0,0,0,34.9,34.9);
	this.mc_LBGameMenu.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);
	new cjs.ButtonHelper(this.mc_LBGameMenu, 0, 1, 2, false, new lib.OptionsBtn(), 3);

	this.instance = new lib.BackArrow();
	this.instance.setTransform(114.9,35.4,0.712,0.712,0,0,0,34.9,34.9);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);
	new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.BackArrow(), 3);

	this.instance_1 = new lib.CloseX();
	this.instance_1.setTransform(916.3,37.4,0.712,0.712,0,0,180,34.9,34.9);
	this.instance_1.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);
	new cjs.ButtonHelper(this.instance_1, 0, 1, 2, false, new lib.CloseX(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.mc_PostGameMenu}]},35).to({state:[{t:this.mc_QuitConfirmInit}]},1).to({state:[]},1).to({state:[{t:this.shape},{t:this.mc_TimeUpTitle,p:{x:469.4,y:7.8,text:"TIME'S UP!",font:"37px 'VAGRounded BT'",lineHeight:43}},{t:this.mc_GameMenu_1},{t:this.mc_QuitGame},{t:this.mc_ReplayGame}]},13).to({state:[{t:this.shape},{t:this.instance_1},{t:this.instance},{t:this.mc_LBGameMenu},{t:this.mc_TimeUpTitle,p:{x:467.5,y:15,text:"THE POST ROOM LEADERBOARD",font:"27px 'VAGRounded'",lineHeight:33}}]},21).wait(1));

	// Layer1
	this.mc_GameMenu_2 = new lib.Options_btn();
	this.mc_GameMenu_2.setTransform(45,35.4,0.712,0.712,0,0,0,34.9,34.9);

	this.mc_CloseGame = new lib.Arrow_Butn();
	this.mc_CloseGame.setTransform(114.9,35.4,0.712,0.712,0,0,0,34.9,34.9);

	this.mc_QuitGame_1 = new lib.Arrow_Butncopy();
	this.mc_QuitGame_1.setTransform(916.3,37.4,0.712,0.712,0,0,180,34.9,34.9);

	this.txt_GameTitle = new cjs.Text("THE POST ROOM", "32px 'VAGRounded BT'", "#FFFFFF");
	this.txt_GameTitle.name = "txt_GameTitle";
	this.txt_GameTitle.textAlign = "center";
	this.txt_GameTitle.lineHeight = 38;
	this.txt_GameTitle.lineWidth = 654;
	this.txt_GameTitle.setTransform(474,9.7,1.29,1.29);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginLinearGradientFill(["#4F3F5B","#4F2A40"],[0,1],0,-35,0,35.1).beginStroke().moveTo(-485,37.5).lineTo(-485,-37.5).lineTo(485,-37.5).lineTo(485,37.5).closePath();
	this.shape_1.setTransform(480.5,36.3);

	this.mc_Timer = new lib.Time();
	this.mc_Timer.setTransform(544.6,34.3,1,0.789,0,0,0,91.1,33.5);
	this.mc_Timer.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);

	this.mc_Scoring = new lib.Score();
	this.mc_Scoring.setTransform(365.8,34.3,1,0.789,0,0,0,91.1,33.5);
	this.mc_Scoring.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);

	this.mc_PostLeaderBoards = new lib.PostRoomLeaderBoard();
	this.mc_PostLeaderBoards.setTransform(477.8,308.3,1,1,0,0,0,393.4,204.2);
	this.mc_PostLeaderBoards.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1,p:{x:480.5,y:36.3}},{t:this.txt_GameTitle},{t:this.mc_QuitGame_1,p:{y:37.4}},{t:this.mc_CloseGame},{t:this.mc_GameMenu_2}]},28).to({state:[{t:this.shape_1,p:{x:483.2,y:35.7}},{t:this.mc_Scoring},{t:this.mc_Timer},{t:this.mc_QuitGame_1,p:{y:36.4}}]},6).to({state:[]},16).to({state:[{t:this.mc_PostLeaderBoards}]},21).wait(1));

	// Letter
	this.mc_Letter = new lib.Letter_mc();
	this.mc_Letter.setTransform(-131.4,269.3,1,1,0,0,0,-103.1,33.1);

	this.mc_newMessage1 = new lib.NewMessage();
	this.mc_newMessage1.setTransform(482.2,477.7,1.52,1.52,0,0,0,103,21.5);
	this.mc_newMessage1.shadow = new cjs.Shadow("rgba(255,255,255,1)",0,0,10);

	this.mc_NewMessageStaticInit = new lib.NewMessageStatic();
	this.mc_NewMessageStaticInit.setTransform(482.2,477.7,1.52,1.52,0,0,0,103,21.5);
	this.mc_NewMessageStaticInit.shadow = new cjs.Shadow("rgba(255,255,255,1)",0,0,10);
	this.mc_NewMessageStaticInit._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.mc_Letter}]},34).to({state:[]},16).to({state:[{t:this.mc_newMessage1}]},3).to({state:[{t:this.mc_NewMessageStaticInit}]},1).to({state:[{t:this.mc_NewMessageStaticInit}]},6).to({state:[{t:this.mc_NewMessageStaticInit}]},1).to({state:[{t:this.mc_NewMessageStaticInit}]},6).to({state:[]},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.mc_NewMessageStaticInit).wait(54).to({_off:false},0).to({y:103.9},6).wait(1).to({y:577.7},6).to({_off:true},1).wait(4));

	// Layer3
	this.mc_ladyBossMC = new lib.LadyBossMC2();
	this.mc_ladyBossMC.setTransform(600.2,684.5,1,1,0,0,0,493.3,84.7);
	this.mc_ladyBossMC._off = true;

	this.mc_Boss = new lib.BossMC();
	this.mc_Boss.setTransform(455.4,789.3,1,1,0,0,0,392.1,178.5);
	this.mc_Boss._off = true;

	this.mc_WinnersCup = new lib.WinnersCupMC();
	this.mc_WinnersCup.setTransform(488.1,646.5,1,1,0,0,0,488.1,271.5);
	this.mc_WinnersCup.alpha = 0;
	this.mc_WinnersCup._off = true;

	this.timeline.addTween(cjs.Tween.get(this.mc_ladyBossMC).wait(1).to({_off:false},0).to({y:284.5},4).to({_off:true},9).wait(1).to({_off:false,y:684.5},0).to({y:284.5},4).to({_off:true},9).wait(44));
	this.timeline.addTween(cjs.Tween.get(this.mc_Boss).wait(38).to({_off:false},0).to({y:361.8},4).to({_off:true},8).wait(22));
	this.timeline.addTween(cjs.Tween.get(this.mc_WinnersCup).wait(54).to({_off:false},0).to({y:521.5,alpha:1},2).to({y:271.5},4).wait(1).to({y:746.5},6).to({_off:true},1).wait(4));

	// Layer4
	this.mc_WhiteCoverMaleInit = new lib.WhiteCover();
	this.mc_WhiteCoverMaleInit.setTransform(650.2,307.2,1,1,0,0,0,296.1,221.1);
	this.mc_WhiteCoverMaleInit.alpha = 0;
	this.mc_WhiteCoverMaleInit._off = true;

	this.mc_Instructions = new lib.InstructionsBit();
	this.mc_Instructions.setTransform(485.6,306.3,0.1,0.1,0,0,0,345.1,152);
	this.mc_Instructions._off = true;

	this.mc_HealthAndSafetyLadyInit = new lib.Health_and_Safety_Advisor();
	this.mc_HealthAndSafetyLadyInit.setTransform(924.5,514.9,1,1,0,0,0,90,90.1);

	this.mc_DidntGetBadge = new lib.DidntGetBadge();
	this.mc_DidntGetBadge.setTransform(488.1,271.5,1,1,0,0,0,488.1,271.5);

	this.mc_PostRoomBadgeAlreadyEarned = new lib.PostRoomBadgeAlreadyGot();
	this.mc_PostRoomBadgeAlreadyEarned.setTransform(488.1,271.5,1,1,0,0,0,488.1,271.5);

	this.mc_PostRoomBadgeEarned = new lib.PostRoomBadge();
	this.mc_PostRoomBadgeEarned.setTransform(488.1,271.5,1,1,0,0,0,488.1,271.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.mc_WhiteCoverMaleInit}]},1).to({state:[{t:this.mc_WhiteCoverMaleInit}]},4).to({state:[]},9).to({state:[{t:this.mc_WhiteCoverMaleInit}]},1).to({state:[{t:this.mc_WhiteCoverMaleInit}]},4).to({state:[{t:this.mc_Instructions}]},9).to({state:[{t:this.mc_Instructions}]},4).to({state:[{t:this.mc_HealthAndSafetyLadyInit}]},2).to({state:[{t:this.mc_DidntGetBadge}]},16).to({state:[{t:this.mc_PostRoomBadgeAlreadyEarned}]},1).to({state:[{t:this.mc_PostRoomBadgeEarned}]},1).to({state:[]},8).to({state:[{t:this.mc_PostRoomBadgeEarned}]},1).wait(11));
	this.timeline.addTween(cjs.Tween.get(this.mc_WhiteCoverMaleInit).wait(1).to({_off:false},0).to({alpha:1},4).to({_off:true},9).wait(1).to({_off:false,alpha:0},0).to({alpha:1},4).to({_off:true},9).wait(44));
	this.timeline.addTween(cjs.Tween.get(this.mc_Instructions).wait(28).to({_off:false},0).to({regX:344.9,regY:152.4,scaleX:1,scaleY:1},4).to({_off:true},2).wait(38));

	// mc_MainAvatar
	this.mc_MainAvatar = new lib.Avatars();
	this.mc_MainAvatar.setTransform(464.9,252.5,1,1,0,0,0,220.5,189.5);

	this.timeline.addTween(cjs.Tween.get(this.mc_MainAvatar).wait(72));

	// assets
	this.mc_FrankOptions = new lib.Frank_options();
	this.mc_FrankOptions.setTransform(178.3,304.6,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_JasonOptions = new lib.Jason_options();
	this.mc_JasonOptions.setTransform(178.3,304.6,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_GregOptions = new lib.Greg_options();
	this.mc_GregOptions.setTransform(178.3,304.6,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_SteveOptions = new lib.Steve_options();
	this.mc_SteveOptions.setTransform(178.3,304.6,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_Steve_Shirt_3 = new lib.shirt03();
	this.mc_Steve_Shirt_3.setTransform(522.9,402.1,1.14,1.14,0,0,0,84.4,77.7);

	this.mc_Steve_Shirt_2 = new lib.shirt04();
	this.mc_Steve_Shirt_2.setTransform(782.9,198.7,1.154,1.154,0,0,0,84.4,77.7);

	this.mc_Steve_Shirt_4 = new lib.shirt2();
	this.mc_Steve_Shirt_4.setTransform(785,401.2,1.14,1.14,0,0,0,84.4,77.7);

	this.mc_Steve_Shirt_1 = new lib.shirt();
	this.mc_Steve_Shirt_1.setTransform(517.8,195.6,1.14,1.14,0,0,0,84.4,77.6);

	this.mc_PeppaOptions = new lib.PeppaOptions();
	this.mc_PeppaOptions.setTransform(179.6,294.4,1,1,0,0,0,85.8,178.5);

	this.mc_LucyOptions = new lib.LucyOptions();
	this.mc_LucyOptions.setTransform(177.4,295.5,1,1,0,0,0,85.8,178.5);

	this.mc_JaneOptions = new lib.JaneOptions();
	this.mc_JaneOptions.setTransform(178.8,295.5,1,1,0,0,0,85.8,178.5);

	this.mc_KateOptions = new lib.KateOptions();
	this.mc_KateOptions.setTransform(175.8,295.5,1,1,0,0,0,85.8,178.5);

	this.mc_Kate_Suit_4 = new lib.GirlOutfit04();
	this.mc_Kate_Suit_4.setTransform(788.4,413.6,1.12,1.12,0,0,0,62.6,83.9);

	this.mc_Kate_Suit_1 = new lib.GirlOutfit01();
	this.mc_Kate_Suit_1.setTransform(528.5,198.2,1.12,1.12,0,0,0,71.2,81);

	this.mc_Kate_Suit_3 = new lib.Girl03();
	this.mc_Kate_Suit_3.setTransform(526.1,410.8,1.12,1.12,0,0,0,66.5,82);

	this.mc_Kate_Suit_2 = new lib.Girl02();
	this.mc_Kate_Suit_2.setTransform(791.9,198.1,1.12,1.12,0,0,0,66.5,82);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.beginFill("rgba(255,255,255,0.502)").beginStroke().moveTo(-488.1,271.5).lineTo(-488.1,-271.5).lineTo(488.1,-271.5).lineTo(488.1,271.5).closePath();
	this.shape_2.setTransform(486.9,271.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_Steve_Shirt_1},{t:this.mc_Steve_Shirt_4},{t:this.mc_Steve_Shirt_2},{t:this.mc_Steve_Shirt_3},{t:this.mc_SteveOptions},{t:this.mc_GregOptions},{t:this.mc_JasonOptions},{t:this.mc_FrankOptions}]}).to({state:[{t:this.mc_Kate_Suit_2},{t:this.mc_Kate_Suit_3},{t:this.mc_Kate_Suit_1},{t:this.mc_Kate_Suit_4},{t:this.mc_KateOptions},{t:this.mc_JaneOptions},{t:this.mc_LucyOptions},{t:this.mc_PeppaOptions}]},14).to({state:[]},14).to({state:[{t:this.shape_2}]},22).wait(22));

	// Layer7
	this.txt_ChooseWorkWear = new cjs.Text("CHOOSE APPROPRIATE WORKWEAR", "27px 'VAGRounded BT'", "#FFFFFF");
	this.txt_ChooseWorkWear.name = "txt_ChooseWorkWear";
	this.txt_ChooseWorkWear.textAlign = "center";
	this.txt_ChooseWorkWear.lineHeight = 33;
	this.txt_ChooseWorkWear.lineWidth = 654;
	this.txt_ChooseWorkWear.setTransform(474,15.7,1.29,1.29);

	this.mc_ConveyOrBelt = new lib.ConveyorBelt();
	this.mc_ConveyOrBelt.setTransform(139.4,375.3,1,1,0,0,0,192.2,108.6);

	this.mc_Table = new lib.Table();
	this.mc_Table.setTransform(501.6,426.4,1,1,0,0,0,237.1,69.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.txt_ChooseWorkWear}]}).to({state:[{t:this.mc_Table},{t:this.mc_ConveyOrBelt}]},28).wait(44));

	// BG2
	this.mc_BGBox = new lib.BGBox();
	this.mc_BGBox.setTransform(199.9,226.3,0.687,2.517,0,0,0,543.5,600.5);
	this.mc_BGBox.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);

	this.mc_Destination6 = new cjs.Text("supply chain", "25px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_Destination6.name = "mc_Destination6";
	this.mc_Destination6.textAlign = "center";
	this.mc_Destination6.lineHeight = 27;
	this.mc_Destination6.lineWidth = 211;
	this.mc_Destination6.setTransform(759.7,382.1,0.949,1.41,0,0,31.3);

	this.mc_Destination5 = new cjs.Text("Marketing \n& comms", "30px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_Destination5.name = "mc_Destination5";
	this.mc_Destination5.textAlign = "center";
	this.mc_Destination5.lineHeight = 32;
	this.mc_Destination5.lineWidth = 188;
	this.mc_Destination5.setTransform(757.8,275.7,0.844,0.798,0,0,22.1);

	this.mc_Destination3 = new cjs.Text("h.r.", "44px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_Destination3.name = "mc_Destination3";
	this.mc_Destination3.textAlign = "center";
	this.mc_Destination3.lineHeight = 46;
	this.mc_Destination3.setTransform(612.5,286.6,0.764,0.86,0,0,33.1);

	this.mc_BtnHR = new lib.ButtonHR();
	this.mc_BtnHR.setTransform(619.9,313.4,0.999,0.68,0,0,13,59.9,60.4);
	new cjs.ButtonHelper(this.mc_BtnHR, 0, 1, 1);

	this.mc_Destination4 = new cjs.Text("commercial", "44px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_Destination4.name = "mc_Destination4";
	this.mc_Destination4.textAlign = "center";
	this.mc_Destination4.lineHeight = 46;
	this.mc_Destination4.setTransform(758.6,193.8,0.549,0.73,0,0,20.5);

	this.mc_BtnSupply = new lib.ButtonSupply();
	this.mc_BtnSupply.setTransform(759.7,406.2,0.9,0.64,0,0,12,77,75.5);
	new cjs.ButtonHelper(this.mc_BtnSupply, 0, 1, 1);

	this.mc_BtnCommercial = new lib.ButtonCommercial();
	this.mc_BtnCommercial.setTransform(759.2,212.5,1,0.55,0,0,18,75.5,56.4);
	new cjs.ButtonHelper(this.mc_BtnCommercial, 0, 1, 1);

	this.mc_BtnMarketing = new lib.ButtonMarketing();
	this.mc_BtnMarketing.setTransform(758.9,309.3,1,0.52,0,0,20,75.4,70.2);
	new cjs.ButtonHelper(this.mc_BtnMarketing, 0, 1, 1);

	this.mc_Destination2 = new cjs.Text("i.t.", "44px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_Destination2.name = "mc_Destination2";
	this.mc_Destination2.textAlign = "center";
	this.mc_Destination2.lineHeight = 46;
	this.mc_Destination2.setTransform(616.4,214.5,0.779,0.83,0,0,24.3);

	this.mc_BtnIT = new lib.ButtonIT();
	this.mc_BtnIT.setTransform(617,230.9,0.999,0.52,0,0,17,59.9,56.7);
	new cjs.ButtonHelper(this.mc_BtnIT, 0, 1, 1);

	this.mc_Destination1 = new cjs.Text("finance", "44px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_Destination1.name = "mc_Destination1";
	this.mc_Destination1.textAlign = "center";
	this.mc_Destination1.lineHeight = 46;
	this.mc_Destination1.setTransform(617.4,140.7,0.606,0.66,0,0,19.8);

	this.mc_BtnFinnance = new lib.ButtonFinnance();
	this.mc_BtnFinnance.setTransform(618.5,154.1,0.999,0.39,0,0,21,59.6,45.2);
	new cjs.ButtonHelper(this.mc_BtnFinnance, 0, 1, 1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.beginFill("#BD3A24").beginStroke().moveTo(7.9,25.2).curveTo(3,21.7,-11.4,12.8).curveTo(-22.4,6.1,-29.2,0.2).curveTo(-42.2,-10.1,-47.9,-14.4).curveTo(-54.7,-19.4,-59.7,-21.7).curveTo(-59.4,-22.2,-59.4,-23.3).lineTo(-59.3,-25.4).curveTo(-59.2,-26.1,-59.8,-60.4).curveTo(-11.2,-32.4,10.2,-19.9).curveTo(35.1,-5.4,59.4,9.6).curveTo(59.4,23.3,59.8,44.8).curveTo(59.8,46.2,59,51.3).curveTo(58.2,56.3,58.5,59.5).lineTo(58.6,60.4).curveTo(35.5,44.6,7.9,25.2).closePath();
	this.shape_3.setTransform(618.8,311.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.beginFill("#443B13").beginStroke().moveTo(153.9,222).lineTo(153.5,222).curveTo(153,222,130.8,206.6).curveTo(104.2,188,78.3,169).curveTo(63.3,158.1,40.1,142.8).curveTo(11.3,123.8,2.6,117.7).curveTo(-48.9,83.7,-85.7,58.8).curveTo(-156.1,11,-156.1,5.3).curveTo(-156.1,4.1,-156,3.7).lineTo(-155.9,3.5).lineTo(-155.3,3.8).curveTo(-156.1,1.4,-156.2,-1.5).lineTo(-156.1,-5.5).curveTo(-155.5,-35.6,-155.4,-50.1).lineTo(-155.3,-55.6).lineTo(-155.3,-89.6).curveTo(-155.3,-109.8,-155.6,-124.4).lineTo(-155.7,-129).curveTo(-155.8,-137.8,-156.1,-144.3).lineTo(-156.1,-144.8).lineTo(-156.1,-148.6).lineTo(-156.1,-176.1).lineTo(-156.1,-188.2).curveTo(-156,-191.8,-156.5,-203.1).curveTo(-156.8,-208.1,-156.8,-210.9).curveTo(-156.7,-213.1,-156.6,-214).lineTo(-156.5,-214.1).lineTo(-156.5,-214.2).curveTo(-156.5,-216.1,-155.9,-218).curveTo(-155.3,-219.8,-154.4,-220.7).curveTo(-154.2,-221.4,-154,-221.6).curveTo(-153.2,-222.3,-151,-221.9).curveTo(-149.8,-221.7,-148.3,-221.2).curveTo(-146.6,-220.5,-145.4,-220.2).curveTo(-140.5,-219.2,-139.3,-218.5).curveTo(-131.5,-216.9,-120.5,-214).lineTo(-108.6,-210.9).lineTo(-95,-207.1).curveTo(-61,-197.4,-0.9,-179.3).curveTo(2.5,-178.1,9.1,-176.1).curveTo(31.7,-169.1,91.9,-152.1).curveTo(149.9,-135.7,155.3,-132.1).curveTo(156.8,-133.6,156.8,-122.6).lineTo(155.9,-106.9).curveTo(155.2,-95.1,155.1,-84.8).lineTo(155.1,-79.8).lineTo(155.1,-79.1).lineTo(155.8,-42.3).curveTo(156.2,-24.3,156.1,-15.5).curveTo(156.1,-10.9,156,-8.9).lineTo(155.9,-8).lineTo(155.9,20.1).lineTo(155.9,24.2).lineTo(155.9,32.4).curveTo(154.3,76.2,155.4,95.6).lineTo(155.9,101.4).lineTo(155.8,103.5).lineTo(154.6,128.7).lineTo(154.5,132.7).lineTo(154.3,136.9).lineTo(155.1,138.7).lineTo(155.1,210.2).curveTo(156.8,215.3,156.8,217.5).curveTo(156.8,219.7,156.4,220.6).curveTo(155.8,222,154.3,222).lineTo(153.9,222).closePath().moveTo(-151.7,-214).lineTo(-151.8,-210.9).curveTo(-151.8,-203.4,-151.2,-189.6).lineTo(-151.2,-176.1).lineTo(-151.2,-146.9).lineTo(-151.2,-146.5).lineTo(-151,-141.5).lineTo(-150.7,-126.8).lineTo(-150.6,-121.8).curveTo(-150.4,-107.8,-150.4,-89.4).lineTo(-150.4,-55.2).lineTo(-150.4,-53.8).lineTo(-150.4,-47.2).curveTo(-150.3,-31.1,-149.6,3.6).curveTo(-149.6,4.9,-149.9,5.5).lineTo(-150.4,6.4).curveTo(-146.1,9,-135.2,17.2).curveTo(-123.5,25.9,-118,29.4).curveTo(-113,32.9,-95,44.8).curveTo(-78,56.1,-71.6,61.3).curveTo(-58.2,72.2,-32.6,89).curveTo(-18.3,98.5,9.5,116.3).curveTo(17.5,121.6,44.8,140.4).curveTo(69.2,157.1,84,166.6).curveTo(97.5,175.4,119.4,191.2).curveTo(140,206,150.8,214.6).curveTo(150.5,212.4,150.2,211.8).lineTo(150.2,140.3).lineTo(149.7,130).lineTo(150,125.1).lineTo(151,103).lineTo(150.8,100.5).lineTo(150.5,92.8).curveTo(150.1,74.4,151.1,31.6).lineTo(151.1,21.9).lineTo(151.1,17.2).lineTo(151.1,-10).lineTo(151.1,-10.9).curveTo(151.3,-12.9,151.3,-17.5).curveTo(151.3,-26.3,150.9,-44.5).curveTo(150.2,-76.4,150.2,-80.3).lineTo(150.1,-80.8).lineTo(149.8,-86.7).curveTo(149.5,-92.5,149.8,-103.8).curveTo(150.2,-116,150.9,-120.9).curveTo(151.4,-124.2,151.2,-124.9).curveTo(151.2,-125.2,151.9,-127.4).curveTo(152.5,-129.1,151.4,-129.1).curveTo(43.8,-159.2,-1.3,-174).lineTo(-8.8,-176.1).lineTo(-82,-196.7).curveTo(-112.9,-205.5,-131.2,-210.9).lineTo(-141.8,-214).lineTo(-151.5,-217).lineTo(-151.7,-214).closePath().moveTo(137,186.1).curveTo(136.3,186.9,135.1,186.9).curveTo(134.5,186.9,104.5,167.6).curveTo(72.7,147.2,66.1,143.8).curveTo(57.1,139.3,27.3,118.9).lineTo(-7,95.1).curveTo(-51.4,65.6,-83.6,43.8).curveTo(-145.5,1.8,-145.5,-2.6).lineTo(-145.4,-4.4).lineTo(-145.4,-4.4).curveTo(-147,-6.3,-146.6,-15.6).curveTo(-146.1,-26.4,-146.3,-27.5).lineTo(-146.3,-79.5).lineTo(-145.5,-80.7).lineTo(-146.1,-176.1).lineTo(-146.3,-205.3).curveTo(-145.8,-205.9,-145.4,-206.2).curveTo(-145.2,-207.8,-136.3,-205.2).curveTo(-124.4,-201.6,-110.9,-196.6).curveTo(-108.4,-195.4,-58.6,-180.9).lineTo(-42,-176.1).curveTo(-3.9,-164.9,5.2,-161.8).curveTo(26.7,-154.3,82.6,-136.8).curveTo(133.7,-120.8,138.8,-118.8).lineTo(138.5,-117.8).curveTo(138.8,-116.9,138.8,-115.4).curveTo(138.8,-109.3,138.4,-96.4).lineTo(138.2,-90.7).lineTo(138.1,-85).lineTo(138,-77.9).curveTo(138,-68,138.8,-22.7).lineTo(138.9,-21.3).lineTo(138.9,-16.1).lineTo(139.2,10.9).lineTo(139.3,12.1).lineTo(139.3,16.5).lineTo(139.7,45.8).lineTo(139.6,172).lineTo(139.6,180.1).curveTo(139.2,186.9,137.2,186.9).lineTo(137,186.1).closePath().moveTo(-3.6,91.9).curveTo(14.1,103.9,35.1,118.5).curveTo(51.5,129.6,72.4,141.5).curveTo(87.9,150.5,111.1,165.8).curveTo(127.8,176.9,135.2,182.9).curveTo(135,181.5,135.1,165.9).curveTo(135.2,149.6,133.9,133.9).lineTo(133.9,120.4).lineTo(133.9,114.4).lineTo(133.9,90.1).lineTo(133.9,83.4).lineTo(133.9,13.9).lineTo(133.9,8.2).lineTo(133.9,2.9).lineTo(133.4,-18.5).lineTo(133.3,-25).curveTo(132.3,-72.8,132.3,-78.4).lineTo(132.3,-87).lineTo(132.3,-92.8).lineTo(132.3,-101.3).curveTo(132.3,-105.2,132.7,-111).lineTo(133,-116.1).curveTo(130.9,-116.1,116,-120.8).curveTo(109,-122.9,72.8,-134.5).lineTo(-16.8,-163).lineTo(-21.1,-164.4).lineTo(-48.3,-173).curveTo(-51.6,-173.5,-61,-176.1).curveTo(-71.5,-179,-89.9,-184.6).curveTo(-121,-194.1,-129.4,-197.2).lineTo(-136.4,-199.7).curveTo(-139.6,-201,-141.4,-202.1).lineTo(-141.4,-187.4).lineTo(-141.4,-183).lineTo(-141.4,-162.4).curveTo(-141.3,-162.1,-141.1,-142.1).lineTo(-141.1,-136.5).lineTo(-141,-122.3).lineTo(-140.9,-120.6).lineTo(-140.9,-116.8).lineTo(-140.6,-79.3).curveTo(-141,-75.1,-141.2,-69).lineTo(-141.3,-65.2).lineTo(-141.4,-53.3).lineTo(-141.4,-49.1).lineTo(-141.3,-41.9).curveTo(-140.7,-7.7,-140.8,-7).lineTo(-140.9,-4.8).curveTo(-140.9,-3.7,-141.2,-3.2).curveTo(-136.2,-1,-129.4,4.1).curveTo(-123.7,8.3,-110.7,18.7).curveTo(-103.9,24.5,-92.9,31.3).curveTo(-78.5,40.2,-73.6,43.6).curveTo(-46,63.1,-22.9,78.9).curveTo(-22.6,80.9,-22.7,79.2).lineTo(-21.2,80.7).lineTo(-21.1,80.7).lineTo(-21.1,80.7).lineTo(-20.9,80.9).lineTo(-21.1,80.7).lineTo(-21.2,80.5).lineTo(-21.6,79.8).curveTo(-22.6,77.5,-21.1,80.6).lineTo(-20.8,80.3).lineTo(-20.3,80.6).curveTo(-19.3,83.3,-18.5,81.8).lineTo(-3.6,91.9).closePath();
	this.shape_4.setTransform(700.3,292.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.beginFill("#3D3510").beginStroke().moveTo(133.5,151.4).lineTo(95.7,128.4).curveTo(79,119.7,53.4,105.2).curveTo(26.1,89.6,13.1,81.3).curveTo(0.3,73.2,-12.6,65.1).curveTo(-12.7,78,-13.1,96.2).lineTo(-12.9,104.6).curveTo(-13.7,113.4,-14.7,115.3).curveTo(-15.6,116.7,-16.5,114.1).lineTo(-17,113.7).lineTo(-17.3,114.1).curveTo(-18.8,110.9,-17.8,113.3).lineTo(-17.4,114).lineTo(-17.3,114.2).lineTo(-17.1,114.4).lineTo(-17.3,114.2).lineTo(-17.3,114.2).lineTo(-17.4,114.1).lineTo(-18.9,112.7).curveTo(-18.9,114.4,-19.1,112.4).lineTo(-19.2,111.5).curveTo(-19.5,108.2,-18.8,103.2).curveTo(-18,98.2,-18,96.7).curveTo(-18.3,75.3,-18.3,61.5).curveTo(-42.6,46.6,-67.5,32).curveTo(-89,19.6,-137.5,-8.5).lineTo(-137.6,-15.7).curveTo(-117.9,-4.8,-71.5,24.9).curveTo(-65.7,28.7,-30.9,48.9).lineTo(-18.3,56.3).curveTo(-18.3,42,-18,38.4).lineTo(-18,36.3).curveTo(-37.7,25.3,-59.1,13.2).curveTo(-103.1,-11.6,-137.5,-31.7).lineTo(-137.4,-35.5).lineTo(-137.3,-35.5).curveTo(-132.3,-34,-64.3,4).lineTo(-18,30.1).lineTo(-18,-3.2).curveTo(-17.6,-7.3,-17.8,-25.6).lineTo(-23.6,-28.4).curveTo(-102.1,-65.5,-137.1,-83.3).lineTo(-137.1,-87.1).lineTo(-137.2,-88.8).lineTo(-131.6,-86.1).curveTo(-113.6,-77.5,-89.2,-65).curveTo(-77.5,-59.2,-17.9,-31.1).lineTo(-18,-40.7).lineTo(-18.3,-53.5).curveTo(-28.6,-57.8,-42.3,-63.3).lineTo(-86.2,-80.8).curveTo(-120.3,-95.1,-137.3,-103.1).lineTo(-137.4,-108.6).lineTo(-128.6,-104.8).lineTo(-88.2,-86.9).curveTo(-70.4,-80.5,-43.1,-69.3).curveTo(-29,-63.6,-18.5,-59.1).lineTo(-18.8,-75).lineTo(-18.8,-106.1).curveTo(-32.1,-110.6,-37.6,-112.7).curveTo(-51.5,-118.1,-110.9,-138.6).curveTo(-117,-140.8,-121.9,-142.6).curveTo(-132.8,-146.7,-137.7,-149.5).lineTo(-137.7,-153.9).curveTo(-130.6,-152.9,-111.2,-146.2).lineTo(-101.2,-142.6).curveTo(-75.4,-133.3,-38.1,-118.1).lineTo(-18.8,-111.4).lineTo(-18.8,-112).curveTo(-18.8,-124.2,-17.8,-130.9).lineTo(-17.3,-130.9).lineTo(-13,-129.5).curveTo(-11.2,-127.8,-13.6,-112.2).lineTo(-13.7,-109.7).curveTo(11.6,-101.3,58.3,-85.9).curveTo(93.9,-74,136,-59.3).lineTo(136,-53.5).curveTo(95.5,-67.5,57.1,-81.1).curveTo(42.9,-86.1,5.3,-98.2).lineTo(-13.7,-104.4).lineTo(-13.8,-74.6).lineTo(-13.5,-57).curveTo(-5.6,-53.6,-0,-51).curveTo(42.3,-31.1,86.4,-12.7).lineTo(137,8.4).lineTo(137.2,14.9).curveTo(118.5,6.7,91.2,-6.1).curveTo(85,-9,47.8,-24.7).curveTo(12.2,-39.6,2,-44.5).curveTo(-3.9,-47.3,-13.4,-51.4).lineTo(-13,-34.3).lineTo(-12.9,-28.7).lineTo(-9.8,-27.3).lineTo(88.7,19.1).curveTo(101.2,25,131.8,38.9).lineTo(137.7,41.7).lineTo(137.7,47.4).curveTo(50.3,6.6,-12.8,-23.2).curveTo(-12.5,-6,-13.1,-1.6).lineTo(-13.1,32.8).lineTo(27.2,55.7).curveTo(40.3,63.1,75.1,81.7).curveTo(106.1,98.2,114.9,103.6).curveTo(121.1,107.4,137.7,116.8).lineTo(137.7,123.6).lineTo(114.4,109).curveTo(97.8,99.5,25.2,60.2).curveTo(7,50.2,-13.1,39).lineTo(-13.1,40.6).curveTo(-12.7,47,-12.6,59.7).curveTo(5.4,70.3,11.9,74.6).curveTo(24.1,82.7,53.3,99.1).lineTo(94.6,122).curveTo(122.1,138.1,137.7,147.9).lineTo(137.7,153.9).lineTo(133.5,151.4).closePath();
	this.shape_5.setTransform(696.6,259.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.beginFill("#000000").beginStroke().moveTo(94.6,132.4).lineTo(53.3,109.5).curveTo(24.1,93.1,11.9,85).curveTo(5.4,80.7,-12.6,70.1).curveTo(-12.7,57.4,-13.1,51).lineTo(-13.1,49.4).curveTo(7,60.6,25.2,70.6).curveTo(97.8,109.9,114.4,119.4).lineTo(137.7,134).lineTo(137.7,158.3).curveTo(122.1,148.5,94.6,132.4).closePath().moveTo(-30.9,59.3).curveTo(-65.7,39.1,-71.5,35.3).curveTo(-117.9,5.6,-137.6,-5.3).lineTo(-137.7,-9.4).lineTo(-137.5,-21.3).curveTo(-103.1,-1.2,-59.1,23.6).curveTo(-37.7,35.7,-18,46.7).lineTo(-18,48.8).curveTo(-18.3,52.4,-18.3,66.7).lineTo(-30.9,59.3).closePath().moveTo(131.8,49.3).curveTo(101.2,35.4,88.7,29.5).lineTo(-9.8,-16.9).lineTo(-12.9,-18.3).lineTo(-13,-23.9).lineTo(-13.4,-41).curveTo(-3.9,-36.9,2,-34.1).curveTo(12.2,-29.2,47.8,-14.3).curveTo(85,1.4,91.2,4.3).curveTo(118.5,17.1,137.2,25.3).lineTo(137.7,46.8).lineTo(137.7,52.1).lineTo(131.8,49.3).closePath().moveTo(-89.2,-54.6).curveTo(-113.6,-67.1,-131.6,-75.8).lineTo(-137.2,-78.4).lineTo(-137.3,-92.7).curveTo(-120.3,-84.7,-86.2,-70.4).lineTo(-42.3,-52.9).curveTo(-28.6,-47.4,-18.3,-43.1).lineTo(-18,-30.3).lineTo(-17.9,-20.7).curveTo(-77.5,-48.8,-89.2,-54.6).closePath().moveTo(58.3,-75.5).curveTo(11.6,-90.9,-13.7,-99.3).lineTo(-13.6,-101.8).curveTo(-11.2,-117.4,-13,-119.1).lineTo(76.5,-90.6).curveTo(112.7,-79,119.8,-76.9).curveTo(134.7,-72.2,136.8,-72.2).lineTo(136.5,-67.1).curveTo(136,-61.3,136,-57.4).lineTo(136,-48.9).curveTo(93.9,-63.6,58.3,-75.5).closePath().moveTo(-38.1,-107.7).curveTo(-75.4,-122.9,-101.2,-132.2).lineTo(-111.2,-135.8).curveTo(-130.6,-142.5,-137.7,-143.5).lineTo(-137.7,-158.3).curveTo(-135.9,-157.1,-132.6,-155.8).lineTo(-125.7,-153.3).curveTo(-117.2,-150.2,-86.2,-140.7).curveTo(-67.7,-135.1,-57.2,-132.2).curveTo(-47.8,-129.6,-44.6,-129.1).lineTo(-17.3,-120.5).lineTo(-17.8,-120.5).curveTo(-18.8,-113.8,-18.8,-101.6).lineTo(-18.8,-101).lineTo(-38.1,-107.7).closePath();
	this.shape_6.setTransform(696.6,249);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.beginFill("#D67535").beginStroke().moveTo(24.8,35.3).curveTo(-19.3,16.9,-61.6,-3).curveTo(-67.2,-5.6,-75.1,-9).lineTo(-75.4,-26.6).lineTo(-75.3,-56.4).lineTo(-56.3,-50.2).curveTo(-18.7,-38.1,-4.5,-33.1).curveTo(33.9,-19.5,74.4,-5.5).lineTo(74.4,3.1).curveTo(74.4,8.6,75.4,56.4).lineTo(24.8,35.3).closePath();
	this.shape_7.setTransform(758.2,211.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.beginFill("#E5B700").beginStroke().moveTo(13.3,30.7).curveTo(-54.8,-7.4,-59.7,-8.9).lineTo(-59.9,-8.9).curveTo(-59.7,-15.1,-59.3,-19.2).lineTo(-59.6,-56.7).curveTo(-24.5,-38.9,53.9,-1.8).lineTo(59.8,1).curveTo(60,19.3,59.6,23.4).lineTo(59.6,56.7).lineTo(13.3,30.7).closePath();
	this.shape_8.setTransform(619,232.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.beginFill("#527BCE").beginStroke().moveTo(52.6,56.8).curveTo(43.8,51.5,12.8,34.9).curveTo(-22,16.4,-35.1,8.9).lineTo(-75.4,-14).lineTo(-75.4,-48.4).curveTo(-74.8,-52.8,-75.1,-70).curveTo(-12,-40.2,75.4,0.6).lineTo(75.4,70).curveTo(58.8,60.6,52.6,56.8).closePath();
	this.shape_9.setTransform(758.9,306.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.beginFill("#834F9E").beginStroke().moveTo(52.8,58.6).curveTo(29.6,43.2,14.1,34.3).curveTo(-6.8,22.3,-23.2,11.2).curveTo(-44.3,-3.4,-62,-15.3).lineTo(-76.8,-25.5).curveTo(-75.8,-27.3,-75,-36.2).lineTo(-75.2,-44.6).curveTo(-74.8,-62.8,-74.7,-75.6).curveTo(-61.8,-67.6,-49,-59.4).curveTo(-36,-51.1,-8.7,-35.6).curveTo(16.9,-21.1,33.6,-12.3).lineTo(71.4,10.6).lineTo(75.6,13.1).lineTo(75.6,26.6).curveTo(76.9,42.3,76.8,58.7).curveTo(76.7,74.2,76.8,75.7).curveTo(69.5,69.6,52.8,58.6).closePath();
	this.shape_10.setTransform(758.7,400.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.beginFill("#4B8471").beginStroke().moveTo(35,35).curveTo(7.6,23.9,-10.1,17.5).lineTo(-50.5,-0.5).lineTo(-59.3,-4.3).curveTo(-59.5,-24.3,-59.6,-24.6).lineTo(-59.6,-45.2).curveTo(-54.7,-42.4,-43.9,-38.3).curveTo(-39,-36.4,-32.9,-34.3).curveTo(26.5,-13.7,40.5,-8.4).curveTo(45.9,-6.2,59.3,-1.8).lineTo(59.3,29.3).lineTo(59.6,45.2).curveTo(49.1,40.7,35,35).closePath();
	this.shape_11.setTransform(618.5,155.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.beginFill("#A38E36").beginStroke().moveTo(119.2,192.4).curveTo(97.3,176.6,83.8,167.8).curveTo(69,158.3,44.6,141.6).curveTo(17.3,122.8,9.3,117.5).curveTo(-18.5,99.7,-32.8,90.2).curveTo(-58.4,73.4,-71.8,62.5).curveTo(-78.2,57.3,-95.2,46).curveTo(-113.2,34.1,-118.2,30.6).curveTo(-123.7,27.1,-135.4,18.4).curveTo(-146.3,10.2,-150.6,7.6).lineTo(-150.1,6.7).curveTo(-149.8,6.1,-149.8,4.8).curveTo(-150.4,-29.9,-150.6,-46).lineTo(-150.6,-52.6).lineTo(-150.6,-54).lineTo(-150.6,-88.2).curveTo(-150.6,-106.6,-150.8,-120.6).lineTo(-150.9,-125.6).lineTo(-151.2,-140.3).lineTo(-151.4,-145.3).lineTo(-151.4,-145.7).lineTo(-151.4,-174.9).lineTo(-151.4,-188.4).curveTo(-152,-202.2,-151.9,-209.7).lineTo(-151.9,-212.8).lineTo(-151.7,-215.8).lineTo(-141.9,-212.8).lineTo(-131.3,-209.7).curveTo(-113,-204.3,-82.2,-195.5).lineTo(-9,-174.9).lineTo(-1.5,-172.8).curveTo(43.6,-158,151.2,-127.9).curveTo(152.3,-127.9,151.7,-126.2).curveTo(151,-124,151,-123.7).curveTo(151.2,-123,150.7,-119.7).curveTo(150,-114.8,149.6,-102.6).curveTo(149.3,-91.3,149.6,-85.5).lineTo(150,-79.6).lineTo(150,-79.1).curveTo(150,-75.2,150.7,-43.3).curveTo(151.1,-25.1,151.1,-16.3).curveTo(151.1,-11.7,151,-9.7).lineTo(150.9,-8.8).lineTo(150.9,18.4).lineTo(150.9,23.1).lineTo(150.9,32.8).curveTo(149.9,75.6,150.3,94).lineTo(150.6,101.7).lineTo(150.8,104.2).lineTo(149.8,126.3).lineTo(149.6,131.2).lineTo(150,141.5).lineTo(150,213).curveTo(150.3,213.5,150.6,215.8).curveTo(139.8,207.2,119.2,192.4).closePath().moveTo(137,188.1).curveTo(139,188.1,139.4,181.3).lineTo(139.4,173.2).lineTo(139.5,47).lineTo(139.2,17.6).lineTo(139.1,13.3).lineTo(139.1,12.1).lineTo(138.7,-14.9).lineTo(138.7,-20.1).lineTo(138.7,-21.5).curveTo(137.8,-66.8,137.8,-76.7).lineTo(137.9,-83.8).lineTo(138,-89.6).lineTo(138.2,-95.2).curveTo(138.6,-108.1,138.6,-114.2).curveTo(138.6,-115.7,138.4,-116.6).lineTo(138.6,-117.6).curveTo(133.5,-119.6,82.4,-135.6).curveTo(26.5,-153.1,5,-160.6).curveTo(-4.1,-163.7,-42.2,-174.9).lineTo(-58.8,-179.7).curveTo(-108.6,-194.2,-111.1,-195.4).curveTo(-124.6,-200.4,-136.5,-204).curveTo(-145.3,-206.6,-145.5,-205.1).curveTo(-146,-204.7,-146.5,-204.1).lineTo(-146.3,-174.9).lineTo(-145.7,-79.5).lineTo(-146.5,-78.3).lineTo(-146.5,-26.3).curveTo(-146.3,-25.2,-146.8,-14.4).curveTo(-147.2,-5.1,-145.6,-3.2).lineTo(-145.6,-3.2).lineTo(-145.7,-1.4).curveTo(-145.7,3,-83.8,45).curveTo(-51.6,66.8,-7.2,96.3).lineTo(27.1,120.1).curveTo(56.9,140.5,65.9,145).curveTo(72.5,148.4,104.3,168.8).curveTo(134.3,188.1,134.9,188.1).curveTo(136.2,188.1,136.8,187.3).lineTo(137,188.1).closePath();
	this.shape_12.setTransform(700.5,291.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.beginFill("#FF0000").beginStroke().moveTo(-0.3,0.1).lineTo(0,-0.1).lineTo(0.3,-0).lineTo(-0.2,0.1).lineTo(-0.3,0.1).closePath();
	this.shape_13.setTransform(548.1,69);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_BGBox}]}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.mc_BtnFinnance},{t:this.mc_Destination1},{t:this.mc_BtnIT},{t:this.mc_Destination2},{t:this.mc_BtnMarketing},{t:this.mc_BtnCommercial},{t:this.mc_BtnSupply},{t:this.mc_Destination4},{t:this.mc_BtnHR},{t:this.mc_Destination3},{t:this.mc_Destination5},{t:this.mc_Destination6}]},28).wait(44));

	// BG
	this.mc_Highlighter = new lib.Highlighter();
	this.mc_Highlighter.setTransform(848.3,37.8,1,1,0,0,0,24.8,24.8);

	this.mc_QuitGame_2 = new lib.Arrow_Butn();
	this.mc_QuitGame_2.setTransform(114.9,35.4,0.712,0.712,0,0,0,34.9,34.9);

	this.mc_QuitGame_3 = new lib.Arrow_Butncopy();
	this.mc_QuitGame_3.setTransform(916.3,37.4,0.712,0.712,0,0,180,34.9,34.9);

	this.mc_EnterGame = new lib.Arrow_Butncopy2();
	this.mc_EnterGame.setTransform(847,37.4,0.712,0.712,0,0,180,34.9,34.9);

	this.mc_GameMenu_3 = new lib.Options_btn();
	this.mc_GameMenu_3.setTransform(45,35.4,0.712,0.712,0,0,0,34.9,34.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.beginLinearGradientFill(["#4F3F5B","#4F2A40"],[0,1],0,-35,0,35.1).beginStroke().moveTo(-485,37.5).lineTo(-485,-37.5).lineTo(485,-37.5).lineTo(485,37.5).closePath();
	this.shape_14.setTransform(480.5,36.3);

	this.instance_2 = new lib.MailSack03();
	this.instance_2.setTransform(220.1,281.1,1,1,0,0,0,63.1,98.5);

	this.instance_3 = new lib.mailSack01();
	this.instance_3.setTransform(97.6,248.8,1,1,0,0,0,85.5,119.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.beginFill().beginStroke("#000000").setStrokeStyle(5,1,1).moveTo(11.7,-285.5).lineTo(66.6,-285.5).lineTo(552.2,-285.5).lineTo(557.5,-285.5).lineTo(557.5,268.5).lineTo(574.5,268.5).moveTo(11.7,-285.5).lineTo(-180,-285.5).lineTo(-183.8,-285.5).lineTo(-230.7,-285.5).lineTo(-273.5,-285.5).lineTo(-483.2,-285.5).lineTo(-489.5,-285.5).lineTo(-510.7,-285.5).lineTo(-564.5,-285.5).lineTo(-569.5,-285.5).moveTo(11.7,60.5).lineTo(11.7,-192.5).lineTo(11.7,-195.6).lineTo(11.7,-285.5).moveTo(247.9,268.6).lineTo(15.4,63.8).lineTo(11.7,60.5).lineTo(-564.5,60.5).lineTo(-564.5,285.5).lineTo(263.5,285.5).lineTo(247.9,268.6).lineTo(557.5,268.5).lineTo(557.5,276.5).moveTo(-564.5,-285.5).lineTo(-564.5,-204.3).lineTo(-564.5,60.5).moveTo(-564.5,285.5).lineTo(-574.5,285.5);
	this.shape_15.setTransform(459.7,274.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.beginFill("#B7B7B7").beginStroke().moveTo(-414,112.5).lineTo(-414,-112.5).lineTo(162.2,-112.5).lineTo(165.9,-109.2).lineTo(398.4,95.6).lineTo(414,112.5).closePath();
	this.shape_16.setTransform(309.2,447.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.beginFill("#80AAAA").beginStroke().moveTo(-269.1,72.2).lineTo(-272.9,68.9).lineTo(-272.9,-184).lineTo(-272.9,-187.2).lineTo(-272.9,-277).lineTo(-217.9,-277).lineTo(267.6,-277).lineTo(272.9,-277).lineTo(272.9,277).lineTo(-36.7,277).closePath();
	this.shape_17.setTransform(744.3,266);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.beginFill("#99CCCC").beginStroke().moveTo(-288.1,173).lineTo(-288.1,-91.8).lineTo(-288.1,-173).lineTo(-234.3,-173).lineTo(-213.1,-173).lineTo(-206.8,-173).lineTo(2.9,-173).lineTo(45.7,-173).lineTo(92.6,-173).lineTo(96.4,-173).lineTo(288.1,-173).lineTo(288.1,-83.1).lineTo(288.1,-80).lineTo(288.1,173).closePath();
	this.shape_18.setTransform(183.3,162);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.mc_GameMenu_3},{t:this.mc_EnterGame},{t:this.mc_QuitGame_3},{t:this.mc_QuitGame_2},{t:this.mc_Highlighter}]}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.instance_3},{t:this.instance_2}]},28).wait(44));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(235.3,101.4,1449.4,846.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;