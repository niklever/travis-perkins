(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 960,
	height: 540,
	fps: 25,
	color: "#FFFFFF",
	manifest: [
		{src:"images/Boss_01__000.png", id:"Boss_01__000"},
		{src:"images/Boss_01__001.png", id:"Boss_01__001"},
		{src:"images/Boss_01__002.png", id:"Boss_01__002"},
		{src:"images/Boss_01__003.png", id:"Boss_01__003"},
		{src:"images/Boss_01__004.png", id:"Boss_01__004"},
		{src:"images/Boss_01__005.png", id:"Boss_01__005"},
		{src:"images/Boss_01__006.png", id:"Boss_01__006"},
		{src:"images/Boss_01__007.png", id:"Boss_01__007"},
		{src:"images/Boss_01__008.png", id:"Boss_01__008"},
		{src:"images/Boss_01__009.png", id:"Boss_01__009"},
		{src:"images/Boss_01__010.png", id:"Boss_01__010"},
		{src:"images/Boss_01__011.png", id:"Boss_01__011"},
		{src:"images/Boss_01__012.png", id:"Boss_01__012"},
		{src:"images/Boss_01__013.png", id:"Boss_01__013"},
		{src:"images/Boss_01__014.png", id:"Boss_01__014"},
		{src:"images/Boss_01__015.png", id:"Boss_01__015"},
		{src:"images/Boss_01__016.png", id:"Boss_01__016"},
		{src:"images/Boss_01__017.png", id:"Boss_01__017"},
		{src:"images/Boss_01__018.png", id:"Boss_01__018"},
		{src:"images/Boss_01__019.png", id:"Boss_01__019"},
		{src:"images/Boss_01__020.png", id:"Boss_01__020"},
		{src:"images/Boss_01__021.png", id:"Boss_01__021"},
		{src:"images/Boss_01__022.png", id:"Boss_01__022"},
		{src:"images/Boss_01__023.png", id:"Boss_01__023"},
		{src:"images/Boss_01__024.png", id:"Boss_01__024"},
		{src:"images/Boss_01__025.png", id:"Boss_01__025"},
		{src:"images/Boss_01__026.png", id:"Boss_01__026"},
		{src:"images/Health_and_Safety_Officer__000.png", id:"Health_and_Safety_Officer__000"}
	]
};



// symbols:



(lib.Boss_01__000 = function() {
	this.initialize(img.Boss_01__000);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__001 = function() {
	this.initialize(img.Boss_01__001);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__002 = function() {
	this.initialize(img.Boss_01__002);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__003 = function() {
	this.initialize(img.Boss_01__003);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__004 = function() {
	this.initialize(img.Boss_01__004);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__005 = function() {
	this.initialize(img.Boss_01__005);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__006 = function() {
	this.initialize(img.Boss_01__006);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__007 = function() {
	this.initialize(img.Boss_01__007);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__008 = function() {
	this.initialize(img.Boss_01__008);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__009 = function() {
	this.initialize(img.Boss_01__009);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__010 = function() {
	this.initialize(img.Boss_01__010);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__011 = function() {
	this.initialize(img.Boss_01__011);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__012 = function() {
	this.initialize(img.Boss_01__012);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__013 = function() {
	this.initialize(img.Boss_01__013);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__014 = function() {
	this.initialize(img.Boss_01__014);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__015 = function() {
	this.initialize(img.Boss_01__015);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__016 = function() {
	this.initialize(img.Boss_01__016);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__017 = function() {
	this.initialize(img.Boss_01__017);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__018 = function() {
	this.initialize(img.Boss_01__018);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__019 = function() {
	this.initialize(img.Boss_01__019);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__020 = function() {
	this.initialize(img.Boss_01__020);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__021 = function() {
	this.initialize(img.Boss_01__021);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__022 = function() {
	this.initialize(img.Boss_01__022);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__023 = function() {
	this.initialize(img.Boss_01__023);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__024 = function() {
	this.initialize(img.Boss_01__024);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__025 = function() {
	this.initialize(img.Boss_01__025);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Boss_01__026 = function() {
	this.initialize(img.Boss_01__026);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,472);


(lib.Health_and_Safety_Officer__000 = function() {
	this.initialize(img.Health_and_Safety_Officer__000);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,369);


(lib.Yes_Btn = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("yes", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 112;
	this.text.setTransform(79.2,13.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AqjkbIVHAAQCOAAAACNIAAEdQAACNiOAAI1HAAQiOAAAAiNIAAkdQAAiNCOAAg");
	this.shape.setTransform(81.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("AqjEcQiOAAAAiNIAAkdQAAiNCOAAIVHAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_1.setTransform(81.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AqjEcQiOAAAAiNIAAkdQAAiNCOAAIVHAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_2.setTransform(90.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.YardSteve03 = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#826631").s().p("ApaA9QgCgngQgLIARAAQAKAKAUAlQAPAWAPAFQgJALgMAAQgkAAgCgjgAqmBdQgPAAgNgJIgGgLQgOgSAFgaQAEgRgFgGIALAAQAGAhARAXQAMARARAJQgHAFgKAAIgCAAgAsgBEQghgWAJghQAIgcAggfIALgOIAJgMQgFAHAgAGQgbAggIAMQgNAWAAAhQAAAMAIAPIAFAHIgHABQgLAAgKgHgAnkA0QAEgPgBgCIgBgKQAOAOAHAEQAKAFAUACIgIADQgKADgLAAQgMAAgMgEgALhAPQgIgEgHgPIgKgWIgHgJQgDgDgCgBIALgCQAIAAALAHQAMAIAGAMQAKAQAOAGQgDAGgEACQgHADgHAAQgHAAgHgEgAMXhRIgBAAQgEgIgMgEIgCAAQAFgDAEABQAHAAAPAIQAKAGAOACQgIAHgIAAQgJAAgLgJg");
	this.shape.setTransform(81.9,88.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A88541").s().p("ArIB1QgQgXgGghIgLAAQgCgDgFAAQgNAAgGAEIgHADQgMAkAMAWQgKAFgIABIgEgHQgJgPABgMQAAghANgYQAHgMAbgeQATADAhABQBcAGCtAAIAAACQAFALAJAOQAHAMgBAUQABAIgJAOQgHALgPAGQgUgBgKgFQgIgEgOgOIgCgEIgDgFQgDgEgGgCIgDAAIgEAAQgMAAgFAEQgDABgCACQAAAUgFAWQgFARgHAJQgPgFgPgWQgVglgJgKIgSAAIgCgBQgQAIgGAZQgIAjgDAEIgEAGQgQgJgNgRgAKPBEQgFgkgKggQgHgbgCgZQgFgNABgJQAAgBAngUQAvgYAegWQABAAAAAAQABgBAAAAQABAAAAABQAAAAABAAIAEAGQAgAfAbA8IgEAQIgCAEQgOgCgKgGQgPgIgHAAQgFgBgEACIgDABIgDACQgEADgDAAIgBADIgBAAQASAuAAAZQAAAPgDAHQgOgGgKgSQgHgMgMgIQgKgHgIAAIgLACIgIABQgHABACAEQAJAYgCAKQAAAMgQADg");
	this.shape_1.setTransform(84.2,82.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ApkHhQgJgGgKgMIgJAKQgHAGgHABQggAFgXgGQgHgCgagLQgNACgQAAQggACgSgKQgxgYAAhMQAAghA3g7QA4g7AAA7QAAAEgIAKIgBgDIgJAMIgLAOQggAfgIAeQgIAhAgAWQANAIAPgDQAIgBAJgEQgLgWALgkIAHgEQAHgEANAAQAEAAACADQAFAGgEARQgFAaAOATIAGAKQANAKAPAAQAMAAAHgGIAEgFQADgFAIgiQAFgZAQgJIADACQAQAKACAnQACAkAlAAQAMAAAIgMQAHgJAFgRQAGgWAAgTQABgCADgCQAGgEALAAIAFAAIACABQAGABAEAFIADAEIACAFIACAJQAAADgDAOQAWAIAWgHIAIgDQAPgHAHgLQAJgOAAgIQAAgTgHgNQgJgNgEgMIgBgCQgDgJACgFIAEgNQAmgDAPAoQAHAVAAAdQAAAegNAQQgOARgqANIglAFQgTAAgMgGIgCAJQgMATgPANQgSAOgSAAQgdAAgTgMgAKOGaQgahHgIgeIgPhDQgJgpAfgeQAfggAtgTQAXgJAOgDQAOgCARACQAYADAQAVQAeAmAKAQQAUAhAAAhQAAAbgOAQQgQATglgBIgEAAQABAPAAAQIgDAWQgDALgEAFQgFAFgLAEQgcAMgZgHQgHAKgMAEQgMAEgMAAQgNAAgMgEgAK4E4QACAAAEAEIAGAJIAKAVQAIASAHAEQANAGAPgGQAEgCADgGQADgHAAgOQABgZgSgwIABAAIABgEQACAAAFgDIADgBIACgBIADAAQALAEAEAIIABAAQAUARAQgPIADgDIADgQQgbg8ggggIgEgFQAAgBgBAAQAAAAAAAAQgBAAAAAAQgBAAAAABQgfAWgvAYQgmATAAABQgCAKAFANQADAZAGAaQAKAiAGAlIAAAAQAPgDABgMQABgJgIgZQgCgEAGgBIAGAAIACAAgAg9leQgzgPgbgVQgTgRgDgRQgDgVACgLQAFgUASgKQASgJAsgBIBTABQBVAAA3AXQA4AXgJAjQgEASgKAJQgKAKgeAPQgwAYgwABIgEAAQgnAAg9gRg");
	this.shape_2.setTransform(82.3,53);

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF9933").s().p("AnoGyIgagEIAChZIAVAAIAAACQAagDAXABIAIAAQALAAAFgEIAEgEIAAgHQACgEAAgDIgBgCQACgxAIhdIEvgTQAJBhgDA9QgBAeAAAlQgCAYgKAIQgKAJgvAFQguAFhAACgAFuEzQgEgagKhWIgKhAICYgNQAlBRAFAZQALAxANBZQgpAghAAOIg5AGQgTgkgNhHgAmFg5IgBgcQADgHgKg8QgMghgYglQgkg1gygqQgWgSgSgMIAGgJQBBgzBkgaIALADQBZAdBNBUQAgAkARAxQAMAiAHAwIAUCEIkMAVIACg8gADOllIgUgjQAhAKAcAKIAAABIABgBQAxAVAdAOQAiBZApCPQAHAXAMAiIhrALQgciphPiXg");
	this.shape_3.setTransform(86.9,49.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Am8BEQAEg1ADgtIABgcIEMgXIAJA+QAJA8AEA0IkuATIAEgsgAEhAXIgUhyIgBgJIBrgLQAUAzAiBJIAUAqIiYANIgIgtg");
	this.shape_4.setTransform(91.4,55.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#A4C6C6").s().p("ACJG2QhqgVgKAAIgEAAIAAgWIAAgiIgBAAIAAgiQAiAIBnAQIgCBaIgOgDgAiTCGQgFgJAAgZQAAhFAZhgQAMgvAQgtQAXg/Aeg6QAyhhBAhBIAQAGQguBOgkBnQgOAsgMArQgmCJAAB8IAFBHIgJAAIgOABQgwAAgTghg");
	this.shape_5.setTransform(19.9,62.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C0E8E8").s().p("Am0HwIgYgEIgogJIAChaIAQADQCCAHBkgHQAOgCA3gCQA2gCA0gPQA0gPgHhuQgHhvgPiaQgOiZgQhaQgWh4g+g/Qg4g3hLghIEBAAQBrARBVAWQADAMAMAbIAiBDQAwBhACAHIAcBsQAHAdAFAaQAFAeADAaQAGA9AHAzQAJBCALA0QAYB0AoBaIBAgJQBGgMAegLIAbgNQALA3AOAsQg/ALhJAfQhpAuiegCIhfADIgBABIlUALQg/gFiUgdgArHDQIgFhHQAAh9AniIQAMgrAPgsQAlhnAthOIAKAEQBIAfAgA9IAbAxQAWAvgBAZIgBAxQAAAdgDAUIgOB7QgHBJAAA1IAAAogAJHBFIgRgnIgWgtQgXgrgVggQgJgTgGgcIAAgBQghhmgDgPQgNhBgFgQQgEgRgHgPQASAPAQARIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmg");
	this.shape_6.setTransform(85.2,58);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#543126").s().p("AkeB1QhWgTgSgxQgFgQgBgaIgBgmIgEgiQgFgjgGgTQAUAIAXAHQAwAOA2AJQgFC2AcAcQgvgKAFgCgAFeA0QgQgCgXgJQAAAAAAAAQAAAAAAgBQAAAAAAAAQAAgBAAAAIAAgEQAAgTgFgcIgDgPIgCgGQgGgbgIgbIgKgdIgDgIIAEgBIBYgDQAYCtAXAGIgnACIgYgBg");
	this.shape_7.setTransform(71.2,138);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#774736").s().p("AmDCWQgrgDg1gMIgCAAQgcgcAEi2IAJABQBoARB/AAQB+AABGgVIASgGIACAxIgBAVIgCAWIgCgBQhKgwimAMQgxADgmAJIgdAIQgxAOAAAUIADANQAFAMANgGQBMgkBWgEIAcgBQAZAAATAEQAoAEAdAGIAYAHQAWAHATAOQAZASAEA9IAAARQgBAEgFAFgACsA5QgXgGgYiuIADAAQB7gIBogSIABAAQAAAbADANIgDgBQg4gLguAHQgpAHgIARQgCAEAAAFQAAAGAEAEIARAOQAEAAAcgMQAbgMAfAAQB3AAAsAwIAGAHQAWAcAFAwIgUAHg");
	this.shape_8.setTransform(95.3,137.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#C1C16E").s().p("AgEAgQgggIgZgHIgHgBIAAg7ICJAAIgBBXQgwgHgYgFg");
	this.shape_9.setTransform(28.5,88.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#A7A7D1").s().p("AgDADIAHgGIgEAHIgDgBg");
	this.shape_10.setTransform(31,14.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#1A2D3A").s().p("AlOA1IgugMQgJgygOgyIgEgRQAhAKAzALQAqAJApAIIASBpIACAKQg6gKg4gOgAE7AyIgGgQQADgbAAgRIAAgKIBWgBIAAAHQAAANAKAwQgsADgcAAIgVAAg");
	this.shape_11.setTransform(66.6,117.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#2B4B60").s().p("AmnA7IgBgKIgThpQBsATBgAEIEugEQgFAjgKA4IgHABIhGALQhUALhJAAQh7AAhygSgADDggIAAgHIBAgBQBsgPBNgVQgEAUgBAxQhpAYhiAJIgeADQgLgwAAgNg");
	this.shape_12.setTransform(86.9,119.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("Aj5MTQhLgKgtgKQgcgFgqgOQgtgJgegmQgRgWAAg4QgBgPgPhFIgGgdQgNgqgCgSIgGgKQAAgFACgEQgOhdABgaIAAgNIgQgFQgFACgJAAQgPAAgGgVIgCgLIAAgiQAAgdALiYIg/AAQgZgEhHgcQgKgIgFgIQgJgKAAguQAAhKAYhnQAJgmALgkQAZhUAmhKQA2htBIhIIAMgLQB1huChgQIEdgEQEsAwCDBMQA1AfAsAvIBMBYIBxCjQBYBeAABGIACAAIADAFQACADAAALQABAMgPAQQgtAkgOAJQgxAgghAKIAPA+QAHAfAJBCQAGAsAAAiQAAAzgXANIgDgHQgbAThBAaIgMAaQgTArgCAjIAAALQAGAXAAAjIgBALQAvAPAjAbIAKAKIAMAKIgBAAQAlAmAHAzQADAVgEAKQgKASglACIlAAAQgFgCgogDQgsgEgXgFQgOgDgLgIQgKgFAAgEQAAgFACgFQgKgPgLgsIgCgJQgGgfgFgjIgCgTIgPBPIgBASQgBAjgDALQAHAOAFAaQAFAYAAAOQAAAhgCAIQgIAbgcAFgAn1INIAFAjIABAoQABAaAFAQQARAxBXATQgFACAvAKIABAAQA1AMAsADIFOAAQAFgFABgEIAAgRQgEg9gZgSQgTgOgWgHIgYgHQgbgGgogEQgUgDgYAAIgcAAQhWAFhMAjQgNAHgFgNIgDgNQAAgUAxgQIAdgIQAmgJAxgDQCkgLBKAxIABABIADgYIABgVIgCgxIgSAGQhEAWh+AAQh/gBhogQIgJgCQg2gJgvgPQgYgGgTgIQAFATAFAigAHwG1QhnASh7AHIgDAAIhYAEIgEABIADAIIAKAdQAIAaAGAcIACAGIACAPQAGAeAAATIAAADQgBABAAAAQAAABAAAAQAAAAABABQAAAAAAAAQAWAJARACQAVACAqgDIE+AAIAUgHQgFgwgWgeIgGgGQgsgxh3AAQggAAgbAMQgbANgEAAIgRgPQgEgEAAgGQAAgFACgEQAIgRApgHQAugHA4ALIADABQgDgNAAgaIgCAAgAokFGQAOAzAJA0IAuALQA4APA6AKQByASB7AAQBJAABSgMIBHgKIAIgCQAKg5AFgjIkuAEQhggFhsgTQgpgHgqgJQgzgLghgKIAEAQgAE9FrIhAACIhWABIAAAKQAAATgDAbIAGAPQAeACA/gFIAfgDQBigJBpgaQABgwAEgVQhNAVhsAPgApSDDIABABIAAAhIAAAWIAEAAQAJAABrAVIAOADIAoAJIAYAEQCUAdBAAFIFTgLIACgBIBegDQCeACBpguQBJgfBAgLQgPgsgKg3IgbANQgfALhGAMIhAAJQgohagYhyQgLg0gJhCQgHg0gGg+QgCgagGgeQgFgagGgdIgdhsQgCgHgvhhIgjhDQgMgbgCgMQhWgWhqgRIkCAAQBMAhA3A3QA+A/AWB4QAQBaAPCZQAMCcAHBtQAHBugxAPQg0APg3ACQg3ACgNACQhlAHiCgHIgQgDQhogQghgIgApSBuIAHABQAZAHAgAIQAaAGAwAGIAaAEIDWAAQBAgCAugFQAvgFAKgIQAKgJACgXQgBgmACgeQADg6gJhiQgFg0gIg+IgKg+IgUiDQgHgxgMghQgRgxggglQhNhUhagcIgKgEQhkAahBAzIgJAIIACABQATAMAWASQAyAqAjA2QAZAkALAhQALA8gDAIIAAAbIgBA+IgBAcQgCAvgFA1IgEAsQgIBdgDAvIACACQAAADgCAEIgBAIIgDADQgFAEgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAAA9gAEHqJQBQCXAcCpIACAJIAUB0IAHAtIAKBAQAJBWAFAZQANBGATAlIA4gHQBBgOApggQgNhXgLgxQgFgZglhQIgUgrQgihLgUgzQgNgigGgXQgpiPgihZQgdgOgygVIAAABIAAgBQgcgKghgKIATAjgAqeAAIEYAEIAAgmQAAg1AHhJIAOh9QADgUAAgdIABgxQACgZgXgvIgbgxQgfg9hIgfIgKgEIgQgFQhBBBgzBhQgeA5gXA/QgQAtgMAvQgaBiAABFQABAZAFAKQAWAiA7gFgAHZogQAEAQAOBBQACAPAiBmIAAABQAGAcAIATQAWAgAWArIAXAvIAQAnIANAmQASgOAbgOQArgXAhgLQgGg/gzhKQgTgbh1iHIhGhUQgQgRgRgPQAGAPAFARg");
	this.shape_13.setTransform(81.1,78.8);

	this.addChild(this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4.9,0,174.3,157.5);


(lib.WindmillSpin = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B9B9B9").s().p("AgaBDQghgTgkgdIBzh5QARANAUAMQAQAJAXAKIgvCgQgrgQgggTg");
	this.shape.setTransform(34.9,114.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A9A9A9").s().p("AhSBOIAoiiQAcADAOAAQAOAAAdgDIAoCiQgoAHgrAAQgqAAgogHg");
	this.shape_1.setTransform(64.7,120.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#979797").s().p("Ahfg6QAXgKAQgJQAUgMARgNIBzB5QgkAdghATQghAUgqAPg");
	this.shape_2.setTransform(94.5,114.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#868686").s().p("AhlgTQANgRALgUQAJgQAKgXIChAvQgPAqgVAhQgUAkgdAhg");
	this.shape_3.setTransform(114.1,94.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#747474").s().p("AhUArQADgdAAgOQAAgOgDgcICigoQAHAoAAAqQAAArgHAog");
	this.shape_4.setTransform(120.8,64.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#636363").s().p("AhNA5QgLgUgNgRIB3hzQAfAkASAhQATAgARArIihAvQgKgXgJgQg");
	this.shape_5.setTransform(114.1,34.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#5C5C5C").s().p("Ag4BOQgUgMgTgHIAvihQAqAPAhAVQAhASAkAeIhzB4QgRgMgUgMg");
	this.shape_6.setTransform(94.5,15.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#525252").s().p("AAABTQgVAAgVACIgoiiQAogHAqgBQArABAoAHIgoCiQgWgCgVAAg");
	this.shape_7.setTransform(64.7,8.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CBCBCB").s().p("AhCAbQgVghgOgqICggvQAIATALAUQALAUANARIh3BzQgdghgUgkg");
	this.shape_8.setTransform(15.3,94.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#DCDCDC").s().p("AhUAAQAAgqAHgoICiAoQgCAYAAASQAAATACAYIiiAoQgHgoAAgrg");
	this.shape_9.setTransform(8.5,64.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EDEDED").s().p("AhlAxQAQgrATggQASghAfgkIB3BzQgNARgLAUQgLAUgIATg");
	this.shape_10.setTransform(15.3,34.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F6F6F6").s().p("AhfgSQAkgeAhgSQAggUArgQIAvChQgTAHgUAMQgUAMgRAMg");
	this.shape_11.setTransform(34.9,15.4);

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,129.5,129.5);


(lib.WhiteCover = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.502)").s().p("EguPAiiMAAAhFDMBcfAAAMAAABFDg");
	this.shape.setTransform(296.1,221.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,592.2,442.1);


(lib.Wall = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#ECFBFF").s().p("EhMSAtjMAAAhbFMCYkAAAMAAABbFg");
	this.shape.setTransform(488.3,291.6);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,976.6,583.1);


(lib.TW_butn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Aj6CdQAMACANAAQBIAAA4gsQgigBgagUQgagUgKgfQAIACALAAQAMAAAPgDQgjgIgYgcQgXgbAAglIAAgBQAWANAYAAQgVgOgMgWQgNgXAAgbQAAgcAOgYQAnAvA3AdQA4AdA/ADQgDgNAAgKQAAgrAcgeQAfgfAqAAQAtABAeAgQAhgGAhgTQgMAkghAVQAfgEAcgMQgVAfgfAWIABANQAABuhMBWQhUBgiEAAQhWAAhIgvg");
	this.shape.setTransform(19.2,19.2,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#23AADB").s().p("AiMC/QgxAAAAgzIAAkXQAAgzAxAAIEZAAQAxAAAAAzIAAEXQAAAzgxAAg");
	this.shape_1.setTransform(19,19.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.1,0.1,38.2,38.2);


(lib.Trowl = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AhFA2QgIgIgBgDQABgKAIAAIAZAFIAaAEQAiAAAWgXQAOgPAGgRQAUhSABA9QgBAjgUAbQgbAhgxAAg");
	this.shape.setTransform(41.1,9.6);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#56381C").ss(2,1,1).p("AgSgRQAIgGAKAAQALAAAJAHQAIAIAAAIQAAAKgIAHQgJAIgLAAQgIAAgHgFQgDgBgCgCQgHgFgBgI");
	this.shape_1.setTransform(33.9,15.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7F5529").s().p("AgOBqQgGgPgBgLIgKioQgBgYAWgGQAOgEAPgBIACAjIACAnIgEgDQgHgGgBgIQABAIAHAGIAEADIAKCsIgTABIgEAAQgQAAgIgSg");
	this.shape_2.setTransform(29.8,22.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CCCCCC").s().p("AAdH5QgEgQgFgLQgHgSgRghIgEgNIgIgWIgXgoQgNgagGgRQgWg3gVgwIgxhyIgZg1IgwhWQgLgUAGgRIBRgJIAZgCQAhgCAZABIAAAKQgEAdAIALQAIALAbAAICHAAQALgBAIgGQAIgIACgLIgBgcIABgVQAQgEAeAEQAeAGAQgDIgKA6QgGAhgHAVIghBvIgiBvIhDDoQgLAkgLAZIgFAHIgIAIIgKgegAggg+QgFgBgEgFQgDgGABgGQAAgGAFgGQAEgEAGgBIBGgCQAQgSgCgWIgIhKIAWgFQAMAAAKAFQASBBAKBSIgRAEIg3ACgAAEoBQgCgCgBgHIABgEIACgCIAhgEQATgCANgBIAEARIg4AHIgCAAQgGAAgFgCg");
	this.shape_3.setTransform(29.4,59.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#996633").s().p("AgsgwQAHAFAJAAQAMAAAJgIQAHgHAAgKQAAgKgHgIQgJgHgMAAQgLAAgIAGIgCgjQAYgDAVAEQAWAEAIAJQAIAIAEAYIAGA+IAFAzQACAfgDAYQgCANgHAIQgIAKgMAAIg2ABg");
	this.shape_4.setTransform(36.8,22.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#231F20").s().p("AAvJQIgDgGQgZg0gthrIgjhMIgihNQgEgLgLgUIg7iPIgEgNQgDgHgDgFQgagpgNgYIgJgQQgJgUgZgiIgGgMIgOgbQgCgGADgIQACgEADgCIAGgEIAJgDIBWgTIC4gMIAWgHIgGgpIgZgIQgRgJgJgLQgMgNgBgTIgEgfIgDggIgBglIgBgkQgBgKAAgbQACgXgDgOQgCgKAKgRIAJgRQAFgJgCgKQgBgGAGgHQAbgoApgGQAcgEASAKQASAKAJAaQAFAOAGABQAOAEAFAMQAEAHADAQIADAkQAHBXADAtQACAkgBAOQgBAcgNAWQgDAGABAIIAIAyQARAIAZgEIAmgGQAbAOAQAmIghCbQgJAvgSA6IgkBnIg8DFIgOAwQgIApghA7QgEAJgIABIgCAAQgHAAgEgHgAhwhmIgYACIhRAJQgHARALAUIAwBWIAZA0IAyBzQAUAvAWA4QAGARAOAZIAXApIAGAWIAEANQASAhAIASQAEAKAFARIAJAeIAIgIIAFgHQAMgZALgkIBDjpIAihvIAhhsQAGgXAGghIALg7QgQADgfgFQgdgEgRAEIAAAUIAAAcQgBAMgIAHQgIAHgMABIiGgBQgbAAgIgKQgIgLAEgdIgBgKIgPgBQgTAAgYACgABRjhIgWAFIAHBKQADAWgQARIhGACQgGABgFAFQgFAFAAAHQgBAGAEAFQADAGAGAAIBQADIA2gDIASgDQgKhTgThAQgJgFgKAAIgCAAgAAinxQgPABgQAEQgUAGABAYIAKCqQABALAFAPQAIAUAVgCIATgBIA3gCQAMAAAIgJQAIgIABgOQADgXgCgfIgFg2IgGg+QgDgYgIgIQgJgIgYgFQgLgCgMAAIgVACgAAvobIggAEIgCACIgCADQABAIADABQAFADAIgBIA4gGIgFgRQgMAAgUADg");
	this.shape_5.setTransform(28.3,59.9);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,56.7,119.9);


(lib.TP_logoWhite = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#231F20").s().p("AgfAmQgPgQAAgWIAAAAQgBgVAPgQQAQgPAWAAQAaAAAPARIgSATQgLgLgMAAQgJAAgHAIQgIAIAAALIAAAAQAAAMAIAIQAHAIAKAAQAMAAAMgLIAQARQgRATgYAAQgWAAgPgPg");
	this.shape.setTransform(567.8,26.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#231F20").s().p("AgNBGIAAiLIAbAAIAACLg");
	this.shape_1.setTransform(559.8,24.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#231F20").s().p("Ag1BEIAAiEIAdAAIAAAOQANgQASgBQATAAANAOQAPAPAAAYIAAABQAAAXgPAPQgNANgTAAQgSAAgNgPIAAAtgAgQgiQgIAIAAAMIAAABQAAANAIAGQAHAIAJAAQAKAAAHgIQAHgGAAgNIAAgBQAAgMgHgIQgHgIgKAAQgJAAgHAIg");
	this.shape_2.setTransform(551.1,27.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CCB55E").s().p("AivCwQhKhJAAhnQAAhmBKhKQBJhJBmAAQBoAABIBJQBKBKAABmQAABnhKBJQhIBKhoAAQhmAAhJhKg");
	this.shape_3.setTransform(558.2,25.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhQDLQgwgRgngeIAshCQBFA0BDAAQAfAAASgMQARgNAAgVIAAgBQAAgWgdgPQgNgGgygRQg+gRgcgTQgrggAAg2IAAgCQAAg7AsgkQAqgiA/AAQAnAAAqANQApANAhAWIgmBFQhBgog2AAQgaAAgQAMQgQAMAAATIAAABQAAAUAdAPQAPAIAyARQA7ASAeAUQAqAgAAAzIAAACQAABAguAkQgqAhhFAAQgsAAgvgQg");
	this.shape_4.setTransform(502.5,46.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("ABcDYIAAjvQAAgxgXgcQgYgbgrAAQgpAAgaAcQgbAdAAAxIAADtIhhAAIAAmmIBhAAIAABCQAyhLBRAAQBHAAAoAsQAoAtAABKIAAEMg");
	this.shape_5.setTransform(460.4,46.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgvEiIAAmmIBfAAIAAGmgAgzjFIAAhcIBnAAIAABcg");
	this.shape_6.setTransform(425.8,38.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("ABVElIh9i3Ig8A9IAAB6IhhAAIAApJIBhAAIAAFdICui6IB2AAIioCqICuD8g");
	this.shape_7.setTransform(395.5,38.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ah6DYIAAmmIBhAAIAABfQAVgyAigbQAngdA2ADIAABmIgGAAQhCAAglAqQgnAsAABQIAACig");
	this.shape_8.setTransform(356.9,46.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AiNCgQg+g+AAhhIAAgBQAAhbA5hAQA8hBBYAAQBhAAA3BFQAyA+AABfIgBAcIk1AAQAHAzAiAcQAhAcAtAAQAjAAAcgNQAagLAbgaIA4AyQhDBQhqAAQhcAAg9g9gAhIhtQgcAegIAyIDYAAQgEgxgcgeQgdggguAAQgrAAgeAfg");
	this.shape_9.setTransform(316.2,46.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AjYEZIAAowIDcAAQBiAAA5AzQA6AzAABVIAAACQAABahCAzQg+AxheAAIhxAAIAAC1gAh2ALIB0AAQA1gBAhgZQAhgcAAgtIAAgBQAAgxghgbQgggZg2AAIh0AAg");
	this.shape_10.setTransform(269.7,39.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhQDLQgwgRgngeIArhCQBGA0BDAAQAfAAASgMQARgMAAgWIAAgBQAAgWgcgPQgOgGgzgRQg8gRgegTQgqggAAg2IAAgCQAAg7AsgkQAqgiA/AAQAoAAApANQAoANAjAWIgnBFQhBgog2AAQgbAAgQAMQgPAMAAATIAAABQAAAVAdAOQAPAIAyARQA7ASAeAUQAqAgAAAzIAAACQAABAgtAkQgrAhhFAAQgsAAgvgQg");
	this.shape_11.setTransform(200,46.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgvEiIAAmmIBfAAIAAGmgAgzjFIAAhcIBnAAIAABcg");
	this.shape_12.setTransform(171,38.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgqDVIiumpIBoAAIBwE2IBzk2IBmAAIitGpg");
	this.shape_13.setTransform(137.6,47);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AiSC5QgtgjAAg+IAAgCQAAhDAxgjQAugiBNAAQA9AAA2ASIAAgIQAAgsgbgYQgbgYgwABQg5gBhBAbIgahOQAngRAhgIQAqgKAtAAQBdAAAwAvQAuAuAABWIAAD6IhhAAIAAg1QgzA9hUAAQhAAAgqgigAhFAhQgaASABAfIAAACQgBAcAZASQAWAQAjAAQAvAAAggZQAggZAAgnIAAgYQgsgRg1AAQgsAAgaARg");
	this.shape_14.setTransform(92.7,46.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("Ah6DYIAAmmIBhAAIAABfQAVgyAigbQAngdA2ADIAABmIgGAAQhCAAgmAqQgmAsAABQIAACig");
	this.shape_15.setTransform(59.5,46.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgwEZIAAnVIizAAIAAhbIHHAAIAABbIizAAIAAHVg");
	this.shape_16.setTransform(22.8,39.9);

	this.addChild(this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,583.3,69);


(lib.TP_logo_white = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AivCxQhKhJAAhoQAAhmBKhJQBJhKBmAAQBnAABKBKQBJBJAABmQAABohJBJQhKBJhnAAQhmAAhJhJgAh8BcIAdAAIAAgsQANAPATAAQAUAAANgOQAOgPAAgZIAAAAQAAgXgOgPQgOgNgTAAQgTAAgNAQIAAgOIgdAAgAA+gbQgOAQAAAUIAAABQAAAXAPAPQAPAPAYAAQAYABARgUIgRgRQgMALgLAAQgMAAgHgIQgIgIABgNIAAAAQgBgJAIgJQAHgIALAAQAMAAALAMIASgUQgQgRgZAAQgYAAgQAPgAAAA9IAeAAIAAiKIgeAAgAhYAfQgHgJAAgNIAAAAQAAgKAHgJQAIgHAKgBQALABAGAHQAIAJAAAKIAAAAQAAANgIAJQgGAHgLAAQgKAAgIgHg");
	this.shape.setTransform(558.2,25.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#231F20").s().p("ACcDsQg/g9ABhhIAAgCQAAhbA5g/QA7hBBbAAQBgAAA3BFQAzA+AABfIgBAbIk4AAQAIA0AiAcQAgAcAxAAQAiAAAcgNQAagMAbgZIA4AyQhDBQhqgBQhdABg+g+gADhghQgcAfgIAwIDaAAQgEgvgcgeQgdghgvABQgsAAgeAegEggiAEHQgtgkAAg9IAAgCQgBhEAxgkQAugjBOABQA+AAA3ARIAAgIQAAgqgbgYQgbgXgzAAQg5AAhAAbIgahOQAngSAhgIQApgJAwgBQBdABAvAuQAvAvgBBTIAAD8IhgAAIAAg1QgzA+hXgBQg/AAgqghgA/WBvQgZARAAAgIAAABQAAAdAYARQAWAQAkABQAwgBAggYQAhgZAAgoIAAgYQgtgRg1AAQguABgaARgEAggAEYQgwgRgngeIArhCQBGA0BGAAQAeAAASgNQARgMAAgVIAAgCQAAgWgcgOQgOgHg0gQQg+gRgcgWQgrgfAAg0IAAgCQAAg7AsgkQAqgiBBgBQAnABAqANQAoAMAiAXIgmBFQhCgog2AAQgcAAgPAMQgQALAAATIAAACQAAAUAdANQAQAIAyAQQA9ATAcAWQArAgAAAzIAAABQAABBguAjQgqAihFAAQguAAgvgQgAuwEYQgvgRgogeIAshCQBFA0BGAAQAfAAARgNQARgMAAgVIAAgCQAAgWgcgOQgOgHg0gQQg9gRgdgWQgqgfAAg0IAAgCQgBg7AtgkQAqgiBBgBQAnABAqANQAoAMAiAXIgmBFQhCgog2AAQgcAAgQAMQgPALAAATIAAACQAAAUAdANQAPAIAzAQQA8ATAdAWQArAgAAAzIAAABQAABBguAjQgqAihGAAQgtAAgwgQgA35EjIiumpIBoAAIByE2IBzk2IBmAAIitGpgAcpEgIAAjxQAAgwgYgaQgXgbgrAAQgrAAgaAcQgbAaAAAwIAADwIhhAAIAAmmIBhAAIAABCQAyhLBTAAQBGAAApAtQAoArAABJIAAEOgAVCEgIAAmmIBhAAIAAGmgASZEgIh/i3Ig8A+IAAB5IhhAAIAApIIBhAAIAAFcICwi6IB2AAIipCqICuD8gAJGEgIAAmmIBhAAIAABgQAUgyAlgcQAngdA1ADIAABmIgFAAQhCAAgmAoQgoAsAABSIAACigAl/EgIAAowIDeAAQBiAAA6AzQA3AzABBVIAAADQgBBYhAA1Qg9AwhhAAIhwAAIAAC1gAkcASIBzAAQA4AAAhgaQAggcABgsIAAgCQgBgwgggbQgggZg5AAIhzAAgAywEgIAAmmIBhAAIAAGmgEglWAEgIAAmmIBhAAIAABgQAVgzAkgbQAogdA2ADIAABmIgHAAQhCAAglAoQgpAsAABSIAACigEgp7AEgIAAnUIizAAIAAhcIHJAAIAABcIiyAAIAAHUgEAohgAvIAAiGIAdAAIAAAOQANgQATAAQATAAAOANQAOAPAAAZIAAAAQAAAZgOAPQgNAOgUAAQgTAAgNgPIAAAsgEApFgCXQgHAJAAAMIAAAAQAAANAHAJQAIAHAKAAQALAAAGgHQAIgJAAgNIAAAAQAAgMgIgJQgGgHgLgBQgKABgIAHgEAregBbQgPgPAAgXIAAgBQAAgWAOgQQAQgPAYAAQAZAAAQARIgSAUQgLgMgMAAQgLAAgHAIQgIAJABALIAAAAQgBANAIAIQAHAIAMAAQALAAAMgLIARARQgRAUgYgBQgYAAgPgPgEAqfgBOIAAiMIAeAAIAACMgAU+jHIAAhdIBoAAIAABdgAy0jHIAAhdIBpAAIAABdg");
	this.shape_1.setTransform(286.3,39.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,583.3,69);


(lib.Time = function() {
	this.initialize();

	// Layer 2
	this.mc_TimerCounter = new cjs.Text("1:35", "35px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_TimerCounter.name = "mc_TimerCounter";
	this.mc_TimerCounter.textAlign = "center";
	this.mc_TimerCounter.lineHeight = 37;
	this.mc_TimerCounter.setTransform(140,22.7);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("AKIEPIAAAtIgwAAIyyAAIgtAAIAAgtIAAoYIAAgyIAtAAISyAAIAwAAIAAAtIAAAFg");
	this.shape.setTransform(138.6,35.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F4D96F").s().p("ApZEPIAAodISyAAIAAIdgAorgLIAACgIAABOIRaAAIAAjuIAAjVIxaAAg");
	this.shape_1.setTransform(138.5,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#474747").s().p("AotB3IAAhNIAAigIRaAAIAADtg");
	this.shape_2.setTransform(138.6,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#666666").s().p("AotBqIAAjTIRaAAIAADTg");
	this.shape_3.setTransform(138.6,23.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("AJYE8IyyAAIgtAAIAAgtIAAoYIAAgyIAtAAISyAAIAvAAIAAAuIAAAEIAAIYIAAAtgApaEPISyAAIAAoeIyyAAg");
	this.shape_4.setTransform(138.6,35.4);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.mc_TimerCounter);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(72.3,2.2,132.6,66.3);


(lib.Symbol2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EAmoACnQAAgFAAgHIAAgVIAAgtIAAj/EgmngCmIAAD/IAAAtIAAAVQAAAHAAAF");
	this.shape.setTransform(247.3,16.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("EgmnACnIAAgMIAAgVIAAgtIAAj/MBNPAAAIAAD/IAAAtIAAAVIAAAMg");
	this.shape_1.setTransform(247.3,16.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AgxCnIAAgMIAAgVIAAgtIAAj/IBiAAIAAD/IAAAtIAAAhg");
	this.shape_2.setTransform(499.6,16.8);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,506.6,37.6);


(lib.Symbol1 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ahig9QA3ADArgoQAtAoA2gDQADB9hmAjQhlgjADh9g");
	this.shape.setTransform(23.9,24.2);

	this.text = new cjs.Text("Leaderboards", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(43.3,15.9,0.59,0.59);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4F2A3F").s().p("AwMDBQgygBAAgyIAAkbQAAgzAyABMAgZAAAQAygBAAAzIAAEbQAAAygyABg");
	this.shape_1.setTransform(113.8,24.2);

	this.addChild(this.shape_1,this.text,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(5,4.9,217.6,38.6);


(lib.Sure = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("are you sure you want to quit the game?", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 395;
	this.text.setTransform(135,-105.1,1.18,1.18);

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EgttAPaMBLlAAAQCOAAAAiNIAApIIP1zeIv1LUIAAi8QAAiNiOAAMhLlAAAQiNAAAACNIAAUOQAACNCNAAg");
	this.shape.setTransform(187.7,-76.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EDED5D").s().p("EgttAPaQiNAAAAiNIAA0OQAAiNCNgBMBLlAAAQCOABAACNIAAC8IP1rUIv1TeIAAJIQAACNiOAAg");
	this.shape_1.setTransform(187.7,-76.1);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-121.1,-176.8,617.6,201.3);


(lib.shirt03 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#483F63").s().p("EArrAPmIwgpsQhDgogwgxQgvgxAAgfQAAgegvgWQgwgVhCAAQhDAAgvAzQgwAzAABIQAABIgaA0QgaAzgmAAQglAAgagzQgbg0AAhIQAAhIgygzQgzgzhHAAQhIAAgyA3QgzA3AABNIAAEbQAABOgaA2QgaA3gmAAQglAAgag3Qgbg3AAhNIAAkbQAAhNg3g3Qg3g3hNAAIgmAAQhNAAg3A3Qg3A3AABNIAAH8QAABNgcA3QgbA3gnAAQgnAAgZg3Qgcg3AAhNIAAn8QAAhNg3g3Qg3g3hNAAIhLAAQhOAAg3A3Qg3A3AABNIAAH8QAABNgbA3QgcA3gmAAQgnAAgcg3Qgbg3AAhNIAAn8QAAhNg3g3Qg3g3hOAAIglAAQhOAAg3A3Qg3A3AABNIAAGlQAABOg3A1Qg3A1hNgHQkSgZkbhSQkhhSjoh6Qj3iBiKiWQiWijAAioQAAimCWikQCKiWD3iBQDph6EghSQEbhRESgaQBNgHA3A1QA3A1AABOIAAGlQAABNA3A3QA3A3BOAAIAlAAQBOAAA3g3QA3g3AAhNIAAn8QAAhNAbg3QAcg3AnAAQAmAAAcA3QAbA3AABNIAAH8QAABNA3A3QA3A3BOAAIBLAAQBNAAA3g3QA3g3AAhNIAAn8QAAhNAcg3QAZg3AnAAQAnAAAbA3QAcA3AABNIAAH8QAABNA3A3QA3A3BNAAIAmAAQBNAAA3g3QA3g3AAhNIAAjuQAAhNAbg3QAag3AlAAQAmAAAaA3QAaA3AABNIAADuQAABNAzA3QAyA3BIAAQBHAAAzgsQAygtAAg/QAAg/AbgsQAagtAlAAQAmAAAaAtQAaAsAAA/QAAA/AwAtQAvAsBDAAQBCAAAwgVQAvgWAAgeQAAgfAvgxQAwgxBDgoIQgpsQBDgnAaAXQAaAWgeBIIk/LsQgfBIAABjQAABlAfBHIE/LsQAeBIgaAWQgKAJgPAAQgaAAgqgZgA+CmfQg+A+AABXQAABXA+A9QA9A+BXAAQBXAAA+g+QA9g9AAhXQAAhXg9g+Qg+g9hXAAQhXAAg9A9g");
	this.shape.setTransform(89,65.1,0.08,0.08,-4.8,0,0,-0.5,-0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B39DF7").s().p("AjGDHQhThTABh0QgBhzBThTQBThSBzAAQB0AABSBSQBTBSAAB0QAAB0hTBTQhSBSh0AAQhzAAhThSg");
	this.shape_1.setTransform(89.2,64.3);

	// Layer 5
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAZByQhTgBgsgiQgngegOgfQgHgQgCgaQgDgiAogbQAogcBPAAQBNgBA0AdQA0AegJAsIgJAtQgIAcgSAYQgUAchPAAIgFAAg");
	this.shape_2.setTransform(82.1,14.6);

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7667A3").s().p("ACIHLQhrgVgJAAIgEAAIAAgWIAAghIgBgBIAAhJIAAhJICLAAIgEDiIgOgDgAiUCcQgFgKgBgZQAAhFAahgQAMgvAQgtQAXg/Aeg5QBEiEBdhJQhBBcgxCJQgNAsgMArQgnCKAAB7IAFBHIgIAAIgOABQgwAAgTggg");
	this.shape_3.setTransform(20,60.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#907EC6").s().p("Am0HwIgYgEIgogJIAEjiIAVAAIAAADQAagEAYABIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAEgzADgvQACgxAAgpQAAhegKgEQgJgCgOBCQgKAwgHAyQgQB2AACDIAAAoIkQgEIgFhHQAAh9AniIQAMgrAPgsQAxiJBBhcQBThBBlgUIE2gEQC8AeB2ArIAAABIAAgBQBIAeAcARQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIhgADIAAABIlUALQg/gFiUgdg");
	this.shape_4.setTransform(85.2,58);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E5E5E5").s().p("AkUCPQgSgEiSgZQhZgQgLgMIgEgHIAZAAQAPgFBSgIIAEAAQAsAmCIAcIA7ALgADPA8QgrgOgFgDQABgDgIgGQAigTAfgPQAnAfCBAQQAaAEBuAIIgKABgAlJALQgbAAgrADIgMgPIAAAAQgNgQgBgDIAKgKQAOgLAPgHIAogQQBkgQBqAQQAPAEAsAUQgCARgKAQIgTAqQhhgXh4gBgAG3hVQgjgDgoABQgWABgUACIAAgFQAAgVAHgNQAsgTA/AAQBDADAqAPQgHAFgVAeIgNASQgNgEg0gKg");
	this.shape_5.setTransform(87.3,136.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_6.setTransform(94.8,140.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#1E4675").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrIAHAoIgIADQgoAZAAAiIAGAiIABADIgCAAQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_7.setTransform(67.5,124.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#275994").s().p("AglCMIgYgIQAHgdANgUQAOgYAAgWQAAgbgQgJQgMgGhMgYQhsgNhrANQg1ATgTAOIgGgnIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgEAYgBAhQhCAEg0AOQgtAVAAA5IAAAQQgbAGgZAJIgNg1g");
	this.shape_8.setTransform(86.9,128);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AjbMJQjFgig5gUQg7gUgBgmQg1kZABg+IAAgMIgQgGQgFACgJAAQgPAAgGgVIgCgLIAAgiQAAgcALiZIg/AAQgZgEhHgcQgKgIgFgIQgJgLAAgtQAAhKAYhmQAJgnALgkQAZhUAmhKQCTkkENgZIEdgFQEsAwCDBNQA1AfAsAuIBMBYIBxCjQBYBeAABGIACAAIADAFQACAEAAALQABALgPAQQgtAkgOAJQgxAgghAKIAPA/QAHAfAJBBQAGAsAAAiQAAAzgXANIgDgHQgbAThBAaIgMAaIgOAdQgEAMgCAOIBTASQASAOAHAKQAEAHABAJQAAAXgeAkIgQASQBLAoAABKQAAARgSAOQgdAWhBgDIk7AAQguACgZgYQgTgRAAgSQgIgXgMg2IgDgMIgBAEQgTBegTAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYguAGgAneKgQALAMBZAQQCRAZASAEIBiAAIB1AAQAQgKAagSIhbgXQh6ggheAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAFAHgAhJHYQBKAXAMAIQAQAKAAAaQAAAXgOAXQgNAVgFAdIAVAIQAgALAWAMQAFgkAUhtQARheAHgyIkuAEQhggEhsgTQgpgIgqgJQgzgLghgKIAEARQAlCIAACRQAUgFBYgHIACgBIgBgCIgGgjQAAghAogZIAIgDQATgRA1gSQA1gHA2AAQA2AAA2AHgAGwIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADAsAOIEwAAIAJgBIADgBQATgDAbgCQg8hrhuAAQgOAAgPACgAgzJtIATgrQAKgRADgQQgsgVgQgEQhpgQhlAQIgnAQQgPAHgOALIgKAKQABADAMAQIAAABIAMARQAsgDAaAAQB4AABhAXgAE9F2IhAABIhWABIAAAKQAAATgDAbQAJASAHAeQAMAwAEAuQATgUAhgQQAQgIARgHQAZgJAcgGIAAgSQAAg5AtgUQA0gOBCgEQABgiAEgXQhNAVhsAPgAH1H0QA0AJAMAEIANgSQAVgeAIgFQgqgOhEgDQg/AAgsASQgGANAAAVIAAAFQAUgCAVgBIAWAAQAcAAAaADgApSCEIAABJIABABIAAAhIAAAWIAEAAQAJAABrAWIAOADIAoAIIAYAEQCUAeBAAEIFTgLIABAAIBfgEQCeACBpgtQBJggBAgLQgSg0gLhDQgQhwgNg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAANAuIAJAtQAGAcAIATQAWAgAWAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgTgbh1iHIhGhTQgpgsgxgeQgcgRhIgdIAAAAIAAAAQh2gsi7geIk3AFQhlAThTBBQhdBJhGCEQgeA5gXBAQgQAsgMAwQgaBhAABFQABAZAFAKQAWAjA7gFIAIAAIEQADIAAgmQAAiDARh4QAHgyAJgwQAOhCAJADQAKADAABeQABApgDAyQgCAugFA2QgLB+gEA5IACADQAAADgCADIgBAIIgDAEQgFADgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAABJg");
	this.shape_9.setTransform(81.1,77.7);

	this.addChild(this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,162.2,155.5);


(lib.Share_Twitter = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Share on Twitter", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(60.3,21.4,0.8,0.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Aj6CdQAMACANAAQBIAAA4gsQgigBgagUQgagUgKgfQAIACALAAQAMAAAPgDQgjgIgYgcQgXgbAAglIAAgBQAWANAYAAQgVgOgMgWQgNgXAAgbQAAgcAOgYQAnAvA3AdQA4AdA/ADQgDgNAAgKQAAgrAcgeQAfgfAqAAQAtABAeAgQAhgGAhgTQgMAkghAVQAfgEAcgMQgVAfgfAWIABANQAABuhMBWQhUBgiEAAQhWAAhIgvg");
	this.shape.setTransform(31.4,31.9,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#23AADB").s().p("A5SExQgyAAAAgyIAAn9QAAgyAyAAMAylAAAQAyAAAAAyIAAH9QAAAygyAAg");
	this.shape_1.setTransform(166.9,30.6);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,333.9,61.2);


(lib.Share_FB = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Share on Facebook", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(56.6,20.6,0.8,0.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhDFDIAAknIhjAAIAAhxIBjAAIAAhUQAAhJApgpQAlgnBEABQA5gBAfAFIAABnIg9AAQghAAgNAPQgLANAAAcIAABJIByAAIgPBxIhjAAIAAEng");
	this.shape.setTransform(29,32.4,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3A5A94").s().p("A3aExQgyAAAAgyIAAn9QAAgyAyAAMAu1AAAQAyAAAAAyIAAH9QAAAygyAAg");
	this.shape_1.setTransform(154.9,30.6);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,309.9,61.2);


(lib.Score = function() {
	this.initialize();

	// Layer 2
	this.mc_Scoring = new cjs.Text("000465", "35px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_Scoring.name = "mc_Scoring";
	this.mc_Scoring.textAlign = "right";
	this.mc_Scoring.lineHeight = 37;
	this.mc_Scoring.setTransform(187.8,23);

	this.mc_ScoringCounter = new cjs.Text("000465", "35px 'Laffayette Comic Pro'", "#999999");
	this.mc_ScoringCounter.name = "mc_ScoringCounter";
	this.mc_ScoringCounter.lineHeight = 37;
	this.mc_ScoringCounter.setTransform(58.3,23);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("AOoEPIAAAtIgwAAI7zAAIgsAAIAAgtIAAoYIAAgyIAsAAIbzAAIAwAAIAAAtIAAAFg");
	this.shape.setTransform(124.5,35.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F4D96F").s().p("At5EPIAAodIbzAAIAAIdgAtMgLIAACgIAABOIabAAIAAjuIAAjVI6bAAg");
	this.shape_1.setTransform(124.3,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#474747").s().p("AtNB3IAAhNIAAigIabAAIAADtg");
	this.shape_2.setTransform(124.5,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#666666").s().p("AtNBqIAAjTIabAAIAADTg");
	this.shape_3.setTransform(124.5,23.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("AN5E8I7zAAIgtAAIAAgtIAAoYIAAgyIAtAAIbzAAIAvAAIAAAuIAAAEIAAIYIAAAtgAt6EPIbzAAIAAoeI7zAAg");
	this.shape_4.setTransform(124.5,35.4);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.mc_ScoringCounter,this.mc_Scoring);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(29.3,2.2,190.3,66.3);


(lib.S_off = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAAARIgtAvIgTgSIAvguIgvgtIATgTIAtAvIAugvIASATIguAtIAuAuIgSASg");
	this.shape.setTransform(6.5,6.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,13,13);


(lib.Roller = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AhEA2QgJgIAAgDQAAgKAJAAIAYAFIAZAEQAjAAAWgXQAOgPAGgRQAUhSAAA9QAAAjgUAbQgbAhgxAAg");
	this.shape.setTransform(43.1,10.5);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#56381C").ss(2,1,1).p("AAdAAQAAAKgJAHQgIAIgMAAQgLAAgIgIQgJgHAAgKQAAgJAJgHQAJgIAKAAQALAAAJAIQAJAHAAAJg");
	this.shape_1.setTransform(36.3,22.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E5E5B7").s().p("Ak2ARIACgRIAogDQAxgGAlgJQATgEA6gBQBFgCAxgGQBYgLCMgPQA8gIAtgPQgHAegJAOQgNAUgcAJQgQAGjLAZQgUAEhcAHQhLAFguAKQh6Abg8AEQAbgKAIg2g");
	this.shape_2.setTransform(47.6,126.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#666666").s().p("AgJAYIgBgSIAEgBQAIACACgKQACgHgBgDIAAgEIAAgBIgFgGQAGABAAABIADASQACAKgBAGIgBABQAAAEgFAEQgEAEgGAAIgDgBg");
	this.shape_3.setTransform(9,119.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DBDBAF").s().p("AgOCIQgZgcgLhEQgPhZAKgtIACgFQAAAFABAEQAFA0AOAYIAAADIgEAQQADAHAAAQQAEAOAVAJIAEABIACABQAUAIALgPQAGgIAEgSQAGgYgbgmQgTgdgKAOIAAAAQgKgYgGgcQgDgOgBgQIAAgBIAAAAIACgBIAAABIAAACQAAAAAAABQABAAAAAAQAAABABAAQAAAAABAAIgCABIAUAOQAwAoAQA3QAMApgNA4IgBAFQgNA0gXAJQgNAFgGAAQgJAAgDgGg");
	this.shape_4.setTransform(8.6,119.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFCC").s().p("AksA4QgIgsgFgPQgFgRgWgcQgNgPgLgIIBVgRQB6gbAvgBQA1AAA0gGQAzgIAagCQBZgICPAXQAwAuAKAqQAHAcgKApIgDAPQguAPg8AIQiMAPhYALQgxAGhFACQg6ABgTAEQglAJgxAGIgoAEQAFgsgGgkg");
	this.shape_5.setTransform(46.7,113.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#87876B").s().p("Ak2DCQgCAFgEAAQgGAAgTgGQgPgFgMgKIgHgEIgBgCIABgBQgogmgKhbQgEgpACgsQACggACgKQAFgYAUgKQADAMAAANIABABQADALgDAKQgCgEAAgFIgCAFQgKAtAQBZQALBFAYAcQAIAJAZgIQAYgKANg0IABgEQAMg5gMgpQgPg3gzgnIgTgOIACgBQgBgBAAAAQgBAAAAgBQgBAAAAAAQAAgBAAAAIgBgCIAAAAQgBgDACgGQAFgXB7gRQBWgMA6gDQA3gCCLgQQBWgJBbAKQB1AOAeBVQASAwgNA5IgBAEQgJAqgJATQgNAZgZAOQgxAaiqAJQg/AEiRAcIjKAmgABviwQgbACg0AIQgyAGg1AAQgwABh6AbIhVARQAMAIAMAPQAXAcAFARQAEARAIAqQAGAkgEAsIgDASQgIA2gbAKQA9gEB6gbQAtgKBKgFQBdgHAVgEQDKgbARgGQAbgJANgUQAJgOAIgeIAEgNQAJgrgGgcQgKgqgwguQhrgShNAAQgaAAgWADgAlaBHIgCAAIgDgCQgWgJgDgNQAAgRgEgGIAEgTQAPAMAQAFIgEABIABASQAIABAHgEQAFgEAAgEIABgBQABgGgCgMIgDgQQAAgBgGgBIAFAGIAAABQgCgEgEgBIAAgCQgGgIgDgKIgGgHIAAgBQgBgCAAgDIAAgBQAKgNAUAdQAbAkgFAaQgEARgGAIQgIAKgMAAQgGAAgIgDg");
	this.shape_6.setTransform(42.9,117.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AiaC8QgQgGgPgLIABgDQgOgYgFg0QADgLgDgLIgBAAQAAgNgDgMIABAAQACgxABgNIABgDQAJglA7gPQAogJBVgDQBggCAbgEQA6gIAFgaIAFgKQABgFgDgPQgDgIgBgKIgCgJIACgDIAFAAIADgDIAEACIALABIABABIABAFQAHAXgBANQAAAFgFAZQgJAlg7ANIgKACIgnAFIhMADQgnAAgpAFIgrAGQg3AGgHAYQgCAIgFAHQgDAJACAWIAAAFIAAACIAAABQACAQADAOQAFAcALAYQAAADABACIAAACIAGAGQADAKAGAIIAAACQAEACACADIAAAFQABADgCAGQgCALgIAAIgCAAg");
	this.shape_7.setTransform(23.8,101);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CC8844").s().p("AgFFRQgMgEgKgRIBxgUIgBASQgBAGgaANQgTAIgTAAQgNAAgMgEgAhBlAQgNAEgHAFIAAgBQADgOAtgLQAugMAOAfIACAJQgygUgoAJg");
	this.shape_8.setTransform(38.1,45.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#996633").s().p("AhQABQgEhTgBiCIAAg3IALgHQAOgOAJgBQAjgHAnASIAMAFIAIAxIALBcQAGBAAFAbQADAWAJBwQAIB6ABA6QgeABgeADQgwAFgQAKQgfhQgLjTgAglj4QgJAHAAAKQAAALAJAHQAIAHAMAAQANAAAGgHQAJgHAAgLQAAgKgJgHQgHgIgMAAQgLAAgJAIg");
	this.shape_9.setTransform(38.1,45.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#3F2A14").s().p("AAiFyIgRAEIgkgNQgvg/ggkGQgej/AXhmQAFgXAbgPQAagQAfACQBOAFAFBjIAQBnQAKA/gBAkIAPCzQAOC1gHAgQgHAdgHADQgSACgYAGIACAGIgLgBIgEgCIgDADIgFAAIgCADgAgXE9QALARALADQAeAMAhgQQAbgMAAgGIABgSgAgvkiQgJACgOANIgLAIIAAA2QABCDAFBTQAKDTAfBQQAQgLAxgEQAdgDAfgBQgCg6gIh6QgJhwgDgXQgEgagHhAIgKhcIgJgyIgMgFQgbgNgbAAQgKAAgKACgAAek1IgDgJQgOgfguANQgtALgDAOIAAAAQAHgEANgEQApgKAyAUg");
	this.shape_10.setTransform(37.7,45.3);

	this.addChild(this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,4.4,85.8,134.3);


(lib.ReplyText = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("reply", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 32;
	this.text.lineWidth = 133;
	this.text.setTransform(78.5,0,1.18,1.18);

	this.addChild(this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,161.8,44.3);


(lib.ReplyBoxShad = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.502)").s().p("AKEEzI1OAAQiOAAAAiNIAAlLQAAiNCOAAIWVAAQCOAAAACNIAAFLQAACNiOAAg");
	this.shape.setTransform(85.8,30.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,171.5,61.6);


(lib.ReplyBox = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("ALMEzIhHAAI1PAAQiOAAAAiNIAAlLQAAiNCOAAIWWAAQCNAAAACNIAAFLQAACNiNAAg");
	this.shape.setTransform(85.8,30.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("AKEEzI1OAAQiOAAAAiNIAAlLQAAiNCOAAIWVAAQCOAAAACNIAAFLQAACNiOAAg");
	this.shape_1.setTransform(85.8,30.8);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,175.5,65.6);


(lib.replaybtn = function() {
	this.initialize();

	// game_icons
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AkgKqQhXgmhfhKQiQhvgfhtIFghLQAcAnAbAVQAZASAyAXQCWBFCehAQCfg/BGiYQARglAIgjIACgIQAEgQABgKIACgVIACgPQACgngFglQgWiliFhlQiEhliiAWQhOALhDAmQhAAlgwA6QArA1CCCEIpaBaIgYqEIA+A5QBIBAAwAmQCsjnE2gqQDQgdDBBXQC8BVB2CpIABgCQBIBoAiB5QAiB6gIB8IgBALQgEAqgDAWIgbBsQhBDKihCJQimCOjZAdQg0AHgzAAQiWAAiPg/g");
	this.shape.setTransform(35.1,35.3,0.26,0.26,0,0,0,-0.1,0.1);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4,1,1).p("AjtFBQAOAFANAEQAwAOA1AAQCPAABmhmQBmhmAAiPQAAiQhmhmQhIhIhagVQgBAAgCgB");
	this.shape_1.setTransform(45.9,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(4,1,1).p("ADWlHQgmgJgqAAQiPAAhmBmQhmBmAACPQAACQBmBnQA2A1A/Aa");
	this.shape_2.setTransform(21.4,33.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F4D96F").s().p("AhEFMQhBgag1g1QhmhmAAiRQAAiOBmhmQBmhmCOAAQAqAAAmAIIADABQBeBeAjBnQAjBngcBfQgcBhhXBJQhSBFh5AmIgbgJg");
	this.shape_3.setTransform(29,34.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("AjfFKQB7gnBShFQBVhJAchhQAchegjhnQgjhohchdQBaAVBHBHQBmBmAACRQAACOhmBmQhmBmiOAAQg1AAgwgNg");
	this.shape_4.setTransform(47.2,35.3);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.phone = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AE9C3QAAAagEAIQgEAGgPADQhBAOg0AAQgHABgIgCIgagLQgagLgRgMQgXgQgVgbQgNgRgBAAQgFgKADgLQAIgdgUghQgQgegugqQgQgPgFgEQgGgDgVgGQgegJgLAQQgOAVgrgHQgbgEgkgaQgcgUgZgbQgIgFgCgDQgEgEABgIQABgCAAgCQAFgZAEgLQAGgNAFgLQAEgMAPgNQAOgNAOgFIATgEQA9gYA4ARQAjALA5AqQA1ArANAIQAPAKAZAXQAcAZAMAIIBkBOQAeAWAMANQAYAZALAoQAFARAFAaQACALACANg");
	this.shape.setTransform(31.8,24.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#636260").s().p("ACiCyIgagKQgagLgRgMQgXgQgVgcIgOgRQgFgKADgLQAIgcgUgiQgQgbgugtIgVgSQgGgDgVgHQgegJgLARQgOAUgrgGQgbgFgkgZQgcgUgZgbQgIgFgCgEQgEgEABgHIABgFQAuAgAbAKQAYAIArAAIA+geQAdgCAnAUQAfARAYAYIAjAgQAXAYAJAaQAVBBAUAWQAqAvBIgHQAygEAfgGIAEAYQAAAagEAHQgEAHgPADQhBAOg0AAIgEAAQgFAAgGgCg");
	this.shape_1.setTransform(31.8,30.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#777674").s().p("AB4CkQgUgWgWhCQgJgbgWgYIgjgeQgYgYgfgQQgngVgdADIg+AeQgrAAgZgJQgbgJgtggQAFgZAEgLIAKgYQAFgMAPgNQAOgNANgFIATgEQA9gYA4ARQAkALA4AqQA1ArAOAIQAOAKAaAXQAbAZAMAIIBkBOQAeAWANANQAYAZALAoIAJArQgeAHgzAEIgSAAQg7AAgkgpg");
	this.shape_2.setTransform(31.6,20.6);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1.9,-2,67.5,52.2);


(lib.PeppaHead = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BA9870").s().p("ACQHdQhsgsh6g9IALgaQATgugBgWQAAgDgYgWQgcAPgHAkQgHAlgNAHQjEh8gBgiQAAgOAQgZQAQgYAAgHQAAgMgEgDIgDgGQgWAGgPAHQgDgYAAguQAAhTAkhpQAoh5BJhjQBThwBqhAQApBXAkBPQBdDVAVB1QhQASABBMQAAAMACACIADAEQAQACATgCIApgIIAKgCQASB8AeBkQAjBvBJCYQhhgWhsgsg");
	this.shape.setTransform(38.2,62.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC0066").s().p("AgmAMIg6geQBFgNB8AfIgcAMQgdALgbAAQgaAAgZgLg");
	this.shape_1.setTransform(89.7,104.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ADeFPQjaAAjehuQhQgng7gqQgcgVgMgMQgLALgMAAQhPAAhfhhQgighgaglQgXgiAAgKQAAgtAFgJQAHgNAYgLQAPgHAWgGIADAFQADAEAAAMQAAAHgPAYQgQAZAAAOQAAAiDFB6QANgHAHglQAHgiAcgPQAYAWAAADQAAAUgSAuIgMAZQB9A+BsAsQBrAsBgAWQBaAUBSACQBuACBchSQAXgUAugxQAngsAWgQQAFgEAEgBQAFgBADADQAEACABAEIADAFQADAEAHgFQAIgGgkA3QgjA3haBKQhaBLh/AAIAAAAgABbCpQg9gqAAgTQAAguBvACQApABDOAZQAYADAAAZQAAASgvAkQg8AthHAAQhHAAhIgwgAB5B2IA5AgQA0AVA5gVIAcgOQhYgWg8AAQgaAAgUAEgADWATQg8gJgSgUQgJgKAAgXQAAg0AmAYQAqAlALAEQA9APA9gPIAAgSIAIgFQAIgFAMAAQANAAAGACIADACIADAFQADAEAAAMQAAAOgQARIgYAVQggAFgkAAQgjAAgngFgAIPi3QgPgBgSgJQgWgLgPgRQgOgPgJgEQgQgHgdAEQgMADgLAEQgHgCgEgEIgDgFIgCgEIgCgDIADgFQAagpBpACQBhACA4AeQAVALAMARQALAPgBAIIgBAHIAAABIgBAAQgLgGgQgDIgOgBIgIgBQgJABgHADIgOAFQgGACgPALQgMAJgKACQgLADgKAAIgGgBgAhDjeQgIgEgaADIgKABIgpAIQgUACgPgCIgDgDQgDgDAAgLQAAhOBPgSQAigJAvADQBwAGCYA5QANAFgPAKQgPAJgZADIgbAAIggAAQgdAiguAFQgRACgQAAQgxAAgogUgABFj/IAEgBIgEAAIAAABg");
	this.shape_2.setTransform(67.8,90.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhMFzQAbgkAKgTIARghQAWg8ARhAQAKgoACg3QACgbAAhGQgBg8ABgdQAAgLgVhsQgThtAAgLQAAgUAIgBQADgBAEAIQACgFAIALQAFAKATBiQARBXAGApIAIA1QAHA9gBAjQgEBpgWBMQgPAygOAjQgUAwgcAwQgDAFgIAAQgNAAgagMg");
	this.shape_3.setTransform(128.5,63.4);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D5AE81").s().p("AltD/QhJiwgai2QAAiMAzCJQAVA7A7DMQAPAgAYBGQAYBIAQAiQAOAfASAaIgShtQgOhZgehDQgnhYgpiFQgniBAAghQAAgEACgFQgMgDgHgEQgOgJAAgbQAAgIAKgGIAAgGQAAgKAEgHIg2gDIgZgYQgGgHAAACQgWhfBUgVQBUgVAbAGQAbAGAyhXQAzhXBZBSQBZBTCFAQQBXAKA0ATQAxARBNAWQA2ARAEATQAcAZAdCsQAOBXAJBSQAAAXgKA9QgKA+AAAVQAAAegXA9QgeBQgxBDQiPDDjqAAQjnAAiRlegABcIcIAOAAIANgEIgbAEg");
	this.shape_4.setTransform(84.2,60.5);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,136.3,124.1);


(lib.PeppaClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BA986F").s().p("AFfIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAAGGiIgOgcQAzAMA1AJIAmB3QgjAGgdAMQgghIggg6gAl6ATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUg");
	this.shape_1.setTransform(47.9,96.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D5AE81").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAIVirQACgCgKgQQgLgPgEAAIgBABIAAgBIgDgHQgCgFgFgXQgGgZgHgSIgPglQgFgMAAgDQAIgjACgVQADgegDgbQgHhDgGgWIAWgJQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APQgiAJgFACQgFABgEACQgohzghg7g");
	this.shape_2.setTransform(82.3,95.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#964F67").s().p("AAGLfQgzgFgHgDIAIgYQAGgPAPgIIAOgGIAGgCIAOgEIAAAHQAAAgBiAZIAaAGQgdABgiAAQgRAAgxgEgAEyLEQAAgNgEgNQAGAAAFgHQAIgKAFgEQAKgHAPgFQALAtANAPQAIAKAUAJIhIAFQgUABgHACQACgLAAgRgACxhmQhMgKh/gPQhkgMhVgRQAGgIAGgTQAOgmAIg7IAOhSQAFgaAAgeIAAgKIAAgCIgBgJIAAgHQAGgCAHgBQAFgBAJANQAIAKAEAMQADAGAIAwQAPA1AnAsQAnAsA2AWQAgANA+ALQA3AKAVALQAhASAIAnQgqgCgegEgAk+oAIg5gUQgZgKgCgJQgDgbAmgsQAfgjAkgZQAtgfBKgZQgVAPgjAvQgmA0gVAuQgGAOgFAgQgDAUgHAAIgBAAg");
	this.shape_3.setTransform(55.5,89.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#AF5D78").s().p("Ah3MoQhjgYAAghIABgHQA6gNCNABQASACAPAQQAQAPgBARIgFAFQgHAHgQAFQghAMg+ADIgagGgACQMSQgNgPgKgtQAcgJAyAAQBIAAAqATQAjAQAJAaQACAIghAEQg2AGhkAIQgTgIgJgKgAATgTQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgDgTgHgFQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgLgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6QArA7AFA/QAGA6gdArQgOAWgPANQAAALACACIACAEQADADATAAQAGAAAIgEIANgGQAAANAFALIALAdQAKAgAOAYIheAUQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_4.setTransform(78.6,81.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7A7A7A").s().p("AgaDYIiSg1IgOgDQAPgYAmhQIALgWQAVACAZABQAqACAigOIAHgGQALgFAQgOQAYgTAGgTQAFgWgQgxQgLgigJgPQgTgkgKgUQAQgfAGgRQApAZB0AQIAEABIgiDXQgeDIgCBEQhJgUhKgbg");
	this.shape_5.setTransform(29.7,103.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#8E8E8E").s().p("AoKDcQABhFAejHIAjjXQA+AIC8ASQBZAICugFQC8gGBYgRQAdgFATgHIBdEUQAnB0AKASQgRAKgQAFIgQAEQhtAhifAmQgXAFiOAQQiZASgwAAQitAAi9gyg");
	this.shape_6.setTransform(94.2,107.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgSgcgcg3QgzhuAAACIAAgFQigguichVIAAABQgPgEgFgHIgDgCIABgEIgBgPQAAgWAxhoQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQAHAGADASIABAHIABAJIAAACIAAAKQAAAegFAaIgOBSQgJA6gNAnQgHASgFAJQBUARBlAMQCBAOBLALQAfADAoADQBFAEBjgBQCkgCBAgOIBegUQgOgYgKgfIgLgeQgFgLAAgMIgNAGQgIADgGAAQgTAAgDgDIgCgEQgCgCAAgLQAPgNAOgWQAdgrgGg6QgFg/grg7Qgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgIABIAEAKQAxCcAAA7IAAAKQgGAMgFAEQgFAEgGAAIj0BHIgrANQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgABxIEQgqACgpABQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMQh0Aah0AKgAkuH/QAgA7AgBIQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJQhkAAhigPQg1gJg1gMIAQAbgApxDWQgmBQgQAYIAPADICSA1QBMAbBJAUQC9AyCtAAQAvAACagRQCPgQAWgFQCfgmBtgiIARgEQAPgFARgJQgKgTgmh0IhekTQgTAGgdAFQhXARi8AGQixAFhXgIQi8gRg+gJIgEgBQh1gQgogZQgGARgQAfQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgiAOgrgCQgYgBgVgCIgLAWgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAIvhnQAKAPgCADQAhA6AoB0QAEgDAFgCQAFgDAigIQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJQAGAXAHBCQADAcgDAeQgCAVgIAjQAAACAFAMIAPAmQAHASAGAYQAFAYACAEIADAHIAAABIABgBQAEAAALAQgAnWjiQgYA3gEAsQALgNAIgEIgCgEQgBgDAAgNQAAgIAKgkIAIggIgGAOgAF1rgIgDgFIADAFgAj5sUQAJgHAMgHQgMAHgJAHg");
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.PaintTwo = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5A88E2").s().p("Ah5BrIAAgDQAGhogChiIgCg4QB3AlB4ALIACEFQh6gLh5glg");
	this.shape.setTransform(16.5,59.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EAEABB").s().p("ABEGHQgsgFgggJQglgKgjgQIgUgMQgNgIgIgDQgDgkADg1IAHhXQB3AlB7ALIABDEQglgCgYgDgAhwhsQgDhVgEg3IgHhhIgBgYIgBgaQAWAPAZAMQAQAHAbAHIApAMQA5ARBBAJIAAA0QgQgCgPAKQgWAMgEAaQgCAHAAAJQgBATAJAQQAJAQAQAIQALAGANgCIADAAIABBOQh6gLh1gkg");
	this.shape_1.setTransform(15.8,55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhhA+QgzgHgwgOIAhgFIAigEQA0gFBBAEQA7ADA6AMQAYAEAfANIgNACQg8AGg5AAQhBAAg+gJgACfAlQgcgKgXgGQgxgLhAgFQhAgFgvAEQhAAGgsAWIgHgCQgOgFgSgKIgfgQIgbgQQAtgnBKgKQAdgDAngBIBFABQCBABAwAFQBkALBHAlQAlAQACAOQABAIgRAIIgXAKQgdAKgvAHQgegOgSgHg");
	this.shape_2.setTransform(38.2,11.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#6699FF").s().p("AjUCUIgBkFQAqADAqABIACADIAEAFIAJAJQAcAfAsAnQAqAlA8AqQAeAVARAJQAcAPAYAFQACAJAEAFQiLAfiGAAQg0AAg0gEgACJBNIgVgKQgMgGgPgKIgagTQgvgggugmIgigeQgBgDgCgCQgTgQgSgTQCcgBChgqIABA8QgBAfgCApIgGBFIgQAGIgPAHQgMAFgDASIgWgJg");
	this.shape_3.setTransform(50,60.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFCC").s().p("AjCF8IgTgBIAAjEQC6ARC+gsIAEAGQAIAHAKgFIANgEQgCA5ACAqIACAnQACAYAAAPIguAKIguAIIg1ALQghAIgUADQgrAGg3ABQgpAAg7gEgAhxhRQgQgVgUgeQgRgagMgNIgFgFIAGgGQAMgQgCgYQAAgYgNgPQgNgQgTgDIgEgBIAAg0IA4AFQBZAIBrgLQAogFAagEQAlgGAegJQAfgJARgTIAAAWQgEAngDA1QgBAmABBFIABAuQihAqibABIgIgIgAjXhOIAAhPQAKgCAJgGIANAUQATAiAhAmQgqgBgqgEg");
	this.shape_4.setTransform(50.2,56.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#231F20").s().p("AhfHzIgMgBQhYgGhBgTQgrgLgXgKQgngSgNgZQgIgDgBgLQgHgpACg5IAFhjIABgJIAChdIABhkIAAhDIgBgjIgFhmQgEg9gBgsIgBgbQAAgRACgMIgBgBQgBg0A3ghQAggTA5gOQAjgJAsgCQAagCA0AAQB/AAA0AFQBmAJBJAhQAhAQASAPQAZAVAEAcIAAAEQAJALADAVQACAOAAAUIgBAtIgCAtIABBpIAAAwIgBA5IAGADQAEABADAFIAFAJQAHAOADANQAIAEAFALQADAJABAJQABAfgEAWQgFAdgOAUQgKAOgMAKIgOAJIgBASIgDA0QAAAcABAXIADAoQABAXgBAQQAFALgCAMQgCANgJADQgWAGgiAGIg5AJIg1ANQggAHgUADQg8AKhEAAQgmAAgpgDgAltlCIABAZIAHBhQAEA3ADBUIABA4QADBggGBqIAAADIgHBWQgDA2ADAkQAIACANAIIAUANQAjAPAlAKQAiAJAsAGQAYADAlACIATABQA7AEAnAAQA3gBAtgHQAUgDAhgHIA1gLIAugJIAugJQAAgPgCgYIgDgoQgCgpADg6IgNAFQgKAFgIgIIgEgFQgEgFgCgJQgYgFgcgPQgRgJgegVQg+gqgqgnQgsgngagfIgJgHIgEgFIgCgDQghglgTgjIgNgTQgKAGgKACIgDABQgNACgLgGQgQgJgJgQQgJgQABgSQAAgJACgIQAEgZAWgNQAPgJAQACIAFAAQATADAMAQQANAQABAYQACAYgNAQIgFAGIAEAEQANAOARAaQAUAeAQAUIAHAJQAQARATAQQACACABADIAiAeQAwAnAvAhIAaATQAPAKAMAGIAVAKIAWAJQADgSAMgFIAPgHIAQgGIAGhHQACgpABgfIgBg6IgBgvQgBhEABgnQACg0AEgnIABgWQgSATgfAJQgdAJglAGQgbAEgoAFQhsALhYgIIg4gGQhCgJg4gRIgrgMQgbgHgQgHQgZgLgWgQIABAagAFYB4QAEgJABgNIAAgXQAAgHADgIQACgKAGgFIgHgRIgDgFIgGBhgAiQldIghAEIghAFQAvAOA0AHQBwARCDgOIANgCQgegNgZgEQg5gMg8gDIgzgCQgjAAgfADgAgUl4QBBAFAxALQAXAGAbAKQATAHAeAOQAugHAegKIAXgKQAQgIgBgIQgBgOglgSQhHglhlgLQgwgFiBgBIhEgBQgoABgcADQhLAKgtAnIAbAQIAfASQATAKAOAFIAHACQArgWBAgGQAXgCAaAAQAdAAAhADg");
	this.shape_5.setTransform(39.6,50.2);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,79.3,100.5);


(lib.PaintThree = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EAEABB").s().p("ABEGHQgsgFgggJQglgKgjgQIgUgMQgNgIgIgDQgDgkADg1IAHhXQB3AlB7ALIABDEQglgCgYgDgAhwhsQgDhVgEg3IgHhhIgBgYIgBgaQAWAPAZAMQAQAHAbAHIApAMQA5ARBBAJIAAA0QgQgCgPAKQgWAMgEAaQgCAHAAAJQgBATAJAQQAJAQAQAIQALAGANgCIADAAIABBOQh6gLh1gkg");
	this.shape.setTransform(15.8,55);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2D872D").s().p("Ah5BrIAAgDQAGhogChiIgCg4QB3AlB4ALIACEFQh6gLh5glg");
	this.shape_1.setTransform(16.5,59.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#339933").s().p("AjUCUIgBkFQAqADAqABIACADIAEAFIAJAJQAcAfAsAnQAqAlA8AqQAeAVARAJQAcAPAYAFQACAJAEAFQiLAfiGAAQg0AAg0gEgACJBNIgVgKQgMgGgPgKIgagTQgvgggugmIgigeQgBgDgCgCQgTgQgSgTQCcgBChgqIABA8QgBAfgCApIgGBFIgQAGIgPAHQgMAFgDASIgWgJg");
	this.shape_2.setTransform(50,60.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhhA+QgzgHgwgOIAhgFIAigEQA0gFBBAEQA7ADA6AMQAYAEAfANIgNACQg8AGg5AAQhBAAg+gJgACfAlQgcgKgXgGQgxgLhAgFQhAgFgvAEQhAAGgsAWIgHgCQgOgFgSgKIgfgQIgbgQQAtgnBKgKQAdgDAngBIBFABQCBABAwAFQBkALBHAlQAlAQACAOQABAIgRAIIgXAKQgdAKgvAHQgegOgSgHg");
	this.shape_3.setTransform(38.2,11.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFCC").s().p("AjCF8IgTgBIAAjEQC6ARC+gsIAEAGQAIAHAKgFIANgEQgCA5ACAqIACAnQACAYAAAPIguAKIguAIIg1ALQghAIgUADQgrAGg3ABQgpAAg7gEgAhxhRQgQgVgUgeQgRgagMgNIgFgFIAGgGQAMgQgCgYQAAgYgNgPQgNgQgTgDIgEgBIAAg0IA4AFQBZAIBrgLQAogFAagEQAlgGAegJQAfgJARgTIAAAWQgEAngDA1QgBAmABBFIABAuQihAqibABIgIgIgAjXhOIAAhPQAKgCAJgGIANAUQATAiAhAmQgqgBgqgEg");
	this.shape_4.setTransform(50.2,56.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#231F20").s().p("AhfHzIgMgBQhYgGhBgTQgrgLgXgKQgngSgNgZQgIgDgBgLQgHgpACg5IAFhjIABgJIAChdIABhkIAAhDIgBgjIgFhmQgEg9gBgsIgBgbQAAgRACgMIgBgBQgBg0A3ghQAggTA5gOQAjgJAsgCQAagCA0AAQB/AAA0AFQBmAJBJAhQAhAQASAPQAZAVAEAcIAAAEQAJALADAVQACAOAAAUIgBAtIgCAtIABBpIAAAwIgBA5IAGADQAEABADAFIAFAJQAHAOADANQAIAEAFALQADAJABAJQABAfgEAWQgFAdgOAUQgKAOgMAKIgOAJIgBASIgDA0QAAAcABAXIADAoQABAXgBAQQAFALgCAMQgCANgJADQgWAGgiAGIg5AJIg1ANQggAHgUADQg8AKhEAAQgmAAgpgDgAltlCIABAZIAHBhQAEA3ADBUIABA4QADBggGBqIAAADIgHBWQgDA2ADAkQAIACANAIIAUANQAjAPAlAKQAiAJAsAGQAYADAlACIATABQA7AEAnAAQA3gBAtgHQAUgDAhgHIA1gLIAugJIAugJQAAgPgCgYIgDgoQgCgpADg6IgNAFQgKAFgIgIIgEgFQgEgFgCgJQgYgFgcgPQgRgJgegVQg+gqgqgnQgsgngagfIgJgHIgEgFIgCgDQghglgTgjIgNgTQgKAGgKACIgDABQgNACgLgGQgQgJgJgQQgJgQABgSQAAgJACgIQAEgZAWgNQAPgJAQACIAFAAQATADAMAQQANAQABAYQACAYgNAQIgFAGIAEAEQANAOARAaQAUAeAQAUIAHAJQAQARATAQQACACABADIAiAeQAwAnAvAhIAaATQAPAKAMAGIAVAKIAWAJQADgSAMgFIAPgHIAQgGIAGhHQACgpABgfIgBg6IgBgvQgBhEABgnQACg0AEgnIABgWQgSATgfAJQgdAJglAGQgbAEgoAFQhsALhYgIIg4gGQhCgJg4gRIgrgMQgbgHgQgHQgZgLgWgQIABAagAFYB4QAEgJABgNIAAgXQAAgHADgIQACgKAGgFIgHgRIgDgFIgGBhgAiQldIghAEIghAFQAvAOA0AHQBwARCDgOIANgCQgegNgZgEQg5gMg8gDIgzgCQgjAAgfADgAgUl4QBBAFAxALQAXAGAbAKQATAHAeAOQAugHAegKIAXgKQAQgIgBgIQgBgOglgSQhHglhlgLQgwgFiBgBIhEgBQgoABgcADQhLAKgtAnIAbAQIAfASQATAKAOAFIAHACQArgWBAgGQAXgCAaAAQAdAAAhADg");
	this.shape_5.setTransform(39.6,50.2);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,79.3,100.5);


(lib.PaintOne = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E88B00").s().p("Ah5BrIAAgDQAGhogChiIgCg4QB3AlB4ALIACEFQh6gLh5glg");
	this.shape.setTransform(16.5,59.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EAEABB").s().p("ABEGHQgsgFgggJQglgKgjgQIgUgMQgNgIgIgDQgDgkADg1IAHhXQB3AlB7ALIABDEQglgCgYgDgAhwhsQgDhVgEg3IgHhhIgBgYIgBgaQAWAPAZAMQAQAHAbAHIApAMQA5ARBBAJIAAA0QgQgCgPAKQgWAMgEAaQgCAHAAAJQgBATAJAQQAJAQAQAIQALAGANgCIADAAIABBOQh6gLh1gkg");
	this.shape_1.setTransform(15.8,55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhhA+QgzgHgwgOIAhgFIAigEQA0gFBBAEQA7ADA6AMQAYAEAfANIgNACQg8AGg5AAQhBAAg+gJgACfAlQgcgKgXgGQgxgLhAgFQhAgFgvAEQhAAGgsAWIgHgCQgOgFgSgKIgfgQIgbgQQAtgnBKgKQAdgDAngBIBFABQCBABAwAFQBkALBHAlQAlAQACAOQABAIgRAIIgXAKQgdAKgvAHQgegOgSgHg");
	this.shape_2.setTransform(38.2,11.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF9900").s().p("AjUCUIgBkFQAqADAqABIACADIAEAFIAJAJQAcAfAsAnQAqAlA8AqQAeAVARAJQAcAPAYAFQACAJAEAFQiLAfiGAAQg0AAg0gEgACJBNIgVgKQgMgGgPgKIgagTQgvgggugmIgigeQgBgDgCgCQgTgQgSgTQCcgBChgqIABA8QgBAfgCApIgGBFIgQAGIgPAHQgMAFgDASIgWgJg");
	this.shape_3.setTransform(50,60.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFCC").s().p("AjCF8IgTgBIAAjEQC6ARC+gsIAEAGQAIAHAKgFIANgEQgCA5ACAqIACAnQACAYAAAPIguAKIguAIIg1ALQghAIgUADQgrAGg3ABQgpAAg7gEgAhxhRQgQgVgUgeQgRgagMgNIgFgFIAGgGQAMgQgCgYQAAgYgNgPQgNgQgTgDIgEgBIAAg0IA4AFQBZAIBrgLQAogFAagEQAlgGAegJQAfgJARgTIAAAWQgEAngDA1QgBAmABBFIABAuQihAqibABIgIgIgAjXhOIAAhPQAKgCAJgGIANAUQATAiAhAmQgqgBgqgEg");
	this.shape_4.setTransform(50.2,56.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#231F20").s().p("AhfHzIgMgBQhYgGhBgTQgrgLgXgKQgngSgNgZQgIgDgBgLQgHgpACg5IAFhjIABgJIAChdIABhkIAAhDIgBgjIgFhmQgEg9gBgsIgBgbQAAgRACgMIgBgBQgBg0A3ghQAggTA5gOQAjgJAsgCQAagCA0AAQB/AAA0AFQBmAJBJAhQAhAQASAPQAZAVAEAcIAAAEQAJALADAVQACAOAAAUIgBAtIgCAtIABBpIAAAwIgBA5IAGADQAEABADAFIAFAJQAHAOADANQAIAEAFALQADAJABAJQABAfgEAWQgFAdgOAUQgKAOgMAKIgOAJIgBASIgDA0QAAAcABAXIADAoQABAXgBAQQAFALgCAMQgCANgJADQgWAGgiAGIg5AJIg1ANQggAHgUADQg8AKhEAAQgmAAgpgDgAltlCIABAZIAHBhQAEA3ADBUIABA4QADBggGBqIAAADIgHBWQgDA2ADAkQAIACANAIIAUANQAjAPAlAKQAiAJAsAGQAYADAlACIATABQA7AEAnAAQA3gBAtgHQAUgDAhgHIA1gLIAugJIAugJQAAgPgCgYIgDgoQgCgpADg6IgNAFQgKAFgIgIIgEgFQgEgFgCgJQgYgFgcgPQgRgJgegVQg+gqgqgnQgsgngagfIgJgHIgEgFIgCgDQghglgTgjIgNgTQgKAGgKACIgDABQgNACgLgGQgQgJgJgQQgJgQABgSQAAgJACgIQAEgZAWgNQAPgJAQACIAFAAQATADAMAQQANAQABAYQACAYgNAQIgFAGIAEAEQANAOARAaQAUAeAQAUIAHAJQAQARATAQQACACABADIAiAeQAwAnAvAhIAaATQAPAKAMAGIAVAKIAWAJQADgSAMgFIAPgHIAQgGIAGhHQACgpABgfIgBg6IgBgvQgBhEABgnQACg0AEgnIABgWQgSATgfAJQgdAJglAGQgbAEgoAFQhsALhYgIIg4gGQhCgJg4gRIgrgMQgbgHgQgHQgZgLgWgQIABAagAFYB4QAEgJABgNIAAgXQAAgHADgIQACgKAGgFIgHgRIgDgFIgGBhgAiQldIghAEIghAFQAvAOA0AHQBwARCDgOIANgCQgegNgZgEQg5gMg8gDIgzgCQgjAAgfADgAgUl4QBBAFAxALQAXAGAbAKQATAHAeAOQAugHAegKIAXgKQAQgIgBgIQgBgOglgSQhHglhlgLQgwgFiBgBIhEgBQgoABgcADQhLAKgtAnIAbAQIAfASQATAKAOAFIAHACQArgWBAgGQAXgCAaAAQAdAAAhADg");
	this.shape_5.setTransform(39.6,50.2);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,79.3,100.5);


(lib.Paintbrush = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AhEA2QgKgIAAgDQAAgKAKAAIAZAFIAZAEQAiAAAWgXQAPgPAEgRQAWhSAAA9QAAAjgWAbQgaAhgwAAg");
	this.shape.setTransform(39,19.9);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#56381C").ss(2,1,1).p("AAdAAQAAAKgJAHQgIAIgMAAQgKAAgJgIQgHgGgCgIQAAgCAAgBQAAgJAJgHIAAAAQAJgIAKAAQAMAAAIAIQAJAHAAAJg");
	this.shape_1.setTransform(30.9,24.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C5C998").s().p("AgeBgIAAgCIAAgDIAAgBIgBgIIAAgHIgBgRIgCgTIgGhlQAAgLAKgLIAEgEQAcgFAngEQgrAVASCoIgbADIgTADIAAgCg");
	this.shape_2.setTransform(9.6,90.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7F552A").s().p("Ah0E9IABgCIgCgEIATgIIBphJIABgBIAEgFQArgzAAhAIgQhUQgPhXAAhTQAAgcAIghIAKgmIAEgOIAGgQIAzg7QARgFANAOIgQAUQgTAZgMAcIAAABQgJAHAAAKIAAAEQgIAYgFAcQgEAYAAAtQAAA1AIAZQAQA2AFAmQADAlgDAuQgFAxgqA1QgqA1g0AZQgHgBgEACQgnAEgeAFQALgLAFgHg");
	this.shape_3.setTransform(20.3,48.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3F2913").s().p("Aj5EpIAAAAIgLgCIADAAQgHgHgFgsQgJhEAUhkQARhWgDgdIgBgFQgCgJgBgOIAAgLQAAgZAEgnQAIhLAAgSQAAgKACgIIAEgMQAEgMAJgGIgBgDIAeAIQgEAHgLAMIgEADQgLALAAALIAHBnIABATIABARIABAHIAAAJIAAABIAAACIABACIAAACIAAABIABAUIAAAGIABAZIAAACIAAAEIAAAHIAAACIgBAFIAAACIgDAPIgEAUIgLA6QgUBfAAAtQAAAXABANIAQACIABAAIABAAIARACIADAAIAEAAIAVABIAEAAIAVAAIAFAAIASAAIAEAAIAVAAIADAAIAUAAIAEAAIAQAAIADAAIAUABIADAAIAKAAIADAAIATAAIADAAIAQAAIAEAAIAPAAIAEAAIATABIADAAIAIAAIADAAIAQAAIAAAAIABAAIACAAIATABIAEAAIAOABIArACQARAAANgCIABAAIAEAAIALgDIACAAIABgBIABAAIAIgEIAHgDIAHgGQAMgLACgSQACgPgFgWIgGgbIAAh2IAAgCIAAgCIAAgBIAAgCIgEhYIgBgfIAAgFIgBgLIAAgDIAAgCIgBgLIgHiEIAAgCIABgnIAAgLIAAgFIABgFIAMgDIABAGIgDABQAGAAADAFIABAGQACAQAIB2QADAlAAAkIABAlQAAAygFAtIAJBuQAFBmgKARQgKARgwAAQgwAAghgDIgngCIkpAJgAj8EmIADgBIADgBQALgDgOgBIgDAGg");
	this.shape_4.setTransform(27.4,107.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3F2A14").s().p("AjnFUQAAgRA/gjQBAgjAUgaQAVgagFgvIgLhpQgHhDgDhbQgEhcAihCQAihDAmgKQAkgKAZAkIAGAKQAbAwADBJQACBKgPBmQgQBkgEAnQgFAmANAgQANAfgBALQgBAMBEAeQBEAdAAALIAAABIgNADIAAgDQgrgLgzgYQgOgGgTgNQgTgOgHgLQgGgKgGglQgEgiAAgPQAAg/APhiQAQhjAAgtQAAgwgJgkQgJglgPgQQgMgNgSAFIgxA7IgFAPIgFAOIgKAmQgHAigBAbQAABTAQBXIAQBUQAABAgrA0IgEAEIgCACIhqBJIgUAIIACADIgBACIgegHg");
	this.shape_5.setTransform(28.7,45.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#DDCCB2").s().p("AB4CzIgOgBQgDgIgBgKIAAgLIABgVIACgVQADgaACg0IAFhmIABg1IAAgHIAAgTIAAgJIAAgQIARABIAAAQIACAbQABAdAAAPIgDAvIgEAtQgGA8AAAgIAAAdQAAATADASIACASIAFAAIgDgSQgDgSAAgTQgBgPAAgPQABghAGg8IADgtQADgbAAgUQABgPgCgbIAAgBIgBgZIgBgRIAJAAIAAASIABAWIAAADIABAiIgBAuIgBAuIAGBcIACAsIAAAeIgBATQAAABAAAAQAAABABAAQAAAAAAAAQAAAAABgBQAAAAABgBQAAAAAAAAQABgBAAAAQAAgBAAAAIABgRIAAgdIgCgsIgFhZIAAgtIABguIgBgnIAAgBIgBgYIAAgRIARABIABATIABAWIAAArIADBaIACBaIABAwIAAAQIgBAZIAEAAIABgbIAAgLIAAg2IgDhcIgCgrIgBgzIAAgMIAAgWIAAgDIgBgSIgBgVIAFAAIAQgBIgBAYIAAANIAAAlQgCAlABA4IABAzIABAqIAAA4IAAAEIAAAiQgFAAgDABIAKAAQgNACgRAAIgrgCgACaCzIAGAAIgCAAIgEAAgADDCOIAAgSIAAghIAAgTIgChGQgBg1ABgqIgBgpIABgRQAAgOABgLIAPAAIAAAfIAABEIABA4IABApIABAxQAAAdgCAVQgCAUAAAOIABAXIgLADIgEAAIABglgABTCxIgCgTIgBgcIABgfIAFhaIAFhcIACgyIAAgdIAAgRIAVACIgBAQIgBAcIAAAEIAAAwIgCA6IgDA4QgCAogEAzIgBAFQgBAVAAAKQABAKACAIIgTgBgABACxIgCgXIgCgYIgBgpQgBgoADg0IAFhjIADgfIAAgIIABgVIAAAAIAAgBIgBgQIAUAAIABARIgBAdIAAABIgCAuQgDAqgBA/IgFBUIAAAbQgBAOABANIACAUgAA1CxIgBgZIgCgWQgDg/ABhDIADhjIAAgiIAAgGIAAgXIAAgBIAAgBIABgQIAOABIABAQIAAABIAAAAIAAAaIAAADIgEAoIgFBhQgDAzABAoIABAjIACAXIACAYgAAfCwIgCgdIgBgRIAAggQAAgmACg3IADhcIACguIABgdIAAgBIAAgBIACgQIALAAIgBAQIAAABIAAABIAAAVIAAAIIgBAfQgCA0AAAyQgBBEADA+IABAVIACAagAAMCwIACggIAAgOIABgfIgBheIgChfIACgsIAAgBIACgbIAAgCIABgQIARAAIgBAQIAAABIAAABIgCAdIgCAvIgDBdQgCA1AAAmIAAAgIABAQIACAegAgICwIADgjIABgLIABgZQAAgigDg6QgDg5AAgmQAAgbACgZIACgcIABgSIARAAIgBAQIAAACIgCAZIAAADQgCAZAAAOIABBfIABBeIAAAkIAAANIgCAhgAgeCwQgBgRAAgTIAAgJIABgcIAEhcIAAgxIAAg0IAEgsIAAgBIACgbIABgSIAMAAIgBASIgCAcIgCAtQAAAoACAyQAEBHgBAbIAAAfIAAAKIgEAkgAgrCwIgBghIAAgMQgBhDABg3QAAgpACg0IACgwIAAgCIABgcIABgRIAQgBIgBASIgBAGIgCAWIAAABIgDA0IAAA2IAAA1IgDBXIAAASIgBAJIAAAkgAhCCvIAAgWIABgWIAAgfIAFhdQACg1AAgoIgBgvIAAgBQgBgPABgNQAAgJACgIIARAAIgBARIgBAXIAAAFIgCAkQgCAvgBA0QgCA6ACBIIAAANIABAggADTCQIAEgqQABgRgBgZIgCgsIAAg0IgBgrIAAhBIAAggIAOAFIAAAAIADApIADArQADAogCA6IgDBgIgCAqIAAATIgHADIgIAEQgCgOAAgRgAhVCvIABgXIAAgTIACiCIAAhaQAAgaACgVIADgcIADgRIAMAAIgCARIAAAZIAAADIABAmQABAlgDA+IgEBhIgBAfIAAAWIABAWgAhtCvIgCgeIAAgJIAAgmQAAgmAEg4IAFhbIAEgxIACgaIABgRIARAAIgCARQgCANgBAOQgCAUAAAXIAABXQAABPgCA7IgBASIgBAYgAiFCvIAAgkIAAgCQAAg+AFhFIAIhiIADgqIAAgCIABgaIAAgPIATgBIgBAQIgCAaIgEA3IgHBfQgDA1AAAmIAAAfIABAIQAAAQABAPgAibCvIACgnIAMABIgMgCIAHh/IAGhbIABgSQABgjgDgcIgBgNIAWAAIAAAPIgBAaQgBAcgCASIgIBhQgFBFAAA9IAAABIAAAlgAi1CvIABgnIAWAAIgCAngAjOCuQgCgZACgeIAAgPQACgeAIg+QAHg/ACggIABguIACgiIAAgLIAPgCIgBALQgBANABASQABAfAAALQgCAUgEAfIgEAfIgDASQgCAVAAAdIAAAwIAAAZIgDgBIAAAGIADAAIgBARIAAAWgAjVCuIgRgCIAAgDIAEgaQACgPACghIAEguIAAgDQAGgvACgtIAAgTIADgvIAAgHIABglIgBgQIAUgCIgBALIgBAkIAAAXIAAAFIgBASQgBAXgDAcIgGAxIgJBZIgBAHQgBAgABAbgAj4CqQgBgNAAgXQAAgtAUhdIALg6IAEgUIADgPIgBAZIgBAiIgFA8IAAAFIgFAlIgDAqQgBAdgBAQQgBALgDAKIgQgCgADrivIABAfIAEBaIAAACIAAABIAAACIAAACIAAB0IAGAbQAFAWgCAPQgCASgMALgAioCFIgMgBIAAgWIAAg2QAAgfADgWIADgaIADgQIAFgsQABgOgCghQAAgVABgOIAAgGIABgFIARAAIACAMQACAPAAAUQAAASgCAeIAAAAIgGBgIgGB3IgKgBgAjTiSIAAgGIgBgUIACAAIABAAIAAASIgBAfIAAACIgBgZg");
	this.shape_6.setTransform(27.9,118);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#231F20").s().p("Aj0C6IAAAAIADABIgDABgACiCuIACgTIgBgeIgCgsIgFhbIABguIAAgvIAAgiIgBgDIgBgWIAAgRIgIgBIAAARIACAaIAAAAQABAbgBAPQAAAUgCAbIgEAvQgGA7AAAgQgBAQABAOQAAAUADARIADATIgEAAIgDgTQgCgRgBgUIAAgdQABgfAFg8IAEgtIADgwQAAgPgBgdIgCgaIAAgRIgRAAIAAAPIAAAKIAAASIAAAHIgBA1IgFBmQgCA0gDAbIgBAVIgCAUIABALQAAALADAIIgEAAQgCgJAAgKQgBgKACgVIAAgFQAFgzABgoIADg4IACg6IAAgwIAAgEIABgcIABgQIgVgCIABARIAAAdIgDAyIgFBdIgFBZIgBAfIACAcIABATIgBAAIgCAAIAAAAIgCgUQAAgNAAgOIAAgbIAFhUQABg/ADgqIACguIAAgBIABgdIgBgRIgUAAIABAQIAAABIAAAAIgBAWIAAAHIgDAfIgFBjQgCA0AAAoIABApIACAYIADAXIgEAAIgCgXIgBgYIgBgjQgBgoACgzIAGhhIADgoIAAgDIAAgaIAAAAIAAgBIgBgQIgOgBIgBAQIAAABIAAACIAAAWIAAAGIAAAjIgCBjQgBBDACA+IACAWIABAZIgDAAIgBgaIgBgVQgDg9AAhCQABg1ACg0IAAgfIAAgIIAAgUIAAgCIAAgBIABgQIgLAAIgBAQIAAABIAAACIgCAcIgCAuIgDBcQgCA3AAAnIABAfIAAARIACAdIgEAAIgCgeIgBgQIAAgfQAAgnACg1IADhdIACgvIACgcIAAgCIAAgBIABgQIgRAAIgBAQIAAADIgCAaIAAACIgBArIABBfIABBeIgBAfIAAAOIgBAgIgFAAIACghIAAgNIABgkIgBhbIgChiQAAgOACgYIAAgEIACgYIAAgDIABgQIgSAAIgCATIgCAcQgBAYAAAbQAAAmADA5QACA6AAAiIgBAZIAAALIgEAjIgBAAIACgkIABgKIAAgfQAAgbgChFQgCg0AAgoIACgsIAAgcIACgTIgLAAIgBATIgCAaIAAACIgEArIAAA0IAAAxIgDBcIgBAcIAAAJQgBAUABAQIgDAAIABgkIAAgJIABgSIAChWIAAg1IAAg3IADg0IAAAAIADgXIAAgFIACgTIgRABIAAASIgBAcIAAABIgCAwQgDA0AAAqQgBA2ABBDIAAAMIACAhIgEAAIgBggIAAgNQgChIACg3QABg3ACguIACgkIAAgGIABgWIACgSIgSAAQgCAIABAKQgBAMAAAPIAAABIABAvQAAAogCA1IgFBdIAAAfIgBAWIAAAWIgDAAIAAgVIAAgXIAAgfIAEhgQADg+gBglIgBgnIAAgDIAAgYIACgSIgMAAIgDASIgDAbQgCAVAAAaIAABbIgBCBIgBAUIgBAWIgEAAIACgXIAAgTQACg7ABhPIAAhXQAAgXABgUQABgOACgMIADgSIgRABIgCARIgCAZIgEAyIgFBaQgDA4gBAmIAAAmIAAAKIACAdIgCAAQgCgPAAgQIAAgHIgBggQAAgmADg1IAHhfIAEg3IACgZIACgRIgUABIAAAQIgBAZIAAACIgCAqIgJBiQgEBGgBA9IAAACIAAAkIgEAAIAAglIAAgBQAAg9AFhFIAIhhQACgSABgcIABgaIABgPIgXABIACAMQACAcAAAjIgBASIgHBcIgHB+IAAACIgCAmIgEAAIABgmIABgDIAFh3IAGhgIAAAAQACgeAAgSQAAgTgCgQIgBgMIgSAAIAAAFIgBAGQgBAOABAVQABAiAAANIgGAsIgDAQIgDAbQgDAVAAAgIAAA2IAAAVIAAAEIgBAnIgEAAIABgWIAAgRIAAgFIAAgZIAAgwQAAgdACgUIADgTIAEgfQAFgfABgUQAAgLgBgfQgBgSABgNIABgKIgOABIgBALIgBAiIgBAuQgDAggHA/QgHA+gDAeIAAAQQgBAdABAZIgDAAQgCgbACgfIAAgIIAJhZIAGgxQADgcABgXIABgRIAAgGIAAgXIABgkIABgLIgUACIABAQIgBAlIAAAHIgCAvIgBATQgCAtgGAvIAAADIgEAuQgCAigCAOIgDAaIgBADIgBAAIAAAAQACgKABgLQACgQAAgdIAEgqIAEgkIAAgGIAFg8IABgiIACgYIAAgCIABgHIAAgDIAAgGIAAgFIAAgCIAAgBIAAggIAAgSIAAAAIgDAAIAAgBIATgDIAcgCQBJgFCvAAQAzAABlADIAAAFIAAFSIgGAFIAAgTIABgpIADhgQACg7gDgoIgDgrIgCgpIAAAAIgPgEIAAAgIAABBIABArIAAAzIACAsQABAZgBARIgEAqQAAASACANIgBAAIgBABIgBAAIgBgXQAAgNACgVQACgVgBgdIgBgxIAAgpIgBg3IgBhFIAAgfIgPAAQgBALAAAPIAAAQIAAApQgBArABA2IACBFIAAATIAAAgIAAASIgBAmIAAAAIgKAAQACgCAFABIABgiIAAgFIgBg4IAAgqIgBgzQgCg4ACglIAAglIABgMIABgZIgRABIgFAAIACAVIABASIAAADIAAAWIAAANIABAyIABArIADBcIAAA2IAAAMIgBAbIgEAAIABgaIAAgQIAAgvIgDhYIgDhdIAAgrIgBgWIgBgTIgRgBIAAARIABAYIAAABIABAnIgBAuIAAAtIAGBZIABAsIAAAdIgBARQAAAAAAABQAAAAgBABQAAAAAAABQgBAAAAAAIgBABIgBgCgAClCwIADgBIACABg");
	this.shape_7.setTransform(26.9,118.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#DEE3AC").s().p("AirhUIALAAIA2gFQBpgIDFgIIgBALIAAAoIAAABIAHCCIAAALIAAACIABADIAAALQhlgDgzAAQivAAhIAFQgUioAtgWg");
	this.shape_8.setTransform(31,89.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#996633").s().p("AisFEQA2gYAqg1QAqg1AFgyQAEgtgEglQgFgmgQg2QgIgaAAg0QAAgtAEgZQAFgbAIgZQACAJAHAFQAJAIAMAAQALAAAHgIQAJgHAAgKQAAgKgJgIQgHgHgLAAQgMAAgJAHQANgdASgZIAOgTQAOAPAKAlQAJAkAAAwQAAAtgQBjQgPBiAAA/QAAAPAEAiQAGAlAGAKQAHALATAOQASANAOAGQA0AYArALIAAAIIAAAEQjFAIhpAIIg3AFIgKAAQAFgCAGABg");
	this.shape_9.setTransform(32.2,48.3);

	this.addChild(this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,10.4,54.7,127.9);


(lib.Options_btn = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAeCrIAAiVICVAAIAACVgAiyCrIAAiVICVAAIAACVgAAegVIAAiVICVAAIAACVgAiygVIAAiVICVAAIAACVg");
	this.shape.setTransform(35.8,35.8);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4,1,1).p("AjtFBQAOAFANAEQAwAOA1AAQCPAABmhmQBmhmAAiPQAAiQhmhmQhIhIhagVQgBAAgCgB");
	this.shape_1.setTransform(45.9,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(4,1,1).p("ADWlHQgmgJgqAAQiPAAhmBmQhmBmAACPQAACQBmBnQA2A1A/Aa");
	this.shape_2.setTransform(21.4,33.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F4D96F").s().p("AhEFMQhBgag1g1QhmhmAAiRQAAiOBmhmQBmhmCOAAQAqAAAmAIIADABQBeBeAjBnQAjBngcBfQgcBhhXBJQhSBFh5AmIgbgJg");
	this.shape_3.setTransform(29,34.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("AjfFKQB7gnBShFQBVhJAchhQAchegjhnQgjhohchdQBaAVBHBHQBmBmAACRQAACOhmBmQhmBmiOAAQg1AAgwgNg");
	this.shape_4.setTransform(47.2,35.3);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.OK_btn = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("ok", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 104;
	this.text.setTransform(61.9,15.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AnvkbIPfAAQCOAAAACNIAAEdQAACNiOAAIvfAAQiOAAAAiNIAAkdQAAiNCOAAg");
	this.shape.setTransform(63.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("AnvEcQiOAAAAiNIAAkdQAAiNCOAAIPfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_1.setTransform(63.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AnvEcQiOAAAAiNIAAkdQAAiNCOAAIPfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_2.setTransform(72.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,138.2,67.5);


(lib.No_Btn = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("no", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 112;
	this.text.setTransform(79.2,13.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AqjkbIVHAAQCOAAAACNIAAEdQAACNiOAAI1HAAQiOAAAAiNIAAkdQAAiNCOAAg");
	this.shape.setTransform(81.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("AqjEcQiOAAAAiNIAAkdQAAiNCOAAIVHAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_1.setTransform(81.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AqjEcQiOAAAAiNIAAkdQAAiNCOAAIVHAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_2.setTransform(90.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.NikSpeechStretch2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EAkOgJWIAAStEgkNAJXIAAyt");
	this.shape.setTransform(231.8,60);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("EgkNAJXIAAytMBIbAAAIAAStg");
	this.shape_1.setTransform(231.8,60);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AgmJXIAAytIBOAAIAAStg");
	this.shape_2.setTransform(467.7,60);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,473.7,124);


(lib.NikSpeechBase2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EAkOgBSIAAAaQAACLiOAAMhD/AAAQgiAAgZgFQgbgFgRgLQgjgZgDg7IgBgHIAAg1");
	this.shape.setTransform(231.8,8.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("Egh/ABTQgiAAgZgFQgbgFgRgLQgjgZgDg7IgBgHIAAg1MBIbAAAIAAAaQAACLiOAAg");
	this.shape_1.setTransform(231.8,8.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("EgiBAB7QhHAAgfgVQgdgUgHgsQAZAFAhAAMBD/AAAQCOAAAAiLIAAgaIBQAAIAABqQAACLiOAAg");
	this.shape_2.setTransform(240,12.4);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,473.7,26.8);


(lib.Nik_SpeechStretch = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EAkOgEhIAAJDEgkNAEiIAApD");
	this.shape.setTransform(231.8,29);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("EgkNAEhIAApCMBIbAAAIAAJCg");
	this.shape_1.setTransform(231.8,29);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AgwEhIAApCIBiAAIAAJCg");
	this.shape_2.setTransform(468.7,29);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,475.7,62);


(lib.Nik_SpeechBubbleBase = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EAkOgBcIAAAuQAACLiOAAMhD/AAAQgXAAgSgCQgogFgWgOQgjgZgDg7QgBgDAAgEIAAhJ");
	this.shape.setTransform(231.8,9.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("Egh/ABdQgXAAgSgCQgogFgWgOQgjgZgDg7IgBgHIAAhJMBIbAAAIAAAuQAACLiOAAg");
	this.shape_1.setTransform(231.8,9.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("EgiAACPQhHAAgfgVQgigYgEg5QASACAWAAMBD/AAAQCOAAAAiLIAAguIBkAAIAACQQAACNiOAAg");
	this.shape_2.setTransform(241.9,14.4);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,475.7,30.8);


(lib.NewMessageStatic = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("New message!", "25px 'Arial'");
	this.text.textAlign = "center";
	this.text.lineHeight = 31;
	this.text.lineWidth = 185;
	this.text.setTransform(102.5,13.2);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("APtgFIAAALIAABTQAABQhQAAI85AAQhQAAAAhQIAAhTIAAgLIAAgDIAAgEIAAhMQAAhQBQAAIc5AAQBQAAAABQIAABMIAAAE");
	this.shape.setTransform(104.5,26.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D3BB61").s().p("AucCpQhQAAAAhQIAAhTIAAgLIAAgDIAAgEIAAhMQAAhQBQAAIc5AAQBQAAAABQIAABMIAAAEIAAADIAAALIAABTQAABQhQAAg");
	this.shape_1.setTransform(104.5,26.1);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,8.2,203,38.9);


(lib.NewMessage = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.text = new cjs.Text("New message!", "25px 'Arial'");
	this.text.textAlign = "center";
	this.text.lineHeight = 31;
	this.text.lineWidth = 185;
	this.text.setTransform(102.5,13.2);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("APtgFIAAALIAABTQAABQhQAAI85AAQhQAAAAhQIAAhTIAAgLIAAgDIAAgEIAAhMQAAhQBQAAIc5AAQBQAAAABQIAABMIAAAE");
	this.shape.setTransform(104.5,26.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D3BB61").s().p("AucCpQhQAAAAhQIAAhTIAAgLIAAgDIAAgEIAAhMQAAhQBQAAIc5AAQBQAAAABQIAABMIAAAEIAAADIAAALIAABTQAABQhQAAg");
	this.shape_1.setTransform(104.5,26.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).to({state:[]},4).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3,8.2,203,38.9);


(lib.Male_Head_01_Front = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AjSAqQgLgLAAgMQAAgGABgEIg/ABQgSAEgaAIQgTAGgKAAQgLAAgJgBIgGgCIgDgRIAEgKQACgHAGgFQASgLBKgIQA9gQB+ANQAWATABAEQAAADgJAIQgIAFgGAEIgnABQACAFAAAHQAAAVgNAKQgKAHgPAAQgZAAgQgQgAD/AZQgVgEgJgJQgCgCgBgDIgCgEIACgFIgFgBQgKgCgKgEIgIgDIAAgDIgBgBQgBgCACgLQAFggBbAEQAoACA5AJIACAAIABAFQACAEgCALQgCAJg3ALIgVAEIgCAAIAAACQgFAageAAIgPgBg");
	this.shape.setTransform(96.2,88.1);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EACFB7").s().p("AjCAwIgLgCQAAgBgEgDQgDgCAAgSIh5AKQgPABgKgCIgIgCQAAAAAAgBQAAAAAAAAQgBAAAAABQgBAAAAABQgDACgBgOQgBgOAFgEQAHgHAdgFQAtgIDZgQIAqgEQAAABBIgBICTgFQAzgDAkACQAyACAWAIQAXAIAAAQQAAAOgMADIgWAAIgOAAQgBAXgCAHIgdAGQgsgCgMgQQgDgDgBgUIghgCQkEAHgKgEIgBAAIgxAJQgBAOgLALQgNALgUACIgPAAIgOAAg");
	this.shape_1.setTransform(94.6,87.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DEBBA4").s().p("ADVJFQhIgbhMhLQgdgcgTgsIgZg6IAKgHQATgMAAgXQAAgMgBgGQgHgOgSAAQgLAAgiAMQghAKgMABQgngBgmgTQg1gdgjhCQgDgGAAgKIABgTQAAgkAagUQAKgHAHgIQAfAIAcAPQATALAaAAQAKAAAHgCQAIgFABgNQAkAXArAFQArAEgKgbQgnh5gSicQgTikAVhoQARhdAkgyIAFgGQBwCWAVCyQAOBsAHD6QATCbAZCEQAZCEAPBbQgbgHgYgKg");
	this.shape_2.setTransform(30.8,89.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#AA8462").s().p("ABhGpQgrgFgkgXIAAAAQAAgRgZgTQgXgQgkgKQgRgFgNgBQgnjbAAgKQAAhHAPhUQAWh/AwhiQAqhXA4g5QArA4AgA2IANAVIgBACIgFAFQgmAygRBdQgVBoATCiQASCcApB7QAJAXgfAAIgNAAg");
	this.shape_3.setTransform(19.9,58.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CEA077").s().p("AkkBDQgsgLgkArIgMgVQghg2grg2QCFiGDcAVID+AAQDIAtByBiQgjCNjYAGIgTAAQisAAk3hQg");
	this.shape_4.setTransform(70.8,19.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgKMJQg8gJgsgNQhsgghKg0Qh0hQgjiCIAAgBIgiABQhlAAg5hEQgogvgCgvIgJgFQgDgVAAgWQAAgsAFgTQAKgqAggKQgLgegKhqIgIhmQAAjkBXi1QAuhfBAg+QCRiNDxAXIAAgEID1AAQECAuCHCOQA8A/AjBjQAVA8AKA1QgDAAALAsQANAwABBvQAAB8gVA+QgDALACAmQACArgBAGQgIA6gMAvQgTBGgkBUQgDAHgEAbIgGAkQgIB1hLBUQhMBXhvAKIjPAEQhRgJglgFgAltGIQACAFAAAMQAAAYgTALIgNAHIAbA6QAUAsAdAdQBLBKBIAcQAYAJAbAIQCDAkDfgBQBmAABLgvQBlhAASiFIAAgCQgfAFgpAKQgsALggAGQhNAMhWgDQg4gChagOIhAgXQAAgdA0AHQCUAYCdgYQAwgHAjgJQAjgJAugSQAHgDALgYIAohaQAWgyAIgwQAHgqAAg5QAAgOACgJQgCgHAAgGQAKhOAFhPQAKidgWhKQgOgygZg9Qg0h4g9g2QhyhkjJgtIj+AAQjbgViFCGQg6A5gqBXQgwBigVB/QgPBWAABHQAAAKAmDZQANABARAFQAkAKAXAQQAbATAAARIAAAAQAAANgJAFQgGACgLAAQgZAAgUgLQgcgPgfgHQgGAHgKAIQgbATAAAlIgBATQAAAKADAFQAjBCA2AdQAlAUAnAAQAMAAAhgLQAigMALAAQAUAAAHAPgACyFPQgMgLgBgQIACgUQAAgQAKgVQALgZAQACQAQACAAAQIAAAqIA2AZICKAAQASgGAFgPQADgKgBgcQAAgrAZAPQAZAOAAAnQAAAmgQASQgfAhhgAAQiEAAgighg");
	this.shape_5.setTransform(69.4,79.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EAD0B7").s().p("AkGJ3QgPhcgZiDQgZiEgTicQgHj5gNhsQgWiyhwiWIABgBQAkgrAsALQFMBUCqgFQDYgGAjiNQA9A2A0B5QAZA8AOAzQAWBKgKCcQgFBQgKBNQAAAHACAGQgCAJAAAOQAAA6gHAqQgIAvgWAzIgoBaQgLAYgHADQguARgjAJQgjAKgwAHQidAXiSgYQg0gHAAAeIBAAXQBYAOA4ACQBWADBNgNQAggFAsgLQApgLAfgEIAAACQgSCFhlA/QhLAvhmABIgMAAQjVAAiBgkgAAkC1QgKAVAAARIgCAUQABAPAMALQAiAhCEAAQBgAAAfghQAQgRAAgnQAAgmgZgPQgZgOAAAqQABAdgDAJQgFAPgSAHIiKAAIg2gZIAAgqQAAgRgQgCIgCAAQgOAAgLAXgAhiA4QADADAAABIALACQAPABAOgBQAUgCANgLQAMgLABgOIAvgJIAAAAQAKAEEGgJIAhADQABAVADADQAMAQAtACIAcgGQADgHABgXIAOAAIAVAAQAMgDAAgQQAAgOgWgIQgWgIgzgCQgkgCgyADIiTAFQhKABAAgBIgrAEQjXAQgsAGQgdAFgIAIQgEAFABAOQABAOACgCQABgBAAAAQABAAAAAAQAAAAABAAQAAAAAAAAIAHACQALACAOgBIB6gKQAAASADACg");
	this.shape_6.setTransform(83.5,86.1);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,138.9,158.3);


(lib.Male_Greg_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BC9A72").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#44446F").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_1.setTransform(18,46.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6666A8").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_2.setTransform(85.2,58);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D5AE81").s().p("ArABdIgygRIABgHQAHgpABgWIDpAAIADBBQgBBDggAAQgdAAiFgtgAJQgNIgPhBQAhgKAxggIAZgRQAIAOAZAhQAmAyAAArQAAAXhaAIQgoAEgWAGIgLg5g");
	this.shape_3.setTransform(88.9,83);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_4.setTransform(86,143.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_5.setTransform(67.5,124.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_6.setTransform(86.9,128);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_7.setTransform(94.8,140.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.Male_Frank_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A4D30").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#975F3C").s().p("ArABdIgygRIABgHQAHgpABgWIDpAAIADBBQgBBDggAAQgdAAiFgtgAJQgNIgPhBQAhgKAxggIAZgRQAIAOAZAhQAmAyAAArQAAAXhaAIQgoAEgWAGIgLg5g");
	this.shape_1.setTransform(88.9,83);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#70656E").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_2.setTransform(18,46.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#91838F").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_3.setTransform(85.2,58);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_4.setTransform(86,143.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_5.setTransform(67.5,124.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_6.setTransform(86.9,128);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_7.setTransform(94.8,140.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.Male_Body_Frank_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4181C0").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape.setTransform(18,46.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#478DD3").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_1.setTransform(85.2,58);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_2.setTransform(86,143.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_3.setTransform(67.5,124.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_4.setTransform(9.8,86.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_5.setTransform(86.9,128);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_6.setTransform(94.8,140.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EAD0B7").s().p("ArABdIgygRIABgHQAHgpABgWIDpAAIADBBQgBBDggAAQgdAAiFgtgAJQgNIgPhBQAhgKAxggIAZgRQAIAOAZAhQAmAyAAArQAAAXhaAIQgoAEgWAGIgLg5g");
	this.shape_7.setTransform(88.9,83);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.Male_Body_02_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#497C59").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape.setTransform(18,46.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5A996E").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_1.setTransform(85.2,58);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_2.setTransform(86,143.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_3.setTransform(67.5,124.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_4.setTransform(9.8,86.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_5.setTransform(86.9,128);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_6.setTransform(94.8,140.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EAD0B7").s().p("ArABdIgygRIABgHQAHgpABgWIDpAAIADBBQgBBDggAAQgdAAiFgtgAJQgNIgPhBQAhgKAxggIAZgRQAIAOAZAhQAmAyAAArQAAAXhaAIQgoAEgWAGIgLg5g");
	this.shape_7.setTransform(88.9,83);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.LucyHead = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC0066").s().p("AgmAMIg6geQBFgNB8AfIgcAMQgdALgbAAQgaAAgZgLg");
	this.shape.setTransform(89.7,104.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DDBAA4").s().p("ACQHdQhsgsh6g9IALgaQATgugBgWQAAgDgYgWQgcAPgHAkQgHAlgNAHQjEh8gBgiQAAgOAQgZQAQgYAAgHQAAgMgEgDIgDgGQgWAGgPAHQgDgYAAguQAAhTAkhpQAoh5BJhjQBThwBqhAQApBXAkBPQBdDVAVB1QhQASABBMQAAAMACACIADAEQAQACATgCIApgIIAKgCQASB8AeBkQAjBvBJCYQhhgWhsgsg");
	this.shape_1.setTransform(38.2,62.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ADeFPQjaAAjehuQhQgng7gqQgcgVgMgMQgLALgMAAQhPAAhfhhQgighgaglQgXgiAAgKQAAgtAFgJQAHgNAYgLQAPgHAWgGIADAFQADAEAAAMQAAAHgPAYQgQAZAAAOQAAAiDFB6QANgHAHglQAHgiAcgPQAYAWAAADQAAAUgSAuIgMAZQB9A+BsAsQBrAsBgAWQBaAUBSACQBuACBchSQAXgUAugxQAngsAWgQQAFgEAEgBQAFgBADADQAEACABAEIADAFQADAEAHgFQAIgGgkA3QgjA3haBKQhaBLh/AAIAAAAgABbCpQg9gqAAgTQAAguBvACQApABDOAZQAYADAAAZQAAASgvAkQg8AthHAAQhHAAhIgwgAB5B2IA5AgQA0AVA5gVIAcgOQhYgWg8AAQgaAAgUAEgADWATQg8gJgSgUQgJgKAAgXQAAg0AmAYQAqAlALAEQA9APA9gPIAAgSIAIgFQAIgFAMAAQANAAAGACIADACIADAFQADAEAAAMQAAAOgQARIgYAVQggAFgkAAQgjAAgngFgAIPi3QgPgBgSgJQgWgLgPgRQgOgPgJgEQgQgHgdAEQgMADgLAEQgHgCgEgEIgDgFIgCgEIgCgDIADgFQAagpBpACQBhACA4AeQAVALAMARQALAPgBAIIgBAHIAAABIgBAAQgLgGgQgDIgOgBIgIgBQgJABgHADIgOAFQgGACgPALQgMAJgKACQgLADgKAAIgGgBgAhDjeQgIgEgaADIgKABIgpAIQgUACgPgCIgDgDQgDgDAAgLQAAhOBPgSQAigJAvADQBwAGCYA5QANAFgPAKQgPAJgZADIgbAAIggAAQgdAiguAFQgRACgQAAQgxAAgogUgABFj/IAEgBIgEAAIAAABg");
	this.shape_2.setTransform(67.8,90.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhMFzQAbgkAKgTIARghQAWg8ARhAQAKgoACg3QACgbAAhGQgBg8ABgdQAAgLgVhsQgThtAAgLQAAgUAIgBQADgBAEAIQACgFAIALQAFAKATBiQARBXAGApIAIA1QAHA9gBAjQgEBpgWBMQgPAygOAjQgUAwgcAwQgDAFgIAAQgNAAgagMg");
	this.shape_3.setTransform(128.5,63.4);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EACFB7").s().p("AltD/QhJiwgai2QAAiMAzCJQAVA7A7DMQAPAgAYBGQAYBIAQAiQAOAfASAaIgShtQgOhZgehDQgnhYgpiFQgniBAAghQAAgEACgFQgMgDgHgEQgOgJAAgbQAAgIAKgGIAAgGQAAgKAEgHIg2gDIgZgYQgGgHAAACQgWhfBUgVQBUgVAbAGQAbAGAyhXQAzhXBZBSQBZBTCFAQQBXAKA0ATQAxARBNAWQA2ARAEATQAcAZAdCsQAOBXAJBSQAAAXgKA9QgKA+AAAVQAAAegXA9QgeBQgxBDQiPDDjqAAQjnAAiRlegABcIcIAOAAIANgEIgbAEg");
	this.shape_4.setTransform(84.2,60.5);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,136.3,124.1);


(lib.LucyHair = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E5C45A").s().p("AAuIkQgfgcgXgUQimh+hIiMQhCiAAAikQAAjwB5inQBBhZBeg8QAgBgAtBrQBOC3BdCZQA7BfAmArQhGAHguAUQgPAKgHAKQgGAGgHATIgBACQgWAmgSBDQgSBDAAAqIAFBVQAKBjAYBRQg/goghgcg");
	this.shape.setTransform(36.2,111);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFDA64").s().p("AHuHlQj2hQi9grQjfgzh7AIIgiADQgmgrg6hhQhdiZhPi1QguhrghhhQAUgMAVgMQAVgLAWgKQBqgxBsgSQEYguDOA3QCRAmBiBTQBiBTBACpQAhBUAsC1QAEASALBeQASBeAmAtQgmAhgwAQQgfALgaAAQgRAAgPgFg");
	this.shape_1.setTransform(99.8,85.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AGKIgQgpgCgrgGQgSgChAgTQhSgYhTgTQhWgVhggZIhbgXQhYgQhhgBQhbgCgbANQgEACgUAHQgTAKgGASIgBABQggAcgJAAIgEAAIAIgRQAHgZgMgUIABgBQAHgUAGgGQAHgKAPgJQAugVBGgHIAhgDQB7gHDiAzQC6AqD3BQQAlAMA0gSQAvgQAmggQgmgtgRhfQgLhegFgSQgsi2gghSQhBiqhihTQhihTiPgmQjQg2kYAuQgFAAAAgCIAAgWQAAgUAXgNQAZgPA0gGQBbgLCMASQCKARByAkQB+AoAuAuQBLBLAkAwQAsA5AfBHQAXA0AWBbQAMAzASBaQAeCGAoBSQAfBBAAAFQAAAQgiAkQgwAzhGAMQgOADgZAAIgYgBg");
	this.shape_2.setTransform(113,84.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#020100").s().p("ADyLqQhKAAieiKQh7hugvhAQh6ingXhIQgMgmgCg0QgBgRAAhlQAAjxBuiwQBhiHBmhDIAUgNQAXgNAYgMQA5gbA8gTQBRgZBDgEIAAAAQgXAOAAAUIgBAWQABACAFAAQhsAShqAwQgWAKgVAMQgVALgTANQhdA9hABYQh5CnAADwQAACkBCCAQBICMCjB/QAaATAeAdQAhAcBAAnQgZhRgJhjIgFhUQAAgqAShDQAShDAWgnQAMAUgIAZIgIARIAFAAQAJAAAggcIABgBIgYBVQgJAogBA1QgBAuADAYQAFAkAYBUQAIAbAHAuQAGApAJAZIACAHIAAABQAAATgEgFQAAAAgBAAQAAAAAAABQgBAAAAABQAAABgBABQgEAEgMAAg");
	this.shape_3.setTransform(32.3,107.1);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.1,30,173.9,151.7);


(lib.LucyClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#45635D").s().p("AAGLfQgzgFgHgDIAIgYQAGgPAPgIIAOgGIAGgCIAOgEIAAAHQAAAgBiAZIAaAGQgdABgiAAQgRAAgxgEgAEyLEQAAgNgEgNQAGAAAFgHQAIgKAFgEQAKgHAPgFQALAtANAPQAIAKAUAJIhIAFQgUABgHACQACgLAAgRgACxhmQhMgKh/gPQhkgMhVgRQAGgIAGgTQAOgmAIg7IAOhSQAFgaAAgeIAAgKIAAgCIgBgJIAAgHQAGgCAHgBQAFgBAJANQAIAKAEAMQADAGAIAwQAPA1AnAsQAnAsA2AWQAgANA+ALQA3AKAVALQAhASAIAnQgqgCgegEgAk+oAIg5gUQgZgKgCgJQgDgbAmgsQAfgjAkgZQAtgfBKgZQgVAPgjAvQgmA0gVAuQgGAOgFAgQgDAUgHAAIgBAAg");
	this.shape_1.setTransform(55.5,89.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#648C83").s().p("Ah3MoQhjgYAAghIABgHQA6gNCNABQASACAPAQQAQAPgBARIgFAFQgHAHgQAFQghAMg+ADIgagGgACQMSQgNgPgKgtQAcgJAyAAQBIAAAqATQAjAQAJAaQACAIghAEQg2AGhkAIQgTgIgJgKgAATgTQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgDgTgHgFQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgLgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6QArA7AFA/QAGA6gdArQgOAWgPANQAAALACACIACAEQADADATAAQAGAAAIgEIANgGQAAANAFALIALAdQAKAgAOAYIheAUQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_2.setTransform(78.6,81.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7A7A7A").s().p("AgaDYIiSg1IgOgDQAPgYAmhQIALgWQAVACAZABQAqACAigOIAHgGQALgFAQgOQAYgTAGgTQAFgWgQgxQgLgigJgPQgTgkgKgUQAQgfAGgRQApAZB0AQIAEABIgiDXQgeDIgCBEQhJgUhKgbg");
	this.shape_3.setTransform(29.7,103.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB59F").s().p("AFfIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAAGGiIgOgcQAzAMA1AJIAmB3QgjAGgdAMQgghIggg6gAl6ATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUg");
	this.shape_4.setTransform(47.9,96.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#8E8E8E").s().p("AoKDcQABhFAejHIAjjXQA+AIC8ASQBZAICugFQC8gGBYgRQAdgFATgHIBdEUQAnB0AKASQgRAKgQAFIgQAEQhtAhifAmQgXAFiOAQQiZASgwAAQitAAi9gyg");
	this.shape_5.setTransform(94.2,107.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EACFB7").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAIVirQACgCgKgQQgLgPgEAAIgBABIAAgBIgDgHQgCgFgFgXQgGgZgHgSIgPglQgFgMAAgDQAIgjACgVQADgegDgbQgHhDgGgWIAWgJQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APQgiAJgFACQgFABgEACQgohzghg7g");
	this.shape_6.setTransform(82.3,95.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgSgcgcg3QgzhuAAACIAAgFQigguichVIAAABQgPgEgFgHIgDgCIABgEIgBgPQAAgWAxhoQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQAHAGADASIABAHIABAJIAAACIAAAKQAAAegFAaIgOBSQgJA6gNAnQgHASgFAJQBUARBlAMQCBAOBLALQAfADAoADQBFAEBjgBQCkgCBAgOIBegUQgOgYgKgfIgLgeQgFgLAAgMIgNAGQgIADgGAAQgTAAgDgDIgCgEQgCgCAAgLQAPgNAOgWQAdgrgGg6QgFg/grg7Qgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgIABIAEAKQAxCcAAA7IAAAKQgGAMgFAEQgFAEgGAAIj0BHIgrANQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgABxIEQgqACgpABQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMQh0Aah0AKgAkuH/QAgA7AgBIQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJQhkAAhigPQg1gJg1gMIAQAbgApxDWQgmBQgQAYIAPADICSA1QBMAbBJAUQC9AyCtAAQAvAACagRQCPgQAWgFQCfgmBtgiIARgEQAPgFARgJQgKgTgmh0IhekTQgTAGgdAFQhXARi8AGQixAFhXgIQi8gRg+gJIgEgBQh1gQgogZQgGARgQAfQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgiAOgrgCQgYgBgVgCIgLAWgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAIvhnQAKAPgCADQAhA6AoB0QAEgDAFgCQAFgDAigIQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJQAGAXAHBCQADAcgDAeQgCAVgIAjQAAACAFAMIAPAmQAHASAGAYQAFAYACAEIADAHIAAABIABgBQAEAAALAQgAnWjiQgYA3gEAsQALgNAIgEIgCgEQgBgDAAgNQAAgIAKgkIAIggIgGAOgAF1rgIgDgFIADAFgAj5sUQAJgHAMgHQgMAHgJAHg");
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.Lead = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Leaderboard", "25px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 31;
	this.text.setTransform(11.8,7.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#502A40").s().p("At5DCIAAmEIbzAAIAAGEg");
	this.shape.setTransform(89,19.5);

	this.addChild(this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,178.1,41.3);


(lib.LadyBoss_Bubble_Yard = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("this is not appropriate workwear for the counter. you'll be dealing with customers, so think about the impression you give with what you wear. choose  something a bit smarter.", "23px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 28;
	this.text.lineWidth = 341;
	this.text.setTransform(134,-142.3,1.227,1.227);

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EgjygY6MBBnAAAQCUAAAACTIAAVPIF9EZIl9ECIAAPlQAACTiUAAMhBnAAAQiTAAAAiTMAAAgtPQAAiTCTAAg");
	this.shape.setTransform(154.4,-6.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("EgjyAY7QiTAAAAiTMAAAgtPQAAiTCTAAMBBnAAAQCUAAAACTIAAVPIF9EZIl9ECIAAPlQAACTiUAAg");
	this.shape_1.setTransform(154.4,-6.4);

	// Layer 5
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("EgjxAZZQiUAAAAiTMAAAguLQAAiTCUAAMBBmAAAQCUAAAACTIAAWLIF9EZIl9ECIAAPlQAACTiUAAg");
	this.shape_2.setTransform(163.7,-0.1);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-91.4,-168,499,330.5);


(lib.LadyBoss = function() {
	this.initialize();

	// Layer 6
	this.instance = new lib.Health_and_Safety_Officer__000();
	this.instance.setTransform(68.5,0,0.938,0.938);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(68.5,0,244,346);


(lib.KateClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#371E2D").s().p("AinAoQg1gEgHgDIAJgZQAFgMAPgJIAOgGIAGgCIARgEIgBAHQAAAeBjAZIAaAGQgdACgjgBQgRABgxgFgACDAOQAAgOgEgKQAGAAAGgIQAIgKAEgDQAKgIAQgFQAKAsANAPQAJAKATAIIhIAGQgUABgGABQABgKAAgRg");
	this.shape_1.setTransform(73,158.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#4D2A3E").s().p("Ai8AqQhjgYAAgfIABgHQA6gNCNABQATACAQAQQAQAOgBAQIgFAFQgHAHgQAFQgjAMg+ADIgagGgABLAUQgNgPgKgrQAcgJAyAAQBIAAAqATQAjAQAJAYQACAIghAEQg2AGhkAIQgTgIgJgKg");
	this.shape_2.setTransform(85.5,158.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7A7A7A").s().p("AgaDYIiSg1IgOgDQAPgYAmhQIALgWQAVACAZABQAqACAigOIAHgGQALgFAQgOQAYgTAGgTQAFgWgQgxQgLgigJgPQgTgkgKgUQAQgfAGgRQApAZB0AQIAEABIgiDXQgeDIgCBEQhJgUhKgbg");
	this.shape_3.setTransform(29.7,103.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB59F").s().p("AFfIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAAGGiIgOgcQAzAMA1AJIAmB3QgjAGgdAMQgghIggg6gAl6ATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUg");
	this.shape_4.setTransform(47.9,96.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#381E2D").s().p("AD+E6QhMgKiBgOQhigMhVgRQAGgJAGgSQAOgnAIg6IAOhSQAFgaAAgdIAAgKIAAgBIgBgJIAAgHQAGgDAHgBQAFAAAJAMQAIALAEAJQADAHAIAvQAPA2AlAsQAnAsA4AWQAgAMA+AMQA3AKAVAKQAhASAIAoQgqgDgegEgAjxheIg5gUQgZgJgCgJQgDgbAmgsQAfgjAkgZQAtggBKgZQgVAQgjAvQgmAzgVAuQgGAPgFAgQgDATgHAAIgBAAg");
	this.shape_5.setTransform(47.8,47.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#8E8E8E").s().p("AoKDcQABhFAejHIAjjXQA+AIC8ASQBZAICugFQC8gGBYgRQAdgFATgHIBdEUQAnB0AKASQgRAKgQAFIgQAEQhtAhifAmQgXAFiOAQQiZASgwAAQitAAi9gyg");
	this.shape_6.setTransform(94.2,107.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EACFB7").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAIVirQACgCgKgQQgLgPgEAAIgBABIAAgBIgDgHQgCgFgFgXQgGgZgHgSIgPglQgFgMAAgDQAIgjACgVQADgegDgbQgHhDgGgWIAWgJQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APQgiAJgFACQgFABgEACQgohzghg7g");
	this.shape_7.setTransform(82.3,95.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#4D2A3F").s().p("AATGLQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgDgTgHgFQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgJgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6QArA5AFA/QAGA6gdArQgOAWgPANQAAALACACIACAEQADADATAAQAGAAAIgEIANgGQAAANAFALIALAdQAKAgAOAYIheAUQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_8.setTransform(78.6,39.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgSgcgcg3QgzhuAAACIAAgFQigguichVIAAABQgPgEgFgHIgDgCIABgEIgBgPQAAgWAxhoQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQAHAGADASIABAHIABAJIAAACIAAAKQAAAegFAaIgOBSQgJA6gNAnQgHASgFAJQBUARBlAMQCBAOBLALQAfADAoADQBFAEBjgBQCkgCBAgOIBegUQgOgYgKgfIgLgeQgFgLAAgMIgNAGQgIADgGAAQgTAAgDgDIgCgEQgCgCAAgLQAPgNAOgWQAdgrgGg6QgFg/grg7Qgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgIABIAEAKQAxCcAAA7IAAAKQgGAMgFAEQgFAEgGAAIj0BHIgrANQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgABxIEQgqACgpABQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMQh0Aah0AKgAkuH/QAgA7AgBIQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJQhkAAhigPQg1gJg1gMIAQAbgApxDWQgmBQgQAYIAPADICSA1QBMAbBJAUQC9AyCtAAQAvAACagRQCPgQAWgFQCfgmBtgiIARgEQAPgFARgJQgKgTgmh0IhekTQgTAGgdAFQhXARi8AGQixAFhXgIQi8gRg+gJIgEgBQh1gQgogZQgGARgQAfQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgiAOgrgCQgYgBgVgCIgLAWgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAIvhnQAKAPgCADQAhA6AoB0QAEgDAFgCQAFgDAigIQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJQAGAXAHBCQADAcgDAeQgCAVgIAjQAAACAFAMIAPAmQAHASAGAYQAFAYACAEIADAHIAAABIABgBQAEAAALAQgAnWjiQgYA3gEAsQALgNAIgEIgCgEQgBgDAAgNQAAgIAKgkIAIggIgGAOgAF1rgIgDgFIADAFgAj5sUQAJgHAMgHQgMAHgJAHg");
	this.shape_9.setTransform(78.9,87.4);

	this.addChild(this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.Jason = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ai4AyQgLgLAAgMQAAgGABgEIg/ABQgSAEgaAIQgTAGgKAAQgLAAgJgBIgGgCIgDgRIAEgMQACgFAGgFQASgLBKgIQA9gQB+ANQAWATABAEQAAADgJAGQgIAHgGAEIgnABQACAFAAAHQAAAVgNAKQgKAHgOAAQgbAAgPgQgADlARQgVgEgJgJQgCgCgBgCIgBgDIABgHIgFgBQgLgCgJgEIgIgDIgBgDIAAgBQgCgCACgLQAGggBbAEQAoACA5AJIACAAIABAFQACAEgCALQgCAJg3ALIgVAEIgCAAIAAACQgFAageAAIgPgBg");
	this.shape.setTransform(96.2,87.3);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D3D37E").s().p("ABhGpQgrgFgkgXIAAAAQAAgRgZgTQgXgQgkgKQgRgFgNgBQgnjbAAgKQAAhHAPhUQAWh/AwhiQAqhXA4g5QArA4AgA2IANAVIgBACIgFAFQgmAygRBdQgVBoATCiQASCcApB7QAJAXgfAAIgNAAg");
	this.shape_1.setTransform(19.9,58.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF99").s().p("AAnDBQkehMhThUQgngmghgJQgcgIgTAPIgNgVQggg2grg4QCFiGDbAVID+AAQDJAtByBkQA9A2A0B2QAZA9AOAyIACAJQgVAfgDAAQgoARhJARQgOADgVAAQhcAAjrg9g");
	this.shape_2.setTransform(78.5,30.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EACFB7").s().p("AjCAwIgLgCQAAgBgEgDQgDgCAAgSIh5AKQgPABgKgCIgIgCQAAAAAAgBQAAAAAAAAQgBAAAAABQgBAAAAABQgDACgBgOQgBgOAFgEQAHgHAdgFQAtgIDZgQIAqgEQAAABBIgBICTgFQAzgDAkACQAyACAWAIQAXAIAAAQQAAAOgMADIgWAAIgOAAQgBAXgCAHIgdAGQgsgCgMgQQgDgDgBgUIghgCQkEAHgKgEIgBAAIgxAJQgBAOgLALQgNALgUACIgPAAIgOAAg");
	this.shape_3.setTransform(94.6,87.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DEBBA4").s().p("ADVJFQhIgbhMhLQgdgcgTgsIgZg6IAKgHQATgMAAgXQAAgMgBgGQgHgOgSAAQgLAAgiAMQghAKgMABQgngBgmgTQg1gdgjhCQgDgGAAgKIABgTQAAgkAagUQAKgHAHgIQAfAIAcAPQATALAaAAQAKAAAHgCQAIgFABgNQAkAXArAFQArAEgKgbQgnh5gSicQgTikAVhoQARhdAkgyIAFgGQBwCWAVCyQAOBsAHD6QATCbAZCEQAZCEAPBbQgbgHgYgKg");
	this.shape_4.setTransform(30.8,89.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgKMJQg8gJgsgNQhsgghKg0Qh0hQgjiCIAAgBIgiABQhlAAg5hEQgogvgCgvIgJgFQgDgVAAgWQAAgsAFgTQAKgqAggKQgLgegKhqIgIhmQAAjkBXi1QAuhfBAg+QCRiNDxAXIAAgEID1AAQECAuCHCOQA8A/AjBjQAVA8AKA1QgDAAALAsQANAwABBvQAAB8gVA+QgDALACAmQACArgBAGQgIA6gMAvQgTBGgkBUQgDAHgEAbIgGAkQgIB1hLBUQhMBXhvAKIjPAEQhRgJglgFgAltGIQACAFAAAMQAAAYgTALIgNAHIAbA6QAUAsAdAdQBLBKBIAcQAYAJAbAIQCDAkDfgBQBmAABLgvQBlhAASiFIAAgCQgfAFgoAKIgBAAQgsALggAGQhNAMhWgDQg4gChagOIhAgXQAAgdA0AHQCUAYCdgYQAwgHAjgJQAjgJAugSIAAAAIADgCQAGgHAJgSIAohaQAWgyAIgwQAHgqAAg5QAAgOACgJQgCgHAAgGQAKhOAFhPQAKiUgUhKIgCgJQgOgygZg9Qg0h4g9g2QhyhkjJgtIj+AAQjbgViFCGQg6A5gqBXQgwBigVB/QgPBWAABHQAAAKAmDZQANABARAFQAkAKAXAQQAbATAAARIAAAAQAAANgJAFQgGACgLAAQgZAAgUgLQgcgPgfgHQgGAHgKAIQgbATAAAlIgBATQAAAKADAFQAjBCA2AdQAlAUAnAAQAMAAAhgLQAigMALAAQAUAAAHAPgACyFPQgMgLgBgQIACgUQAAgQAKgVQALgZAQACQAQACAAAQIAAAqIA2AZICKAAQASgGAFgPQADgKgBgcQAAgrAZAPQAZAOAAAnQAAAmgQASQgfAhhgAAQiEAAgighg");
	this.shape_5.setTransform(69.4,79.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EAD0B7").s().p("AkGJKQgPhcgZiDQgZiEgTicQgHj5gNhsQgWiyhwiWIABgBQATgPAcAIQAhAJAnAnQBSBVEhBLQEfBMBJgRQBJgSAogQQADgBAVgeQAUBKgKCTQgFBQgKBPQAAAHACAGQgCAJAAAOQAAA4gHAqQgIAvgWAzIgoBaQgJASgGAGIgDADIAAAAQguARgjAJQgjAKgwAHQidAXiSgYQg0gHAAAeIBAAXQBYAOA4ACQBWADBNgNQAggFAsgLIABAAQAogLAfgEIAAACQgSCFhlA/QhLAvhmABIgMAAQjVAAiBgkgAAkCIQgKAVAAARIgCAUQABAPAMALQAiAhCEAAQBgAAAfghQAQgRAAgnQAAgmgZgPQgZgOAAAqQABAdgDAJQgFAPgSAHIiKAAIg2gZIAAgqQAAgRgQgCIgCAAQgOAAgLAXgAhiALQADADAAABIALACQAPABAOgBQAUgCANgLQAMgJABgOIAvgJIAAAAQAKAEEGgJIAhADQABAVADADQAMAOAtACIAcgGQADgFABgXIAOAAIAVAAQAMgDAAgQQAAgQgWgIQgWgIgzgCQgkgCgyADIiTAFQhKABAAgBIgrAEQjXAQgsAIQgdAFgIAIQgEAFABAOQABAMACgBQABAAAAAAQABAAAAAAQAAAAABAAQAAAAAAAAIAHAAQALACAOgBIB6gIQAAAQADACg");
	this.shape_6.setTransform(83.5,90.6);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,138.9,158.3);


(lib.JaneHead = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A4D30").s().p("ACQHdQhsgsh6g9IALgaQATgugBgWQAAgDgYgWQgcAPgHAkQgHAlgNAHQjEh8gBgiQAAgOAQgZQAQgYAAgHQAAgMgEgDIgDgGQgWAGgPAHQgDgYAAguQAAhTAkhpQAoh5BJhjQBThwBqhAQApBXAkBPQBdDVAVB1QhQASABBMQAAAMACACIADAEQAQACATgCIApgIIAKgCQASB8AeBkQAjBvBJCYQhhgWhsgsg");
	this.shape.setTransform(38.2,62.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC0066").s().p("AgmAMIg6geQBFgNB8AfIgcAMQgdALgbAAQgaAAgZgLg");
	this.shape_1.setTransform(89.7,104.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ADeFPQjaAAjehuQhQgng7gqQgcgVgMgMQgLALgMAAQhPAAhfhhQgighgaglQgXgiAAgKQAAgtAFgJQAHgNAYgLQAPgHAWgGIADAFQADAEAAAMQAAAHgPAYQgQAZAAAOQAAAiDFB6QANgHAHglQAHgiAcgPQAYAWAAADQAAAUgSAuIgMAZQB9A+BsAsQBrAsBgAWQBaAUBSACQBuACBchSQAXgUAugxQAngsAWgQQAFgEAEgBQAFgBADADQAEACABAEIADAFQADAEAHgFQAIgGgkA3QgjA3haBKQhaBLh/AAIAAAAgABbCpQg9gqAAgTQAAguBvACQApABDOAZQAYADAAAZQAAASgvAkQg8AthHAAQhHAAhIgwgAB5B2IA5AgQA0AVA5gVIAcgOQhYgWg8AAQgaAAgUAEgADWATQg8gJgSgUQgJgKAAgXQAAg0AmAYQAqAlALAEQA9APA9gPIAAgSIAIgFQAIgFAMAAQANAAAGACIADACIADAFQADAEAAAMQAAAOgQARIgYAVQggAFgkAAQgjAAgngFgAIPi3QgPgBgSgJQgWgLgPgRQgOgPgJgEQgQgHgdAEQgMADgLAEQgHgCgEgEIgDgFIgCgEIgCgDIADgFQAagpBpACQBhACA4AeQAVALAMARQALAPgBAIIgBAHIAAABIgBAAQgLgGgQgDIgOgBIgIgBQgJABgHADIgOAFQgGACgPALQgMAJgKACQgLADgKAAIgGgBgAhDjeQgIgEgaADIgKABIgpAIQgUACgPgCIgDgDQgDgDAAgLQAAhOBPgSQAigJAvADQBwAGCYA5QANAFgPAKQgPAJgZADIgbAAIggAAQgdAiguAFQgRACgQAAQgxAAgogUgABFj/IAEgBIgEAAIAAABg");
	this.shape_2.setTransform(67.8,90.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhMFzQAbgkAKgTIARghQAWg8ARhAQAKgoACg3QACgbAAhGQgBg8ABgdQAAgLgVhsQgThtAAgLQAAgUAIgBQADgBAEAIQACgFAIALQAFAKATBiQARBXAGApIAIA1QAHA9gBAjQgEBpgWBMQgPAygOAjQgUAwgcAwQgDAFgIAAQgNAAgagMg");
	this.shape_3.setTransform(128.5,63.4);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#975F3C").s().p("AlvDGQhJivgai3QAAiMAzCJQAVA7A7DNQAPAfAYBHQAYBHAQAjQAOAfASAaIgShuQgOhYgehDQgnhZgpiFQgniAAAghQAAgFACgEQgNgDgGgEQgOgKAAgbQAAgHAKgGIAAgHQAAgJAEgIIg2gDIgZgYQgGgGAAABQAAgkCaAFQAfABAkgVQAKgGA0glQBUg8BIANQAoAHCFAQQBXAKA0ATQAxASBNAVQA2ASAEATQAcAYAdCtQAOBWAJBUQAAAXgKA8QgKA+AAAVQAAAdgXA+QgeBPgxBDQiPDDjqAAQjnAAiRlegABaHjIAOAAIANgDIgbADg");
	this.shape_4.setTransform(84.4,66.1);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,8.1,136.3,116);


(lib.JaneClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7A4D30").s().p("AFfIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAAGGiIgOgcQAzAMA1AJIAmB3QgjAGgdAMQgghIggg6gAl6ATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUg");
	this.shape_1.setTransform(47.9,96.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#975F3C").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAIVirQACgCgKgQQgLgPgEAAIgBABIAAgBIgDgHQgCgFgFgXQgGgZgHgSIgPglQgFgMAAgDQAIgjACgVQADgegDgbQgHhDgGgWIAWgJQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APQgiAJgFACQgFABgEACQgohzghg7g");
	this.shape_2.setTransform(82.3,95.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4B9FB5").s().p("AD+E6QhMgKiBgOQhigMhVgRQAGgJAGgSQAOgnAIg6IAOhSQAFgaAAgdIAAgKIAAgBIgBgJIAAgHQAGgDAHgBQAFAAAJAMQAIALAEAJQADAHAIAvQAPA2AlAsQAnAsA4AWQAgAMA+AMQA3AKAVAKQAhASAIAoQgqgDgegEgAjxheIg5gUQgZgJgCgJQgDgbAmgsQAfgjAkgZQAtggBKgZQgVAQgjAvQgmAzgVAuQgGAPgFAgQgDATgHAAIgBAAg");
	this.shape_3.setTransform(47.8,47.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#58BAD3").s().p("AATGLQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgDgTgHgFQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgJgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6QArA5AFA/QAGA6gdArQgOAWgPANQAAALACACIACAEQADADATAAQAGAAAIgEIANgGQAAANAFALIALAdQAKAgAOAYIheAUQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_4.setTransform(78.6,39.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7A7A7A").s().p("AAcGqQgzgEgIgDIAJgYQAFgPAQgJIAMgGIAGgCIAQgDIgBAHQABAgBiAZIAaAGQgdABgjAAQgQAAgxgFgAFHGQQABgNgEgNQAGAAAFgHQAIgKAFgEQAKgHAPgFQALAtANAPQAIAKATAIIhHAGQgUABgHABQACgKgBgRgAkHAwIiSgzIgOgDQAPgYAmhQIALgWQAVACAZABQAqACAigOIAJgGQALgFAQgOQAYgVAGgTQAFgWgQgxQgLgigJgPQgTgkgKgUQAQgfAGgRQApAZB0AQIAEABIgiDXQgeDIgCBEQhJgUhMgbg");
	this.shape_5.setTransform(53.4,120);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#8E8E8E").s().p("AkTGWQhjgZAAggIABgHQA6gNCMABQAUACAQAQQAPAPAAARIgFAFQgIAGgPAGQgjALg+AEIgagGgAgKGAQgNgPgKguQAcgIAwAAQBIAAApASQAjARAJAaQADAIghAEQg2AGhkAIQgRgIgJgKgAoKBOQABhFAejHIAjjXQA+AIC8ASQBZAHCugFQC8gGBYgQQAdgGATgGIBdEVQAnB0AKASQgRAJgQAFIgQADQhtAiifAmQgXAFiOAQQiZARgwAAQitABi9gyg");
	this.shape_6.setTransform(94.2,121.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgSgcgcg3QgzhuAAACIAAgFQigguichVIAAABQgPgEgFgHIgDgCIABgEIgBgPQAAgWAxhoQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQAHAGADASIABAHIABAJIAAACIAAAKQAAAegFAaIgOBSQgJA6gNAnQgHASgFAJQBUARBlAMQCBAOBLALQAfADAoADQBFAEBjgBQCkgCBAgOIBegUQgOgYgKgfIgLgeQgFgLAAgMIgNAGQgIADgGAAQgTAAgDgDIgCgEQgCgCAAgLQAPgNAOgWQAdgrgGg6QgFg/grg7Qgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgIABIAEAKQAxCcAAA7IAAAKQgGAMgFAEQgFAEgGAAIj0BHIgrANQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgABxIEQgqACgpABQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMQh0Aah0AKgAkuH/QAgA7AgBIQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJQhkAAhigPQg1gJg1gMIAQAbgApxDWQgmBQgQAYIAPADICSA1QBMAbBJAUQC9AyCtAAQAvAACagRQCPgQAWgFQCfgmBtgiIARgEQAPgFARgJQgKgTgmh0IhekTQgTAGgdAFQhXARi8AGQixAFhXgIQi8gRg+gJIgEgBQh1gQgogZQgGARgQAfQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgiAOgrgCQgYgBgVgCIgLAWgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAIvhnQAKAPgCADQAhA6AoB0QAEgDAFgCQAFgDAigIQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJQAGAXAHBCQADAcgDAeQgCAVgIAjQAAACAFAMIAPAmQAHASAGAYQAFAYACAEIADAHIAAABIABgBQAEAAALAQgAnWjiQgYA3gEAsQALgNAIgEIgCgEQgBgDAAgNQAAgIAKgkIAIggIgGAOgAF1rgIgDgFIADAFgAj5sUQAJgHAMgHQgMAHgJAHg");
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.hns_stretch_bubble_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EAo9gLtIAAXbEgo8ALuIAA3b");
	this.shape.setTransform(262.1,75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("Ego8ALuIAA3bMBR5AAAIAAXbg");
	this.shape_1.setTransform(262.1,75);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AgkLuIAA3bIBJAAIAAATIAAXIg");
	this.shape_2.setTransform(528,75);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,533.8,154);


(lib.hns_head_bubble_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("Ego8ABoIAAhDQAAiMCOAAMBNdAAAQBzAAAWBeQAFAUAAAaIAABD");
	this.shape.setTransform(262.1,10.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("Ego8ABnIAAhCQAAiLCOgBMBNdAAAQBzAAAWBeQAFAUAAAaIAABCg");
	this.shape_1.setTransform(262.1,10.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AghA5IAAhBQAAgagFgWQBHAZAGBYg");
	this.shape_2.setTransform(527.7,15.1);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,533.7,24.8);


(lib.hns_bottom_bubble_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EAmGgGsIAAAxQAACNiOAAIlZAAIBtBVILHIsIAhAaI38qbMg9eAAAQgjAAgagIQhQgbAAhqIAAgx");
	this.shape.setTransform(280.5,43);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("AT3jtMg9dAAAQgjAAgagJQhQgbAAhqIAAgxMBR5AAAIAAAxQAACOiPAAIlYAAIBsBUILIIsIAhAag");
	this.shape_1.setTransform(280.5,43);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AUMimMg9gAAAQhzABgWheQAbAJAjAAMA9dAAAIX8KaIghgaIBFA0gAfSimIhthUIFZAAQCOAAAAiOIAAgxIBLAAIAACGQAACNiOAAg");
	this.shape_2.setTransform(286.2,44.3);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,566.5,90.5);


(lib.HInfront = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#352011").s().p("ApUKcQhfgJg4g9Qgxg2gBhLQgBhdA0hEQAggrAEgHQAQgcABghQABglgcg6Qgcg7AAghQAAgeA2g/QA5hEAHgoQAHgmAAg4QAAg8AEgkQARh/B3hyQAbgZAdgXQAWAyAiA8QATAiAsBGQAdAuAAAHIAFA/QAEAaAIAMQASAWBaAAQAvAABagNIBPgNIAvAMQAxAMAbACQCNAKBngnQAggMAZgQIATgNQAQAAAaAJQgPAGgLAXQgRAigGAIIgXAZQgTATgWAfQgbAnhQAkQhCAdhngJQgJgBgfgXIghgYQAbgTAZgaQAYgZAAgIQAAgQgEgFQgFgGAAgBQgagCgdAXQgRANgoAmQhXBMiKABQgOAAgpgVQgpgUgOAAIgIAAIgDAAQgQAAAAAEQAAABAAABQAAABAAAAQAAABAAAAQgBAAAAAAIgDAOIABADIgBAPQAAAbARApQAQAmAAAVQAAAagMAbIgcAzQgoBIAABYQAAA5AfBCQAgBCAAAuIgBAgQgFAggUAZQg0A+iLAAQggAAglgDgAHgisQAkgmAYgsQAWgoAAgVIAAgVQAhAMAWAAQBQAAAogzQAZggAfhUQAFAXAAAbQAAAqgPAjQgQAngnAqQgzA4hBAeQg2AZgzABIgHAAQgJAAgLgBg");
	this.shape.setTransform(84.8,88.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5B371E").s().p("AACEQQgZgCgxgMIgugMIhSANQhaANgvAAQhaAAgSgXQgIgLgEgaIgEg/QgBgHgdgvQgshGgTgfQgig8gWgyQBthTCYg0QCWgqCUAqQCEA7CbAvQCZArA4ASQBaAfAlAmQAdAcAKAvQggBUgZAgQgoAzhQAAQgWAAghgMIgBgEQgCgFgFACIgGACQgagIgQAAIgTANQgaAPggAMQhTAfhrAAQgaAAgcgBg");
	this.shape_1.setTransform(103.1,32.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AsuKlQgXgogGg1IAAg6QAAg8A1heQA0hdAAgDQAAgcgcg7Qgcg6AAgwQAAgeARgcQAJgOAegfQA8hBAGhDIAJhjQAEgxAAgxQARh7CPh+QAbgYAcgVQB3hZCQgvQCagpCpApQCWA8C0A2QC2A3AjAOQBWAhAhAxQAIAMAHAPQAVAxAABPQAABegmA1QgxBFieBWIhvAAIgagWIgBAAQh1BthyAAQhOABhDgsQgfgUgNgPQh9BDhhAAQgmABgegIQAJAsAEArQAABEgsBKQgtBJAAAtQAAATAiBLQAhBMAABGQAABOgdAqQg5BSimgBQjnAAhJh7gAijrUQiYA0htBTQgdAXgbAZQh3BygRB/QgEAkAAA7QAAA4gHAnQgHAog5BEQg2A9AAAgQAAAgAcA7QAcA7gBAlQgBAhgQAcQgEAHggAqQg0BEABBeQABBKAxA3QA4A9BfAIQDEATBAhNQAUgZAFghIABgfQAAgugghCQgfhCAAg5QAAhYAohIIAcgzQAMgbAAgaQAAgVgQgoQgRgpAAgZIABgQIgBgCIADgOQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgBAAgBQAAgFAQABIADAAIAIAAQAOAAApAUQApAVAOgBQCKgBBXhLQAogmARgNQAdgXAaACQAAABAFAFQAEAGAAAQQAAAHgYAZQgZAbgbASIAhAZQAfAWAJACQBnAJBCgeQBQgjAbgnQAWgfATgTIAXgZQAGgIARgjQALgXAPgFIAHgDQAFgCACAGIABADIAAAWQAAAVgWAoQgYArgkAnQAPACAMgBQAzgCA2gYQBBgeAzg4QAngqAQgoQAPgiAAgqQAAgbgFgYQgJgvgdgeQglgmhagfQg4gSiZgrQibguiGg8QhKgVhJAAQhKAAhLAVg");
	this.shape_2.setTransform(84.5,80.1);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,169,160.2);


(lib.HighlightCircle = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(3,1,1).p("AD4AAQAABmhKBIQhIBKhmAAQhlAAhJhKQhJhIAAhmQAAhlBJhJQBJhJBlAAQBmAABIBJQBKBJAABlg");
	this.shape.setTransform(24.8,24.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,52.6,52.6);


(lib.Health_and_Safety_Advisor = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// H and S
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("APWAAQAAGWkgEfQkgEhmWAAQmVAAkgkhQkgkfAAmWQAAmWEgkfQEgkgGVAAQGWAAEgEgQEgEfAAGWg");
	this.shape.setTransform(90.1,90.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ap7J8QkJkHAAl1QAAl0EJkIQEHkHF0gBQF0ABEIEHQEJEIAAF0QAAF1kJEHQkIEJl0gBQl0ABkHkJg");
	this.shape_1.setTransform(90.1,90.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCB55E").s().p("Aq1K1QkfkfgBmWQABmWEfkfQEgkfGVgBQGWABEgEfQEgEfAAGWQAAGWkgEfQkgEhmWgBQmVABkgkhgAp7p8QkJEIAAF0QAAF1EJEHQEHEJF0gBQF0ABEIkJQEJkHAAl1QAAl0kJkIQkIkHl0gBQl0ABkHEHg");
	this.shape_2.setTransform(90.1,90.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(46));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.6,-9.6,199.4,199.4);


(lib.HeadG = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC0066").s().p("AgmAMIg6geQBFgNB8AfIgcAMQgdALgbAAQgaAAgZgLg");
	this.shape.setTransform(89.7,104.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DDBAA4").s().p("ACQHdQhsgsh6g9IALgaQATgugBgWQAAgDgYgWQgcAPgHAkQgHAlgNAHQjEh8gBgiQAAgOAQgZQAQgYAAgHQAAgMgEgDIgDgGQgWAGgPAHQgDgYAAguQAAhTAkhpQAoh5BJhjQBThwBqhAQApBXAkBPQBdDVAVB1QhQASABBMQAAAMACACIADAEQAQACATgCIApgIIAKgCQASB8AeBkQAjBvBJCYQhhgWhsgsg");
	this.shape_1.setTransform(38.2,62.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AjaDhQhQgng6gqQgcgVgNgMQgLALgLAAQhQAAhehhQgjghgZglQgXgiAAgKQAAgtAEgJQAHgNAZgLQAPgHAWgGIADAFQADAEAAAMQAAAHgQAYQgPAZAAAOQAAAiDFB6QANgHAGglQAHgiAdgPQAYAWAAADQAAAUgTAuIgLAZQB9A+BrAsQBsAsBgAWQBaAUBSACQBuACBchSQAXgUAtgxQAogsAWgQQAFgEAEgBQAEgBAEADQADACACAEIADAFQADAEAAAMQAAAYgfAoQgfAogyAmQh8Beh/AAQjaAAjfhugABcCpQg+gqAAgTQAAguBvACQApABDPAZQAYADAAAZQAAASgwAkQg7AthHAAQhHAAhIgwgAB5B2IA6AgQAzAVA6gVIAcgOQhYgWg8AAQgaAAgVAEgADXATQg8gJgTgUQgIgKAAgXQAAg0AlAYQArAlALAEQA9APA9gPIAAgSIAIgFQAIgFAMAAQAMAAAGACIAEACIADAFQADAEAAAMQAAAOgQARIgYAVQggAFgkAAQgjAAgngFgAIMipQgOgCgSgLQgVgMgOgRQgOgQgIgEQgQgIgdADQgNABgKAEQgHgCgEgEIgGgMQAmhMCVAvQA3ARAsAbQArAbgCAOIgCAHQgKgGgRgEIgOgCIgHgBQgKAAgHADIgPAEQgFACgQAKQgNAIgJACQgKACgIAAIgKgBgAhDjeQgIgEgZADIgLABIgoAIQgUACgPgCIgDgDQgDgDAAgLQgBhOBQgSQAhgJAwADQBvAGCYA5QAOAFgQAKQgPAJgYADIgbAAIggAAQgdAigvAFQgQACgQAAQgxAAgpgUgABFj/IAEgBIgEAAIAAABg");
	this.shape_2.setTransform(67.8,90.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EACFB7").s().p("AAUJSQhQgBhagVQhKiXgihwQgfhkgRh8QAagDAHAEQA1AaBHgIQAvgFAdgiIAgABIAbgBQAWgCAPgKQAQgKgNgFQiXg3hxgGQgwgCghAIQgUh1hejVQgkhPgqhXQCHhQCtAAQBlAABzAlQCBAoBpBKQEJC6AAEsQAACcgyBBQgIAMgrAqQgdAfgVAfQgEABgFADQgWARgoArQgtAxgXAVQhZBPhrAAIgGAAgAiuGjQAAATA+ApQBIAwBFAAQBHAAA7gsQAwgkAAgTQAAgZgYgDQjNgYgogBIgMAAQhkAAAAAsgAhMEJQAAAXAIAKQATAWA6AJQBNALBBgLIAYgXQAQgRAAgOQAAgLgDgEIgDgGIgDgCQgHgCgMAAQgMAAgIAFIgIAFIAAASQg9AQg9gQQgLgDgpglQgLgIgJAAQgRAAAAAjgADlBNQAIAFAOAQQAOARAVAMQASAKAOACQALACARgDQAJgBAOgJQAPgJAFgCIAQgFQAGgCAKAAIAHAAIAOACQARAEALAHIABgIQACgOgqgbQgtgag2gRQiWgugmBKIAGAMQAEAEAHADQALgEAMgCIAPgBQASAAAMAGg");
	this.shape_3.setTransform(88.4,59.5);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,139.6,124.1);


(lib.HardHatIsolated = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A0A056").s().p("AhVBFQALg/AQgoQAXg6ApgrQApgpA3gWQgaA+gTBJQgTBIgIBPIgLAGQgjAXgiAgQgiAdgRAVQAEhEAMg+g");
	this.shape.setTransform(13,26.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CECE6F").s().p("AlUBLQANgMApgfQAegYAZgKQBJgqBNgeQCQg6CQAAQBNABA4AjQAsAdAAAVQAAALgSATQgUATghAUQgTAMgdAUIgggdQgSgNgdgJQhLgVhiAEQhVADg+AMQhUAThGAoIgLAHIhUBBQAIgaAjggg");
	this.shape_1.setTransform(44.4,35.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF91").s().p("AjpAFQAThGAag/QBFgdBIAAQB9AABQAwQA+AmApBJQiGgBhtAhQieAxh4BLQAIhOAThLg");
	this.shape_2.setTransform(42.2,19.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#212112").s().p("AmeEeQgEgEgCgJQgDgMAAgWQABgUgBgZIgCghQADjnB4h4QA2g1BFgYIAkgLQBagYBnASQA2AJAgAOQAZAMAjAaQAeAYAXAeQAUAcAUApIABACIADgBQBCAJAmAnQAdAfgBAbQgBAig3AqIhfBCQgKgBgDgCQgEgDAAgOIgIgHQgQgOgNgHQg0gghoAAQjTAAh+BPQgRALgQANQgoAggfAsIgJANQAAACgRAeQgIgDgDgEgAhBgRQhNAchJApQgZAMgeAZQgpAfgNAMQgjAggIAZIBUhAIALgHQBGgpBUgSQA+gOBVgDQBigEBLAXQAdAJASANIAgAdQAdgVATgLQAhgUAUgVQASgTAAgMQAAgSgsgdQg4gkhNAAQiQAAiQA6gAjCjpQg3AXgoApQgsArgXA6QgQAqgLA8QgMA/gDBEQAQgWAigdQAigfAlgXIALgHQB4hJCegxQBsghCHABQgphLg/gmQhQgwh8AAQhIAAhFAdg");
	this.shape_3.setTransform(42.8,30);

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#444324").s().p("AhUhpIAzAAQA0AIAkAnQAcAdABAXIAAAEQAAAGgFAGQgHAGgIAAQgEAAgHgGQgRAXgkAbQgiAZgeAOIgQAHg");
	this.shape_4.setTransform(62.9,42.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#444424").s().p("AhACSQgegBgbgCQgdgEgZgGQgvgLgIgIQgKgJgBgKQgQgBgFgNIgCgMIAAgCQAAgoCNhPIAJgGQCRhUBEgDICjAAIADDUQhwAwhFAPQhRAQg7AAIgIAAg");
	this.shape_5.setTransform(28.2,46.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#212112").s().p("AiwBtIgcgDQhigMgggVQgUgLgBgbQAAgGADgIQAEgIAAgIIAAAAIACALQAFAOAQAAQABAKAKAJQAIAIAvAMQAZAGAdADQAbADAeABQA/ABBVgSQBFgOBwgxIAQgFQAfgOAjgaQAlgaAQgXQAHAFAEAAQAJAAAGgGQAGgFAAgJIgBgDQAIAAABACQABACgSAWQg3BEhnAvQhMAlhnAaQhPARg8AAQgVAAgSgCg");
	this.shape_6.setTransform(36.6,53.1);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.2,0.7,85.2,63.6);


(lib.HardHat = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A0A056").s().p("AhVBFQALg/AQgoQAXg6ApgrQApgpA3gWQgaA+gTBJQgTBIgIBPIgLAGQgjAXgiAgQgiAdgRAVQAEhEAMg+g");
	this.shape.setTransform(13,26.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CECE6F").s().p("AlUBLQANgMApgfQAegYAZgKQBJgqBNgeQCQg6CQAAQBNABA4AjQAsAdAAAVQAAALgSATQgUATghAUQgTAMgdAUIgggdQgSgNgdgJQhLgVhiAEQhVADg+AMQhUAThGAoIgLAHIhUBBQAIgaAjggg");
	this.shape_1.setTransform(44.4,35.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF91").s().p("AjpAFQAThGAag/QBFgdBIAAQB9AABQAwQA+AmApBJQiGgBhtAhQieAxh4BLQAIhOAThLg");
	this.shape_2.setTransform(42.2,19.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#212112").s().p("AmeEeQgEgEgCgJQgDgMAAgWQABgUgBgZIgCghQADjnB4h4QA2g1BFgYIAkgLQBagYBnASQA2AJAgAOQAZAMAjAaQAeAYAXAeQAUAcAUApIABACIADgBQBCAJAmAnQAdAfgBAbQgBAig3AqIhfBCQgKgBgDgCQgEgDAAgOIgIgHQgQgOgNgHQg0gghoAAQjTAAh+BPQgRALgQANQgoAggfAsIgJANQAAACgRAeQgIgDgDgEgAhBgRQhNAchJApQgZAMgeAZQgpAfgNAMQgjAggIAZIBUhAIALgHQBGgpBUgSQA+gOBVgDQBigEBLAXQAdAJASANIAgAdQAdgVATgLQAhgUAUgVQASgTAAgMQAAgSgsgdQg4gkhNAAQiQAAiQA6gAjCjpQg3AXgoApQgsArgXA6QgQAqgLA8QgMA/gDBEQAQgWAigdQAigfAlgXIALgHQB4hJCegxQBsghCHABQgphLg/gmQhQgwh8AAQhIAAhFAdg");
	this.shape_3.setTransform(42.8,30);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.2,0.7,85.2,58.7);


(lib.Handlecopy = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9E8C49").s().p("ArOM0IgBgOQADh+DWieQBRg7CLhWID7iYQE9jECViTQDVjUAAjbQABgsAAgYQAAgogIgeQgVhQhZgvIgOgCQATgEAVAAQBGAAA0B6QApBjAABGQAADpksELQjYC+kgCVQhkA0ibBtQicBthuA6QhaAvgXCIIAAgBg");
	this.shape.setTransform(80,103.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("ArRQDQgUgNgPgPQgkghgOgnQgUg4gBgUIABg9QAAiPDWimQBSg/CJhVQBRgyCrhnQFDjECSiMQDWjPAAjVQAAhIgEgTQgLg9gvgoIhiADQgNAOgFASQgEAOAAAbQAAAqATAuQAUAvAAA+IAAA2QgIAjguAAQgUAAgPgDIgKgDQAAgCgDgEQgDgDAAgKQhFglgjiOQgPg7gIhGIgEg1QAAiOBohFQBhhACEAeQCJAeBbB1QBoCFAADKQAAELjaD3QinC7k4DAQhrBCiUBRIjfB5QjaB6AAA0QAAAwBoAlIA3AUIAHgBQAaAWAAAiQAAAPgfAfQg8AVgxAUQg5AVg0AAQg0AAgvgVgAHps9IAOACQBZAvAVBQQAIAeAAAoQAAAYgBAsQAADbjVDUQiVCTk9DEIj7CYQiLBWhRA7QjWCegDB+IABAOIAAABQABAwAKAcQANAlAZATIACAGQBCAtAxgUIAGAAQAQAAAKgLQAXgCAOgGIACgBQANAAAEgCQAHgDAAgKIgGgGQAAgNhUgjQgmgUgUgkIgIgTQgCgMAAgMQAAhTB2hIQCyhcB6hLQA2ghCshhQB2hDBqhJQEKi2CQjXQB+i+AAifQAAhxg3h7QhRi2icAAQiaAAgzBSQgPAYgFAoIgFAHQgIAOAAAWIABAsIAPBzQAZBxAvAAQAHAAAEgDIADgDQABgIAAgJIgBgDIABgKQAAgRgUg1QgUg1AAg6QAAgjAhguQAmgYBAAAQAcAAAgAFg");
	this.shape_1.setTransform(82.8,104.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCB55E").s().p("Aq+OwIgCgGQgZgTgNgkQgKgdgBgwQAXiIBagvQBug5CchuQCbhsBkg0QEgiVDYi+QEskLAAjpQAAhHgphiQg0h7hGAAQgVAAgTAEQhtgOg1AiQghAtAAAjQAAA7AUA1QAUA1AAARIgBAJIABADQAAAKgBAHIgDAEQgEADgHAAQgvAAgZhxIgPh0IgBgsQAAgVAIgPIAFgHQAFgnAPgZQAzhRCaAAQCcAABRC2QA3B6AABxQAACfh+C+QiQDYkKC2QhqBIh0BDQiuBig2AhQh6BLiyBcQh2BIAABTQAAALACAMIAIAUQAUAjAmAUQBUAjAAANIAGAHQAAAKgHACQgEACgNAAIgCABQgOAGgXADQgKALgQAAIgGAAQgOAFgRAAQgmAAgugfg");
	this.shape_2.setTransform(83.5,105);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,165.7,209.7);


(lib.Handle = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9E8C49").s().p("AqcMDQg6gsgCg1QgFiDCvh4QBRg4CrhMQBigsCwhrQCvhsBFg3QDdi2BoiNQCXjQAckAQANA6AAA5QAACfh+C+QiQDZkKC0QhpBJh1BDQitBhg2AhQh6BLizBcQh1BIAABTQAAAMABAMIAIATQAUAkAmAUQBVAjAAANIAFAGQAAAKgGADQgFABgLABIgCAAIgQAAQg5AAg2gog");
	this.shape.setTransform(86,118.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("ArRQDQhAgrgVg5QgUg4gBgUIABg9QAAiPDWimQBSg/CJhVQBRgyCrhnQFDjECSiMQDWjPAAjVQAAhIgEgTQgLg9gvgoIhiADQgNAOgFASQgEAOAAAbQAAAqATAuQAUAvAAA+IAAA2QgIAjguAAQgUAAgPgDIgKgDQAAgCgDgEQgDgDAAgKQhFglgjiOQgPg7gIhGIgEg1QAAiOBohFQBhhACEAeQCJAeBbB1QAPAUAOAWQBLB6AACrQAAELjaD3QinC7k4DAQhrBCiUBRIjfB5QjaB6AAA0QAAAwBoAlIA3AUIAHgBQAaAWAAAiQAAAPgfAfQg8AVgxAUQg5AVg0AAQg0AAgvgVgAH3s7QBZAvAVBQQAIAeAAAoQAAAYgBAsQAADbjVDUQiVCTk9DEIj7CYQiLBWhRA7QjWCegDB+IABAOQABAwAKAdQANAlAZATIACAGQBCAtAxgUIAGAAQAQAAAKgLQAXgCAOgGIACgBIACAAQALgBAEgBQAHgDAAgKIgGgGQAAgNhUgjQgmgUgUgkIgIgTQgCgMAAgMQAAhTB2hIQCyhcB6hLQA2ghCshhQB2hDBqhJQEKi2CQjXQB+i+AAifQAAg4gOg7QgNg7gcg+QhRi2icAAQiaAAgzBSQgPAYgFAoIgFAHQgIAOAAAWIABAsIAPBzQAZBxAvAAQAHAAAEgDIADgDQABgIAAgJIgBgDIABgKQAAgRgUg1QgUg1AAg6QAAgjAhguQAmgYBAAAQAhAAApAHg");
	this.shape_1.setTransform(82.8,104.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCB55E").s().p("Aq2OwIgDgGQgZgTgMgkQgLgdAAgxIgBgNQACh/DXieQBQg7CLhVID7iYQE9jECWiUQDUjUAAjbQACgsgBgXQAAgpgHgdQgVhQhagwQh3gTg4AlQgiAtAAAjQAAA7AVA1QAUA1AAARIgCAJIACADQAAAKgCAHIgDAEQgDADgIAAQguAAgZhxIgQh0IgBgsQABgVAIgPIAEgHQAFgnAQgZQAyhRCaAAQCcAABSC2QAbA9AOA7QgcEBiXDPQhoCOjdCzQhFA6ivBrQiwBrhiAtQirBMhRA3QivB4AFCDQACA2A6ArQA2ApA5AAIAQgBIgBABQgOAGgYADQgKALgQAAIgFAAQgPAFgRAAQgmAAgtgfg");
	this.shape_2.setTransform(82.8,105);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,165.7,209.7);


(lib.GregHead = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AjSAqQgLgLAAgMQAAgGABgEIg/ABQgSAEgaAIQgTAGgKAAQgLAAgJgBIgGgCIgDgRIAEgKQACgHAGgFQASgLBKgIQA9gQB+ANQAWATABAEQAAADgJAIQgIAFgGAEIgnABQACAFAAAHQAAAVgNAKQgKAHgPAAQgZAAgQgQgAD/AZQgVgEgJgJQgCgCgBgDIgCgEIACgFIgFgBQgKgCgKgEIgIgDIAAgDIgBgBQgBgCACgLQAFggBbAEQAoACA5AJIACAAIABAFQACAEgCALQgCAJg3ALIgVAEIgCAAIAAACQgFAageAAIgPgBg");
	this.shape.setTransform(96.2,91.1);

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D5AE81").s().p("AgRAlQgVgEgIgJQgDgCgBgDIgBgEIABgHIgEgBQgLgCgKgEIgIgBIAAgDIgBgBQgBgCACgLQAFggA3ATQA1ATA3AKIgfAFIgVAEIgCAAIAAACQgFAcgcAAIgPgBg");
	this.shape_1.setTransform(123.6,85.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgKMJQg8gJgsgNQhsgghKg0Qh0hQgjiCIAAgBIgiABQhlAAg5hEQgQgTgTgpQgPgmgBgBQgDgVAAgWQAAgsAFgTQAKgqAggKQgLgegKhqIgIhmQAAjkBXi1QAuhfBAg+QCRiNDxAXIAAgEID1AAQECAuCHCOQA8A/AjBjQAVA8AKA1QgDAAALAsQANAwABBvQAAB8gVA+QgDALACAmQACArgBAGQgIA6gMAvQgTBGgkBUQgDAHgEAbIgGAkQgIB1hLBUQhMBXhvAKIjPAEQhRgJglgFgAltGIQACAFAAAMQAAAYgTALIgNAHIAbA6QAUAsAdAdQBLBKBIAcQAYAJAbAIQCDAkDfgBQBmAABLgvQBlhAASiFIAAgCQgfAFgpAKQgsALggAGQhNAMhWgDQg4gChagOIhAgXQAAgdA0AHQCUAYCdgYQAwgHAjgJQAjgJAugSQAHgDALgYIAohaQAWgyAIgwQAHgqAAg5QAAgOACgJQgCgHAAgGIADgSIAAgBQANhTAEgyQAOiZgfhTQgRg1gghTQgziGiEhMQhZgziSghIj+AAQjbgViFCGQg6A5gqBXQgwBigVB/QgPBWAABHQAAAKAmDZQANABARAFQAkAKAXAQQAbATAAARIAAAAQAAANgJAFQgGACgLAAQgZAAgUgLQgcgPgfgHQgGAHgKAIQgbATAAAlIgBATQAAAKADAFQAjBCA2AdQAlAUAnAAQAMAAAhgLQAigMALAAQAUAAAHAPg");
	this.shape_2.setTransform(69.4,79.1);

	// Layer 3
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AiTAnQgLgLgBgPIABgSQAAgRAKgVQALgYAQABQAQACABARIgBAqIA2AXICIAAQASgHAFgNQADgJAAgdQgBgqAZAOQAZAPAAAmQAAAlgPARQggAhhgAAQiCAAgighg");
	this.shape_3.setTransform(102.1,108.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#725942").s().p("ABDGpQgngFgjgXIAAgBQAAgQgbgTQgXgQgkgLQgRgFgNAAQgnjbAAgKQAAhIAPhUQAWh+AwhjQAqhWA4g6QASAYA3CZQA7CiAKASIgBABIgFAGQgQAUgNApIgaBPQgQAsgCBHQgBBcAWB0QAFAYgeAAIgNgBg");
	this.shape_4.setTransform(22.6,58.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#917154").s().p("Am2A4Qg3iXgSgYQCFiGDbAVID+AAQCSAhBZAzQCEBMAzCEQhaBgj5ArQjzAqjcgmQgugHgiAnQgKgRg7iig");
	this.shape_5.setTransform(76,28.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BC9A72").s().p("ADcHhQiRg1hAh/QgehAgJhQIgXAIQghALgMAAQgoAAglgUQg1gdgkhCQgCgFAAgKIABgTQAAgjAagTQAKgIAHgHQAfAHAbAPQAUALAZAAQALAAAGgCQAJgFAAgNQAmAYAnAEQAqADgGgaQgWhzABhcQAChHAPgsIAahRQAOgpAQgVIAEgFQAoBnAZB4QAWBpANCGQAKBcAaCQQACABgCAAIAGAjIADANQARBdAOBSIAJAxIgBAAIADASQgXgGgWgIg");
	this.shape_6.setTransform(30.9,101.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#D5AE81").s().p("AgoJGIAAAAQgZAEgXAAQhyABhYgYIgDgTIABAAIgIgwQgOhTgShcIgCgNIgHgkQACABgCgBQgaiRgJhdQgOiEgWhqQgZh3gnhoIABgBQAigoAuAIQDdAmDygqQD5grBahgQAgBTARA1QAfBTgOCZQgHBNgNBMQAAAHACAGQgCAKAAANQAAA4gHAqQgIAwgWAyIgoBaIgJARIAQAKQgFAWgDACIgBABIAAAAQAAAXgCgDIgFAGQgFAFgKAAIABACIADAGIACgBIAAACIgBAKIAAABIAAAMQAAAagBAEQgCAGgEAFQAHAKAAAIQAAARgYAdQgZAhgnAdQhkBPhnAAQg9gBgtgWgAiFFJIApAMIAaAAIgRgOQgFgFAAgBIAAAAQgVAFgYADgAESEvQACABAAANQAAAGgSAOIAagMQASgHAOAAIAFAAIgBgBQgTgLgegGQAAABAAAAQABABAAAAQAAAAABAAQAAABABAAgAAGBeQgIAWAAAQIgCAUQABAPAKAMQAiAgCEAAQBgAAAfggQAQgSAAgnQAAgmgZgOQgZgPAAArQABAcgDAKQgFAOgSAHIiKAAIg2gZIAAgqQAAgRgQgBIgCgBQgOAAgLAXg");
	this.shape_7.setTransform(86.5,94.8);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,138.9,158.3);


(lib.GotIt = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("got it", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 158;
	this.text.setTransform(94.2,9.1,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AsvkbIZfAAQCOAAAACNIAAEdQAACNiOAAI5fAAQiOAAAAiNIAAkdQAAiNCOAAg");
	this.shape.setTransform(95.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("AsvEcQiOAAAAiNIAAkdQAAiNCOAAIZfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_1.setTransform(95.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AsvEcQiOAAAAiNIAAkdQAAiNCOAAIZfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_2.setTransform(104.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,202.2,67.5);


(lib.Girl03 = function() {
	this.initialize();

	// 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#748475").s().p("AiWCoQg/g4gLhUIAAgBQALgsAggeQAjgeAxACQA0ABAbAtQAaArgYAsQgNAYgZAJQgUAHgXgFQgPgBgKAOQgKANACAGQgLgOABgPQABgPAKgFIgIgLQgLgZAJgZQAJgYAbgEQAZgEALASQAKARgLAPQAFgbgYgBQgYgCgDATQgDANAJAKQAJAKAOADQAiAHAKgkQAEgUgLgRQgMgRgUgIQgxgWghAuQgdArARA5QAOAwAhAXQAMAKADgFQABgEgDgMQgDgMAEgMQADgNALgIQgGAQAHAaQAJAaASALQALAIAEgDQAFgDgFgJQgJgOgBgSQgCgYAPgOQgJAgAhAlQAlAqBAgRQAjgJAngvQAfgnAGgUQACgHgGgGQgEgGgIAAQgUABgLAqQgOgJADgWQADgWAMgFQg6gIgoAVQgkAUADAeQABAlAhAEQAiADAEghQACgTgUgCQgTgBgEATQgDgTALgKQAJgKARABQAQABALAKQALANgBAUQgCAkgoAPQgoAQgdgdQgfgeANguQAMguAogSQApgSA3AEQAzAEApAUQgOBRg+A1QhAA2hSAAQhVAAhBg6gAALgGQgFgLgOgXIgDgEQgdgoABgtQABgYAMghQgigDgcAPQgaARAFAWQAGAYAWgKQAIgFACgGQACgIgKgHQALgCAHAIQAIAHABALQADAcgjAHQgXAFgQgLQgPgKgEgUQgEgTAIgVQAHgUAUgLIAEgDQAzgbA4AAQAqAAAnAPQgwABgYA0QgWA0AhAnQARAUAcAEQAZABAYgNQAYgMAHgVQAJgYgQgZQgbghggAVQgMAHgEANQgEAOAIALQANATAUgIQAVgIgGgSQAQAdgiAOQggAOgWgcQgPgTAFgXQAFgXAUgPQAUgQAZAAQAcAAAYAVQAjAogMAuQgKAkgfAVQgRANgZAFQgeADgbgHQgCALgEAGQgFAJgMAFQADgPgHgPgADagMQgCgFgFgIQgHALgSAHQgTAGgXgBQAegKAPghQAPghgIghQAdA0AAA7IAAALQgDgNgEgKgAjNheQARgjAbgcQgGAGgGASQgIARgDASQAHgEALABQAPgCALALQgXADgWASQgdAWgLAgQADgoARglg");
	this.shape.setTransform(75.5,44.5);

	// Layer 7
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#748475").s().p("Ai8AMQgugcAAgZIABgMQAAAAAAAAQAAABAAgBQABAAAAAAQAAAAAAgBIAJAAQAMAeAfAWQA7ApB1ACQBbACBDgMQAQgDAXgUQAXgTAJgVIAGAAIADADIABANQAAAQgfAVQgbATgTAEQgiAIhEACIg7ABIgIAAQhxAAhAgrg");
	this.shape_1.setTransform(72.7,13.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ai7BjQhjgYAAghIABgHQA6gNCNABQATACAQAQQAQAPgBARIgFAFQgHAHgQAFQgjAMg+ADIgagGgABMBNQgNgPgKgtQAcgJAyAAQBIAAAqATQAjAQAJAaQACAIghAEQg2AGhkAIQgTgIgJgKgAkdgjQgFgZADgSQAOgSAggFQAegGAqAGQAIACAUAOQATANABAAIACAaIAAATIgMgCQgmgDgkADQgSABgQADIgnARQgDgGgEgVgACiglQg3gFgtAJIgSAEIgBgMQAAgYAXgLQBOgPBUANQABAMgGAbQAAAHACAGQgmgKgZgBg");
	this.shape_2.setTransform(76.7,148.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#BABABA").s().p("AinAoQg1gEgHgDIAJgYQAFgOAQgIIANgGIAGgCIARgEIgBAHQAAAfBjAYIAaAGQgdABgjABQgRgBgxgEgACDANQAAgNgEgKQAHgBAEgGQAJgKAFgFQAJgHAQgEQAKArANAPQAIAJAUAJIhIAFQgUABgHACQACgKAAgSg");
	this.shape_3.setTransform(64.4,154.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AkrMpQg9gKgIgLQgHgMAAgWQAAgzA0gjIAhgQQgMgUgCglIgCgRQAAgQAGgXQBKgjBAAGQAiAEAxAVQAYALADAVQACAMgDAZQAAARgDAOQAZAJAfAPIAAABQAGgLAOgJIgBgdQAAglAEgGQAHgMAvgcQBjgSBcASQALAIAHAHIAIABQAKAJAEAJQAEAJAAAOQAAAkgJAVIANAHQBAAkgEA1QgBAPgEAGQgGAHgSAEQgOAEigAUQhCAHg3gHQgWgFgCgHIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAkHKwIgQAEIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgCQA+gDAjgMQAQgFAHgHIAFgFQABgRgQgPQgQgQgTgCIgUAAQh9AAg2AMgABLKeQgQAEgJAIQgFAEgIAKQgFAHgGAAQADAMAAAOQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gGQAhgEgCgIQgJgagjgQQgqgThIAAIgFAAQguAAgbAJgAjbImQggAFgOASQgDASAFAZQAEAVADAGIAngRQAQgDASgBQAkgDAmADIAMACIAAgTIgCgaQgBAAgTgNQgUgOgIgCQgWgDgSAAQgSAAgOADgAD2JxQgBgGAAgHQAGgbgBgMQhUgNhOAPQgXALAAAYIABAMIASgEQAtgJA3AFQAZABAlAKIAAAAgAEoJMIABgFIAAgFIgBAKgAhurIQhMgYgFgYQgEgPAggRQAggRBUgCQBRgBBNATQBNATgJAUQgEAHgEAPQgGANgUALQgZANhaAAQhbgBgxgQg");
	this.shape_4.setTransform(74.5,83.2);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#748474").s().p("AE4FAIgtgGQhJgKh6gOIgJgBIgkgEIhKgLIAAAAQgXgNgegHIgPgCIAHgTQANgmAJg7IAOhSQAFgaAAgcIAAgKIAAgCIgBgJIgBgHQAHgCAHgBQAFgBAJANQAHAKAFAKQADAGAIAwQAOA1AlAsQBCBKB4AkIBMAXIAMAFQAYANAJARQAAABgGAAIgWgBgAkwD8IgBgBIgCgBIgBAAIgGgGIgEAAQgNAAgFhHIgDhIIABhAQABgHAHgTQACg0AzhsQATgoAMgUQAAgPgCgIQASgSAUgOQAtgfBKgZQgVAPgiAvQgnA0gUAuQgHAOgFAgQgCAQgFADIAAACQgDAcghCMQgHAkgDAyQgCAXAAA1IADABIAAAIIAAAKQADAHAKACQgfgCgRgLg");
	this.shape_5.setTransform(37.8,43.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#A5BCA5").s().p("AAQFjQgJgRgWgNIgMgFIhMgXQh3gkhChKQgngsgPg1QgIgwgDgGQgEgMgIgKQgJgNgFABQgHABgGACQgDgQgHgFQgKgHgQAVIgHAIIAAAEQAAALgGAbIgDALIAAAVQAAAJgEAXIAAABIgEAWIgCAHIgDANIgKAvIAEgJIgJAhQgKAjAAAJIABANIAAAIIAAACQgkAHgcgBIALgCIgRAAQgLADgJAAIgDAAIgGAAQgKgCgCgHIAAgKIAAgIIgEgBQABg1ABgXQADgyAIgkQAhiMADgcIAAgCQAFgDACgQQAFggAGgOQAVguAmg0QAjgvAVgPIB7gqQAmgNAQgHIAUgEIAagHIEuAAQBfAOAYAAQALAAAFACQAsATBZAxQAJAFAaATQBBApAUAUQAJAIAkBRQBBEegPA3QgaA2g+gOQgGgBABgLQACgMAAgFQgEg9ACgGQAHgjACgVQADgXgBgWIgDgVIgBgPIgBgRIgBgDQgCgMgJgeQgJgggLgSQgNgWgTgWQgbghgTgEQgVgEAAAiQAAAFApA5QArA5AGA/QAFA6gcArQgOAWgQANQAAALACADIACADQAEAEATAAQAFAAAJgEIAMgGIADA5QACAlAEAMIABADIgJACIgOAGQgRAGgXAFQhBAOikACIgmABQhLAAg3gEg");
	this.shape_6.setTransform(70.2,39.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#396086").s().p("AAqCaQg0ingRgcQhFh0gciBIAHghIAHglQAeAMBhARIAIABIAFAAIASADQABB6AcChQAFAVAQCpQAPCfAMAwQgigrgxigg");
	this.shape_7.setTransform(36.8,109.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#396087").s().p("AhYFPQgWAAgOgEIgigNQgQgIgIgGIgGgFQhjkwgLgcQgziLAAiQIADgSQAcCBBGB0QARAcA1CnQAyCgAhArIAVAagADfFJIhAgKIAAgIQgCgagDh/IgDiOQAQAdAXAYQALALA6CCQA0ByAnALIgQAAQg4AAg3gGg");
	this.shape_8.setTransform(58.9,114.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#371E2D").s().p("ABhAMIhKgMIAAgLIAAgCIADAAIAPACQAeAHAXALIAAAAIABAAIALAHIgJgCgAhpAAQAJAAALgDIARAAIgLACIgVABIgFAAg");
	this.shape_9.setTransform(23.5,70.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#4B7DAF").s().p("AkwFJQgMgwgPigQgRiogEgWQgdihAAh6QBEAJCjAPQBZAICggFQCtgGBXgRIAXgGIgECgQgHBkgdBVQgQAuggBUQgFALgLBoIgOBtIgsADQgwAGggAAQgngKgzhzQg4iCgLgLQgXgYgRgcIgFhWQAEg3gCgYQgnADgFAoQgDAUAFAVIAEBcIADCMIADCGIAAAKQgBAIgVADIiqANg");
	this.shape_10.setTransform(79.9,112.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("ACQLvQg4gFgPgHIgRgIIgFgCIgBgBIgKAFQgqAVhKAEQgmAFgngGQgXgEhOgUQgZgFgDgDQgfgchGjbQhRj5gBidQAAhBAFgVQACgKADgHIgpAAIgKAAIgTABQgJAAgNgCQgPgDgWgGIgNgEIgFgCQgXgHgKgEIgHABQgJgGgGgfQgEgWAAgNQgEgXACggQAGgsAAgPQAAhcAehjIABgCIAUgpIAOgbIAOghQAHgMAQgYIADgCIAAAAIAXglQACgDAOgKQAPgMAJgFQBwhCCvghICGgTIDaAAQAnAFA3AKQA4ALABACIACABIA2AaQA3AaAYAKQApASAeAWQBjBLARBpIAcCqQAMBHAAAWQABAxgBAHIABALQAAAfgWAWQgMANgTAGQgJAEgMACQgMACgOAAIgMgBIADAbQAFAoAAAOQAABcgCAdQgGBTgZBNQgOArgjC0QgeCZgRAYQABACgDAFQgDAGgGABQAEABgVAGIgrAIQg5AKgzAAQgyAAg6gKgAmeAJIgHAhIgDATQAACQAzCKQAKAdBkExIAGAFQAIAHAQAHIAiANQAOAEAVABIAOAAICogNQAVgDABgIIAAgKIgDiGIgDiMIgEheQgFgVADgUQAFgoAngDQACAYgEA3IAFBYIAECNQACCAACAaIAAAHIBAAKQBAAHA/gBQAgAAAwgGIAsgDIAOhtQALhoAFgLQAghUAQgwQAdhVAHhkIAEigIgXAGQhXARitAGQiiAFhXgIQijgPhEgJIgSgCIgFgBIgIgBQhjgOgegNIgHAjgADdgIQCkgCBBgOQAXgFARgHIAOgFIAJgDIgBgDQgEgLgCglIgDg6IgMAGQgJAEgFAAQgTAAgEgDIgCgEQgCgCAAgLQAQgNAOgWQAcgrgFg6QgGg/grg7Qgpg6AAgFQAAghAVAEQATAEAbAgQATAWANAXQALARAJAhQAJAgACAMIACAUIABAPIADAUQABAWgDAYQgCAUgHAjQgCAGAEA+QAAAEgCANQgBAKAGACQA+ANAag2QAPg3hBkgQgkhQgJgJQgUgUhBgpQgagSgJgFQhZgxgsgTQgFgCgLAAQgYAAhfgOIkuAAIgaAGIgUAEQgQAHgmANIh7AqQhKAZgtAgQgVAOgSARQACAJAAAOQgMAVgSAnQgzBtgCA1QgHATgCAHIAABAIACBIQAGBIANAAIADgBIAHAGIABABIACABIAAAAQARALAfACIAGABIADAAIAEAAIAWgBQAcABAkgIIAAgCIAAgHIgBgOQAAgIAKgkIAJggIgEAJIAKgwIADgNIACgHIAEgVIAAgCQAEgXAAgJIAAgVIADgKQAGgbAAgLIAAgEIAHgLQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAnIgHASIgDAAIAAACIAAANIBKANIAJACIgLgHIAAgBIBLALIAkAFIAJABQB7AOBJAJIAqAGQAeAEgCgDQBFAEBjgBgAp3hFIAAAAIAAgBg");
	this.shape_11.setTransform(66.5,76.8);

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0.7,133,163.8);


(lib.Girl02 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9B4D00").s().p("AAWAeQglgFgxABQgWAAgHgYIgFgeQgEgQBlAcQBkAaAAALIgBAGIgCADIgCACQgEACgIAAQgTAAgpgEg");
	this.shape.setTransform(72.3,3.1);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC6600").s().p("AEVBMIAAgKQAAgagbgdQggggg1gPIgEgCIg6gUIgygRIgMgEQBwAMBFAVQAgAKAdAQIATALQASAJAAAJQAFAJgGAOQgCACgDAQIgEATQgCAHgGAHIgIACIgEABQgHgEgGgGgAhUgJQgFgBgDgDQgDACgPgEIgzgMIgRgCIgTAAIgEgBIgBAAQgqgEgogJQglgJAAgDQAAgQBBgJQBBgKAnAMIABAAIAIAAQAcABASAHQATAIALAQQAKAJAJAWIADAMIABAHIgKACIgegPg");
	this.shape_1.setTransform(62,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5A986F").s().p("AgQgGIABAAQAVAGALAHQgRgHgQgGg");
	this.shape_2.setTransform(88.4,-0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhODQIgHgRQAAgMAKgFQgEgJgHgqQgIgwgEgQIgZhlIgFAAQgJAAgSgNIgngdQg5gmg0ALIgTgPQAAgRAaAAIABAAIAEAAIATABIARACIA0ALQAPAEACgBQAEACAFABIAeARIAKAGIABABIgBgCIgBgBIAAgBIAAgEIgBgJIgCgNQgJgVgLgJQgLgRgTgIQgRgHgdAAIgHAAIAGgGQAFgDAFgBQAFgIAUgBIAUACIgCgDQAAgBAAgBQAAAAAAAAQAAgBAAAAQAAgBABAAQAZgPBqAJQBzAJBaAkQASAHARAIIAFADQBGAjAAAkQAAAagHAYQgGAXgMALQAVAXACAEQAMAQAIAYQAaBHACAcQAPACAAAXQAAAOgCADQgGALgaAAIgVgCIgJgBIgCgMQAIgNALgKQgHgKgTg5QgYhIgPgjQgJAAgFgBIgDgBQAAgBAAAAQAAAAgBgBQAAAAAAAAQgBAAAAAAQgCgBAAgIIAKgRIAJgCQAFgIACgHIAEgSQAEgQABgDQAGgOgEgIQgBgLgSgKIgTgLQgcgPgggKQhGgWhtgMIALAFIAwAQIA6AUIAEACQA2APAgAjQAaAcAAAaIAAALIgCAVIgDACQgEACgKAAQgOAAgLghIgJgcIgPABQg6AMgdAAQhCAAgpgTQAFAJABAIIAoAbQAuAhAAAAQAAAIgJAEQgFADgIAAIg9giQAEATAAAdQACACAMA3QAIAfAFBDQANAIAAAIQAAAJgPAJQgMAIgGAAQgKAAgJgSgAg7huQATAHAOABQgNgHgKgDIgKACgAiRjGIABAAIABgCIgMgBIAKADg");
	this.shape_3.setTransform(71.4,16.9);

	// Layer 4
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAiBSQhVAAgtgZQhGgngFgjQgEgYAqgUQAqgUBTgBQBQAAA2AVQA2AVgJAgQgDAJgEAXQgGAVgSARQgWAVhQAAIgEgBg");
	this.shape_4.setTransform(77.8,11.5);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#4E2A3F").ss(1,1,1).p("AD1AKQgZgFgegEQhSgKg4gGQhogLgvALQgTAFg1AHQgkADgQAJQgMAGgJAH");
	this.shape_5.setTransform(69.8,2.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#9B4D00").s().p("AELE6QhLgKiBgOQg7gHgzgJIAAAAQgXgNgegGIgPgDIAHgSQANgnAJg6IAOhSQAFgaAAgdIAAgKIAAgBIgBgJIgBgHQAHgDAHgBQAFAAAJAMQAHALAFAJQADAHAIAvQAOA2AlAsQAoAsA3AWQAgAMA+AMQA4AKAVAKQAhASAIAoQgqgDgfgEgAkwD8IgBAAIgCgBIgBgBIgGgGIgEABQgNAAgFhIIgDhIIABhAQABgHAHgTQACgzAzhtQATgnAMgVQAAgOgCgJQASgRAUgOQAtggBKgZQgVAQgiAvQgnAzgUAuQgHAPgFAgQgCAPgFAEIAAACQgDAcghCLQgHAlgDAxQgCAYAAA1IADAAIAAAIIAAAKQADAIAKABQgfgCgRgLg");
	this.shape_6.setTransform(37.8,43.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#A55200").s().p("AinAoQg1gEgHgDIAJgYQAFgOAQgIIANgGIAGgCIARgEIgBAHQAAAfBjAYIAaAGQgdABgjABQgRgBgxgEgACDANQAAgNgEgKQAHgBAEgGQAJgKAFgFQAJgHAQgEQAKArANAPQAIAJAUAJIhIAFQgUABgHACQACgKAAgSg");
	this.shape_7.setTransform(64.4,154.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CC6600").s().p("Ah4MVQhjgZAAggIABgHQA6gNCMABQAUACAOAPQAQAPgBASIgFAFQgHAGgRAGQggALg+AEIgagGgACPL+QgNgPgKgtQAcgJAyABQBIAAAqASQAjAQAJAbQACAHghAFQg2AGhkAIQgUgJgIgKgAARgnQgHgngfgSQgWgLg3gKQg+gLgggNQg3gWgogsQgngsgPg1QgHgwgEgGQgEgMgHgKQgJgNgGABQgGABgHACQgDgSgHgFQgKgHgPAVIgIAKIAAAEQAAALgGAbIgDALIAAAVQAAAJgDAXIAAABIgFAWIgBAHIgDANIgLAvIAEgJIgIAhQgLAjABAJIABANIgBAIIAAACQgkAHgcgBIALgCIgRAAQgLADgJAAIgDAAIgFAAQgLgCgCgHIAAgKIAAgIIgEgBQABg1ABgXQAEgyAHgkQAhiOADgcIAAgCQAFgDACgQQAFggAHgOQAUguAmg0QAjgvAVgPIB8gqQA5gUAHgGQADgCgBgFIgDgGQAQgJAlgFQA0gHAUgFQAvgLBnALQA5AGBRAMQAeAEAZAFQgCAEAAAFQAAATABgKQAbAbBwAuIAkAYQBAApAVAUQANANAVBAIAEAMIACAFIgBAPIABAFIgBAAIAhDAIAMBLQAGAjAFAQQgbA2g9gOQgHgBABgLQADgMgBgFQgDg9ABgGQAHgjACgVQADgXgBgWIgDgVIgBgPIgBgRIgBgDQgGgOgJgXQgPgtgBgMQgNgWgSgWQgbghgUgEQgVgEAAAiQAAAFAqA5QAqA7AGA/QAFA6gcArQgOAWgQANQABALACADIACADQADAEATAAQAGAAAIgEIAMgGIADA5QACArAFAJQgHABgQAHQgRAGgXAFQhBAOijACIgnAAQhKAAg4gDg");
	this.shape_8.setTransform(70.1,79.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#396086").s().p("AgbAuQhFhxgciCIAHghIAHglQAeANBhAQIAIABIAFABIASACQABB6AcCfQARBfAfB+Qhbh2g9hog");
	this.shape_9.setTransform(36.8,100.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#396087").s().p("AhYD2QgygBgugRQgogTgDAAQgFgRgJgYQgchHgQgrQhAisAAhtIADgSQAcCBBGB0QA/BmBaB2IAVAaIgEAAIgFAAIgFAAgADfDvIhAgKIAAgHIgIh1QAQAdAXAYQA7A7BlAcIgQABQg4AAg3gHg");
	this.shape_10.setTransform(58.9,105.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#371E2D").s().p("ABhAMIhKgMIAAgLIAAgCIADAAIAPACQAeAHAXALIAAAAIABAAIALAHIgJgCgAhpAAQAJAAALgDIARAAIgLACIgVABIgFAAg");
	this.shape_11.setTransform(23.5,70.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#4B7DAF").s().p("AkwDvQgfh9gRhfQgdigAAh6QBEAJCjAPQBZAICggFQCtgGBXgQIAXgHIgECgQgHBkgdBUQgQAvggBUIgRAuIggABIgMACQguAFgvABQhlgcg4g7QgXgZgRgcIgFhYQAEg1gCgYQgnADgFAmQgDAVAFAVIAEBdIAGBgIAAAKQgBAIgVADQgoAChCAFIhAAFIgUgag");
	this.shape_12.setTransform(79.9,103.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AjbMmQg9gKgHgLQgIgMAAgVQAAg0A0gjQBHgkAjgFQAQgEASgBQAkgDAnADQAnAEBDAhIABABQANgUAogNQAQgGAQgCQAtgKA4AFQAnADBKAXIATAJQA/AlgDA1QgBAOgFAGQgFAHgTAFQgOADigAUQhCAHg3gHQgWgFgBgGIgDgQIgEAGQgVAYhZAMQg6AEg0AAQg1AAgwgEgAi3KtIgQAFIgGABIgOAHQgPAIgGAPIgIAYQAHADA1AFQAxAEARAAQAiAAAdgCQA9gDAigLQAQgGAIgHIAFgEQAAgSgPgPQgQgQgUgCIgRAAQh9AAg3AMgACbKbQgPAFgKAHQgFAEgIAKQgFAHgGAAQAEAMAAAOQAAARgCAKQAHgBAUgBIBIgGQBkgHA2gGQAhgFgDgIQgJgagjgQQgpgShIgBIgFAAQgvAAgbAJgACQH6Qg5gFgOgHIgRgIIgFgDIgBAAIgKAFQgrAVhJADQhOALiLgjQgZgFgDgDQgbgZg0iaQg7iugGh6QgDg0AIgiQACgKADgHIgpAAIgKAAIgTABQgJAAgNgCQgUgEgegJQgbgIgLgFIgHABQgJgFgGggQgEgWAAgNQgEgXACggQAGgsAAgPQAAhcAehjIABgCIAUgpIAOgbIAOghQAHgMAQgYIADgCIAAAAIAXglQACgDAOgKQAPgLAJgGQB/hKCogaQABABACAFIABABIADAHQABAEgDACQgIAHg5ATIh7ArQhKAYgtAgQgVAOgSASQACAIAAAOQgMAVgSAoQgzBsgCA1QgHATgCAIIAAA/IACBJQAGBHANAAIADAAIAHAGIABAAIACABIAAABQARALAfABIAGABIADAAIAEAAIAWgBQAcABAkgIIAAgCIAAgHIgBgNQAAgJAKgkIAJggIgEAJIAKgwIADgMIACgHIAEgWIAAgCQAEgXAAgIIAAgWIADgKQAGgbAAgLIAAgEIAHgLQAQgUAKAHQAHAFADASIAAAHIABAJIAAABIAAALQAAAegFAaIgOBSQgIA7gOAmIgHATIgDAAIAAABIAAANIBKANIAJACIgLgHIAAgBQA1AJA6AHQCBAPBMAKQAcAEAqACQBFAEBjgBQCkgCBBgOQAXgFARgGQAQgHAHgBQgFgJgCgrIgDg5IgMAFQgJAFgFAAQgTgBgEgDIgCgEQgCgCAAgLQAQgNAOgWQAcgrgFg6QgGg/grg7Qgpg6AAgEQAAgiAVAEQATAEAbAhQATAVANAXQAAAMAQAtQAIAWAHAPIACAUIABAPIADAUQABAXgDAXQgCAUgHAjQgCAHAEA9QAAAFgCAMQgBALAGABQA+AOAag2QgEgQgGgjIgMhLIghjAIABAAIgBgFIABgQIgCgEIgEgNQgWhAgNgMQgUgVhBgoIgjgYQhwgvgbgbQgBALAAgTQAAgGACgDQAFgHAUAJIAyAZIBTAlQApASAeAXQBuBTAABhIAAADQAHA2AWB9QASBnAAATIgBAOIABALQAAAfgWAWQgbAbgzAAIgMgBIADAbQAFAqAAAOQAABbgCAcQgGBTgZBOIgqCFQgVA8gUAcQABABgDAGQgDAGgGABQAEABgVAGQgVAFgWADQg5AJg5AAQg5AAg6gJgAmeg3IgHAhIgDASQAABrBACuQAPAqAcBIQAKAYAFARQADAAAoATQAuARAxAAIAKAAIAEAAIBAgFQBCgFAmgCQAVgDABgIIAAgKIgGhgIgEhdQgFgVADgVQAFgoAngDQACAYgEA3IAFBYIAIB1IAAAHIBAAKQBAAHA/gBQAvgBAugFIAMgCIAggBIARguQAghUAQgvQAdhWAHhkIAEieIgXAHQhXAQitAGQiiAFhXgIQijgPhEgJIgSgCIgFgBIgIgBQhjgQgegMIgHAlgAp3iIIAAAAIAAAAgAjTsVQAJgGAMgHQgMAHgJAGg");
	this.shape_13.setTransform(66.5,83.5);

	// Layer 5
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#CC6600").s().p("Ak6CBQgZgOgEgXQgCgEAAggQAAhDA9g+QBOhQCRgPIDfACIBiAfQBWAcAAALQAAAdgZArQgbAugsApQhxBpiTABQjtAAhDgog");
	this.shape_14.setTransform(40.4,14.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AlzCIQggggAAg2QAAhQBNhEQBXhOCbgdQCFgHB0AHQAOAEB0AeQBtAhAAAWQgBAmgRAvQgYA6gvAxQiECGkPAAQjUAAhHhKgAkhhNQg9A/AABDQAAAfABAFQAFAWAZAOQBDAoDsAAQCTAAByhqQAsgpAagtQAagrAAgeQgBgKhVgcIhjggIjegBQiRAPhOBPg");
	this.shape_15.setTransform(41,14.9);

	this.addChild(this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-6.1,133,170.6);


(lib.GHAIR = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3A1B00").s().p("AJQMLQAAhvgriIQgPgvgog8QgXgjhhiAQgbglg0gzQg1gzgughQiLhchmgXQhmgYgJA7QgRAGgQALQipiniTkyQCqhPD3gKIDfAAQEABBCRCbQB3B/AfCqQBCDrg6EpQAABahmDvIAAhAgAp5oNQhIhfgNhvIgCgVQBIg0BsgmIDjAAQCMAtAZAaQANANACATIAAAwQAAAwgIAYQgGASgUAMIgNAJQjPANiXBIQgYAMgWAMIgxg2g");
	this.shape.setTransform(91.8,89.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAEKaQlVkGgBk+QAAjuB6inQBNhrB6hDQASgLAXgKQCSEyCqCpQhRAygjCeQgaB1gBDGIACBHQAFBUAKBGQAJBCANAuQichghMg7gAlKm8QgIgQgBgZIABgzQABh3A8hSQAiguA1glIACAVQANBvBIBfIAxA2QhyBChNBoQhFgtgQgeg");
	this.shape_1.setTransform(38.8,96.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#020100").s().p("AIPQbQAAgbAbhaIAdhaQAVhBAHgiQALg0AAhiQAAhlgghYQglh4hjiAQjGj9k3goIgDgCQgPAKgMANIgPAQQhHBRgRB2QgIA4gGBmQgEBQAAAcQAABUAzC0QAZBXAZBJIACAGIAAACQAAASgEgFQAAAAgBAAQAAABAAAAQAAAAgBABQAAABgBABQgEAEgMAAIgEAAIgEAAQhLAAjjiyQjJiegpg4Qh6imgXhJQgMgmgCg0QgBgQAAhmQAAjwBuixQhPgLgXhbQgJghAAgoIABgkQAAiUBDhXQAqg4BPgwQBLgwBtgpIDtAAQCdAkArA8QAQAXAEAhQABAJAAAwQAAA8gHAYIgGATIANAAIAAgEIDjAAQEXBECcCtQB1CBApCwQAdB9gBCVQgBBpgTCtQgRCXhhD2QhFCuhWCqQgHAEgFAAQgMAAAAgZgAmrpzQgWAKgVAMQh6BChNBrQh5CnAADwQAAE8FXEHQBMA7CdBgQgOgvgJhCQgKhGgEhUIgChHQAAjFAah2QAkicBQg0QAPgLASgGQAJg9BkAYQBnAXCMBeQAuAiA0AzQA0AwAcAlQBgCBAYAiQAnA8APAvQAsCJAABvIgBBAQBnjvAAhbQA5kphBjqQgfirh3h+QiRibkAhBIjfAAQj3AJiqBPgAqDulQg1AmghAtQg9BTAAB3IgBAzQAAAYAJAQQAPAfBFAtQBNhpByhBQAXgNAYgLQCWhJDQgNIANgJQAUgLAGgTQAIgXAAgxIAAgvQgCgUgNgNQgagaiLgtIjjAAQhtAmhIA0g");
	this.shape_2.setTransform(84.1,107.7);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.1,215.3);


(lib.FrankHead = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("AlpBZIAAilID6AAIAACFQgOAFgtAJQg2AJhOAJgACKBMIAAiUIAcgNICxgDQADAGAIAMQAIANAAATQgBAZgIAfQgKAkgNAZg");
	this.shape.setTransform(98.6,88.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7A4D30").s().p("ADUJCQhIgchLhJQgdgdgTgsIgag6IAMgGQASgMAAgXQAAgMgBgFQgHgPgSAAQgMAAghAMQggAKgMABQgngBglgTQg1gdgkhBQgCgGAAgJIAAgUQAAgkAbgUQAKgHAGgHQAfAHAcAPQATALAZAAQALAAAGgCQAJgFAAgNQAlAXAqAFQArAFgJgcQgoh3gSicQgSijAUhoQAShcAjgxIAEgFQBwCUAWCxQANBrAGD4QAUCaAZCEQAYCCAPBbQgbgHgXgJg");
	this.shape_1.setTransform(30.8,89.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5B371E").s().p("ABgGmQgqgFglgXQAAgRgZgTQgWgQgjgKQgSgFgNAAQgljagBgKQABhHAPhTQAUh+AwhiQAqhWA3g5QArA4AgA2IAMAVIgBABIgEAGQglAxgRBcQgVBoASChQASCbAqB6QAIAYggAAIgMgBg");
	this.shape_2.setTransform(20,58.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#804E29").s().p("AkiBDQgrgLglAqIgMgVQggg2grg1QCFiFDZAVID9AAQDHAsBxBiQgjCLjVAGIgUABQirAAk1hPg");
	this.shape_3.setTransform(70.6,19.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#975F3C").s().p("AkEJzQgPhbgYiCQgZiEgUibQgGj3gNhrQgWixhwiVIABgBQAlgrArALQFJBUCqgFQDWgGAjiMQA9A1AzB4QAZA9AOAyQAWBJgKCcIgCAbQi1gYg2BOQgMATgFATIhXADIhAABIgEgbQgGgpgOgMIkqAAQgRAZgFAqQgCASgBAjQgDAEABANQABAMACAAQABAaAFAaQAJA1AWASIBXAAQCmABArhAQAPgWAAggIAAghICTgEIACAMIAAA/QACAxAXATIAiAAQgGAHAAAUQABAdgDAJQgFAPgSAHIiJAAIg2gaQACgfgBgKQgBgRgPgBQgQgCgLAYQgKAVAAAQIgCAUQABAQAMALQAiAhCCAAQBgAAAfghQAQgSAAgmQAAgmgZgOICYAAQgJApgTAqIgoBaQgLAYgGADQguARgjAJQgjAKgvAHQidAWiRgXQgzgHAAAdIBAAXQBXAOA4ACQBVADBMgMQAhgGArgLQAogKAggFIAAACQgSCEhkA/QhLAvhlAAIgSAAQjQAAh+gjg");
	this.shape_4.setTransform(83.2,85.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgSMEQg8gJgrgNQhrgghKgzQhyhQgkiCIAAAAIghAAQhlABg4hDQgogwgDguIgIgFQgDgUAAgXQAAgsAEgSQALgqAggKQgMgegJhpIgIhlQAAjiBWi0QAthfBAg9QCQiMDvAWIAAgDID0AAQEBAtCFCNQA8A/AiBhQAWA9AKA0QgEAAAMAsQANAwAABuQAAA4gEArQAOAMAEALQACAFAAAtQAAATgUBDQgLAkgKAWQgHAmgIAgQgTBGgkBUQgDAGgEAaIgGAkQgIB1hKBTQhMBXhuAKIjOAEIh1gOgAlyGFQABAGAAALQAAAYgSALIgOAHIAcA5QATAsAdAdQBLBJBIAdQAXAIAbAIQCCAlDegCQBlgBBLguQBkg/ASiEIAAgDQggAGgoAKQgrALghAGQhMAMhVgDQg4gChZgPIhAgWQAAgdAzAHQCTAXCdgXQAvgGAjgLQAjgIAugSQAGgDALgYIAohZQATgrAJgpIiYAAQAZAOAAAnQAAAmgQASQgfAghgABQiCgBgiggQgMgLgBgQIACgUQAAgQAKgVQALgYAQABQAPACABAQQABAKgCAgIA2AaICJAAQASgHAFgQQADgIgBgdQAAgUAGgIIgiAAQgXgSgCgxIAAg/IgCgNIiTAFIAAAgQAAAhgPAWQgrBAimgBIhXAAQgWgSgJg1QgFgagBgaQgCgBgBgMQgBgNADgDQABglACgSQAFgoARgZIEqAAQAOALAGAoIAEAbIBAgBIBXgDQAFgTAMgTQA2hNC1AZIACgcQAKibgWhJQgOgzgZg8Qgzh4g9g2QhxhjjIgsIj8AAQjZgUiFCEQg6A5gpBWQgwBhgVB+QgPBWAABHQAAAKAmDXQANABARAFQAkAKAWAQQAbATAAARQAAANgJAFQgGACgLAAQgZAAgTgLQgcgPgfgIQgGAIgKAHQgbAUAAAkIAAATQAAAKACAGQAkBAA1AdQAlAUAnAAQAMAAAggLQAhgLAMAAQAUAAAHAOgAhLC0IA6AAQBMgIA3gKQAtgIAOgFIAAiHIj4AAgAHEAFIgcAMIAACXIDAADQANgZAJgkQAJgiAAgYQAAgTgHgOQgIgLgDgGg");
	this.shape_5.setTransform(70,79);

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AjSAqQgLgLAAgMQAAgGABgEIg/ABQgSAEgaAIQgTAGgKAAQgLAAgJgBIgGgCIgDgRIAEgKQACgHAGgFQASgLBKgIQA9gQB+ANQAWATABAEQAAADgJAIQgIAFgGAEIgnABQACAFAAAHQAAAVgNAKQgKAHgPAAQgZAAgQgQgAD/AZQgVgEgJgJQgCgCgBgDIgCgEIACgFIgFgBQgKgCgKgEIgIgDIAAgDIgBgBQgBgCACgLQAFggBbAEQAoACA5AJIACAAIABAFQACAEgCALQgCAJg3ALIgVAEIgCAAIAAACQgFAageAAIgPgBg");
	this.shape_6.setTransform(96.2,88.1);

	// Layer 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EACFB7").s().p("AjCAwIgLgCQAAgBgEgDQgDgCAAgSIh5AKQgPABgKgCIgIgCQAAAAAAgBQAAAAAAAAQgBAAAAABQgBAAAAABQgDACgBgOQgBgOAFgEQAHgHAdgFQAtgIDZgQIAqgEQAAABBIgBICTgFQAzgDAkACQAyACAWAIQAXAIAAAQQAAAOgMADIgWAAIgOAAQgBAXgCAHIgdAGQgsgCgMgQQgDgDgBgUIghgCQkEAHgKgEIgBAAIgxAJQgBAOgLALQgNALgUACIgPAAIgOAAg");
	this.shape_7.setTransform(94.6,87.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#DEBBA4").s().p("ADVJFQhIgbhMhLQgdgcgTgsIgZg6IAKgHQATgMAAgXQAAgMgBgGQgHgOgSAAQgLAAgiAMQghAKgMABQgngBgmgTQg1gdgjhCQgDgGAAgKIABgTQAAgkAagUQAKgHAHgIQAfAIAcAPQATALAaAAQAKAAAHgCQAIgFABgNQAkAXArAFQArAEgKgbQgnh5gSicQgTikAVhoQARhdAkgyIAFgGQBwCWAVCyQAOBsAHD6QATCbAZCEQAZCEAPBbQgbgHgYgKg");
	this.shape_8.setTransform(30.8,89.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#AA8462").s().p("ABhGpQgrgFgkgXIAAAAQAAgRgZgTQgXgQgkgKQgRgFgNgBQgnjbAAgKQAAhHAPhUQAWh/AwhiQAqhXA4g5QArA4AgA2IANAVIgBACIgFAFQgmAygRBdQgVBoATCiQASCcApB7QAJAXgfAAIgNAAg");
	this.shape_9.setTransform(19.9,58.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#CEA077").s().p("AkkBDQgsgLgkArIgMgVQghg2grg2QCFiGDcAVID+AAQDIAtByBiQgjCNjYAGIgTAAQisAAk3hQg");
	this.shape_10.setTransform(70.8,19.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgKMJQg8gJgsgNQhsgghKg0Qh0hQgjiCIAAgBIgiABQhlAAg5hEQgogvgCgvIgJgFQgDgVAAgWQAAgsAFgTQAKgqAggKQgLgegKhqIgIhmQAAjkBXi1QAuhfBAg+QCRiNDxAXIAAgEID1AAQECAuCHCOQA8A/AjBjQAVA8AKA1QgDAAALAsQANAwABBvQAAB8gVA+QgDALACAmQACArgBAGQgIA6gMAvQgTBGgkBUQgDAHgEAbIgGAkQgIB1hLBUQhMBXhvAKIjPAEQhRgJglgFgAltGIQACAFAAAMQAAAYgTALIgNAHIAbA6QAUAsAdAdQBLBKBIAcQAYAJAbAIQCDAkDfgBQBmAABLgvQBlhAASiFIAAgCQgfAFgpAKQgsALggAGQhNAMhWgDQg4gChagOIhAgXQAAgdA0AHQCUAYCdgYQAwgHAjgJQAjgJAugSQAHgDALgYIAohaQAWgyAIgwQAHgqAAg5QAAgOACgJQgCgHAAgGQAKhOAFhPQAKidgWhKQgOgygZg9Qg0h4g9g2QhyhkjJgtIj+AAQjbgViFCGQg6A5gqBXQgwBigVB/QgPBWAABHQAAAKAmDZQANABARAFQAkAKAXAQQAbATAAARIAAAAQAAANgJAFQgGACgLAAQgZAAgUgLQgcgPgfgHQgGAHgKAIQgbATAAAlIgBATQAAAKADAFQAjBCA2AdQAlAUAnAAQAMAAAhgLQAigMALAAQAUAAAHAPgACyFPQgMgLgBgQIACgUQAAgQAKgVQALgZAQACQAQACAAAQIAAAqIA2AZICKAAQASgGAFgPQADgKgBgcQAAgrAZAPQAZAOAAAnQAAAmgQASQgfAhhgAAQiEAAgighg");
	this.shape_11.setTransform(69.4,79.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EAD0B7").s().p("AkGJ3QgPhcgZiDQgZiEgTicQgHj5gNhsQgWiyhwiWIABgBQAkgrAsALQFMBUCqgFQDYgGAjiNQA9A2A0B5QAZA8AOAzQAWBKgKCcQgFBQgKBNQAAAHACAGQgCAJAAAOQAAA6gHAqQgIAvgWAzIgoBaQgLAYgHADQguARgjAJQgjAKgwAHQidAXiSgYQg0gHAAAeIBAAXQBYAOA4ACQBWADBNgNQAggFAsgLQApgLAfgEIAAACQgSCFhlA/QhLAvhmABIgMAAQjVAAiBgkgAAkC1QgKAVAAARIgCAUQABAPAMALQAiAhCEAAQBgAAAfghQAQgRAAgnQAAgmgZgPQgZgOAAAqQABAdgDAJQgFAPgSAHIiKAAIg2gZIAAgqQAAgRgQgCIgCAAQgOAAgLAXgAhiA4QADADAAABIALACQAPABAOgBQAUgCANgLQAMgLABgOIAvgJIAAAAQAKAEEGgJIAhADQABAVADADQAMAQAtACIAcgGQADgHABgXIAOAAIAVAAQAMgDAAgQQAAgOgWgIQgWgIgzgCQgkgCgyADIiTAFQhKABAAgBIgrAEQjXAQgsAGQgdAFgIAIQgEAFABAOQABAOACgCQABgBAAAAQABAAAAAAQAAAAABAAQAAAAAAAAIAHACQALACAOgBIB6gKQAAASADACg");
	this.shape_12.setTransform(83.5,86.1);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,139.8,158.3);


(lib.FinalScorecopy = function() {
	this.initialize();

	// Layer 2
	this.mc_Score = new cjs.Text("20465", "30px 'Laffayette Comic Pro'", "#FFFFFF");
	this.mc_Score.name = "mc_Score";
	this.mc_Score.textAlign = "center";
	this.mc_Score.lineHeight = 32;
	this.mc_Score.setTransform(112.7,23);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("ASTEPIAAAtIgwAAMgjIAAAIgtAAIAAgtIAAoYIAAgyIAtAAMAjIAAAIAwAAIAAAtIAAAFg");
	this.shape.setTransform(117.1,35.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9E8C49").s().p("ARjElIAAodMgjIAAAIAAIdIgtAAIAAoXIAAgyIAtAAMAjIAAAIAvAAIAAAtIAAAFIAAIXg");
	this.shape_1.setTransform(117.1,33.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#474747").s().p("Aw4B3IAAhNIAAigMAhwAAAIAADtg");
	this.shape_2.setTransform(117.1,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9E8C48").s().p("ARjAWMgjIAAAIgtAAIAAgrIAtAAMAjIAAAIAvAAIAAArg");
	this.shape_3.setTransform(117.1,64.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#666666").s().p("Aw4BqIAAjTMAhwAAAIAADTg");
	this.shape_4.setTransform(117.1,23.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CCB55E").s().p("AxkEPIAAodMAjIAAAIAAIdgAw2gLIAACgIAABOMAhwAAAIAAjuIAAjVMghwAAAg");
	this.shape_5.setTransform(117,35.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.mc_Score);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1.5,2.2,237.2,66.3);


(lib.FB_butn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhDFDIAAknIhjAAIAAhxIBjAAIAAhUQAAhJApgpQAlgnBEABQA5gBAfAFIAABnIg9AAQghAAgNAPQgLANAAAcIAABJIByAAIgPBxIhjAAIAAEng");
	this.shape.setTransform(25.8,21.4,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3A5A94").s().p("AiMC/QgxAAAAgzIAAkXQAAgzAxAAIEZAAQAxAAAAAzIAAEXQAAAzgxAAg");
	this.shape_1.setTransform(19,19.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.1,0.1,38.2,38.2);


(lib.EndWhiteBoxcopy = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EA2wgbXMhtfAAAQiOAAAACOIAALTIAAJSIAAduQAACOCOAAMBtfAAAQCOAAAAiOIAA9uIAApSIAArTQAAiOiOAAg");
	this.shape.setTransform(363.7,195.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C1C1C1").s().p("Eg49ALZIAApSIAArRQAAiOCOAAMBtfAAAQCOAAAACOIAALRIAAJSg");
	this.shape_1.setTransform(363.7,92.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Eg2vAP/QiOAAAAiOIAA9vMBx7AAAIAAdvQAACOiOAAg");
	this.shape_2.setTransform(363.7,268.1);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2.9,18,733.3,354.5);


(lib.EndWhiteBox = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EA2wgbXMhtfAAAQiOAAAACOIAALTIAAMkIAAacQAACOCOAAMBtfAAAQCOAAAAiOIAA6cIAAskIAArTQAAiOiOAAg");
	this.shape.setTransform(363.7,195.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C1C1C1").s().p("Eg49ANCIAAskIAArRQAAiOCOAAMBtfAAAQCOAAAACOIAALRIAAMkg");
	this.shape_1.setTransform(363.7,103.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Eg2vAOWQiOAAAAiOIAA6cMBx7AAAIAAacQAACOiOAAg");
	this.shape_2.setTransform(363.7,278.6);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2.9,18,733.3,354.5);


(lib.DrillBoxcopy2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("Ai2CwQgKgCgHgFIgPgLQgGgDgBgJQgGgbAKgXQAFgMAKgCIAJgCIABgDIAAgEQgCgwACgsIABgZIACgbIAAgKQgBgGgHgGIgTgQQgIgHAAgOQABgSAJgNQAGgHAGgEQAHgEAJgCIABAGIAAAEIgDAgQgBAHACADQACACAIABIASgBIASgCQAHgBABgGIABgLIAAgaQAFgBAFADQAUAQAAAZQAAAKgCAFIgEAIQgDAEgEABQgGADgBAEQgBADgDACIgFAEIgCAOIgBAPIgCAkQgCAeABAbIgBANQgBAIADAKQABAHAHACIAEACQAOAIAGANQAJAWgGAZQgDAQgOAFIgLAGIgIACIgXAAQgMABgKgDgAitBeQgLAJgEACIgHAFQgEACAAADQgBACABAEQACAIAAALIAAAJQAGAFAKAFIATAIIAIgFIAHgIIAPgOIAAAAQgBgMADgOQABgFgBgDIglgQgAgLCJIgBgEIgBgHIACgGQAFgLAGgDIADgCQAEgCABgDIABgGIAAgYIgBgVIgCgwIAAgbIAAgHIgMgGQgMgGgBgOQgBgIABgEQACgHADgEQAFgJAIgEIABADIAAACIgCAWQgBAHAFABQAZAEASgEQAGgOgBgQQAOAEAFANQAFANgIAMIgHAHQgEAFgFABQgHAEAAAKIABAWIgBBTIgBAFIACAHQAJACAFAJQAEAGADAKQADAKgCANQgCAEgHADIgBgSQAAgLgFgHQgGACgIAAIgOAAIgLACIgEACQgCABAAAEIABAHIACANQAAAHgBAGQgRgFgCgSgAC4CWIgDgBIgDAAQgIgIgCgIIgGgTQgDgLAAgIIgBgSIgCgTIgCgSQgBgLAAgHQABgHgCgKIgBgGIABgJIgEgCQgGAAgFgDIgJgHQgKgKgBgMQgBgRAKgSIAGgHIAHgGIACgBIADAAIAFAoIAEAAIADAAIAbgCQAEgBACgCQACgDAAgEIABgPIAAgIQAAgEACgEQAGACAFAHQAIAKADAGQAGAJACAJQACAKgEAJIgDAGQgIAIgKADQgGACAAAFIAFAmIACAQQACAJgBAHIAAAIIACAIIAAACIACAaIABAZIgBAHQgBADgDADQgHAIgHAAIgFAAgACyByQgEAEAAAFQAAAEADADQACADAFACQAFABAFgGQACgEgCgGQgCgFgEgCIgDgBQgDAAgEACg");
	this.shape.setTransform(55.1,52.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#231F20").s().p("AjBC3QgNgHgIgHQgLgJgGgLQgJgQAFgUIAFgPQABgDACgDIAIgLQAFgGAHgDQAFgCABgHIABgZIABgaQgBgcACgPIABgTIABgTIABgGQABgGgFgDIgPgIQgIgFgFgEQgKgLADgPQADgUANgUQALgRAUgEIAIgCQAEgBABABIADAFQACAFgBAHQgDANgBAOIABAJQAIACAJgBIARgDIABgGIAAgTQAAgIgCgEQgCgEADgDQACgEAFABIAPAEQAEABAEAEIAHAHQAKAMACAMIACALIABAMQABAGgDAHIgFAMIgEAHQgDAHgHAEIgIAGIgDBHQgBAmACAgIAPAIQANAIAKASQAFALgCASQgCAPgHALIgJANQgFAIgHADIgGADQgIAFgJACIgTACIgDAAQgYAAgRgJgAjFitQgHAEgFAHQgJANgBASQgBAOAJAHIATAQQAHAGABAGIAAAKIgCAbIgBAZQgDAsADAwIgBAEIAAADIgJACQgLABgFAMQgKAYAGAbQACAIAGAEIAPALQAGAFAKACQAKACANAAIAXAAIAIgDIAKgFQAOgFAEgRQAFgYgJgXQgGgNgNgHIgFgCQgGgCgBgHQgDgLABgHIABgNQgCgbADgeIACgkIAAgPIACgOIAGgEQADgCABgDQABgEAFgDQAEgBADgEIAEgIQACgFAAgKQAAgagUgPQgEgEgFACIAAAaIgBALQgCAGgGABIgSACIgTABQgIgBgBgCQgDgDABgHIADggIABgFIgBgFQgKACgGAEgAAKCoIgHgCQgRgFgDgUIgCgNQgBgOAMgNIAGgFQAEgCAAgIQABgdgBgQIgCgdIAAgoIgKgGQgKgGgEgMQgFgVAIgOQALgUARgHIAEAAQAEALgBAIIgCAUQAPAFAOgEQAFgKgEgKQgDgIAFgHQAFgCAGADIAJAFIAJAGQALAMACAOQACAPgLANIgFAHIgHAGQgGADAAAIIABANIgCBZIAIAHQAOAMACAXIAAAHIAAAHQACAKgJAHIgNAJQgFACgGgCQgEgEAAgHIAAgKIgBgLQgCgBgDAAIgFABIgKACIgBAHIAAAIIAAAQQgFADgFAAIgFgBgAgKhUQgDAEgBAGQgBAFAAAIQACAOAMAFIAMAHIAAAGIAAAdIACAuIABAWIAAAXIgBAHQgCADgEACIgCABQgHAEgFAKIgBAHIABAHIAAAEQADASAQAFQACgGgBgHIgBgNIgBgHQAAgEABgBIAFgDIAKgBIAOAAQAJAAAGgCQAEAHABALIAAASQAHgDADgEQABgNgDgKQgDgLgDgGQgGgIgJgDIgBgGIAAgFIABhTIAAgXQgBgJAIgEQAEgCAEgEIAHgIQAJgLgFgNQgFgNgPgEQACAPgGAPQgTADgYgDQgHgBAAgHIADgWIAAgDIgCgCQgFAEgGAJgAixCeQgJgFgGgFIAAgJQAAgLgCgJQgCgDABgCQABgDADgCIAIgFQAEgCALgJIAGgEIAkAQQACACgBAGQgDANABAMIAAABIgQAOIgGAIIgJAFIgTgIgAivBxQgIAEABALIACAMIAJAFIALAEQAEACADgEIAMgMIADgFIAAgIQABgEgBgDQgLgJgNgCIgNAJgACxCdIgJgKQgEgGgCgGIgEgSIgDgRIgEgyIAAgDIgDgSIgBgTIgCgMQgNgFgJgHQgNgIgCgRQgCgJADgJIACgJIACgHQAEgIAFgGIARgQIAIgFQAFgCAEADQAEAHAAAJIABAVQAAAGAFgBIASgCQADgDAAgEIAAgIIAAgVIABgEIAFgEQAIACAIAGQAFAEAHAIQAMANADAOQAFAYgKARQgEAIgIADIgJAFIAFAkIAEAVIADAUQABAPAAAdQAAANgDAMQgGATgTABIgJAAQgGAAgDgCgACPhdIgGAGIgGAHQgKARABASQABAMAJAJIAJAIQAGADAGAAIADABIAAAJIAAAHQACAKAAAGQgBAIACALIACASIABASIACASQAAAJADALIAFATQADAIAHAIIADAAIAEAAQAKADAJgKQACgDABgDIABgIIAAgZIgCgZIgBgDIgBgIIgBgHQABgHgBgJIgDgQIgFgmQABgGAFgBQAKgDAIgJIAEgFQADgKgCgJQgCgJgFgJQgEgHgHgJQgFgHgHgCQgCAEAAAEIAAAIIAAAOQgBAFgBACQgCACgFABIgbADIgDAAIgDAAIgFgoIgDAAIgDABgAC8CGQgEgCgDgDQgCgDAAgEQgBgFAFgEQAFgEAFACQAEACACAGQACAGgDADQgDAGgFAAIgCAAgAC6B4QAAAAAAAAQAAABAAAAQAAABAAAAQAAABABABQAAAAABABQAAAAABAAQAAABABAAQAAAAABAAIABgBQABgBAAAAQAAgBAAAAQAAAAAAgBQAAAAgBgBQAAgBAAAAQgBgBAAAAQAAAAAAgBQgBAAAAAAIgCAAIgCACg");
	this.shape_1.setTransform(54.7,53);

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B796C4").s().p("AmchEIAJgIIABgBIBHAAIAJgFIIsghQAWAfA7A4QBBA9AiAnIpfAsg");
	this.shape_2.setTransform(44.5,14.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#836B8C").s().p("AAJD3Qg4g2gxgcIgNimQgFgsAChSQAChCADgqIAHhIQAAgdgHgUIACgBIDaC7IAAINIgDADQgxg+g0gxgAhOACICWBzIAAhaIiWhyg");
	this.shape_3.setTransform(13.7,43.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#A083AB").s().p("AkyjwIJfgsIABACIAFAFQgHALgGBAQgHBEgBAEIAAFmIgPAHQiAALi+AMIi9AMQg4AFgOAKgAB3jkIAABbICYgMIAAhRIAAgKg");
	this.shape_4.setTransform(55.8,50.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFF99").s().p("AhLgmICWgMIAAAKIAABPIiWAMg");
	this.shape_5.setTransform(75.3,31.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CCCCCC").s().p("AhLgLIAAhbICWByIAABag");
	this.shape_6.setTransform(13.3,44.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("Ai9GRIgBgDQgEAEgEABQhohxhlhbIgJAEIgDgPIgKgJIAJAEQgPhIgEhjIgChuQAAhUABgYQADhKALgyQABAAABAAQAAAAAAAAQAAgBAAAAQAAgBgBgCIABgEIAAAAIABgDIABAAIAAgCIABgCQABgDADgCIAJgDQAOgQBBAAIAAgDIIdgeQAOADASAAIABAAQAJALALADQAWAVAwA2IBjBwIgBBOQgCBIgEAWIAAFsQgRAbh6AHQh/ACgTACQkkAbgoAAgAmNlRIgJAIIgCABQAHAUgBAcIgGBJQgEAqgBBCQgCBSAEAqIANCoQAyAcA6A2QAzAxAyA9IADgCQAOgKA3gGIC8gLQC/gMCAgLIAPgHIAAlmQACgEAGhEQAHhAAGgMIgEgEIgCgCQgigohBg+Qg7g4gWggIosAhIgJAFIhHAAg");
	this.shape_7.setTransform(43.8,40.3);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,87.7,80.7);


(lib.DrillBoxcopy = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AinArQgDgCgBgIIAAgIQgBgIAIgDIAEgDQAFgLgIgFQgFgEAAgFQAAgFAEgGIAHgHIAIgHIAKAGIAGgGQAFgFADABQADAAAFAGIACACIAJgBQAHAIgDALIgCAKQgCAFgEACQgCAAAAAGQABAEAFAAQANgCAYADIAKAAIAXgCIAXgBIAfgBIAdgCQATgDAJAAIAsgCIAcgBIAVgCQAEAFAAAKQgCADgDABIgGACIhSACIgnACIgvAEQgeADgUADIg2AIQgGACgHgBQgQAAgQADIgFAAQgEAAgCgBgAiOgWQgDACgBADQgCAEADAGQAEAFAFABQADABAEgDQAFgDAAgEQABgGgCgGIgCgDIgEgDIgLAGg");
	this.shape.setTransform(50.8,68.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCCCCC").s().p("AhEBwIgCgHQgCgGgHgBQgHgCgEAGIgGAJQgDABgCgDIgDgEIgHgGQAGgOAMgPIAWgZIAVgYIAOADIAPACQASABALgLQAMgLABgRIABgFQADgPgLgIIgEgDIANgRIAOgQQAFgFAWgaIAJgJIAXAHIgGALIgDAIIASAJQABAEgFAIQgGAIADAFIAOAGQgBAFgDADIgGAHQgDADAAADQABADAEADIAIAIIgHAFIgLAGQgEAEAAAAQgBADAEAFIAKAMQgDAFgDACIgHACIgKAEQgGACgBADQgBADACAFIADALIgBADIgNABIgIABQgJABABALIACAPQgDAAgKgBQgIgCgFADIgGARQgEgBgHgEIgHgGQgMADgDACQgDADgEAKQAAAAgBABQgBAAAAAAQgBAAAAgBQgBAAAAgBIgDgDQgEgFgGgBQgGAAgFAFIgKAJQgEgBgDgEg");
	this.shape_1.setTransform(54.2,50.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC0000").s().p("Ah9CoIAIgRQADgGgBgDQgBgDgGgBQgJgDgHgHQgFgFgGgKQgRgbABgfIABgRIABgSQAEgSAJgRQAOgeAagaIAKgIIAwgbQAOgHALgCIAfgFQATgCAOADQAlAHATAKIAVgUIALgOQAHgIAGgFIAIgIIALgLQAFgFAEAIIgDAGIhXBpIgDAEIgEAFQgKAHgMAPIgVAXQgDACAAADQABACADADIAPAMIgBALIgCAKQgBAFgEAEQgHAFgFACQgHAEgHgBQgMAAgJgEIgKgEQgEACgFAFIgHAJIgKAMQgQARgSAZIgQAXIgRAVQgBABAAAAQgBABAAAAQgBAAAAABQgBAAAAAAIgFgDgAAAAcQgCADgBAGQAAAGACACQABADAFABQAGABAEgCQADgCABgEQACgEgBgEQgDgKgJgDIgIAHg");
	this.shape_2.setTransform(51.4,45.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#231F20").s().p("Ai7DmIgCgfIAAgEQgHgNAPgGIgEgJQgDgHACgHQACgIAFgFQALgKAGgEQAJgGALgDIAGgPQgGgLgEgEQgageAAgxIAAgNQgCgfARgiQASgjASgTQAOgQANgHIAXgNIAWgLQAXgLAagFQAbgFAfAIIAiAJQALADAIgIIAqgyQAIgJAIgCQAGACAFAGIAJAKQAGAGgGAIQgJANgFAGQgQARgaAmIgBAEQACACAEABIAHADQAHADACAEQACAFgEAHIgFAJIAOAGQAGACgBAHIgEAOQALAHAAAFQABAFgJAKIgEAEIAJAMQAJAQgRAIIgKADQAEAPAEAHQAEAGgDAFQgCAEgIABQgPADgGAEIAFAOQACALgKACIgYAEQgCAEAAAEIACAIQACAMgFAEQgEADgLgCIgJgCIgCAAIgEANQgCAFgDABQgDABgEgCIgdgNQgEAFgCAHIgEAMQgBAEgEABQgDACgDgCQgEgCgDgEIgBgEIgCgFIgEgHIgQALQgNAMgGgQIgCgDIgCgEQgEACgEAEIgHAHQgEADgCABQgGABgDgGIgDgKIgEgGIgEAEIgDADIgCADIAAAEQAFAIABALIgBATQAEADAGAAQAEABAGAAIAsgEIAEAAQAJABAOgCIAZgBIADAAQAMgDARgBIAdgCIAGgCIAGAAQAKABAPgBIAZgDQAMAAAGgFQAPAFABAQIABAfQAAAIgKAEIgGABQgkAFgagCQgUgCgTADIgdACQgYAAgMADIgFABQgQAAgkAFIhIAIQgRABgJABIgHABQgRAAgOgMgAibCRIgHAIQgEAFAAAFQAAAGAEADQAJAHgFAMIgFACQgHAEAAAIIABAIQABAHADADQADACAHgBQARgEAQAAQAGABAGgBIA2gIQAVgEAdgDIAwgEIAngCIBRgCIAGgBQADgBACgEQABgKgEgGIgWABIgcADIgrADQgKAAgTACIgdADIgeABIgYABIgXABIgJABQgZgDgNABQgFABgBgFQAAgFACgCQAFgCACgGIABgJQADgLgHgJIgIABIgDgCQgFgGgDAAQgCAAgGAFIgGAFIgKgFIgHAGgAAMioIgfAFQgLACgOAHIgwAbIgKAIQgaAagOAeQgJATgEASIgBASIgBAPQgBAfARAbQAGAKAFAFQAHAHAJADQAGABABADQABADgDAGIgIARIAFADQABAAAAAAQABAAAAgBQAAAAABAAQAAgBABgBIARgVIAQgXQASgZAQgRIAKgMIAHgJQAFgFAEgCIAKAEQAHAEAOAAQAHABAHgEQAFgCAHgFQAEgEABgDIACgKIABgLIgPgMQgDgDgBgCQAAgDADgDIAVgYQAMgPAKgHIAEgFIADgEIBXhpIADgGQgEgIgFAFIgLALIgIAIQgGAFgHAIIgLAOIgVAUQgTgKglgHQgIgBgJAAIgQAAgAgrBiQAGABACAHIADAHQACAEAFABIAJgJQAFgFAGAAQAFABAEAFIACADQABABAAAAQABABAAAAQABAAABAAQAAgBABAAQAEgKACgDQADgCAMgDIAKAGQAGAEAFAAIAFgQQAFgDAIABQAKACAEgBIgCgOQgCgLAKgCIAIAAIANgCIAAgCIgDgLQgCgFACgDQABgDAFgCIALgEIAGgDQAEgCADgEIgLgNQgDgEAAgDQAAgBAFgDIAKgHIAIgEIgJgIQgEgDAAgDQAAgDADgEIAFgGQAEgEAAgEIgNgGQgDgGAFgHQAGgIgCgFIgSgIIAEgJIAFgLIgXgHIgIAKQgXAagFAFIgNAQIgNAQIADAEQALAHgDAPIAAAGQgCARgMALQgMAKgSgBIgNgBIgPgDIgVAXIgWAaQgLAPgHANIAIAGIACAFQACACADAAIAHgJQADgFAGAAIACAAgAiHCxQgFgBgDgGQgDgFABgFQABgCADgCIALgGIAEADIADADQACAFgBAGQgBAFgEACQgDADgDAAIgCAAgAANABQgFgBgDgBQgCgCAAgGQABgGADgDIAJgHQAJADADAKQABAEgCAEQgBAEgDAAQgDACgEAAIgDgBg");
	this.shape_3.setTransform(50.6,50.2);

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFF99").s().p("AhLgmICWgMIAABZIiWAMg");
	this.shape_4.setTransform(75.3,58.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CCCCCC").s().p("AhLgLIAAhbICWByIAABag");
	this.shape_5.setTransform(13.3,44.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#848484").s().p("AAJD3Qg4g2gxgcIgNimQgFgsAChSQAChCADgqIAHhIQAAgdgHgUIACgBIDaC7IAAINIgDADQgxg+g0gxgAhOACICWBzIAAhaIiWhyg");
	this.shape_6.setTransform(13.7,43.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#BCBCBC").s().p("AmchEIAJgIIABgBIBHAAIAJgFIIsghQAWAfA7A4QBBA9AiAnIpfAsg");
	this.shape_7.setTransform(44.5,14.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ai9GRIgBgDQgEAEgEABQhohxhlhbIgJAEIgDgPIgKgJIAJAEQgPhIgEhjIgChuQAAhUABgYQADhKALgyQABAAABAAQAAAAAAAAQAAgBAAAAQAAgBgBgCIABgEIAAAAIABgDIABAAIAAgCIABgCQABgDADgCIAJgDQAOgQBBAAIAAgDIIdgeQAOADASAAIABAAQAJALALADQAWAVAwA2IBjBwIgBBOQgCBIgEAWIAAFsQgRAbh6AHQh/ACgTACQkkAbgoAAgAmNlRIgJAIIgCABQAHAUgBAcIgGBJQgEAqgBBCQgCBSAEAqIANCoQAyAcA6A2QAzAxAyA9IADgCQAOgKA3gGIC8gLQC/gMCAgLIAPgHIAAlmQACgEAGhEQAHhAAGgMIgEgEIgCgCQgigohBg+Qg7g4gWggIosAhIgJAFIhHAAg");
	this.shape_8.setTransform(43.8,40.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#999999").s().p("AkyjwIJfgsIABACIAFAFQgHALgGBAQgHBEgBAEIAAFmIgPAHQiAALi+AMIi9AMQg4AFgOAKgAB3AtIAABbICYgNIAAhbg");
	this.shape_9.setTransform(55.8,50.1);

	this.addChild(this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,87.7,80.7);


(lib.drill = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("AE4DCIgThAIAEgDIAZgIIADAHIACAGQACAWAHAbQACAHgCADQgDADgIAAgAlQixQAAgBAAgBQAAAAAAgBQABAAAAgBQAAAAABgBIAFgBIACgBIBVgGIAkgCIALgBQAGAAAFACQAAAIgBACQgBACgEABIgRABIhJABIgZACIgXABQgHAAgBgEg");
	this.shape.setTransform(36.6,35.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5A88E2").s().p("ABRDbIgBhAIgBgKQgBgGgFgEIgTADIgEgDQgIgRgRAEIgIABIgIAAQgJgKAAgOIAAgdQgBgRABgMQAAgJgBgDQgDgDgJACIgbAEQgPADgMABQgSABgmgCIgIgDIgIgDQgCgIABgLIACgRQACgdgCgTIgCgUIgBgVIgFgwIgBgGQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAAAAAAAQAFgHgBgJIgBgRIAAgSIANgCIAWAAQAHAUAAAUIABBLQABAXAKAMIAHAIQAJAJAJgDQAJgCADgMIAHggQAHgjgCggIgBgXIABgYQAHgFALADQAEAEABAFIAAAJIgBA4IgCA3QgCAXAUARQAHAIAKgDQAKgDAEgMQACgGACgMQAHg2gEgiQgCgMgBgSIgCgeQAKgIAPADIAFASQACALgBAJQgEAuAKA4QAFAZAXAGIAFgFIAEgFQANgbgCglIgCg0IgDgZQgBgJAFgEQAGgFAIADQAZAHAFAEQAIAGAHAYQASA7AHAoIAQBIIAGAlIAZBnIABAJIgIAJIgeAZQgSAOgRAFIgGACQgMAJgIADQgIACgPAAIgXABgAjAgKQgXgBgUgFQgKgsAAgXIACg/IAXgFIArgEQAFAAAJADQAMAdgBAjQgBAIACAMIABATQACAWgFAPQgMACgPAAIgMAAg");
	this.shape_1.setTransform(52.2,24.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFCC").s().p("ABSCXQgFgEgBgGQgDgYACgVIARgCQAFALAAARIABAcQgFAEgEAAQgEAAgDgDgAjVAAQgEgBABgKIAEgoQAOgEATAAIAiAAQACAMABAPIAAAbIgGABIgFABQgdADgTAAQgJAAgDgEgABogQQgEgKAAgKIAEhqIACgGIALgDIAMgCIACAEIAFAxQACAdgBATQgBASgEAUIgDAGIgEAHQgQAAgFgPgAAjgPQgGgGgDgFQgGgLAAgVIgBhYIAkgDIACAMIABA2QAAAOgDARIgIAfIgBAEQgCADgDABIgBAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAgBgBgADDgZQgGgQgBgIQgGguACgtIACgKQALgDAMACIACAGQAFA9ABAUQABAXgNAWIgBAAQgHAAgCgGg");
	this.shape_2.setTransform(41.1,19.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#231F20").s().p("AECEZQgJgigFgaIgEgLIgeACQgRABgFgVIgBgNIACgsIgXgEIgHgRIgGgBIgQACQgQABgGgLQgCgEgIgCIgYgJQgGgNgFgTIgHgfIgFgCQgMACgTgBQgXgCgHABQgHAAgIgFQgEgCgLgJIAAgdIgOgDIgsgBQgVgCgOgIIgGgTIhCAAQgJAAgEgCQgDgCgDgIIgCgIQgDgIgIAAIgWAAQgNAAgJACIgMABIgNAAIgjACQgUACgWgEQgHgBgDgDQgCgCgBgHIgEgcQAGgMAOgCIBbgOIA/gHIAIgLIBHgGQACgFAAgHIgBgLQAAgIADgEQADgDAHAAIAXAAIAVgDIAqgCQAFgGgBgMIgBgNQAAgNAMgCQAZgIAPgBQATgDAJAUQALABAKgJQALgLAagCQASgBAFAPIACAGQAGgEAAgGQACgIACgCQAXgNAiAMQADABACAEIADAGQAEAKAJgDIAHgOQAUgEAbADQAkAEAMAiQAKAbAHAbIANBBIAKAvIAHAuIAJArQAHAiAMAkQAKAggYATIgXAVQgOAMgMAGQgJAFABALIAFAaIAGAdQABAMgDAEQgDAGgBAGgAENDIIgEAEIASA/IAOAAQAIAAACgDQACgCgBgHQgHgbgCgWIgCgHIgDgHgADPBtIABAKIABBAIATAAIAXgBQAQAAAHgCQAIgDANgJIAFgCQARgFASgOIAfgZIAIgJIgCgJIgYhnIgHgjIgPhKQgHgogSg7QgIgYgHgGQgFgEgagHQgIgDgFAFQgFAEABAJIACAZIADA0QABAlgNAbIgEAFIgEAFQgYgGgFgZQgKg4AFguQABgJgCgLIgFgSQgQgDgJAIIABAeQABASACAMQAFAigIA2QgBAMgCAGQgFAMgJADQgKADgJgIQgUgRABgXIACg3IACg4IgBgJQgBgFgEgEQgLgDgHAFIAAAYIAAAXQADAggIAjIgGAgQgEAMgJACQgJADgJgJIgGgIQgKgMgBgXIgBhLQgBgUgHgUIgUAAIgNACIAAASIACARQAAAJgEAHQAAAAgBAAQAAABAAAAQAAABAAAAQABABAAABIABAGIAFAwIABAVIABAUQACATgCAdIgBATQgBALACAIIAHADIAGACQAmABATgBQALAAAQgCIAagEQAJgCADADQAEADgBAHQgCAMABARIABAdQAAAOAJAKIAJAAIAIgBQARgEAIARIADADIAUgDQAEAEACAGgABbAOQgCAVADAYQABAGAFAEQAHAGAJgHIgBgcQAAgRgFgLgAhdi7IgWAFIgCA/QgBAXALAsQATAFAXABQAXACAQgEQAGgPgCgWIgCgTQgBgMAAgIQACgjgNgdQgJgDgEAAIgsAEgAjEiFIgEAoQgBAKAEADQADAEAJAAQATAAAdgDIAFgBIAGgDIAAgbQgBgPgCgMIgiAAQgTAAgOAEgACFjpIgLADIgCAGIgEBqQAAAKAEAKQAFAPAQAAIAEgHIADgGQAEgUABgSQABgTgCgdIgFgxIgCgEIgMACgAAjjkIABBYQAAAVAGALQADAFAGAGQADACADAAQADgBACgDIABgEIAIgfQADgRAAgOIgBg2IgCgMgAjrh3IgjADIhWAGIgCAAIgFACQAAAAgBAAQAAABAAAAQAAABAAABQAAAAAAABQAAAFAIAAIAXgCIAZgCIBJgBIAQgBQAFAAABgDQABgCgBgIQgEgBgHAAIgLAAgADKjoIgCAKQgCAtAGAuQABAIAGAQQACAHAIgBQANgWgBgXQgBgUgFg9IgCgGIgLgBQgGAAgGACg");
	this.shape_3.setTransform(39.5,28.1);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,79,56.3);


(lib.DarkOverlay = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.6)").s().p("EhMHAq0MAAAhVoMCYPAAAMAAABVog");
	this.shape.setTransform(487.2,274.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,974.5,548.2);


(lib.Customers = function() {
	this.initialize();

}).prototype = p = new cjs.Container();
p.nominalBounds = null;


(lib.customer_bubble_head_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EgqeAKZIAAykQAAiNCOAAIBHAAMBC4AAAQB5AAASBnQADASAAAUIAAK7IBkAUIK+CLIimAvIp8C1IAABSIAAAU");
	this.shape.setTransform(271.9,66.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("EgqeAKZIAAykQAAiNCOAAIBHAAMBC4AAAQB5AAASBnQADASAAAUIAAK7IBkAUIK+CLIimAvIp8C0IAABTIAAAUg");
	this.shape_1.setTransform(271.9,66.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AnAJmIAAgUIAAhTIJ6i0IEKA0IriDTIg+ARIAAADgAnAB9IAAq7QAAgVgDgSQBnASAAB5IAAJqg");
	this.shape_2.setTransform(508.6,71.7);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,555.9,137.1);


(lib.counter_bubble_head_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EgkNAFTIAAhQQAAiOCOAAIBHAAIe2AAIhWnHIBQBSIFuF1IACAAIeYAAQBvAAAYBXQAHAYAAAfIAABQ");
	this.shape.setTransform(231.8,33.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("EgkNAFSIAAhQQAAiNCOAAIBHAAIe2AAIhWnHIBQBSIFuF1IACAAIeYAAQBvAAAYBXQAHAYAAAeIAABQg");
	this.shape_1.setTransform(231.8,33.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("ASkEqIAAhQQAAgegHgYQBXAYAABugAuCBNIlwl1IAAgBIFyF2g");
	this.shape_2.setTransform(344.9,37.9);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,473.7,71.8);


(lib.Counter = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("EBMLgbSIAAAvIAAAxIAADGIAAAzIAABZEBMLgc/IAAA8EBJhgXWIAABAIAAAzIAACgIAAAVIAAAWEBJAgT+IABA8IAAAdIABA8IAAAhIAFJzIAAAtIABBKIAAAtEBKygA9IAAgZIgCg0IgFi0IgBgtIAAAAEBLogCuIAAAdIAAAzIgCA8IhXdiIBZsFEBKPAdAIJgvtEhSngHaIgDAxIgDAwIgLDbIgDAuIgDA4EhSCgCjIADAsIAMDBIAAAnIAAA5IgBAgIgFJcIAAAtIAABMIAAAvEhSTASSIgEggIgTidIgBgKEhSDAc+Ig6pQIgIhJIAAgrIABAAEhSDAc+IhCoPEhTuAUAIBrI+MCcSAAC");
	this.shape.setTransform(543.3,185.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D5363A").s().p("EgogAGmIAOi1QAOiygFgbIAggDQGdg5H6hFIgBA4IABAlQgEA5gCBCQgCAzgBCDIAAAEIAAAMQAECQAJA2ItlBwIh3AKQAChmAIh1gAb5BOQgGgIAEgvIAAnyIAIg5QEngoIGhEIAAJvIhWAQIrZBdQABgHgFgHg");
	this.shape_1.setTransform(535.3,169.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C6D3D6").s().p("EhOXAOvIg6pPUBPRgJFBPSgLLIhXdhg");
	this.shape_2.setTransform(519.7,276.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9EA8AA").s().p("EhPWAJdQC6gXAjABIHKg5IAQACUAgigEKAApgACIHxhBIBEgCIEYgkISSh+IV6i4QBSgWHphGQD0gkDkgeIBKgFMAq/gFiIA2gIIgCA9UhPTALJhPRAJHgEBOhgKFIAAgZg");
	this.shape_3.setTransform(519.5,244);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#123686").s().p("EhOtAI7IAxgHILShbIB8gEQDzgoJGhUIIchNMAkBgEpQA0gMAugGQAmgGBbgGIEKggQBDgFDNgSMAkvgEvIBGgPMAisgEbIBjgNIAFC0Qi0AZwBCFQv0CDjFAbQ1/DDglAKI2hC9IhLAEIoMA7QoVA9goADIkUAjIhEAEInuA/UgAqAABggWAEHIgQgBInOA7QhqAJgyAGg");
	this.shape_4.setTransform(518,226.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#101445").s().p("EhNgAJoIK4hZIBVgDQA6gCARgCQDEgaSdixIDagaIAAgCISQiYIA9gDIACgFINThvIBGgPQAXgDBNgCIELghICWgMQBugKAXgBIY3jPIAAABIMlhoIBGgPMAiagEXIAAAtMgirAEbIhHAPMgkvAEwQjMAPhDAFIkLAgQhaAHgmAFQgvAHg0ALMgkAAErIocBNQpGBUjzAoIh9AEIrRBcg");
	this.shape_5.setTransform(515.2,216.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C4D0D3").s().p("EhNgAPWIDMgYIAlABMAidgEgQAEhNAMikIANjRQADgzAIhMQAHg9gBgTIACAAIAAgGQAAgSABgHIADgKIhtAGQiUAVwZCKQvtCFi1AfIABg6QPaiGN8h6IIrhOIgDAAQAvgCgBgDIIrhMQDegdFlgrIADgBQATgFEogfQJJhED6ghIMmhlITmiXIgDgDIBQgKQI7hIG8g/IgCABIRHiUQNDhxBBgRIAAA8QhDAFh5ASQhNAMrSBdIrkBfIkeApIAAgCIgvAHQAABoAHAaIAAGyQAGAXACBwMAgDgEHIAABKMgiaAEVIhGAOIslBpIAAgBI43DPQgXAAhuALIiWAMIkLAhQhMAEgYADIhGAOItTBwIgCAFIg9ADIyQCYIAAACIjaAaQydCxjEAZQgQACg7ADIhUADIq5BZgEgmbAJ2QAAAOgDAOIgBAGICKgSIBIgEINhhvQgCgHABgNQgBgQACgHIAAgEIAAAAQAIg5ACiTIAAi9IAChgIABheQgCgKgBgcQAAgGACgGIAAgFIhLABIgCgBQmCA1nrA6IhPAHIgMCfQgMCugIBAQgQCggBBgIgBAAgA0VIWIAFgBIAAAAgA0kiWQgDAngBABQgHD2gBAFIAAEmQgFAVgBAfQgCAaACAKIAmgCIAAgCMAxvgGTIgBn1QgFgcAFgxIABgLQABgjACgdIgBgFIABgrIg0AAIAAABQn5BCodBCIxRCCIuRCHIgLAFIgmAJQgnAJgFAAQAAAKADAEgA1aHkIABgCIgBAAgAeRn5QgCADAAB2QAADRACBDQADB0ALBoILjhgIBygTIAvgGIAAgFIAFAAQgDgEAAgaQACgnACi8IgEgSIAAluQAEgLgHg3QnTBCm6A6IAAAQIAAAZIACAAQgBAtgFAGgA0ojEIAAACIAOgDIgOABg");
	this.shape_6.setTransform(515.2,172.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#113685").s().p("EhNwAIUQBDgHAcAAIHJg5IARACUAgigELAAqgABIHwhAIBEgEIEYgjIRHh4IBLgEIV6i6QBRgWHphHQD1gjDkgfMArxgFkIAACgIghABIAAg8IAAA8Qh7AIvcB/IvRB/Qi2AXkKArQkiAvjcAeQm7A8sFBaQqHBKj5AmQkoAtokA+QqJBJjvAfIorBKQgHgGgHAIQnLA8r/BoQtGBxl0A0IgMAJg");
	this.shape_7.setTransform(516.2,120.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#111445").s().p("EhOtAUdIgWgMQgFgDAAgCQgCgKACgDQAEgGARgFIAHgBIAAAqgEhOsATzIAAAAQAPgEAfgEQAygHBqgJIHOg7IAQACUAgWgEHAAqgACIHug/IBEgDIEUgjQAogDIVg9IIMg8IBLgDIWhi/QAlgLV/jCQDFgbP0iEQQBiFC0gWIABAxMgq+AFjIhKAEQjkAfj1AkQnoBGhSAWI16C5IySB/IkYAjIhEADInxBBUgApAABggiAELIgRgCInJA5QgjgBi6AWgEhN6AULIgEgggEhNaADDIAMgJQF0g0NGhxQL/hlHLg9QAHgHAHAFIIrhJQDvggKJhJQIkg+EogtQD5gmKHhMQMFhaG7g7QDcgeEigwQEKgrC2gWIPRh/QPciAB7gHIABAcQhBAQtDBxIxHCVIACgBQm8A+o7BJIhPAKIACACIzmCYIslBkQj7AhpIBEQkpAfgSAFIgEABQllAsjeAdIorBMQABACgvACIAEABIosBNQt8B7vaCGgEhNqgAqIIdhCIAQACUAgWgEIAAqgABIHuhAIBEgDIEUgjQAogDIVg8IINg8IBKgEIWhi+QAvgNV2jBQClgWQIiGQPjiBDHgcIAAA0MgrxAFjQjkAgj1AjQnpBGhRAWI16C6IhLAEIxHB6IkYAkIhEADInwBAUgAqAACggiAEKIgRgBInJA4QgcAAhDAGg");
	this.shape_8.setTransform(515.3,173.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#D5E3E6").s().p("EhOdAKgIB7gCIQCiHQPjiCDngpIBBgHIgKCIQgCARgjHLMgiAAEdIglgCIi5AYgA1GLXIAEjuQAIhCAAiPIgChUQI2hPGAg4QeIjlBVgNICUgVIAAJpIgRABMgwjAGLQACgYABg8gEhPWABcIADgxIADguIfKkHIArABIGPgyIBqgIQBTgGATAAIHIg5IAlABIfMkAQAPgEA4gMIBJgPIZTjRIAaADIJohOIAfACIDugdIAlABMAkOgElIAAAvIAAAxIAADFIiUAUIgWADIAAhAIAABAQjGAcvkCAQwICGilAWQ11DCgvAMI2iC+IhKAFIoNA8QoVA8gnADIkUAiIhEAEInvBAUgApAABggWAEFIgRgCIocBCIg2AGgEAtEACaIAAmqQgEgegCgwIQ1iPQNWhyBQgSIAFJxI/SD+QAAhDgIghgEBM4gO/g");
	this.shape_9.setTransform(521.8,138.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#71797A").s().p("EhOoAT+IC5gYIAlACMAiAgEdQAjnLADgRIAKiIIhBAHQjoApvjCCIwCCHIh6ACIAAgfQC1gfPtiFQQZiLCUgWIBtgGIgDAKQgBAHAAAUIAAAGIgCAAQABATgHA9QgIBMgDAzIgNDRQgMCkgEBNMgidAEgIglgBIjMAYgEgnmAPnQADgOAAgOIAAgNIABAAQABhgAQigQAIhAAMiuIAMihIBPgHQHrg6GCg1IACABIBLgBIAAAFQgCAGAAAGQABAcACAKIgBBeIgCBiIAAC9QgCCTgIA5IAAAAIAAAEQgCAHABAQQgBANACAHIthBvIhIAEIiKASgEgloAFWIghAEQAFAbgOCyIgNC1QgJB1gCBmIB4gKINkhwQgJg2gDiQIAAgMIAAgEQAAiEACgyQAChDAFg4IgBgnIABg5Qn6BFmdA7gA1+M8QABgfAFgVIAAkmQABgFAHj4QABgBADgnQgDgEAAgKQAFAAAngJIAmgJIALgFIORiHIRRiAQIdhCH5hCIAAgBIA0AAIgBArIABAFQgCAdgBAjIgBALQgFAxAFAcIABH1MgxvAGTIAAACIgmACQgCgKACgagAZRizQhVAN+IDlQl/A4o2BPIABBUQAACPgIBCIgDDuQgBA8gDAYMAwkgGLIARgBIAAppIiVAVgAdJDnQgChDAAjPQAAh2ACgDQAFgGABgtIgCAAIAAgZIAAgQQG6g6HThCQAHA3gEALIAAFsIAEASQgCC8gCAnQAAAaADAEIgFAAIAAAFIgvAGIhyAVIrjBgQgLhogDh2gAeAjeIgIA5IAAHyQgFAvAGAHQAGAIgBAHILZhfIBVgQIAAptQoFBEknAogEAsOACuIAAmwQgHgaAAhoIAvgHIAAACIEegpILkhfQLShdBNgMQB5gSBDgFIAAAiQhPAStXByIw1CPQACAwAEAeIAAGqQAIAhAABDIfTj/IAAAtMggDAEHQgChwgGgXgEhPYgAxIe4kDIArAAIGKgxIBqgGQBUgHASAAIHOg5IAmAAIfnkDQAPgFA4gLIBKgRIY9jMIAaACIJjhNIAgABIDsgcIAlABMAkggErIAAAyMgkOAElIglgBIjuAdIgfgCIpoBOIgagDI5SDRIhKAPQg4AMgOAEI/NEAIglgBInIA5QgTAAhTAGIhpAIImQAyIgrgBI/KEHIgGAAg");
	this.shape_10.setTransform(522.4,138.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("EhR3AUkICYgTIAAAuIgxAHIgBgKIABAKIhXALIgKAAgEhQQAVGgEhSYATMQgFgCAAgBQgDgOAYgNICqgXIAAAtIikAWIgWgOgA2PMDIAAAAIgFABgA3YLQIAAAAIAAACgEhQWAIrQgFgDACgBQAbgGAlgFIAAAfIgoAAIgVgQgEhPYAG8IAAAmIhBAJgEBNLADlIA4gHIDHgcIATABIAEAFQADAEACAKQABAMgPAEIhQAZIiFASIAAgzIAAAzIg3AIgEBODAERgEhR6AD7QgFgDAAgCQgCgKACgCQAEgIASgEQAOgEAmgGIAXgCIA2gGIADAtIg7AGIACgtIgCAtIhEAJIgWgNgEhQgAD/gEBLiAARIBigNIABAAIEuglQAfARAAACQABAHgbARIkyAnIgCgtIACAtIhjANgA2nAoIAOgBIgOADgEhRlAACIgDgFQgDgFgBgLQAAgBALgGQAMgGgDgPIBJgJIgDAxIgzAFIgJACQgJACgLAAIgDAAgEBLigBkIFTgrIATAMQAIAEAAADQABAHgWARIlZAtgEBLcgL4QAkgCAUAAQAAABAMgBQAMgDAAALQAAALhQARgEBLbgNQIAhgCIAAAVIggAJgEBL8gNSQAigBAAAGQAAAHgiAJgEBL8gQkIAWgDICUgUIAAA0IAAg0IBrgPIATACIAEAEQADADACALQABALgPAGIhQAYIgpAGIiqAWgEBOmgUxIAtgHQBFgLBrgNQAbAQAAAEQACAKgGAEQgGAEgWAEQhXANhnAWIgaADg");
	this.shape_11.setTransform(527.9,148.7);

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,1080.3,373.2);


(lib.ComputerScreen = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2D2D2D").s().p("Ak+LTQgSgKgFgQQgEgLAAgdQAAheC0g7IAAgEIBFgHIAFh9IiDACQgsALkEAKQgZAAgqAIIgnAHIgJABIgEAAIgOgCIAAAAQgWgGgNgSQgGgJgDgHIgGgCIgCgEIgCgDQgEgEgFgDIAAhQQADhVAJioQAKiyADhOQAFg1AEhMIAHiCIAChBIADgaIgFgDIAAg3QALgJAJgDIACgEIABgBQAGgIAMAAQGJgaCqgSQDvgaAkADIEOgLIAhgDIBcgBQAKgGAWAGIAOAAIAIAHIgBAAIAEACQAcALAUASQAWATAAAPIAAAKQADARgBAZIgrOSIABABQAHAHAAACQAAALgDACQgDACgkAKQglAKifAWIjeADIAAAuIAABHQCtAEA8AQQA/AQAAAxQAAA9glAhQhFA8jOAWIlXAcQiYgSgigSgAkPJzQgZA0AmANQAmANBWAAIEugZQAigBAMgBQC8gUAygtQAQgOADgTQABgQACgEQhDgSgsgIQg0gJhWgDQgGA1gQAmQgrADgpAFQglAEgLAEIiRAAQgSgMgFgkQgBgMgBgZIgxAFQgrADgHAIIgNAJIghASQgJAGgEADQAAABgBAAQAAABABAAQAAAAACgBQABAAACgBQgLANgIASgAgqEFIAAA2IgCA1QgCBAgEAtIAABjIBzAAQAOgEAegFQAkgGAhgCIAAlAgApeFbQBhgQEngOIB7gCIAAhTQAlgTBHgHQBJgHBxgNQATAlAEBAIABANIDYgDQASgGBUgLQA8gJAhgCQgGgggCg+QAAg1AYmLIAXmDQABgBgIgCQgJgCABAAIgSgNIgRgNIgLgKIgCABIAAgCIgbgXQgrAHAQAEIl7AUQgpAGjdAXQipARl0AYIAyA5IgDAAg");
	this.shape.setTransform(72.3,76);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#666666").s().p("AklKnQgmgNAZg0QAIgSALgNQgCABgCABQgBAAAAAAQgBAAAAAAQAAgBABgBQAEgDAJgGIAhgSIANgJQAHgIArgDIAxgFQABAZABAMQAFAkASAMICRAAQALgEAlgEQApgFArgDQAQgmAGg1QBWADA0AJQAsAIBDASQgCAEgBAQQgDATgQAOQgyAti8AUQgMABgiABIkuAZQhWAAgmgNgAhVIzIAAhjQAEgtAChAIACg1IAAg2IDcgWIAAFAQghACgkAGQgeAFgOAEgApipEIADAAITAhvIASANQgBAAAJACQAIACgBABIgXGDQgYGLAAA1QACA+AGAgQghACg8AJQhUALgSAGIjYADIgBgNQgEhAgTglQhxANhHAHQhJAHglATIAABTIh7ACQknAOhhAQg");
	this.shape_1.setTransform(75.8,77.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#727272").s().p("Ap4AWQF0gWCpgSQDcgWApgHIF7gTQgPgFAqgGIAcAXIAAACIABgBIAMAKIAQANIzABsg");
	this.shape_2.setTransform(73.4,11.2);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,144.6,152);


(lib.BossAnims = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.Boss_01__000();
	this.instance.setTransform(400.9,0);

	this.instance_1 = new lib.Boss_01__001();
	this.instance_1.setTransform(400.9,0);

	this.instance_2 = new lib.Boss_01__002();
	this.instance_2.setTransform(400.9,0);

	this.instance_3 = new lib.Boss_01__003();
	this.instance_3.setTransform(400.9,0);

	this.instance_4 = new lib.Boss_01__004();
	this.instance_4.setTransform(400.9,0);

	this.instance_5 = new lib.Boss_01__005();
	this.instance_5.setTransform(400.9,0);

	this.instance_6 = new lib.Boss_01__006();
	this.instance_6.setTransform(400.9,0);

	this.instance_7 = new lib.Boss_01__007();
	this.instance_7.setTransform(400.9,0);

	this.instance_8 = new lib.Boss_01__008();
	this.instance_8.setTransform(400.9,0);

	this.instance_9 = new lib.Boss_01__009();
	this.instance_9.setTransform(400.9,0);

	this.instance_10 = new lib.Boss_01__010();
	this.instance_10.setTransform(400.9,0);

	this.instance_11 = new lib.Boss_01__011();
	this.instance_11.setTransform(400.9,0);

	this.instance_12 = new lib.Boss_01__012();
	this.instance_12.setTransform(400.9,0);

	this.instance_13 = new lib.Boss_01__013();
	this.instance_13.setTransform(400.9,0);

	this.instance_14 = new lib.Boss_01__014();
	this.instance_14.setTransform(400.9,0);

	this.instance_15 = new lib.Boss_01__015();
	this.instance_15.setTransform(400.9,0);

	this.instance_16 = new lib.Boss_01__016();
	this.instance_16.setTransform(400.9,0);

	this.instance_17 = new lib.Boss_01__017();
	this.instance_17.setTransform(400.9,0);

	this.instance_18 = new lib.Boss_01__018();
	this.instance_18.setTransform(400.9,0);

	this.instance_19 = new lib.Boss_01__019();
	this.instance_19.setTransform(400.9,0);

	this.instance_20 = new lib.Boss_01__020();
	this.instance_20.setTransform(400.9,0);

	this.instance_21 = new lib.Boss_01__021();
	this.instance_21.setTransform(400.9,0);

	this.instance_22 = new lib.Boss_01__022();
	this.instance_22.setTransform(400.9,0);

	this.instance_23 = new lib.Boss_01__023();
	this.instance_23.setTransform(400.9,0);

	this.instance_24 = new lib.Boss_01__024();
	this.instance_24.setTransform(400.9,0);

	this.instance_25 = new lib.Boss_01__025();
	this.instance_25.setTransform(400.9,0);

	this.instance_26 = new lib.Boss_01__026();
	this.instance_26.setTransform(400.9,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(400.9,0,400,472);


(lib.boss_bubble_head_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EArOALDIAAz3QAAgVgDgSQgShnh4AAMhI0AAAQiOAAAACOIAAI1IpMJYIDyguIFahAIAAB3IAABh");
	this.shape.setTransform(276.7,70.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("EgiBALCIAAhgIAAh3IlaBAIjyAtIJMpXIAAo1QAAiNCOgBMBI0AAAQB5ABARBmQADASAAAVIAAT2g");
	this.shape_1.setTransform(276.7,70.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("EApqAKPIAAz3QAAgUgDgSQBnASAAB4IAASTgEgo/AH4IFahAIAAB2InoBbg");
	this.shape_2.setTransform(286.7,75.9);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,565.4,145.4);


(lib.boss_base_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EgmngBAQAEBtBiARQASADAVAAMBI1AAAQCIAAAFiB");
	this.shape.setTransform(247.3,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("EgkaABBQgVAAgRgDQhjgRgEhtMBNPAAAQgGCBiIAAg");
	this.shape_1.setTransform(247.3,6.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("EgkbABzQh5AAgShnQASADAVAAMBI0AAAQCIAAAGiBIBkAAIAABZQAACMiOAAg");
	this.shape_2.setTransform(257.4,11.6);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,506.5,25.1);


(lib.BGBox = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8E7E6").s().p("EgiUAL/IAA39MBEpAAAIAAX9g");
	this.shape.setTransform(521.9,629.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(302.1,553.2,439.5,153.5);


(lib.Behind = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5B371D").s().p("AhADYQAphmAAhDQAAhGgIiDIgGhxQARAkAOAYQAsBPAUAyQAcBFAAA4QAABKg0A8QgqAzhNAkIAVg0g");
	this.shape.setTransform(28.3,48.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#352011").s().p("AhPGVQgZgUgGhQQgCgaAAhaQgFhtgChHQAJgsAQg7QAJgfgFg9IgIhMQAKgpAOgrQAGgVAYgPQANgIAggNQAQAgAlBYQAoBiAMAbIAGBwQAICBAABJQAABDgpBmIgVA0QghAPgmANg");
	this.shape_1.setTransform(13.8,38);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AiYGwQgmgkgKhRQgDgZAAhmQAAhLAKg9IAHgsQADBHAFBtQAABaACAaQAGBRAZATIBCAAQAogNAggPQBMgkAsgzQA0g8AAhKQAAg6gchFQgUgwgvhPQgOgYgQgkQgNgbglhhQglhZgRggQghANgNAIQgYAPgHAVQgNArgKApIgBgbQgEh5BagTQAvgJA/CRQAhBLAhBcQAaA6AuBYQAjBKAAA9QAACjjgBeQgaALgdAKg");
	this.shape_2.setTransform(20.5,38);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-5.1,40.9,86.4);


(lib.Avatars = function() {
	this.initialize();

}).prototype = p = new cjs.Container();
p.nominalBounds = null;


(lib.Arrow_Butncopy = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("ABEC5IhEhfIhDBfIi1AAICnjAIiRixICsAAIA2BRIA3hRICsAAIiRCxICnDAg");
	this.shape.setTransform(33.9,36.6);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4,1,1).p("ADuFBQgOAFgOAEQgvAOg1AAQiPAAhmhmQhmhmAAiPQAAiQBmhmQBIhIBZgVQACAAACgB");
	this.shape_1.setTransform(23.8,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(4,1,1).p("AjVlHQAmgJAqAAQCPAABmBmQBmBmAACPQAACQhmBnQg2A1g/Aa");
	this.shape_2.setTransform(48.3,33.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F4D96F").s().p("AihDqQhXhJgchhQgchfAjhnQAjhnBeheIADgBQAmgIAqAAQCOAABmBmQBmBmAACOQAACRhmBmQg1A1hBAaIgbAJQh5gmhShFg");
	this.shape_3.setTransform(40.7,34.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("Ah5DxQhmhmAAiOQAAiRBmhmQBHhHBagVQhbBdgkBoQgjBnAcBeQAcBhBVBJQBSBFB7AnQgvANg2AAQiOAAhmhmg");
	this.shape_4.setTransform(22.5,35.3);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.ARR = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgzgHIBRg9QgDA6ARA4IAIAXg");
	this.shape.setTransform(-3,26);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AjKBOIgIgXQgSg5ADg6ID+i+IAACvIDFAAIAACXIjFAAIAACvg");
	this.shape_1.setTransform(22.7,25.1);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-8.2,0,53.6,50.2);


(lib.AnswerBoxThinStretch = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EA2FgDRIAAGWQAAAHAAAGEg2DADSQgBgGAAgHIAAmW");
	this.shape.setTransform(346.2,21);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("Eg2DADSIgBgNIAAmWMBsJAAAIAAGWIAAANg");
	this.shape_1.setTransform(346.2,21);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,696.3,46);


(lib.AnswerBoxThimBase = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EA2FgA/QgGB/iIAAIhHAAMhmmAAAQiIAAgGh/");
	this.shape.setTransform(346.1,6.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("EAywABAMhmmAAAQiIAAgGh/MBsJAAAQgGB/iIAAg");
	this.shape_1.setTransform(346.1,6.5);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,696.3,17);


(lib.AnswerBoxStretchMC = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EA2FgFYIAAKxEg2EAFZIAAqx");
	this.shape.setTransform(346.2,34.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("Eg2EAFZIAAqxMBsJAAAIAAKxg");
	this.shape_1.setTransform(346.2,34.5);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,696.3,73);


(lib.AnswerBoxShadThin = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.502)").s().p("EAyxAFdMhmnAAAQiOAAAAiNIAAmfQAAiNCOAAMBnuAAAQCNAAAACNIAAGfQAACNiNAAg");
	this.shape.setTransform(372.5,35);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(26.4,0,692.3,70);


(lib.AnswerBoxShad = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.502)").s().p("EAyxAH4MhmnAAAQiOAAAAiNIAArVQAAiNCOAAMBnuAAAQCNAAAACNIAALVQAACNiNAAg");
	this.shape.setTransform(372.5,50.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(26.4,0,692.3,101);


(lib.AnswerBoxBaseMC = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EA2FgBQIAAAVQAACMiNAAIhHAAMhmnAAAQiOAAAAiMIAAgV");
	this.shape.setTransform(346.2,8.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("EAywABRMhmmAAAQiOAAAAiLIAAgWMBsJAAAIAAAWQAACLiOAAg");
	this.shape_1.setTransform(346.2,8.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,696.3,20.3);


(lib._50Percent_white = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.502)").s().p("EhOHAsrMAAAhZVMCcOAAAMAAABZVg");
	this.shape.setTransform(500,286);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1000,571.9);


(lib.YesBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Yes_Btn();
	this.instance.setTransform(99,31.7,1,1,0,0,0,99,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AtLEyIAApkIaXAAIAAJkg");
	this.shape.setTransform(81.5,30.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1,x:99}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85,x:99.1}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.YardSteve_03 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.YardSteve03();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4.9,0,174.3,157.5);


(lib.WinnersCup = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(272.5,380.2,0.48,0.48,0,0,0,291.4,34.5);

	this.text = new cjs.Text("young grafter", "58px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 61;
	this.text.lineWidth = 318;
	this.text.setTransform(266.8,86.6,1.18,1.18,-8);

	// Layer 6
	this.instance_1 = new lib.Handle();
	this.instance_1.setTransform(82.5,164.8,1,1,0,0,180,82.8,103.8);

	this.instance_2 = new lib.Handlecopy();
	this.instance_2.setTransform(458.1,164.8,1,1,0,0,0,82.8,103.8);

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("A6Cn+ILfhlMAi7gE0AqTOYIGkg7IdykP");
	this.shape.setTransform(242.1,147.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7A6C38").s().p("Ah8G5Qidifh2jOQjIlfhWj+QgPgvgMhYILdhlQAjB5AZBMQEKM2FuHEImkA8Qj6iainirg");
	this.shape_1.setTransform(146.8,163.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#9E8C49").s().p("AjYRSQmuiIk+jFIGkg7QlunEkKs2QgYhNgkh4MAi7gE0QG1AiIzA9QAEAAALgCQAIgCgBADQgOB9gtCpQgZBaghBqQgjB8hWCtQgrBWhtDAQhpC8ikCiI9yEOIAOARQB4CRB5BNICGBUQA0AhA1AkgA+1qjQgSg9gHgqQgFgjgDgLQAAgogFgvQgIg5gCghIDmguQCvgiB4gSQAggEAxgBIBPgCQA6DOAqCPIrfBlQAAgNgCgGg");
	this.shape_2.setTransform(272.6,162.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CCB55E").s().p("Ag/RcQg1glgzghIiHhTQh4hOh4iRIgOgRIdxkOQkFEBjqCQQlfDXmNAvgA1ExIQPygqOFA0QDCALDmATMgi7AE0QgqiPg6jNg");
	this.shape_3.setTransform(273.9,161.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AjdSpQnLhml7kMIgpgdQkzjjjolEQjZkuh4lVQhtk4AAj+IABgjIgIgCQAAAAAAAAQAAgBgBAAQAAAAgBAAQAAAAgBAAQgDABAAgTQAAgyEHgrQA8gKCSgUQBwgQAfgHICBgGQO4goPSAuQB2ARK/A1QBYAGD2AcQEwAiAdASQASACAGAFQAKAFADAQQADAUgEAWQAADbh8FAQiKFijpE6QhABWhDBOQjdECkICyQmPENnKA7gA03xRIhPACQgxAAggAFQh4ASivAiIjmAtQACAiAIA5QAFAvAAAoQADALAFAiQAHArASA9QACAGAAANQAMBYAQAuQBVD+DJFfQB1DOCeCgQCpCqD5CaQE+DFGuCIICmAAICoAAQGOguFfjYQDpiPEGkBQCkiiBpi8QBtjBArhWQBWisAjh8QAhhrAZhZQAtipAOh9QABgDgIACQgLACgEAAQozg9m1gjQjlgSjDgLQn2gdoWAAQmrAAnAASg");
	this.shape_4.setTransform(272.5,162.2);

	// Layer 7
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7A6C38").s().p("AkfEpQiEgUgegGQAHgoBbgdQAxgQCbgeQCVgeBMgZQB1gnAug4QASg0Auh0QAuh1ANgmQAugCAoABQgMEKhHB3QhDByiIA7QhAAcjFAyIgGABQhdgKhbgMg");
	this.shape_5.setTransform(223,302.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#9E8C49").s().p("AjuFOQkrgCkOgcIAGgCQDHgxBAgcQCIg8BDhxQBHh4AMkJQC0AFAxA/QAXAcADA2QADAeADBDQAQB9CDBBQC6BdHSANIAAAKIgHATQgPAegaAHQhIATgYAFQgvAJg1ADQnMAWkeAAIg4AAg");
	this.shape_6.setTransform(293.4,303.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AhLGjQl+gGlMgzIgQgCQiegZg7gYQg1gWAAgkQAAgoBXgxQBVgxBygbQBkgYCxgmQCGggAkgpQAhglAOhCIAPhyQAJhDAVggQAbgtA8gLQA3ADAvAGQERAeA1BkQATAjABBGQAABJANAdQAsBjCDAxQBdAjClAOIDrAUQBpAQAEAuQACAZgIAYQgKAbghATQhZAzj5AdQlDAnlbAAIhdgBgAiWlQQgNAmgtB1QguB0gSA2QgvA2h1AnQhLAZiXAeQibAegxAQQhbAdgHAoQAeAGCEAUQBbAMBdAKQEOAcErADQEoACH6gZQA1gCAvgKQAYgFBIgTQAagHAPgdIAHgUIAAgKQnSgNi6hcQiDhBgQh9QgDhDgDgfQgFg1gXgdQgxg/iygFIgjAAIg0ABg");
	this.shape_7.setTransform(274.5,304);

	// Layer 8
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#9E8C49").s().p("A2HBFIBuhCQBUgwA2gXIGfAVQAOAEEgAPIEdAPIAWAGIT9gVQBGALBZAfQBIAbA0Acg");
	this.shape_8.setTransform(272.5,332);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CCB55E").s().p("A3qExIgKhHIgNkTQgNkGAAgQQAAgWAUgZQAagbAIgLMAu/AAEQAEAEAHARQAHARADADQAnFxgnFmQhoABAAAFQgmADusACQgXAGpMAHQgVAG0vAHQAEglgJg/g");
	this.shape_9.setTransform(270.4,385.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("A4uIFQAAgRAHgLIAGgEQAAgBABAAQABgBAAAAQAAgBAAAAQABAAgBgBQgaiqgJi6QgHiFABjgQAAg2ABgJQAIgzAlgMQAHgGAMgGIARAAQAtgkBcgwQBxg6BugqQG7ABATAGQIrAGAWAHIT3AAQEYBJCUBbIgCAAQAmBAAAFTIABD6QgDB+gKA2QAGAHgEAOQgEANgHgBQgIATgJAAQggAAgDgDIhKgEQhAgDhKAGImuADQmxAFgMAEIk2AFQk4AEgOAFI1LASQgngkAAgCgA37kUQgVAaAAAVQAAARANEDIAOEWIAJBHQAKA/gEAkQUugGAWgGQJMgHAWgHQOsgCAmgCQAAgGBpAAQAnlngnlwQgDgDgHgRQgHgSgEgEMgu/gADQgIAKgaAbgA0Fm0IhvBCMAsQAAAQg0gdhIgbQhYgghGgLIz9AVIgXgHIkdgOQkfgPgPgFImegVQg2AYhUAyg");
	this.shape_10.setTransform(270.5,376.1);

	this.addChild(this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance_2,this.instance_1,this.text,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.4,42.8,541.4,388.8);


(lib.WindmillLoader = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_11 = function() {
		this.gotoAndPlay(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(11).call(this.frame_11).wait(1));

	// Layer 1
	this.instance = new lib.WindmillSpin("synched",0);
	this.instance.setTransform(55.5,64.7,1,1,0,0,0,64.7,64.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regY:64.8,rotation:30,y:64.8},0).wait(1).to({rotation:60},0).wait(1).to({rotation:90},0).wait(1).to({rotation:120},0).wait(1).to({rotation:150},0).wait(1).to({rotation:180},0).wait(1).to({regX:64.8,rotation:210,x:55.4,y:64.7},0).wait(1).to({rotation:240},0).wait(1).to({rotation:270},0).wait(1).to({rotation:300},0).wait(1).to({rotation:330},0).wait(1));

	// Dark Overlay
	this.mc_bg = new lib.DarkOverlay();
	this.mc_bg.setTransform(55,73.9,1.013,1.076,0,0,0,487.2,274.2);

	this.timeline.addTween(cjs.Tween.get(this.mc_bg).wait(12));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-438.5,-221.2,987,590);


(lib.TheCounter = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("the\ncounter", "30px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 35;
	this.text.lineWidth = 395;
	this.text.setTransform(220.8,199.7,1.18,1.18,-8);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A6C38").s().p("AiPINIhkjCIgKgUIgYgvIgEgIQg9h7g2h9IgCgHIgOgeQhejhhDjqIJUhPICigVQCgJFDlH5IivAYIn7BDIgjhAg");
	this.shape.setTransform(80.9,262.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9E8C49").s().p("AkAZ0QnRjbk6jnQjVidiVjiQgYglgZgqIH3hDICvgXQEJI9FiHeIhrgxgAyfntMAongFZQAkgHBFgIQA2gGA9gOIHhhQQhGG6iEGBIgWA/Qg7CihGCZIg9AJMgnBAFNQjln8igpDgA/triIAAgDIgLg2QgWhxgQhzQghjvgFjbIADirIBnAAIAngIIETgEICigVIBogPQBWJfCSIfIiiAVIpXBQQgoiPgeiSg");
	this.shape_1.setTransform(217.9,252.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCB55E").s().p("EgH1AgDIgWgKQlindkJo+MAmqgFIIAbgFQgbA4gcA2QhBCAgjBCQg9Byg3BUQiVDijVCdQk6DnnPDbQifBLiCA3QhMgghVgngA8M1PQC7gcBUgVQDCgwEpiQQDHhgDgieQDyiqAlhhQAkBhDyCqQDeCeDIBgQEoCQDDAwQCHAiGSAzIESADIAnAIIBoAAQABB+gMCXIAAAHIgEApIgBAJIAAAJIglEqIAAAAInlBOIjkAjMgozAFcQiSofhWpeg");
	this.shape_2.setTransform(255.2,218.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#1C180C").s().p("A5AM5IH9hEICvgXMAnBgFNIA8gJIgfBBIgaAEMgmrAFJIivAXIn3BCIgfg2gEggigE1IJYhQICigWMAoygFcIDkgjIHmhNIgCAJIgDAXIgEAaInhBQQg9AOg3AGQhEAIglAHMgomAFZIijAVIpWBPIgQg4gEAgjgNuIAAABIgEApIgBAJIgDABIAIg0g");
	this.shape_3.setTransform(230.1,238.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("EgA0Ah+IgLgDQgggMAFgCIgCAAQj8hXkzi4QkOihlHj3QiNhrifkPIgHgLIgfg3QhKiGhAiSIgYg3IgbhDQihmUgfhQIgghYQhBitgmiFQgUhFgQhIIgMg3Qg6kjAAlKQgDh+AHhkIAEguIgCgCQgGgFAAgDQAAgPADgDQACgCAIAAQAFgeBIACQBqACAXgGIEVgCICLgWIBlgPIA8gIQCUgXBVgTQDcgzEiicQCphaDUiuQDujFAyggIAAAEIABgEQAyAgDuDFQDUCuCpBaQEiCcDdAzQBUATCUAXQDpAiBEALIEVACQAXAGBqgCQBHgCAGAeQAHAAACACQAEADAAAPQAAADgGAFIgDACQAMBpgDCnIgBAPIgEA5QgLCfgVCWIAAACIAAABIgIA0QgbC0gqCmQg1DThfEQIgWA+Qg2CVhBCmIgVA1IgFANQhdDmiHDgQijEMiNBvQpwHwoWC3QguARgtANIgDAAQgNAAgkgNgEghkgR2QAFDaAhDwQAQBzAWBxIALA2IAAADQAeCSAoCPIAQA4QBDDoBeDhIAOAhIACAGQA2B+A9B6IAEAIIAYAvIALAVIBkDBIAjBBIAfA2QAZArAYAkQCWDiDUCeQE6DnHRDbIBrAxIAWAKQBVAnBKAfQCCg3CfhKQHRjbE6jnQDVieCVjiQA3hUA9hyQAjhCBBh/QAcg3Abg3IAfhBQBGiaA7ikIAWg+QCEl/BGm6IAEgaIAEgXIABgJIAAgBIAlkpIAAgJIABgJIAEgpIAAgIQAMiXgBh9IhoAAIgngIIkSgEQmSgyiHgiQjDgwkoiRQjIhfjgifQjyiqgkhgQgjBgjyCqQjgCfjHBfQkpCRjCAwQhUAVi7AbIhoAPIiiAVIkSAEIgoAIIhnAAg");
	this.shape_4.setTransform(221,218.8);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(0,0,0,0.502)").s().p("EgA0Ah/IgLgEQgggMAFgBIgCgBQj8hYkzi2QkOiilHj3QiNhrifkPIgHgLIgfg3QhKiGhAiRIgYg4IgbhEQihmUgfhPIgghXQhBiugmiFQgUhFgQhIIgLg3Qg7kjAAlJQgDiAAHhiIAEgvIgCgCQgGgGAAgCQAAgPADgDQACgBAIgBQAFgdBIAAQBqADAXgFIEVgDQAogHBjgPIBlgPIA8gJQCUgVBVgUQDcgzEiibQCphbDUivQDujEAyggIAAAFIABgFQAyAgDuDEQDUCvCpBbQEiCbDdAzQBUAUCUAVQDpAjBEALIEVADQAXAFBqgDQBHAAAGAdQAHABACABQAEADAAAPQAAACgGAGIgDACQAMBpgDCoIgBANIgEA6QgLCfgUCWIAAABIgBACIAAABIgDApIgBAJIgDABQgcCzgqCoQg0DShgERIgWA+Qg2CUhBCnIgVAzIgFAPQhdDliHDfQijENiNBvQpwHwoWC4QguAPgtANIgDABQgNAAgkgMg");
	this.shape_5.setTransform(230.5,228.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-10.2,0,484.3,447);


(lib.SoundIconRoundBtncopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1));

	// Layer 3
	this.txt_Sound = new cjs.Text("Sound on", "32px 'Arial'", "#FFFFFF");
	this.txt_Sound.name = "txt_Sound";
	this.txt_Sound.lineHeight = 38;
	this.txt_Sound.lineWidth = 214;
	this.txt_Sound.setTransform(51.4,17.9,0.59,0.59);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAIBXQgCgDAAgEQAAgEACgDQAfgeAAgrQAAgpgfgfQgCgDAAgEQAAgEACgCQAEgDAEAAQADAAADADQAkAkAAAxQAAAzgkAkQgDACgDAAQgEAAgEgCgAgVA9QgEgDAAgEQAAgEAEgCQATgUAAgcQAAgbgTgTQgEgDAAgEQAAgEAEgDQACgDAEAAQAEAAADADQAXAZABAjQgBAkgXAZQgEADgDAAQgEAAgCgDgAg2AkQgDgDAAgEQAAgEADgDQAJgJAAgNQAAgMgJgJQgDgDAAgEQAAgEADgCQADgDAEAAQADAAADADQAPAOAAAUQAAAVgPAPQgDACgDAAQgEAAgDgCg");
	this.shape.setTransform(37.5,25.9);

	this.instance = new lib.S_off();
	this.instance.setTransform(38.6,26.4,1,1,0,0,0,6.5,6.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.txt_Sound,p:{text:"Sound on",lineWidth:214}}]}).to({state:[{t:this.txt_Sound,p:{text:"Sound off",lineWidth:243}},{t:this.instance}]},1).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAzByIhGhJIgkAAQAAAAgBgBQgBAAAAAAQgBAAAAAAQgBgBAAAAQgCgCAAgDIAAhDQAAgDACgCQAAgBABAAQAAAAABAAQAAgBABAAQABAAAAAAIAkAAIBGhJQACgCADAAIACAAQAEACAAAFIAADZQAAAFgEACIgCAAQgDAAgCgCg");
	this.shape_1.setTransform(23.2,26.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(2));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#4F2A3F").s().p("AwJC/QgxgBAAgyIAAkYQAAgyAxABMAgTAAAQAygBAAAyIAAEYQAAAygyABg");
	this.shape_2.setTransform(118.9,25.9);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(10.5,6.8,216.8,38.2);


(lib.shirt2 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAnGKQgEhLABjCQgGhrAKhVQgPgRgggxIgPgWIgCADQgUgYgTgRQgkghhEAAQgKAAgVAJQgWAKgLAAQgMAAgGgCIgFgBIgCgEIABgFQABgGgDgDQgjgfgVg+QgPgrAAgSQAAhGAlhBQArhNBEAAQCAgXC0AXQBwAYAyAwQAmAmAAAyQAABNgCAKQgDAOgrBbQggATgEgHIAAgBQg2AHgrAeQhCAwABBbIAAC4QAQGKAAAUQAAAugYASQgbgkgIixgAiCkRQA1AHAjAWQARAJAKAMQALAJANAQQAYAaATAfQANhAA+goQA9gnA5AVIARgzQAUg6gBg7QABgkglgcQgrgihXgOQibgaiJAaQguARgiBLQgZA3AAAVQAAAcAoApQAUAVAKAOQATgFAYAAQASAAAVADg");
	this.shape.setTransform(82.6,51.9);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ADsCTIAAgZIABgkQACgXAGgOIAAgTIgEARQgEAPgKAAIgHAAIgCgBIgBgHIAGgSIgBgFIABgHQgIANgEAKQgEAFgLADIgQgVQgEgGgBgGQAAgEAEgHIgSgLIgTgNIgQgKQgegTgmgKQgdgIgogFQhDgHg/A0QgFADgEgDQgEgCgDgFIgCABIgDAAQgLAagHADQgHgGgEgHQgBAFgCADQgFAFgQAAQgDAAhOhFIgEgEQAMgIARgPIAsgpQAZgUA8gHQAVgCALgFQA/ACBFAGQBZAGBAAJQAEAEAHACQAxAQAQAHQAEACAYAdQAHAKAIAGIgOATQAUAbAAAzQAAAngLArQgQA7gcAAQgHAAgCgWgAjKgwIACgCIAAgBIgCAAIAAADg");
	this.shape_1.setTransform(82.4,8.7);

	// Layer 4
	this.instance = new lib.TP_logoWhite();
	this.instance.setTransform(111.2,46.8,0.048,0.048,-3.5,0,0,292.3,34.4);

	// Layer 5
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhIA/QhZhEgDgiQgDgiAogcQAogcBPAAQBNAAA0AdQA0AdgJAsQgIAqgqA4QgpA3gdAEIgDAAQgeAAhThDg");
	this.shape_2.setTransform(82.1,16.2);

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#282828").s().p("AAUG3IgEAAIAAgWIAAgBIAAgBIAAgBIAAgIIAAgWIgBgBIAAAAIAAhJIAAhJICLAAIgEDfIgOABQhrgWgJAAgAiUCdQgFgKgBgZQAAhFAahfIAAgCQAMguAQgsQAXg/Aeg6QBEiDBdhKQhBBdgxCIQgNAsgMArIAAAAIgEARIgjDpIAAAMIAFBHIgIAAIgOAAQgwAAgTggg");
	this.shape_3.setTransform(20,59.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#1E1E1E").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_4.setTransform(86,143.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#333333").s().p("AieLYIg8gLQiHgbgtgmQBJgGATAAQBdAAB7AfIBbAYQgYASgTAJgAGWJ5QiBgQgogfQBbgtA/gHQCDgQBEB4QgcADgSADIgDAAQhtgHgagEgApNFQIgFgQQAiAJAyALQArAKApAHIASBsQAQBdADArIg3AEQhZAIgTAFQAAiRgliJgACLHgQgIgegIgTQADgaAAgUIAAgJIBWgCIAAAIQAAAQAQBGIAMA3QgRAGgQAIQghARgUATQgDgugMgvgAl4E1Ig8gLIgYgFIg2gLIAOgBIAEjeIAVAAIAAACQAagDAYABIAHAAQAMAAAEgEIAEgEIABgHQACgEAAgDIgCgCQADg1AKhtIACgWQAEg2ADguQACgyAAgoQAAhegKgEQgJgDgOBDQgKAwgHAyIAAAAIgDAaIgNC9IAAAEIAAAEIAAAEIAAAEIAAAKIAAAEIAAAGIAAAmIkQgEIgFhFIAAgMIAijrIAFgQIgBAAQANgrAPgsQAxiJBBhcQBehKB2gPIEaAAQEZAsB9BMQAxAdApAsIBGBTQB0CHAUAcQAzBKAGA/QghAKgrAXQgbAPgSANIgNglQgPgogYguQgXgrgVghQgJgSgGgdIgJgsQgMgugtgBQgBARAAAXQAAA1A+B6QBECGAFAWQAMA4ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQgxgEhmgTg");
	this.shape_5.setTransform(85.2,77.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#666666").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_6.setTransform(86.9,128);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AjbMJQjFgig5gUQg7gUgBgmQg1kZABg+IAAgMIgQgGQgFACgJAAQgPAAgGgVIgCgLIAAgFIAAgdIAAgBIABgZIABgUIAJiHIg/AAIgDAAQgXgFhGgbQgKgIgFgIQgJgLAAgtQAAhHAWhhIACgIQAJgnALgkQAZhUAmhKQCTkkENgZIEdgFQEsAwCDBNQA1AfAsAuIBMBYIBxCjQBYBeAABGIACAAIADAFQACAEAAALQABALgPAQQgtAkgOAJQgxAgghAKIAPA/IALA7QAKBRABAiQAAAzgXANIgDgHQgbAThBAaIgMAaQgTAsgCAjQgBATAFAnQAAAQgGANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhBgDIk7AAQguACgZgYQgTgRAAgSQgIgXgMg2IgDgMIgBAEQgTBegTAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYguAGgAneKgQALAMBZAQQCRAZASAEIBiAAIB1AAQAQgKAagSIhbgXQh6ggheAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAFAHgAASKDQAgALAWAMQAFgkAUhtQARheAHgyIkuAEQhggEhsgTQgpgIgqgJQgzgLghgKIAEARQAlCIAACRQAUgFBYgHIA3gFQAzgDAdAAQCmAAB4AtgAGwIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADAsAOIEwAAIAJgBIADgBQATgDAbgCQg8hrhuAAQgOAAgPACgAE9F2IhAABIhWABIAAAKQAAATgDAbQAJASAHAeQAMAwAEAuQATgUAhgQQAQgIARgHQBAgXBQgDQAogBAkAEIgDgMQgCgMAAghQABhOAFgbQhNAVhsAPgApSCEIAABJIAAAAIABABIAAAVIAAAJIAAABIAAABIAAABIAAAWIAEAAQAJAABrAWIA2ALIAYAEIA9AMQBlATAyADIGzgPQCeACBpgtQBJggBAgLQgSg0gLhDQgQhwgNg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAANAuIAJAtQAGAcAIATQAWAgAWAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgTgbh1iHIhGhTQgpgsgxgeQh9hLkYgtIkaAAQh3APheBKQhdBJhGCEQgeA5gXBAQgQArgMAvIAAACQgaBhAABFQABAZAFAKQAWAjA7gFIAIAAIEQADIAAgmIAAgFIAAgFIAAgKIAAgEIAAgEIAAgDIAAgEIANi+IAEgaIAAAAQAHgyAJgwQAOhCAJADQAKADAABeQABApgDAyQgCAugFA2IgCAVQgJBugEA0IACADQAAADgCADIgBAIIgDAEQgFADgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAABJg");
	this.shape_7.setTransform(81.1,77.7);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.instance,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-8.8,162.2,164.3);


(lib.shirt = function() {
	this.initialize();

	// Layer 6
	this.instance = new lib.TP_logoWhite();
	this.instance.setTransform(111.7,49.3,0.048,0.048,-3.5,0,0,292.3,34.4);

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AhIA/QhZhEgDgiQgDgiAogcQAogcBPAAQBNAAA0AdQA0AdgJAsQgIAqgqA4QgpA3gdAEIgDAAQgeAAhThDg");
	this.shape.setTransform(82.1,16.2);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#282828").s().p("AB8HLQhqgVgKAAIgEAAIAAgWIAAghIgBgBIAJlPICMAAIgOGfIgOgDgAiXgfQgBgEAFgMQAPgiAUg3QAXg/Aeg5QBEiEBehJQhCBcguCJQgQAsgMArQgRA/gFAiQgEAZACAXIgJAAIgNAAQgwAAgUgfg");
	this.shape_1.setTransform(21.2,60.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333333").s().p("AmgHwIgYgEIgogJIANmfIAVAAIAAADQAYgEAaABIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAPidAAgcQAAhegKgEQgJgCgOBCQgJAwgHAyQgHA2gBARIAAAdIkQgEQgBgVAEgZQAFgiARg/QAMgrAPgsQAxiJBBhcQBThBBlgUQAEAVAYA+QAhBRAYAZQAXAYBSBcQBJBRAPgBQALAAAqhQIABgBQAGA4AVBbQALAwARAmIArAAQgIg1gHgeQgHgwgYhZIgWhQIBGAuQBkBBACAAQAeAAAuh3QAqhsAAgdIAAgIQAkAPAcARQAxAdApAsIBGBUQA/BIAUAdIA4BaQhGAYgzAlIgNglQgYg9gZgyQglhGgYgLQAAAJgDALQgCAJAEALIBRDdQAVA5AVBEQAiBpAHAwQALBDARA0Qg/ALhJAfQhpAuiegCIiPAFIgCAAIkjAKQg/gFiUgdgAiLkqQg9g8gXghQg0hqgSggIEPAAQC8AeB2ArQgHAUgIAgQgNA0gMAcQgUAvgdAYQgMgLg9ghQhEgkgUAAIgBAAQgFgIgNAAQgbAAgWA5QgHAWgNA2QgVgchAg+g");
	this.shape_2.setTransform(83.2,58);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#1A2D3A").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_3.setTransform(67.5,124.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#2B4B60").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_4.setTransform(86.9,128);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3F1E11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_5.setTransform(86,143.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_6.setTransform(94.8,140.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AjHMJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgOAAgGgVIgCgLIAAgiQAAgRAHiJQAIiQAFhGIg/AAQgYgEhIgcQgLgJgFgHQgEgGADgRQAFgVAXhMQA1i0Bsh6QCFiYC5gVIEagFQERAnCOBRQBbAzBkB3IBGBhQBIBlAGAVIABAAIADAFQADAEAAALQAAAJgPASQgrAjgPAKQgxAgghAKQACAJAhBMQAfBJADANQAGAfAKBBQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYguAGgAnLKgQALAMBaAQQCRAZASAEIBiAAIByAAQATgKAagSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAAmKDQAfALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA3gFQAygDAeAAQCmAAB4AtgAHEIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAFRF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgAo/DNIACABIAAAhIAAAWIAEAAQAJAABrAWIAOADIAoAIIAYAEQCUAeA/AEIEjgKIACAAICPgFQCeACBpgtQBJggA/gLQgRg0gLhDQgHgwgihnQgVhDgVg6IhRjfQgEgKACgKQADgLAAgIQAYAKAlBHQAZAxAYA9IANAmQAzgmBGgXIg4hbQgUgcg/hJIhGhTQgpgsgxgeQgcgRgkgPIAAAIQAAAegqBrQguB3geAAQgCAAhkhBIhGguIAWBQQAYBZAHAwQAHAeAIA4IgrAAQgRgpgLgwQgWhbgHg4IgBACQgoBQgLAAQgPAAhJhRQhShcgXgXQgYgaghhRQgYg9gEgVQhlAThTBBQhdBJhGCEQgfA5gWBAQgUA2gPAiQgGANACADQAWAlA6gFIAJAAIEQADIAAgeQABgSAHg2QAGgyAKgwQAOhCAJADQAKADAABeQAAAcgPCfIACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAQgagBgYAEIAAgDIgVAAIiMAAgAi2pMQAXAgA9A8QBAA/AVAbQANg2AFgVQAWg6AdAAQAMAAAGAIIABAAQAUAABEAkQA9AhAMALQAdgXAUgwQAMgcANg0QAIgfAHgUQh2gsi+geIkNAAQASAhA0Bqg");
	this.shape_7.setTransform(79.1,77.7);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(1.5,0,155.4,155.5);


(lib.ShareTwitter = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("Share on Twitter", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(87.1,20.9,0.59,0.59);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Aj6CdQAMACANAAQBIAAA4gsQgigBgagUQgagUgKgfQAIACALAAQAMAAAPgDQgjgIgYgcQgXgbAAglIAAgBQAWANAYAAQgVgOgMgWQgNgXAAgbQAAgcAOgYQAnAvA3AdQA4AdA/ADQgDgNAAgKQAAgrAcgeQAfgfAqAAQAtABAeAgQAhgGAhgTQgMAkghAVQAfgEAcgMQgVAfgfAWIABANQAABuhMBWQhUBgiEAAQhWAAhIgvg");
	this.shape.setTransform(67.5,30,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#23AADB").s().p("AwIC/QgzAAAAgyIAAkYQAAgzAzAAMAgSAAAQAxAAAAAzIAAEYQAAAygxAAg");
	this.shape_1.setTransform(156.4,29.1);

	this.instance = new lib.Share_Twitter();
	this.instance.setTransform(167.2,31.7,0.85,0.85,0,0,0,166.3,30.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3A5A94").s().p("A5SExQgyAAAAgyIAAn9QAAgyAyAAMAylAAAQAyAAAAAyIAAH9QAAAygyAAg");
	this.shape_2.setTransform(167.8,31.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).to({state:[{t:this.instance}]},2).to({state:[{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(48,10,216.8,38.2);


(lib.ShareFB = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("Share on Facebook", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(87,21.3,0.59,0.59);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhDFDIAAknIhjAAIAAhxIBjAAIAAhUQAAhJApgpQAlgnBEABQA5gBAfAFIAABnIg9AAQghAAgNAPQgLANAAAcIAABJIByAAIgPBxIhjAAIAAEng");
	this.shape.setTransform(64.1,29.5,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#395994").s().p("AwIC/QgzAAAAgyIAAkYQAAgzAzAAMAgSAAAQAxAAAAAzIAAEYQAAAygxAAg");
	this.shape_1.setTransform(156.4,29.1);

	this.instance = new lib.Share_FB();
	this.instance.setTransform(167.2,31.7,0.85,0.85,0,0,0,166.3,30.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3A5A94").s().p("A5SExQgyAAAAgyIAAn9QAAgyAyAAMAylAAAQAyAAAAAyIAAH9QAAAygyAAg");
	this.shape_2.setTransform(167.8,31.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).to({state:[{t:this.instance}]},2).to({state:[{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(48,10,216.8,38.2);


(lib.PostRoomLeaderBoard = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("SCORE", "49px 'VAGRounded'");
	this.text.lineHeight = 60;
	this.text.lineWidth = 163;
	this.text.setTransform(581.4,3.3,0.75,0.75);

	this.text_1 = new cjs.Text("NAME", "49px 'VAGRounded'");
	this.text_1.lineHeight = 60;
	this.text_1.lineWidth = 163;
	this.text_1.setTransform(44.5,0.4,0.75,0.75);

	this.text_2 = new cjs.Text("10,564\n9,324\n8,361\n7,183\n6,753\n6,650\n5,921", "40px 'Laffayette Comic Pro'");
	this.text_2.textAlign = "right";
	this.text_2.lineHeight = 54;
	this.text_2.lineWidth = 187;
	this.text_2.setTransform(689.2,74.8,0.88,0.88);

	this.text_3 = new cjs.Text("COOLJ\nMARCO_B\n259_BRYCE\nDAVESYKES32\nTOM93\nPAT_JONES\nJO_KINGSLEY", "40px 'Laffayette Comic Pro'");
	this.text_3.lineHeight = 54;
	this.text_3.lineWidth = 398;
	this.text_3.setTransform(45.4,74.8,0.88,0.88);

	// Layer 3
	this.instance = new lib.ARR();
	this.instance.setTransform(765.5,89.7,0.75,0.68,0,-90,90,22.6,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 193, 193, 193, 0)];
	this.instance.cache(-10,-2,58,54);

	this.instance_1 = new lib.ARR();
	this.instance_1.setTransform(765.5,360.7,0.75,0.68,-90,0,0,22.6,25.1);
	this.instance_1.filters = [new cjs.ColorFilter(0, 0, 0, 1, 193, 193, 193, 0)];
	this.instance_1.cache(-10,-2,58,54);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C1C1C1").s().p("AgyJGIAAyLIBlAAIAASLg");
	this.shape.setTransform(765.5,176.1);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4,1,1).p("Eg9lgdQIAAGIMAAAA0YQACA+AeAhQAWAYAjAJQAbAGAaAAMB2iAAAIAOAAQAcAAAYgGQAjgKAWgXQAJgKAHgNQADgGADgHQAIgVACgeQAAgIAAgIMAAAgu8IAAlUIAAgqIAAlVQAAgwgQggQgfg7hXgDMh3AAAAQiBAFgECAg");
	this.shape_1.setTransform(394.3,200.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C1C1C1").s().p("Eg9lAEGIAAmGQAEiACBgFMB3AAAAQBXADAfA7QAQAfAAAxIAAFTIAAAqg");
	this.shape_2.setTransform(394.3,26.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EA7KAbPMh2hAAAQgaAAgbgGQgjgJgWgXQgeghgCg+MAAAg0YMB7LAAAIAAFUMAAAAu8IAAAPQgCAfgJAUIgFAOQgHANgJAKQgWAXgjAJQgZAGgbAAgEA9GAapQAJgKAHgNIAAAXgEA9WAaSg");
	this.shape_3.setTransform(394.3,226.8);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance_1,this.instance,this.text_3,this.text_2,this.text_1,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,792.5,429.7);


(lib.PhoneMain = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#DDBAA4").s().p("AguAGIgNhwQAAgNAHgOIAKgSIBOAAIAYEkIgNAJIhRABQAAgggMhxg");
	this.shape.setTransform(13.5,-45.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EACFB7").s().p("AhGiRIBoAAQAcAVAHAqQACAPAAA1QAAA3ggAlQgSAUhDAwg");
	this.shape_1.setTransform(24.4,-45.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AheC/QgbgXgSiCQgOheAAgtIABgTQAFgVASgQQAhgdBGgDQAygDBHAKQAtAfAKA4QAEARAABBQAABGgjAqQgXAbhcBAIgBABgAhxiBQgHAOAAANIANBwQANBwAAAhIBTgCIALgJQBFgwATgUQAggkAAg4QAAg1gDgPQgHgqgcgVIhoAAIhRAAIgKASg");
	this.shape_2.setTransform(19.5,-45.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

	// Layer 2
	this.instance = new lib.phone();
	this.instance.setTransform(66.4,24.1,1,1,0,0,0,31.8,24.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({skewX:-71.5,skewY:108.5,x:27,y:-50.1},0).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5B5C5E").s().p("AAWBbQg0hEgwgUQAFgNAAgVQAAgQgMhKQgJg3gCgSQBkB1AwA8QAYAbALAZQAGAQACAPQACAaAABnIgFAAQgYgvgug5g");
	this.shape_3.setTransform(14.8,50.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D2D2CC").s().p("AhXAeQgJgFgCgLIBmgiIAygMIAIgBQAcAQAJAKIgUAGIhNAQIhFASIgHABQgHAAgGgEg");
	this.shape_4.setTransform(25.6,26.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#DADBDC").s().p("AghBeQgbgEgBgRIAlgHQATgEAFABQACACALANIAFAIQgTAIgUAAIgMAAgAAPA4IANgEIAzgGQAHgBAEADIAYAVIgDAGIg5AJIgIABQgZAAgGgdgAB+BBQgJgHgQgRQAtgMAXAGQAVAGAIAXIg7AFIgCAAQgFAAgGgEgAhiAiIA3gLQAFAAADACIAYAaIg+ALgAAAAqQgIgEgMgNIgIgIIBEgOQAZAWALAJIgOADIgSAAQgKgBgGADQgLAFgJAAQgFAAgDgCgABXAdIghgdIAqgCQAXAAAGACQAGABAIAHIAOALIg1AKIgIABIgFgBgAiUgEIBDgMQAXAQAGAKIg7APgAgrABQgFgDgOgQIAugQQAGgCAEAEIAfAcIgKAEIguAEIgDAAQgGAAgDgDgAAkgKIgjgbIABgFIAhgHQAxgJAWAqIg9AIgAi1gdIgPgNQAigLAVACQAbADARAWIgUAEIgYADQgIADgHAAQgOAAgLgNgAhRgiQgIgDgcgVIA5gLQADgBAEABIARAKQAIAHAGAIIgnAMIgKABQgGAAgEgDgAgphNIACgEQAEAAAXgJQAQgGALAGQAeANADAOIgdAFIgaAIg");
	this.shape_5.setTransform(42.7,39.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#7B7C7F").s().p("AlTgGQgCgOgHgRQAkAMAIABIAfAAQGwhEC9gcIABATQAAANgOBhQgOAJgWAGQgvANiiAXQlEAuhnAPQAAhmgCgZg");
	this.shape_6.setTransform(58.4,58);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AjlEwIgOgEIAAAAQgEACgJAAQgNAAhbhhQhZhggYglQAHgBAIABQgIgYgHg+IAAgCIgQhpQAAgoABgMIABgEQAGgaAjgSQBPgmFhgsQDUggCcCFQAqAlAgArQATAOAJAIQAUAUAEAdIACApQAEAPACAQQACANAAANQAAAogDAbIABARQABAygdAcQgYAYiiAOQimALgUADIiEAZQiAAYgLAAQgPAAgfgGgAhQj6IguAIIi2AfQgWAGgUAIQgvAUgZAYQgSARAAAKIABAJQACASAJA3QAMBIAAAQQAAAVgFAPQAwAUA2BEQAuA5AYAvIAFAAQBogQFDgtQCjgXAugNQAXgHAOgIQANhkAAgNIgBgTQgGhVhGhEIgbgQQgngggpgcQgqgVgtgOQg7gTgpAAQhQAAhHAFg");
	this.shape_7.setTransform(49.1,43);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#939598").s().p("AjTC+QgIgBgkgMQgLgYgXgeQgwg7hmh0IgBgIQAAgLARgRQAZgXAwgUQATgIAWgHIC3geIAugIQBGgFBRAAQAoAAA7ASQAuAOApAWQApAcAoAgIAbAPQBFBCAHBYQi9AcmwBEIgfAAgAhYBjIgmAHQACARAbAEQAaADAagLIgEgIQgLgNgFgCIgDAAQgGAAgOADgAAOBPIgxAGIgMAEQAGAhAggFIA4gJIACgGIgXgVQgDgCgGAAIgDAAgAAlBKQAQARAIAHQAIAFAGgBIA6gFQgHgXgVgGQgIgCgJAAQgVAAgeAIgAiiBDIAYAcIA/gLIgYgaQgDgCgFAAgAhdAyIAJAIQALANAJAEQAMAGASgJQAGgDAKABIARAAIANgDQgLgJgXgWgAgJAhIAgAdQAEACAJgCIA1gKIgOgLQgIgHgGgDQgGgCgYAAIgoAEgAjUAbIAlAfIA7gPQgGgKgYgSgAhQgBIguAOQANAQAGAFQAEAFAIgCIAwgGIAKgEIghgbQgDgBgDAAIgEAAgAgcgQIggAHIgBAFIAjAZIAIACIA8gIQgSghgjAAIgRACgAkFgJIAQALQAQATAXgJIAYgDIAVgEQgSgUgagDIgHAAQgTAAgeAJgAh9gkIg4ALQAcAVAHADQAHADANgCIAogLQgGgIgJgHIgQgKIgEgBIgEABgAhMg5QgYAJgDAAIgCAEIAhAbIAdgIIAdgFQgEgOgdgNQgGgDgHAAQgHAAgJADgAiziCIgyAMIhoAkQACALAJAFQAJAFALgCIBHgSIBNgSIAUgGQgJgKgcgQIgBAAIgHABg");
	this.shape_8.setTransform(49.1,36.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(2));

	// Layer 4
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#B52D30").s().p("AglApQg4kNAAhAIAAg0QAAgZASgLIAOgIIAXAAICEMIIgnABQgig8g6kgg");
	this.shape_9.setTransform(16,12.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#D5363A").s().p("Ah+mEICHAAQAhA3AoCYQAtCtAAA/QAABAgbEMIheABg");
	this.shape_10.setTransform(24.8,12.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAnGqIg8AAQg7hKghhoQgchYgFheQAAgNgViXQgUiXAAgpIAAhHQAIgwAngCQAFgGAGgCIAYAAIB9AAIAAgEQBPgYAyDcQAlCjAAB8QAFBUgFBQQgKCbgvAvgAiIl6QgSALAAAZIAAA0QAABAA4EOQA8EfAiA8IAlAAIBegCQAakMAAg/QAAhAgtisQgniZgig2IiGAAIgXAAIgOAHg");
	this.shape_11.setTransform(22.1,12.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-2,100.1,76.1);


(lib.OKbutton = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.OK_btn();
	this.instance.setTransform(98,31.7,1,1,0,0,0,67,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AqrFDIAAqFIVXAAIAAKFg");
	this.shape.setTransform(98.2,30.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(29,-2,138.2,67.5);


(lib.NoBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.No_Btn();
	this.instance.setTransform(99,31.7,1,1,0,0,0,99,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AtGEtIAApaIaNAAIAAJag");
	this.shape.setTransform(80,30.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1,x:99}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85,x:99.1}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.MotifGreg = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BC9A72").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D5AE81").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgOEQQgKhDAAgbIABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgPQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgvQAAgmAGgtIAtADQgGAnAAAgQAAAZAGAKQAVAlA7gFIEXADIAAgoIAAgTQAaAUASAaIgCAiIACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAiQgMA8gZAOIhEAAQgsgRg1gRQACAPAAATIAAAWIAEAAQALAACdAhIAYAFIBUAPQgFALgGAKQgKAQgPANIgmgGQgpgIgrgJQgygLgigKIAFARQALAoAIAoIgMAAIghgBgAjDgwQAAAGBRAdIAyAPQCDAtAdAAQAgAAABhBIgDhBIjnAAIgXAAQgZgEgUgGIgWAtg");
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt03();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D5AE81").s().p("AhJAgIgOg/QAggKAyggIAXgRQAIAOAZAhQAlAwAAAtQAAAXhYAIQgoAEgWAGIgLg7g");
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#497C59").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5A996E").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_5.setTransform(85.2,58);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_6.setTransform(86,143.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_7.setTransform(67.5,124.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_8.setTransform(9.8,86.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_11.setTransform(25.6,90.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_12.setTransform(84.4,77.7);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.MotifFrank = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A4D30").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#975F3C").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgOEQQgKhDAAgbIABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgPQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgvQAAgmAGgtIAtADQgGAnAAAgQAAAZAGAKQAVAlA7gFIEXADIAAgoIAAgTQAaAUASAaIgCAiIACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAiQgMA8gZAOIhEAAQgsgRg1gRQACAPAAATIAAAWIAEAAQALAACdAhIAYAFIBUAPQgFALgGAKQgKAQgPANIgmgGQgpgIgrgJQgygLgigKIAFARQALAoAIAoIgMAAIghgBgAjDgwQAAAGBRAdIAyAPQCDAtAdAAQAgAAABhBIgDhBIjnAAIgXAAQgZgEgUgGIgWAtg");
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt03();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#975F3C").s().p("AhJAgIgOg/QAggKAyggIAXgRQAIAOAZAhQAlAwAAAtQAAAXhYAIQgoAEgWAGIgLg7g");
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#497C59").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5A996E").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_5.setTransform(85.2,58);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_6.setTransform(86,143.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_7.setTransform(67.5,124.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_8.setTransform(9.8,86.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_11.setTransform(25.6,90.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_12.setTransform(84.4,77.7);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.Motif = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape.setTransform(25.6,90.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_1.setTransform(9.8,86.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgOEQQgKhDAAgbIABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgPQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgvQAAgmAGgtIAtADQgGAnAAAgQAAAZAGAKQAVAlA7gFIEXADIAAgoIAAgTQAaAUASAaIgCAiIACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAiQgMA8gZAOIhEAAQgsgRg1gRQACAPAAATIAAAWIAEAAQALAACdAhIAYAFIBUAPQgFALgGAKQgKAQgPANIgmgGQgpgIgrgJQgygLgigKIAFARQALAoAIAoIgMAAIghgBgAjDgwQAAAGBRAdIAyAPQCDAtAdAAQAgAAABhBIgDhBIjnAAIgXAAQgZgEgUgGIgWAtg");
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt03();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#497C59").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_3.setTransform(18,46.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#5A996E").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_4.setTransform(85.2,58);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_5.setTransform(86,143.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_6.setTransform(67.5,124.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_7.setTransform(9.8,86.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_8.setTransform(86.9,128);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_9.setTransform(94.8,140.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EAD0B7").s().p("ArABdIgygRIABgHQAHgpABgWIDpAAIADBBQgBBDggAAQgdAAiFgtgAJQgNIgPhBQAhgKAxggIAZgRQAIAOAZAhQAmAyAAArQAAAXhaAIQgoAEgWAGIgLg5g");
	this.shape_10.setTransform(88.9,83);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_11.setTransform(84.4,77.7);

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.LogOutRoundBtncopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		this.stop()
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1));

	// Layer 4
	this.text = new cjs.Text("Logout", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(35,-4.2,0.59,0.59);

	this.instance = new lib.ARR();
	this.instance.setTransform(9.6,5.4,0.348,0.348,0,0,0,22.7,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 255, 255, 0)];
	this.instance.cache(-10,-2,58,54);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{skewY:0}},{t:this.text,p:{text:"Logout",lineWidth:84}}]}).to({state:[{t:this.instance,p:{skewY:180}},{t:this.text,p:{text:"Login",lineWidth:67}}]},1).wait(1));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ahfh7IC/AAIAAD3Ii/AA");
	this.shape.setTransform(15.2,4.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4F2A3F").s().p("AwJC/QgyAAAAgzIAAkXQAAgzAyAAMAgTAAAQAxAAAAAzIAAEXQAAAzgxAAg");
	this.shape_1.setTransform(102.5,4);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.9,-15.1,216.8,38.2);


(lib.LeaderboardButton = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Lead();
	this.instance.setTransform(89,19.5,1,1,0,0,0,89,19.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("At5DCIAAmEIbzAAIAAGEg");
	this.shape.setTransform(89,19.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regY:19.5,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regY:19.4,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,178.1,41.3);


(lib.HS_Bubble = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("consecte tur adip iscing elit Mauris id lorem ipsum dolor sit amet iscing elit Maur.  ", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 395;
	this.text.setTransform(103,-80.3,1.18,1.18);

	// Layer 4
	this.stretch_mc = new lib.hns_stretch_bubble_mc();
	this.stretch_mc.setTransform(100.8,19,1,1.139,0,0,0,264.9,75);

	this.bottom_mc = new lib.hns_bottom_bubble_mc();
	this.bottom_mc.setTransform(117.1,137.2,1,1,0,0,0,281.2,43.2);

	this.head_mc = new lib.hns_head_bubble_mc();
	this.head_mc.setTransform(100.8,-66.4,1,1,0,0,0,264.9,10.4);

	this.addChild(this.head_mc,this.bottom_mc,this.stretch_mc,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-166.1,-80.3,566.5,262.8);


(lib.Highlight = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.HighlightCircle("synched",0);
	this.instance.setTransform(24.8,24.8,0.839,0.839,0,0,0,24.8,24.8);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({_off:false},0).to({scaleX:0.98,scaleY:0.98,alpha:1},2).to({scaleX:1.63,scaleY:1.63},9).to({scaleX:1.85,scaleY:1.85,alpha:0},3).to({_off:true},1).wait(1));

	// Layer 2
	this.instance_1 = new lib.HighlightCircle("synched",0);
	this.instance_1.setTransform(24.8,24.8,0.839,0.839,0,0,0,24.8,24.8);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:0.98,scaleY:0.98,alpha:1},2).to({scaleX:1.63,scaleY:1.63},9).to({scaleX:1.85,scaleY:1.85,alpha:0},3).to({_off:true},1).wait(5));

	// Layer 1
	this.instance_2 = new lib.HighlightCircle("synched",0);
	this.instance_2.setTransform(24.8,24.8,0.839,0.839,0,0,0,24.8,24.8);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX:0.98,scaleY:0.98,alpha:1},2).to({scaleX:1.63,scaleY:1.63},9).to({scaleX:1.85,scaleY:1.85,alpha:0},3).to({_off:true},1).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(2.8,2.8,44.1,44.1);


(lib.HandS_Bubble = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.OKbutton();
	this.instance.setTransform(247.2,137.3,1,1,0,0,0,95.5,31.7);
	new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.OKbutton(), 3);

	this.instance_1 = new lib.HS_Bubble();
	this.instance_1.setTransform(570.5,197.9,1,1,0,0,0,418.4,161.1);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-14,-43.5,566.5,262.8);


(lib.GotItButton = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.GotIt();
	this.instance.setTransform(99,31.7,1,1,0,0,0,99,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AvnFHIAAqNIfPAAIAAKNg");
	this.shape.setTransform(100.1,32.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1,x:99}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85,x:99.1}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,202.2,67.5);


(lib.GirlOutfit04 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.TP_logoWhite();
	this.instance.setTransform(84.8,46.6,0.046,0.046,-3.5,0,0,292.4,34);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#282828").s().p("ACPFVQhegPhUgJIgkgEQgfgFgdgHIg1gNQAFgJAHgSQANgnAJg6IAOhSQAFgaAAgeIAAgKIAAgCIgBgJIgBgFQAHgCAHgCQAFAAAJALQAHAKAFALQADAHgKAvQgDA2AmAsQAoAsAxAWQAeANA4ALQAyAKASAKQAeASAIAoQgzgEgWgDgAkkhDIg4gUQgagJgBgJQgEgbAngsQAegjAkgZQAtgfBKgaQgVAQgiAvQgnA0gUAtQgHAPgFAgQgCAUgIAAIgBgBgAFDipQgLgbgPgPIgGgFIAvAAIAJgFIAdBAQgQAYgOAOQgJgOgOgkgABLiNQgMgDgNgPIgcgbQgWgWgMgVQgOgVgIgRIgPgeIgIgYQgDgLgIgCIABAAIAjgNQAUAiAoAqQAJAIAeApQAdAoAgAfQgTAOgKAAQgQgBgIgDg");
	this.shape.setTransform(41.3,44.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#494949").s().p("AAvDRIg9gTQgHgWgVghQgTgegIggQgYhngGghQgIgvAAgwQAAgNACgWQADgVgBgJIApAUQAaALArAGIAMABQABA2AIAxIAXBzQAKA1AQAxIAfBZQgegFgfgKg");
	this.shape_1.setTransform(32,99.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AigDQQgigCg3gJIgfhZQgRgxgKg0IgWh0QgIgxgBg2IBRALQA+AHBLAIQBXAHCDgFQCNgFBYgRIAGgBQgBAGAGArQAEAxgIAfQgfB4ggBUIgZA+IgCAAQgTAFh6AJQh+ALgzgBQguAAgogEg");
	this.shape_2.setTransform(68,102.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#333333").s().p("ABvFgQADgWABgcIgBhoQADirgKhRQgGgsgNg2IANARIAjAyQALARAMAAQA0AAAyhhQAqhSAAgmIgBgQQARAFArAiQAyAmATAdQAdApAPBKQABAFgUAKIgbANQgghAglgaQgngcAAAxQAAAFApA5QArA7AGA9QAFA6gcArQgeAhAAACQAAALACADIACADIAEACQAHACAMAAQAFAAAJgEIAMgGQABATAHAmQAHAqAGAKIgdAIQgWAGgXAFQhKAQiTAAIgaAAgAAyFfIgRgCQgIgogbgRQgTgLgxgKQg4gLgegNQg0gWgngsQgngrADg2QAKgwgDgGQgEgMgIgKQgJgLgFABQgHABgGABQgDgRgHgFQgKgHgQAVIgJAMIgQADIgBgDIgpgmQgLgLgKgGIgjgOQAIABADgVQAFggAGgOQAVguAmg0QAjgvAVgPQALgEARgMQAQgMAYgIIgBAMQAAAIANAhQAPApAVAkQA/BrBNAAQAeAAA6g1QAPgNAFgQQAFgVACgCQAQA/AIA8QADANAJBfIAAE8IgUgBgAhyjdQgegogJgJQgpgqgVgiIAOgFQA0A9APALQAiAbATALQAeARAsAJIAaAAIgfAeQgXAXgSAMQgggfgdgogACkjcQARgKAlgjQAfgdALgQQADgCAHACQgTAcgNAqQgJAbgWAlIgNAVg");
	this.shape_3.setTransform(59.5,44.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#070707").s().p("AinAoQg1gFgHgDIAIgYQAGgNAPgIIAOgGIAWgGIAAAHQAAAeBiAZIAaAGQgbABgkAAQgRABgxgFgACDANQAAgNgDgLQAFABAGgIQAKgMACgBQAKgIAPgFQALAsANAOQAJALATAIIhIAGQgSAAgIACQABgLAAgRg");
	this.shape_4.setTransform(59,148);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#262626").s().p("Ai8AqQhjgZAAgeIABgHQA6gMCNAAQAUADAPAOQAQAPgBAQQgCAHgRAGQgiAOhIAFgABLATQgNgPgKgrQAcgIAyAAQBIAAAqATQAjAQAJAYQACAHghAFQhDAIhXAGQgTgIgJgLg");
	this.shape_5.setTransform(71.5,147.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AigLxQg8gKgIgLQgHgLgBgXQAAgyA1gkQBIgjAigHQAPgDASgBQAmgDAjAEQArADBCAiIABAAQANgUAogOQAOgFARgDQAugIA3AEQAnACAhAKQAzASAJAGQA/AjgDA2QgBAPgFAFQgFAHgTAEQgOAEigAUQhCAHg2gHQgWgEgCgIQgDgHAAgIIgEAGQgVAZhbALQg3ADg1AAQg1AAgwgDgAh8J4IgWAHIgOAFQgPAJgGAPIgIAYQAHADA1AEQAxAFARAAQAkAAAbgBQBHgFAigOQAQgHADgHQABgRgQgPQgPgPgUgCIgLgBQiCAAg5AMgADWJmQgPAFgKAHQgCACgKAMQgGAHgFAAQADAMAAAOQAAARgCALQAJgDASAAIBIgFQBXgHBDgIQAhgEgDgHQgIgbgjgQQgqgThIAAQgyABgdAIgAhpHEIgWgCQgygJg4gMIAAABIgBgBQgYgGgCgFIAEgEQgJgDgPgQQgOgPAAgHQgWgggLgYQgWgrgLhEQgLhHgIg/IgEgyQAAgtANg5IAAgBIABgBQADgOAFgGIgBgJQAJg0gCAFQAIgrAJg+QgEhGACgFIgMgJQgugjgbgKIgSgIQgPgGgLgBQgFAAgIgEIgHgDQgOgMgcgJIAAgEQgJgEgFgGQgEgIgCgNIABgNIAAgBQAIhcCIhPQBJgrAggPQA2gZBCgMIAjgFQABgBABAGIABABIABADQAZAIAuAjIA7AsQAWARAWADICbAAQAYgNAbgoQASgcAWgEQAKgMAgAIQAHABADAGQAFAHAJAFQAaANAaARQAMAEALALIAaAVQBrBRAABoIgBAHIABAFIgGAKIgKAFQgtAfgGACQAHAXAHBCQADAcgDAeQgCAVgIAjQgBADAFAlQAEAkADAKIAMBGQAAAMgGAKIABARQAAA9gZCMQgYCIgQAlIgKAUQgKAUgMAMQhIARhHAIIh1AKIg8ADQhnAAhcgMgAlQgQQgDAUAAANQAAAwAIAuQAGAiAZBoQAHAhAUAeQAUAhAHAVIA/AUQAfAKAfAEQA2AJAjADQAmAEAtAAQAzAACBgKQB6gKATgFIACAAIAZg+QAghTAfh7QAIgegFgxQgFgsABgEIgGABQhYAPiOAGQiEAFhYgIQhIgHg+gHIhRgJIgNgCQgtgFgagLIgogUQAAAJgCAVgACWmuQAKBTgCCrIAABoQgBAbgDAXQCnACBQgSQAXgFAWgHIAdgIQgGgJgHgqQgGgngBgSIgNAGQgJADgFAAQgMABgGgCIgFgCIgCgEQgCgCABgLQAAgCAdghQAdgrgGg6QgFg/gsg7Qgpg6AAgFQAAgwAnAcQAlAaAhBAIAbgOQATgJgBgFQgPhKgdgqQgTgcgygmQgrgigQgGIAAARQAAAlgqBSQgyBigzAAQgNAAgLgRIgjgyIgNgRQANA2AGAsgAkamPQAHAGADASIAAAHIABAJIAAACIAAAKQAAAegEAaIgOBSQgJA6gNAnQgHASgGAJIA2ANQAcAHAgAFIAkAEQBVAJBdAQQAWACAzAEIARACIATABIAAk8QgJhhgCgNQgJg9gQg+QgCACgFAVQgFAQgPANQg5A1gfAAQhNAAg/hrQgUgkgQgpQgNghAAgIIABgMQgYAIgQAMQgRALgLAEQhKAagtAfQgkAZgeAjQgnAsADAbQACAJAZAJIA5AUIAjAPQAKAFAMAMIAoAoIABADIAQgDIAJgOQAMgQAJAAQADAAACABgACepKQAPAPALAbQAOAkAIAOQAOgOARgYIANgVQAXgkAJgcQAMgpATgdQgHgBgDABQgKAQggAdQglAkgRAKIgIAFIgwAAgAisrSIgjANIAAAAQAHACAEALIAHAYIAQAeQAIARAOAVQANAVAWAWIAcAbQANAPAMADQAJADAQABQAKAAASgOQAQgMAYgXIAggeIgcAAQgqgKgegQQgTgLgigbQgPgMgzg8g");
	this.shape_6.setTransform(55.2,82.1);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,6.4,110.4,151.4);


(lib.GirlOutfit01 = function() {
	this.initialize();

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#282828").s().p("AjjAKQgegngQgnQgPgmAKgJIAEgEQBfC+CZAgIgCADQgLAOgNAAQhcAAhThugABrBbQgQgSgNgDIAMgLIADAAQAFAAAUAEIAZAEQAXAABOgvIAngXQgQAUgZAXQhBA+g2ASQgCgMgOgRg");
	this.shape.setTransform(72.5,15);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AklhpQAlgdArAxQAYAbAdAxQAlAmAdAYQAbAXAjAWIgNATQiZgghfi+gABjBNQgUgEgFAAIgDAAIBqhaQAagXAhgvQAagbAfAnQAEAnghAtIgnAZQhOAvgXAAIgZgEg");
	this.shape_1.setTransform(74.4,13.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgFCxIAAhEQgFg0gBgbQgVARglAbQhyAAhZiAQgkg0gLgwQgMgwAUgRIAHgGQAogdAjAXQAPAKBFBOQBaBmAxAeQAFgNAKABIAHABQAUgRAkgcQBIg5AMgHQAPgJAPgbQASggAEgFQAegiAuAZQAqAYgBAZQgBANgHAQQgOAmguA6QhOBhhRAVQgLADgPgeQgSgigWgGIAAAPQgBAcgGA1IAABKQgDA8gNAAQgLAAgDhBgAkljIIgEADQgKAJAPAmQAQAnAeAqQBTBsBcAAQANAAALgPIACgCIANgUQgjgTgbgXQgdgZglgoQgdgwgYgcQgbgegZAAQgOAAgOALgADsi3QghAvgaAYIhqBcIgMAKQANADAQAQQAOARACAMQA2gRBBg8QAYgYAQgWQAigugEgoQgTgXgQAAQgMAAgKALg");
	this.shape_2.setTransform(74.4,23.1);

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#543126").s().p("AinAoQg1gEgHgEIAIgXQAGgOAQgIIANgGIAHgCIAQgEIgBAHQAAAfBiAYIAaAGQgdACgiAAQgRAAgxgFgACDANQAAgNgDgLQAFAAAFgGQAIgLAGgEQAJgHAPgEQALArANAOQAIAKAUAJIhIAFQgUACgHABQACgLAAgRg");
	this.shape_3.setTransform(65.9,152.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#774736").s().p("Ai8AqQhjgYAAgfIABgHQA6gNCNABQATACAQAQQAQAOgBAQIgFAFQgHAHgQAFQgjAMg+ADIgagGgABLAUQgNgPgKgrQAcgJAyAAQBIAAAqATQAjAQAJAYQACAIghAEQg2AGhkAIQgTgIgJgKg");
	this.shape_4.setTransform(78.3,151.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AkrBgQg9gKgIgLQgHgMAAgVQAAgxA0gkQBHgjAjgHQAQgDASgBQAkgDAmAEQApADBEAhIAAABQAMgVAogNQAPgEARgEQAtgIA3AEQAoADBKAXIASAKQBAAkgEAzQgBAOgEAGQgGAHgSAEQgOAEigAUQhCAHg3gHQgWgFgCgGIgDgQIgDAGQgWAYhZAMQg5AEg1AAQg1AAgvgEgAkHgXIgQAFIgHACIgNAGQgQAIgFANIgJAYQAHADA1AFQAxAEARAAQAjAAAdgCQA+gDAjgMQAQgFAHgHIAFgFQABgPgQgPQgQgQgTgBIgUgBQh9AAg2AMgABLgpQgQAEgJAIQgFAEgIAKQgFAHgGAAQADAKAAAOQAAARgBALQAGgCAUgBIBIgFQBkgJA2gGQAhgEgCgHQgJgZgjgQQgqgThIAAIgFAAQguAAgbAJg");
	this.shape_5.setTransform(76,152);

	// Layer 4
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#192D3A").s().p("AhYFyQgWAAgOgEIgigNQgQgIgIgGIgGgFQhjkygLgcQgziJAAiQIADgSIAHghIAIglQAdAMBkARIAIABIAEAAIATADQAAB6AdChQAEAVAQCpQAQCfALAwIAVAagADfFsIhAgKIAAgIQgCgagDh/IgDiOQAQAdAXAYQALALA6CCQA0ByAnALIgQAAQg4AAg3gGg");
	this.shape_6.setTransform(58.9,111.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#2B4A60").s().p("AkzFKQgLgwgQifQgQipgEgVQgdihgBh6QBFAICjAPQBZAICggFQCsgGBYgQQADgBAKgGQAJgFABAGQAJBBgIBXQgKBkgfBdQgGAOgHArQgKA6gLAtIgsDCIgtADQgwAFgfABQgngLg0hyQg4iCgLgLQgXgYgQgdIgFhVQADg4gCgYQgmADgGAoQgDAVAFAVIAFBbIADCNIADCFIgBALQAAAHgVADIiqANg");
	this.shape_7.setTransform(80.1,112.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("ABfGLQg5gFgOgHIgRgIIgGgDIAAAAIgJAFQgrAUhKAEQgmAFgngGQgYgEhNgTQgZgGgDgDQgfgbhHjcQhQj3gCidQAAghAHgmQAGgjAHgOIC8AAIAkAEIAJABQB6AOBJAKIAtAGQAbADgBgDQBFAEBjgBQCkgCBAgOQAXgFARgGIAPgGIAJgCIgCgDQA+AHgJAAIgNAAIAEAbQAFAqAAANQAABdgCAcQgHBTgZBOQgNAogjC0QgeCagSAYQACABgDAGQgEAFgFABQAEACgVAFIgsAJQg5AJgyAAQgzAAg5gJgAnQlZIgHAhIgDASQAACQAzCLQALAaBjEyIAGAFQAIAGAQAIIAiANQAOAEAWAAIAOAAICqgNQAVgDAAgHIABgLIgDiFIgDiNIgFhbQgFgVADgVQAGgoAkgDQACAYgDA4IAFBVIADCOQADB/ACAaIAAAIIBAAKQA/AHBAgBQAfgBAwgFIAtgDIAsjCQALgtAKg6QAHgrAGgOQAfhdAKhkQAIhXgJhBQgBgGgJAFQgKAGgDABQhYAQisAGQiiAFhXgIQijgPhFgIIgSgDIgEAAIgIgBQhkgRgdgMIgIAlg");
	this.shape_8.setTransform(71.4,112.4);

	// Layer 1
	this.instance = new lib.TP_logoWhite();
	this.instance.setTransform(97.2,38.2,0.048,0.048,-3.5,0,0,292.3,34.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#282828").s().p("AEUFZQhpgNhkgLQhlgNhSgQQAGgIAGgTQAIgWAOhlIAOhSQAFgaAAgfIAAgIIAAgBIgBgJIAAgHQAGgDAHgBQAFAAAJAMQAIALAEAJQADAHAIAvQANA2AnAsQAnAsA4AaQAeAOBAATQA2APAWAOQAhAWAIAoQgygEgWgDgAkZB3QgqgPgTgRQgNgKAWhQQAUhLANgWIAWgoQANgYAMgPIAaglQAdgoASgOQBphYBrAIIg2ATIg4AVQgpAegVAvIgTAhQgOAYgIAVQgEANgMBVQgMBdgcBgQgQgDgcgKg");
	this.shape_9.setTransform(38.5,40.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#333333").s().p("AgFFfQgIgoghgWQgWgOg3gPQg/gTgfgOQg3gagogsQgngsgOg2QgIgvgDgHQgFgJgHgLQgJgMgFAAQgHABgHADQgDgTgHgFQgDgCgFABQgFgegXAiIgQAdQgDAFgOA5QgPA7gOAaQgDAFgRAAQgTAAgegGQAchgANhdQALhVAFgNQAHgVAPgYIASghQAWgvAogeIA5gVIA3gTIAWADQAHgCAFgDIABAFQAGgCAHAAQARAAAOALQAIAHAIALIAZAfQASAVAIALQAXAlAvAcQA6AkA8gBIBJAAQAqgMAigeQASgQAdggQAZgXAMgpQAFgRAOgGQAHgDAKAAQAOAAAJAFIABgDIAdAfIByBUQAfAXAWAoIAMAWIARApIAGAPIAMAoIATBGQALAsAQAiQgQABgQAEQgUAEgSAJQglASgHAUQgDgIAAgCQAIgjACgUQADgegDgcQgHhAgGgXQgdg+gZgfQgbgggUgEQgVgEAAAhQAAAFAqA6QArA7AFA9QAGA6gdArQgOAWgPANQAAALACACIACAEQADADATAAQAGAAAIgEIANgGQAAANAFALIALAdQAFAQgGAbQgFAXgGAJQgJAMgJAEQgIAEggAHQg0ALh9AEIhVABQhGAAg+gDg");
	this.shape_10.setTransform(74,40.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#262626").s().p("AADCeIgwilIgUhiQgMg4gDgkIgCgKIAFgLQAoAaBzAQIAFAAIgNBBQgMA8gKA+QgUCFgHBLIgSg9g");
	this.shape_11.setTransform(33,95);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#424242").s().p("AmWDRIgRhDQAHhLAUiDQALhAALg8IANhBQBLAKCuAQQBaAICugFQC5gGBXgQQgVAlAAAeQAAAGAFAKQAEAJAAAGQAAAXgRA/QgUBMgIA1IgQB1IgEAnIgKACQg2AJiTAKQiVAKg1AAQiiAAiygtg");
	this.shape_12.setTransform(77.6,102.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("Ai5KKQgZgEhHgQIgcgGIgMg0QCyAsCgAAQA1AACXgKQCTgJA2gKIAKgCIgCApIgHACQhGAQhUAIIiBAJQgqADgqABQiQAAhhgPgAgbCFQiugQhLgKIgFAAQh0gQgogaIgFALQgCgJgIgEQgMgEgIgFQADgDAIgNQAGgLADgCIgDgEIgBgnQAAgJAKgjQAMgqADgaIAJg4IgLA3QgLA0gNADQgLAIgHgBQgWgBgZgFQg0gIgkgSQgmgRgTgQQgPgNAEAAIgFgHQgEgIAAgMQAAgiAqiLQAMgnAfg1QAlhAAqgpQAcgbAWgQQAbgTAjgOQA9gXAtgJQA0gMAsABIADAFQACADAAAMIACAJIAIAjQgOgLgRABQgHAAgFABIgBgEQgFADgIABIgVgCQhtgIhpBXQgSAPgdAnIgaAmQgMAPgNAYIgWAnQgNAXgUBLQgWBRANALQATAQAqAPQAcALAQACQAdAGATAAQARABADgFQAPgaAOg8QAPg7ADgFIAQgcQAXgiAFAeQAEgCAEADQAHAFADASIAAAHIABAJIAAACIAAAKQAAAegFAaIgOBSQgOBlgIAUQgGATgGAIQBSARBnAMQBkALBpAOQAWADAwADQBoAFB0gDQB9gDA0gMQAfgHAJgDQAIgEAJgNQAHgIAFgYQAFgZgFgPIgLgeQgEgKgBgNIgMAGQgJAEgFAAQgTAAgEgEIgCgDQgCgDAAgLQAQgNAOgWQAcgrgFg6QgGg/grg7Qgpg5AAgFQAAgiAVAEQATAEAbAhQAaAeAcA+QAHAXAHBDQADAbgEAeQgCAVgHAjQgBACADAHQAHgUAlgRQATgJAUgFQAPgEAQgBQgQghgKgsIgThIIgNgpIgFgPIgSgpIgLgWQgXgngfgXIhyhVIgdgfIAAADQgJgFgOAAQgKAAgIADIADgWQABgIAFgBQAFgCAIADQAIADAFgBQAGgCAUAGIgDAAIALACQAcAGAlAYQApAZAgAgQAtAqAdA2IAKAVQAsBgAPApQAZBJAAA/IAAAGIAAAFIgBAFIgBACIgGALIgNADIgBgBIgjARQgfAMgwAQIgLgBQABADABAZQABAdACAKQABALAAASQgBATgCAGQgLAagHAJIgOASIgDAFQhXAQi5AGQhEACg3AAQhYAAg1gFg");
	this.shape_13.setTransform(69.1,66.5);

	// Layer 2
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0F0F0F").s().p("Ai/BVQgegTgTgWQgQgSAAgHQAAgUAOgLQAHgGAKgEIgKgFQhQgpAAgXQAAgTACACIAFgCQADgCAFgBQgHgMAAgHQAAgPAWALIBEAmQBbAvBmAPQBNAJAqgJQAqgJANgGQAOgGBKgwQBGguAAA4QAAAEgBAEQAJAOAAAjQAAA0gsAwQhLBQisAAQiAAAhYg5g");
	this.shape_14.setTransform(74.2,17.3);

	// Layer 3
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0F0F0F").s().p("AB1AuIidAAIgHAAQg2gJgzgUQgZgKgbgTQgPgLgEgGIAAAAIgBAAIgDAAIgCgBQgIgBgHgEQgFgEAAgCQAAgBAAAAQAAgBAAAAQABAAAAgBQAAAAABAAIACgBIAQAAIAGABIADAAQAKAAAfAIQAhAIA9ATIC9AAQBhgSAcAAQAPABAEAHIABAEIABAFQAAAXg2gBQAKACAAANQAAALgTAIg");
	this.shape_15.setTransform(73.7,9.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#223825").s().p("AghgBQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAAAAAQACgEAJAAIAkAAQAGAAAHADQAGACAAACQAAACgDAAQgEAAgJgCIgnAAIgGABQACACARABQAOABAAAEQAAADgEAAQgjAAAAgLg");
	this.shape_16.setTransform(44.9,6.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AggBNIgWgHICeAAQgDABgEAAQghACgRAEgAhjANQg9gRgggIQgfgIgLAAIgDAAIgGgBIgQAAQgIgCgFABQAAAAgBAAQgBAAAAAAQgBAAAAgBQAAAAAAAAQAAgKAJgCQgSgKAAgIQgBgFAHAAIAAgDQAAgeAiAdIAJAJIAhAJQBpAgAnAAICJgDQAhgBCEgWIAJAAQAJgEAXAKQAAADgNAJQgIAMgcAOIgBgDQgFgGgPAAQgcAAhhAQgAjugFIABAAIAAAAIAAABg");
	this.shape_17.setTransform(75.1,7.5);

	this.addChild(this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.instance,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.2,-1.1,137.8,163.2);


(lib.FrankShirtandTie = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BC9A72").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D5AE81").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgOEQQgKhDAAgbIABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgPQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgvQAAgmAGgtIAtADQgGAnAAAgQAAAZAGAKQAVAlA7gFIEXADIAAgoIAAgTQAaAUASAaIgCAiIACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAiQgMA8gZAOIhEAAQgsgRg1gRQACAPAAATIAAAWIAEAAQALAACdAhIAYAFIBUAPQgFALgGAKQgKAQgPANIgmgGQgpgIgrgJQgygLgigKIAFARQALAoAIAoIgMAAIghgBgAjDgwQAAAGBRAdIAyAPQCDAtAdAAQAgAAABhBIgDhBIjnAAIgXAAQgZgEgUgGIgWAtg");
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt2();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D5AE81").s().p("AhJAgIgOg/QAggKAyggIAXgRQAIAOAZAhQAlAwAAAtQAAAXhYAIQgoAEgWAGIgLg7g");
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#497C59").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5A996E").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_5.setTransform(85.2,58);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_6.setTransform(86,143.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_7.setTransform(67.5,124.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_8.setTransform(9.8,86.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_11.setTransform(25.6,90.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_12.setTransform(84.4,77.7);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-8.8,168.9,164.3);


(lib.FrankShirt = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BC9A72").s().p("AhGBeIAAgBIACgHQAAgXAJgRQAPgaAMgZQAMgaAegQIADgQIAAgvIAAgRIA6AAIgGAhQgHAfgHAPQADAFgRAYQgKAXgDAnIgDAPIgDAAIgCATIgEAeIgBAJIAAACIgBADIgBAHQhPgdAAgFg");
	this.shape.setTransform(12.4,77.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D5AE81").s().p("AhHB0IgygSIABgHIABgDIAAgBIABgKIAEgeIACgSIADAAIADgQQADglAKgZQARgYgDgFQAHgPAHgfIAGghICVgCIAGAoQAEAbAOBNIAEAUIAEAWIAAAAQAAAKgCAGIAAABIACAxQgBBDggAAQgdAAiDgsg");
	this.shape_1.setTransform(25.6,80.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgsDFIAAgOQAAgdADgRIgxgOIgggJQgtgOgTgQQgWgUAAggQAAgHALgcQALgcACgBIAHgLIACgDIAEgIQAVgjAQgjIADgbQACgSAHgbIAsANIAAAAIAAAQIAAAwIgCAPQghARgMAZQgMAbgPAaQgJAQAAAXIgCAGIAAABQAAAGBRAcIAyASQCDAsAdAAQAgAAABhDIgCgxIAAgBQACgGAAgKIAAAAIgEgUIgEgWQgOhNgEgbIgGgoIAjgBIADAWQACANAMA2QAHAfABAaIATArIAAABQACAUgCAVQAAAcgHAkQgMA8gZAOIhEAAQgsgSg1gQQABAOAAAUIAAAPg");
	this.shape_2.setTransform(21.7,83.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_3.setTransform(25.6,90.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_4.setTransform(9.8,86.7);

	// Layer 1
	this.instance = new lib.shirt();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#D5AE81").s().p("AhJAUQgmh3gCgNQASgHALgJIAdgaIAZApQAaApAXAgQAGAJARAJIASAKQAcARAPAsQAMAigBAfQAAAXhZAIQgmAEgXAGQgDgXgihwg");
	this.shape_5.setTransform(153,71.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EACFB7").s().p("AgdCVQAEgZAAgHIAAgBQgRALgEAAIgNAAQgOAMgGAAQgNAAgFgLIgBAAQgOAAgDgNIgBgJQADgIACgNQADgnAKgZQARgXgDgEQAHgQAHgfQAKgtAAgYIgBgJIABgHIAAgHIACgDQAKgMAuAAQAcgBAOAHQACgKAJgDQALgGALAMIADACIAAAEQAEAUABAOQACAXAIAzQAEAbAOBLIAEAVIAEAWIAAAAQAAAKgCAHQgFAPgOAAQgNAAgEgJIgCAAQgEAAgDgDQgJAMgWAAQgFAAgEgCQgIASgMAAIgBAAQgHAYgJAAQgVAAgBgVg");
	this.shape_6.setTransform(26.1,73.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_7.setTransform(86,143.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_8.setTransform(67.5,124.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CCB59F").s().p("AhICEIACgHQAAgXAJgSQAPgaAMgbQAMgZAfgPIACgPIAAgwQAAgegBgLIADgVQAAgJgDgEQALgLASgFQAegJAEAYIAAAGIAAAHIAAAKQAAAYgKAtQgHAfgHAQQADAEgRAXQgJAYgEAnQgCANgCAJIgDAGQgCAUgFAeIgBAIQhPgdAAgGg");
	this.shape_9.setTransform(12.6,74);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_10.setTransform(86.9,128);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_11.setTransform(94.8,140.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EAD0B7").s().p("AhHASIgygSIABgFQAFgeACgUIACgGIABAIQADAOAOAAIABAAQAFAKANAAQAGAAAOgMIANABQAEgBARgKIAAAAQAAAIgEAZQABATAVAAQAJAAAHgWIABAAQAMAAAIgSQAEABAFAAQAWAAAJgMQADADAEAAIACAAQAEAJANAAQAOAAAFgPIACAyQgBBBggAAQgdAAiDgsg");
	this.shape_12.setTransform(25.6,90.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AkAImQjFghg5gUQg7gVgBglQg1kaABg+IAAgMIgQgGQgFADgJAAQgTAAgEggIAAgUIAAgOQAAgbADgRIgxgNIgggKQgsgOgUgQQgWgUAAggQAAgHALgeQALgcACgBIAHgLIACgDIAEgIQAVgiAQgkIADgbQACgVAKgiQgBgMAHgHQAIgSANgEQAKgDADAIQADAEAAAJIgDAVQABALAAAeIAAAwIgCAPQghARgMAZQgMAbgPAaQgJASAAAXIgCAHQAAAGBRAcIAyASQCFAsAdAAQAhAAAAhDIgCgxQACgHAAgKIAAAAIgEgWIgEgWQgOhMgEgcQgIgygCgYQgBgOgDgUIgBgDIgCgDQACgZARAQQAUARABATIAJBEQACANAMA2QAHAgABAZIATAtIAAABQACAUgCAVQAAAcgHAkQgMA7gZANIhDAAQgsgQg3gQQABAOAAASIAAAPIAAAHIAEAAQAMAACeAgIAYAFQCUAdBAAFIGzgPQBIABBIgOQA3gKBAgVQAjgLAogLQAegJAggLQgSgygLhDQgRh1gMg1QgDgOgfhAIAKhyIAPAiIASArQADANAmB6QAhBvAEAXQAWgGApgDQBZgIAAgYQAAgfgLghQgPgtgcgSIgSgKQgRgJgHgKQgZgggagoIgYgpIgeAZQgKAKgTAGQAQgLAJgvQAJgrAUgHQgBgUgIgYIAxAAQAGAVgBAPIACAAIADAFQADADAAALQAAAJgPATQACACAcA3QAcA1AGACQA1AUAjBFQAeA4AAAtQAAAkgFAGQgLARg6APIh1AAQAGArAAAjQAAAwgXAOIgDgHQgbAShBAaIgMAbQgTArgCAjQgBATAFAoQAAAQgGANQAjAHAdAOQBRAoAABMQAAASgSANQgdAWhBgCIk7AAQguABgZgXQgTgRAAgTQgIgWgMg3IgDgMIgBAEQgTBegTAmIACAEIAAABIABAAIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoDG9QALANBZAPQCRAZASAEIBiAAIB1AAQASgJAYgSIhbgYQh6gfheAAQgTAAhJAGIgDAAQhTAIgPAFIgZAAIAFAGgAgRGgQAeAMAWAMQAFgkAUhtQARheAHgzIkuAFQhggFhsgTQgpgHgqgKQgzgLghgJIAEAQQAlCJAACRQAUgFBYgIIA3gEQAzgEAdAAQCmAAB6AtgAGLE0Qg/AHhbAtQgeAPgiATQAHAFAAADQAEAEAsAOIEwAAIAJgCIADAAQATgDAbgDQg8hqhuAAQgOAAgPACgAEYCTIhAABIhWACIAAAJQAAAUgDAaQAJATAHAeQAMAvAEAuQATgTAhgRQAQgIARgGQBAgYBQgCQAogCAkAEIgDgMQgCgLAAgiQABhNAFgcQhNAVhsAPgAI2mNIAAAAg");
	this.shape_13.setTransform(84.8,100.4);

	this.addChild(this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.instance,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.8,0,168.1,155.5);


(lib.Frank_ShirtandTie = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A4D30").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#975F3C").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgOEQQgKhDAAgbIABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgPQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgvQAAgmAGgtIAtADQgGAnAAAgQAAAZAGAKQAVAlA7gFIEXADIAAgoIAAgTQAaAUASAaIgCAiIACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAiQgMA8gZAOIhEAAQgsgRg1gRQACAPAAATIAAAWIAEAAQALAACdAhIAYAFIBUAPQgFALgGAKQgKAQgPANIgmgGQgpgIgrgJQgygLgigKIAFARQALAoAIAoIgMAAIghgBgAjDgwQAAAGBRAdIAyAPQCDAtAdAAQAgAAABhBIgDhBIjnAAIgXAAQgZgEgUgGIgWAtg");
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt2();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#975F3C").s().p("AhJAgIgOg/QAggKAyggIAXgRQAIAOAZAhQAlAwAAAtQAAAXhYAIQgoAEgWAGIgLg7g");
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#497C59").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5A996E").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_5.setTransform(85.2,58);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_6.setTransform(86,143.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_7.setTransform(67.5,124.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_8.setTransform(9.8,86.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_11.setTransform(25.6,90.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_12.setTransform(84.4,77.7);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-8.8,168.9,164.3);


(lib.Frank_Shirt = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A4D30").s().p("AhGBeIAAgBIACgHQAAgXAJgRQAPgaAMgZQAMgaAegQIADgQIAAgvIAAgRIA6AAIgGAhQgHAfgHAPQADAFgRAYQgKAXgDAnIgDAPIgDAAIgCATIgEAeIgBAJIAAACIgBADIgBAHQhPgdAAgFg");
	this.shape.setTransform(12.4,77.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#975F3C").s().p("AhHB0IgygSIABgHIABgDIAAgBIABgKIAEgeIACgSIADAAIADgQQADglAKgZQARgYgDgFQAHgPAHgfIAGghICVgCIAGAoQAEAbAOBNIAEAUIAEAWIAAAAQAAAKgCAGIAAABIACAxQgBBDggAAQgdAAiDgsg");
	this.shape_1.setTransform(25.6,80.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgsDFIAAgOQAAgdADgRIgxgOIgggJQgtgOgTgQQgWgUAAggQAAgHALgcQALgcACgBIAHgLIACgDIAEgIQAVgjAQgjIADgbQACgSAHgbIAsANIAAAAIAAAQIAAAwIgCAPQghARgMAZQgMAbgPAaQgJAQAAAXIgCAGIAAABQAAAGBRAcIAyASQCDAsAdAAQAgAAABhDIgCgxIAAgBQACgGAAgKIAAAAIgEgUIgEgWQgOhNgEgbIgGgoIAjgBIADAWQACANAMA2QAHAfABAaIATArIAAABQACAUgCAVQAAAcgHAkQgMA8gZAOIhEAAQgsgSg1gQQABAOAAAUIAAAPg");
	this.shape_2.setTransform(21.7,83.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_3.setTransform(25.6,90.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_4.setTransform(9.8,86.7);

	// Layer 1
	this.instance = new lib.shirt();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7A4D30").s().p("AhICEIACgHQAAgXAJgSQAPgaAMgbQAMgZAfgPIACgPIAAgwQAAgegBgLIADgVQAAgJgDgEQALgLASgFQAegJAEAYIAAAGIAAAHIAAAKQAAAYgKAtQgHAfgHAQQADAEgRAXQgJAYgEAnQgCANgCAJIgDAGQgCAUgFAeIgBAIQhPgdAAgGg");
	this.shape_5.setTransform(12.6,74);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#975F3C").s().p("AqWCWQAEgZAAgIIAAAAQgRALgEAAIgNgBQgOAMgGABQgNAAgFgLIgBAAQgOAAgDgOIgBgIQADgIACgNQADgnAKgZQARgXgDgEQAHgQAHgfQAKgtAAgYIgBgJIABgHIAAgHIACgDQAKgMAugBQAeAAAOAGQACgJAJgDQALgGALAMIADACIAAAEQAEAUABAOQACAXAIAzQAEAbAOBKIAEAWIAEAWIAAAAQAAAKgCAHQgFAOgOABQgNgBgEgIIgCAAQgEAAgDgEQgJAMgWABQgFgBgEgBQgIASgMAAIgBAAQgHAYgLAAQgVAAgBgVgAIwAEQglh3gDgNQASgHALgJIAegaIAYApQAaAoAZAhQAHAJAQAJIATAKQAcATAPAqQALAiAAAeQAAAYhaAIQgoADgWAHQgEgYgihvg");
	this.shape_6.setTransform(89.4,73.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_7.setTransform(86,143.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_8.setTransform(67.5,124.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_9.setTransform(86.9,128);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_10.setTransform(94.8,140.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#EAD0B7").s().p("AhHASIgygSIABgFQAFgeACgUIACgGIABAIQADAOAOAAIABAAQAFAKANAAQAGAAAOgMIANABQAEgBARgKIAAAAQAAAIgEAZQABATAVAAQAJAAAHgWIABAAQAMAAAIgSQAEABAFAAQAWAAAJgMQADADAEAAIACAAQAEAJANAAQAOAAAFgPIACAyQgBBBggAAQgdAAiDgsg");
	this.shape_11.setTransform(25.6,90.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AkAImQjFghg5gUQg7gVgBglQg1kaABg+IAAgMIgQgGQgFADgJAAQgTAAgEggIAAgUIAAgOQAAgbADgRIgxgNIgggKQgsgOgUgQQgWgUAAggQAAgHALgeQALgcACgBIAHgLIACgDIAEgIQAVgiAQgkIADgbQACgVAKgiQgBgMAHgHQAIgSANgEQAKgDADAIQADAEAAAJIgDAVQABALAAAeIAAAwIgCAPQghARgMAZQgMAbgPAaQgJASAAAXIgCAHQAAAGBRAcIAyASQCFAsAdAAQAhAAAAhDIgCgxQACgHAAgKIAAAAIgEgWIgEgWQgOhMgEgcQgIgygCgYQgBgOgDgUIgBgDIgCgDQACgZARAQQAUARABATIAJBEQACANAMA2QAHAgABAZIATAtIAAABQACAUgCAVQAAAcgHAkQgMA7gZANIhDAAQgsgQg3gQQABAOAAASIAAAPIAAAHIAEAAQAMAACeAgIAYAFQCUAdBAAFIGzgPQBIABBIgOQA3gKBAgVQAjgLAogLQAegJAggLQgSgygLhDQgRh1gMg1QgDgOgfhAIAKhyIAPAiIASArQADANAmB6QAhBvAEAXQAWgGApgDQBZgIAAgYQAAgfgLghQgPgtgcgSIgSgKQgRgJgHgKQgZgggagoIgYgpIgeAZQgKAKgTAGQAQgLAJgvQAJgrAUgHQgBgUgIgYIAxAAQAGAVgBAPIACAAIADAFQADADAAALQAAAJgPATQACACAcA3QAcA1AGACQA1AUAjBFQAeA4AAAtQAAAkgFAGQgLARg6APIh1AAQAGArAAAjQAAAwgXAOIgDgHQgbAShBAaIgMAbQgTArgCAjQgBATAFAoQAAAQgGANQAjAHAdAOQBRAoAABMQAAASgSANQgdAWhBgCIk7AAQguABgZgXQgTgRAAgTQgIgWgMg3IgDgMIgBAEQgTBegTAmIACAEIAAABIABAAIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoDG9QALANBZAPQCRAZASAEIBiAAIB1AAQASgJAYgSIhbgYQh6gfheAAQgTAAhJAGIgDAAQhTAIgPAFIgZAAIAFAGgAgRGgQAeAMAWAMQAFgkAUhtQARheAHgzIkuAFQhggFhsgTQgpgHgqgKQgzgLghgJIAEAQQAlCJAACRQAUgFBYgIIA3gEQAzgEAdAAQCmAAB6AtgAGLE0Qg/AHhbAtQgeAPgiATQAHAFAAADQAEAEAsAOIEwAAIAJgCIADAAQATgDAbgDQg8hqhuAAQgOAAgPACgAEYCTIhAABIhWACIAAAJQAAAUgDAaQAJATAHAeQAMAvAEAuQATgTAhgRQAQgIARgGQBAgYBQgCQAogCAkAEIgDgMQgCgLAAgiQABhNAFgcQhNAVhsAPgAI2mNIAAAAg");
	this.shape_12.setTransform(84.8,100.4);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.instance,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.8,0,168.1,155.5);


(lib.Frank_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 3
	this.instance = new lib.HardHat();
	this.instance.setTransform(91.9,33.6,1.89,1.89,0,0,0,43.1,29.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).to({_off:true},1).wait(2));

	// Layer 2
	this.instance_1 = new lib.FrankHead();
	this.instance_1.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 1
	this.instance_2 = new lib.Male_Frank_Front();
	this.instance_2.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_3 = new lib.Frank_Shirt();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.YardSteve_03();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.MotifFrank();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_6 = new lib.Frank_ShirtandTie();
	this.instance_6.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.FemaleTrousersPeppa = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D5AE81").s().p("Ag0BeQgZgQgNgKQgMgLgKgKIgbgdQgLgMgVgeQgagoAAgMQAAgJAIgGQAIgIANAJQAGAEACAJIADAIQANgEAaAEQAFABBAAQICAAAQAEgEAYgEIAZgFQAHAAAEAEIAKgSQAPggAQABQAOABAAARQAAAOhWBaQgTASgyAnIgfAYIgRANQgFAFgJAAQgFAAgcgRg");
	this.shape.setTransform(84.4,14.1);

	// Layer 3
	this.instance = new lib.GirlOutfit01();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 2
	this.instance_1 = new lib.GirlOutfit01();
	this.instance_1.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BA986F").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQArhSAJhtQACgaAShVIAUhdIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape_1.setTransform(12,71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D5AE81").s().p("Ap1EyIgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAH/CuQgUgtAMg/QACgCgKgPQgLgQgEAAIgBABIAAgBIgDgHQgCgEAKgWQAJgYgHgSIgPgmQgFgMAAgDQAIgjACgUQADgegDgcQgHhCgGgXIAWgJQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKALAkAiQA5A3AABEQAAAsg4APQgiAJgFADQgfADgDACQgygZgUgsg");
	this.shape_2.setTransform(82.3,72);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ao5FtQgYgBgVgDQhbgWgYgTQgrgjgMhMQgHgxAIgcQAGgSAUgcQAzhHAOhtQAIg3AKguIAWhlIAAgVIAEgFQAIgQADgCQAVABAKgFIA2ATIAjAPQALAFALALIApApIABACIAPgCQAVAKgwCwIgFALQgYA3gEAqQgEAqAQAdQAJATAUAlQAJAPALAiQAQAxgGAVQgFAUgYAVQgQAOgLAFIgJAFQgeANglAAIgKAAgAp7jOQgSBVgCAaQgJBtgtBSQgLAUgRAcQgLAUADANQAPBBAqAoQAVAUAVAKIAOAFQAKADALABIAQABQBTAGAggeIAHgHQAOgSgFgVIAAgCQgEgLghg+IgJgRQgagygCgIIAAgBQgDgVAXhtQAYhtAZhAIAIgUIgMgJQgtgigbgKIgTgIQgOgGgMgBQgEgBgJgDIgMgFIgUBdgAJ3EpIg5gFQhwhZAqhnIggAJQgEgZgNgWIAVgFQgEgGAHgSQAHgUgEgMIgLgbQgFgLAAgNIgNAGQgIAEgGAAQgTAAgDgDIgCgEQgCgCAAgLQAPgNAOgWQAdgrgGg6QgFg2gggzIAegVIAPgKIALAUQAOgJAYgMQAOgHALgDIgIgSQAUgBAUgCIABASIgBAIIABAEIgCAGIAAACIgDACIgBABIgKAFQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAiQAOASAgAYQAaAUAKAVQAOAgAAA/QAAAhgeAgQglAng9AFIgDAAIgbgBgAIRBBQAKAPgCACQgMA/AUAtQAUAsAyAZQADgCAfgDQAFgDAigJQA4gPAAgsQAAhEg5g3QgkgigKgNQgYgdgKgmQgCgNgEgNQgHgWgYg2QgrhmgIggIgWAJQAGAXAHBCQADAcgDAeQgCAUgIAjQAAADAFAMIAPAmQAHASgJAWQgKAYACAEIADAHIAAABIABgBQAEAAALAQg");
	this.shape_3.setTransform(78.9,70.4);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.instance_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,2.4,157.8,163.2);


(lib.FemaleTrousersJane = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#975F3C").s().p("Ag0BeQgZgQgNgKQgMgLgKgKIgbgdQgLgMgVgeQgagoAAgMQAAgJAIgGQAIgIANAJQAGAEACAJIADAIQANgEAaAEQAFABBAAQICAAAQAEgEAYgEIAZgFQAHAAAEAEIAKgSQAPggAQABQAOABAAARQAAAOhWBaQgTASgyAnIgfAYIgRANQgFAFgJAAQgFAAgcgRg");
	this.shape.setTransform(84.4,14.1);

	// Layer 3
	this.instance = new lib.GirlOutfit01();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 2
	this.instance_1 = new lib.GirlOutfit01();
	this.instance_1.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7A4D30").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQArhSAJhtQACgaAShVIAUhdIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape_1.setTransform(12,71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#975F3C").s().p("Ap1EyIgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAH/CuQgUgtAMg/QACgCgKgPQgLgQgEAAIgBABIAAgBIgDgHQgCgEAKgWQAJgYgHgSIgPgmQgFgMAAgDQAIgjACgUQADgegDgcQgHhCgGgXIAWgJQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKALAkAiQA5A3AABEQAAAsg4APQgiAJgFADQgfADgDACQgygZgUgsg");
	this.shape_2.setTransform(82.3,72);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ao5FtQgYgBgVgDQhbgWgYgTQgrgjgMhMQgHgxAIgcQAGgSAUgcQAzhHAOhtQAIg3AKguIAWhlIAAgVIAEgFQAIgQADgCQAVABAKgFIA2ATIAjAPQALAFALALIApApIABACIAPgCQAVAKgwCwIgFALQgYA3gEAqQgEAqAQAdQAJATAUAlQAJAPALAiQAQAxgGAVQgFAUgYAVQgQAOgLAFIgJAFQgeANglAAIgKAAgAp7jOQgSBVgCAaQgJBtgtBSQgLAUgRAcQgLAUADANQAPBBAqAoQAVAUAVAKIAOAFQAKADALABIAQABQBTAGAggeIAHgHQAOgSgFgVIAAgCQgEgLghg+IgJgRQgagygCgIIAAgBQgDgVAXhtQAYhtAZhAIAIgUIgMgJQgtgigbgKIgTgIQgOgGgMgBQgEgBgJgDIgMgFIgUBdgAJ3EpIg5gFQhwhZAqhnIggAJQgEgZgNgWIAVgFQgEgGAHgSQAHgUgEgMIgLgbQgFgLAAgNIgNAGQgIAEgGAAQgTAAgDgDIgCgEQgCgCAAgLQAPgNAOgWQAdgrgGg6QgFg2gggzIAegVIAPgKIALAUQAOgJAYgMQAOgHALgDIgIgSQAUgBAUgCIABASIgBAIIABAEIgCAGIAAACIgDACIgBABIgKAFQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAiQAOASAgAYQAaAUAKAVQAOAgAAA/QAAAhgeAgQglAng9AFIgDAAIgbgBgAIRBBQAKAPgCACQgMA/AUAtQAUAsAyAZQADgCAfgDQAFgDAigJQA4gPAAgsQAAhEg5g3QgkgigKgNQgYgdgKgmQgCgNgEgNQgHgWgYg2QgrhmgIggIgWAJQAGAXAHBCQADAcgDAeQgCAUgIAjQAAADAFAMIAPAmQAHASgJAWQgKAYACAEIADAHIAAABIABgBQAEAAALAQg");
	this.shape_3.setTransform(78.9,70.4);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.instance_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,2.4,157.8,163.2);


(lib.FemaleTrousers = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EACFB7").s().p("Ag0BeQgZgQgNgKQgMgLgKgKIgbgdQgLgMgVgeQgagoAAgMQAAgJAIgGQAIgIANAJQAGAEACAJIADAIQANgEAaAEQAFABBAAQICAAAQAEgEAYgEIAZgFQAHAAAEAEIAKgSQAPggAQABQAOABAAARQAAAOhWBaQgTASgyAnIgfAYIgRANQgFAFgJAAQgFAAgcgRg");
	this.shape.setTransform(84.4,14.1);

	// Layer 3
	this.instance = new lib.GirlOutfit01();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 2
	this.instance_1 = new lib.GirlOutfit01();
	this.instance_1.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EACFB7").s().p("Ap1EyIgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAH/CuQgUgtAMg/QACgCgKgPQgLgQgEAAIgBABIAAgBIgDgHQgCgEAKgWQAJgYgHgSIgPgmQgFgMAAgDQAIgjACgUQADgegDgcQgHhCgGgXIAWgJQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKALAkAiQA5A3AABEQAAAsg4APQgiAJgFADQgfADgDACQgygZgUgsg");
	this.shape_1.setTransform(82.3,72);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCB59F").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQArhSAJhtQACgaAShVIAUhdIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape_2.setTransform(12,71);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ao5FtQgYgBgVgDQhbgWgYgTQgrgjgMhMQgHgxAIgcQAGgSAUgcQAzhHAOhtQAIg3AKguIAWhlIAAgVIAEgFQAIgQADgCQAVABAKgFIA2ATIAjAPQALAFALALIApApIABACIAPgCQAVAKgwCwIgFALQgYA3gEAqQgEAqAQAdQAJATAUAlQAJAPALAiQAQAxgGAVQgFAUgYAVQgQAOgLAFIgJAFQgeANglAAIgKAAgAp7jOQgSBVgCAaQgJBtgtBSQgLAUgRAcQgLAUADANQAPBBAqAoQAVAUAVAKIAOAFQAKADALABIAQABQBTAGAggeIAHgHQAOgSgFgVIAAgCQgEgLghg+IgJgRQgagygCgIIAAgBQgDgVAXhtQAYhtAZhAIAIgUIgMgJQgtgigbgKIgTgIQgOgGgMgBQgEgBgJgDIgMgFIgUBdgAJ3EpIg5gFQhwhZAqhnIggAJQgEgZgNgWIAVgFQgEgGAHgSQAHgUgEgMIgLgbQgFgLAAgNIgNAGQgIAEgGAAQgTAAgDgDIgCgEQgCgCAAgLQAPgNAOgWQAdgrgGg6QgFg2gggzIAegVIAPgKIALAUQAOgJAYgMQAOgHALgDIgIgSQAUgBAUgCIABASIgBAIIABAEIgCAGIAAACIgDACIgBABIgKAFQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAiQAOASAgAYQAaAUAKAVQAOAgAAA/QAAAhgeAgQglAng9AFIgDAAIgbgBgAIRBBQAKAPgCACQgMA/AUAtQAUAsAyAZQADgCAfgDQAFgDAigJQA4gPAAgsQAAhEg5g3QgkgigKgNQgYgdgKgmQgCgNgEgNQgHgWgYg2QgrhmgIggIgWAJQAGAXAHBCQADAcgDAeQgCAUgIAjQAAADAFAMIAPAmQAHASgJAWQgKAYACAEIADAHIAAABIABgBQAEAAALAQg");
	this.shape_3.setTransform(78.9,70.4);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.instance_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,2.4,157.8,163.2);


(lib.FemaleMotifPeppa = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.Girl03();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BA986F").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQAcgyAMg8QAogfAKh+QAEg0gChDIAAgHIAAgCIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape.setTransform(15,71);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D5AE81").s().p("AouE4IgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAHoA6QACgBgGgRQgGgSgEgBIgBAAIAAgBIgBgHIAAgcQABgYgCgTIgFgpQgCgNABgCQARggAHgUQALgcAEgbQAIgxACgZIAAgQIAMgBIANADIAAAMQACAmAMBYQAJA7ABAXQAAANgBAOQAAAnAQAhQAGAQAZAqQApBEgRBCQgMAqg6ABQgjgBgGABIgJADQgJh8gQhCg");
	this.shape_1.setTransform(78.2,71.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AnzF6QgZgBgVgDQhbgWgYgTQgrgjgLhMQgIgxAJgcQAGgSAUgcQAdgoARg2QAGACAHAAQARAAANgIQgOA8gcAyQgKAUgRAcQgLAUADANQAPBBAqAoQAUAUAWAKIAOAFQAKADALABIAPABQBTAGAggeIAHgHQAPgSgFgVIgBgCQgDgLghg+IgJgRQgbgygCgIIAAgBQgDgVAYhtQAXhtAahAIAIgUIgMgJQgugigbgKIgTgIQgOgGgLgBQgFgBgIgDIgMgFIAAACIgDgzIAJADIA5AUIAjAPQAKAFALALIApApIABACIAQgCIAJgPQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAlQgGASgGAJQAuAJAyAIIAGA1QhYgPghgVQgHAQgPAgQAJATAUAlQAIAPALAiQAQAxgFAVQgFAUgYAVQgRAOgKAFIgKAFQgdANglAAIgKAAgAmRgrQgYA1gDAsQALgNAIgEIgDgFQgBgCAAgNQAAgIAKgiIAJggIgHAOgAIrE6Ig4ABIgBAAIgOjhQgRACgZgCIgFgyIAfABQgHgagCggIgDgfQgCgLADgNIgOACIgKABQABgXAEgWIAIgIQAngiAKg6QAEgYgCgZQAOgPARgPIAEgBIADABIASgXIAJgEQANgHALgDIgGgPIAKgKIAXgWQAGAWAAAWIAAAIIAAAEIgBAGIgBACIgCACIgBABIgLAFQgIADACAFIAHASIAMADQAGA3AKA4QAIAyABA0QACBCARAqQAJAVAZAgQAUAaAEAXQAFAigQA9QgJAgglAXQghAUgoAAQgQAAgRgDgAIFA/QAFARgCABQARBCAJB8IAJgDQAGgBAiABQA7gBALgqQAShCgqhEQgZgqgGgQQgQghAAgnQACgOgBgNQgBgXgJg7QgMhYgCgmIAAgMIgNgDIgLABIgBAQQgCAZgHAxQgEAbgMAcQgHAUgQAgQgBACACANIAFApQACATgBAWIgBAeIABAHIABABIABAAQAEABAGASg");
	this.shape_2.setTransform(74.9,69.1);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,4.2,143.9,163.8);


(lib.FemaleMotifJane = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.Girl03();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A4D30").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQAcgyAMg8QAogfAKh+QAEg0gChDIAAgHIAAgCIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape.setTransform(15,71);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#975F3C").s().p("AouE4IgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAHoA6QACgBgGgRQgGgSgEgBIgBAAIAAgBIgBgHIAAgcQABgYgCgTIgFgpQgCgNABgCQARggAHgUQALgcAEgbQAIgxACgZIAAgQIAMgBIANADIAAAMQACAmAMBYQAJA7ABAXQAAANgBAOQAAAnAQAhQAGAQAZAqQApBEgRBCQgMAqg6ABQgjgBgGABIgJADQgJh8gQhCg");
	this.shape_1.setTransform(78.2,71.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AnzF6QgZgBgVgDQhbgWgYgTQgrgjgLhMQgIgxAJgcQAGgSAUgcQAdgoARg2QAGACAHAAQARAAANgIQgOA8gcAyQgKAUgRAcQgLAUADANQAPBBAqAoQAUAUAWAKIAOAFQAKADALABIAPABQBTAGAggeIAHgHQAPgSgFgVIgBgCQgDgLghg+IgJgRQgbgygCgIIAAgBQgDgVAYhtQAXhtAahAIAIgUIgMgJQgugigbgKIgTgIQgOgGgLgBQgFgBgIgDIgMgFIAAACIgDgzIAJADIA5AUIAjAPQAKAFALALIApApIABACIAQgCIAJgPQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAlQgGASgGAJQAuAJAyAIIAGA1QhYgPghgVQgHAQgPAgQAJATAUAlQAIAPALAiQAQAxgFAVQgFAUgYAVQgRAOgKAFIgKAFQgdANglAAIgKAAgAmRgrQgYA1gDAsQALgNAIgEIgDgFQgBgCAAgNQAAgIAKgiIAJggIgHAOgAIrE6Ig4ABIgBAAIgOjhQgRACgZgCIgFgyIAfABQgHgagCggIgDgfQgCgLADgNIgOACIgKABQABgXAEgWIAIgIQAngiAKg6QAEgYgCgZQAOgPARgPIAEgBIADABIASgXIAJgEQANgHALgDIgGgPIAKgKIAXgWQAGAWAAAWIAAAIIAAAEIgBAGIgBACIgCACIgBABIgLAFQgIADACAFIAHASIAMADQAGA3AKA4QAIAyABA0QACBCARAqQAJAVAZAgQAUAaAEAXQAFAigQA9QgJAgglAXQghAUgoAAQgQAAgRgDgAIFA/QAFARgCABQARBCAJB8IAJgDQAGgBAiABQA7gBALgqQAShCgqhEQgZgqgGgQQgQghAAgnQACgOgBgNQgBgXgJg7QgMhYgCgmIAAgMIgNgDIgLABIgBAQQgCAZgHAxQgEAbgMAcQgHAUgQAgQgBACACANIAFApQACATgBAWIgBAeIABAHIABABIABAAQAEABAGASg");
	this.shape_2.setTransform(74.9,69.1);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,4.2,143.9,163.8);


(lib.FemaleMotif = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.Girl03();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCB59F").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQAcgyAMg8QAogfAKh+QAEg0gChDIAAgHIAAgCIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape.setTransform(15,71);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EACFB7").s().p("AouE4IgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAHoA6QACgBgGgRQgGgSgEgBIgBAAIAAgBIgBgHIAAgcQABgYgCgTIgFgpQgCgNABgCQARggAHgUQALgcAEgbQAIgxACgZIAAgQIAMgBIANADIAAAMQACAmAMBYQAJA7ABAXQAAANgBAOQAAAnAQAhQAGAQAZAqQApBEgRBCQgMAqg6ABQgjgBgGABIgJADQgJh8gQhCg");
	this.shape_1.setTransform(78.2,71.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AnzF6QgZgBgVgDQhbgWgYgTQgrgjgLhMQgIgxAJgcQAGgSAUgcQAdgoARg2QAGACAHAAQARAAANgIQgOA8gcAyQgKAUgRAcQgLAUADANQAPBBAqAoQAUAUAWAKIAOAFQAKADALABIAPABQBTAGAggeIAHgHQAPgSgFgVIgBgCQgDgLghg+IgJgRQgbgygCgIIAAgBQgDgVAYhtQAXhtAahAIAIgUIgMgJQgugigbgKIgTgIQgOgGgLgBQgFgBgIgDIgMgFIAAACIgDgzIAJADIA5AUIAjAPQAKAFALALIApApIABACIAQgCIAJgPQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAlQgGASgGAJQAuAJAyAIIAGA1QhYgPghgVQgHAQgPAgQAJATAUAlQAIAPALAiQAQAxgFAVQgFAUgYAVQgRAOgKAFIgKAFQgdANglAAIgKAAgAmRgrQgYA1gDAsQALgNAIgEIgDgFQgBgCAAgNQAAgIAKgiIAJggIgHAOgAIrE6Ig4ABIgBAAIgOjhQgRACgZgCIgFgyIAfABQgHgagCggIgDgfQgCgLADgNIgOACIgKABQABgXAEgWIAIgIQAngiAKg6QAEgYgCgZQAOgPARgPIAEgBIADABIASgXIAJgEQANgHALgDIgGgPIAKgKIAXgWQAGAWAAAWIAAAIIAAAEIgBAGIgBACIgCACIgBABIgLAFQgIADACAFIAHASIAMADQAGA3AKA4QAIAyABA0QACBCARAqQAJAVAZAgQAUAaAEAXQAFAigQA9QgJAgglAXQghAUgoAAQgQAAgRgDgAIFA/QAFARgCABQARBCAJB8IAJgDQAGgBAiABQA7gBALgqQAShCgqhEQgZgqgGgQQgQghAAgnQACgOgBgNQgBgXgJg7QgMhYgCgmIAAgMIgNgDIgLABIgBAQQgCAZgHAxQgEAbgMAcQgHAUgQAgQgBACACANIAFApQACATgBAWIgBAeIABAHIABABIABAAQAEABAGASg");
	this.shape_2.setTransform(74.9,69.1);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,4.2,143.9,163.8);


(lib.FemaleHoodiePeppa = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC6600").s().p("AhwAiQgZghAAgaQAAgYACgIQAEgVARgJQClgCA5AiQAeASAAAhQAAATggAhQgmApgxAAQhZAAgqg3g");
	this.shape.setTransform(86.4,21.8);

	// Layer 2
	this.instance = new lib.Girl02();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BA986F").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQAcgyAMg8QAogfAKh+QAEg0gChDIAAgHIAAgCIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape_1.setTransform(15,71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D5AE81").s().p("AouE4IgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAHoA6QACgBgGgRQgGgSgEgBIgBAAIAAgBIgBgHIAAgcQABgYgCgTIgFgpQgCgNABgCQARggAHgUQALgcAEgbQAIgxACgZIAAgQIAMgBIANADIAAAMQACAmAMBYQAJA7ABAXQAAANgBAOQAAAnAQAhQAGAQAZAqQApBEgRBCQgMAqg6ABQgjgBgGABIgJADQgJh8gQhCg");
	this.shape_2.setTransform(78.2,71.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AnzF6QgZgBgVgDQhbgWgYgTQgrgjgLhMQgIgxAJgcQAGgSAUgcQAdgoARg2QAGACAHAAQARAAANgIQgOA8gcAyQgKAUgRAcQgLAUADANQAPBBAqAoQAUAUAWAKIAOAFQAKADALABIAPABQBTAGAggeIAHgHQAPgSgFgVIgBgCQgDgLghg+IgJgRQgbgygCgIIAAgBQgDgVAYhtQAXhtAahAIAIgUIgMgJQgugigbgKIgTgIQgOgGgLgBQgFgBgIgDIgMgFIAAACIgDgzIAJADIA5AUIAjAPQAKAFALALIApApIABACIAQgCIAJgPQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAlQgGASgGAJQAuAJAyAIIAGA1QhYgPghgVQgHAQgPAgQAJATAUAlQAIAPALAiQAQAxgFAVQgFAUgYAVQgRAOgKAFIgKAFQgdANglAAIgKAAgAmRgrQgYA1gDAsQALgNAIgEIgDgFQgBgCAAgNQAAgIAKgiIAJggIgHAOgAIrE6Ig4ABIgBAAIgOjhQgRACgZgCIgFgyIAfABQgHgagCggIgDgfQgCgLADgNIgOACIgKABQABgXAEgWIAIgIQAngiAKg6QAEgYgCgZQAOgPARgPIAEgBIADABIASgXIAJgEQANgHALgDIgGgPIAKgKIAXgWQAGAWAAAWIAAAIIAAAEIgBAGIgBACIgCACIgBABIgLAFQgIADACAFIAHASIAMADQAGA3AKA4QAIAyABA0QACBCARAqQAJAVAZAgQAUAaAEAXQAFAigQA9QgJAgglAXQghAUgoAAQgQAAgRgDgAIFA/QAFARgCABQARBCAJB8IAJgDQAGgBAiABQA7gBALgqQAShCgqhEQgZgqgGgQQgQghAAgnQACgOgBgNQgBgXgJg7QgMhYgCgmIAAgMIgNgDIgLABIgBAQQgCAZgHAxQgEAbgMAcQgHAUgQAgQgBACACANIAFApQACATgBAWIgBAeIABAHIABABIABAAQAEABAGASg");
	this.shape_3.setTransform(74.9,69.1);

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#BA986F").s().p("AC3COQgWgOgFgpQgDgPAAgzQAAghADgLQACgJAKgIQAcgYAdgsIgVD6gAixALQgYhggXgtQAugJAigCIBEEWIgIAAQhAgqgdhUg");
	this.shape_4.setTransform(72.4,141.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#D5AE81").s().p("ABWCTIAVj6QANgTAOgYQA/ABAaADIAoAFQgJAVgcB3QgTBUgyA8gAkGiIQAvgDAaAHIAkAPQAXAJAcAEIAAB7QgIATgEAuQgJAoglAQIgiACg");
	this.shape_5.setTransform(84.2,140.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAlB8QgHgdABgmIADgjIAAhNQAFgVAWgPIApgdQAHgHAQgjQAMgbAQgBIABAAQCggHAeAbQAKAJgEAPQgCAIgHANIgSBTQgMA3gOAkQgrBthoAdQgQAFgOABIgIAAQg8AAgPhFgACShqQgdAsgcAYQgKAIgDAJQgCALAAAhQAAAzACAPQAGApAWAOIAUAAIBIAAQAyg8AThTQAch4AIgVIgogFQgagCg/gCQgNAYgNATgAiSC1QgogBgXgMQgYgOgRAAQgKAAgDgCIgHgGQAAgegohvQgohtAAgmQAAgOAPgFQALgFBKgNIARAAQCbAFAoAtQAPARABAZQAAAOgCAYQAADmh4AAIgCAAgAjfiLQgjACguAJQAYAtAYBgQAcBUBAAqIAIAAIAigBQAmgRAIgoQAEguAIgTIAAh7QgbgEgXgJIglgOQgTgGgdAAIgYABg");
	this.shape_6.setTransform(80.4,140.9);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,-2.6,143.9,170.6);


(lib.FemaleHoodieJane = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC6600").s().p("AhwAiQgZghAAgaQAAgYACgIQAEgVARgJQClgCA5AiQAeASAAAhQAAATggAhQgmApgxAAQhZAAgqg3g");
	this.shape.setTransform(86.4,21.8);

	// Layer 2
	this.instance = new lib.Girl02();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7A4D30").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQAcgyAMg8QAogfAKh+QAEg0gChDIAAgHIAAgCIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape_1.setTransform(15,71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#975F3C").s().p("AouE4IgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAHoA6QACgBgGgRQgGgSgEgBIgBAAIAAgBIgBgHIAAgcQABgYgCgTIgFgpQgCgNABgCQARggAHgUQALgcAEgbQAIgxACgZIAAgQIAMgBIANADIAAAMQACAmAMBYQAJA7ABAXQAAANgBAOQAAAnAQAhQAGAQAZAqQApBEgRBCQgMAqg6ABQgjgBgGABIgJADQgJh8gQhCg");
	this.shape_2.setTransform(78.2,71.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AnzF6QgZgBgVgDQhbgWgYgTQgrgjgLhMQgIgxAJgcQAGgSAUgcQAdgoARg2QAGACAHAAQARAAANgIQgOA8gcAyQgKAUgRAcQgLAUADANQAPBBAqAoQAUAUAWAKIAOAFQAKADALABIAPABQBTAGAggeIAHgHQAPgSgFgVIgBgCQgDgLghg+IgJgRQgbgygCgIIAAgBQgDgVAYhtQAXhtAahAIAIgUIgMgJQgugigbgKIgTgIQgOgGgLgBQgFgBgIgDIgMgFIAAACIgDgzIAJADIA5AUIAjAPQAKAFALALIApApIABACIAQgCIAJgPQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAlQgGASgGAJQAuAJAyAIIAGA1QhYgPghgVQgHAQgPAgQAJATAUAlQAIAPALAiQAQAxgFAVQgFAUgYAVQgRAOgKAFIgKAFQgdANglAAIgKAAgAmRgrQgYA1gDAsQALgNAIgEIgDgFQgBgCAAgNQAAgIAKgiIAJggIgHAOgAIrE6Ig4ABIgBAAIgOjhQgRACgZgCIgFgyIAfABQgHgagCggIgDgfQgCgLADgNIgOACIgKABQABgXAEgWIAIgIQAngiAKg6QAEgYgCgZQAOgPARgPIAEgBIADABIASgXIAJgEQANgHALgDIgGgPIAKgKIAXgWQAGAWAAAWIAAAIIAAAEIgBAGIgBACIgCACIgBABIgLAFQgIADACAFIAHASIAMADQAGA3AKA4QAIAyABA0QACBCARAqQAJAVAZAgQAUAaAEAXQAFAigQA9QgJAgglAXQghAUgoAAQgQAAgRgDgAIFA/QAFARgCABQARBCAJB8IAJgDQAGgBAiABQA7gBALgqQAShCgqhEQgZgqgGgQQgQghAAgnQACgOgBgNQgBgXgJg7QgMhYgCgmIAAgMIgNgDIgLABIgBAQQgCAZgHAxQgEAbgMAcQgHAUgQAgQgBACACANIAFApQACATgBAWIgBAeIABAHIABABIABAAQAEABAGASg");
	this.shape_3.setTransform(74.9,69.1);

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7A4D30").s().p("AC3COQgWgOgFgpQgDgPAAgzQAAghADgLQACgJAKgIQAcgYAdgsIgVD6gAixALQgYhggXgtQAugJAigCIBEEWIgIAAQhAgqgdhUg");
	this.shape_4.setTransform(72.4,141.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#975F3C").s().p("ABWCTIAVj6QANgTAOgYQA/ABAaADIAoAFQgJAVgcB3QgTBUgyA8gAkGiIQAvgDAaAHIAkAPQAXAJAcAEIAAB7QgIATgEAuQgJAoglAQIgiACg");
	this.shape_5.setTransform(84.2,140.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAlB8QgHgdABgmIADgjIAAhNQAFgVAWgPIApgdQAHgHAQgjQAMgbAQgBIABAAQCggHAeAbQAKAJgEAPQgCAIgHANIgSBTQgMA3gOAkQgrBthoAdQgQAFgOABIgIAAQg8AAgPhFgACShqQgdAsgcAYQgKAIgDAJQgCALAAAhQAAAzACAPQAGApAWAOIAUAAIBIAAQAyg8AThTQAch4AIgVIgogFQgagCg/gCQgNAYgNATgAiSC1QgogBgXgMQgYgOgRAAQgKAAgDgCIgHgGQAAgegohvQgohtAAgmQAAgOAPgFQALgFBKgNIARAAQCbAFAoAtQAPARABAZQAAAOgCAYQAADmh4AAIgCAAgAjfiLQgjACguAJQAYAtAYBgQAcBUBAAqIAIAAIAigBQAmgRAIgoQAEguAIgTIAAh7QgbgEgXgJIglgOQgTgGgdAAIgYABg");
	this.shape_6.setTransform(80.4,140.9);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,-2.6,143.9,170.6);


(lib.FemaleHoodie = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC6600").s().p("AhwAiQgZghAAgaQAAgYACgIQAEgVARgJQClgCA5AiQAeASAAAhQAAATggAhQgmApgxAAQhZAAgqg3g");
	this.shape.setTransform(86.4,21.8);

	// Layer 2
	this.instance = new lib.Girl02();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCB59F").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQAcgyAMg8QAogfAKh+QAEg0gChDIAAgHIAAgCIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape_1.setTransform(15,71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EACFB7").s().p("AouE4IgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAHoA6QACgBgGgRQgGgSgEgBIgBAAIAAgBIgBgHIAAgcQABgYgCgTIgFgpQgCgNABgCQARggAHgUQALgcAEgbQAIgxACgZIAAgQIAMgBIANADIAAAMQACAmAMBYQAJA7ABAXQAAANgBAOQAAAnAQAhQAGAQAZAqQApBEgRBCQgMAqg6ABQgjgBgGABIgJADQgJh8gQhCg");
	this.shape_2.setTransform(78.2,71.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AnzF6QgZgBgVgDQhbgWgYgTQgrgjgLhMQgIgxAJgcQAGgSAUgcQAdgoARg2QAGACAHAAQARAAANgIQgOA8gcAyQgKAUgRAcQgLAUADANQAPBBAqAoQAUAUAWAKIAOAFQAKADALABIAPABQBTAGAggeIAHgHQAPgSgFgVIgBgCQgDgLghg+IgJgRQgbgygCgIIAAgBQgDgVAYhtQAXhtAahAIAIgUIgMgJQgugigbgKIgTgIQgOgGgLgBQgFgBgIgDIgMgFIAAACIgDgzIAJADIA5AUIAjAPQAKAFALALIApApIABACIAQgCIAJgPQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAlQgGASgGAJQAuAJAyAIIAGA1QhYgPghgVQgHAQgPAgQAJATAUAlQAIAPALAiQAQAxgFAVQgFAUgYAVQgRAOgKAFIgKAFQgdANglAAIgKAAgAmRgrQgYA1gDAsQALgNAIgEIgDgFQgBgCAAgNQAAgIAKgiIAJggIgHAOgAIrE6Ig4ABIgBAAIgOjhQgRACgZgCIgFgyIAfABQgHgagCggIgDgfQgCgLADgNIgOACIgKABQABgXAEgWIAIgIQAngiAKg6QAEgYgCgZQAOgPARgPIAEgBIADABIASgXIAJgEQANgHALgDIgGgPIAKgKIAXgWQAGAWAAAWIAAAIIAAAEIgBAGIgBACIgCACIgBABIgLAFQgIADACAFIAHASIAMADQAGA3AKA4QAIAyABA0QACBCARAqQAJAVAZAgQAUAaAEAXQAFAigQA9QgJAgglAXQghAUgoAAQgQAAgRgDgAIFA/QAFARgCABQARBCAJB8IAJgDQAGgBAiABQA7gBALgqQAShCgqhEQgZgqgGgQQgQghAAgnQACgOgBgNQgBgXgJg7QgMhYgCgmIAAgMIgNgDIgLABIgBAQQgCAZgHAxQgEAbgMAcQgHAUgQAgQgBACACANIAFApQACATgBAWIgBAeIABAHIABABIABAAQAEABAGASg");
	this.shape_3.setTransform(74.9,69.1);

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB59F").s().p("AC3COQgWgOgFgpQgDgPAAgzQAAghADgLQACgJAKgIQAcgYAdgsIgVD6gAixALQgYhggXgtQAugJAigCIBEEWIgIAAQhAgqgdhUg");
	this.shape_4.setTransform(72.4,141.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EACFB7").s().p("ABWCTIAVj6QANgTAOgYQA/ABAaADIAoAFQgJAVgcB3QgTBUgyA8gAkGiIQAvgDAaAHIAkAPQAXAJAcAEIAAB7QgIATgEAuQgJAoglAQIgiACg");
	this.shape_5.setTransform(84.2,140.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAlB8QgHgdABgmIADgjIAAhNQAFgVAWgPIApgdQAHgHAQgjQAMgbAQgBIABAAQCggHAeAbQAKAJgEAPQgCAIgHANIgSBTQgMA3gOAkQgrBthoAdQgQAFgOABIgIAAQg8AAgPhFgACShqQgdAsgcAYQgKAIgDAJQgCALAAAhQAAAzACAPQAGApAWAOIAUAAIBIAAQAyg8AThTQAch4AIgVIgogFQgagCg/gCQgNAYgNATgAiSC1QgogBgXgMQgYgOgRAAQgKAAgDgCIgHgGQAAgegohvQgohtAAgmQAAgOAPgFQALgFBKgNIARAAQCbAFAoAtQAPARABAZQAAAOgCAYQAADmh4AAIgCAAgAjfiLQgjACguAJQAYAtAYBgQAcBUBAAqIAIAAIAigBQAmgRAIgoQAEguAIgTIAAh7QgbgEgXgJIglgOQgTgGgdAAIgYABg");
	this.shape_6.setTransform(80.4,140.9);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,-2.6,143.9,170.6);


(lib.DrillBox = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.drill();
	this.instance.setTransform(55.9,48.3,0.67,0.67,0,0,0,39.5,28.2);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#61A061").s().p("AmchEIAJgIIABgBIBHAAIAJgFIIsghQAWAfA7A4QBBA9AiAnIpfAsg");
	this.shape.setTransform(44.5,14.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4F824F").s().p("AAJD3Qg4g2gxgcIgNimQgFgsAChSQAChCADgqIAHhIQAAgdgHgUIACgBIDaC7IAAINIgDADQgxg+g0gxgAhOACICWBzIAAhaIiWhyg");
	this.shape_1.setTransform(13.7,43.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5D995D").s().p("AkyjwIJfgsIABACIAFAFQgHALgGBAQgHBEgBAEIAAFmIgPAHQiAALi+AMIi9AMQg4AFgOAKgAjXBrIAABbICYgNIAAhbg");
	this.shape_2.setTransform(55.8,50.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFF99").s().p("AhLgmICWgMIAABZIiWAMg");
	this.shape_3.setTransform(41.8,64.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCCCCC").s().p("AhLgLIAAhbICWByIAABag");
	this.shape_4.setTransform(13.3,44.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("Ai9GRIgBgDQgEAEgEABQhohxhlhbIgJAEIgDgPIgKgJIAJAEQgPhIgEhjIgChuQAAhUABgYQADhKALgyQABAAABAAQAAAAAAAAQAAgBAAAAQAAgBgBgCIABgEIAAAAIABgDIABAAIAAgCIABgCQABgDADgCIAJgDQAOgQBBAAIAAgDIIdgeQAOADASAAIABAAQAJALALADQAWAVAwA2IBjBwIgBBOQgCBIgEAWIAAFsQgRAbh6AHQh/ACgTACQkkAbgoAAgAmNlRIgJAIIgCABQAHAUgBAcIgGBJQgEAqgBBCQgCBSAEAqIANCoQAyAcA6A2QAzAxAyA9IADgCQAOgKA3gGIC8gLQC/gMCAgLIAPgHIAAlmQACgEAGhEQAHhAAGgMIgEgEIgCgCQgigohBg+Qg7g4gWggIosAhIgJAFIhHAAg");
	this.shape_5.setTransform(43.8,40.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,87.7,80.7);


(lib.DidntGetBadge = function() {
	this.initialize();

	// Layer 1
	this.mc_FinalScoreNotEnoughtPts = new lib.FinalScorecopy();
	this.mc_FinalScoreNotEnoughtPts.setTransform(608.7,193.7,1.38,1.21,0,0,0,91.1,33.5);

	this.text = new cjs.Text("FINAL SCORE", "49px 'VAGRounded BT'");
	this.text.textAlign = "center";
	this.text.lineHeight = 60;
	this.text.lineWidth = 317;
	this.text.setTransform(298.7,167.6,0.97,0.97);

	this.mc_Twitter = new lib.TW_butn();
	this.mc_Twitter.setTransform(833,424.7,1,1,0,0,0,19.2,19.2);
	new cjs.ButtonHelper(this.mc_Twitter, 0, 1, 1);

	this.mc_Facebook = new lib.FB_butn();
	this.mc_Facebook.setTransform(832.9,370,1,1,0,0,0,19.1,19.1);
	new cjs.ButtonHelper(this.mc_Facebook, 0, 1, 1);

	this.text_1 = new cjs.Text("you haven't gained enough points to earn a badge. better luck next time.", "49px 'Laffayette Comic Pro'");
	this.text_1.textAlign = "center";
	this.text_1.lineHeight = 60;
	this.text_1.lineWidth = 786;
	this.text_1.setTransform(456.5,312.1,0.86,0.86);

	this.instance = new lib.EndWhiteBoxcopy();
	this.instance.setTransform(482.4,288.3,1.082,1.05,0,0,0,365.2,185.9);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.addChild(this.instance,this.text_1,this.mc_Facebook,this.mc_Twitter,this.text,this.mc_FinalScoreNotEnoughtPts);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(83.1,111,799,379);


(lib.CustomerSpeechBubb = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"01_Counter_Query":0,"02_Phone_Query":1});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// customer text copy
	this.txt_CounterReply = new cjs.Text("Do you know when my wall tiles will arrive? They’re late.", "30px 'Laffayette Comic Pro'");
	this.txt_CounterReply.name = "txt_CounterReply";
	this.txt_CounterReply.textAlign = "center";
	this.txt_CounterReply.lineHeight = 36;
	this.txt_CounterReply.lineWidth = 363;
	this.txt_CounterReply.setTransform(42.8,-87.1,1.18,1.18);
	this.txt_CounterReply._off = true;

	this.timeline.addTween(cjs.Tween.get(this.txt_CounterReply).wait(1).to({_off:false},0).wait(1));

	// customer
	this.head2_mc = new lib.counter_bubble_head_mc();
	this.head2_mc.setTransform(48.7,-111.8,1,1,0,0,0,234.8,33.9);

	this.stretch2_mc = new lib.NikSpeechStretch2();
	this.stretch2_mc.setTransform(49.7,-78,1,1,0,0,0,235.8,0);

	this.base2_mc = new lib.NikSpeechBase2();
	this.base2_mc.setTransform(48.7,53.3,1,1,0,0,0,234.8,11.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.base2_mc},{t:this.stretch2_mc},{t:this.head2_mc}]},1).wait(1));

	// counter text
	this.txt_CustomerQuestion = new cjs.Text("How much of this paint will I need? ", "28px 'Laffayette Comic Pro'");
	this.txt_CustomerQuestion.name = "txt_CustomerQuestion";
	this.txt_CustomerQuestion.textAlign = "center";
	this.txt_CustomerQuestion.lineHeight = 34;
	this.txt_CustomerQuestion.lineWidth = 363;
	this.txt_CustomerQuestion.setTransform(20.8,-88.1,1.18,1.18);

	this.timeline.addTween(cjs.Tween.get(this.txt_CustomerQuestion).to({_off:true},1).wait(1));

	// counter
	this.head_mc = new lib.customer_bubble_head_mc();
	this.head_mc.setTransform(67.8,-33.6,1,1,0,0,0,275.9,66.5);

	this.stretch_mc = new lib.Nik_SpeechStretch();
	this.stretch_mc.setTransform(28.7,27,1,1.23,0,0,0,236.8,0);

	this.base_mc = new lib.Nik_SpeechBubbleBase();
	this.base_mc.setTransform(27.7,104.3,1,1,0,0,0,235.8,13.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.base_mc},{t:this.stretch_mc},{t:this.head_mc}]}).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-210.1,-102.1,555.9,221.9);


(lib.CounterBadgeAlreadyGot = function() {
	this.initialize();

	// Layer 1
	this.mc_Leaderboard = new lib.LeaderboardButton();
	this.mc_Leaderboard.setTransform(122.6,413.2);
	new cjs.ButtonHelper(this.mc_Leaderboard, 0, 1, 2, false, new lib.LeaderboardButton(), 3);

	this.mc_Twitter = new lib.TW_butn();
	this.mc_Twitter.setTransform(833,445,1,1,0,0,0,19.2,19.2);
	new cjs.ButtonHelper(this.mc_Twitter, 0, 1, 1);

	this.mc_Facebook = new lib.FB_butn();
	this.mc_Facebook.setTransform(832.9,394,1,1,0,0,0,19.1,19.1);
	new cjs.ButtonHelper(this.mc_Facebook, 0, 1, 1);

	this.mc_FinalScore = new lib.FinalScorecopy();
	this.mc_FinalScore.setTransform(246.7,224.7,1.38,1.21,0,0,0,91.1,33.5);

	this.text = new cjs.Text("You've already earned a counter badge", "40px 'Laffayette Comic Pro'");
	this.text.lineHeight = 53;
	this.text.lineWidth = 496;
	this.text.setTransform(118.9,319.8,0.77,0.77);

	this.text_1 = new cjs.Text("FINAL SCORE", "49px 'VAGRounded BT'");
	this.text_1.lineHeight = 60;
	this.text_1.lineWidth = 552;
	this.text_1.setTransform(121,134.1,0.75,0.75);

	this.instance = new lib.TheCounter();
	this.instance.setTransform(652.7,290.8,0.78,0.78,10.8,0,0,221.1,218.7);

	this.instance_1 = new lib.EndWhiteBox();
	this.instance_1.setTransform(482.4,288.3,1.082,1.05,0,0,0,365.2,185.9);
	this.instance_1.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.addChild(this.instance_1,this.instance,this.text_1,this.text,this.mc_FinalScore,this.mc_Facebook,this.mc_Twitter,this.mc_Leaderboard);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(83.1,91,799,408.4);


(lib.CounterBadge = function() {
	this.initialize();

	// Layer 1
	this.mc_Leaderboard = new lib.LeaderboardButton();
	this.mc_Leaderboard.setTransform(122.6,418.8);
	new cjs.ButtonHelper(this.mc_Leaderboard, 0, 1, 2, false, new lib.LeaderboardButton(), 3);

	this.mc_Twitter = new lib.TW_butn();
	this.mc_Twitter.setTransform(833,445,1,1,0,0,0,19.2,19.2);
	new cjs.ButtonHelper(this.mc_Twitter, 0, 1, 1);

	this.mc_Facebook = new lib.FB_butn();
	this.mc_Facebook.setTransform(832.9,394,1,1,0,0,0,19.1,19.1);
	new cjs.ButtonHelper(this.mc_Facebook, 0, 1, 1);

	this.mc_FinalScore = new lib.FinalScorecopy();
	this.mc_FinalScore.setTransform(246.7,224.7,1.38,1.21,0,0,0,91.1,33.5);

	this.text = new cjs.Text("You've earned \na counter badge", "55px 'Laffayette Comic Pro'");
	this.text.lineHeight = 68;
	this.text.lineWidth = 517;
	this.text.setTransform(120.7,313.6,0.77,0.77);

	this.text_1 = new cjs.Text("FINAL SCORE", "49px 'VAGRounded BT'");
	this.text_1.lineHeight = 60;
	this.text_1.lineWidth = 552;
	this.text_1.setTransform(121,134.1,0.75,0.75);

	this.instance = new lib.TheCounter();
	this.instance.setTransform(652.7,290.8,0.78,0.78,10.8,0,0,221.1,218.7);

	this.instance_1 = new lib.EndWhiteBox();
	this.instance_1.setTransform(482.4,288.3,1.082,1.05,0,0,0,365.2,185.9);
	this.instance_1.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.addChild(this.instance_1,this.instance,this.text_1,this.text,this.mc_FinalScore,this.mc_Facebook,this.mc_Twitter,this.mc_Leaderboard);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(83.1,91,799,408.4);


(lib.Boss_Bubble = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"01_Boss_Response_If_Incorrect":0});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 2
	this.mc_BossGotItBtn = new lib.GotItButton();
	this.mc_BossGotItBtn.setTransform(119,163.7,1,1,0,0,0,99,31.7);
	new cjs.ButtonHelper(this.mc_BossGotItBtn, 0, 1, 2, false, new lib.GotItButton(), 3);

	this.timeline.addTween(cjs.Tween.get(this.mc_BossGotItBtn).wait(1));

	// Layer 1
	this.mc_BossMessage = new cjs.Text("You have been honest and told the customer you have never used a level but you still haven't answered their question. Always let the customer know that you will find the answer if you don't know it", "17px 'Laffayette Comic Pro'");
	this.mc_BossMessage.name = "mc_BossMessage";
	this.mc_BossMessage.textAlign = "center";
	this.mc_BossMessage.lineHeight = 23;
	this.mc_BossMessage.lineWidth = 391;
	this.mc_BossMessage.setTransform(125.9,-12.8,1.18,1.18);

	this.timeline.addTween(cjs.Tween.get(this.mc_BossMessage).wait(1));

	// Layer 4
	this.instance = new lib.Symbol2("synched",0);
	this.instance.setTransform(131.8,135,1,1,0,0,0,251.2,16.8);

	this.head_mc = new lib.boss_bubble_head_mc();
	this.head_mc.setTransform(102.5,49.3,1,1,0,0,0,280.7,70.7);

	this.base_mc = new lib.boss_base_mc();
	this.base_mc.setTransform(131.8,160.6,1,1,0,0,0,251.2,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.base_mc},{t:this.head_mc},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-180.2,-23.4,565.4,220.9);


(lib.BadgeYard = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("the\nyard", "30px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 35;
	this.text.lineWidth = 395;
	this.text.setTransform(220.8,199.7,1.18,1.18,-8);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A6C38").s().p("AiPINIhkjCIgKgUIgYgvIgEgIQg9h7g2h9IgCgHIgOgeQhejhhDjqIJUhPICigVQCgJFDlH5IivAYIn7BDIgjhAg");
	this.shape.setTransform(80.9,262.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9E8C49").s().p("AkAZ0QnRjbk6jnQjVidiVjiQgYglgZgqIH3hDICvgXQEJI9FiHeIhrgxgAyfntMAongFZQAkgHBFgIQA2gGA9gOIHhhQQhGG6iEGBIgWA/Qg7CihGCZIg9AJMgnBAFNQjln8igpDgA/triIAAgDIgLg2QgWhxgQhzQghjvgFjbIADirIBnAAIAngIIETgEICigVIBogPQBWJfCSIfIiiAVIpXBQQgoiPgeiSg");
	this.shape_1.setTransform(217.9,252.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCB55E").s().p("EgH1AgDIgWgKQlindkJo+MAmqgFIIAbgFQgbA4gcA2QhBCAgjBCQg9Byg3BUQiVDijVCdQk6DnnPDbQifBLiCA3QhMgghVgngA8M1PQC7gcBUgVQDCgwEpiQQDHhgDgieQDyiqAlhhQAkBhDyCqQDeCeDIBgQEoCQDDAwQCHAiGSAzIESADIAnAIIBoAAQABB+gMCXIAAAHIgEApIgBAJIAAAJIglEqIAAAAInlBOIjkAjMgozAFcQiSofhWpeg");
	this.shape_2.setTransform(255.2,218.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#1C180C").s().p("A5AM5IH9hEICvgXMAnBgFNIA8gJIgfBBIgaAEMgmrAFJIivAXIn3BCIgfg2gEggigE1IJYhQICigWMAoygFcIDkgjIHmhNIgCAJIgDAXIgEAaInhBQQg9AOg3AGQhEAIglAHMgomAFZIijAVIpWBPIgQg4gEAgjgNuIAAABIgEApIgBAJIgDABIAIg0g");
	this.shape_3.setTransform(230.1,238.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("EgA0Ah+IgLgDQgggMAFgCIgCAAQj8hXkzi4QkOihlHj3QiNhrifkPIgHgLIgfg3QhKiGhAiSIgYg3IgbhDQihmUgfhQIgghYQhBitgmiFQgUhFgQhIIgMg3Qg6kjAAlKQgDh+AHhkIAEguIgCgCQgGgFAAgDQAAgPADgDQACgCAIAAQAFgeBIACQBqACAXgGIEVgCICLgWIBlgPIA8gIQCUgXBVgTQDcgzEiicQCphaDUiuQDujFAyggIAAAEIABgEQAyAgDuDFQDUCuCpBaQEiCcDdAzQBUATCUAXQDpAiBEALIEVACQAXAGBqgCQBHgCAGAeQAHAAACACQAEADAAAPQAAADgGAFIgDACQAMBpgDCnIgBAPIgEA5QgLCfgVCWIAAACIAAABIgIA0QgbC0gqCmQg1DThfEQIgWA+Qg2CVhBCmIgVA1IgFANQhdDmiHDgQijEMiNBvQpwHwoWC3QguARgtANIgDAAQgNAAgkgNgEghkgR2QAFDaAhDwQAQBzAWBxIALA2IAAADQAeCSAoCPIAQA4QBDDoBeDhIAOAhIACAGQA2B+A9B6IAEAIIAYAvIALAVIBkDBIAjBBIAfA2QAZArAYAkQCWDiDUCeQE6DnHRDbIBrAxIAWAKQBVAnBKAfQCCg3CfhKQHRjbE6jnQDVieCVjiQA3hUA9hyQAjhCBBh/QAcg3Abg3IAfhBQBGiaA7ikIAWg+QCEl/BGm6IAEgaIAEgXIABgJIAAgBIAlkpIAAgJIABgJIAEgpIAAgIQAMiXgBh9IhoAAIgngIIkSgEQmSgyiHgiQjDgwkoiRQjIhfjgifQjyiqgkhgQgjBgjyCqQjgCfjHBfQkpCRjCAwQhUAVi7AbIhoAPIiiAVIkSAEIgoAIIhnAAg");
	this.shape_4.setTransform(221,218.8);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(0,0,0,0.502)").s().p("EgA0Ah/IgLgEQgggMAFgBIgCgBQj8hYkzi2QkOiilHj3QiNhrifkPIgHgLIgfg3QhKiGhAiRIgYg4IgbhEQihmUgfhPIgghXQhBiugmiFQgUhFgQhIIgLg3Qg7kjAAlJQgDiAAHhiIAEgvIgCgCQgGgGAAgCQAAgPADgDQACgBAIgBQAFgdBIAAQBqADAXgFIEVgDQAogHBjgPIBlgPIA8gJQCUgVBVgUQDcgzEiibQCphbDUivQDujEAyggIAAAFIABgFQAyAgDuDEQDUCvCpBbQEiCbDdAzQBUAUCUAVQDpAjBEALIEVADQAXAFBqgDQBHAAAGAdQAHABACABQAEADAAAPQAAACgGAGIgDACQAMBpgDCoIgBANIgEA6QgLCfgUCWIAAABIgBACIAAABIgDApIgBAJIgDABQgcCzgqCoQg0DShgERIgWA+Qg2CUhBCnIgVAzIgFAPQhdDliHDfQijENiNBvQpwHwoWC4QguAPgtANIgDABQgNAAgkgMg");
	this.shape_5.setTransform(230.5,228.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-10.2,0,484.3,447);


(lib.BadgeDeliveryTruck = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("the", "30px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 112;
	this.text.setTransform(62.3,219.6,1,1,-21);

	this.text_1 = new cjs.Text("delivery truck", "49px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text_1.textAlign = "center";
	this.text_1.lineHeight = 49;
	this.text_1.lineWidth = 332;
	this.text_1.setTransform(220,193.8,1.18,1.18,-8);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A6C38").s().p("AiPINIhkjCIgKgUIgYgvIgEgIQg9h7g2h9IgCgHIgOgeQhejhhDjqIJUhPICigVQCgJFDlH5IivAYIn7BDIgjhAg");
	this.shape.setTransform(80.9,262.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9E8C49").s().p("AkAZ0QnRjbk6jnQjVidiVjiQgYglgZgqIH3hDICvgXQEJI9FiHeIhrgxgAyfntMAongFZQAkgHBFgIQA2gGA9gOIHhhQQhGG6iEGBIgWA/Qg7CihGCZIg9AJMgnBAFNQjln8igpDgA/triIAAgDIgLg2QgWhxgQhzQghjvgFjbIADirIBnAAIAngIIETgEICigVIBogPQBWJfCSIfIiiAVIpXBQQgoiPgeiSg");
	this.shape_1.setTransform(217.9,252.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCB55E").s().p("EgH1AgDIgWgKQlindkJo+MAmqgFIIAbgFQgbA4gcA2QhBCAgjBCQg9Byg3BUQiVDijVCdQk6DnnPDbQifBLiCA3QhMgghVgngA8M1PQC7gcBUgVQDCgwEpiQQDHhgDgieQDyiqAlhhQAkBhDyCqQDeCeDIBgQEoCQDDAwQCHAiGSAzIESADIAnAIIBoAAQABB+gMCXIAAAHIgEApIgBAJIAAAJIglEqIAAAAInlBOIjkAjMgozAFcQiSofhWpeg");
	this.shape_2.setTransform(255.2,218.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#1C180C").s().p("A5AM5IH9hEICvgXMAnBgFNIA8gJIgfBBIgaAEMgmrAFJIivAXIn3BCIgfg2gEggigE1IJYhQICigWMAoygFcIDkgjIHmhNIgCAJIgDAXIgEAaInhBQQg9AOg3AGQhEAIglAHMgomAFZIijAVIpWBPIgQg4gEAgjgNuIAAABIgEApIgBAJIgDABIAIg0g");
	this.shape_3.setTransform(230.1,238.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("EgA0Ah+IgLgDQgggMAFgCIgCAAQj8hXkzi4QkOihlHj3QiNhrifkPIgHgLIgfg3QhKiGhAiSIgYg3IgbhDQihmUgfhQIgghYQhBitgmiFQgUhFgQhIIgMg3Qg6kjAAlKQgDh+AHhkIAEguIgCgCQgGgFAAgDQAAgPADgDQACgCAIAAQAFgeBIACQBqACAXgGIEVgCICLgWIBlgPIA8gIQCUgXBVgTQDcgzEiicQCphaDUiuQDujFAyggIAAAEIABgEQAyAgDuDFQDUCuCpBaQEiCcDdAzQBUATCUAXQDpAiBEALIEVACQAXAGBqgCQBHgCAGAeQAHAAACACQAEADAAAPQAAADgGAFIgDACQAMBpgDCnIgBAPIgEA5QgLCfgVCWIAAACIAAABIgIA0QgbC0gqCmQg1DThfEQIgWA+Qg2CVhBCmIgVA1IgFANQhdDmiHDgQijEMiNBvQpwHwoWC3QguARgtANIgDAAQgNAAgkgNgEghkgR2QAFDaAhDwQAQBzAWBxIALA2IAAADQAeCSAoCPIAQA4QBDDoBeDhIAOAhIACAGQA2B+A9B6IAEAIIAYAvIALAVIBkDBIAjBBIAfA2QAZArAYAkQCWDiDUCeQE6DnHRDbIBrAxIAWAKQBVAnBKAfQCCg3CfhKQHRjbE6jnQDVieCVjiQA3hUA9hyQAjhCBBh/QAcg3Abg3IAfhBQBGiaA7ikIAWg+QCEl/BGm6IAEgaIAEgXIABgJIAAgBIAlkpIAAgJIABgJIAEgpIAAgIQAMiXgBh9IhoAAIgngIIkSgEQmSgyiHgiQjDgwkoiRQjIhfjgifQjyiqgkhgQgjBgjyCqQjgCfjHBfQkpCRjCAwQhUAVi7AbIhoAPIiiAVIkSAEIgoAIIhnAAg");
	this.shape_4.setTransform(221,218.8);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(0,0,0,0.502)").s().p("EgA0Ah/IgLgEQgggMAFgBIgCgBQj8hYkzi2QkOiilHj3QiNhrifkPIgHgLIgfg3QhKiGhAiRIgYg4IgbhEQihmUgfhPIgghXQhBiugmiFQgUhFgQhIIgLg3Qg7kjAAlJQgDiAAHhiIAEgvIgCgCQgGgGAAgCQAAgPADgDQACgBAIgBQAFgdBIAAQBqADAXgFIEVgDQAogHBjgPIBlgPIA8gJQCUgVBVgUQDcgzEiibQCphbDUivQDujEAyggIAAAFIABgFQAyAgDuDEQDUCvCpBbQEiCbDdAzQBUAUCUAVQDpAjBEALIEVADQAXAFBqgDQBHAAAGAdQAHABACABQAEADAAAPQAAACgGAGIgDACQAMBpgDCoIgBANIgEA6QgLCfgUCWIAAABIgBACIAAABIgDApIgBAJIgDABQgcCzgqCoQg0DShgERIgWA+Qg2CUhBCnIgVAzIgFAPQhdDliHDfQijENiNBvQpwHwoWC4QguAPgtANIgDABQgNAAgkgMg");
	this.shape_5.setTransform(230.5,228.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance,this.text_1,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,451.6,447);


(lib.Badge = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("the\npost room", "30px 'Laffayette Comic Pro'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 395;
	this.text.setTransform(220.8,199.7,1.18,1.18,-8);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A6C38").s().p("AiPINIhkjCIgKgUIgYgvIgEgIQg9h7g2h9IgCgHIgOgeQhejhhDjqIJUhPICigVQCgJFDlH5IivAYIn7BDIgjhAg");
	this.shape.setTransform(80.9,262.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9E8C49").s().p("AkAZ0QnRjbk6jnQjVidiVjiQgYglgZgqIH3hDICvgXQEJI9FiHeIhrgxgAyfntMAongFZQAkgHBFgIQA2gGA9gOIHhhQQhGG6iEGBIgWA/Qg7CihGCZIg9AJMgnBAFNQjln8igpDgA/triIAAgDIgLg2QgWhxgQhzQghjvgFjbIADirIBnAAIAngIIETgEICigVIBogPQBWJfCSIfIiiAVIpXBQQgoiPgeiSg");
	this.shape_1.setTransform(217.9,252.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCB55E").s().p("EgH1AgDIgWgKQlindkJo+MAmqgFIIAbgFQgbA4gcA2QhBCAgjBCQg9Byg3BUQiVDijVCdQk6DnnPDbQifBLiCA3QhMgghVgngA8M1PQC7gcBUgVQDCgwEpiQQDHhgDgieQDyiqAlhhQAkBhDyCqQDeCeDIBgQEoCQDDAwQCHAiGSAzIESADIAnAIIBoAAQABB+gMCXIAAAHIgEApIgBAJIAAAJIglEqIAAAAInlBOIjkAjMgozAFcQiSofhWpeg");
	this.shape_2.setTransform(255.2,218.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#1C180C").s().p("A5AM5IH9hEICvgXMAnBgFNIA8gJIgfBBIgaAEMgmrAFJIivAXIn3BCIgfg2gEggigE1IJYhQICigWMAoygFcIDkgjIHmhNIgCAJIgDAXIgEAaInhBQQg9AOg3AGQhEAIglAHMgomAFZIijAVIpWBPIgQg4gEAgjgNuIAAABIgEApIgBAJIgDABIAIg0g");
	this.shape_3.setTransform(230.1,238.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("EgA0Ah+IgLgDQgggMAFgCIgCAAQj8hXkzi4QkOihlHj3QiNhrifkPIgHgLIgfg3QhKiGhAiSIgYg3IgbhDQihmUgfhQIgghYQhBitgmiFQgUhFgQhIIgMg3Qg6kjAAlKQgDh+AHhkIAEguIgCgCQgGgFAAgDQAAgPADgDQACgCAIAAQAFgeBIACQBqACAXgGIEVgCICLgWIBlgPIA8gIQCUgXBVgTQDcgzEiicQCphaDUiuQDujFAyggIAAAEIABgEQAyAgDuDFQDUCuCpBaQEiCcDdAzQBUATCUAXQDpAiBEALIEVACQAXAGBqgCQBHgCAGAeQAHAAACACQAEADAAAPQAAADgGAFIgDACQAMBpgDCnIgBAPIgEA5QgLCfgVCWIAAACIAAABIgIA0QgbC0gqCmQg1DThfEQIgWA+Qg2CVhBCmIgVA1IgFANQhdDmiHDgQijEMiNBvQpwHwoWC3QguARgtANIgDAAQgNAAgkgNgEghkgR2QAFDaAhDwQAQBzAWBxIALA2IAAADQAeCSAoCPIAQA4QBDDoBeDhIAOAhIACAGQA2B+A9B6IAEAIIAYAvIALAVIBkDBIAjBBIAfA2QAZArAYAkQCWDiDUCeQE6DnHRDbIBrAxIAWAKQBVAnBKAfQCCg3CfhKQHRjbE6jnQDVieCVjiQA3hUA9hyQAjhCBBh/QAcg3Abg3IAfhBQBGiaA7ikIAWg+QCEl/BGm6IAEgaIAEgXIABgJIAAgBIAlkpIAAgJIABgJIAEgpIAAgIQAMiXgBh9IhoAAIgngIIkSgEQmSgyiHgiQjDgwkoiRQjIhfjgifQjyiqgkhgQgjBgjyCqQjgCfjHBfQkpCRjCAwQhUAVi7AbIhoAPIiiAVIkSAEIgoAIIhnAAg");
	this.shape_4.setTransform(221,218.8);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(0,0,0,0.502)").s().p("EgA0Ah/IgLgEQgggMAFgBIgCgBQj8hYkzi2QkOiilHj3QiNhrifkPIgHgLIgfg3QhKiGhAiRIgYg4IgbhEQihmUgfhPIgghXQhBiugmiFQgUhFgQhIIgLg3Qg7kjAAlJQgDiAAHhiIAEgvIgCgCQgGgGAAgCQAAgPADgDQACgBAIgBQAFgdBIAAQBqADAXgFIEVgDQAogHBjgPIBlgPIA8gJQCUgVBVgUQDcgzEiibQCphbDUivQDujEAyggIAAAFIABgFQAyAgDuDEQDUCvCpBbQEiCbDdAzQBUAUCUAVQDpAjBEALIEVADQAXAFBqgDQBHAAAGAdQAHABACABQAEADAAAPQAAACgGAGIgDACQAMBpgDCoIgBANIgEA6QgLCfgUCWIAAABIgBACIAAABIgDApIgBAJIgDABQgcCzgqCoQg0DShgERIgWA+Qg2CUhBCnIgVAzIgFAPQhdDliHDfQijENiNBvQpwHwoWC4QguAPgtANIgDABQgNAAgkgMg");
	this.shape_5.setTransform(230.5,228.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-10.2,0,482.8,447);


(lib.Arrow_Butncopy2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.ARR();
	this.instance.setTransform(37.3,34.5,1,1,0,0,0,22.7,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 0, 0, 0, 0)];
	this.instance.cache(-10,-2,58,54);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("ADuFBQgOAFgNAEQgwAOg1AAQiPAAhmhmQhmhmAAiPQAAiQBmhmQBIhIBagVQABAAACgB");
	this.shape.setTransform(23.8,35.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(4,1,1).p("AjVlHQAmgJAqAAQCPAABmBmQBmBmAACPQAACQhmBnQg2A1g/Aa");
	this.shape_1.setTransform(48.3,33.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F4D96F").s().p("AihDqQhXhJgchhQgchfAjhnQAjhnBeheIADgBQAmgIAqAAQCOAABmBmQBmBmAACOQAACRhmBmQg1A1hBAaIgbAJQh5gmhShFg");
	this.shape_2.setTransform(40.7,34.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CCB55E").s().p("Ah5DxQhmhmAAiOQAAiRBmhmQBHhHBagVQhcBdgjBoQgjBnAcBeQAcBhBVBJQBSBFB7AnQgwANg1AAQiOAAhmhmg");
	this.shape_3.setTransform(22.5,35.3);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.Arrow_Butn = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.ARR();
	this.instance.setTransform(37.3,34.5,1,1,0,0,0,22.7,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 0, 0, 0, 0)];
	this.instance.cache(-10,-2,58,54);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AjtFBQAOAFANAEQAwAOA1AAQCPAABmhmQBmhmAAiPQAAiQhmhmQhIhIhagVQgBAAgCgB");
	this.shape.setTransform(45.9,35.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(4,1,1).p("ADWlHQgmgJgqAAQiPAAhmBmQhmBmAACPQAACQBmBnQA2A1A/Aa");
	this.shape_1.setTransform(21.4,33.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F4D96F").s().p("AhEFMQhBgag1g1QhmhmAAiRQAAiOBmhmQBmhmCOAAQAqAAAmAIIADABQBeBeAjBnQAjBngcBfQgcBhhXBJQhSBFh5AmIgbgJg");
	this.shape_2.setTransform(29,34.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CCB55E").s().p("AjfFKQB7gnBShFQBVhJAchhQAchegjhnQgjhohchdQBaAVBHBHQBmBmAACRQAACOhmBmQhmBmiOAAQg1AAgwgNg");
	this.shape_3.setTransform(47.2,35.3);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.AnswerBoxThin = function() {
	this.initialize();

	// Layer 1
	this.stretch_mc = new lib.AnswerBoxThinStretch();
	this.stretch_mc.setTransform(372.6,36,1,1.2,0,0,0,346.2,21);

	this.base_mc = new lib.AnswerBoxThimBase();
	this.base_mc.setTransform(372.5,63.5,1,1,0,0,0,346.1,6.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("Eg2EABLIAAgJQAAiMCOAAMBnuAAAQCNAAAACMIAAAJ");
	this.shape.setTransform(372.5,7.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("Eg2EABLIAAgJQAAiLCOgBMBnuAAAQCNABAACLIAAAJg");
	this.shape_1.setTransform(372.5,7.5);

	this.addChild(this.shape_1,this.shape,this.base_mc,this.stretch_mc);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(24.4,-2,696.3,74);


(lib.AnswerBox = function() {
	this.initialize();

	// Layer 1
	this.stretch_mc = new lib.AnswerBoxStretchMC();
	this.stretch_mc.setTransform(372.6,13.7,1,1.06,0,0,0,346.2,0);

	this.base_mc = new lib.AnswerBoxBaseMC();
	this.base_mc.setTransform(372.6,92.9,1,1,0,0,0,346.2,8.2);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("Eg2EABOIAAgPQAAiMCOAAMBnuAAAQCNAAAACMIAAAP");
	this.shape.setTransform(372.5,7.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("Eg2EABOIAAgPQAAiMCOAAMBnuAAAQCNAAAACMIAAAPg");
	this.shape_1.setTransform(372.5,7.8);

	this.addChild(this.shape_1,this.shape,this.base_mc,this.stretch_mc);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(24.4,-2,696.3,105);


(lib.WinnersCupMC = function() {
	this.initialize();

	// Layer 1
	this.mc_Close = new lib.Arrow_Butncopy();
	this.mc_Close.setTransform(837.7,149,0.712,0.712,0,0,180,34.9,34.9);

	this.instance = new lib.TheCounter();
	this.instance.setTransform(155.4,283.6,0.151,0.151,10.8,0,0,221.5,219);

	this.instance_1 = new lib.BadgeYard();
	this.instance_1.setTransform(390.4,283.6,0.151,0.151,10.8,0,0,221.2,219.1);

	this.instance_2 = new lib.BadgeDeliveryTruck();
	this.instance_2.setTransform(312.7,283.7,0.151,0.151,10.8,0,0,221.5,218.7);

	this.instance_3 = new lib.Badge();
	this.instance_3.setTransform(233.7,283.5,0.151,0.151,10.8,0,0,220.8,218.5);

	this.mc_Twitter = new lib.TW_butn();
	this.mc_Twitter.setTransform(833,445,1,1,0,0,0,19.2,19.2);
	new cjs.ButtonHelper(this.mc_Twitter, 0, 1, 1);

	this.mc_Facebook = new lib.FB_butn();
	this.mc_Facebook.setTransform(832.9,394,1,1,0,0,0,19.1,19.1);
	new cjs.ButtonHelper(this.mc_Facebook, 0, 1, 1);

	this.text = new cjs.Text("you've earned a young grafters winner's cup!", "45px 'Laffayette Comic Pro'");
	this.text.lineHeight = 62;
	this.text.lineWidth = 416;
	this.text.setTransform(126.4,338.8,0.77,0.77);

	this.text_1 = new cjs.Text("YOU HAVE ALL FOUR BADGES", "49px 'VAGRounded BT'");
	this.text_1.lineHeight = 43;
	this.text_1.lineWidth = 358;
	this.text_1.setTransform(126.4,156.6,0.75,0.75);

	this.instance_4 = new lib.WinnersCup();
	this.instance_4.setTransform(601.4,297,0.78,0.78,3,0,0,221.1,218.8);

	this.instance_5 = new lib.EndWhiteBox();
	this.instance_5.setTransform(482.4,288.3,1.082,1.05,0,0,0,365.2,185.9);
	this.instance_5.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.addChild(this.instance_5,this.instance_4,this.text_1,this.text,this.mc_Facebook,this.mc_Twitter,this.instance_3,this.instance_2,this.instance_1,this.instance,this.mc_Close);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(83.1,111,799,379);


(lib.Sure_you_wanna_quit = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_23 = function() {
		this.stop();
	}
	this.frame_29 = function() {
		gameCounter.confirmedQuitGame();
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(23).call(this.frame_23).wait(6).call(this.frame_29).wait(1));

	// NoBtn
	this.btn_No = new lib.NoBtn();
	this.btn_No.setTransform(609.6,94.4,0.17,0.17,0,0,0,85.2,31.8);
	new cjs.ButtonHelper(this.btn_No, 0, 1, 2, false, new lib.NoBtn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.btn_No).to({regX:85,regY:31.7,scaleX:1,scaleY:1,x:236.3,y:250.3},4).wait(20).to({regX:85.2,regY:31.8,scaleX:0.17,scaleY:0.17,x:609.6,y:94.4},4).to({_off:true},1).wait(1));

	// YesBtn
	this.btn_Yes = new lib.YesBtn();
	this.btn_Yes.setTransform(647.3,94.4,0.17,0.17,0,0,0,85.2,31.8);
	new cjs.ButtonHelper(this.btn_Yes, 0, 1, 2, false, new lib.YesBtn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.btn_Yes).to({regX:85,regY:31.7,scaleX:1,scaleY:1,x:458.3,y:250.3},4).wait(20).to({regX:85.2,regY:31.8,scaleX:0.17,scaleY:0.17,x:647.3,y:94.4},4).to({_off:true},1).wait(1));

	// Sure
	this.instance = new lib.Sure();
	this.instance.setTransform(685.9,62.5,0.17,0.17,0,0,0,481.6,-167.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:481.8,scaleX:1,scaleY:1},4).wait(20).to({regX:481.6,scaleX:0.17,scaleY:0.17},4).to({_off:true},1).wait(1));

	// Layer 2
	this.mc_bg = new lib._50Percent_white();
	this.mc_bg.setTransform(281,263.4,1,1,0,0,0,500,285.9);

	this.timeline.addTween(cjs.Tween.get(this.mc_bg).wait(30));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-219,-22.5,1000,571.9);


(lib.SteveShirtandTie = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape.setTransform(25.6,90.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_1.setTransform(9.8,86.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgOEQQgKhDAAgbIABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgPQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgvQAAgmAGgtIAtADQgGAnAAAgQAAAZAGAKQAVAlA7gFIEXADIAAgoIAAgTQAaAUASAaIgCAiIACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAiQgMA8gZAOIhEAAQgsgRg1gRQACAPAAATIAAAWIAEAAQALAACdAhIAYAFIBUAPQgFALgGAKQgKAQgPANIgmgGQgpgIgrgJQgygLgigKIAFARQALAoAIAoIgMAAIghgBgAjDgwQAAAGBRAdIAyAPQCDAtAdAAQAgAAABhBIgDhBIjnAAIgXAAQgZgEgUgGIgWAtg");
	this.shape_2.setTransform(24.9,92.1);

	// Layer 1
	this.instance = new lib.shirt2();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#497C59").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_3.setTransform(18,46.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#5A996E").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_4.setTransform(85.2,58);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_5.setTransform(86,143.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_6.setTransform(67.5,124.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_7.setTransform(9.8,86.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_8.setTransform(86.9,128);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_9.setTransform(94.8,140.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EAD0B7").s().p("ArABdIgygRIABgHQAHgpABgWIDpAAIADBBQgBBDggAAQgdAAiFgtgAJQgNIgPhBQAhgKAxggIAZgRQAIAOAZAhQAmAyAAArQAAAXhaAIQgoAEgWAGIgLg5g");
	this.shape_10.setTransform(88.9,83);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_11.setTransform(84.4,77.7);

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-8.8,168.9,164.3);


(lib.SteveShirt = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFIABgDIAAgBIABgJIAEgfIACgSIADAAIDkAAIABAPIAAAAIACAxQgBBBggAAQgdAAiDgsg");
	this.shape.setTransform(25.6,90.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCB59F").s().p("AhGBeIAAgBIACgHQAAgXAJgRQAPgaAMgZQAMgaAegQIADgQIAAgvIAAgRIA6AAIgGAhQgHAfgHAPQADAFgRAYQgKAXgDAnIgDAPIgDAAIgCATIgEAeIgBAJIAAACIgBADIgBAHQhPgdAAgFg");
	this.shape_1.setTransform(12.4,77.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgsDFIAAgOQAAgdADgRIgxgOIgggJQgtgOgTgQQgWgUAAggQAAgHALgcQALgcACgBIAHgLIACgDIAEgIQAVgjAQgjIADgbQACgSAHgbIAsANIAAAAIAAAQIAAAwIgCAPQghARgMAZQgMAbgPAaQgJAQAAAXIgCAGIAAABQAAAGBRAcIAyASQCDAsAdAAQAgAAABhDIgCgxIAAgBQACgGAAgKIAAAAIgEgUIgEgWQgOhNgEgbIgGgoIAjgBIADAWQACANAMA2QAHAfABAaIATArIAAABQACAUgCAVQAAAcgHAkQgMA8gZAOIhEAAQgsgSg1gQQABAOAAAUIAAAPg");
	this.shape_2.setTransform(21.7,83.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EACFB7").s().p("ABxBXIjkAAIADgPQAEgnAJgZQARgXgDgEQAHgQAHgfIAHghICUgCIAGApQAEAbAOBKIAEAWIAEAWIAAAAQAAAKgCAHIgBgPg");
	this.shape_3.setTransform(26.2,75);

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_4.setTransform(25.6,90.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_5.setTransform(9.8,86.7);

	// Layer 1
	this.instance = new lib.shirt();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EACFB7").s().p("AgdCVQAEgZAAgHIAAgBQgRALgEAAIgNAAQgOAMgGAAQgNAAgFgLIgBAAQgOAAgDgNIgBgJQADgIACgNQADgnAKgZQARgXgDgEQAHgQAHgfQAKgtAAgYIgBgJIABgHIAAgHIACgDQAKgMAuAAQAcgBAOAHQACgKAJgDQALgGALAMIADACIAAAEQAEAUABAOQACAXAIAzQAEAbAOBLIAEAVIAEAWIAAAAQAAAKgCAHQgFAPgOAAQgNAAgEgJIgCAAQgEAAgDgDQgJAMgWAAQgFAAgEgCQgIASgMAAIgBAAQgHAYgJAAQgVAAgBgVg");
	this.shape_6.setTransform(26.1,73.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_7.setTransform(86,143.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_8.setTransform(67.5,124.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CCB59F").s().p("AhICEIACgHQAAgXAJgSQAPgaAMgbQAMgZAfgPIACgPIAAgwQAAgegBgLIADgVQAAgJgDgEQALgLASgFQAegJAEAYIAAAGIAAAHIAAAKQAAAYgKAtQgHAfgHAQQADAEgRAXQgJAYgEAnQgCANgCAJIgDAGQgCAUgFAeIgBAIQhPgdAAgGg");
	this.shape_9.setTransform(12.6,74);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_10.setTransform(86.9,128);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_11.setTransform(94.8,140.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EAD0B7").s().p("ArACdIgygRIABgHQAGgfABgUIADgGIAAAJQADANAOAAIACAAQAEALANAAQAGAAAOgMIANAAQAEAAARgLIAAABQAAAHgEAZQABAVAVAAQALAAAHgYIABAAQAMAAAIgSQAEACAFAAQAWAAAJgMQADADAEAAIACAAQAEAJANAAQAOAAAFgPIACAyQgBBDggAAQgdAAiFgtgAI1gZQglh5gDgNQASgHALgJIAegaIAYApQAaApAZAgQAHAJAQAJIATAKQAcATAPAqQALAiAAAfQAAAXhaAIQgoAEgWAGQgEgXgihug");
	this.shape_12.setTransform(88.9,76.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AkAImQjFghg5gUQg7gVgBglQg1kaABg+IAAgMIgQgGQgFADgJAAQgTAAgEggIAAgUIAAgOQAAgbADgRIgxgNIgggKQgsgOgUgQQgWgUAAggQAAgHALgeQALgcACgBIAHgLIACgDIAEgIQAVgiAQgkIADgbQACgVAKgiQgBgMAHgHQAIgSANgEQAKgDADAIQADAEAAAJIgDAVQABALAAAeIAAAwIgCAPQghARgMAZQgMAbgPAaQgJASAAAXIgCAHQAAAGBRAcIAyASQCFAsAdAAQAhAAAAhDIgCgxQACgHAAgKIAAAAIgEgWIgEgWQgOhMgEgcQgIgygCgYQgBgOgDgUIgBgDIgCgDQACgZARAQQAUARABATIAJBEQACANAMA2QAHAgABAZIATAtIAAABQACAUgCAVQAAAcgHAkQgMA7gZANIhDAAQgsgQg3gQQABAOAAASIAAAPIAAAHIAEAAQAMAACeAgIAYAFQCUAdBAAFIGzgPQBIABBIgOQA3gKBAgVQAjgLAogLQAegJAggLQgSgygLhDQgRh1gMg1QgDgOgfhAIAKhyIAPAiIASArQADANAmB6QAhBvAEAXQAWgGApgDQBZgIAAgYQAAgfgLghQgPgtgcgSIgSgKQgRgJgHgKQgZgggagoIgYgpIgeAZQgKAKgTAGQAQgLAJgvQAJgrAUgHQgBgUgIgYIAxAAQAGAVgBAPIACAAIADAFQADADAAALQAAAJgPATQACACAcA3QAcA1AGACQA1AUAjBFQAeA4AAAtQAAAkgFAGQgLARg6APIh1AAQAGArAAAjQAAAwgXAOIgDgHQgbAShBAaIgMAbQgTArgCAjQgBATAFAoQAAAQgGANQAjAHAdAOQBRAoAABMQAAASgSANQgdAWhBgCIk7AAQguABgZgXQgTgRAAgTQgIgWgMg3IgDgMIgBAEQgTBegTAmIACAEIAAABIABAAIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoDG9QALANBZAPQCRAZASAEIBiAAIB1AAQASgJAYgSIhbgYQh6gfheAAQgTAAhJAGIgDAAQhTAIgPAFIgZAAIAFAGgAgRGgQAeAMAWAMQAFgkAUhtQARheAHgzIkuAFQhggFhsgTQgpgHgqgKQgzgLghgJIAEAQQAlCJAACRQAUgFBYgIIA3gEQAzgEAdAAQCmAAB6AtgAGLE0Qg/AHhbAtQgeAPgiATQAHAFAAADQAEAEAsAOIEwAAIAJgCIADAAQATgDAbgDQg8hqhuAAQgOAAgPACgAEYCTIhAABIhWACIAAAJQAAAUgDAaQAJATAHAeQAMAvAEAuQATgTAhgRQAQgIARgGQBAgYBQgCQAogCAkAEIgDgMQgCgLAAgiQABhNAFgcQhNAVhsAPgAI2mNIAAAAg");
	this.shape_13.setTransform(84.8,100.4);

	this.addChild(this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.instance,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.8,0,168.1,155.5);


(lib.Steve_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 3
	this.instance = new lib.HardHat();
	this.instance.setTransform(91.9,33.6,1.89,1.89,0,0,0,43.1,29.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).to({_off:true},1).wait(2));

	// Layer 2
	this.instance_1 = new lib.Male_Head_01_Front();
	this.instance_1.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 1
	this.instance_2 = new lib.Male_Body_02_Front();
	this.instance_2.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_3 = new lib.SteveShirt();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.YardSteve_03();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.Motif();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_6 = new lib.SteveShirtandTie();
	this.instance_6.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.PeppaSkirt = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.GirlOutfit04();
	this.instance.setTransform(82.1,89.6,1.12,1.12,0,0,0,62.6,83.9);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BA986F").s().p("ABQIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAjqHGQgbhJgSgeQARAEAkAbQAjAaASADIAmB3QgjAGgdAMIgjhegAqJATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUgAKBAAQgfgWgOgjQgFgMgFgYQgCgUAAgXQgKhAgfg1IgGgKQgKgRgGgEIgJgfQgOg4gFgfIgPhHQA7BrAmBYQAiBSADAgQAEAzAWApQAaAxArAJIgTAGQgFACgEADIAAAAIgKADg");
	this.shape_1.setTransform(75,96.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D5AE81").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAI1hAQgWgpgEgzQgDgggihSQgmhYg7hqQgKgtgCgSIAOgEIABgBIAIgBQAVgEAXAAIADAAIAWgIQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APIgUAGQgrgKgagxg");
	this.shape_2.setTransform(82.3,95.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#371E2D").s().p("AinAoQg1gEgHgDIAJgZQAFgMAPgJIAOgGIAGgCIARgEIgBAHQAAAeBjAZIAaAGQgdACgjgBQgRABgxgFgACDAOQAAgOgEgKQAGAAAGgIQAIgKAEgDQAKgIAQgFQAKAsANAPQAJAKATAIIhIAGQgUABgGABQABgKAAgRg");
	this.shape_3.setTransform(73,158.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#4D2A3E").s().p("Ai8AqQhjgYAAgfIABgHQA6gNCNABQATACAQAQQAQAOgBAQIgFAFQgHAHgQAFQgjAMg+ADIgagGgABLAUQgNgPgKgrQAcgJAyAAQBIAAAqATQAjAQAJAYQACAIghAEQg2AGhkAIQgTgIgJgKg");
	this.shape_4.setTransform(85.5,158.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#381E2D").s().p("AD+E6QhMgKiBgOQhigMhVgRQAGgJAGgSQAOgnAIg6IAOhSQAFgaAAgdIAAgKIAAgBIgBgJIAAgHQAGgDAHgBQAFAAAJAMQAIALAEAJQADAHAIAvQAPA2AlAsQAnAsA4AWQAgAMA+AMQA3AKAVAKQAhASAIAoQgqgDgegEgAjxheIg5gUQgZgJgCgJQgDgbAmgsQAfgjAkgZQAtggBKgZQgVAQgjAvQgmAzgVAuQgGAPgFAgQgDATgHAAIgBAAg");
	this.shape_5.setTransform(47.8,47.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#4D2A3F").s().p("AATGLQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgBgLgDgGQgDgEgDgDQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgJgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6IAMAPIAKASIADAHIgBAAIgOAEQgRAHgFALQgFAOgGAZQgHAegBALIAAAkQAAAyASAxQAMAfAmBHIADAGIgtAKQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_6.setTransform(78.6,39.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgXghgnhnIgjheIAAgGIA7AQQASAeAbBJIAjBeQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJIAsgBQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMIA0gMQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgAo5DDQgYgBgVgCIg1gLQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABQgMAHgJAHQAJgHAMgHIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQADADACAEIhIDbQgYA3gEAsQgHAxATAWQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgeANglAAIgKgBgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAJZB/IgBAAIgCAAQgSAAgOgSQgUgSgQgvQgUg2AAgZIABgRQgBgSgOgnQgWg5AAgCIAAgJIAAAAIAAgBQAAgRAHAFQAGAEALARIAGAKQAfA1AKBAQAAAXACATQAFAWAFAMQAOAjAfAYIAcAAIAKgFIAAABQAEgDAFgCIATgGIAUgFQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJIgDgBQgXAAgVAFIgIABIgDgHIgLgSIgLgRQgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgFAAIg3gBgAF1rgIgDgFIADAFg");
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,173.4);


(lib.PeppaOptions = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		this.stop();
	}
	this.frame_2 = function() {
		this.stop();
	}
	this.frame_3 = function() {
		this.stop();
	}
	this.frame_4 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1).call(this.frame_2).wait(1).call(this.frame_3).wait(1).call(this.frame_4).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#491616").s().p("AhFHQQhzhLgcgOIgTiIQgMhRABghQAAhWAHgoQAGgpAXhDQBckRByiBQAbghAggZQAkCKA9C0QAyCWAmBjIgrAAQgNAGgLARQhFAYgMCaQgEA6AEBLIAFBSIADBOQADA7AFATQhJgihshIg");
	this.shape.setTransform(41.4,113.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330F0F").s().p("ABNC3IgDAAQgYAAADhDQgBgJABgJQgHg3gJhbIgMgnIgBgEQgeBLgHAaIgcBmQgVBCgVABQgGAEgGAAIgBAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBABAAQABgPAEgVIACgJIABgGIAKgpQAFgUAHgWQALgpANghQAKgeAMgaIACgCQANggAOgZQAFgJAFgIIALgOIAEgHIADgDIAAABIAGALIAHARIAEALQAFARAEAVQAFATACAXIADAhIAAAGQAEBIAHAwQACATADAPQABAOADAKIAAAgIADAAIgLACQgFgBgEgCg");
	this.shape_1.setTransform(83,100.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6B2121").s().p("AD3FdQhvgUgmgCIjkAAQgDgPgCgTQgHgwgEhKIAAgGIgDghQgCgXgFgTQgEgVgFgRIgEgLIgHgRIgGgLIAAgBIgDADIgEAHIgLAOQgFAIgFAJQgQAZgNAfIgCADQgMAagKAeQgNAjgLApQgHAWgFAUIgKApIhTAAQgmhjgyiWQg9izgkiJQCjiLD4ACQCmAADCBZQDNBfBZCFQAeAtAXBVQAXBYAJBmQABALgDAwQgCAhAJAXQkLgRhAgLg");
	this.shape_2.setTransform(105.2,80.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AljKtQhDgchJgwQhTg6gxgfQgMgJgfgLQgYgJgNgOIgOgQIgKAAIgDgEQgFgDgDgTQgXhogGhiQgKizArigQBNkhDnimQCJhiC5gWQA1gHA1ABQB1gBB1AhQCjArB+BiQB/BjBBCHQBECMgICdIgGBWIgDgCIAAAKIgDANQAFAGACAIIgLAZIiKAAIi2gWQiWgUgtgCIjOAAIgDAAIAAggQgDgLgBgOIDmAAQAmACBvAUQBAAMELAQQgJgWACgiQADgvgBgJQgJhmgXhYQgXhXgegtQhZiGjNhfQjDhZilAAQj4gBijCKQggAagcAgQhzCBhcESQgXBBgGApQgHAqAABWQgBAgAMBSIATCIQAcANBzBMQBuBHBJAiQgFgTgDg7IgDhOIgFhRQgEhMAEg6QAMiaBFgYQALgRANgIIArAAIBTAAIgBAHIgCAJQgEAUgBAPQAAABgBAAQAAABAAAAQAAABAAAAQAAABAAAAIAAACIhwAAQgtBegJCIQgDAjADEPIADAIQgFAOgDAFQgGAGgJAAQgGAAgGgDg");
	this.shape_3.setTransform(89.1,107.2);

	this.instance = new lib.PeppaHead();
	this.instance.setTransform(91.2,146.6,1,1,0,0,0,69.8,62);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#6B2121").s().p("AgLEqQgcAAgUgBQggh+gHgiQgIgrAJhSQAFgyAEggQAIg6AOgpQAsh1BzgLIAJDaQABBNgOB+QgIBIgEAUQgKA5gRAZIg9AAg");
	this.shape_4.setTransform(151.2,142.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgHFXIg7gCQhAiGgHgpQgDgZAOh8IAKhfQAHg2ANgmQAihkBphIIBFAIQAUA4AFBoQAFB3gODAQgBAWgNBoQgGAwgJAKQgUAWhHAAIgPAAgAA3EyQARgaALg5QADgUAIhIQAPiAgBhLIgJjaQhzAMgsB0QgPApgIA6QgEAggEAzQgJBSAHArQAHAiAgB9QAUACAcgBIA9ABg");
	this.shape_5.setTransform(150.7,141.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(5));

	// Layer 2
	this.instance_1 = new lib.PeppaClothes();
	this.instance_1.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_2 = new lib.FemaleTrousersPeppa();
	this.instance_2.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_3 = new lib.FemaleHoodiePeppa();
	this.instance_3.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_4 = new lib.FemaleMotifPeppa();
	this.instance_4.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_5 = new lib.PeppaSkirt();
	this.instance_5.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(11.7,38.3,159.9,318.6);


(lib.OptionsMenu = function() {
	this.initialize();

	// Layer 2
	this.btn_Leaderboard = new lib.Symbol1();
	this.btn_Leaderboard.setTransform(36.1,100.1,1.26,1.26,0,0,0,24.1,24.1);

	this.btn_Twitter = new lib.ShareTwitter();
	this.btn_Twitter.setTransform(163,343.4,1.26,1.26,0,0,0,167.8,31.7);
	new cjs.ButtonHelper(this.btn_Twitter, 0, 1, 2, false, new lib.ShareTwitter(), 3);

	this.btn_Facebook = new lib.ShareFB();
	this.btn_Facebook.setTransform(163,283.3,1.26,1.26,0,0,0,167.8,31.7);
	new cjs.ButtonHelper(this.btn_Facebook, 0, 1, 2, false, new lib.ShareFB(), 3);

	this.btn_Sound = new lib.SoundIconRoundBtncopy();
	this.btn_Sound.setTransform(49.1,232,1.26,1.26,0,0,0,39.9,35.3);

	this.btn_Logout = new lib.LogOutRoundBtncopy();
	this.btn_Logout.setTransform(36.8,171.3,1.26,1.26,0,0,0,13.7,12.8);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("A1z9mMArnAAAQBkAAAABkMAAAA4FQAABkhkAAMgrnAAAQhkAAAAhkMAAAg4FQAAhkBkAAg");
	this.shape.setTransform(149.6,189.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F4D96F").s().p("A1zdmQhkAAAAhjMAAAg4FQAAhjBkgBMArnAAAQBkABAABjMAAAA4FQAABjhkAAg");
	this.shape_1.setTransform(149.6,189.5);

	// Layer 3
	this.mc_bg = new lib._50Percent_white();
	this.mc_bg.setTransform(480,270.4,1,1,0,0,0,500,285.9);

	this.addChild(this.mc_bg,this.shape_1,this.shape,this.btn_Logout,this.btn_Sound,this.btn_Facebook,this.btn_Twitter,this.btn_Leaderboard);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-20,-15.5,1000,571.9);


(lib.option5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 3
	this.big_txt = new cjs.Text("They will all be fine", "30px 'Laffayette Comic Pro'");
	this.big_txt.name = "big_txt";
	this.big_txt.textAlign = "center";
	this.big_txt.lineHeight = 28;
	this.big_txt.lineWidth = 563;
	this.big_txt.setTransform(343.7,14.6,1.18,1.18);
	this.big_txt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.big_txt).wait(1).to({_off:false},0).wait(1));

	// Layer 2
	this.small_txt = new cjs.Text("You may need to cut a little off the bottom or side, what measurements do you have?", "22px 'Laffayette Comic Pro'");
	this.small_txt.name = "small_txt";
	this.small_txt.textAlign = "center";
	this.small_txt.lineHeight = 20;
	this.small_txt.lineWidth = 563;
	this.small_txt.setTransform(343.7,5.8,1.18,1.18);

	this.timeline.addTween(cjs.Tween.get(this.small_txt).to({_off:true},1).wait(1));

	// Layer 1
	this.mc_C5 = new lib.AnswerBoxThin();
	this.mc_C5.setTransform(346.2,50.5,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.mc_C5, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.mc_C5).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.2,-2,696.7,74);


(lib.option4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 3
	this.big_txt = new cjs.Text("They will all be fine", "30px 'Laffayette Comic Pro'");
	this.big_txt.name = "big_txt";
	this.big_txt.textAlign = "center";
	this.big_txt.lineHeight = 28;
	this.big_txt.lineWidth = 563;
	this.big_txt.setTransform(343.7,14.6,1.18,1.18);
	this.big_txt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.big_txt).wait(1).to({_off:false},0).wait(1));

	// Layer 2
	this.small_txt = new cjs.Text("Let's take a look at the coverage per litre \ndetails on the pot", "22px 'Laffayette Comic Pro'");
	this.small_txt.name = "small_txt";
	this.small_txt.textAlign = "center";
	this.small_txt.lineHeight = 20;
	this.small_txt.lineWidth = 563;
	this.small_txt.setTransform(343.7,5.8,1.18,1.18);

	this.timeline.addTween(cjs.Tween.get(this.small_txt).to({_off:true},1).wait(1));

	// Layer 1
	this.mc_C4 = new lib.AnswerBoxThin();
	this.mc_C4.setTransform(346.2,50.5,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.mc_C4, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.mc_C4).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.2,-2,696.7,74);


(lib.option3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 3
	this.big_txt = new cjs.Text("They will all be fine", "30px 'Laffayette Comic Pro'");
	this.big_txt.name = "big_txt";
	this.big_txt.textAlign = "center";
	this.big_txt.lineHeight = 28;
	this.big_txt.lineWidth = 563;
	this.big_txt.setTransform(343.7,14.6,1.18,1.18);
	this.big_txt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.big_txt).wait(1).to({_off:false},0).wait(1));

	// Layer 2
	this.small_txt = new cjs.Text("Let's take a look at the coverage per litre \ndetails on the pot", "22px 'Laffayette Comic Pro'");
	this.small_txt.name = "small_txt";
	this.small_txt.textAlign = "center";
	this.small_txt.lineHeight = 20;
	this.small_txt.lineWidth = 563;
	this.small_txt.setTransform(343.7,5.8,1.18,1.18);

	this.timeline.addTween(cjs.Tween.get(this.small_txt).to({_off:true},1).wait(1));

	// Layer 1
	this.mc_C3 = new lib.AnswerBoxThin();
	this.mc_C3.setTransform(346.2,50.5,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.mc_C3, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.mc_C3).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.2,-2,696.7,74);


(lib.option2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 3
	this.big_txt = new cjs.Text("They will all be fine", "30px 'Laffayette Comic Pro'");
	this.big_txt.name = "big_txt";
	this.big_txt.textAlign = "center";
	this.big_txt.lineHeight = 28;
	this.big_txt.lineWidth = 563;
	this.big_txt.setTransform(343.7,14.6,1.18,1.18);
	this.big_txt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.big_txt).wait(1).to({_off:false},0).wait(1));

	// Layer 2
	this.small_txt = new cjs.Text("Let's take a look at the coverage per litre \ndetails on the pot", "22px 'Laffayette Comic Pro'");
	this.small_txt.name = "small_txt";
	this.small_txt.textAlign = "center";
	this.small_txt.lineHeight = 20;
	this.small_txt.lineWidth = 563;
	this.small_txt.setTransform(343.7,5.8,1.18,1.18);

	this.timeline.addTween(cjs.Tween.get(this.small_txt).to({_off:true},1).wait(1));

	// Layer 1
	this.mc_C2 = new lib.AnswerBoxThin();
	this.mc_C2.setTransform(346.2,50.5,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.mc_C2, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.mc_C2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.2,-2,696.7,74);


(lib.option1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 3
	this.big_txt = new cjs.Text("They will all be fine", "30px 'Laffayette Comic Pro'");
	this.big_txt.name = "big_txt";
	this.big_txt.textAlign = "center";
	this.big_txt.lineHeight = 28;
	this.big_txt.lineWidth = 563;
	this.big_txt.setTransform(343.7,14.6,1.18,1.18);
	this.big_txt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.big_txt).wait(1).to({_off:false},0).wait(1));

	// Layer 2
	this.small_txt = new cjs.Text("Let's take a look at the coverage per litre \ndetails on the pot", "22px 'Laffayette Comic Pro'");
	this.small_txt.name = "small_txt";
	this.small_txt.textAlign = "center";
	this.small_txt.lineHeight = 20;
	this.small_txt.lineWidth = 563;
	this.small_txt.setTransform(343.7,5.8,1.18,1.18);

	this.timeline.addTween(cjs.Tween.get(this.small_txt).to({_off:true},1).wait(1));

	// Layer 1
	this.mc_C1 = new lib.AnswerBoxThin();
	this.mc_C1.setTransform(346.2,50.5,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.mc_C1, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.mc_C1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.2,-2,696.7,74);


(lib.LadyBossMC2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{Default:0,StartTalking:8,TalkLoop:13,StopTalking:30,PressGotIt:52});

	// timeline functions:
	this.frame_8 = function() {
		this.stop();
	}
	this.frame_51 = function() {
		this.stop();
	}
	this.frame_63 = function() {
		this.gotoAndStop(0);
		gameCounter.doGotItWrongWearPressedDone();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(8).call(this.frame_8).wait(43).call(this.frame_51).wait(12).call(this.frame_63).wait(2));

	// Layer 3
	this.mc_LadyBossWrongWorkware = new lib.LadyBoss();
	this.mc_LadyBossWrongWorkware.setTransform(743.4,132.1,0.94,0.94,0,0,0,216.6,210.7);

	this.timeline.addTween(cjs.Tween.get(this.mc_LadyBossWrongWorkware).wait(8).to({mode:"synched",startPosition:0,loop:false},0).wait(22).to({startPosition:0},0).wait(25).to({y:131.3},0).to({y:551.3},8).to({_off:true},1).wait(1));

	// Layer 1
	this.mc_GotItBtn = new lib.GotItButton();
	this.mc_GotItBtn.setTransform(631.1,123.4,0.142,0.142,0,0,0,99,31.7);
	this.mc_GotItBtn._off = true;
	new cjs.ButtonHelper(this.mc_GotItBtn, 0, 1, 2, false, new lib.GotItButton(), 3);

	this.timeline.addTween(cjs.Tween.get(this.mc_GotItBtn).wait(4).to({_off:false},0).to({scaleX:1,scaleY:1,x:414.3,y:253.6},4,cjs.Ease.get(1)).wait(44).to({scaleX:0.14,scaleY:0.14,x:631.1,y:123.4},4).to({_off:true},1).wait(8));

	// Layer 2
	this.instance = new lib.LadyBoss_Bubble_Yard();
	this.instance.setTransform(667.1,101.9,0.142,0.142,0,0,0,394.4,13.4);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({_off:false},0).to({regY:13.3,scaleX:1,scaleY:1,x:668.2,y:101.7},4,cjs.Ease.get(1)).wait(44).to({regY:13.4,scaleX:0.14,scaleY:0.14,x:667.1,y:101.9},4).to({_off:true},1).wait(8));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(604.2,-66,229.4,325.3);


(lib.KateSkirtJane = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.GirlOutfit04();
	this.instance.setTransform(82.1,89.6,1.12,1.12,0,0,0,62.6,83.9);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7A4D30").s().p("ABQIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAjqHGQgbhJgSgeQARAEAkAbQAjAaASADIAmB3QgjAGgdAMIgjhegAqJATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUgAKBAAQgfgWgOgjQgFgMgFgYQgCgUAAgXQgKhAgfg1IgGgKQgKgRgGgEIgJgfQgOg4gFgfIgPhHQA7BrAmBYQAiBSADAgQAEAzAWApQAaAxArAJIgTAGQgFACgEADIAAAAIgKADg");
	this.shape_1.setTransform(75,96.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#975F3C").s().p("AhkLkQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBLcQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1ERIgQgBQgLgBgKgEIgOgEIgKgmIgMgnQgOg+ARglQAcgsAPgrQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBtADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFAVgOATIgHAGQgbAahAAAIgYgBgAI1CMQgWgpgEgzQgDgggihQQgmhYg7hqQgKgtgCgSQgSAHgEALQgFAOgGAZQgHAegBALIAAAkQAAAyASAxQAMAeAmBGIADAGIgtAKQhAANikADQhjABhDgFQgIgnghgQQgVgKg4gKQg+gMgggMQg3gXgogrQgngtgOg1QgIgvgDgHQgFgLgHgLQgJgNgFABQgHABgHACQgCgKgDgGQgCgFgDgCQgKgHgPAVIgKAPIgPACIgBgCIgpgpQgLgLgLgFIgjgPQAIACADgVQAFghAHgOQAUguAngzQAigwAVgPIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBvgcBtAAQBDAAAfAJQBgAcA7BfQgLABAAAMQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgFQgVgDAAAhQAAAFAqA6IALARIALASIADAGIAIgBQAVgEAXAAIADAAIAWgIQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAgQA5A3AABEQAAAsg4APIgUAGQgrgKgagxg");
	this.shape_2.setTransform(82.3,75.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#371E2D").s().p("AinAoQg1gEgHgDIAJgZQAFgMAPgJIAOgGIAGgCIARgEIgBAHQAAAeBjAZIAaAGQgdACgjgBQgRABgxgFgACDAOQAAgOgEgKQAGAAAGgIQAIgKAEgDQAKgIAQgFQAKAsANAPQAJAKATAIIhIAGQgUABgGABQABgKAAgRg");
	this.shape_3.setTransform(73,158.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#4D2A3E").s().p("Ai8AqQhjgYAAgfIABgHQA6gNCNABQATACAQAQQAQAOgBAQIgFAFQgHAHgQAFQgjAMg+ADIgagGgABLAUQgNgPgKgrQAcgJAyAAQBIAAAqATQAjAQAJAYQACAIghAEQg2AGhkAIQgTgIgJgKg");
	this.shape_4.setTransform(85.5,158.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#381E2D").s().p("AD+E6QhMgKiBgOQhigMhVgRQAGgJAGgSQAOgnAIg6IAOhSQAFgaAAgdIAAgKIAAgBIgBgJIAAgHQAGgDAHgBQAFAAAJAMQAIALAEAJQADAHAIAvQAPA2AlAsQAnAsA4AWQAgAMA+AMQA3AKAVAKQAhASAIAoQgqgDgegEgAjxheIg5gUQgZgJgCgJQgDgbAmgsQAfgjAkgZQAtggBKgZQgVAQgjAvQgmAzgVAuQgGAPgFAgQgDATgHAAIgBAAg");
	this.shape_5.setTransform(47.8,47.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgXghgnhnIgjheIAAgGIA7AQQASAeAbBJIAjBeQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJIAsgBQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMIA0gMQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgAo5DDQgYgBgVgCIg1gLQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABQgMAHgJAHQAJgHAMgHIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQADADACAEIhIDbQgYA3gEAsQgHAxATAWQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgeANglAAIgKgBgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAJZB/IgBAAIgCAAQgSAAgOgSQgUgSgQgvQgUg2AAgZIABgRQgBgSgOgnQgWg5AAgCIAAgJIAAAAIAAgBQAAgRAHAFQAGAEALARIAGAKQAfA1AKBAQAAAXACATQAFAWAFAMQAOAjAfAYIAcAAIAKgFIAAABQAEgDAFgCIATgGIAUgFQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJIgDgBQgXAAgVAFIgIABIgDgHIgLgSIgLgRQgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgFAAIg3gBgAF1rgIgDgFIADAFg");
	this.shape_6.setTransform(78.9,87.4);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,173.4);


(lib.KateSkirt = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.GirlOutfit04();
	this.instance.setTransform(82.1,89.6,1.12,1.12,0,0,0,62.6,83.9);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#371E2D").s().p("AinAoQg1gEgHgDIAJgZQAFgMAPgJIAOgGIAGgCIARgEIgBAHQAAAeBjAZIAaAGQgdACgjgBQgRABgxgFgACDAOQAAgOgEgKQAGAAAGgIQAIgKAEgDQAKgIAQgFQAKAsANAPQAJAKATAIIhIAGQgUABgGABQABgKAAgRg");
	this.shape_1.setTransform(73,158.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#4D2A3E").s().p("Ai8AqQhjgYAAgfIABgHQA6gNCNABQATACAQAQQAQAOgBAQIgFAFQgHAHgQAFQgjAMg+ADIgagGgABLAUQgNgPgKgrQAcgJAyAAQBIAAAqATQAjAQAJAYQACAIghAEQg2AGhkAIQgTgIgJgKg");
	this.shape_2.setTransform(85.5,158.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CCB59F").s().p("ABQIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAjqHGQgbhJgSgeQARAEAkAbQAjAaASADIAmB3QgjAGgdAMIgjhegAqJATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUgAKBAAQgfgWgOgjQgFgMgFgYQgCgUAAgXQgKhAgfg1IgGgKQgKgRgGgEIgJgfQgOg4gFgfIgPhHQA7BrAmBYQAiBSADAgQAEAzAWApQAaAxArAJIgTAGQgFACgEADIAAAAIgKADg");
	this.shape_3.setTransform(75,96.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#381E2D").s().p("AD+E6QhMgKiBgOQhigMhVgRQAGgJAGgSQAOgnAIg6IAOhSQAFgaAAgdIAAgKIAAgBIgBgJIAAgHQAGgDAHgBQAFAAAJAMQAIALAEAJQADAHAIAvQAPA2AlAsQAnAsA4AWQAgAMA+AMQA3AKAVAKQAhASAIAoQgqgDgegEgAjxheIg5gUQgZgJgCgJQgDgbAmgsQAfgjAkgZQAtggBKgZQgVAQgjAvQgmAzgVAuQgGAPgFAgQgDATgHAAIgBAAg");
	this.shape_4.setTransform(47.8,47.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EACFB7").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAI1hAQgWgpgEgzQgDgggihSQgmhYg7hqQgKgtgCgSIAOgEIABgBIAIgBQAVgEAXAAIADAAIAWgIQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APIgUAGQgrgKgagxg");
	this.shape_5.setTransform(82.3,95.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#4D2A3F").s().p("AATGLQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgBgLgDgGQgDgEgDgDQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgJgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6IAMAPIAKASIADAHIgBAAIgOAEQgRAHgFALQgFAOgGAZQgHAegBALIAAAkQAAAyASAxQAMAfAmBHIADAGIgtAKQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_6.setTransform(78.6,39.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgXghgnhnIgjheIAAgGIA7AQQASAeAbBJIAjBeQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJIAsgBQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMIA0gMQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgAo5DDQgYgBgVgCIg1gLQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABQgMAHgJAHQAJgHAMgHIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQADADACAEIhIDbQgYA3gEAsQgHAxATAWQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgeANglAAIgKgBgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAJZB/IgBAAIgCAAQgSAAgOgSQgUgSgQgvQgUg2AAgZIABgRQgBgSgOgnQgWg5AAgCIAAgJIAAAAIAAgBQAAgRAHAFQAGAEALARIAGAKQAfA1AKBAQAAAXACATQAFAWAFAMQAOAjAfAYIAcAAIAKgFIAAABQAEgDAFgCIATgGIAUgFQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJIgDgBQgXAAgVAFIgIABIgDgHIgLgSIgLgRQgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgFAAIg3gBgAF1rgIgDgFIADAFg");
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,173.4);


(lib.KateOptions = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		this.stop();
	}
	this.frame_2 = function() {
		this.stop();
	}
	this.frame_3 = function() {
		this.stop();
	}
	this.frame_4 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1).call(this.frame_2).wait(1).call(this.frame_3).wait(1).call(this.frame_4).wait(1));

	// Layer 1
	this.instance = new lib.GHAIR();
	this.instance.setTransform(84,107.7,1,1,0,0,0,84,107.7);

	this.instance_1 = new lib.HeadG();
	this.instance_1.setTransform(90.5,146.6,1,1,0,0,0,69.8,62);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(5));

	// Layer 2
	this.instance_2 = new lib.KateClothes();
	this.instance_2.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_3 = new lib.FemaleTrousers();
	this.instance_3.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_4 = new lib.FemaleHoodie();
	this.instance_4.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_5 = new lib.FemaleMotif();
	this.instance_5.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_6 = new lib.KateSkirt();
	this.instance_6.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,171.6,356.9);


(lib.Jason_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 4
	this.instance = new lib.HardHat();
	this.instance.setTransform(91.9,33.6,1.89,1.89,0,0,0,43.1,29.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).to({_off:true},1).wait(2));

	// Layer 2
	this.instance_1 = new lib.Jason();
	this.instance_1.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 1
	this.instance_2 = new lib.Male_Body_Frank_Front();
	this.instance_2.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_3 = new lib.SteveShirt();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.YardSteve_03();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.Motif();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_6 = new lib.SteveShirtandTie();
	this.instance_6.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.JaneOptions = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		this.stop();
	}
	this.frame_2 = function() {
		this.stop();
	}
	this.frame_3 = function() {
		this.stop();
	}
	this.frame_4 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1).call(this.frame_2).wait(1).call(this.frame_3).wait(1).call(this.frame_4).wait(1));

	// Layer 1
	this.instance = new lib.HInfront();
	this.instance.setTransform(87.1,123,0.97,1.07,0,0,0,84.5,80.2);

	this.instance_1 = new lib.JaneHead();
	this.instance_1.setTransform(90,146.6,1,1,0,0,0,69.8,62);

	this.instance_2 = new lib.Behind();
	this.instance_2.setTransform(155.1,157.5,1.07,1.07,8.5,0,0,20.5,40.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(5));

	// Layer 2
	this.instance_3 = new lib.JaneClothes();
	this.instance_3.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_4 = new lib.FemaleTrousersJane();
	this.instance_4.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_5 = new lib.FemaleHoodieJane();
	this.instance_5.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_6 = new lib.FemaleMotifJane();
	this.instance_6.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_7 = new lib.KateSkirtJane();
	this.instance_7.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3}]}).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(5.2,37.2,178.8,319.7);


(lib.Instructions = function() {
	this.initialize();

	// Layer 1
	this.mc_InstructionGotIt = new lib.GotItButton();
	this.mc_InstructionGotIt.setTransform(198.2,276.3,1,1,0,0,0,99,31.7);
	new cjs.ButtonHelper(this.mc_InstructionGotIt, 0, 1, 2, false, new lib.GotItButton(), 3);

	this.text = new cjs.Text("answer the customer queries by choosing from multiple choice answers. levels 2 and 3 are against the clock!", "28px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 33;
	this.text.lineWidth = 355;
	this.text.setTransform(194.2,17.9,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EgjcgVTMBG4AAAQCOAAAACOMAAAAmLQAACOiOAAMhG4AAAQiNAAAAiOIAAiGMAAAghHIAAi+QAAiOCNAAg");
	this.shape.setTransform(199.7,138.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EDED5E").s().p("EgjcAVUQiNAAAAiOIAAiGMAAAghHIAAi+QAAiOCNAAMBG4AAAQCOAAAACOMAAAAmLQAACOiOAAg");
	this.shape_1.setTransform(199.7,138.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("EgjcAVUQiNAAAAiOIAAiGMAAAghHIAAi+QAAiOCNAAMBG4AAAQCOAAAACOMAAAAmLQAACOiOAAg");
	this.shape_2.setTransform(208.2,146.7);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text,this.mc_InstructionGotIt);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-43.4,-0.2,492.7,310.3);


(lib.Highlighter = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_36 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(36).call(this.frame_36).wait(1));

	// HIGHLIGHTER
	this.instance = new lib.Highlight("synched",0,false);
	this.instance.setTransform(24.8,24.8,1,1,0,0,0,24.8,24.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(36).to({startPosition:23},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(2.8,2.8,44.1,44.1);


(lib.Health_and_Safety_Bubble = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.HandS_Bubble();
	this.instance.setTransform(540.5,201,0.153,0.153,0,0,0,540.4,201);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:540.5,scaleX:1,scaleY:1},4).wait(20).to({regX:540.4,scaleX:0.15,scaleY:0.15},4).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(455.6,163.6,86.6,40.3);


(lib.Greg_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 4
	this.instance = new lib.HardHat();
	this.instance.setTransform(91.9,33.6,1.89,1.89,0,0,0,43.1,29.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).to({_off:true},1).wait(2));

	// Layer 2
	this.instance_1 = new lib.GregHead();
	this.instance_1.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 1
	this.instance_2 = new lib.Male_Greg_Front();
	this.instance_2.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_3 = new lib.FrankShirt();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.YardSteve_03();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.MotifGreg();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_6 = new lib.FrankShirtandTie();
	this.instance_6.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.BossMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"Default":0,"StartTalking":4,"TalkLoop":9,"StopTalking":26});

	// timeline functions:
	this.frame_25 = function() {
		this.stop();
	}
	this.frame_34 = function() {
		gameCounter.bossTalkDone();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(25).call(this.frame_25).wait(9).call(this.frame_34).wait(1));

	// Layer 3
	this.instance = new lib.BossAnims("single",0);
	this.instance.setTransform(-153.8,170.3,0.79,0.79,0,0,0,232.8,228.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({mode:"synched",loop:false},0).wait(5).to({startPosition:5},0).wait(17).to({startPosition:21},0).wait(9));

	// Boss_Bubble
	this.mc_BossBubble = new lib.Boss_Bubble();
	this.mc_BossBubble.setTransform(223.9,148.3,0.114,0.114,0,0,0,-176.1,115);
	this.mc_BossBubble._off = true;

	this.timeline.addTween(cjs.Tween.get(this.mc_BossBubble).wait(4).to({_off:false},0).to({regX:-176.3,scaleX:1,scaleY:1},4).wait(18).to({regX:-176.1,scaleX:0.11,scaleY:0.11},4).to({_off:true},1).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.1,-10.5,316,372.9);


(lib.BackArrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Arrow_Butn();
	this.instance.setTransform(34.9,34.9,1,1,0,0,0,34.9,34.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AkLEMQhvhvAAidQAAicBvhvQBPhPBmgXQAqgJAsAAQCdAABvBvQBvBvAACcQAACdhvBvQg6A7hHAbQhAAZhLAAQicAAhvhvg");
	this.shape.setTransform(34.8,34.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regX:34.9,regY:34.9,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regX:34.8,regY:34.8,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.SpeechBubbcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"01_Reply":0,"01_Answers":1,"01_Answer01_Incorrect":2,"01_Answer03_Correct":3,"02_Reply":5,"02_Answers":6,"01_Answer01_Correct":7,"01_Answer02_Incorrect":8,"03_Reply":10,"03_Answers":11,"01_Answer01_Incorrect":12,"01_Answer02_Correct":13});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_3 = function() {
		this.stop();
	}
	this.frame_5 = function() {
		this.stop();
	}
	this.frame_10 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3).call(this.frame_3).wait(2).call(this.frame_5).wait(5).call(this.frame_10).wait(4));

	// Level 3 copy 2
	this.txt_l3_right = new cjs.Text("You may need to cut a little \noff the bottom or side. what\nmeasurements do you have?", "24px 'Laffayette Comic Pro'");
	this.txt_l3_right.name = "txt_l3_right";
	this.txt_l3_right.textAlign = "center";
	this.txt_l3_right.lineHeight = 30;
	this.txt_l3_right.lineWidth = 379;
	this.txt_l3_right.setTransform(280.6,23,1.18,1.18);
	this.txt_l3_right._off = true;

	this.timeline.addTween(cjs.Tween.get(this.txt_l3_right).wait(13).to({_off:false},0).wait(1));

	// Level 3 copy
	this.txt_l3_wrong = new cjs.Text("Just use the matt one.", "24px 'Laffayette Comic Pro'");
	this.txt_l3_wrong.name = "txt_l3_wrong";
	this.txt_l3_wrong.textAlign = "center";
	this.txt_l3_wrong.lineHeight = 30;
	this.txt_l3_wrong.lineWidth = 282;
	this.txt_l3_wrong.setTransform(194.8,130.3,1.18,1.18);
	this.txt_l3_wrong._off = true;

	this.timeline.addTween(cjs.Tween.get(this.txt_l3_wrong).wait(12).to({_off:false},0).to({_off:true},1).wait(1));

	// Level 3
	this.txt_Reply_3 = new lib.ReplyText();
	this.txt_Reply_3.setTransform(115.1,67.5,1,1,0,0,0,80.9,18.9);

	this.button5 = new lib.option5();
	this.button5.setTransform(73.6,403.6,1,1,0,0,0,346.2,49.4);

	this.button4 = new lib.option4();
	this.button4.setTransform(73.6,315.1,1,1,0,0,0,346.2,49.4);

	this.button3 = new lib.option3();
	this.button3.setTransform(73.6,226.5,1,1,0,0,0,346.2,49.4);

	this.button2 = new lib.option2();
	this.button2.setTransform(73.6,138,1,1,0,0,0,346.2,49.4);

	this.button1 = new lib.option1();
	this.button1.setTransform(73.6,49.4,1,1,0,0,0,346.2,49.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.txt_Reply_3}]},10).to({state:[{t:this.button1},{t:this.button2},{t:this.button3},{t:this.button4},{t:this.button5}]},1).to({state:[]},1).wait(2));

	// Level 2 copy 2
	this.txt_l2_wrong = new cjs.Text("It’ll probably come today.", "24px 'Laffayette Comic Pro'");
	this.txt_l2_wrong.name = "txt_l2_wrong";
	this.txt_l2_wrong.textAlign = "center";
	this.txt_l2_wrong.lineHeight = 30;
	this.txt_l2_wrong.lineWidth = 284;
	this.txt_l2_wrong.setTransform(196.1,131.1,1.18,1.18);
	this.txt_l2_wrong._off = true;

	this.timeline.addTween(cjs.Tween.get(this.txt_l2_wrong).wait(8).to({_off:false},0).to({_off:true},1).wait(5));

	// Level 2 copy
	this.txt_l2_right = new cjs.Text("I’ll check the delivery date on the computer.", "24px 'Laffayette Comic Pro'");
	this.txt_l2_right.name = "txt_l2_right";
	this.txt_l2_right.textAlign = "center";
	this.txt_l2_right.lineHeight = 30;
	this.txt_l2_right.lineWidth = 280;
	this.txt_l2_right.setTransform(270.5,31.9,1.18,1.18);
	this.txt_l2_right._off = true;

	this.timeline.addTween(cjs.Tween.get(this.txt_l2_right).wait(7).to({_off:false},0).to({_off:true},1).wait(6));

	// Level 2
	this.txt_Reply_2 = new lib.ReplyText();
	this.txt_Reply_2.setTransform(115.1,67.4,1,1,0,0,0,80.9,18.9);

	this.txt_l2_answer4 = new cjs.Text("I don’t know, sorry.", "24px 'Laffayette Comic Pro'");
	this.txt_l2_answer4.name = "txt_l2_answer4";
	this.txt_l2_answer4.textAlign = "center";
	this.txt_l2_answer4.lineHeight = 30;
	this.txt_l2_answer4.lineWidth = 553;
	this.txt_l2_answer4.setTransform(70.6,285,1.18,1.18);

	this.txt_l2_answer3 = new cjs.Text("It’ll probably come today.", "24px 'Laffayette Comic Pro'");
	this.txt_l2_answer3.name = "txt_l2_answer3";
	this.txt_l2_answer3.textAlign = "center";
	this.txt_l2_answer3.lineHeight = 30;
	this.txt_l2_answer3.lineWidth = 556;
	this.txt_l2_answer3.setTransform(67.2,194,1.18,1.18);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.txt_Reply_2}]},5).to({state:[{t:this.txt_l2_answer3},{t:this.txt_l2_answer4}]},1).to({state:[]},1).wait(7));

	// Level 1 - right
	this.txt_l1_right = new cjs.Text("We’re getting a delivery tomorrow, you can call back then.", "24px 'Laffayette Comic Pro'");
	this.txt_l1_right.name = "txt_l1_right";
	this.txt_l1_right.textAlign = "center";
	this.txt_l1_right.lineHeight = 30;
	this.txt_l1_right.lineWidth = 237;
	this.txt_l1_right.setTransform(253.9,24.3,1.18,1.18);
	this.txt_l1_right._off = true;

	this.timeline.addTween(cjs.Tween.get(this.txt_l1_right).wait(3).to({_off:false},0).to({_off:true},1).wait(10));

	// Level 1- wrong
	this.txt_l1_wrong = new cjs.Text("There are other taps on the shelves, choose a different one.", "24px 'Laffayette Comic Pro'");
	this.txt_l1_wrong.name = "txt_l1_wrong";
	this.txt_l1_wrong.textAlign = "center";
	this.txt_l1_wrong.lineHeight = 30;
	this.txt_l1_wrong.lineWidth = 273;
	this.txt_l1_wrong.setTransform(266.6,32.2,1.18,1.18);
	this.txt_l1_wrong._off = true;

	this.timeline.addTween(cjs.Tween.get(this.txt_l1_wrong).wait(2).to({_off:false},0).to({_off:true},1).wait(11));

	// Level 1
	this.txt_Reply_1 = new lib.ReplyText();
	this.txt_Reply_1.setTransform(115.1,68.4,1,1,0,0,0,80.9,18.9);

	this.txt_l1_answer3 = new cjs.Text("We’re getting a delivery tomorrow, you can call back then.", "24px 'Laffayette Comic Pro'");
	this.txt_l1_answer3.name = "txt_l1_answer3";
	this.txt_l1_answer3.textAlign = "center";
	this.txt_l1_answer3.lineHeight = 30;
	this.txt_l1_answer3.lineWidth = 553;
	this.txt_l1_answer3.setTransform(70.6,278.8,1.18,1.18);

	this.txt_l1_answer2 = new cjs.Text("That probably means we haven’t got any.", "24px 'Laffayette Comic Pro'");
	this.txt_l1_answer2.name = "txt_l1_answer2";
	this.txt_l1_answer2.textAlign = "center";
	this.txt_l1_answer2.lineHeight = 30;
	this.txt_l1_answer2.lineWidth = 564;
	this.txt_l1_answer2.setTransform(70.7,156.9,1.18,1.18);

	this.txt_l1_answer1 = new cjs.Text("There are other taps on the shelves, choose a different one.", "24px 'Laffayette Comic Pro'");
	this.txt_l1_answer1.name = "txt_l1_answer1";
	this.txt_l1_answer1.textAlign = "center";
	this.txt_l1_answer1.lineHeight = 30;
	this.txt_l1_answer1.lineWidth = 589;
	this.txt_l1_answer1.setTransform(71.6,35.9,1.18,1.18);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.txt_Reply_1}]}).to({state:[{t:this.txt_l1_answer1},{t:this.txt_l1_answer2},{t:this.txt_l1_answer3}]},1).to({state:[]},1).wait(12));

	// Level 3
	this.mc_reply_3 = new lib.ReplyBox();
	this.mc_reply_3.setTransform(115.4,65.6,1,1,0,0,0,85.8,30.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EAkxALbIhHAAMhCaAAAQiOAAAAiOIAAk4ImACbIGAocIAAngQAAiOCOAAMBDhAAAQCOAAAACOIAASZQAACOiOAAg");
	this.shape.setTransform(263.5,83.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("EAjqALaMhCaAAAQiOAAAAiNIAAk4Il/CbIF/ocIAAngQAAiNCOAAMBDhAAAQCNAAABCNIAASZQgBCNiNAAg");
	this.shape_1.setTransform(263.5,83.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.mc_reply_3}]},10).to({state:[]},1).to({state:[{t:this.shape_1},{t:this.shape}]},2).wait(1));

	// Level 2
	this.mc_reply_2 = new lib.ReplyBox();
	this.mc_reply_2.setTransform(115.4,65.6,1,1,0,0,0,85.8,30.8);

	this.txt_l2_answer1 = new cjs.Text("It’ll probably come today.", "24px 'Laffayette Comic Pro'");
	this.txt_l2_answer1.name = "txt_l2_answer1";
	this.txt_l2_answer1.textAlign = "center";
	this.txt_l2_answer1.lineHeight = 30;
	this.txt_l2_answer1.lineWidth = 554;
	this.txt_l2_answer1.setTransform(68.7,9,1.18,1.18);

	this.mc_B1 = new lib.AnswerBoxThin();
	this.mc_B1.setTransform(73.6,50.5,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.mc_B1, 0, 1, 1);

	this.txt_l2_answer2 = new cjs.Text("It’ll probably come today.", "24px 'Laffayette Comic Pro'");
	this.txt_l2_answer2.name = "txt_l2_answer2";
	this.txt_l2_answer2.textAlign = "center";
	this.txt_l2_answer2.lineHeight = 30;
	this.txt_l2_answer2.lineWidth = 556;
	this.txt_l2_answer2.setTransform(68.2,103,1.18,1.18);

	this.mc_B2 = new lib.AnswerBoxThin();
	this.mc_B2.setTransform(73.6,146,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.mc_B2, 0, 1, 1);

	this.mc_B3 = new lib.AnswerBoxThin();
	this.mc_B3.setTransform(73.6,236.9,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.mc_B3, 0, 1, 1);

	this.mc_B4 = new lib.AnswerBoxThin();
	this.mc_B4.setTransform(73.6,326.8,1,1,0,0,0,372.5,50.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(4,1,1).p("AfzMBIhHAAMgx8AAAQiOAAAAiNIAAlnIsigQIMilxIAAn/QAAiNCOAAMAzDAAAQCOAAAACNIAATnQAACNiOAAg");
	this.shape_2.setTransform(231.7,101.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7EEA9B").s().p("AesMBMgx8AAAQiOAAAAiNIAAlnIsigQIMilxIAAn/QAAiNCOAAMAzDAAAQCOAAAACNIAATnQAACNiOAAg");
	this.shape_3.setTransform(231.7,101.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(4,1,1).p("AfzPqIhHAAMgx8AAAQgfAAgYgHQhXgYAAhvIAAoYIAAg3Ipfi8IjDg8IMiiJIAArnQAAiOCOAAMAzDAAAQBrAAAaBQQAJAbAAAjIAAa3QAACOiOAAg");
	this.shape_4.setTransform(157.2,217.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(0,0,0,0.502)").s().p("AfDPqMgyqAAAQhvAAgYhXQAYAHAfAAMAx8AAAIBHAAQCOAAAAiOIAA63QgBgjgIgbQCHADAACLIAAa3QAACOiNAAgEgiXAAVIBzgUIJfC8IAAA3g");
	this.shape_5.setTransform(167.5,225.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FD6737").s().p("AesPqMgx8AAAQgfAAgYgHQhXgYAAhvIAAoYIAAg3Ipfi8IjDg8IMiiJIAArnQAAiOCOAAMAzDAAAQBrAAAaBQQAJAbAAAjIAAa3QAACOiOAAg");
	this.shape_6.setTransform(157.2,217.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.mc_reply_2}]},5).to({state:[{t:this.mc_B4},{t:this.mc_B3},{t:this.mc_B2},{t:this.txt_l2_answer2},{t:this.mc_B1},{t:this.txt_l2_answer1}]},1).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4}]},1).to({state:[]},1).wait(5));

	// Level 1
	this.mc_reply_1 = new lib.ReplyBox();
	this.mc_reply_1.setTransform(115.4,65.6,1,1,0,0,0,85.8,30.8);

	this.mc_A1 = new lib.AnswerBox();
	this.mc_A1.setTransform(73.6,81.9,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.mc_A1, 0, 1, 1);

	this.mc_A2 = new lib.AnswerBox();
	this.mc_A2.setTransform(73.6,202.8,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.mc_A2, 0, 1, 1);

	this.mc_A3 = new lib.AnswerBox();
	this.mc_A3.setTransform(73.6,323.7,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.mc_A3, 0, 1, 1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(4,1,1).p("AfzPqIhHAAMgx8AAAQiOAAAAiOIAApPIsij4IMiiJIAArnQAAiOCOAAMAzDAAAQCOAAAACOIAAa3QAACOiOAAg");
	this.shape_7.setTransform(231.7,124.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FD6737").s().p("AesPqMgx8AAAQiOAAAAiOIAApPIsij4IMiiJIAArnQAAiOCOAAMAzDAAAQCOAAAACOIAAa3QAACOiOAAg");
	this.shape_8.setTransform(231.7,124.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(4,1,1).p("Ad/R1IhHAAMguUAAAQiOAAAAiNIAAtnIsij2IMiiKIAAroQAAiNCOAAMAvbAAAQCOAAAACNIAAfPQAACNiOAAg");
	this.shape_9.setTransform(220.1,138.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#7EEA9B").s().p("Ac4R1MguUAAAQiNAAAAiNIAAtnIsij2IMiiKIAAroQAAiNCNAAMAvbAAAQCOAAAACNIAAfPQAACNiOAAg");
	this.shape_10.setTransform(220.1,138.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_reply_1}]}).to({state:[{t:this.mc_A3},{t:this.mc_A2},{t:this.mc_A1}]},1).to({state:[{t:this.shape_8},{t:this.shape_7}]},1).to({state:[{t:this.shape_10},{t:this.shape_9}]},1).to({state:[]},1).wait(10));

	// Layer 2
	this.instance = new lib.ReplyBoxShad();
	this.instance.setTransform(123.9,74.1,1,1,0,0,0,85.8,30.8);

	this.instance_1 = new lib.AnswerBoxShad();
	this.instance_1.setTransform(81.6,89.9,1,1,0,0,0,372.5,50.5);

	this.instance_2 = new lib.AnswerBoxShad();
	this.instance_2.setTransform(81.6,210.8,1,1,0,0,0,372.5,50.5);

	this.instance_3 = new lib.AnswerBoxShad();
	this.instance_3.setTransform(81.6,331.7,1,1,0,0,0,372.5,50.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("rgba(0,0,0,0.502)").s().p("AfDPqMgyqAAAQiOAAAAiOIAApPIsij4IMiiJIAArnQAAiOCOAAMAzxAAAQCOAAAACOIAAa3QAACOiOAAg");
	this.shape_11.setTransform(242.1,132.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(0,0,0,0.502)").s().p("AdPR1MgvCAAAQiOAAAAiNIAAtnIsij2IMiiKIAAroQAAiNCOAAMAwJAAAQCOAAAACNIAAfPQAACNiOAAg");
	this.shape_12.setTransform(230.4,146.7);

	this.instance_4 = new lib.AnswerBoxShadThin();
	this.instance_4.setTransform(80.6,58,1,1,0,0,0,372.5,50.5);

	this.instance_5 = new lib.AnswerBoxShadThin();
	this.instance_5.setTransform(81.2,154,1,1,0,0,0,372.5,50.5);

	this.instance_6 = new lib.AnswerBoxShadThin();
	this.instance_6.setTransform(81.6,243.9,1,1,0,0,0,372.5,50.5);

	this.instance_7 = new lib.AnswerBoxShadThin();
	this.instance_7.setTransform(81.6,333.8,1,1,0,0,0,372.5,50.5);
	new cjs.ButtonHelper(this.instance_7, 0, 1, 1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("rgba(0,0,0,0.502)").s().p("AfDMBMgyqAAAQiOAAAAiNIAAlnIsigQIMilxIAAn/QAAiNCOAAMAzxAAAQCOAAAACNIAATnQAACNiOAAg");
	this.shape_13.setTransform(242.1,109.5);

	this.instance_8 = new lib.AnswerBoxShadThin();
	this.instance_8.setTransform(81.6,324.9,1,1,0,0,0,372.5,50.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(4,1,1).p("AfzPqIhHAAMgx8AAAQgfAAgYgHQhXgYAAhvIAAoYIAAg3Ipfi8IjDg8IMiiJIAArnQAAiOCOAAMAzDAAAQBrAAAaBQQAJAbAAAjIAAa3QAACOiOAAg");
	this.shape_14.setTransform(157.2,217.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FD6737").s().p("AesPqMgx8AAAQgfAAgYgHQhXgYAAhvIAAoYIAAg3Ipfi8IjDg8IMiiJIAArnQAAiOCOAAMAzDAAAQBrAAAaBQQAJAbAAAjIAAa3QAACOiOAAg");
	this.shape_15.setTransform(157.2,217.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("rgba(0,0,0,0.502)").s().p("AfDPqMgyqAAAQhvAAgYhXQAYAHAfAAMAx8AAAIBHAAQCOAAAAiOIAA63QgBgjgIgbQCHADAACLIAAa3QAACOiNAAgEgiXAAVIBzgUIJfC8IAAA3g");
	this.shape_16.setTransform(167.5,225.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("rgba(0,0,0,0.502)").s().p("EAkBALaMhCKAAAQiOAAAAiNIAAk4Im+CbIG+ocIAAngQAAiOCOABMBDRAAAQCOgBAACOIAASZQAACNiOAAg");
	this.shape_17.setTransform(273.8,91.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance_7,p:{y:333.8}},{t:this.instance_6,p:{x:81.6,y:243.9}},{t:this.instance_5,p:{x:81.2,y:154}},{t:this.instance_4,p:{x:80.6,y:58}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[]},1).to({state:[{t:this.instance}]},2).to({state:[{t:this.instance_8},{t:this.instance_7,p:{y:57.1}},{t:this.instance_6,p:{x:82.5,y:236}},{t:this.instance_5,p:{x:82.3,y:149.5}},{t:this.instance_4,p:{x:79.3,y:413.5}}]},1).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14}]},1).to({state:[{t:this.shape_17}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(27.6,32.8,182,72.1);


(lib.LucyOptions = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		this.stop();
	}
	this.frame_2 = function() {
		this.stop();
	}
	this.frame_3 = function() {
		this.stop();
	}
	this.frame_4 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1).call(this.frame_2).wait(1).call(this.frame_3).wait(1).call(this.frame_4).wait(1));

	// Layer 1
	this.instance = new lib.LucyHair();
	this.instance.setTransform(83.8,107.7,1,1,0,0,0,84,107.7);

	this.instance_1 = new lib.LucyHead();
	this.instance_1.setTransform(90.3,146.6,1,1,0,0,0,69.8,62);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E5C45A").s().p("Ai+DJQgQgSgMgWQgjhBAAhfQAAhpAFgaQAOhCA/hHQEHgnBqCIQA4BJAABhQAABMgZA9QgNAhgUAfQgZAmglAjIiFACQh9AAhChLg");
	this.shape.setTransform(30,164.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AkSDQIgHgLQgbgygGhBIAAhNQAAi7BjheQBXhSCAAPQB9APBbBdQBiBlAACKQAABfglBdIgHAQQg/CKiDAAQkIAAhWiKgAirkEQg+BHgOBCQgFAagBBpQABBeAjBCQAMAWAQASQBBBLB+AAICEgDQAlgjAZglQAVgfANghQAZg+AAhNQgBhfg4hJQhShqiwAAQgzAAg8AJg");
	this.shape_1.setTransform(29.9,163.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance_1},{t:this.instance}]}).wait(5));

	// Layer 2
	this.instance_2 = new lib.LucyClothes();
	this.instance_2.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_3 = new lib.FemaleTrousers();
	this.instance_3.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_4 = new lib.FemaleHoodie();
	this.instance_4.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_5 = new lib.FemaleMotif();
	this.instance_5.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_6 = new lib.KateSkirt();
	this.instance_6.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.6,30,175.4,326.9);


// stage content:



(lib.The_Counter_10_CC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{ChooseSteveWorkwear:0,NotSuitableSteve:1,ChooseKateWorkwear:14,NotSuitableKate:15,Instructions:28,GameMenu:34,GameQuit:35,GamePlay:36,Boss:42,NotEnoughForBadge:49,AlreadyGotBadge:50,EarnedBadge:51,CounterLeaderboard:70});

	// timeline functions:
	this.frame_5 = function() {
		this.stop();
	}
	this.frame_14 = function() {
		this.stop();
	}
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_32 = function() {
		this.stop();
	}
	this.frame_34 = function() {
		this.stop();
	}
	this.frame_35 = function() {
		this.stop();
	}
	this.frame_36 = function() {
		this.stop();
	}
	this.frame_46 = function() {
		gameCounter.bossDoneShowing();
		this.stop();
	}
	this.frame_49 = function() {
		this.stop();
	}
	this.frame_50 = function() {
		this.stop();
	}
	this.frame_51 = function() {
		this.stop();
	}
	this.frame_59 = function() {
		this.stop();
	}
	this.frame_66 = function() {
		this.gotoAndStop("EarnedBadge");
	}
	this.frame_70 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(5).call(this.frame_5).wait(9).call(this.frame_14).wait(5).call(this.frame_19).wait(13).call(this.frame_32).wait(2).call(this.frame_34).wait(1).call(this.frame_35).wait(1).call(this.frame_36).wait(10).call(this.frame_46).wait(3).call(this.frame_49).wait(1).call(this.frame_50).wait(1).call(this.frame_51).wait(8).call(this.frame_59).wait(7).call(this.frame_66).wait(4).call(this.frame_70).wait(1));

	// Loader windmill
	this.mc_loader = new lib.WindmillLoader();
	this.mc_loader.setTransform(481.7,286,1,1,0,0,0,64.7,64.7);
	this.mc_loader.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.mc_loader).wait(71));

	// Close Game
	this.mc_ConfirmQuitGame = new lib.Sure_you_wanna_quit();
	this.mc_ConfirmQuitGame.setTransform(500.7,180.7,1,1,0,0,0,301.7,173.7);
	this.mc_ConfirmQuitGame.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.mc_ConfirmQuitGame).wait(71));

	// Menu
	this.mc_GameMenuOn = new lib.Options_btn();
	this.mc_GameMenuOn.setTransform(45,35.4,0.712,0.712,0,0,0,34.9,34.9);
	this.mc_GameMenuOn.visible = false;

	this.mc_MainMenu = new lib.OptionsMenu();
	this.mc_MainMenu.setTransform(139.6,185.5,1,1,0,0,0,139.6,185.5);
	this.mc_MainMenu.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);
	this.mc_MainMenu.visible = false;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_MainMenu},{t:this.mc_GameMenuOn}]}).wait(71));

	// game level title
	this.txt_GameLevel = new cjs.Text("LEVEL 1", "35px 'VAGRounded BT'", "#FFFFFF");
	this.txt_GameLevel.name = "txt_GameLevel";
	this.txt_GameLevel.lineHeight = 41;
	this.txt_GameLevel.lineWidth = 129;
	this.txt_GameLevel.setTransform(307,6,1.29,1.29);
	this.txt_GameLevel._off = true;

	this.timeline.addTween(cjs.Tween.get(this.txt_GameLevel).wait(34).to({_off:false},0).to({_off:true},15).wait(21).to({_off:false,x:497.7,y:210.8,text:"LEVEL 2",font:NaN},0).wait(1));

	// Layer 1
	this.mc_CloseGame = new lib.Arrow_Butn();
	this.mc_CloseGame.setTransform(114.9,35.4,0.712,0.712,0,0,0,34.9,34.9);

	this.txt_GameTitle = new cjs.Text("THE COUNTER", "38px 'VAGRounded BT'", "#FFFFFF");
	this.txt_GameTitle.name = "txt_GameTitle";
	this.txt_GameTitle.textAlign = "center";
	this.txt_GameTitle.lineHeight = 44;
	this.txt_GameTitle.lineWidth = 654;
	this.txt_GameTitle.setTransform(474,5.5,1.29,1.29);

	this.mc_Timer = new lib.Time();
	this.mc_Timer.setTransform(749.9,36.1,1,0.789,0,0,0,91.1,33.5);
	this.mc_Timer.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);

	this.mc_Scoring = new lib.Score();
	this.mc_Scoring.setTransform(571.1,36.1,1,0.789,0,0,0,91.1,33.5);
	this.mc_Scoring.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);

	this.mc_ReplayGame = new lib.replaybtn();
	this.mc_ReplayGame.setTransform(114.9,35.4,0.712,0.712,0,0,0,34.9,34.9);

	this.instance = new lib.BackArrow();
	this.instance.setTransform(114.9,35.4,0.712,0.712,0,0,0,34.9,34.9);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);
	new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.BackArrow(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.txt_GameTitle,p:{x:474,y:5.5,text:"THE COUNTER",font:"38px 'VAGRounded BT'",lineHeight:44,lineWidth:654}},{t:this.mc_CloseGame}]},28).to({state:[{t:this.mc_Scoring},{t:this.mc_Timer}]},6).to({state:[{t:this.txt_GameTitle,p:{x:469.4,y:7.8,text:"TIME'S UP!",font:"37px 'VAGRounded BT'",lineHeight:43,lineWidth:593}},{t:this.mc_ReplayGame}]},15).to({state:[{t:this.instance},{t:this.txt_GameTitle,p:{x:471.5,y:16.3,text:"THE COUNTER LEADERBOARD",font:"27px 'VAGRounded'",lineHeight:33,lineWidth:593}}]},21).wait(1));

	// Layer 13
	this.instance_1 = new lib.Health_and_Safety_Bubble();
	this.instance_1.setTransform(599,406.2,1,1,0,0,0,284.2,102.7);

	this.mc_Boss = new lib.BossMC();
	this.mc_Boss.setTransform(455.4,789.3,1,1,0,0,0,392.1,178.5);
	this.mc_Boss._off = true;

	this.instance_2 = new lib.NewMessage();
	this.instance_2.setTransform(482.2,477.7,1.52,1.52,0,0,0,103,21.5);
	this.instance_2.shadow = new cjs.Shadow("rgba(255,255,255,1)",0,0,10);

	this.instance_3 = new lib.NewMessageStatic();
	this.instance_3.setTransform(482.2,477.7,1.52,1.52,0,0,0,103,21.5);
	this.instance_3.shadow = new cjs.Shadow("rgba(255,255,255,1)",0,0,10);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_1}]},41).to({state:[{t:this.mc_Boss}]},1).to({state:[{t:this.mc_Boss}]},4).to({state:[]},2).to({state:[{t:this.instance_2}]},4).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},6).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},6).to({state:[]},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.mc_Boss).wait(42).to({_off:false},0).to({y:385.2},4).to({_off:true},2).wait(23));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(53).to({_off:false},0).to({y:103.9},6).wait(1).to({y:577.7},6).to({_off:true},1).wait(4));

	// Layer 11
	this.mc_Instructions = new lib.Instructions();
	this.mc_Instructions.setTransform(483.2,266,0.17,0.17,0,0,0,201.5,128.6);
	this.mc_Instructions._off = true;

	this.mc_HealthAndSafetyLadyInit = new lib.Health_and_Safety_Advisor();
	this.mc_HealthAndSafetyLadyInit.setTransform(924.5,514.9,1,1,0,0,0,90,90.1);

	this.mc_WinnersCup = new lib.WinnersCupMC();
	this.mc_WinnersCup.setTransform(488.1,646.5,1,1,0,0,0,488.1,271.5);
	this.mc_WinnersCup.alpha = 0;
	this.mc_WinnersCup._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.mc_Instructions}]},28).to({state:[{t:this.mc_Instructions}]},4).to({state:[{t:this.mc_HealthAndSafetyLadyInit}]},2).to({state:[]},15).to({state:[{t:this.mc_WinnersCup}]},4).to({state:[{t:this.mc_WinnersCup}]},2).to({state:[{t:this.mc_WinnersCup}]},4).to({state:[{t:this.mc_WinnersCup}]},1).to({state:[{t:this.mc_WinnersCup}]},6).to({state:[]},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.mc_Instructions).wait(28).to({_off:false},0).to({regX:201.4,regY:128.4,scaleX:1,scaleY:1,y:265.9},4).to({_off:true},2).wait(37));
	this.timeline.addTween(cjs.Tween.get(this.mc_WinnersCup).wait(53).to({_off:false},0).to({y:521.5,alpha:1},2).to({y:271.5},4).wait(1).to({y:746.5},6).to({_off:true},1).wait(4));

	// Layer 6
	this.mc_GameMenu = new lib.Options_btn();
	this.mc_GameMenu.setTransform(45,35.4,0.712,0.712,0,0,0,34.9,34.9);
	this.mc_GameMenu._off = true;

	this.timeline.addTween(cjs.Tween.get(this.mc_GameMenu).wait(28).to({_off:false},0).wait(43));

	// Layer 10
	this.mc_CustomerBubble = new lib.CustomerSpeechBubb();
	this.mc_CustomerBubble.setTransform(435.6,435.9,1,1,0,0,0,163.5,75.5);
	this.mc_CustomerBubble.visible = false;

	this.mc_DidntGetBadge = new lib.DidntGetBadge();
	this.mc_DidntGetBadge.setTransform(488.1,271.5,1,1,0,0,0,488.1,271.5);

	this.mc_BadgeAlreadyEarned = new lib.CounterBadgeAlreadyGot();
	this.mc_BadgeAlreadyEarned.setTransform(488.1,271.5,1,1,0,0,0,488.1,271.5);

	this.mc_BadgeEarned = new lib.CounterBadge();
	this.mc_BadgeEarned.setTransform(488.1,271.5,1,1,0,0,0,488.1,271.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.mc_CustomerBubble}]},36).to({state:[]},1).to({state:[{t:this.mc_DidntGetBadge}]},12).to({state:[{t:this.mc_BadgeAlreadyEarned}]},1).to({state:[{t:this.mc_BadgeEarned}]},1).to({state:[]},19).wait(1));

	// Layer 5
	this.mc_QuitGame = new lib.Arrow_Butncopy();
	this.mc_QuitGame.setTransform(916.3,37.4,0.712,0.712,0,0,180,34.9,34.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#4F3F5B","#4F2A40"],[0,1],0,-35,0,35.1).s().p("EhLxAF3IAArtMCXjAAAIAALtg");
	this.shape.setTransform(480.5,36.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape},{t:this.mc_QuitGame}]},28).wait(43));

	// Layer 2
	this.mc_CounterReply = new lib.SpeechBubbcopy();
	this.mc_CounterReply.setTransform(409.7,93.7);
	this.mc_CounterReply.visible = false;

	this.instance_4 = new lib.PostRoomLeaderBoard();
	this.instance_4.setTransform(477.8,308.3,1,1,0,0,0,393.4,204.2);
	this.instance_4.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.mc_CounterReply}]},36).to({state:[]},5).to({state:[{t:this.instance_4}]},29).wait(1));

	// Layer 4
	this.mc_ladyBossMC = new lib.LadyBossMC2();
	this.mc_ladyBossMC.setTransform(600.2,684.5,1,1,0,0,0,493.3,84.7);
	this.mc_ladyBossMC._off = true;

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.502)").s().p("EhMQAqaMAAAhU0MCYhAAAMAAABU0g");
	this.shape_1.setTransform(486.9,271.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.mc_ladyBossMC}]},1).to({state:[{t:this.mc_ladyBossMC}]},4).to({state:[]},9).to({state:[{t:this.mc_ladyBossMC}]},1).to({state:[{t:this.mc_ladyBossMC}]},4).to({state:[]},9).to({state:[{t:this.shape_1}]},21).wait(22));
	this.timeline.addTween(cjs.Tween.get(this.mc_ladyBossMC).wait(1).to({_off:false},0).to({y:284.5},4).to({_off:true},9).wait(1).to({_off:false,y:684.5},0).to({y:284.5},4).to({_off:true},9).wait(43));

	// counter
	this.instance_5 = new lib.WhiteCover();
	this.instance_5.setTransform(650.2,298.4,1,1,0,0,0,296.1,221.1);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.instance_6 = new lib.PhoneMain("single",0);
	this.instance_6.setTransform(297.5,291.7,1,1,0,0,0,50.1,36);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_5}]},4).to({state:[]},9).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_5}]},4).to({state:[]},9).to({state:[{t:this.instance_6}]},21).wait(22));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1).to({_off:false},0).to({alpha:1},4).to({_off:true},9).wait(1).to({_off:false,y:307.2,alpha:0},0).to({alpha:1},4).to({_off:true},9).wait(43));

	// Main Avatar
	this.mc_MainAvatar = new lib.Avatars();
	this.mc_MainAvatar.setTransform(272.4,227.8,1,1,0,0,0,84.4,144.8);

	this.timeline.addTween(cjs.Tween.get(this.mc_MainAvatar).wait(71));

	// Customer
	this.mc_Customer = new lib.Customers();
	this.mc_Customer.setTransform(925,313);

	this.timeline.addTween(cjs.Tween.get(this.mc_Customer).wait(71));

	// Layer 14
	this.mc_Steve_Shirt_1 = new lib.YardSteve_03();
	this.mc_Steve_Shirt_1.setTransform(509.5,222.8,1,1,0,0,0,84.4,77.7);

	this.instance_7 = new lib.HardHatIsolated();
	this.instance_7.setTransform(502,138.3,1.46,1.46,0,0,0,43.1,29.6);

	this.mc_FrankOptions = new lib.Frank_options();
	this.mc_FrankOptions.setTransform(187.5,299.3,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_JasonOptions = new lib.Jason_options();
	this.mc_JasonOptions.setTransform(187.5,302.3,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_GregOptions = new lib.Greg_options();
	this.mc_GregOptions.setTransform(188.3,302.3,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_SteveOptions = new lib.Steve_options();
	this.mc_SteveOptions.setTransform(187.5,302.3,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_FrankOptions_1 = new lib.Frank_options();
	this.mc_FrankOptions_1.setTransform(188.3,302.3,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_JasonOptions_1 = new lib.Jason_options();
	this.mc_JasonOptions_1.setTransform(188.3,301.3,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_GregOptions_1 = new lib.Greg_options();
	this.mc_GregOptions_1.setTransform(187.5,302.3,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_SteveOptions_1 = new lib.Steve_options();
	this.mc_SteveOptions_1.setTransform(187.5,302.3,1.14,1.14,0,0,0,84.4,143.7);

	this.mc_Steve_Shirt_4 = new lib.shirt03();
	this.mc_Steve_Shirt_4.setTransform(785.5,409.8,1.14,1.14,0,0,0,84.4,77.7);

	this.mc_Steve_Shirt_2 = new lib.shirt2();
	this.mc_Steve_Shirt_2.setTransform(779,214,1.14,1.14,0,0,0,84.4,77.7);

	this.mc_Steve_Shirt_3 = new lib.shirt();
	this.mc_Steve_Shirt_3.setTransform(531,408.2,1.14,1.14,0,0,0,84.4,77.6);

	this.mc_PeppaOptions = new lib.PeppaOptions();
	this.mc_PeppaOptions.setTransform(175.8,299.9,1,1,0,0,0,85.8,178.5);

	this.mc_LucyOptions = new lib.LucyOptions();
	this.mc_LucyOptions.setTransform(177.4,301,1,1,0,0,0,85.8,178.5);

	this.mc_JaneOptions = new lib.JaneOptions();
	this.mc_JaneOptions.setTransform(175.8,301,1,1,0,0,0,85.8,178.5);

	this.mc_Kate_Suit_1 = new lib.FemaleHoodie();
	this.mc_Kate_Suit_1.setTransform(525.8,215.5,1,1,0,0,0,78.9,83.9);

	this.mc_KateOptions = new lib.KateOptions();
	this.mc_KateOptions.setTransform(175.8,301,1,1,0,0,0,85.8,178.5);

	this.mc_Kate_Suit_2 = new lib.GirlOutfit04();
	this.mc_Kate_Suit_2.setTransform(768.7,215.8,1.12,1.12,0,0,0,62.6,83.9);

	this.mc_Kate_Suit_3 = new lib.GirlOutfit01();
	this.mc_Kate_Suit_3.setTransform(529.4,416.7,1.12,1.12,0,0,0,71.2,81);

	this.mc_Kate_Suit_4 = new lib.Girl03();
	this.mc_Kate_Suit_4.setTransform(765.9,415.8,1.12,1.12,0,0,0,66.5,82);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_Steve_Shirt_3},{t:this.mc_Steve_Shirt_2},{t:this.mc_Steve_Shirt_4},{t:this.mc_SteveOptions_1},{t:this.mc_GregOptions_1},{t:this.mc_JasonOptions_1},{t:this.mc_FrankOptions_1},{t:this.mc_SteveOptions},{t:this.mc_GregOptions},{t:this.mc_JasonOptions},{t:this.mc_FrankOptions},{t:this.instance_7},{t:this.mc_Steve_Shirt_1}]}).to({state:[{t:this.mc_Kate_Suit_4},{t:this.mc_Kate_Suit_3},{t:this.mc_Kate_Suit_2},{t:this.mc_KateOptions},{t:this.mc_Kate_Suit_1},{t:this.mc_JaneOptions},{t:this.mc_LucyOptions},{t:this.mc_PeppaOptions}]},14).to({state:[]},14).wait(43));

	// guy
	this.instance_8 = new lib.BGBox();
	this.instance_8.setTransform(199.9,226.3,0.687,2.517,0,0,0,543.5,600.5);
	this.instance_8.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,4);

	this.instance_9 = new lib.DrillBoxcopy2();
	this.instance_9.setTransform(859.8,150.5,1,1,0,0,-1.8,43.8,40.4);

	this.instance_10 = new lib.DrillBox();
	this.instance_10.setTransform(829.6,208.9,1,1,0,0,-1.8,43.8,40.3);

	this.instance_11 = new lib.DrillBoxcopy();
	this.instance_11.setTransform(894.8,201.8,1.001,1,0,0,-3,43.8,40.3);

	this.instance_12 = new lib.PaintThree();
	this.instance_12.setTransform(103.4,209.9,1,1,0,0,0,39.6,50.2);

	this.instance_13 = new lib.PaintOne();
	this.instance_13.setTransform(72.4,292.3,1,1,0,0,0,39.6,50.2);

	this.instance_14 = new lib.PaintTwo();
	this.instance_14.setTransform(138.5,285.5,1,1,0,0,0,39.6,50.2);

	this.instance_15 = new lib.ComputerScreen();
	this.instance_15.setTransform(523.5,218.1,1,1,0,0,0,72.3,76);

	this.instance_16 = new lib.Counter();
	this.instance_16.setTransform(511.7,377.8,1,1,0,0,0,539.9,185.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_8}]}).to({state:[{t:this.instance_16},{t:this.instance_15},{t:this.instance_14},{t:this.instance_13},{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9}]},28).wait(43));

	// wall
	this.mc_Highlighter = new lib.Highlighter();
	this.mc_Highlighter.setTransform(848,36,1,1,0,0,0,24.8,24.8);

	this.text = new cjs.Text("CHOOSE APPROPRIATE WORKWEAR", "27px 'VAGRounded BT'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 33;
	this.text.lineWidth = 654;
	this.text.setTransform(474,15.7,1.29,1.29);

	this.mc_QuitGame_1 = new lib.Arrow_Butn();
	this.mc_QuitGame_1.setTransform(114.9,35.4,0.712,0.712,0,0,0,34.9,34.9);

	this.mc_QuitGame_2 = new lib.Arrow_Butncopy();
	this.mc_QuitGame_2.setTransform(916.3,37.4,0.712,0.712,0,0,180,34.9,34.9);

	this.mc_EnterGame = new lib.Arrow_Butncopy2();
	this.mc_EnterGame.setTransform(847,37.4,0.712,0.712,0,0,180,34.9,34.9);

	this.mc_GameMenu_1 = new lib.Options_btn();
	this.mc_GameMenu_1.setTransform(45,35.4,0.712,0.712,0,0,0,34.9,34.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#4F3F5B","#4F2A40"],[0,1],0,-35,0,35.1).s().p("EhLxAF3IAArtMCXjAAAIAALtg");
	this.shape_2.setTransform(480.5,36.3);

	this.instance_17 = new lib.Trowl();
	this.instance_17.setTransform(193.8,161.7,1,1,0,0,0,28.3,59.9);

	this.instance_18 = new lib.Paintbrush();
	this.instance_18.setTransform(132.2,167.4,1.007,1,0,0,-7,27.4,74.4);

	this.instance_19 = new lib.Roller();
	this.instance_19.setTransform(71,164.6,0.939,0.939,0,0,0,42.8,68.2);

	this.instance_20 = new lib.Wall();
	this.instance_20.setTransform(472.4,348.2,1,1,0,0,0,488.3,291.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.mc_GameMenu_1},{t:this.mc_EnterGame},{t:this.mc_QuitGame_2},{t:this.mc_QuitGame_1},{t:this.text},{t:this.mc_Highlighter}]}).to({state:[{t:this.instance_20},{t:this.instance_19},{t:this.instance_18},{t:this.instance_17}]},28).wait(43));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(235.3,80.8,1449.4,866.9);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;