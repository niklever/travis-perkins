<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends Model{	
	var $id;
	var $category_id;
	var $name;
	var $price;
	var $price_text;
	var $price_l;
	var $discount;
	var $promotion_price;
	var $promotion_price_l;
	var $promotion_end_date;
	var $promotion_text;
	var $promo_code;
	var $dis_promo_code;
	var $image;
	var $description;
	var $weight_size;
	var $serving_recommended;
	var $start_date;
	var $expire_date;
	var $reheating_methods;
	var $cooking_method;
	var $workingday;
	var $cut_time;
	var $published;
	var $created;
	var $modified;
	var $uid;
	
	function Product_model(){
		parent::Model();
	}
	
	function get_product($id){
		$this->db->select('p.*,cate.name as category_name');
		$this->db->join('category cate', 'p.category_id = cate.id', 'left');
		$this->db->where('p.id', $id);
		$user = getUser();
		if($user['role'] != "0")
			$this->db->where('p.uid', $user['uid']);
			
		$rs = $this->db->get('products p');
		
		if($rs){
			$row = $rs->row();
			return $row;
		}
		
		return null;
	}

	function insert_product(){
 		$data = array( 'name' => $this->name,
						'category_id' => $this->category_id,
						'price' => $this->price,
						'price_text' => $this->price_text,
						'price_l' => $this->price_l,
						'discount' => $this->discount,
						'promotion_price' => $this->promotion_price,
						'promotion_price_l' => $this->promotion_price_l,
						'promotion_end_date' => $this->promotion_end_date,
						'promotion_text' => $this->promotion_text,
						'promo_code' => $this->promo_code,
						'dis_promo_code' => $this->dis_promo_code,
 						'image' => $this->image,
 						'description' => $this->description,
						'weight_size' => $this->weight_size,
						'serving_recommended' => $this->serving_recommended,
						'start_date' => $this->start_date,
						'expire_date' => $this->expire_date,
						'reheating_methods' => $this->reheating_methods,
						'cooking_method' => $this->cooking_method,
						'published' => $this->published,
						'text_color' => $this->text_color,
						'workingday' => $this->workingday,
						'cut_time' => $this->cut_time,
						'created' => $this->created,
				 		'modified' => $this->modified,
						'uid' => $this->uid
 					   );
			           
		$this->db->insert('products', $data); 
		
		return $this->db->insert_id();
 	}
 	
 	function update_product($id, $hp){
		$user = getUser();
		if($user['role'] != "0")
			$this->db->where('uid', $user['uid']);		
		$this->db->where('id', $id);
		return $this->db->update('products', $hp);
	}
	
	function delete_product($id){
		$user = getUser();
		$where = "";
		if($user['role'] != "0")
			$where = " AND uid=".$user['uid'];
		$this->db->query("DELETE FROM products WHERE id = '$id'".$where);
		return true; 
		
	}
	function update_product_published($id,$status){
		$this->db->query("update products set published = '$status'  WHERE id='$id'");
		return true; 
		
	}
	
	function update_product_soldout($id,$status){
		$this->db->query("update products set sold_out = '$status'  WHERE id='$id'");
		return true; 
	}
	
	function update_product_image($id){
		$this->db->query("update products set image = ''  WHERE id='$id'");
		return true; 
	}
	
    
	function count_products($params){
		$category_id = (isset($params['category_id']) && $params['category_id']) ? $params['category_id'] : null;
	
		$this->db->select('p.id');
		
		if($category_id)
		   $this->db->where('p.category_id', $category_id);
		   
		$user = getUser();
		if($user['role'] != "0")
			$this->db->where('p.uid', $user['uid']);
		  		
		$rs = $this->db->get('products p');
		
		if ($rs){
			return $rs->num_rows();
		}
		
		return null;
	}
	
	function get_products($params){
		$category_id = (isset($params['category_id']) && $params['category_id']) ? $params['category_id'] : null;
		$limit = (isset($params['limit']) && $params['limit']) ? $params['limit'] :null;
 		$start = (isset($params['start']) && $params['start']) ? $params['start'] : 0;
		$published = (isset($params['published'])) ? $params['published'] : null;
 		
		$this->db->select('p.*,cate.name as category_name,u.fullname');
		
		$this->db->join('user u', 'p.uid = u.id', 'left');
		$this->db->join('category cate', 'p.category_id = cate.id', 'left');
		
		if($category_id)
		  $this->db->where('p.category_id', $category_id);
		  
		if($published)
		  $this->db->where('p.published', $published);
		  
		$user = getUser();
		if($user['role'] != "0")
			$this->db->where('p.uid', $user['uid']);

		//$this->db->order_by('p.order_num', 'asc');
  		$this->db->order_by('p.id', 'asc'); 

		if($limit )
		  $this->db->limit($limit, $start);

		$rs = $this->db->get('products p');
		if ($rs){
			return $rs->result();
		}
		
		return null;
	}	
	
	function get_all_products($params){
		$category_id = (isset($params['category_id']) && $params['category_id']) ? $params['category_id'] : null;
		$limit = (isset($params['limit']) && $params['limit']) ? $params['limit'] :null;
 		$start = (isset($params['start']) && $params['start']) ? $params['start'] : 0;
		$published = (isset($params['published'])) ? $params['published'] : null;
 		
		$this->db->select('p.*,cate.name as category_name');
		
		$this->db->join('category cate', 'p.category_id = cate.id', 'left');
		if($category_id)
		  $this->db->where('p.category_id', $category_id);
		  
		if(!$published)
		  $this->db->where('p.published', $published);

		$this->db->order_by('p.order_num', 'asc');
  		$this->db->order_by('p.id', 'asc');  

		if($limit )
		  $this->db->limit($limit, $start);

		$rs = $this->db->get('products p');
		if ($rs){
			return $rs->result();
		}
		
		return null;
	}
	
	function count_all_products($params){
		$category_id = (isset($params['category_id']) && $params['category_id']) ? $params['category_id'] : null;
		$published = (isset($params['published'])) ? $params['published'] : null;
		$this->db->select('p.id');
		
		if($category_id)
		   $this->db->where('p.category_id', $category_id);
		   
		if(!$published)
		  $this->db->where('p.published', $published);
		  		
		$rs = $this->db->get('products p');
		
		if ($rs){
			return $rs->num_rows();
		}
		
		return null;
	}
	
	function get_products_list_id($ids){		
		$this->db->select('*');
		$this->db->where('id in ('.$ids.')');
		$rs = $this->db->get('products');
		if ($rs){
			return $rs->result();
		}
		return null;
	}	
	
	
}
?>