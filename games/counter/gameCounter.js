var mc_CustomerBubble_DefaultHeight = 219;

var sprites = new Object();
sprites.avatar = null;
sprites.mc_LadyBoss = null;
sprites.loaded = false;
sprites.lbloaded = false;
sprites.customers1 = {mc: null, loaded : false, name : "", startWalkPos: {x:320.5, y: 92.35}, endWalkPos: {x: -330.25, y: -90.85}, walkOutPos: {x: 150.25, y: -106.85}};
sprites.customers2 = {mc: null, loaded : false, name : "", startWalkPos: {x:320.5, y: 92.35}, endWalkPos: {x: -330.25, y: -90.85}, walkOutPos: {x: 150.25, y: -106.85}};
sprites.customers3 = {mc: null, loaded : false, name : "", startWalkPos: {x:320.5, y: 92.35}, endWalkPos: {x: -330.25, y: -90.85}, walkOutPos: {x: 150.25, y: -106.85}};
sprites.customers4 = {mc: null, loaded : false, name : "", startWalkPos: {x:320.5, y: 92.35}, endWalkPos: {x: -330.25, y: -90.85}, walkOutPos: {x: 150.25, y: -106.85}};

function initLadyBoss(){
    
    if (sprites.mc_LadyBoss == null) {
        loadLadyBoss();
    }

    setTimeout(checkLBSpriteLoaded, 200);
}

function checkLBSpriteLoaded(){
    if (sprites.lbloaded) {

    }else{
        setTimeout(checkLBSpriteLoaded, 200);    
    }
    
}


function initPerson(name, gender){

    console.log("init person " + name);

    gameCounter.avatarName = name;
    exportRoot.mc_MainAvatar.visible = false;

    showSprite(name, gender);

    setTimeout(checkSpriteLoaded, 200);
}

function showSprite(name, gender){  
    if (sprites.avatar==null){
        loadMainAvatar(name, gender);
    }
        
    if (sprites.loaded && sprites.customers1.loaded && sprites.customers2.loaded && sprites.customers3.loaded && sprites.customers4.loaded) {
        sprites.avatar.visible = true;
        gameCounter.allSpritesLoaded();
    } 
}


function checkSpriteLoaded(){
    //dbLog("checkSpriteLoaded");
    // console.log("checkSpriteLoaded " + gameCounter.avatarName + " : " + sprites.loaded);
    if (sprites.loaded && sprites.customers1.loaded && sprites.customers2.loaded && sprites.customers3.loaded && sprites.customers4.loaded){
        exportRoot.mc_MainAvatar.visible = true;
        console.log("exportRoot.mc_MainAvatar " + exportRoot.mc_MainAvatar);
        gameCounter.allSpritesLoaded();
    }else{
        setTimeout(checkSpriteLoaded, 200);
    }
}


function initCustomer(name){

    console.log("init customer " + name);

    exportRoot.mc_Customer.visible = false;

    showCustomerSprite(name);


    // if (!sprites.loaded){
    //     setTimeout(checkCustomerLoaded, 200);
    // }else{
        
    // }
    switch(name) {
        case "Customer01":
            loadCustomer1();
            if (!sprites.customers1.loaded)setTimeout(checkCustomerLoaded, 200);
            break;
        case "Customer02":
            loadCustomer2();
            if (!sprites.customers2.loaded)setTimeout(checkCustomerLoaded, 200);
            break;
        case "Customer03":
            loadCustomer3();
            if (!sprites.customers3.loaded)setTimeout(checkCustomerLoaded, 200);
            break;
        case "Customer04":
            loadCustomer4();
            if (!sprites.customers4.loaded)setTimeout(checkCustomerLoaded, 200);
            break;
    }
}

function showCustomerSprite(name){  
    switch(name) {
        case "Customer01":
            loadCustomer1();
            if (gameCounter.customerIsPlaying && sprites.loaded && sprites.customers1.loaded && sprites.customers2.loaded && sprites.customers3.loaded && sprites.customers4.loaded) {
                sprites.customers1.mc.visible = true;
                gameCounter.allSpritesLoaded();
            }
            break;
        case "Customer02":
            loadCustomer2();
            if (gameCounter.customerIsPlaying && sprites.loaded && sprites.customers1.loaded && sprites.customers2.loaded && sprites.customers3.loaded && sprites.customers4.loaded) {
                sprites.customers2.mc.visible = true;
                gameCounter.allSpritesLoaded();
            }
            break;
        case "Customer03":
            loadCustomer3();
            if (gameCounter.customerIsPlaying && sprites.loaded && sprites.customers1.loaded && sprites.customers2.loaded && sprites.customers3.loaded && sprites.customers4.loaded){
                sprites.customers3.mc.visible = true;
                gameCounter.allSpritesLoaded();
            } 
            break;
        case "Customer04":
            loadCustomer4();
            if (gameCounter.customerIsPlaying && sprites.loaded && sprites.customers1.loaded && sprites.customers2.loaded && sprites.customers3.loaded && sprites.customers4.loaded){
                sprites.customers4.mc.visible = true;
                gameCounter.allSpritesLoaded();
            } 
            break;
    }
}


function checkCustomerLoaded(){
    //dbLog("checkSpriteLoaded");
    // console.log(gameCounter.avatarName + " : " + sprites.loaded);
    if (gameCounter.customerIsPlaying && sprites.loaded && sprites.customers1.loaded && sprites.customers2.loaded && sprites.customers3.loaded && sprites.customers4.loaded) {
        exportRoot.mc_Customer.visible = true;
        gameCounter.allSpritesLoaded();
    }else{
        setTimeout(checkCustomerLoaded, 200);
    }
}


var gameCounter = {
    GAME_NAME : "The Counter",
    avatarMode: "male",//male , female
    GAME_ID : "1",
    GAME_NAME : "counter",
    SOUND_ENABLED : true,
    gameLoaded : false,
    selectedWorkwear : 0,//0, 1: Steve, 2,3: Kate
    gamePlaying : false,
    gamePause : false,
    currentScore : 0,
    gameRemainingTime : 59, // in second
    MAX_PLAY_TIME : 59,
    SCORE_INC_STEP : 500,
    mc_currentAvatar : null,
    mc_currentCustomer : null,
    avatarName : null,
    customerIsPlaying : false,
    playingCustomer : 0,
    currentQuestionNo : 0, //current question number
    wrongAnswer : false,
    currentQuestion : null,
    wrongText : [],
    questions : [],
    currentLevel : 1,
    playingAvatar: null,
    newCustomerAvailable: true,
    init: function (gmode) {
        exportRoot.mc_JasonOptions.visible = false;
        exportRoot.mc_FrankOptions.visible = false;
        exportRoot.mc_GregOptions.visible = false;
        exportRoot.mc_SteveOptions.visible = false;
        exportRoot.mc_KateOptions.visible = false;
        exportRoot.mc_PeppaOptions.visible = false;
        exportRoot.mc_LucyOptions.visible = false;
        exportRoot.mc_JaneOptions.visible = false;

        console.log("storageIDAvatar " + $.localStorage.get("storageIDAvatar"));
        if ($.localStorage.get("storageIDAvatar") == 1 || $.localStorage.get("storageIDAvatar") == 5 || $.localStorage.get("storageIDAvatar") == 2 || $.localStorage.get("storageIDAvatar") == 6) {
            gameCounter.avatarMode = "male";    
        }else{
            gameCounter.avatarMode = "female";    
        }

        //gameCounter.avatarMode = gmode;
        gameCounter.doUpdateAvatar($.localStorage.get("storageIDAvatar"));
        sManager.playBGMusic(true);
        this.doInitSelectWorkwear();
        //exportRoot.mc_Timer.mc_TimerCounter.text = "01:45";
        exportRoot.mc_ladyBossMC.mc_GotItBtn.addEventListener("click", this.doGotItWrongSteveWear); 

        this.addClickHandleGameToolbars();

        gameCounter.enableStartGame(false);
        apiHelper.init(gameCounter.GAME_ID);  
        gameCounter.enableCursorType();  


        //load lady boss select work ware
        //mc_LadyBossWrongWorkware
        initLadyBoss();

        exportRoot.mc_Customer.visible = false;

        exportRoot.mc_ConfirmQuitGame.mc_bg.addEventListener ("click" , function () {

        });

        exportRoot.mc_MainMenu.mc_bg.addEventListener ("click" , function () {

        });

        exportRoot.mc_loader.mc_bg.addEventListener ("click" , function () {

        });

        gameCounter.fixBrowserCompatible();
        exportRoot.mc_Scoring.mc_Scoring.visible = false;   
        gameCounter.currentLevel = 1;
        exportRoot.mc_Timer.visible = false;
        exportRoot.txt_GameLevel.visible = false;
        gameCounter.questions = gameQuestions;
    },
    resetGameTimer: function () {
        exportRoot.mc_Timer.visible = true;
        exportRoot.mc_Timer.mc_TimerCounter.text = "00:59";
        // gameCounter.gameRemainingTime = gameCounter.MAX_PLAY_TIME;
        // gameTimer.seconds = gameCounter.MAX_PLAY_TIME;
        gameCounter.startGameTimer();
    },
    updateGameLevel: function (lvl) {
        exportRoot.txt_GameLevel.visible = true;
		exportRoot.txt_GameLevel.text = "LEVEL " + lvl;
    },
    enableCursorType : function () {
        exportRoot.mc_GameMenu.cursor = "pointer";
        exportRoot.mc_GameMenu_1.cursor = "pointer";
        
        exportRoot.mc_EnterGame.cursor = "pointer";
        exportRoot.mc_QuitGame.cursor = "pointer";
        exportRoot.mc_QuitGame_1.cursor = "pointer";
        exportRoot.mc_QuitGame_2.cursor = "pointer";
        
        exportRoot.mc_Instructions.mc_InstructionGotIt.cursor = "pointer";
        exportRoot.mc_Boss.mc_BossBubble.mc_BossGotItBtn.cursor = "pointer";
        
        exportRoot.mc_CloseGame.cursor = "pointer";
        exportRoot.mc_ReplayGame.cursor = "pointer";

        //reply and answers
        exportRoot.mc_CounterReply.cursor = "pointer";

    },
    enableStartGame: function (enabled) {
        exportRoot.mc_EnterGame.visible = enabled;
        exportRoot.mc_Highlighter.visible = enabled;
        if (enabled) {
            exportRoot.mc_Highlighter.gotoAndPlay(0);
        }
    },
    doUpdateAvatar: function () {
        if ($.localStorage.get("storageIDAvatar") == 1) {
            exportRoot.mc_SteveOptions.visible = true;
            gameCounter.playingAvatar = exportRoot.mc_SteveOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 2) {
            exportRoot.mc_GregOptions.visible = true;
            gameCounter.playingAvatar = exportRoot.mc_GregOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 5) {
            exportRoot.mc_JasonOptions.visible = true;
            gameCounter.playingAvatar = exportRoot.mc_JasonOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 6) {
            exportRoot.mc_FrankOptions.visible = true;
            gameCounter.playingAvatar = exportRoot.mc_FrankOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 3){
            //jane
            exportRoot.mc_JaneOptions.visible = true;
            gameCounter.playingAvatar = exportRoot.mc_JaneOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 4){
            //kate
            exportRoot.mc_KateOptions.visible = true;
            gameCounter.playingAvatar = exportRoot.mc_KateOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 7){
            //lucy
            exportRoot.mc_LucyOptions.visible = true;
            gameCounter.playingAvatar = exportRoot.mc_LucyOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 8){
            //jane
            exportRoot.mc_PeppaOptions.visible = true;
            gameCounter.playingAvatar = exportRoot.mc_PeppaOptions;
        }
        
    },
    addClickHandleGameToolbars : function () {
        exportRoot.mc_GameMenu.addEventListener("click", this.doOpenGameMenu);    
        exportRoot.mc_GameMenu_1.addEventListener("click", this.doOpenGameMenu);    
        
        exportRoot.mc_EnterGame.addEventListener("click", this.doStartGame);    
        exportRoot.mc_QuitGame.addEventListener("click", this.doQuitGame);      
        exportRoot.mc_QuitGame_1.addEventListener("click", this.doQuitGame);      
        exportRoot.mc_QuitGame_2.addEventListener("click", this.doQuitGame);      
        
        exportRoot.mc_Instructions.mc_InstructionGotIt.addEventListener("click", this.doStartGameAfterInstruction);    
        exportRoot.mc_Boss.mc_BossBubble.mc_BossGotItBtn.addEventListener("click", this.doGotItBossHandle); 

        //gameCounter.addClickHandleLetters();

        exportRoot.mc_CloseGame.addEventListener("click", this.doQuitGame);

        exportRoot.mc_ReplayGame.addEventListener("click", function (e) {
            console.log("replace game");
            apiHelper.replayGame();
        });
    },

    removeClickHandleAnswers : function () {
        exportRoot.mc_CounterReply.mc_A1.removeAllEventListeners();    
        exportRoot.mc_CounterReply.mc_A2.removeAllEventListeners();
        exportRoot.mc_CounterReply.mc_A3.removeAllEventListeners();

        exportRoot.mc_CounterReply.mc_B1.removeAllEventListeners();
        exportRoot.mc_CounterReply.mc_B2.removeAllEventListeners();
        exportRoot.mc_CounterReply.mc_B3.removeAllEventListeners();
        exportRoot.mc_CounterReply.mc_B4.removeAllEventListeners();

        exportRoot.mc_CounterReply.button5.removeAllEventListeners();
        exportRoot.mc_CounterReply.button2.removeAllEventListeners();
        exportRoot.mc_CounterReply.button3.removeAllEventListeners();
        exportRoot.mc_CounterReply.button4.removeAllEventListeners();
        exportRoot.mc_CounterReply.button1.removeAllEventListeners();

        exportRoot.mc_CounterReply.txt_l1_answer1.mouseEnabled = false;
        exportRoot.mc_CounterReply.txt_l1_answer2.mouseEnabled = false;
        exportRoot.mc_CounterReply.txt_l1_answer3.mouseEnabled = false;

        exportRoot.mc_CounterReply.txt_l2_answer1.mouseEnabled = false;
        exportRoot.mc_CounterReply.txt_l2_answer2.mouseEnabled = false;
        exportRoot.mc_CounterReply.txt_l2_answer3.mouseEnabled = false;
        exportRoot.mc_CounterReply.txt_l2_answer4.mouseEnabled = false;
    },
    addClickHandleAnswers : function () {

        exportRoot.mc_CounterReply.mc_A1.addEventListener("click", gameCounter.doAnswer1);    
        exportRoot.mc_CounterReply.mc_A2.addEventListener("click", gameCounter.doAnswer2);    
        exportRoot.mc_CounterReply.mc_A3.addEventListener("click", gameCounter.doAnswer3);    

        exportRoot.mc_CounterReply.mc_B1.addEventListener("click", gameCounter.doAnswer1);    
        exportRoot.mc_CounterReply.mc_B2.addEventListener("click", gameCounter.doAnswer2);    
        exportRoot.mc_CounterReply.mc_B3.addEventListener("click", gameCounter.doAnswer3);    
        exportRoot.mc_CounterReply.mc_B4.addEventListener("click", gameCounter.doAnswer4);    

        exportRoot.mc_CounterReply.button1.addEventListener("click", gameCounter.doAnswer1);    
        exportRoot.mc_CounterReply.button2.addEventListener("click", gameCounter.doAnswer2);    
        exportRoot.mc_CounterReply.button3.addEventListener("click", gameCounter.doAnswer3);    
        exportRoot.mc_CounterReply.button4.addEventListener("click", gameCounter.doAnswer4);    
        exportRoot.mc_CounterReply.button5.addEventListener("click", gameCounter.doAnswer5);
        
    },
    removeAnswerOptions : function () {
        gameCounter.answers = gameCounter.currentQuestion.answers;
        if (gameCounter.currentLevel == 1) {
            var flagCorrect = false, flagIncorect = false;
            while(1){
                var idx = chance.natural({min: 0, max: gameCounter.answers.length - 1});
                if (!flagCorrect && gameCounter.answers[idx].correct) {
                    gameCounter.answers.splice(idx,1);
                    flagCorrect = true;
                    continue;                        
                }
                if (!flagIncorect && !gameCounter.answers[idx].correct) {
                    gameCounter.answers.splice(idx,1);
                    flagIncorect = true;                        
                    continue;    
                }
                if (flagCorrect && flagIncorect) {
                    return;
                }
            }
        }else if (gameCounter.currentLevel == 2) {
            while(1){
                var idx = chance.natural({min: 0, max: gameCounter.answers.length - 1});
                if (gameCounter.answers[idx].correct == false) {
                    gameCounter.answers.splice(idx,1);
                    return;
                }    
            }
        }
        return;
    },
    doReply : function () {
        console.log("Do reply");
        
        exportRoot.mc_CounterReply.removeAllEventListeners();
        gameCounter.wrongAnswer = false;
        //hide customer bubble
        exportRoot.mc_CustomerBubble.visible = false;

        exportRoot.mc_CounterReply.txt_Reply_1.mouseEnabled = false;
        exportRoot.mc_CounterReply.txt_Reply_2.mouseEnabled = false;
        exportRoot.mc_CounterReply.txt_Reply_3.mouseEnabled = false;

        if (gameCounter.currentLevel == 1) {
            exportRoot.mc_CounterReply.gotoAndStop(1);
            exportRoot.mc_CounterReply.txt_l1_answer1.text = gameCounter.answers[0].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.txt_l1_answer1, 36, 91);
            exportRoot.mc_CounterReply.txt_l1_answer2.text = gameCounter.answers[1].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.txt_l1_answer2, 156, 91);
            exportRoot.mc_CounterReply.txt_l1_answer3.text = gameCounter.answers[2].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.txt_l1_answer3, 278, 91);
        }else if (gameCounter.currentLevel == 2) {
            exportRoot.mc_CounterReply.gotoAndStop(6);
            //gameCounter.removeAnswerOptions();
            exportRoot.mc_CounterReply.txt_l2_answer1.text = gameCounter.answers[0].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.txt_l2_answer1, 9, 55);
            exportRoot.mc_CounterReply.txt_l2_answer2.text = gameCounter.answers[1].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.txt_l2_answer2, 103, 55);
            exportRoot.mc_CounterReply.txt_l2_answer3.text = gameCounter.answers[2].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.txt_l2_answer3, 194, 55);
            exportRoot.mc_CounterReply.txt_l2_answer4.text = gameCounter.answers[3].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.txt_l2_answer4, 285, 55);
        }else if (gameCounter.currentLevel == 3) {
            exportRoot.mc_CounterReply.gotoAndStop(11);
            // gameCounter.removeAnswerOptions();
            exportRoot.mc_CounterReply.button1.small_txt.text = gameCounter.answers[0].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.button1.small_txt, 5, 60);
            exportRoot.mc_CounterReply.button2.small_txt.text = gameCounter.answers[1].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.button2.small_txt, 5, 60);
            exportRoot.mc_CounterReply.button3.small_txt.text = gameCounter.answers[2].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.button3.small_txt, 5, 60);
            exportRoot.mc_CounterReply.button4.small_txt.text = gameCounter.answers[3].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.button4.small_txt, 5, 60);
            exportRoot.mc_CounterReply.button5.small_txt.text = gameCounter.answers[4].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.button5.small_txt, 5, 60);
        }
    },
    doAnswer: function (answerNo) {
        gameCounter.removeClickHandleAnswers();
        gameCounter.answerNo = answerNo;

        console.log("play counter animation: " + gameCounter.answers[gameCounter.answerNo].avatar_animation);
        gameCounter.mc_currentAvatar.gotoAndPlay(gameCounter.answers[gameCounter.answerNo].avatar_animation);
        // gameCounter.mc_currentAvatar.gotoAndPlay("Talk");

        if (gameCounter.answers[gameCounter.answerNo].correct) {
            exportRoot.mc_CounterReply.gotoAndStop(3);
            exportRoot.mc_CounterReply.txt_l1_right.text = gameCounter.answers[answerNo].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.txt_l1_right, 24, 230);
            //inc score
            sManager.playRightChoose(false);
            gameCounter.incScore(gameCounter.SCORE_INC_STEP);
        }else{
            exportRoot.mc_CounterReply.gotoAndStop(2);
            exportRoot.mc_CounterReply.txt_l1_wrong.text = gameCounter.answers[answerNo].answer;
			gameCounter.verticallyCentreText(exportRoot.mc_CounterReply.txt_l1_wrong, 32, 184);
            sManager.playWrongChoose(false);
        }
    },
    doAnswer1: function () {
        console.log("doAnswer1");
        gameCounter.doAnswer(0);
    },
    doAnswer2 : function () {
        console.log("doAnswer2");
        gameCounter.doAnswer(1);
    },
    doAnswer3 : function () {
        console.log("doAnswer3");
        gameCounter.doAnswer(2);
    },
    doAnswer4 : function () {
        console.log("doAnswer4");
        gameCounter.doAnswer(3);
    },
    doAnswer5 : function () {
        console.log("doAnswer5");
        gameCounter.doAnswer(4);
    },
    counterDoneReply : function() {
        //customer walk out.
        exportRoot.mc_CounterReply.visible = false;
        
        exportRoot.mc_CustomerBubble.visible = true;
        exportRoot.mc_CustomerBubble.gotoAndStop(0);
        exportRoot.mc_CustomerBubble.txt_CustomerQuestion.text = gameCounter.answers[gameCounter.answerNo].customer_response;
		gameCounter.verticallyCentreText(exportRoot.mc_CustomerBubble.txt_CustomerQuestion, -88, 184);

        if (gameCounter.answers[gameCounter.answerNo].awayAFTER == false) {
            console.log("customer walk out before boss show");
            setTimeout(gameCounter.customerWalkOut, 1500);
            // gameCounter.customerWalkOut();
        }else{
            //show boss first
            console.log("show boss before customer walk out");
            setTimeout(gameCounter.showManager, 1500);
        }
        //exportRoot.mc_CustomerBubble.visible = false;
    },
    counterAnimationDone : function (name) {
        console.log("counterAnimationDone " + name);
        // setTimeout(gameCounter.updateCustomerQuestion, 10);
        setTimeout(gameCounter.counterDoneReply, 2000);

        
    },


    doStartGameAfterInstruction : function () {
        console.log("start game");

        gameCounter.currentScore = 0;

        exportRoot.gotoAndStop("GamePlay");

        gameCounter.gamePlaying = true;
        gameCounter.gamePause = false;
        gameCounter.customerIsPlaying = false;

        apiHelper.init_game_session(gameCounter.GAME_ID, gameCounter.GAME_NAME);    

        gameCounter.incScore(gameCounter.currentScore);
        gameCounter.fixBrowserCompatible();

        gameCounter.updateGameLevel(gameCounter.currentLevel);

        //gameCounter.startGameTimer();
    },
    startGameTimer: function() {
        console.log("start timer");
        gameTimer.seconds = gameCounter.MAX_PLAY_TIME;
        gameCounter.gameRemainingTime = gameCounter.MAX_PLAY_TIME;

        gameTimer.updateStatus = function(sec) {
            gameCounter.gameRemainingTime = sec;
                // if (!gameCounter.gamePlaying) {
                //     return;
                // }
            gameCounter.updateGameTimer(sec);
        };
        gameTimer.counterEnd = function() {
            if (gameCounter.currentLevel == 2) {
                console.log("GAME END LEVEL 2");
                //restart timer
                gameCounter.currentLevel = 3;
                gameCounter.updateGameLevel(3); 
                gameCounter.resetGameTimer();
            }else if (gameCounter.currentLevel == 3) {
                console.log("GAME OVER");
                gameCounter.gamePlaying = false;
                gameCounter.gamePause = true;
                gameCounter.gameOver();
            }
        };
        gameTimer.start();  
    },
    updateGameTimer : function (sec) {
        console.log(sec);
        // var firstNum = "";
        var nextNum = "";
        var m = ~~(sec / 60);
        var s = sec % 60;
        if (m > 9) {
            nextNum = "" + m + ":";
        }else{
            nextNum = "0" + m + ":";
        }
        if (s > 9) {
            nextNum = nextNum + s;
        }else{
            nextNum = nextNum + "0" + s;
        }
        exportRoot.mc_Timer.mc_TimerCounter.text = nextNum;

    },
    addLeadingZeros : function (n, length){
        var str = (n == 0 ? "" : n) + "";
        var zeros = "";
        for (var i = length - str.length; i > 0; i--)
            zeros += "0";
        return zeros;
    },
    incScore : function (score) {
        if (score > 0) {
            apiHelper.inc_game_score(gameCounter.GAME_ID, score);        
        }
        
        gameCounter.currentScore += score;
        exportRoot.mc_Scoring.mc_ScoringCounter.text = gameCounter.addLeadingZeros(gameCounter.currentScore, 6);
        if (gameCounter.currentScore == 0) {
            
            exportRoot.mc_Scoring.mc_Scoring.visible = false;   
        }else{
            exportRoot.mc_Scoring.mc_Scoring.text = gameCounter.currentScore;    
            exportRoot.mc_Scoring.mc_Scoring.visible = true;   
        }
    },
    gameOver : function () {

        gameCounter.removeClickHandleAnswers();

        // exportRoot.mc_HealtySafetyMessage.visible = true;

        gameCounter.gamePlaying = false;
        gameCounter.gamePause = true;
        exportRoot.txt_GameLevel.visible = false;
        exportRoot.txt_GameTitle.visible = false;
        
        gameTimer.stop();

        sManager.stopBackgroundMusic();


        apiHelper.game_over(gameCounter.GAME_ID, gameCounter.currentScore, function (data) {
            //handle badge info
            console.log(data);
            if (!data) {
                gameCounter.didNotEarnBadge();          
            }else if (data.status == "OK" && data.badge_info){
                if (data.badge_info.badge_status == 1) {
                    gameCounter.didNotEarnBadge();          
                }else if (data.badge_info.badge_status == 2) {
                    gameCounter.alreadyGotBadge();
                }else if (data.badge_info.badge_status == 3) {
                    gameCounter.earnedBadge();          
                }else if (data.badge_info.badge_status == 4) {
                    gameCounter.earnedAllBadge();          
                }else{
                    gameCounter.didNotEarnBadge();
                }
            }else{
                gameCounter.didNotEarnBadge();
            }
        });    
    
    },
    addClickHandleWorkWear : function () {
        exportRoot.mc_Steve_Shirt_1.addEventListener("click", this.doSelectShirt1);    
        exportRoot.mc_Steve_Shirt_2.addEventListener("click", this.doSelectShirt2);    
        exportRoot.mc_Steve_Shirt_3.addEventListener("click", this.doSelectShirt3);    
        exportRoot.mc_Steve_Shirt_4.addEventListener("click", this.doSelectShirt4);

        exportRoot.mc_Kate_Suit_1.addEventListener("click", this.doSelectSuit1);    
        exportRoot.mc_Kate_Suit_2.addEventListener("click", this.doSelectSuit2);    
        exportRoot.mc_Kate_Suit_3.addEventListener("click", this.doSelectSuit3);    
        exportRoot.mc_Kate_Suit_4.addEventListener("click", this.doSelectSuit4);        
    },
    removeClickHandleWorkWear : function () {
        exportRoot.mc_Steve_Shirt_1.removeAllEventListeners();    
        exportRoot.mc_Steve_Shirt_2.removeAllEventListeners();    
        exportRoot.mc_Steve_Shirt_3.removeAllEventListeners();    
        exportRoot.mc_Steve_Shirt_4.removeAllEventListeners();

        exportRoot.mc_Kate_Suit_1.removeAllEventListeners();    
        exportRoot.mc_Kate_Suit_2.removeAllEventListeners();    
        exportRoot.mc_Kate_Suit_3.removeAllEventListeners();    
        exportRoot.mc_Kate_Suit_4.removeAllEventListeners();
    },
    doLogout : function () {
        apiHelper.logoutFromGame(function () {
            apiHelper.gotoMainMenu();
        });
    },
    switchSound : function () {
        apiHelper.switchSound(!apiHelper.isSoundMute() ? false : true);
        if (!apiHelper.isSoundMute()) {
            //sound is on
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(0);
            sManager.playBGMusic();
        }else{
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(1);
            //stop bg music
            sManager.stopBackgroundMusic();
        }
    },
    addHandlersMenuButton : function () {
        exportRoot.mc_MainMenu.btn_Leaderboard.addEventListener("click", gameCounter.showLeaderBoards);    
        exportRoot.mc_MainMenu.btn_Logout.addEventListener("click", gameCounter.doLogout);    
        exportRoot.mc_MainMenu.btn_Sound.addEventListener("click", gameCounter.switchSound);    
        exportRoot.mc_MainMenu.btn_Facebook.addEventListener("click", function () {
            apiHelper.shareFacebookViaGameMenu();
        }) ;
        exportRoot.mc_MainMenu.btn_Twitter.addEventListener("click", function () {
            apiHelper.shareTwitterViaGameMenu();
        });    

        exportRoot.mc_MainMenu.btn_Leaderboard.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Logout.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Sound.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Facebook.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Twitter.cursor = "pointer";;
    },
    removeHandlersMenuButton : function () {
        exportRoot.mc_MainMenu.btn_Leaderboard.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Logout.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Sound.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Facebook.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Twitter.removeAllEventListeners();
    },
    doOpenGameMenu : function () {
        console.log("Open game menu");
        exportRoot.mc_GameMenu.removeAllEventListeners();    
        exportRoot.mc_GameMenu_1.removeAllEventListeners(); 

        exportRoot.mc_MainMenu.visible = true;
        exportRoot.mc_GameMenuOn.visible = true;

        exportRoot.mc_GameMenuOn.cursor = "pointer";;

        exportRoot.mc_GameMenuOn.addEventListener("click" , function () {
            exportRoot.mc_MainMenu.visible = false;
            exportRoot.mc_GameMenuOn.visible = false;
            exportRoot.mc_GameMenuOn.removeAllEventListeners();

            exportRoot.mc_GameMenu.addEventListener("click", gameCounter.doOpenGameMenu);    
            exportRoot.mc_GameMenu_1.addEventListener("click", gameCounter.doOpenGameMenu);    
        });

        gameCounter.removeHandlersMenuButton();
        gameCounter.addHandlersMenuButton();

        //checking sound
        if (!apiHelper.isSoundMute()) {
            //sound is on
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(0);
        }else{
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(1);
        }

    },
    doStartGame : function () {

        gameCounter.gameLoaded = false;

        gameCounter.fixBrowserCompatible();

        //showing loader
        gameCounter.showLoader(true);

        initCustomer("Customer01");
        initCustomer("Customer02");
        initCustomer("Customer03");
        initCustomer("Customer04");

        
        var name = "";
        console.log("gameCounter.avatarMode " + gameCounter.avatarMode);
        console.log("gameCounter.selectedWorkwear " + gameCounter.selectedWorkwear);
        console.log("storageIDAvatar " + $.localStorage.get('storageIDAvatar'));
        var storageIDAvatar = "" + $.localStorage.get('storageIDAvatar');
        if (gameCounter.avatarMode == "male") {
            if (gameCounter.selectedWorkwear == 0) {
                switch(storageIDAvatar) {
                    case "1"://Steve
                        name = "Male_Steve_Option_1";
                        break;
                    case "2"://Greg
                        name = "Male_Greg_Option_1";
                        break;
                    case "5"://Jason
                        name = "Male_Jason_Option_1";
                        break;
                    case "6"://Frank
                        name = "Male_Frank_Option_1";
                        break;
                }
                // initPerson("MaleSteveShirt1"); 
            }else{
                // initPerson("MaleSteveShirt2"); 
                switch(storageIDAvatar) {
                    case "1"://Steve
                        name = "Male_Steve_Option_2";
                        break;
                    case "2"://Greg
                        name = "Male_Greg_Option_2";
                        break;
                    case "5"://Jason
                        name = "Male_Jason_Option_2";
                        break;
                    case "6"://Frank
                        name = "Male_Frank_Option_2";
                        break;
                }
            }
        }else{
            if (gameCounter.selectedWorkwear == 2) {
                switch(storageIDAvatar) {
                    case "3"://Jane
                        name = "Female_Jane_Option_1";
                        break;
                    case "4"://Kate
                        name = "Female_Kate_Option_1";
                        break;
                    case "7"://Lucy
                        name = "Female_Lucy_Option_1";
                        break;
                    case "8"://Peppa
                        name = "Female_Peppa_Option_1";
                        break;
                }
                // initPerson("FemaleKateSuit1"); 
                
            }else{
                // initPerson("FemaleKateSuit2"); 
                switch(storageIDAvatar) {
                    case "3"://Jane
                        name = "Female_Jane_Option_2";
                        break;
                    case "4"://Kate
                        name = "Female_Kate_Option_2";
                        break;
                    case "7"://Lucy
                        name = "Female_Lucy_Option_2";
                        break;
                    case "8"://Peppa
                        name = "Female_Peppa_Option_2";
                        break;
                }
            }
        }
        initPerson(name, (gameCounter.avatarMode == "male"));
        console.log("Do start loader until all sprites loaded for avatar: " + name);
        // exportRoot.gotoAndPlay("Instructions"); 
    },
    allSpritesLoaded : function () {
        console.log("allSpritesLoaded " + gameCounter.gameLoaded);
        if (sprites.loaded && sprites.customers1.loaded && sprites.customers2.loaded && sprites.customers3.loaded && sprites.customers4.loaded && !gameCounter.gameLoaded) {
            gameCounter.gameLoaded = true;
            console.log("allSpritesLoaded show Instructions");
            gameCounter.showLoader(false);
            exportRoot.gotoAndPlay("Instructions");
            
        }
    },
    showLoader : function (visible) {
        exportRoot.mc_loader.visible = visible;
        if (visible) {
            exportRoot.mc_loader.gotoAndPlay(0);
        }else{
            exportRoot.mc_loader.stop();    
            //remove from stage
            exportRoot.removeChild(exportRoot.mc_loader);
        }
    },
    doConfirmQuitGame : function () {
        exportRoot.gotoAndStop("GameQuit");
        exportRoot.mc_QuitConfirmInit.mc_No.addEventListener ("click", function () {

        });
        exportRoot.mc_QuitConfirmInit.mc_Yes.addEventListener ("click", function () {

        });
    },
    doQuitGame : function () {
        exportRoot.mc_ConfirmQuitGame.visible = true;
        exportRoot.mc_ConfirmQuitGame.gotoAndPlay(0);
        exportRoot.mc_ConfirmQuitGame.btn_No.removeAllEventListeners();
        exportRoot.mc_ConfirmQuitGame.btn_No.addEventListener("click", function () {
            gameCounter.sureQuitGame = false;
            exportRoot.mc_ConfirmQuitGame.visible = false;
            exportRoot.mc_ConfirmQuitGame.gotoAndPlay(24);
        });
        exportRoot.mc_ConfirmQuitGame.btn_Yes.removeAllEventListeners();
        exportRoot.mc_ConfirmQuitGame.btn_Yes.addEventListener("click", function () {
            gameCounter.sureQuitGame = true;
            exportRoot.mc_ConfirmQuitGame.visible = false;
            exportRoot.mc_ConfirmQuitGame.gotoAndPlay(24);
        });
    },
    doGotItWrongSteveWear : function () {
        exportRoot.mc_ladyBossMC.gotoAndPlay("PressGotIt");
    },
    doGotItWrongWearPressedDone : function () {
        gameCounter.doInitSelectWorkwear();
    },
    doSelectShirt1: function () {
        gameCounter.doSelectShirt(1);
    },
    doSelectShirt2: function () {
        gameCounter.doSelectShirt(2);
    },
    doSelectShirt3: function () {
        gameCounter.doSelectShirt(3);
    },
    doSelectShirt4: function () {
        gameCounter.doSelectShirt(4);
    },
    doSelectSuit1: function () {
        gameCounter.doSelectSuit(1);
    },
    doSelectSuit2: function () {
        gameCounter.doSelectSuit(2);
    },
    doSelectSuit3: function () {
        gameCounter.doSelectSuit(3);
    },
    doSelectSuit4: function () {
        gameCounter.doSelectSuit(4);
    },
    doSelectShirt: function (shirtNo) {
        console.log("do click shirt " + shirtNo);
        // exportRoot.mc_SteveOptions.gotoAndStop(shirtNo == 1 ? 2 : shirtNo == 2 ? 4 : shirtNo == 3 ? 1 : 3);
        gameCounter.playingAvatar.gotoAndStop(shirtNo == 1 ? 2 : shirtNo == 2 ? 4 : shirtNo == 3 ? 1 : 3);
        //wrong shirt: The avatar can't wear the motif top or the hoodie. They have to choose one of the two suitable workwear choices to play the game. The Health and Safety Officer tells you to pick something else if you've chosen something inappropriate.
        if (shirtNo == 2 || shirtNo == 3) {
            
            sManager.playRightChoose(false);
            if (shirtNo == 2) {
                gameCounter.selectedWorkwear = 0;
            }else{
                gameCounter.selectedWorkwear = 1;
            }
            gameCounter.enableStartGame(true);

        }else{
            sManager.playWrongChoose(false);
            gameCounter.removeClickHandleWorkWear();
            exportRoot.gotoAndPlay("NotSuitableSteve");
            exportRoot.mc_ladyBossMC.gotoAndPlay("Default");
            gameCounter.enableStartGame(false);
        }
    },
    doSelectSuit: function (shirtNo) {
        console.log("do click suit " + shirtNo);
        gameCounter.playingAvatar.gotoAndStop(shirtNo == 1 ? 2 : shirtNo == 2 ? 4 : shirtNo == 3 ? 1 : 3);
        //wrong shirt: The avatar can't wear the motif top or the hoodie. They have to choose one of the two suitable workwear choices to play the game. The Health and Safety Officer tells you to pick something else if you've chosen something inappropriate.
        if (shirtNo == 2 || shirtNo == 3) {
            
            sManager.playRightChoose(false);
            if (shirtNo == 1) {
                gameCounter.selectedWorkwear = 2;
            }else{
                gameCounter.selectedWorkwear = 3;
            }
            gameCounter.enableStartGame(true);

        }else{

            sManager.playWrongChoose(false);
            gameCounter.removeClickHandleWorkWear();
            exportRoot.gotoAndPlay("NotSuitableKate");
            exportRoot.mc_ladyBossMC.gotoAndPlay("Default");
            gameCounter.enableStartGame(false);
            
        }
    },
    doInitSelectWorkwear : function () {
        if (gameCounter.avatarMode == "male") {
            exportRoot.gotoAndStop("ChooseSteveWorkwear");
            
        }else{
            exportRoot.gotoAndStop("ChooseKateWorkwear");
        }
        gameCounter.addClickHandleWorkWear();
    },
    
    frameTicker : function (e) {
        if (gameCounter.gamePlaying && !gameCounter.gamePause) {
			if (!gameCounter.customerIsPlaying){
				//create new customer
				exportRoot.mc_Customer.visible = true;
				gameCounter.newCustomer();
				gameCounter.fixBrowserCompatible();
			}
			gameCounter.updateGameLevel(gameCounter.currentLevel);
        }
    },
    randQuestionNo : function () {
        console.log("get new question");
        var idx = chance.natural({min: 0, max: gameCounter.questions.length - 1});
        gameCounter.currentQuestion = gameCounter.questions[idx];
        //remove this questions from list
        gameCounter.questions.splice(idx,1);

        return gameCounter.currentQuestion.no;
    },

    customerWalkInDone : function () {
        //mc_CustomerBubble

        gameCounter.mc_currentCustomer.mc.gotoAndPlay("Talking_Loop");            

        //get new question
        gameCounter.randQuestionNo();

        gameCounter.removeAnswerOptions();
        
        exportRoot.mc_CustomerBubble.visible = true;
        exportRoot.mc_CustomerBubble.gotoAndStop(0);
        exportRoot.mc_CustomerBubble.txt_CustomerQuestion.text = gameCounter.currentQuestion.question;
		gameCounter.verticallyCentreText(exportRoot.mc_CustomerBubble.txt_CustomerQuestion, -90, 180);
        // setTimeout(gameCounter.updateCustomerQuestion, 10); 
    },
    customerTalkDone : function () {
        //show reply box
        console.log("customerTalkDone");
        gameCounter.removeClickHandleAnswers();
        exportRoot.mc_CounterReply.visible = true;
        exportRoot.mc_CounterReply.addEventListener("click", gameCounter.doReply); 
        gameCounter.addClickHandleAnswers();
    },
    customerWalkIn : function () {
        if (gameCounter.mc_currentCustomer.loaded) {
            

            gameCounter.mc_currentCustomer.mc.x = 0;
            gameCounter.mc_currentCustomer.mc.y = 0;

            console.log("customerWalkIn " + gameCounter.mc_currentCustomer.mc.x  + " : " + gameCounter.mc_currentCustomer.mc.y);

            gameCounter.mc_currentCustomer.mc.gotoAndPlay("Walk_In");    

            createjs.Tween.get(gameCounter.mc_currentCustomer.mc, {override:true}).to({x: gameCounter.mc_currentCustomer.endWalkPos.x, y: gameCounter.mc_currentCustomer.endWalkPos.y}, 2500);

        }else{
            setTimeout(gameCounter.customerWalkIn, 200);
        }
    },
    showManager: function () {
        console.log("show boss");
        exportRoot.mc_Boss.visible = true;
        exportRoot.mc_CustomerBubble.visible = false;
        //gameCounter.updateGameLevel(gameCounter.currentLevel);
        exportRoot.gotoAndPlay("Boss");
        //gameCounter.updateGameLevel(gameCounter.currentLevel);
        //gameCounter.fixBrowserCompatible();
    },
    customerWalkOutDone : function () {
        exportRoot.mc_CustomerBubble.visible = false;
        if (gameCounter.answers[gameCounter.answerNo].awayAFTER == false) {
            console.log("show boss after customer walk out done");
            gameCounter.showManager();
        }else{
            gameCounter.continueGame();
            gameCounter.newCustomerAvailable = true;    
        }

        gameCounter.updateGameLevel(gameCounter.currentLevel);
    },
    customerWalkOut : function () {
        console.log("customerWalkOut");
        exportRoot.mc_CustomerBubble.visible = false;
        gameCounter.mc_currentCustomer.mc.gotoAndPlay("Walk_Out");    
        createjs.Tween.get(gameCounter.mc_currentCustomer.mc, {override:true}).to({x: gameCounter.mc_currentCustomer.walkOutPos.x, y: gameCounter.mc_currentCustomer.walkOutPos.y}, 1500);        
    },

    newCustomer : function () {
        if (!gameCounter.newCustomerAvailable)return;
        var newCustomer = gameCounter.randCustomerNo();
        console.log("newCustomer " + newCustomer);
        var flag = false;
        if (newCustomer == 1 && sprites.customers1.loaded) {
            gameCounter.mc_currentCustomer = sprites.customers1;
            flag = true;
        }else if (newCustomer == 2 && sprites.customers2.loaded) {
            gameCounter.mc_currentCustomer = sprites.customers2;
            flag = true;
        }else if (newCustomer == 3 && sprites.customers3.loaded) {
            gameCounter.mc_currentCustomer = sprites.customers3;
            flag = true;
        }else if (newCustomer == 4 && sprites.customers4.loaded) {
            gameCounter.mc_currentCustomer = sprites.customers4;
            flag = true;
        }

        if (!flag) {
            console.log("no customer loaded");
            return;
        }

        
        //mc_Customer
        gameCounter.playingCustomer = newCustomer;
        gameCounter.mc_currentCustomer.mc.visible = true;
        
        gameCounter.customerIsPlaying = true;

        gameCounter.newCustomerAvailable = false;

        gameCounter.customerWalkIn();

    },
    randCustomerNo : function () {
        //we only got 3 customer type
        return chance.natural({min: 1, max: 4});
    },
    bossTalkDone : function() {
        console.log("boss talk done");
        exportRoot.mc_Boss.visible = false;
        if (gameCounter.answers[gameCounter.answerNo].awayAFTER == true) {
            // setTimeout(gameCounter.customerWalkOut, 2000);
            console.log("customer walk out after boss talk");
            gameCounter.customerWalkOut();
        }else{
            gameCounter.newCustomerAvailable = true;   
            gameCounter.continueGame();    
        } 
        gameCounter.fixBrowserCompatible();    
    },
    continueGame : function () {
        //remove current question
        console.log("question length " + gameCounter.questions.length);

        if (gameCounter.questions.length == 0) {
            //game over
            gameCounter.gamePlaying = false;
            gameCounter.gamePause = true;
            gameCounter.gameOver();

        }else{

            if (gameCounter.questions.length == 10) {
                //level 2
                gameCounter.currentLevel = 2;
                //show game timer
                gameCounter.resetGameTimer();
            }else if (gameCounter.questions.length == 5) {
                gameCounter.currentLevel = 3;
                gameCounter.resetGameTimer();
            }
            
            gameCounter.gamePause = false;

            //hide reply
            
            //customer away
            exportRoot.gotoAndStop("GamePlay");
            exportRoot.mc_CounterReply.gotoAndStop(0);
            exportRoot.mc_CounterReply.visible = false;

            //start new customer
            gameCounter.gamePlaying = true;
            gameCounter.gamePause = false;
            gameCounter.customerIsPlaying = false;

            console.log("game level " + gameCounter.currentLevel);
            gameCounter.updateGameLevel(gameCounter.currentLevel);
        }
    },
	verticallyCentreText : function(txt, orgY, height){
		var textHeight = txt.getMeasuredHeight();
		txt.y = (height - textHeight)/2 + orgY;
	},
    bossDoneShowing : function () {
        console.log("boss done showing");
        //exportRoot.mc_Boss.play();
        exportRoot.mc_Boss.gotoAndPlay("StartTalking");
        exportRoot.mc_Boss.mc_BossBubble.visible = true;
        exportRoot.mc_Boss.mc_BossBubble.gotoAndStop(0);
        exportRoot.mc_Boss.mc_BossBubble.mc_BossMessage.text = gameCounter.answers[gameCounter.answerNo].manager_response;
		gameCounter.verticallyCentreText(exportRoot.mc_Boss.mc_BossBubble.mc_BossMessage, -13, 145);
        //setTimeout(gameCounter.updateBossMessage, 10);
        //gameCounter.updateGameLevel(gameCounter.currentLevel);
    },
    doGotItBossHandle : function () {
        console.log("got it");
        exportRoot.mc_Boss.mc_BossBubble.visible = false;
        exportRoot.mc_Boss.gotoAndPlay("StopTalking");
    },
    didNotEarnBadge : function () {
        exportRoot.mc_DidntGetBadge.mc_FinalScoreNotEnoughtPts.mc_Score.text = "" + gameCounter.currentScore;
        exportRoot.mc_DidntGetBadge.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gameCounter.GAME_NAME, gameCounter.currentScore);
        });
        exportRoot.mc_DidntGetBadge.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gameCounter.GAME_NAME, gameCounter.currentScore);
        });
        exportRoot.gotoAndStop("NotEnoughForBadge");
    },
    alreadyGotBadge : function () {
        
        exportRoot.mc_BadgeAlreadyEarned.mc_FinalScore.mc_Score.text = "" + gameCounter.currentScore;
        exportRoot.mc_BadgeAlreadyEarned.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gameCounter.GAME_NAME, gameCounter.currentScore);
        });
        exportRoot.mc_BadgeAlreadyEarned.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gameCounter.GAME_NAME, gameCounter.currentScore);
        });
        exportRoot.mc_BadgeAlreadyEarned.mc_Leaderboard.addEventListener("click" , gameCounter.showLeaderBoards);
        
        exportRoot.gotoAndStop("AlreadyGotBadge");
    },
    earnedBadge : function () {
        exportRoot.mc_BadgeEarned.mc_FinalScore.mc_Score.text = "" + gameCounter.currentScore;
        exportRoot.mc_BadgeEarned.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gameCounter.GAME_NAME, gameCounter.currentScore);
        });
        exportRoot.mc_BadgeEarned.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gameCounter.GAME_NAME, gameCounter.currentScore);
        });
        exportRoot.mc_BadgeEarned.mc_Leaderboard.addEventListener("click" , gameCounter.showLeaderBoards);
        exportRoot.gotoAndStop("EarnedBadge");
    },
    earnedAllBadge : function () {

        //
        exportRoot.mc_WinnersCup.mc_Close.addEventListener("click" , function (e) {
            //show leader board
            console.log("close winner cup");
            exportRoot.gotoAndPlay("CloseEarnedBadge");
        });
        exportRoot.mc_WinnersCup.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gameCounter.GAME_NAME, gameCounter.currentScore);
        });
        exportRoot.mc_WinnersCup.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gameCounter.GAME_NAME, gameCounter.currentScore);
        });


        exportRoot.mc_BadgeEarned.mc_FinalScore.mc_Score.text = "" + gameCounter.currentScore;
        exportRoot.mc_BadgeEarned.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gameCounter.GAME_NAME, gameCounter.currentScore);
        });
        exportRoot.mc_BadgeEarned.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gameCounter.GAME_NAME, gameCounter.currentScore);
        });
        exportRoot.mc_BadgeEarned.mc_Leaderboard.addEventListener("click" , gameCounter.showLeaderBoards);

        exportRoot.gotoAndPlay("EarnedBadge");
    },
    showLeaderBoards : function () {
        apiHelper.openLeaderBoards(gameCounter.GAME_ID);
        //exportRoot.gotoAndStop("PostRoomLeaderboard");
    },
    confirmedQuitGame : function () {
        if (gameCounter.sureQuitGame == true) {
            apiHelper.gotoMainMenu();
        }
    },
    fixBrowserCompatible : function () {
        if (browser.mozilla) {
            try{
                if (gameCounter.avatarMode == "male") {
                    exportRoot.mc_MainAvatar.setTransform(272.4,225.8,1,1,0,0,0,84.4,144.8);
                }else{
                    exportRoot.mc_MainAvatar.setTransform(272.4,218.8,1,1,0,0,0,84.4,144.8);    
                }
                
            //this.mc_MainAvatar.setTransform(272.4,227.8,1,1,0,0,0,84.4,144.8);
            }catch(e){}

            try{
                exportRoot.txt_ChooseWorkWear.setTransform(474,25.7,1.29,1.29)
                exportRoot.txt_GameTitle.setTransform(exportRoot.txt_GameTitle.x,20.5,1.29,1.29);

                //lbl_GameTimeUp
            }catch(e){}

            try{
                exportRoot.txt_GameLevel.setTransform(307,15,1.29,1.29);
            }catch(e){

            }
            
            //exportRoot.mc_TimeUpTitle.setTransform(469.4,20.7,1.29,1.29);
        }
        
    }
}



var gameTimer = {
    timer : null,
    seconds : 10,
    updateStatus : null,
    counterEnd : null,
    decrementCounter : function () {
        gameTimer.updateStatus(gameTimer.seconds);
        if (gameTimer.seconds === 0) {
            gameTimer.counterEnd();
            if (gameTimer.seconds === 0) {
                gameTimer.stop();
            }
        }
        gameTimer.seconds--;
    },
    start : function() {
        clearInterval(gameTimer.timer);
        gameTimer.timer = 0;
        //seconds = options.seconds;
        gameTimer.timer = setInterval(gameTimer.decrementCounter, 1000);
    },
    stop : function() {
        console.log("timer stop");
        gameTimer.updateStatus = null;
        gameTimer.counterEnd = null;
        gameTimer.seconds = -1;
        clearInterval(this.timer);
    },
    increaseTimer : function (newTime) {
        gameTimer.seconds = parseInt(gameTimer.seconds) + parseInt(newTime);
        //console.log("new timer " + seconds);
    },
    getRemainTime : function () {
        return gameTimer.seconds;
    }
}