var loadLocationCanvas = {
    setPositionCanvasLogin: function (_objectID) {
        this.setPostionResize(_objectID);
        $(window).resize(function () {
            $("#"+_objectID).css({
                left: parseInt(defineValueDefault.loadPositionCanvas().x),
                top: parseInt(defineValueDefault.loadPositionCanvas().y)
            });
        })
    },
    setPostionResize: function (_objectID) {
        $("#"+_objectID).css({
            left: parseInt(defineValueDefault.loadPositionCanvas().x),
            top: parseInt(defineValueDefault.loadPositionCanvas().y)
        });
    }
}