<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends Model{	
	function User_model(){
		parent::Model();
	}
	
	function is_exist_user($email, $password){	
		if($password != ''){
			$password = md5($password) ;
			
			$this->db->select('u.*');
			$this->db->where("u.email = '" . $email . "'"); 
			$this->db->where('u.password', $password);
			$rs = $this->db->get('users u');
			
			if ($rs->num_rows() > 0){	
				$rows = $rs->result();
			    return $rows[0];
			}				
		}
		
		return null;		
 	}
	function is_exist_fb($fb_id){	
			$this->db->select('*');
			$this->db->where("fb_id = '" . $fb_id . "'"); 
			$rs = $this->db->get('users');
			
			if ($rs->num_rows() > 0){	
				$rows = $rs->result();
			    return $rows[0];
			}				
		return null;		
 	}
	
	function is_exist_twitter($twitter_id){	
			$this->db->select('*');
			$this->db->where("twitter_id = '" . $twitter_id . "'"); 
			$rs = $this->db->get('users');
			
			if ($rs->num_rows() > 0){	
				$rows = $rs->result();
			    return $rows[0];
			}				
		return null;		
 	}
 	function is_exist_email($email){
 		$this->db->select('*');		
		$this->db->where('email', $email);
		$rs = $this->db->get('users ');
		
		if ($rs){
			//$row = $rs->row();
			//if ($row) return true;
			return $rs->row();
		}
		
		return null;//false;
 	}
 	function get_user($id){
	    $this->db->select('u.id as user_id,u.access_token, u.name,u.email,c.country');
		$this->db->join('countries c', 'u.country_id=c.id', 'left');
		$this->db->where('u.id', $id);
		$rs = $this->db->get('users u');
		if($rs){
			$row = $rs->row();
			$user=$row;
			return $user;
		}
		
		return null;
	}
	function get_facebook_user($facebook_id){	
			$this->db->select('*');
			$this->db->where('fb_id ="'.$facebook_id.'"');
			$rs = $this->db->get('users');
			
			if ($rs->num_rows() > 0){	
				$rows = $rs->result();
				return $rows[0];
			}				
		
		return null;		
 	}
	
	function get_twitter_user($twitter_id){	
			$this->db->select('*');
			$this->db->where('twitter_id ="'.$twitter_id.'"');
			$rs = $this->db->get('users');
			
			if ($rs->num_rows() > 0){	
				$rows = $rs->result();
				return $rows[0];
			}				
		
		return null;		
 	}
 	
    function check_session($id,$access_token){	
			$this->db->select('*');
			$this->db->where('id', $id);
			$this->db->where("access_token = '" . $access_token . "'"); 
			$rs = $this->db->get('users');
			
			if ($rs->num_rows() > 0){	
				$rows = $rs->result();
			    return $rows[0];
			}				
		return null;		
 	}
	function insert_user($data){
		$this->db->insert('users', $data); 
		
		return $this->db->insert_id();
 	}
 	
 	function update_user($id, $user){		
		$this->db->where('id', $id);
		return $this->db->update('users', $user);
	}
	function delete_user($id){ 
		$this->db->trans_start();
		
		if (is_array($id) && count($id)>0){
			$this->db->query("DELETE FROM users WHERE id IN ('".implode('\',\'', $id)."');");			
			//$this->db->query("DELETE FROM userss_role WHERE user_id IN ('".implode('\',\'', $id)."');");
			
		}else if ($id){
			//delete a message
			$this->db->query("DELETE FROM users WHERE id = '$id'");
			//$this->db->query("DELETE FROM userss_role WHERE user_id = '$id'");
		}
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE){
    		return false;
		}
		
		return true;
	}
    
	
	function count_users(){
		$this->db->select('id');		
		$rs = $this->db->get('users');
		
		if ($rs){
			return $rs->num_rows();
		}
		
		return null;
	}
	
	
	function get_users($start=0,$limit=null){
		$this->db->select('u.*');
		$this->db->order_by('u.id', 'desc');
		if($limit)
		   $this->db->limit($limit, $start);		
		$rs = $this->db->get('users u');
		
		/*if ($rs){
			return $rs->result();
		}*/
		
		if ($rs){
		    $users_arr=array();
		    $users=$rs->result();
			for ($i=0; $i<count($users); $i++){
			 $user=$users[$i];
			 //$this->get_user_roles(&$user);
			 $this->get_user_roles($user);
			 array_push($users_arr,$user); 
			}
			return $users_arr;
		}
		return null;
	}
	
	
	function resetPassword( $email){
       $user=$this->is_exist_email(trim( $email));
       if(!$user) return;  
	   
      $randpassword = createRandomPassword();
      $obj = new stdClass();
      $obj->password = md5($randpassword);
	  $this->db->where('email', $email);
	  $result=$this->db->update('users', $obj);
	   if(!empty($result)){
        $emlobj = new stdClass();
        $emlobj->name = (isset($user->name))?$user->name:$email;
        $emlobj->email = $email;
        $emlobj->new_password = $randpassword;
        $email_info = prepareEmail('notice-resetpassword', $emlobj);
        //file_put_contents('resetpassword.txt', $body);
        //sendemail($email, /*'Your password has been reset'*/ $email_info['title'], $email_info['body']);
		sendemail($email, 'Your password has been reset', $email_info['body']);
      }
      return $result;
    }
	
	
	function changePassword($id,$new_password){
	  $user_info = $this->get_user($id);
      if(! empty($user_info)){
		  $obj = new stdClass();
		  $obj->password = md5($new_password);
		   $this->db->where('id', $id);
	       $result=$this->db->update('users', $obj);
		   if(!empty($result)){
			// send email notice
			$emlobj = new stdClass();
			$emlobj->name = $user_info->name;
	        $emlobj->url = base_url().'reset/';
			$email_info = prepareEmail('notice-change-password', $emlobj);
			sendemail($user_info->email, 'Your password has been changed already.', $email_info['body']);
		  }
		  return $result;
	  }
	  return;
    }
	
	function getCountries(){
	   $query = "select * from countries";
		
		$rs = $this->db->query($query);
		
		if ($rs){
			return $rs->result();
		}
		
		return null;
	}
	function getLeaderboards($game_id){
		$where='';
		$where1='';
		if(isset($game_id) && !empty($game_id) && $game_id>0){
		   $where=" AND s.`game_id` ={$game_id} ";
		    $where1=" AND s1.`game_id` ={$game_id} ";
		}
	   $query = "SELECT s.`total_score` , u.name ,
	   			(SELECT COUNT(s1.id)+1 FROM game_sessions s1 WHERE  s1.total_score>s.total_score {$where1})as position
	   			 FROM `game_sessions` s LEFT JOIN users u on u.id=s.`user_id` WHERE u.name IS NOT NULL AND s.`total_score` >0 
				 {$where}
				 ORDER BY `total_score` DESC limit 0,5";
		
		$rs = $this->db->query($query);
		
		if ($rs){
			return $rs->result();
		}
		
		return null;
	}	
	
	function getMyScore($game_id,$user_id){
		
	   $query = "SELECT s.`total_score` , u.name ,
	                   (SELECT COUNT(s1.id)+1 FROM game_sessions s1 WHERE s1.game_id={$game_id} AND s1.total_score>s.total_score)as position
	             FROM `game_sessions` s LEFT JOIN users u on u.id=s.`user_id`
				 WHERE s.`game_id` ={$game_id} AND s.`user_id` ={$user_id} ORDER BY `total_score` DESC limit 0,1";
		
		$rs = $this->db->query($query);
		
		if ($rs->num_rows() > 0){	
				$rows = $rs->result();
			    return $rows[0];
			}				
		return null;	
	}
	
	function create_game_session($data){
		$this->db->insert('game_sessions', $data); 
		
		return $this->db->insert_id();
 	}	
	
	function add_game_score($data){
		$this->db->insert('game_scores', $data); 
		
		return $this->db->insert_id();
 	}	
	
	 function get_current_score($session_id){
	    $query = "SELECT SUM(score)  as score FROM `game_scores` WHERE `session_id`={$session_id}";
		
		$rs = $this->db->query($query);
		
		if ($rs){
			return $rs->row()->score;
		}
		
		return 0;
	}
	
	function update_game_over($id, $data){		
		$this->db->where('id', $id);
		return $this->db->update('game_sessions', $data);
	}
	
	function get_current_win_badge($user_id,$game_id){
	    $query = "SELECT count(id) as win_badge FROM `game_sessions` WHERE `user_id`={$user_id} AND game_id={$game_id} AND win_badge=1";
		
		$rs = $this->db->query($query);
		
		if ($rs){
		
			return  $rs->row()->win_badge;
		}
		
		return 0;
	}
	
	
	function get_user_badges($user_id){
	    $query = "SELECT count(id) as my_badges FROM `game_sessions` WHERE `user_id`={$user_id} AND win_badge=1 GROUP BY game_id";
		
		$rs = $this->db->query($query);
		
		if ($rs && $rs->num_rows()>0){
		
			return  $rs->num_rows();
		}
		
		return 0;
	}
	
}
?>