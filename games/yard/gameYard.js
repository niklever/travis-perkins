var sprites = new Object();
sprites.avatar = null;
sprites.loaded = false;

function initPerson(name){
    gameYard.avatarName = name;
    exportRoot.worker.visible = false;
	exportRoot.worker.x = 160;
	
    showSprite(name);

    setTimeout(checkSpriteLoaded, 200);
}

function showSprite(name){  
    if (sprites.avatar==null){
        loadWorker(name);
    }
        
    if (sprites.loaded) sprites.avatar.visible = true;
}


function checkSpriteLoaded(){
    //dbLog("checkSpriteLoaded");
    console.log(gameYard.avatarName + " : " + sprites.loaded);
    if (sprites.loaded){
        exportRoot.worker.visible = true;
        gameYard.allSpritesLoaded();
    }else{
        setTimeout(checkSpriteLoaded, 200);
    }
}


var gameYard  = {
    GAME_NAME : "The Yard",
    avatarMode: "male",//male , female
    GAME_ID : "3",
    GAME_NAME : "yard",
    SOUND_ENABLED : true,
    gameLoaded : false,
    selectedWorkwear : 0,//0, 1: Steve, 2,3: Kate
    gamePlaying : false,
    gamePause : false,
	gameCompleted : false,
    currentScore : 0,
    gameRemainingTime : 180, // in second
    MAX_PLAY_TIME : 180,
    SCORE_INC_STEP : 500,
    mc_currentAvatar : null,
    mc_currentCustomer : null,
    avatarName : null,
    customerIsPlaying : false,
    init: function (gmode) {
        exportRoot.mc_JasonOptions.visible = false;
        exportRoot.mc_FrankOptions.visible = false;
        exportRoot.mc_GregOptions.visible = false;
        exportRoot.mc_SteveOptions.visible = false;
        exportRoot.mc_KateOptions.visible = false;
        exportRoot.mc_PeppaOptions.visible = false;
        exportRoot.mc_LucyOptions.visible = false;
        exportRoot.mc_JaneOptions.visible = false;

        console.log("storageIDAvatar " + $.localStorage.get("storageIDAvatar"));
        if ($.localStorage.get("storageIDAvatar") == 1 || $.localStorage.get("storageIDAvatar") == 5 || $.localStorage.get("storageIDAvatar") == 2 || $.localStorage.get("storageIDAvatar") == 6) {
            gameYard.avatarMode = "male";    
        }else{
            gameYard.avatarMode = "female";    
        }
        gameYard.enableStartGame(false);
        gameYard.doUpdateAvatar($.localStorage.get("storageIDAvatar"));
        gameYard.doInitSelectWorkwear();
        exportRoot.mc_ladyBossMC.mc_GotItBtn.addEventListener("click", this.doGotItWrongSteveWear); 
        gameYard.enableCursorType();
        gameYard.fixBrowserCompatible();
        gameYard.addClickHandleGameToolbars();
    },
    doUpdateAvatar: function () {
        if ($.localStorage.get("storageIDAvatar") == 1) {
            // exportRoot.mc_SteveOptions.mc_SteveAvatar.visible = true;
            exportRoot.mc_SteveOptions.visible = true;
            gameYard.playingAvatar = exportRoot.mc_SteveOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 2) {
            // exportRoot.mc_SteveOptions.mc_GregAvatar.visible = true;
            exportRoot.mc_GregOptions.visible = true;
            gameYard.playingAvatar = exportRoot.mc_GregOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 5) {
            // exportRoot.mc_SteveOptions.mc_JasonAvatar.visible = true;
            exportRoot.mc_JasonOptions.visible = true;
            gameYard.playingAvatar = exportRoot.mc_JasonOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 6) {
            // exportRoot.mc_SteveOptions.mc_FrankAvatar.visible = true;
            exportRoot.mc_FrankOptions.visible = true;
            gameYard.playingAvatar = exportRoot.mc_FrankOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 3){
            //jane
            // exportRoot.mc_KateOptions.mc_JaneAvatar.visible = true;
            exportRoot.mc_JaneOptions.visible = true;
            gameYard.playingAvatar = exportRoot.mc_JaneOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 4){
            //kate
            // exportRoot.mc_KateOptions.mc_KateAvatar.visible = true;
            exportRoot.mc_KateOptions.visible = true;
            gameYard.playingAvatar = exportRoot.mc_KateOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 7){
            //lucy
            // exportRoot.mc_KateOptions.mc_LucyAvatar.visible = true;
            exportRoot.mc_LucyOptions.visible = true;
            gameYard.playingAvatar = exportRoot.mc_LucyOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 8){
            //jane
            // exportRoot.mc_KateOptions.mc_PeppaAvatar.visible = true;
            exportRoot.mc_PeppaOptions.visible = true;
            gameYard.playingAvatar = exportRoot.mc_PeppaOptions;
        }
        
    },
    enableCursorType : function () {
        exportRoot.mc_GameMenu.cursor = "pointer";
        exportRoot.prevBtn.cursor = "pointer";
        
        exportRoot.mc_EnterGame.cursor = "pointer";
        exportRoot.exitBtn.cursor = "pointer";
        
        // exportRoot.mc_CloseGame.cursor = "pointer";
        exportRoot.replayBtn.cursor = "pointer";
    },
    doInitSelectWorkwear : function () {
        if (gameYard.avatarMode == "male") {
            exportRoot.gotoAndStop("ChooseSteveWorkwear");
            
        }else{
            exportRoot.gotoAndStop("ChooseKateWorkwear");
        }
        gameYard.addClickHandleWorkWear();
    },
    addClickHandleGameToolbars : function () {
        exportRoot.mc_GameMenu.addEventListener("click", this.doOpenGameMenu);    
        
        exportRoot.mc_EnterGame.addEventListener("click", this.doStartGame);    
        exportRoot.prevBtn.addEventListener("click", this.doQuitGame);      
        exportRoot.exitBtn.addEventListener("click", this.doQuitGame);      
        
        // exportRoot.mc_Instructions.mc_InstructionGotIt.addEventListener("click", gameYard.doStartGameAfterInstruction);    
        // exportRoot.mc_Boss.mc_BossBubble.mc_BossGotItBtn.addEventListener("click", this.doGotItBossHandle); 

        exportRoot.exitBtn.addEventListener("click", this.doQuitGame);

        exportRoot.replayBtn.addEventListener("click", function (e) {
            console.log("replace game");
            apiHelper.replayGame();
        });
    },
    doStartGameAfterInstruction : function () {
        console.log("start game");

        gameYard.currentScore = 0;

        exportRoot.gotoAndStop("Game");

        gameYard.gamePlaying = true;
        gameYard.gamePause = false;
        
        gameYard.incScore(gameYard.currentScore);
        
        gameYard.startGameTimer();
    },
    confirmedQuitGame : function () {
        if (gameYard.sureQuitGame == true) {
            apiHelper.gotoMainMenu();
        }
    },
    doQuitGame : function () {
        exportRoot.mc_ConfirmQuitGame.visible = true;
        exportRoot.mc_ConfirmQuitGame.gotoAndPlay(0);
        exportRoot.mc_ConfirmQuitGame.btn_No.removeAllEventListeners();
        exportRoot.mc_ConfirmQuitGame.btn_No.addEventListener("click", function () {
            gameYard.sureQuitGame = false;
            exportRoot.mc_ConfirmQuitGame.visible = false;
            exportRoot.mc_ConfirmQuitGame.gotoAndPlay(24);
        });
        exportRoot.mc_ConfirmQuitGame.btn_Yes.removeAllEventListeners();
        exportRoot.mc_ConfirmQuitGame.btn_Yes.addEventListener("click", function () {
            gameYard.sureQuitGame = true;
            exportRoot.mc_ConfirmQuitGame.visible = false;
            exportRoot.mc_ConfirmQuitGame.gotoAndPlay(24);
        });
    },
    doStartGame : function () {
        console.log("Do start game");

        gameYard.showLoader(true);
        var name = "";
        var storageIDAvatar = "" + $.localStorage.get('storageIDAvatar');
        if (gameYard.avatarMode == "male") {
            if (gameYard.selectedWorkwear == 0) {
                switch(storageIDAvatar) {
                    case "1"://Steve
                        name = "Male_Steve_White_Hard_Hat";
                        break;
                    case "2"://Greg
                        name = "Male_Greg_White_Hard_Hat";
                        break;
                    case "5"://Jason
                        name = "Male_Jason_White_Hard_Hat";
                        break;
                    case "6"://Frank
                        name = "Male_Frank_White_Hard_Hat";
                        break;
                }
                // initPerson("MaleSteveShirt1"); 
            }else{
                // initPerson("MaleSteveShirt2"); 
                switch(storageIDAvatar) {
                    case "1"://Steve
                        name = "Male_Steve_Yellow_Hard_Hat";
                        break;
                    case "2"://Greg
                        name = "Male_Greg_Yellow_Hard_Hat";
                        break;
                    case "5"://Jason
                        name = "Male_Jason_Yellow_Hard_Hat";
                        break;
                    case "6"://Frank
                        name = "Male_Frank_Yellow_Hard_Hat";
                        break;
                }
            }
        }else{
            if (gameYard.selectedWorkwear == 2) {
                switch(storageIDAvatar) {
                    case "3"://Jane
                        name = "Female_Jane_White_Hard_Hat";
                        break;
                    case "4"://Kate
                        name = "Female_Kate_White_Hard_Hat";
                        break;
                    case "7"://Lucy
                        name = "Female_Lucy_White_Hard_Hat";
                        break;
                    case "8"://Peppa
                        name = "Female_Peppa_White_Hard_Hat";
                        break;
                }
                // initPerson("FemaleKateSuit1"); 
                
            }else{
                // initPerson("FemaleKateSuit2"); 
                switch(storageIDAvatar) {
                    case "3"://Jane
                        name = "Female_Jane_Yellow_Hard_Hat";
                        break;
                    case "4"://Kate
                        name = "Female_Kate_Yellow_Hard_Hat";
                        break;
                    case "7"://Lucy
                        name = "Female_Lucy_Yellow_Hard_Hat";
                        break;
                    case "8"://Peppa
                        name = "Female_Peppa_Yellow_Hard_Hat";
                        break;
                }
            }
        }
        initPerson(name);

        gameYard.currentScore = 0;

        apiHelper.init_game_session(gameYard.GAME_ID, gameYard.GAME_NAME);    

        gameYard.incScore(gameYard.currentScore);

        console.log("Do start loader until all sprites loaded for avatar: " + name);
    },
    allSpritesLoaded : function () {
        console.log("allSpritesLoaded " + gameYard.gameLoaded);
        if (sprites.loaded && !gameYard.gameLoaded) {
            gameYard.gameLoaded = true;
            console.log("allSpritesLoaded show Instructions");
            gameYard.showLoader(false);
            exportRoot.gotoAndPlay("Instructions");
            
        }
    },
    showLoader : function (visible) {
        exportRoot.mc_loader.visible = visible;
        if (visible) {
            exportRoot.mc_loader.gotoAndPlay(0);
        }else{
            exportRoot.mc_loader.stop();    
            //remove from stage
            exportRoot.removeChild(exportRoot.mc_loader);
        }
    },
    doLogout : function () {
        apiHelper.logoutFromGame(function () {
            apiHelper.gotoMainMenu();
        });
    },
    switchSound : function () {
        apiHelper.switchSound(!apiHelper.isSoundMute() ? false : true);
        if (!apiHelper.isSoundMute()) {
            //sound is on
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(0);
            sManager.playBGMusic();
        }else{
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(1);
            //stop bg music
            sManager.stopBackgroundMusic();
        }
    },
    addHandlersMenuButton : function () {
        exportRoot.mc_MainMenu.btn_Leaderboard.addEventListener("click", gameYard.showLeaderBoards);    
        exportRoot.mc_MainMenu.btn_Logout.addEventListener("click", gameYard.doLogout);    
        exportRoot.mc_MainMenu.btn_Sound.addEventListener("click", gameYard.switchSound);    
        exportRoot.mc_MainMenu.btn_Facebook.addEventListener("click", function () {
            apiHelper.shareFacebookViaGameMenu();
        }) ;
        exportRoot.mc_MainMenu.btn_Twitter.addEventListener("click", function () {
            apiHelper.shareTwitterViaGameMenu();
        });    

        exportRoot.mc_MainMenu.btn_Leaderboard.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Logout.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Sound.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Facebook.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Twitter.cursor = "pointer";;
    },
    removeHandlersMenuButton : function () {
        exportRoot.mc_MainMenu.btn_Leaderboard.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Logout.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Sound.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Facebook.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Twitter.removeAllEventListeners();
    },
    doOpenGameMenu : function () {
        console.log("Open game menu");
        exportRoot.mc_MainMenu.removeAllEventListeners();    
        
        exportRoot.mc_MainMenu.visible = true;
        exportRoot.mc_GameMenuOn.visible = true;

        exportRoot.mc_GameMenuOn.cursor = "pointer";;

        exportRoot.mc_GameMenuOn.addEventListener("click" , function () {
            exportRoot.mc_MainMenu.visible = false;
            exportRoot.mc_GameMenuOn.visible = false;
            exportRoot.mc_GameMenuOn.removeAllEventListeners();

            try{
                exportRoot.mc_GameMenu.addEventListener("click", gameYard.doOpenGameMenu);    
            }catch(e){}

            try{
                exportRoot.mc_GameMenu_1.addEventListener("click", gameYard.doOpenGameMenu);    
            }catch(e){}
            
            
        });

        gameYard.removeHandlersMenuButton();
        gameYard.addHandlersMenuButton();


        //checking sound
        if (!apiHelper.isSoundMute()) {
            //sound is on
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(0);
        }else{
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(1);
        }

    },
    addClickHandleWorkWear : function () {
        exportRoot.mc_Steve_Shirt_1.addEventListener("click", this.doSelectShirt1);    
        exportRoot.mc_Steve_Shirt_2.addEventListener("click", this.doSelectShirt2);    
        exportRoot.mc_Steve_Shirt_3.addEventListener("click", this.doSelectShirt3);    
        exportRoot.mc_Steve_Shirt_4.addEventListener("click", this.doSelectShirt4);

        exportRoot.mc_Kate_Suit_1.addEventListener("click", this.doSelectSuit1);    
        exportRoot.mc_Kate_Suit_2.addEventListener("click", this.doSelectSuit2);    
        exportRoot.mc_Kate_Suit_3.addEventListener("click", this.doSelectSuit3);    
        exportRoot.mc_Kate_Suit_4.addEventListener("click", this.doSelectSuit4);        
    },
    removeClickHandleWorkWear : function () {
        exportRoot.mc_Steve_Shirt_1.removeAllEventListeners();    
        exportRoot.mc_Steve_Shirt_2.removeAllEventListeners();    
        exportRoot.mc_Steve_Shirt_3.removeAllEventListeners();    
        exportRoot.mc_Steve_Shirt_4.removeAllEventListeners();

        exportRoot.mc_Kate_Suit_1.removeAllEventListeners();    
        exportRoot.mc_Kate_Suit_2.removeAllEventListeners();    
        exportRoot.mc_Kate_Suit_3.removeAllEventListeners();    
        exportRoot.mc_Kate_Suit_4.removeAllEventListeners();
    },
    doGotItWrongSteveWear : function () {
        exportRoot.mc_ladyBossMC.gotoAndPlay("PressGotIt");
    },
    doGotItWrongWearPressedDone : function () {
        gameYard.doInitSelectWorkwear();
    },
    doSelectShirt1: function () {
        gameYard.doSelectShirt(1);
    },
    doSelectShirt2: function () {
        gameYard.doSelectShirt(2);
    },
    doSelectShirt3: function () {
        gameYard.doSelectShirt(3);
    },
    doSelectShirt4: function () {
        gameYard.doSelectShirt(4);
    },
    doSelectSuit1: function () {
        gameYard.doSelectSuit(1);
    },
    doSelectSuit2: function () {
        gameYard.doSelectSuit(2);
    },
    doSelectSuit3: function () {
        gameYard.doSelectSuit(3);
    },
    doSelectSuit4: function () {
        gameYard.doSelectSuit(4);
    },
    doSelectShirt: function (shirtNo) {
        console.log("do click shirt " + shirtNo);
        gameYard.playingAvatar.gotoAndStop(shirtNo== 4 ? 3 : shirtNo == 3 ? 4 : shirtNo);
        //wrong shirt: The avatar can't wear the motif top or the hoodie. They have to choose one of the two suitable workwear choices to play the game. The Health and Safety Officer tells you to pick something else if you've chosen something inappropriate.
        if (shirtNo == 2 || shirtNo == 3) {
            sManager.playWrongChoose(false);
            gameYard.removeClickHandleWorkWear();
            exportRoot.gotoAndPlay("NotSuitableSteve");
            exportRoot.mc_ladyBossMC.gotoAndPlay("Default");
            gameYard.enableStartGame(false);
        }else{
            sManager.playRightChoose(false);
            if (shirtNo == 1) {
                gameYard.selectedWorkwear = 0;
            }else{
                gameYard.selectedWorkwear = 1;
            }
            gameYard.enableStartGame(true);
        }
    },
    doSelectSuit: function (shirtNo) {
        console.log("do click suit " + shirtNo);
        gameYard.playingAvatar.gotoAndStop(shirtNo== 4 ? 2 : shirtNo == 2 ? 4 : shirtNo);
        //wrong shirt: The avatar can't wear the motif top or the hoodie. They have to choose one of the two suitable workwear choices to play the game. The Health and Safety Officer tells you to pick something else if you've chosen something inappropriate.
        if (shirtNo == 2 || shirtNo == 3) {
            sManager.playWrongChoose(false);
            gameYard.removeClickHandleWorkWear();
            exportRoot.gotoAndPlay("NotSuitableKate");
            exportRoot.mc_ladyBossMC.gotoAndPlay("Default");
            gameYard.enableStartGame(false);
        }else{
            sManager.playRightChoose(false);
            if (shirtNo == 1) {
                gameYard.selectedWorkwear = 2;
            }else{
                gameYard.selectedWorkwear = 3;
            }
            gameYard.enableStartGame(true);
        }
    },
    enableStartGame: function (enabled) {
        exportRoot.mc_EnterGame.visible = enabled;
        exportRoot.mc_Highlighter.visible = enabled;
        if (enabled) {
            exportRoot.mc_Highlighter.gotoAndPlay(0);
        }
    },
    fixBrowserCompatible : function () {
        if (browser.mozilla) {
            try{
                // exportRoot.txt_ChooseWorkWear.setTransform(474,25.7,1.29,1.29);
                // exportRoot.txt_GameTitle.setTransform(474,20.7,1.29,1.29);
                // exportRoot.mc_TimeUpTitle.setTransform(469.4,20.7,1.29,1.29);
            }catch(e){}
        }
        
    },
    addLeadingZeros : function (n, length){
        var str = (n == 0 ? "" : n) + "";
        var zeros = "";
        for (var i = length - str.length; i > 0; i--)
            zeros += "0";
        return zeros;
    },
    incScore : function (score) {
        if (score > 0) {
            apiHelper.inc_game_score(gameYard.GAME_ID, score);        
        }
        
        gameYard.currentScore += score;
        exportRoot.mc_Scoring.mc_ScoringCounter.text = gameYard.addLeadingZeros(gameYard.currentScore, 6);
        if (gameYard.currentScore == 0) {
            
            exportRoot.mc_Scoring.mc_Scoring.visible = false;   
        }else{
            exportRoot.mc_Scoring.mc_Scoring.text = gameYard.currentScore;    
            exportRoot.mc_Scoring.mc_Scoring.visible = true;   
        }
    },
    gameOver : function () {

        // exportRoot.mc_HealtySafetyMessage.visible = false;
        gameYard.gamePlaying = false;
        gameYard.gamePause = true;
        gameTimer.stop();
        sManager.stopBackgroundMusic();
		
        apiHelper.game_over(gameYard.GAME_ID, gameYard.currentScore, function (data) {
            //handle badge info
            console.log(data);
            if (!data) {
                gameYard.didNotEarnBadge();          
            }else if (data.status == "OK" && data.badge_info){
                if (data.badge_info.badge_status == 1) {
                    gameYard.didNotEarnBadge();          
                }else if (data.badge_info.badge_status == 2) {
                    gameYard.alreadyGotBadge();
                }else if (data.badge_info.badge_status == 3) {
                    gameYard.earnedBadge();          
                }else if (data.badge_info.badge_status == 4) {
                    gameYard.earnedAllBadge();          
                }else{
                    gameYard.didNotEarnBadge();
                }
            }else{
                gameYard.didNotEarnBadge();
            }
        });    
    },
    didNotEarnBadge : function () {
        exportRoot.mc_DidntGetBadge.mc_FinalScoreNotEnoughtPts.mc_Score.text = "" + gameYard.currentScore;
        exportRoot.mc_DidntGetBadge.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gameYard.GAME_NAME, gameYard.currentScore);
        });
        exportRoot.mc_DidntGetBadge.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gameYard.GAME_NAME, gameYard.currentScore);
        });
        exportRoot.gotoAndStop("NotEnoughForBadge");
		gameYard.setEndTitle();
    },
    alreadyGotBadge : function () {
        
        exportRoot.mc_BadgeAlreadyEarned.mc_FinalScore.mc_Score.text = "" + gameYard.currentScore;
        exportRoot.mc_BadgeAlreadyEarned.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gameYard.GAME_NAME, gameYard.currentScore);
        });
        exportRoot.mc_BadgeAlreadyEarned.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gameYard.GAME_NAME, gameYard.currentScore);
        });
        exportRoot.mc_BadgeAlreadyEarned.mc_Leaderboard.addEventListener("click" , gameYard.showLeaderBoards);
        
        exportRoot.gotoAndStop("AlreadyGotBadge");
		gameYard.setEndTitle();
    },
    earnedBadge : function () {
        exportRoot.mc_BadgeEarned.mc_FinalScore.mc_Score.text = "" + gameYard.currentScore;
        exportRoot.mc_BadgeEarned.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gameYard.GAME_NAME, gameYard.currentScore);
        });
        exportRoot.mc_BadgeEarned.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gameYard.GAME_NAME, gameYard.currentScore);
        });
        exportRoot.mc_BadgeEarned.mc_Leaderboard.addEventListener("click" , gameYard.showLeaderBoards);
        exportRoot.gotoAndStop("EarnedBadge");
		gameYard.setEndTitle();
    },
    earnedAllBadge : function () {

        //
        exportRoot.mc_WinnersCup.mc_Close.addEventListener("click" , function (e) {
            //show leader board
            console.log("close winner cup");
            exportRoot.gotoAndPlay("CloseEarnedBadge");
        });
        exportRoot.mc_WinnersCup.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gameYard.GAME_NAME, gameYard.currentScore);
        });
        exportRoot.mc_WinnersCup.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gameYard.GAME_NAME, gameYard.currentScore);
        });


        exportRoot.mc_BadgeEarned.mc_FinalScore.mc_Score.text = "" + gameYard.currentScore;
        exportRoot.mc_BadgeEarned.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gameYard.GAME_NAME, gameYard.currentScore);
        });
        exportRoot.mc_BadgeEarned.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gameYard.GAME_NAME, gameYard.currentScore);
        });
        exportRoot.mc_BadgeEarned.mc_Leaderboard.addEventListener("click" , gameYard.showLeaderBoards);

        exportRoot.gotoAndPlay("EarnedBadge");
		gameYard.setEndTitle();
    },
    showLeaderBoards : function () {
        apiHelper.openLeaderBoards(gameYard.GAME_ID);
        //exportRoot.gotoAndStop("Leaderboard");
    },
    startGameTimer: function() {
        gameTimer.seconds = gameYard.MAX_PLAY_TIME;
        gameTimer.updateStatus = function(sec) {
                gameYard.gameRemainingTime = sec;
                if (!gameYard.gamePlaying) {
                    return;
                }
                gameYard.updateGameTimer(sec);
            };
        gameTimer.counterEnd = function() {
                console.log("GAME OVER");
                gameYard.gamePlaying = false;
                gameYard.gamePause = true;
                gameYard.gameOver();
            };
        gameTimer.start();  
    },
	setEndTitle : function () {
		if (gameYard.gameCompleted){
			exportRoot.endtitle_txt.text = "NICE JOB!";
		}else{
			exportRoot.endtitle_txt.text = "TIME'S UP!";
		}
	},
    updateGameTimer : function (sec) {
        // var firstNum = "";
        var nextNum = "";
        var m = ~~(sec / 60);
        var s = sec % 60;
        if (m > 9) {
            nextNum = "" + m + ":";
        }else{
            nextNum = "0" + m + ":";
        }
        if (s > 9) {
            nextNum = nextNum + s;
        }else{
            nextNum = nextNum + "0" + s;
        }
        exportRoot.mc_Timer.mc_TimerCounter.text = nextNum;

    },
    addLeadingZeros : function (n, length){
        var str = (n == 0 ? "" : n) + "";
        var zeros = "";
        for (var i = length - str.length; i > 0; i--)
            zeros += "0";
        return zeros;
        
    },
}
var gameTimer = {
    timer : null,
    seconds : 10,
    updateStatus : null,
    counterEnd : null,
    decrementCounter : function () {
        gameTimer.updateStatus(gameTimer.seconds);
        if (gameTimer.seconds === 0) {
            gameTimer.counterEnd();
            gameTimer.stop();
        }
        gameTimer.seconds--;
    },

    start : function() {
        clearInterval(gameTimer.timer);
        gameTimer.timer = 0;
        //seconds = options.seconds;
        gameTimer.timer = setInterval(gameTimer.decrementCounter, 1000);
    },
    stop : function() {
        gameTimer.updateStatus = null;
        gameTimer.counterEnd = null;
        gameTimer.seconds = -1;
        clearInterval(this.timer);
    },
    increaseTimer : function (newTime) {
        gameTimer.seconds = parseInt(gameTimer.seconds) + parseInt(newTime);
        //console.log("new timer " + seconds);
    },
    getRemainTime : function () {
        return gameTimer.seconds;
    }
}