(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 960,
	height: 540,
	fps: 25,
	color: "#FFFFFF",
	manifest: [
		{src:"sounds/correct.mp3", id:"correct"},
		{src:"sounds/forklift.mp3", id:"forklift"},
		{src:"sounds/hitdude.mp3", id:"hitdude"},
		{src:"sounds/lowerforks.mp3", id:"lowerforks"},
		{src:"sounds/music.mp3", id:"music"},
		{src:"sounds/pothole.mp3", id:"pothole"},
		{src:"sounds/raiseforks.mp3", id:"raiseforks"},
		{src:"sounds/wrong.mp3", id:"wrong"}
	]
};



// symbols:



(lib.Boss_01__000 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__001 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__002 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__003 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__004 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__005 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__006 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__007 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__008 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__009 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__010 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__011 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__012 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__013 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__014 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__015 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__016 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__017 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__018 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__019 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__020 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__021 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__022 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__023 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__024 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__025 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(25);
}).prototype = p = new cjs.Sprite();



(lib.Boss_01__026 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(26);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__000 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(27);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__001 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(28);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__002 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(29);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__003 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(30);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__004 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(31);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__005 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(32);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__006 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(33);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__007 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(34);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__008 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(35);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__009 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(36);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__010 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(37);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__011 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(38);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__012 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(39);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__013 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(40);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__014 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(41);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__015 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(42);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__016 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(43);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__017 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(44);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__018 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(45);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__019 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(46);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__020 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(47);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__021 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(48);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__022 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(49);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__023 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(50);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__024 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(51);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__025 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(52);
}).prototype = p = new cjs.Sprite();



(lib.Health_and_Safety_Officer__026 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(53);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__006 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(54);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__007 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(55);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__008 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(56);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__009 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(57);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__010 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(58);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__011 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(59);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__012 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(60);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__013 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(61);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__014 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(62);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__015 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(63);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__016 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(64);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__017 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(65);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__018 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(66);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__019 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(67);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__020 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(68);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__021 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(69);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__022 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(70);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__023 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(71);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__024 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(72);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__025 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(73);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__026 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(74);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__027 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(75);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__028 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(76);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01__029 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(77);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__006 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(78);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__007 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(79);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__008 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(80);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__009 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(81);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__010 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(82);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__011 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(83);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__012 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(84);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__013 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(85);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__014 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(86);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__015 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(87);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__016 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(88);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__017 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(89);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__018 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(90);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__019 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(91);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__020 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(92);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__021 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(93);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__022 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(94);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__023 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(95);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__024 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(96);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__025 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(97);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__026 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(98);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__027 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(99);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__028 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(100);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_01_Back__029 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(101);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__006 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(102);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__007 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(103);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__008 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(104);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__009 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(105);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__010 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(106);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__011 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(107);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__012 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(108);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__013 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(109);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__014 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(110);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__015 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(111);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__016 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(112);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__017 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(113);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__018 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(114);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__019 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(115);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__020 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(116);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__021 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(117);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__022 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(118);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__023 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(119);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__024 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(120);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__025 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(121);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__026 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(122);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__027 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(123);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__028 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(124);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02__029 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(125);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__006 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(126);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__007 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(127);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__008 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(128);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__009 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(129);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__010 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(130);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__011 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(131);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__012 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(132);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__013 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(133);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__014 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(134);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__015 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(135);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__016 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(136);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__017 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(137);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__018 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(138);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__019 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(139);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__020 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(140);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__021 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(141);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__022 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(142);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__023 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(143);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__024 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(144);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__025 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(145);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__026 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(146);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__027 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(147);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__028 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(148);
}).prototype = p = new cjs.Sprite();



(lib.Little_Dude_Walking_02_Back__029 = function() {
	this.spriteSheet = ss["deliverytruck_atlas_"];
	this.gotoAndStop(149);
}).prototype = p = new cjs.Sprite();



(lib.Yes_Btn = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("yes", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 112;
	this.text.setTransform(79.2,13.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AqjkbIVHAAQCOAAAACNIAAEdQAACNiOAAI1HAAQiOAAAAiNIAAkdQAAiNCOAAg");
	this.shape.setTransform(81.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("AqjEcQiOAAAAiNIAAkdQAAiNCOAAIVHAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_1.setTransform(81.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AqjEcQiOAAAAiNIAAkdQAAiNCOAAIVHAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_2.setTransform(90.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.YellHatBack = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF91").s().p("ABpBWQhDgFgogIIgZgGIAAgBQgagWgIgIQgLgOAAgQIADgIQACgIAAgDIAAgCQAPgWAggVQAtgeArAEQAYACASAFQAHAPAEATQAIAhAAAhIgBAqQgBAJgEAMgAhgAhQgggUAAgNQAAgYAIgKQASgVA8gCQgdAVgFAUQgEADgBAHIgBAMQANAiANARQgNgHgbgRg");
	this.shape.setTransform(13.6,9.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("ABIBfQhbgJgkgJQgNgDgVgJQgCAAgKgFQgegLgPgNQgWgUAAgQQAAgeAKgKQATgUBGgEIAGABIAHgFQA7gkBDANIAGABQA4AXAWArQAPAcAAAvIgBAdIABACIgBADQgCACgGADQgIAEgGABgAg1g5QggAVgPAWIAAACQAAADgCAIIgDAIQAAAQALAOQAIAIAaAWIAAABIAZAGQAoAIBDAFIASAAIA1AAIAUgIQABgygKggQgMgpgdgSQgOgJgWgFQgSgFgYgCIgKAAQgkAAgqAagAiZghQgIAKAAAXQAAAOAgAUQAbARANAHQgNgRgNgiIABgMQABgHAEgDQAFgUAdgVQg8ACgSAVg");
	this.shape_1.setTransform(16.9,9.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#A0A056").s().p("AgcBSQAEgMABgIIAAgrQAAgggHgiQgFgTgGgPQAWAFAOAJQAaASANAqQAKAggCAxIgTAIg");
	this.shape_2.setTransform(29,10.1);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,33.9,19.1);


(lib.YardSteve04copy3 = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape.setTransform(25.6,90.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_1.setTransform(9.8,86.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag3DAIAAgOQAAgdADgRIgxgNIgggKQgtgOgTgQQgWgUAAggQAAgZAQgkQgKgHgGgIQgIgMAAgvQAAgmAHgtIAsAEQgGAmAAAhQAAAZAGAJQAVAlA7gFIEXAEIAAgoIAAgUQAaAVATAaIgDAiIACACQAAADgCAEIgBAHIgEAEQgEAEgMAAIgHAAIAAABQACASgCAVQAAAcgHAkQgMA8gZAOIhEAAQgsgSg1gQQACAOAAAUIAAAPgAiuAgQAAAGBRAdIAyARQCDAsAdAAQAgAAABhDIgDg+IjnAAIgXAAQgZgFgUgFIgWArg");
	this.shape_2.setTransform(22.8,84);

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhIA/QhZhEgDgiQgDgiAogcQAogcBPAAQBNAAA0AdQA0AdgJAsQgIAqgqA4QgpA3gdAEIgDAAQgeAAhThDg");
	this.shape_3.setTransform(82.1,16.2);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#392C38").s().p("AgDDLQgRgjg+ixIABgNIgHgGQgCgCAAgEQAAgOBMhyQAXgkAaAjQAWAcAGAoQAYAvACAIQACAKAAAvIgCAjQgDAcgDARIgSCmQAGgbAAAwQgBAwgFALQADAEgMAAQgSAAgpiQgAhQjvIgGgUQAAhiA5ANQAVAFAQASQASARAAAOQAAAOgRAZQgSAfgYAAQgiAAgNgTgAg1j9QAFAFAOAAQALAAACgEIAAAAQgQAHgMgHIgIgIIAEAHg");
	this.shape_4.setTransform(91.8,65);

	// Layer 2
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("ABxHlIgQgcQgBgJhGixQhDivAAgIQAAgDAghFQAehCgBgCQgegNgUgbQgRAOgJAEQgPAIgJgIQh9h+g/hiQg9hhgHhOQgCgXAXgFQAYgFgCAPQgIBEBBBnQA9BiBfBQIABgBIANgaQAQgiABgJQADgPAIgSQgdhRghhUIgghOQAAgUAHgDQAFgCAOgBQBKAlA+C/QAdALApAbQAyAiAVAUIATALIAQghQAUgnAZgmQATgXAcg5QAQghAYABIgzCYQgeBUgbAxQAAAHggAAQgFAAgygYQgOAggSAVQAKARAMAcQAOAeAIANQAeA1AFAXQAFAWgEBKQgFBygVCgQAAAJgGASQgIAXgMAPQgKgEgQgagAAxArIAAgCIgBABIABABIAAAAg");
	this.shape_5.setTransform(84.1,52.9);

	// Layer 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6C4848").s().p("AiAElQgFgKAAgZQAAhFAZhhIAAgCQAMgvAQgrQAXg+Aeg5QBEiEBdhJQhBBcgwCJQgQAsgKArIAAAAIgEAPIgiDrIAAAMIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_6.setTransform(18,46.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#966363").s().p("Al4H8Ig8gMIgYgEIg2gMIAOgBIAEjeIAVAAIAAADQAagEAYABIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQADg2AKhuIACgWQAEgzADgvQACgxAAgpQAAhegKgEQgJgCgOBCQgKAwgHAyIAAAAIgDAaIgNC7IAAAEIAAAEIAAAEIAAAEIAAAKIAAAFIAAAFIAAAoIkQgEIgFhHIAAgLIAijpIAFgRIgBAAQANgrAPgsQAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQgxgEhmgSg");
	this.shape_7.setTransform(85.2,58);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#D6D6AB").s().p("Ag/BaIgEAAIAAgWIAAgCIAAgBIAAgBIAAgIIAAgWIgBAAIAAAAIAAhHIAAhJICJAAIgEDcIgOABQhpgVgJAAg");
	this.shape_8.setTransform(28.5,94.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#1E1E1E").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_9.setTransform(86,143.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#333333").s().p("AicDMIg8gLQiHgcgtglQBJgHATAAQBeAAB6AgIBbAXQgYASgSAKgAGYBtQiBgQgoggQBbgsA/gIQCDgNBEB2QgbACgTADIgDABQhtgIgagDgApLi6IgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAHgUAFQAAiPgliIgACNgrQgHgegJgSQADgbAAgTIAAgKIBWgBIAAAHQAAAQAQBHIAMA2QgRAFgQAIQghAQgTAUQgEgugMgug");
	this.shape_10.setTransform(85,130.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#666666").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_11.setTransform(86.9,128);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AjbMJQjFgig5gUQg7gUgBgmQg1kZABg+IAAgMIgQgGQgFACgJAAQgPAAgGgVIgCgLIAAgFIAAgdIAAgBIABgZIABgUIAJiHIg/AAIgDAAQgXgFhGgbQgKgIgFgIQgJgLAAgtQAAhHAWhhIACgIQAJgnALgkQAZhUAmhKQCTkkENgZIEdgFQEsAwCDBNQA1AfAsAuIBMBYIBxCjQBYBeAABGIACAAIADAFQACAEAAALQABALgPAQQgtAkgOAJQgxAgghAKIAPA/IALA7QAKBRABAiQAAAzgXANIgDgHQgbAThBAaIgMAaQgTAsgCAjQgBATAFAnQAAAQgGANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhBgDIk7AAQguACgZgYQgTgRAAgSQgIgXgMg2IgDgMIgBAEQgTBegTAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYguAGgAneKgQALAMBZAQQCRAZASAEIBiAAIB1AAQAQgKAagSIhbgXQh6ggheAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAFAHgAASKDQAgALAWAMQAFgkAUhtQARheAHgyIkuAEQhggEhsgTQgpgIgqgJQgzgLghgKIAEARQAlCIAACRQAUgFBYgHIA3gFQAzgDAdAAQCmAAB4AtgAGwIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADAsAOIEwAAIAJgBIADgBQATgDAbgCQg8hrhuAAQgOAAgPACgAE9F2IhAABIhWABIAAAKQAAATgDAbQAJASAHAeQAMAwAEAuQATgUAhgQQAQgIARgHQBAgXBQgDQAogBAkAEIgDgMQgCgMAAghQABhOAFgbQhNAVhsAPgApSCEIAABJIAAAAIABABIAAAVIAAAJIAAABIAAABIAAABIAAAWIAEAAQAJAABrAWIA2ALIAYAEIA9AMQBlATAyADIGzgPQCeACBpgtQBJggBAgLQgSg0gLhDQgQhwgNg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAANAuIAJAtQAGAcAIATQAWAgAWAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgTgbh1iHIhGhTQgpgsgxgeQh9hLkYgtIkaAAQh3APheBKQhdBJhGCEQgeA5gXBAQgQArgMAvIAAACQgaBhAABFQABAZAFAKQAWAjA7gFIAIAAIEQADIAAgmIAAgFIAAgFIAAgKIAAgEIAAgEIAAgDIAAgEIANi+IAEgaIAAAAQAHgyAJgwQAOhCAJADQAKADAABeQABApgDAyQgCAugFA2IgCAVQgJBugEA0IACADQAAADgCADIgBAIIgDAEQgFADgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAABJg");
	this.shape_12.setTransform(81.1,77.7);

	// Layer 6
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#EAD0B7").s().p("AhJAgIgOg/QAggKAyggIAXgRQAIAOAZAhQAlAwAAAtQAAAXhYAIQgoAEgWAGIgLg7g");
	this.shape_13.setTransform(155.6,78.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AIUIEQAtgQApgIQgRg0gLhCQgRhwgMg6QgFgWhEiGQg+h4AAg2QAAgWABgRQAtABAMAtIAJAtQAGAdAJASQAVAeAXAsQAYAuAPAnIANAmQASgNAbgPQArgXAhgKQgGg/gzhJQgUgbh0iHIhGhTQgpgsgxgdQh9hMkZgtIkaAAQh2AQheBJQhdBKhGCDQg0BjgeBwQgZBhAABFQAAAaAGAJQAVAlA7gFIEZADIAAgnQAAi6AhihQAOhDAJADQAKADAABfQAABNgJBvQgLB+gEA7IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACAUgCAVQAAAbgHAlQgMA7gZAOIhEAAQgsgRg3gRQACAPAAATIAAAQIgtAAIAAgPQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgHQgIgMAAgvQAAhKAYhmQAdh8A1hqQCTklEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBcABBHIABAAIADAEQADAEAAALQAAAMgPAQQAKAIAMARQA8BPAABFQAAAlgFAGQgLARg6AOIh1AAQAFAsAAAjQAAAZgGARgAsWFkQAAAFBRAdIAyASQCFAsAdAAQAgAAABhDIgDhBIkAAAQgZgEgUgFIgWAtgALADBQgxAfghAKIAPBBIALA7QAWgGAogEQBagHAAgYQAAgtgmgyQgZgggIgOIgZARg");
	this.shape_14.setTransform(84.4,51.6);

	this.addChild(this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.YardSteve04copy2 = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape.setTransform(25.6,90.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_1.setTransform(9.8,86.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag3DAIAAgOQAAgdADgRIgxgNIgggKQgtgOgTgQQgWgUAAggQAAgZAQgkQgKgHgGgIQgIgMAAgvQAAgmAHgtIAsAEQgGAmAAAhQAAAZAGAJQAVAlA7gFIEXAEIAAgoIAAgUQAaAVATAaIgDAiIACACQAAADgCAEIgBAHIgEAEQgEAEgMAAIgHAAIAAABQACASgCAVQAAAcgHAkQgMA8gZAOIhEAAQgsgSg1gQQACAOAAAUIAAAPgAiuAgQAAAGBRAdIAyARQCDAsAdAAQAgAAABhDIgDg+IjnAAIgXAAQgZgFgUgFIgWArg");
	this.shape_2.setTransform(22.8,84);

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhIA/QhZhEgDgiQgDgiAogcQAogcBPAAQBNAAA0AdQA0AdgJAsQgIAqgqA4QgpA3gdAEIgDAAQgeAAhThDg");
	this.shape_3.setTransform(82.1,16.2);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#392C38").s().p("AgDDLQgRgjg+ixIABgNIgHgGQgCgCAAgEQAAgOBMhyQAXgkAaAjQAWAcAGAoQAYAvACAIQACAKAAAvIgCAjQgDAcgDARIgSCmQAGgbAAAwQgBAwgFALQADAEgMAAQgSAAgpiQgAhQjvIgGgUQAAhiA5ANQAVAFAQASQASARAAAOQAAAOgRAZQgSAfgYAAQgiAAgNgTgAg1j9QAFAFAOAAQALAAACgEIAAAAQgQAHgMgHIgIgIIAEAHg");
	this.shape_4.setTransform(91.8,65);

	// Layer 2
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("ABxHlIgQgcQgBgJhGixQhDivAAgIQAAgDAghFQAehCgBgCQgegNgUgbQgRAOgJAEQgPAIgJgIQh9h+g/hiQg9hhgHhOQgCgXAXgFQAYgFgCAPQgIBEBBBnQA9BiBfBQIABgBIANgaQAQgiABgJQADgPAIgSQgdhRghhUIgghOQAAgUAHgDQAFgCAOgBQBKAlA+C/QAdALApAbQAyAiAVAUIATALIAQghQAUgnAZgmQATgXAcg5QAQghAYABIgzCYQgeBUgbAxQAAAHggAAQgFAAgygYQgOAggSAVQAKARAMAcQAOAeAIANQAeA1AFAXQAFAWgEBKQgFBygVCgQAAAJgGASQgIAXgMAPQgKgEgQgagAAxArIAAgCIgBABIABABIAAAAg");
	this.shape_5.setTransform(84.1,52.9);

	// Layer 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#764F4F").s().p("AAUG3IgEAAIAAgWIAAgBIAAgBIAAgBIAAgIIAAgWIgBgBIAAAAIAAhJIAAhJICLAAIgEDfIgOABQhrgWgJAAgAiUCdQgFgKgBgZQAAhFAahfIAAgCQAMguAQgsQAXg/Aeg6QBEiDBdhKQhBBdgxCIQgNAsgMArIAAAAIgEARIgjDpIAAAMIAFBHIgIAAIgOAAQgwAAgTggg");
	this.shape_6.setTransform(20,59.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#966363").s().p("Al4H8Ig8gMIgYgEIg2gMIAOgBIAEjeIAVAAIAAADQAagEAYABIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQADg2AKhuIACgWQAEgzADgvQACgxAAgpQAAhegKgEQgJgCgOBCQgKAwgHAyIAAAAIgDAaIgNC7IAAAEIAAAEIAAAEIAAAEIAAAKIAAAFIAAAFIAAAoIkQgEIgFhHIAAgLIAijpIAFgRIgBAAQANgrAPgsQAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQgxgEhmgSg");
	this.shape_7.setTransform(85.2,58);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#1E1E1E").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_8.setTransform(86,143.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#333333").s().p("AicDMIg8gLQiHgcgtglQBJgHATAAQBeAAB6AgIBbAXQgYASgSAKgAGYBtQiBgQgoggQBbgsA/gIQCDgNBEB2QgbACgTADIgDABQhtgIgagDgApLi6IgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAHgUAFQAAiPgliIgACNgrQgHgegJgSQADgbAAgTIAAgKIBWgBIAAAHQAAAQAQBHIAMA2QgRAFgQAIQghAQgTAUQgEgugMgug");
	this.shape_9.setTransform(85,130.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#666666").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_10.setTransform(86.9,128);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AjbMJQjFgig5gUQg7gUgBgmQg1kZABg+IAAgMIgQgGQgFACgJAAQgPAAgGgVIgCgLIAAgFIAAgdIAAgBIABgZIABgUIAJiHIg/AAIgDAAQgXgFhGgbQgKgIgFgIQgJgLAAgtQAAhHAWhhIACgIQAJgnALgkQAZhUAmhKQCTkkENgZIEdgFQEsAwCDBNQA1AfAsAuIBMBYIBxCjQBYBeAABGIACAAIADAFQACAEAAALQABALgPAQQgtAkgOAJQgxAgghAKIAPA/IALA7QAKBRABAiQAAAzgXANIgDgHQgbAThBAaIgMAaQgTAsgCAjQgBATAFAnQAAAQgGANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhBgDIk7AAQguACgZgYQgTgRAAgSQgIgXgMg2IgDgMIgBAEQgTBegTAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYguAGgAneKgQALAMBZAQQCRAZASAEIBiAAIB1AAQAQgKAagSIhbgXQh6ggheAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAFAHgAASKDQAgALAWAMQAFgkAUhtQARheAHgyIkuAEQhggEhsgTQgpgIgqgJQgzgLghgKIAEARQAlCIAACRQAUgFBYgHIA3gFQAzgDAdAAQCmAAB4AtgAGwIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADAsAOIEwAAIAJgBIADgBQATgDAbgCQg8hrhuAAQgOAAgPACgAE9F2IhAABIhWABIAAAKQAAATgDAbQAJASAHAeQAMAwAEAuQATgUAhgQQAQgIARgHQBAgXBQgDQAogBAkAEIgDgMQgCgMAAghQABhOAFgbQhNAVhsAPgApSCEIAABJIAAAAIABABIAAAVIAAAJIAAABIAAABIAAABIAAAWIAEAAQAJAABrAWIA2ALIAYAEIA9AMQBlATAyADIGzgPQCeACBpgtQBJggBAgLQgSg0gLhDQgQhwgNg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAANAuIAJAtQAGAcAIATQAWAgAWAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgTgbh1iHIhGhTQgpgsgxgeQh9hLkYgtIkaAAQh3APheBKQhdBJhGCEQgeA5gXBAQgQArgMAvIAAACQgaBhAABFQABAZAFAKQAWAjA7gFIAIAAIEQADIAAgmIAAgFIAAgFIAAgKIAAgEIAAgEIAAgDIAAgEIANi+IAEgaIAAAAQAHgyAJgwQAOhCAJADQAKADAABeQABApgDAyQgCAugFA2IgCAVQgJBugEA0IACADQAAADgCADIgBAIIgDAEQgFADgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAABJg");
	this.shape_11.setTransform(81.1,77.7);

	// Layer 6
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EAD0B7").s().p("AhJAgIgOg/QAggKAyggIAXgRQAIAOAZAhQAlAwAAAtQAAAXhYAIQgoAEgWAGIgLg7g");
	this.shape_12.setTransform(155.6,78.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AIUIEQAtgQApgIQgRg0gLhCQgRhwgMg6QgFgWhEiGQg+h4AAg2QAAgWABgRQAtABAMAtIAJAtQAGAdAJASQAVAeAXAsQAYAuAPAnIANAmQASgNAbgPQArgXAhgKQgGg/gzhJQgUgbh0iHIhGhTQgpgsgxgdQh9hMkZgtIkaAAQh2AQheBJQhdBKhGCDQg0BjgeBwQgZBhAABFQAAAaAGAJQAVAlA7gFIEZADIAAgnQAAi6AhihQAOhDAJADQAKADAABfQAABNgJBvQgLB+gEA7IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACAUgCAVQAAAbgHAlQgMA7gZAOIhEAAQgsgRg3gRQACAPAAATIAAAQIgtAAIAAgPQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgHQgIgMAAgvQAAhKAYhmQAdh8A1hqQCTklEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBcABBHIABAAIADAEQADAEAAALQAAAMgPAQQAKAIAMARQA8BPAABFQAAAlgFAGQgLARg6AOIh1AAQAFAsAAAjQAAAZgGARgAsWFkQAAAFBRAdIAyASQCFAsAdAAQAgAAABhDIgDhBIkAAAQgZgEgUgFIgWAtgALADBQgxAfghAKIAPBBIALA7QAWgGAogEQBagHAAgYQAAgtgmgyQgZgggIgOIgZARg");
	this.shape_13.setTransform(84.4,51.6);

	this.addChild(this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.YardSteve02NoFeet = function() {
	this.initialize();

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#421F10").s().p("AlLBbIgFAAIgEgBIgCABIgDgDQgHgHgEgIQgEgHAAgFQAAgOAKgWIAEgKQALgTAQgPIAZAUIAIAHQgLAMgFAPQgDAMgCAUIAAARIgFAEIgHACIgIABIgEAAgAEwAGQgCgDgEgDIgBgCQgHgeARggQAIgPANgLIAhAWIgBABIgKAmIgFATQgDAJgJABIgEAEIgBABQgEADgIAAQgJAAgDgCg");
	this.shape.setTransform(93.1,142.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Al/LtQgFABgJAAIgaAAIgFAAQgZAAgFgWQAghkAFgGQAigiAygTQAPgGARgCIAdgBQAGgCAhAGQAoAGAPAMIAgAYQAKAHAHAKQAFAIAAAEIAAAEQADACAAADIAAABQADAJAAAJQADAagLAAQAAAAgBgBQAAAAAAAAQgBAAAAAAQAAgBgBAAIgDABQgFAAgCgEIAAgDIAAgFIgCABIAAAAQgDADgDAAQgGAAgIgdIgEAAQgVAAgagGIgWAAQhHAAgWA1IgOAkQgIAQgSAAQgGAAgGgBgAmULdIAEABIAFAAIAIgBIAGgDIAFgEIABgRQABgUADgLQAFgQALgMIAKgJQASgOAwgDIAEAAIAPAAIAbABIAFAAIAPACIAAAAIAHACQAQABAPAEQAMADACACIAHAJIACAEIAEAHQAHANAEAAQAGAAAAgJIAAgDQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAgSgrgZIAAAAIgDgCQgugZglgEIgwAAIgdAIIgHAEQgVAIgSAPIgFAFQgRAPgKAWIgFAKQgJAWAAAOQAAAEADAHQAEAIAIAIIADADIABgCIAFABgADqKWIgFABQgaAAgFgZIAAgaQAAhBBGgpQAOgJAMgDQAOgJA1AEQBCAFAFARIACABQAMAFADAOQAEAGADAJQADASAAARIAAAMQgBAJgGAAIgCAAQAAAAgBABQAAAAAAAAQgBAAAAAAQAAABgBAAQgFAAgCgKQAIgBAAgJIgBgDIAAgFQgBgUgGgQQgKgZgSgCIAAAAIgagIIgvAAIggANQgaAMgDAGQgKAHgJAHQgMAMgIAPQgRAfAHAfIABAEQAEACACADQADACAJAAQAHAAAEgCIACgBIADgEQAJgEADgIIAFgUIALgmIAAAAQALgLAcgFQAQgHAogHIAVgDQAOAHAFAKQABADACATQACAOAFABIAAACQAAACgCACQgBABAAAAQgBAAAAABQgBAAAAAAQgBAAAAAAQgKAAgBgKIACgQQAAgEgFgKIgBgDQgXAHgVABQgEAAgaANIgFACQgMAKgKANQgDADgKAhQgOAggaAAQgTAAgJgLgAjupfQgzgPgbgVQgTgRgDgRQgDgVACgLQAEgUATgKQASgJAsgBIBUABQBVAAA4AXQA2AXgJAjQgFASgJAJQgIAKgeAPQgwAYgwABIgFAAQgoAAg9gRg");
	this.shape_1.setTransform(100,78.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#65301B").s().p("AiyBGIgEgHIgCgEIgGgJQgDgCgMgDQgPgEgQgBIgGgCIgBAAIgPgCIgEAAIgcgBIgPAAIgDAAQgxADgSAOIgKAJIgIgHIgYgWIAFgFQASgPAVgHIAHgFIAdgGIAxAAQAkAEAuAXIADACIAAAAQArAZAAASQABABgBABQAAAAAAABQAAAAAAABQAAAAgBABIABADQgBAJgGAAQgEAAgHgNgAGYAFIgBAAQgFgBgCgMQgBgTgCgDQgFgKgOgHIgUADQgoAHgRAIQgcAEgLALIghgWQAJgIALgHQACgGAagMIAggNIAvAAIAaAIIAAABQASABAKAZQAGAQABAUIAAAFIABADQAAAHgHABIgDAAg");
	this.shape_2.setTransform(103,137.6);

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3A628C").s().p("AAUG3IgEAAIAAgWIAAgBIAAgBIAAgBIAAgIIAAgWIgBgBIAAAAIAAhJIAAhJICLAAIgEDfIgOABQhrgWgJAAgAiUCdQgFgKgBgZQAAhFAahfIAAgCQAdhxA0hiQBEiDBdhKQhBBdgxCIQgUA/gVB4IgXCZIAAANIAFBHIgIAAIgOAAQgwAAgTggg");
	this.shape_3.setTransform(20,59.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#4C7FB5").s().p("Al4H8Ig8gMIgYgEIg2gMIAOgBIAEjeIAVAAIAAADQAagEAYABIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQADg2AKhuIACgWQAJhuAAhOQAAhegKgEQgJgCgOBCQgWBqgHBxIACACIgGBaIAAAEIAAAEIAAAEIAAATIAAAFIAAAoIkQgEIgFhHIAAgMIAXiZQAVh5AWg+QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQBwCCAQAWQA9BSAEA/QgbAcglAOQgkAPgVAFIgNgmQgPgngYgtQgXgrgVggQgWg9gLgZQgRgqgfgLQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg+ALhIAfIgGgBIgBAEQhoAriagCIm0APQgxgEhmgSg");
	this.shape_4.setTransform(85.2,58);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#82AD56").s().p("AmAA1QgHgZgIgxIgIgxIAAAAIAEACIAAAAQA5ARAfAHIAfAGIALBtQhHgMgogGgAE7AsQAAgegDgOIgBgBIAAgIIBggBIABAzIgKAAIhQAEIgDgBg");
	this.shape_5.setTransform(65.8,116.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#99CC66").s().p("AmYBGIhFgMIgLhsIA1AKQBrATBgAFIEngFIgBAUQABAUABAMQgBAOgDAKQgLAAgnAGIg1AJgADLgXIBAgBQBZgMB1ghIABAAQAPAqAAAOQAAANidASQgwAFhPAFg");
	this.shape_6.setTransform(86.2,117.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AlpKGQgdgHgggDIAAABIhugWQgygJAAgXIAAgCQgHglgGhEIgEgoQgGACgKgBQgTAAgEgfIAAgGIAAgeIAAgCIAAgBIAAgLIABgNIABgSIAJiHIg/AAIgDAAIgDgBQgWgEhEgbQgKgIgFgHQgJgMAAgvIAAgDIAAgHIAAgEIABgPQAEg7ARhNIACgJQAdh+A2hqQCTklENgZIAAgFIEdAAQEsAwCDBNQA1AfAsAuIBMBYIBxCjQAiAuAQAfQAWAmAQAyIACAAIADAEQACAEAAALQAAAKgvAsIgaARQgxAfghAKIAPBBIALA7QAKBRABAjQAAAIgNAaQgMAZgBgDQgbAThBAaIgHACIAEALQAMAgAAAdQAAAWgFAIQgIAMgcAHQgLADgXAEIAAAAQh7AQiAANIhDAAIgoAAQgEAMgHAGQgGAJgNABQhOAGhEAIgAooHvQAIAzAHAZQAoAGBHAMIBEAMIFsAAIA3gJQAmgGALAAQADgLACgNQgCgMAAgXIAAgTIkmAFQhggFhsgTIg1gKIgegHQgfgHg5gRIAAAAIgEgCIAAAAIAIAxgAE9H5Ig/ABIhgABIAAAIIABABQACAQAAAeQAKABBJgEIALAAQBPgFAxgFQCdgUgBgNQAAgOgPgqIAAAAQh2AhhZAMgApSEHIAABJIAAAAIABABIAAAWIAAAIIAAABIAAABIAAABIAAAWIAEAAQAJABBrAVIA2ALIAYAFIA9ALQBlATAyADIGzgOQCaABBogrIACgDIAFAAQBIgfA/gLQgSg0gLhCQgQhwgNg6QgFgWhEiEQg+h6AAg2QAAgWABgRQAfALASAqQAKAZAWA8QAWAgAWAsQAYAuAPAlIANAmQAVgFAkgOQAlgPAbgZQgEhBg9hTQgQgWhwiCIhGhTQgpgsgxgdQh9hMkYgtIkaAAQh3AQheBJQhdBKhGCDQg0BigdBxIAAACQgaBfAABFQABAaAFAJQAWAlA7gFIAIAAIEQADIAAgnIAAgGIAAgSIAAgFIAAgDIAAgEIAGhYIgCgCQAIh0AVhpQAOhDAJADQAKADAABfQAABOgJBwIgCATQgJBugEA2IACADQAAADgCADIgBAIIgDAEQgFADgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAABJg");
	this.shape_7.setTransform(81.1,64.6);

	// Layer 7
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#421F10").s().p("AjWBNIiQAAQhNgIgLgFQhKgPgYgPQgWgOAAgZIABgJQACATATAMQAYAQBKAOQALAFBNAIICQAAIB2AAQAygRAFgHIAIgHQAEgEACgFQACAEAAADQAAAKgIAIIgIAHQgFAIgyARgAC3gGQghAAgkgFQgkgGgHgHQghgZAAgGQAAgHABgGQAGAIAaAUQAHAGAkAGQAkAGAhAAID4AAQArgEAxgTQALgEAHgFIAMgGQAKgHADgJIABAJQAAANgOAKQgFAEgHADQgHAEgLAFQgxATgrADg");
	this.shape_8.setTransform(93,150.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AovBPQgagXAAgtQAAgIALgNIADgEQAEgHAIgEIALgIIAJgEQAMgLAvgFIAegGIAnAAIAQgCIAqAAIAZAHIACAAIBEAIIA7AHIAXAEIAHABIAhAFIAOAEQA1ALAZAMQAOADAGAEQALAFADAWIAAABQADAMgCALQAAAWgJALIgDAEQgaAlhOAAQhuAGgzAAQjPAAhCg5gAnYgnIgJADIgNAEIgFACIghAJQgQAIgEAHQgIAEgCAOIAAAJQAAAbAWAOQAYAQBKAOQALAFBNAIICPAAIB2AAQAzgRAFgHIAIgHQAIgJAAgKQAAgDgDgDIgCgDIgBgBIAAAAQAAgFgCgEQgFgNgTgJIgFgCIgDgBIgEgDIgPgFIgcgJIgOgFIgUgFIgIgCIgygGIgjgFIgNgBIhEgIIgQgCIgpgFIhMAAIgXAGgACaAwQhWgNgggWQgagPAAgeQAAgiAcgRQAEgMAmgPQAhgOAigEIAggHIBTAAQAoACAPADQAEAAACADQAUABAAAJIgBACIgMgCIgMgBIgKAAIh2AAIgKABIgWADIgPADQgdAHgWAMIgDABIgCABIgFADQgaAQgEARQgGAFgDAKQgBAFAAAIQAAAGAgAXQAIAGAkAGQAjAGAiAAID3AAQAsgEAxgRQALgEAHgFQAHgDAFgDQAOgKAAgNIgCgJQgDgKgLgBIgEgCIgGgEIgGgDQgbgNg+gOIgYgGQgxgLgigEIAwAAIAOABQANgFBEATIARAGQA3ANAMAJQAKAFAEAMQAEAKgCALQAAAfgYAOQglAYhtANg");
	this.shape_9.setTransform(92.6,147.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#65301B").s().p("AjWBoIiQAAQhNgIgLgFQhJgOgZgQQgSgMgDgWQACgQAIgEQADgHAQgFIAigKIAFgCIANgEIAJgDIAXgFIBLAAIAqAEIAQACIBDAIIAOABIAjAFIAyAHIAIACIAUACIANAFIAdALIAPAGIAEACIACABIAFACQAUAJAFANQACAEAAAFIAAABIAAAAIADADQgCAFgEAEIgHAGQgGAIgyARgAC4ATQgiABgkgGQgjgGgIgHQgagRgFgIQACgKAHgFQAEgRAagQIAFgCIACgCIADgBQAWgLAdgHIAOgEIAXgDIAJgBIB3AAIAKABIAMABIAMABIABAAQAhAEAyAMIAYAFQA9AOAcANIAGADIAGAEIAEACQALACADAJQgDAJgJAGIgMAHQgHAFgLAEQgxARgsADg");
	this.shape_10.setTransform(92.9,145.9);

	this.addChild(this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,162.2,161.1);


(lib.YardSteve02 = function() {
	this.initialize();

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#421F10").s().p("AlLBbIgFAAIgEgBIgCABIgDgDQgHgHgEgIQgEgHAAgFQAAgOAKgWIAEgKQALgTAQgPIAZAUIAIAHQgLAMgFAPQgDAMgCAUIAAARIgFAEIgHACIgIABIgEAAgAEwAGQgCgDgEgDIgBgCQgHgeARggQAIgPANgLIAhAWIgBABIgKAmIgFATQgDAJgJABIgEAEIgBABQgEADgIAAQgJAAgDgCg");
	this.shape.setTransform(93.1,142.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AmKLjQgFABgFAAIgCAAQgIAKgLAAQgaAAgFgWQAhhkAFgGQAigiAxgTQAigOAbAAQAGgBAMAAQApAAAkAcQAWASAFAKIAEADQAXAQAAAGIAAAEQACACAAADQAAAEgCACQgEADgLAAQgJAAgGgCIgLAAQgVAAgagGIgWAAQhIAAgVA1IgOAkQgIAQgSAAQgSAAgIgLgAmSLdIAFABIAEAAIAJgBIAGgDIAFgEIAAgRQACgUADgLQAFgQALgMIAKgJQARgOAxgDIADAAIAPAAIAcABIAEAAIAPACIABAAIAHACQAgAJAGgHIABgDIAGAAIgFgHIgBAHIAAgBQAAgBgLgKIAFgEIAAAAIAAAAIgBgBIghgMIgKgDQgZgHgVgDQhJgJguAnIgFAFQgRAPgKAWIgFAKQgKAWAAAOQAAAEAEAHQAEAIAHAIIADADIACgCIAEABgADtKWIgFABQgaAAgGgZIAAgaQABhBBGgpQAbgRAUAEQAKABAGAHIALgBIAOAAQBBAAAMAMQAQAHAAAUIAAACQAEACABADQAAAGgKACQgCACgEABIgRABIgFABQgXAHgVABQgEAAgbANIgEACQgMAKgLANQgCADgKAhQgOAggaAAQgTAAgJgLgAFCIIIgDAAQgjALgWATQgNAMgHAPQgRAfAHAfIABAEQADACACADQAEACAJAAQAHAAAEgCIABgBIAEgEQAJgEADgIIAFgUIALgmIAAAAQALgLAcgFQAQgHAogHQAWgDAOgBQAEgFgCgGIgBgDIgHgDQgKgGg2AAQgPAAgTAEgAjspfQgygPgbgVQgTgRgEgRQgCgVABgLQAFgUATgKQASgJAsgBIBUABQBVAAA4AXQA2AXgJAjQgFASgKAJQgHAKgeAPQgxAYgwABIgEAAQgoAAg+gRg");
	this.shape_1.setTransform(99.7,78.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#65301B").s().p("Al+A7IgYgWIAFgFQAuglBJAIQAWADAYAHIAKADIAhAMIABAAIAAAAIAAABIgFADQALAKAAACIAAAAIAAAAIgBADQgGAHgggJIgHgBIAAAAIgPgDIgFAAIgbgBIgPAAIgDABQgxACgSAOIgKAJIgIgHgADwgfQAWgUAkgLIACAAQAUgEAPAAQA1AAAKAGIAIADIAAAEQADAFgEAFQgPABgWADQgoAHgQAIQgcAEgLALIghgWg");
	this.shape_2.setTransform(101.9,137.1);

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#DDBAA4").s().p("AlhBSQgXgZgDgzQABg+gFgkQAgAEAdAGIADAAQAGB6BFBUQhRgMgcgegADtAqQgIgggBg3QgBg6ACgUIBCAAQgJCyBigFQgWACgVAAQg1AAgzgKg");
	this.shape_3.setTransform(77.2,137.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3F6996").s().p("AAUG3IgEAAIAAgWIAAgBIAAgBIAAgBIAAgIIAAgWIgBgBIAAAAIAAhJIAAhJICLAAIgEDfIgOABQhrgWgJAAgAiUCdQgFgKgBgZQAAhFAahfIAAgCQAdhxA0hiQBEiDBdhKQhBBdgxCIQgUA/gVB4IgXCZIAAANIAFBHIgIAAIgOAAQgwAAgTggg");
	this.shape_4.setTransform(20,59.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#4C7FB5").s().p("Al4H8Ig8gMIgYgEIg2gMIAOgBIAEjeIAVAAIAAADQAagEAYABIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQADg2AKhuIACgWQAJhuAAhOQAAhegKgEQgJgCgOBCQgWBqgHBxIACACIgGBaIAAAEIAAAEIAAAEIAAATIAAAFIAAAoIkQgEIgFhHIAAgMIAXiZQAVh5AWg+QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQBwCCAQAWQA9BSAEA/QgbAcglAOQgkAPgVAFIgNgmQgPgngYgtQgXgrgVggQgWg9gLgZQgRgqgfgLQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg+ALhIAfIgGgBIgBAEQhoAriagCIm0APQgxgEhmgSg");
	this.shape_5.setTransform(85.2,58);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#27754D").s().p("AmAA1QgHgZgIgxIgIgxIAAAAIAEACIAAAAQA5ARAfAHIAfAGIALBtQhHgMgogGgAE7AsQAAgegDgOIgBgBIAAgIIBggBIABAzIgKAAIhQAEIgDgBg");
	this.shape_6.setTransform(65.8,116.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EACFB7").s().p("AmcCGIgVgDQhFhUgGh6IFLAAIgBACIAABOQgTgEgWgBIAOAAQAGAAAEgDQAEgEAAgFQAAgKgOgFIiUAAIg0ARQggAKAAALIABAHQADAIAJAAIAegKIAvgPIAUAAIgFABQgIACAAAQQAAACAEADIAQAOQCRgFA6AdQAjASAAAcQAAAHgUAIQgaAKgsACQhCAEg2AAQhFAAgzgGgABqh0QCBgNBJgKQgCAPAEAUIAAAdIgiAAQhHASgMAEQgMAEAAAMQAAAGADADIARAOQATgEBEgGQBAgGAPAAQBfAAAfAQQAPAJAAARQADAIgGAIQgMAPgsAGQheAKiagCIgDAAIgDAAIgFAAQhdAAAJitg");
	this.shape_7.setTransform(96.1,136.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#339966").s().p("AmYBGIhFgMIgLhsIA1AKQBrATBgAFIEngFIgBAUQABAUABAMQgBAOgDAKQgLAAgnAGIg1AJgADLgXIBAgBQBZgMB1ghIABAAQAPAqAAAOQAAANidASQgwAFhPAFg");
	this.shape_8.setTransform(86.2,117.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("Aj+MAQiogPgVgwQgLgbgIhCQgHg7AAg4Ig/gLQgygJAAgYIAAgBQgHgmgGhEIgEgoQgGACgKAAQgTAAgEggIAAgFIAAgeIAAgCIAAgCIAAgLIABgMIABgSIAJiHIg/AAIgDAAIgDgBQgWgEhEgbQgKgIgFgIQgJgLAAgtIAAgEIAAgGIAAgFIABgOQAEg9ARhOIACgIQAdh/A2hqQCTkkENgZIAAgFIEdAAQEsAwCDBNQA1AfAsAuIBMBYIBxCjQAiAuAQAeQAWAmAQAyIACAAIADAFQACAEAAALQAAALgvAsIgaARQgxAgghAKIAPA/IALA7QAKBRABAiQAAAJgNAaQgMAZgBgDQgbAThBAaIgHACIAEAKQAMAhAAAdQAAAWgFAHQgIAMgcAIQgLADgXADQgBAcgFAgIgCALIBuAAQBCAXAPATQAHAKABAkQAAAkgGAJQgQAdhDACQikALjZAAIgrgEQgJgHgHgWQgLgdgCgrQgCghADgmIADgoIgLAAQgEAMgHAFQgGAJgNACQhOAGgYAHIgDAoIgEAzQBEAWAUAlQAIAOAAAaQAAAcghATQgkAWg7AAQieAAhLgHgAmhJeQACA0AXAYQAcAeBSANIAUADQBbAKCWgIQApgDAagKQAUgIAAgGQAAgdgjgRQg4geiRAGIgQgOQgEgEAAgBQAAgQAIgDIAFgBIgUAAIgvAPIgdAKQgKAAgDgHIgBgIQAAgMAhgLIAzgQICVAAQANAEAAALQAAAFgDAEQgFAEgFAAIgPAAQAXACATADIAAhQIABgCIlMAAIgDAAQgdgGgggDQAGAkgBA/gAC9IpQACA6AHAfQBIAPBMgGIACgBIADAAQCbADBdgLQAsgFAMgQQAGgHgDgIQAAgUgPgIQgfgRheAAQgQAAhAAGQhEAHgTAEIgRgOQgDgEAAgGQAAgMAMgEQAMgEBIgSIAhAAIAAgcQgDgUABgQQhIAKiBANIhDAAQgCAVABA5gAooFtQAIA0AHAZQAoAFBHANIBEAMIFsAAIA3gJQAmgGALgBQADgKACgNQgCgMAAgXIAAgTIkmAEQhggEhsgTIg1gLIgegGQgfgHg5gSIAAAAIgEgBIAAAAIAIAwgAE9F4Ig/ABIhgABIAAAIIABABQACAQAAAeQAKABBJgEIALAAQBPgFAxgGQCdgTgBgNQAAgOgPgqIAAAAQh2AghZANgApSCGIAABJIAAAAIABABIAAAVIAAAJIAAABIAAABIAAABIAAAWIAEAAQAJAABrAWIA2ALIAYAEIA9AMQBlATAyADIGzgPQCaACBogrIACgDIAFAAQBIgfA/gLQgSg0gLhDQgQhwgNg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAfAKASAqQAKAZAWA9QAWAgAWAsQAYAuAPAnIANAmQAVgFAkgOQAlgPAbgbQgEhBg9hTQgQgWhwiCIhGhTQgpgsgxgeQh9hLkYgtIkaAAQh3APheBKQhdBJhGCEQg0BhgdByIAAACQgaBhAABFQABAZAFAKQAWAjA7gFIAIAAIEQADIAAgmIAAgFIAAgTIAAgEIAAgDIAAgEIAGhaIgCgDQAIhzAVhqQAOhCAJADQAKADAABeQAABPgJBwIgCAVQgJBugEA0IACADQAAADgCADIgBAIIgDAEQgFADgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAABJg");
	this.shape_9.setTransform(81.1,77.5);

	// Layer 7
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#421F10").s().p("AjWBNIiQAAQhNgIgLgFQhKgPgYgPQgWgOAAgZIABgJQACATATAMQAYAQBKAOQALAFBNAIICQAAIB2AAQAygRAFgHIAIgHQAEgEACgFQACAEAAADQAAAKgIAIIgIAHQgFAIgyARgAC3gGQghAAgkgFQgkgGgHgHQghgZAAgGQAAgHABgGQAGAIAaAUQAHAGAkAGQAkAGAhAAID4AAQArgEAxgTQALgEAHgFIAMgGQAKgHADgJIABAJQAAANgOAKQgFAEgHADQgHAEgLAFQgxATgrADg");
	this.shape_10.setTransform(93,150.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AovA7QgagXAAgqQAAgJALgPIADgDQAEgIAIgEIALgHQAdgSAFAkIABAKIAFAHIABACIgGgCQgNgEgHADQgJgLgIADQgKADgCASIAAAHQAAAbAWAOQAYAPBKAPQALAFBNAIICPAAIB2AAQAzgRAFgIIAIgHQAIgIAAgKQAAgDgDgEIgCgDIgNgLIgCABIgEgBQgHADgEAGIgFAGIgBABIgRALIlAAAIgQgDIgSgMQAlAKAxAEIDCAAIAKgDQAigJAcgEIACAAIAEgBIACgDIAHgMQAFgNAPgCQAKgJAJACQAOACADAYIAAABQADALgCAMQAAAWgJAKIgDAEQgaAmhOAAQhuAFgzAAQjPAAhCg5gACaAcQhWgMgggUQgagRAAgfQAAgtAygPQAUgGANAHQAQAHAAAVQAAAKgRASQgIAAgEACQADgLgBgGIgBgEQgDgJgMADQgSAEgFARQgBAGAAAHQAAAGAgAZQAIAHAkAGQAjADAiAAID3AAQAsgBAxgTQALgFAHgEQAHgDAFgEQAOgKAAgNIgCgJQgDgKgNgBQgSgBgMAPIgKAAIAVgSQANgMAIAAIACAAQAVABAGATQAEAKgCALQAAAegYAQQglAXhtAMg");
	this.shape_11.setTransform(92.6,149.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#65301B").s().p("AjWBLIiQAAQhNgIgLgFQhJgOgZgQQgSgMgDgUQACgRAKgDQAIgDAIALQAIgCAMADIAHACQAJAEALAFQAXANAfAUIASAMIAPADIFAAAIASgLIABgBIAEgGQAFgGAGgDIAFABIABgBIANALIADAEQgCAEgEAEIgHAHQgGAHgyARgAC4gHQgiAAgkgGQgjgGgIgGQgagUgFgIQAEgRASgEQANgCADAIIABAEQABAHgDAKQADgCAJAAQAJAAAFAHIACADIAwABQAsADAFABIADAAQAjgBAGgCICcgDQAogBAFgIQAEgHA7gBIAKgBQALgPASABQAOABADALQgDAIgJAHIgMAGQgHAFgLAEQgxATgsAEg");
	this.shape_12.setTransform(92.9,148.8);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,162.2,161.1);


(lib.YardSGreg02 = function() {
	this.initialize();

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#421F10").s().p("AlLBbIgFAAIgEgBIgCABIgDgDQgHgHgEgIQgEgHAAgFQAAgOAKgWIAEgKQALgTAQgPIAZAUIAIAHQgLAMgFAPQgDAMgCAUIAAARIgFAEIgHACIgIABIgEAAgAEwAGQgCgDgEgDIgBgCQgHgeARggQAIgPANgLIAhAWIgBABIgKAmIgFATQgDAJgJABIgEAEIgBABQgEADgIAAQgJAAgDgCg");
	this.shape.setTransform(93.1,142.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AmKLjQgFABgFAAIgCAAQgIAKgLAAQgaAAgFgWQAhhkAFgGQAigiAxgTQAigOAbAAQAGgBAMAAQApAAAkAcQAWASAFAKIAEADQAXAQAAAGIAAAEQACACAAADQAAAEgCACQgEADgLAAQgJAAgGgCIgLAAQgVAAgagGIgWAAQhIAAgVA1IgOAkQgIAQgSAAQgSAAgIgLgAmSLdIAFABIAEAAIAJgBIAGgDIAFgEIAAgRQACgUADgLQAFgQALgMIAKgJQARgOAxgDIADAAIAPAAIAcABIAEAAIAPACIABAAIAHACQAgAJAGgHIABgDIAGAAIgFgHIgBAHIAAgBQAAgBgLgKIAFgEIAAAAIAAAAIgBgBIghgMIgKgDQgZgHgVgDQhJgJguAnIgFAFQgRAPgKAWIgFAKQgKAWAAAOQAAAEAEAHQAEAIAHAIIADADIACgCIAEABgADtKWIgFABQgaAAgGgZIAAgaQABhBBGgpQAbgRAUAEQAKABAGAHIALgBIAOAAQBBAAAMAMQAQAHAAAUIAAACQAEACABADQAAAGgKACQgCACgEABIgRABIgFABQgXAHgVABQgEAAgbANIgEACQgMAKgLANQgCADgKAhQgOAggaAAQgTAAgJgLgAFCIIIgDAAQgjALgWATQgNAMgHAPQgRAfAHAfIABAEQADACACADQAEACAJAAQAHAAAEgCIABgBIAEgEQAJgEADgIIAFgUIALgmIAAAAQALgLAcgFQAQgHAogHQAWgDAOgBQAEgFgCgGIgBgDIgHgDQgKgGg2AAQgPAAgTAEgAjspfQgygPgbgVQgTgRgEgRQgCgVABgLQAFgUATgKQASgJAsgBIBUABQBVAAA4AXQA2AXgJAjQgFASgKAJQgHAKgeAPQgxAYgwABIgEAAQgoAAg+gRg");
	this.shape_1.setTransform(99.7,78.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#65301B").s().p("Al+A7IgYgWIAFgFQAuglBJAIQAWADAYAHIAKADIAhAMIABAAIAAAAIAAABIgFADQALAKAAACIAAAAIAAAAIgBADQgGAHgggJIgHgBIAAAAIgPgDIgFAAIgbgBIgPAAIgDABQgxACgSAOIgKAJIgIgHgADwgfQAWgUAkgLIACAAQAUgEAPAAQA1AAAKAGIAIADIAAAEQADAFgEAFQgPABgWADQgoAHgQAIQgcAEgLALIghgWg");
	this.shape_2.setTransform(101.9,137.1);

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#BC9A72").s().p("AlhBSQgXgZgDgzQABg+gFgkQAgAEAdAGIADAAQAGB6BFBUQhRgMgcgegADtAqQgIgggBg3QgBg6ACgUIBCAAQgJCyBigFQgWACgVAAQg1AAgzgKg");
	this.shape_3.setTransform(77.2,137.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D5AE81").s().p("AmcCGIgVgDQhFhUgGh6IFLAAIgBACIAABOQgTgEgWgBIAOAAQAGAAAEgDQAEgEAAgFQAAgKgOgFIiUAAIg0ARQggAKAAALIABAHQADAIAJAAIAegKIAvgPIAUAAIgFABQgIACAAAQQAAACAEADIAQAOQCRgFA6AdQAjASAAAcQAAAHgUAIQgaAKgsACQhCAEg2AAQhFAAgzgGgABqh0QCBgNBJgKQgCAPAEAUIAAAdIgiAAQhHASgMAEQgMAEAAAMQAAAGADADIARAOQATgEBEgGQBAgGAPAAQBfAAAfAQQAPAJAAARQADAIgGAIQgMAPgsAGQheAKiagCIgDAAIgDAAIgFAAQhdAAAJitg");
	this.shape_4.setTransform(96.1,136.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3F6996").s().p("AAUG3IgEAAIAAgWIAAgBIAAgBIAAgBIAAgIIAAgWIgBgBIAAAAIAAhJIAAhJICLAAIgEDfIgOABQhrgWgJAAgAiUCdQgFgKgBgZQAAhFAahfIAAgCQAdhxA0hiQBEiDBdhKQhBBdgxCIQgUA/gVB4IgXCZIAAANIAFBHIgIAAIgOAAQgwAAgTggg");
	this.shape_5.setTransform(20,59.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#4C7FB5").s().p("Al4H8Ig8gMIgYgEIg2gMIAOgBIAEjeIAVAAIAAADQAagEAYABIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQADg2AKhuIACgWQAJhuAAhOQAAhegKgEQgJgCgOBCQgWBqgHBxIACACIgGBaIAAAEIAAAEIAAAEIAAATIAAAFIAAAoIkQgEIgFhHIAAgMIAXiZQAVh5AWg+QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQBwCCAQAWQA9BSAEA/QgbAcglAOQgkAPgVAFIgNgmQgPgngYgtQgXgrgVggQgWg9gLgZQgRgqgfgLQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg+ALhIAfIgGgBIgBAEQhoAriagCIm0APQgxgEhmgSg");
	this.shape_6.setTransform(85.2,58);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#27754D").s().p("AmAA1QgHgZgIgxIgIgxIAAAAIAEACIAAAAQA5ARAfAHIAfAGIALBtQhHgMgogGgAE7AsQAAgegDgOIgBgBIAAgIIBggBIABAzIgKAAIhQAEIgDgBg");
	this.shape_7.setTransform(65.8,116.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#339966").s().p("AmYBGIhFgMIgLhsIA1AKQBrATBgAFIEngFIgBAUQABAUABAMQgBAOgDAKQgLAAgnAGIg1AJgADLgXIBAgBQBZgMB1ghIABAAQAPAqAAAOQAAANidASQgwAFhPAFg");
	this.shape_8.setTransform(86.2,117.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("Aj+MAQiogPgVgwQgLgbgIhCQgHg7AAg4Ig/gLQgygJAAgYIAAgBQgHgmgGhEIgEgoQgGACgKAAQgTAAgEggIAAgFIAAgeIAAgCIAAgCIAAgLIABgMIABgSIAJiHIg/AAIgDAAIgDgBQgWgEhEgbQgKgIgFgIQgJgLAAgtIAAgEIAAgGIAAgFIABgOQAEg9ARhOIACgIQAdh/A2hqQCTkkENgZIAAgFIEdAAQEsAwCDBNQA1AfAsAuIBMBYIBxCjQAiAuAQAeQAWAmAQAyIACAAIADAFQACAEAAALQAAALgvAsIgaARQgxAgghAKIAPA/IALA7QAKBRABAiQAAAJgNAaQgMAZgBgDQgbAThBAaIgHACIAEAKQAMAhAAAdQAAAWgFAHQgIAMgcAIQgLADgXADQgBAcgFAgIgCALIBuAAQBCAXAPATQAHAKABAkQAAAkgGAJQgQAdhDACQikALjZAAIgrgEQgJgHgHgWQgLgdgCgrQgCghADgmIADgoIgLAAQgEAMgHAFQgGAJgNACQhOAGgYAHIgDAoIgEAzQBEAWAUAlQAIAOAAAaQAAAcghATQgkAWg7AAQieAAhLgHgAmhJeQACA0AXAYQAcAeBSANIAUADQBbAKCWgIQApgDAagKQAUgIAAgGQAAgdgjgRQg4geiRAGIgQgOQgEgEAAgBQAAgQAIgDIAFgBIgUAAIgvAPIgdAKQgKAAgDgHIgBgIQAAgMAhgLIAzgQICVAAQANAEAAALQAAAFgDAEQgFAEgFAAIgPAAQAXACATADIAAhQIABgCIlMAAIgDAAQgdgGgggDQAGAkgBA/gAC9IpQACA6AHAfQBIAPBMgGIACgBIADAAQCbADBdgLQAsgFAMgQQAGgHgDgIQAAgUgPgIQgfgRheAAQgQAAhAAGQhEAHgTAEIgRgOQgDgEAAgGQAAgMAMgEQAMgEBIgSIAhAAIAAgcQgDgUABgQQhIAKiBANIhDAAQgCAVABA5gAooFtQAIA0AHAZQAoAFBHANIBEAMIFsAAIA3gJQAmgGALgBQADgKACgNQgCgMAAgXIAAgTIkmAEQhggEhsgTIg1gLIgegGQgfgHg5gSIAAAAIgEgBIAAAAIAIAwgAE9F4Ig/ABIhgABIAAAIIABABQACAQAAAeQAKABBJgEIALAAQBPgFAxgGQCdgTgBgNQAAgOgPgqIAAAAQh2AghZANgApSCGIAABJIAAAAIABABIAAAVIAAAJIAAABIAAABIAAABIAAAWIAEAAQAJAABrAWIA2ALIAYAEIA9AMQBlATAyADIGzgPQCaACBogrIACgDIAFAAQBIgfA/gLQgSg0gLhDQgQhwgNg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAfAKASAqQAKAZAWA9QAWAgAWAsQAYAuAPAnIANAmQAVgFAkgOQAlgPAbgbQgEhBg9hTQgQgWhwiCIhGhTQgpgsgxgeQh9hLkYgtIkaAAQh3APheBKQhdBJhGCEQg0BhgdByIAAACQgaBhAABFQABAZAFAKQAWAjA7gFIAIAAIEQADIAAgmIAAgFIAAgTIAAgEIAAgDIAAgEIAGhaIgCgDQAIhzAVhqQAOhCAJADQAKADAABeQAABPgJBwIgCAVQgJBugEA0IACADQAAADgCADIgBAIIgDAEQgFADgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAABJg");
	this.shape_9.setTransform(81.1,77.5);

	// Layer 7
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#421F10").s().p("AjWBNIiQAAQhNgIgLgFQhKgPgYgPQgWgOAAgZIABgJQACATATAMQAYAQBKAOQALAFBNAIICQAAIB2AAQAygRAFgHIAIgHQAEgEACgFQACAEAAADQAAAKgIAIIgIAHQgFAIgyARgAC3gGQghAAgkgFQgkgGgHgHQghgZAAgGQAAgHABgGQAGAIAaAUQAHAGAkAGQAkAGAhAAID4AAQArgEAxgTQALgEAHgFIAMgGQAKgHADgJIABAJQAAANgOAKQgFAEgHADQgHAEgLAFQgxATgrADg");
	this.shape_10.setTransform(93,150.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AovA7QgagXAAgqQAAgJALgPIADgDQAEgIAIgEIALgHQAdgSAFAkIABAKIAFAHIABACIgGgCQgNgEgHADQgJgLgIADQgKADgCASIAAAHQAAAbAWAOQAYAPBKAPQALAFBNAIICPAAIB2AAQAzgRAFgIIAIgHQAIgIAAgKQAAgDgDgEIgCgDIgNgLIgCABIgEgBQgHADgEAGIgFAGIgBABIgRALIlAAAIgQgDIgSgMQAlAKAxAEIDCAAIAKgDQAigJAcgEIACAAIAEgBIACgDIAHgMQAFgNAPgCQAKgJAJACQAOACADAYIAAABQADALgCAMQAAAWgJAKIgDAEQgaAmhOAAQhuAFgzAAQjPAAhCg5gACaAcQhWgMgggUQgagRAAgfQAAgtAygPQAUgGANAHQAQAHAAAVQAAAKgRASQgIAAgEACQADgLgBgGIgBgEQgDgJgMADQgSAEgFARQgBAGAAAHQAAAGAgAZQAIAHAkAGQAjADAiAAID3AAQAsgBAxgTQALgFAHgEQAHgDAFgEQAOgKAAgNIgCgJQgDgKgNgBQgSgBgMAPIgKAAIAVgSQANgMAIAAIACAAQAVABAGATQAEAKgCALQAAAegYAQQglAXhtAMg");
	this.shape_11.setTransform(92.6,149.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#65301B").s().p("AjWBLIiQAAQhNgIgLgFQhJgOgZgQQgSgMgDgUQACgRAKgDQAIgDAIALQAIgCAMADIAHACQAJAEALAFQAXANAfAUIASAMIAPADIFAAAIASgLIABgBIAEgGQAFgGAGgDIAFABIABgBIANALIADAEQgCAEgEAEIgHAHQgGAHgyARgAC4gHQgiAAgkgGQgjgGgIgGQgagUgFgIQAEgRASgEQANgCADAIIABAEQABAHgDAKQADgCAJAAQAJAAAFAHIACADIAwABQAsADAFABIADAAQAjgBAGgCICcgDQAogBAFgIQAEgHA7gBIAKgBQALgPASABQAOABADALQgDAIgJAHIgMAGQgHAFgLAEQgxATgsAEg");
	this.shape_12.setTransform(92.9,148.8);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,162.2,161.1);


(lib.YardGreg04 = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BC9A72").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D5AE81").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag3DAIAAgOQAAgdADgRIgxgNIgggKQgtgOgTgQQgWgUAAggQAAgZAQgkQgKgHgGgIQgIgMAAgvQAAgmAHgtIAsAEQgGAmAAAhQAAAZAGAJQAVAlA7gFIEXAEIAAgoIAAgUQAaAVATAaIgDAiIACACQAAADgCAEIgBAHIgEAEQgEAEgMAAIgHAAIAAABQACASgCAVQAAAcgHAkQgMA8gZAOIhEAAQgsgSg1gQQACAOAAAUIAAAPgAiuAgQAAAGBRAdIAyARQCDAsAdAAQAgAAABhDIgDg+IjnAAIgXAAQgZgFgUgFIgWArg");
	this.shape_2.setTransform(22.8,84);

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhIA/QhZhEgDgiQgDgiAogcQAogcBPAAQBNAAA0AdQA0AdgJAsQgIAqgqA4QgpA3gdAEIgDAAQgeAAhThDg");
	this.shape_3.setTransform(82.1,16.2);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#392C38").s().p("AgDDLQgRgjg+ixIABgNIgHgGQgCgCAAgEQAAgOBMhyQAXgkAaAjQAWAcAGAoQAYAvACAIQACAKAAAvIgCAjQgDAcgDARIgSCmQAGgbAAAwQgBAwgFALQADAEgMAAQgSAAgpiQgAhQjvIgGgUQAAhiA5ANQAVAFAQASQASARAAAOQAAAOgRAZQgSAfgYAAQgiAAgNgTgAg1j9QAFAFAOAAQALAAACgEIAAAAQgQAHgMgHIgIgIIAEAHg");
	this.shape_4.setTransform(91.8,65);

	// Layer 2
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("ABxHlIgQgcQgBgJhGixQhDivAAgIQAAgDAghFQAehCgBgCQgegNgUgbQgRAOgJAEQgPAIgJgIQh9h+g/hiQg9hhgHhOQgCgXAXgFQAYgFgCAPQgIBEBBBnQA9BiBfBQIABgBIANgaQAQgiABgJQADgPAIgSQgdhRghhUIgghOQAAgUAHgDQAFgCAOgBQBKAlA+C/QAdALApAbQAyAiAVAUIATALIAQghQAUgnAZgmQATgXAcg5QAQghAYABIgzCYQgeBUgbAxQAAAHggAAQgFAAgygYQgOAggSAVQAKARAMAcQAOAeAIANQAeA1AFAXQAFAWgEBKQgFBygVCgQAAAJgGASQgIAXgMAPQgKgEgQgagAAxArIAAgCIgBABIABABIAAAAg");
	this.shape_5.setTransform(84.1,52.9);

	// Layer 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#764F4F").s().p("AAUG3IgEAAIAAgWIAAgBIAAgBIAAgBIAAgIIAAgWIgBgBIAAAAIAAhJIAAhJICLAAIgEDfIgOABQhrgWgJAAgAiUCdQgFgKgBgZQAAhFAahfIAAgCQAMguAQgsQAXg/Aeg6QBEiDBdhKQhBBdgxCIQgNAsgMArIAAAAIgEARIgjDpIAAAMIAFBHIgIAAIgOAAQgwAAgTggg");
	this.shape_6.setTransform(20,59.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#966363").s().p("Al4H8Ig8gMIgYgEIg2gMIAOgBIAEjeIAVAAIAAADQAagEAYABIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQADg2AKhuIACgWQAEgzADgvQACgxAAgpQAAhegKgEQgJgCgOBCQgKAwgHAyIAAAAIgDAaIgNC7IAAAEIAAAEIAAAEIAAAEIAAAKIAAAFIAAAFIAAAoIkQgEIgFhHIAAgLIAijpIAFgRIgBAAQANgrAPgsQAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQgxgEhmgSg");
	this.shape_7.setTransform(85.2,58);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#1E1E1E").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_8.setTransform(86,143.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#333333").s().p("AicDMIg8gLQiHgcgtglQBJgHATAAQBeAAB6AgIBbAXQgYASgSAKgAGYBtQiBgQgoggQBbgsA/gIQCDgNBEB2QgbACgTADIgDABQhtgIgagDgApLi6IgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAHgUAFQAAiPgliIgACNgrQgHgegJgSQADgbAAgTIAAgKIBWgBIAAAHQAAAQAQBHIAMA2QgRAFgQAIQghAQgTAUQgEgugMgug");
	this.shape_9.setTransform(85,130.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#666666").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_10.setTransform(86.9,128);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AjbMJQjFgig5gUQg7gUgBgmQg1kZABg+IAAgMIgQgGQgFACgJAAQgPAAgGgVIgCgLIAAgFIAAgdIAAgBIABgZIABgUIAJiHIg/AAIgDAAQgXgFhGgbQgKgIgFgIQgJgLAAgtQAAhHAWhhIACgIQAJgnALgkQAZhUAmhKQCTkkENgZIEdgFQEsAwCDBNQA1AfAsAuIBMBYIBxCjQBYBeAABGIACAAIADAFQACAEAAALQABALgPAQQgtAkgOAJQgxAgghAKIAPA/IALA7QAKBRABAiQAAAzgXANIgDgHQgbAThBAaIgMAaQgTAsgCAjQgBATAFAnQAAAQgGANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhBgDIk7AAQguACgZgYQgTgRAAgSQgIgXgMg2IgDgMIgBAEQgTBegTAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYguAGgAneKgQALAMBZAQQCRAZASAEIBiAAIB1AAQAQgKAagSIhbgXQh6ggheAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAFAHgAASKDQAgALAWAMQAFgkAUhtQARheAHgyIkuAEQhggEhsgTQgpgIgqgJQgzgLghgKIAEARQAlCIAACRQAUgFBYgHIA3gFQAzgDAdAAQCmAAB4AtgAGwIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADAsAOIEwAAIAJgBIADgBQATgDAbgCQg8hrhuAAQgOAAgPACgAE9F2IhAABIhWABIAAAKQAAATgDAbQAJASAHAeQAMAwAEAuQATgUAhgQQAQgIARgHQBAgXBQgDQAogBAkAEIgDgMQgCgMAAghQABhOAFgbQhNAVhsAPgApSCEIAABJIAAAAIABABIAAAVIAAAJIAAABIAAABIAAABIAAAWIAEAAQAJAABrAWIA2ALIAYAEIA9AMQBlATAyADIGzgPQCeACBpgtQBJggBAgLQgSg0gLhDQgQhwgNg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAANAuIAJAtQAGAcAIATQAWAgAWAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgTgbh1iHIhGhTQgpgsgxgeQh9hLkYgtIkaAAQh3APheBKQhdBJhGCEQgeA5gXBAQgQArgMAvIAAACQgaBhAABFQABAZAFAKQAWAjA7gFIAIAAIEQADIAAgmIAAgFIAAgFIAAgKIAAgEIAAgEIAAgDIAAgEIANi+IAEgaIAAAAQAHgyAJgwQAOhCAJADQAKADAABeQABApgDAyQgCAugFA2IgCAVQgJBugEA0IACADQAAADgCADIgBAIIgDAEQgFADgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAABJg");
	this.shape_11.setTransform(81.1,77.7);

	// Layer 6
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#D5AE81").s().p("AhJAgIgOg/QAggKAyggIAXgRQAIAOAZAhQAlAwAAAtQAAAXhYAIQgoAEgWAGIgLg7g");
	this.shape_12.setTransform(155.6,78.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AIUIEQAtgQApgIQgRg0gLhCQgRhwgMg6QgFgWhEiGQg+h4AAg2QAAgWABgRQAtABAMAtIAJAtQAGAdAJASQAVAeAXAsQAYAuAPAnIANAmQASgNAbgPQArgXAhgKQgGg/gzhJQgUgbh0iHIhGhTQgpgsgxgdQh9hMkZgtIkaAAQh2AQheBJQhdBKhGCDQg0BjgeBwQgZBhAABFQAAAaAGAJQAVAlA7gFIEZADIAAgnQAAi6AhihQAOhDAJADQAKADAABfQAABNgJBvQgLB+gEA7IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACAUgCAVQAAAbgHAlQgMA7gZAOIhEAAQgsgRg3gRQACAPAAATIAAAQIgtAAIAAgPQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgHQgIgMAAgvQAAhKAYhmQAdh8A1hqQCTklEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBcABBHIABAAIADAEQADAEAAALQAAAMgPAQQAKAIAMARQA8BPAABFQAAAlgFAGQgLARg6AOIh1AAQAFAsAAAjQAAAZgGARgAsWFkQAAAFBRAdIAyASQCFAsAdAAQAgAAABhDIgDhBIkAAAQgZgEgUgFIgWAtgALADBQgxAfghAKIAPBBIALA7QAWgGAogEQBagHAAgYQAAgtgmgyQgZgggIgOIgZARg");
	this.shape_13.setTransform(84.4,51.6);

	this.addChild(this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.YardFrank04 = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A4D30").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#975F3C").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag3DAIAAgOQAAgdADgRIgxgNIgggKQgtgOgTgQQgWgUAAggQAAgZAQgkQgKgHgGgIQgIgMAAgvQAAgmAHgtIAsAEQgGAmAAAhQAAAZAGAJQAVAlA7gFIEXAEIAAgoIAAgUQAaAVATAaIgDAiIACACQAAADgCAEIgBAHIgEAEQgEAEgMAAIgHAAIAAABQACASgCAVQAAAcgHAkQgMA8gZAOIhEAAQgsgSg1gQQACAOAAAUIAAAPgAiuAgQAAAGBRAdIAyARQCDAsAdAAQAgAAABhDIgDg+IjnAAIgXAAQgZgFgUgFIgWArg");
	this.shape_2.setTransform(22.8,84);

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhIA/QhZhEgDgiQgDgiAogcQAogcBPAAQBNAAA0AdQA0AdgJAsQgIAqgqA4QgpA3gdAEIgDAAQgeAAhThDg");
	this.shape_3.setTransform(82.1,16.2);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#392C38").s().p("AgDDLQgRgjg+ixIABgNIgHgGQgCgCAAgEQAAgOBMhyQAXgkAaAjQAWAcAGAoQAYAvACAIQACAKAAAvIgCAjQgDAcgDARIgSCmQAGgbAAAwQgBAwgFALQADAEgMAAQgSAAgpiQgAhQjvIgGgUQAAhiA5ANQAVAFAQASQASARAAAOQAAAOgRAZQgSAfgYAAQgiAAgNgTgAg1j9QAFAFAOAAQALAAACgEIAAAAQgQAHgMgHIgIgIIAEAHg");
	this.shape_4.setTransform(91.8,65);

	// Layer 2
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("ABxHlIgQgcQgBgJhGixQhDivAAgIQAAgDAghFQAehCgBgCQgegNgUgbQgRAOgJAEQgPAIgJgIQh9h+g/hiQg9hhgHhOQgCgXAXgFQAYgFgCAPQgIBEBBBnQA9BiBfBQIABgBIANgaQAQgiABgJQADgPAIgSQgdhRghhUIgghOQAAgUAHgDQAFgCAOgBQBKAlA+C/QAdALApAbQAyAiAVAUIATALIAQghQAUgnAZgmQATgXAcg5QAQghAYABIgzCYQgeBUgbAxQAAAHggAAQgFAAgygYQgOAggSAVQAKARAMAcQAOAeAIANQAeA1AFAXQAFAWgEBKQgFBygVCgQAAAJgGASQgIAXgMAPQgKgEgQgagAAxArIAAgCIgBABIABABIAAAAg");
	this.shape_5.setTransform(84.1,52.9);

	// Layer 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#764F4F").s().p("AAUG3IgEAAIAAgWIAAgBIAAgBIAAgBIAAgIIAAgWIgBgBIAAAAIAAhJIAAhJICLAAIgEDfIgOABQhrgWgJAAgAiUCdQgFgKgBgZQAAhFAahfIAAgCQAMguAQgsQAXg/Aeg6QBEiDBdhKQhBBdgxCIQgNAsgMArIAAAAIgEARIgjDpIAAAMIAFBHIgIAAIgOAAQgwAAgTggg");
	this.shape_6.setTransform(20,59.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#966363").s().p("Al4H8Ig8gMIgYgEIg2gMIAOgBIAEjeIAVAAIAAADQAagEAYABIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQADg2AKhuIACgWQAEgzADgvQACgxAAgpQAAhegKgEQgJgCgOBCQgKAwgHAyIAAAAIgDAaIgNC7IAAAEIAAAEIAAAEIAAAEIAAAKIAAAFIAAAFIAAAoIkQgEIgFhHIAAgLIAijpIAFgRIgBAAQANgrAPgsQAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQgxgEhmgSg");
	this.shape_7.setTransform(85.2,58);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#1E1E1E").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_8.setTransform(86,143.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#333333").s().p("AicDMIg8gLQiHgcgtglQBJgHATAAQBeAAB6AgIBbAXQgYASgSAKgAGYBtQiBgQgoggQBbgsA/gIQCDgNBEB2QgbACgTADIgDABQhtgIgagDgApLi6IgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAHgUAFQAAiPgliIgACNgrQgHgegJgSQADgbAAgTIAAgKIBWgBIAAAHQAAAQAQBHIAMA2QgRAFgQAIQghAQgTAUQgEgugMgug");
	this.shape_9.setTransform(85,130.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#666666").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_10.setTransform(86.9,128);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AjbMJQjFgig5gUQg7gUgBgmQg1kZABg+IAAgMIgQgGQgFACgJAAQgPAAgGgVIgCgLIAAgFIAAgdIAAgBIABgZIABgUIAJiHIg/AAIgDAAQgXgFhGgbQgKgIgFgIQgJgLAAgtQAAhHAWhhIACgIQAJgnALgkQAZhUAmhKQCTkkENgZIEdgFQEsAwCDBNQA1AfAsAuIBMBYIBxCjQBYBeAABGIACAAIADAFQACAEAAALQABALgPAQQgtAkgOAJQgxAgghAKIAPA/IALA7QAKBRABAiQAAAzgXANIgDgHQgbAThBAaIgMAaQgTAsgCAjQgBATAFAnQAAAQgGANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhBgDIk7AAQguACgZgYQgTgRAAgSQgIgXgMg2IgDgMIgBAEQgTBegTAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYguAGgAneKgQALAMBZAQQCRAZASAEIBiAAIB1AAQAQgKAagSIhbgXQh6ggheAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAFAHgAASKDQAgALAWAMQAFgkAUhtQARheAHgyIkuAEQhggEhsgTQgpgIgqgJQgzgLghgKIAEARQAlCIAACRQAUgFBYgHIA3gFQAzgDAdAAQCmAAB4AtgAGwIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADAsAOIEwAAIAJgBIADgBQATgDAbgCQg8hrhuAAQgOAAgPACgAE9F2IhAABIhWABIAAAKQAAATgDAbQAJASAHAeQAMAwAEAuQATgUAhgQQAQgIARgHQBAgXBQgDQAogBAkAEIgDgMQgCgMAAghQABhOAFgbQhNAVhsAPgApSCEIAABJIAAAAIABABIAAAVIAAAJIAAABIAAABIAAABIAAAWIAEAAQAJAABrAWIA2ALIAYAEIA9AMQBlATAyADIGzgPQCeACBpgtQBJggBAgLQgSg0gLhDQgQhwgNg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAANAuIAJAtQAGAcAIATQAWAgAWAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgTgbh1iHIhGhTQgpgsgxgeQh9hLkYgtIkaAAQh3APheBKQhdBJhGCEQgeA5gXBAQgQArgMAvIAAACQgaBhAABFQABAZAFAKQAWAjA7gFIAIAAIEQADIAAgmIAAgFIAAgFIAAgKIAAgEIAAgEIAAgDIAAgEIANi+IAEgaIAAAAQAHgyAJgwQAOhCAJADQAKADAABeQABApgDAyQgCAugFA2IgCAVQgJBugEA0IACADQAAADgCADIgBAIIgDAEQgFADgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAABJg");
	this.shape_11.setTransform(81.1,77.7);

	// Layer 6
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#975F3C").s().p("AhJAgIgOg/QAggKAyggIAXgRQAIAOAZAhQAlAwAAAtQAAAXhYAIQgoAEgWAGIgLg7g");
	this.shape_12.setTransform(155.6,78.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AIUIEQAtgQApgIQgRg0gLhCQgRhwgMg6QgFgWhEiGQg+h4AAg2QAAgWABgRQAtABAMAtIAJAtQAGAdAJASQAVAeAXAsQAYAuAPAnIANAmQASgNAbgPQArgXAhgKQgGg/gzhJQgUgbh0iHIhGhTQgpgsgxgdQh9hMkZgtIkaAAQh2AQheBJQhdBKhGCDQg0BjgeBwQgZBhAABFQAAAaAGAJQAVAlA7gFIEZADIAAgnQAAi6AhihQAOhDAJADQAKADAABfQAABNgJBvQgLB+gEA7IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACAUgCAVQAAAbgHAlQgMA7gZAOIhEAAQgsgRg3gRQACAPAAATIAAAQIgtAAIAAgPQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgHQgIgMAAgvQAAhKAYhmQAdh8A1hqQCTklEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBcABBHIABAAIADAEQADAEAAALQAAAMgPAQQAKAIAMARQA8BPAABFQAAAlgFAGQgLARg6AOIh1AAQAFAsAAAjQAAAZgGARgAsWFkQAAAFBRAdIAyASQCFAsAdAAQAgAAABhDIgDhBIkAAAQgZgEgUgFIgWAtgALADBQgxAfghAKIAPBBIALA7QAWgGAogEQBagHAAgYQAAgtgmgyQgZgggIgOIgZARg");
	this.shape_13.setTransform(84.4,51.6);

	this.addChild(this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.YardFrank02 = function() {
	this.initialize();

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#421F10").s().p("AlLBbIgFAAIgEgBIgCABIgDgDQgHgHgEgIQgEgHAAgFQAAgOAKgWIAEgKQALgTAQgPIAZAUIAIAHQgLAMgFAPQgDAMgCAUIAAARIgFAEIgHACIgIABIgEAAgAEwAGQgCgDgEgDIgBgCQgHgeARggQAIgPANgLIAhAWIgBABIgKAmIgFATQgDAJgJABIgEAEIgBABQgEADgIAAQgJAAgDgCg");
	this.shape.setTransform(93.1,142.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AmKLjQgFABgFAAIgCAAQgIAKgLAAQgaAAgFgWQAhhkAFgGQAigiAxgTQAigOAbAAQAGgBAMAAQApAAAkAcQAWASAFAKIAEADQAXAQAAAGIAAAEQACACAAADQAAAEgCACQgEADgLAAQgJAAgGgCIgLAAQgVAAgagGIgWAAQhIAAgVA1IgOAkQgIAQgSAAQgSAAgIgLgAmSLdIAFABIAEAAIAJgBIAGgDIAFgEIAAgRQACgUADgLQAFgQALgMIAKgJQARgOAxgDIADAAIAPAAIAcABIAEAAIAPACIABAAIAHACQAgAJAGgHIABgDIAGAAIgFgHIgBAHIAAgBQAAgBgLgKIAFgEIAAAAIAAAAIgBgBIghgMIgKgDQgZgHgVgDQhJgJguAnIgFAFQgRAPgKAWIgFAKQgKAWAAAOQAAAEAEAHQAEAIAHAIIADADIACgCIAEABgADtKWIgFABQgaAAgGgZIAAgaQABhBBGgpQAbgRAUAEQAKABAGAHIALgBIAOAAQBBAAAMAMQAQAHAAAUIAAACQAEACABADQAAAGgKACQgCACgEABIgRABIgFABQgXAHgVABQgEAAgbANIgEACQgMAKgLANQgCADgKAhQgOAggaAAQgTAAgJgLgAFCIIIgDAAQgjALgWATQgNAMgHAPQgRAfAHAfIABAEQADACACADQAEACAJAAQAHAAAEgCIABgBIAEgEQAJgEADgIIAFgUIALgmIAAAAQALgLAcgFQAQgHAogHQAWgDAOgBQAEgFgCgGIgBgDIgHgDQgKgGg2AAQgPAAgTAEgAjspfQgygPgbgVQgTgRgEgRQgCgVABgLQAFgUATgKQASgJAsgBIBUABQBVAAA4AXQA2AXgJAjQgFASgKAJQgHAKgeAPQgxAYgwABIgEAAQgoAAg+gRg");
	this.shape_1.setTransform(99.7,78.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#65301B").s().p("Al+A7IgYgWIAFgFQAuglBJAIQAWADAYAHIAKADIAhAMIABAAIAAAAIAAABIgFADQALAKAAACIAAAAIAAAAIgBADQgGAHgggJIgHgBIAAAAIgPgDIgFAAIgbgBIgPAAIgDABQgxACgSAOIgKAJIgIgHgADwgfQAWgUAkgLIACAAQAUgEAPAAQA1AAAKAGIAIADIAAAEQADAFgEAFQgPABgWADQgoAHgQAIQgcAEgLALIghgWg");
	this.shape_2.setTransform(101.9,137.1);

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7A4D30").s().p("AlhBSQgXgZgDgzQABg+gFgkQAgAEAdAGIADAAQAGB6BFBUQhRgMgcgegADtAqQgIgggBg3QgBg6ACgUIBCAAQgJCyBigFQgWACgVAAQg1AAgzgKg");
	this.shape_3.setTransform(77.2,137.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#975F3C").s().p("AmcCGIgVgDQhFhUgGh6IFLAAIgBACIAABOQgTgEgWgBIAOAAQAGAAAEgDQAEgEAAgFQAAgKgOgFIiUAAIg0ARQggAKAAALIABAHQADAIAJAAIAegKIAvgPIAUAAIgFABQgIACAAAQQAAACAEADIAQAOQCRgFA6AdQAjASAAAcQAAAHgUAIQgaAKgsACQhCAEg2AAQhFAAgzgGgABqh0QCBgNBJgKQgCAPAEAUIAAAdIgiAAQhHASgMAEQgMAEAAAMQAAAGADADIARAOQATgEBEgGQBAgGAPAAQBfAAAfAQQAPAJAAARQADAIgGAIQgMAPgsAGQheAKiagCIgDAAIgDAAIgFAAQhdAAAJitg");
	this.shape_4.setTransform(96.1,136.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3F6996").s().p("AAUG3IgEAAIAAgWIAAgBIAAgBIAAgBIAAgIIAAgWIgBgBIAAAAIAAhJIAAhJICLAAIgEDfIgOABQhrgWgJAAgAiUCdQgFgKgBgZQAAhFAahfIAAgCQAdhxA0hiQBEiDBdhKQhBBdgxCIQgUA/gVB4IgXCZIAAANIAFBHIgIAAIgOAAQgwAAgTggg");
	this.shape_5.setTransform(20,59.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#4C7FB5").s().p("Al4H8Ig8gMIgYgEIg2gMIAOgBIAEjeIAVAAIAAADQAagEAYABIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQADg2AKhuIACgWQAJhuAAhOQAAhegKgEQgJgCgOBCQgWBqgHBxIACACIgGBaIAAAEIAAAEIAAAEIAAATIAAAFIAAAoIkQgEIgFhHIAAgMIAXiZQAVh5AWg+QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQBwCCAQAWQA9BSAEA/QgbAcglAOQgkAPgVAFIgNgmQgPgngYgtQgXgrgVggQgWg9gLgZQgRgqgfgLQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg+ALhIAfIgGgBIgBAEQhoAriagCIm0APQgxgEhmgSg");
	this.shape_6.setTransform(85.2,58);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#27754D").s().p("AmAA1QgHgZgIgxIgIgxIAAAAIAEACIAAAAQA5ARAfAHIAfAGIALBtQhHgMgogGgAE7AsQAAgegDgOIgBgBIAAgIIBggBIABAzIgKAAIhQAEIgDgBg");
	this.shape_7.setTransform(65.8,116.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#339966").s().p("AmYBGIhFgMIgLhsIA1AKQBrATBgAFIEngFIgBAUQABAUABAMQgBAOgDAKQgLAAgnAGIg1AJgADLgXIBAgBQBZgMB1ghIABAAQAPAqAAAOQAAANidASQgwAFhPAFg");
	this.shape_8.setTransform(86.2,117.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("Aj+MAQiogPgVgwQgLgbgIhCQgHg7AAg4Ig/gLQgygJAAgYIAAgBQgHgmgGhEIgEgoQgGACgKAAQgTAAgEggIAAgFIAAgeIAAgCIAAgCIAAgLIABgMIABgSIAJiHIg/AAIgDAAIgDgBQgWgEhEgbQgKgIgFgIQgJgLAAgtIAAgEIAAgGIAAgFIABgOQAEg9ARhOIACgIQAdh/A2hqQCTkkENgZIAAgFIEdAAQEsAwCDBNQA1AfAsAuIBMBYIBxCjQAiAuAQAeQAWAmAQAyIACAAIADAFQACAEAAALQAAALgvAsIgaARQgxAgghAKIAPA/IALA7QAKBRABAiQAAAJgNAaQgMAZgBgDQgbAThBAaIgHACIAEAKQAMAhAAAdQAAAWgFAHQgIAMgcAIQgLADgXADQgBAcgFAgIgCALIBuAAQBCAXAPATQAHAKABAkQAAAkgGAJQgQAdhDACQikALjZAAIgrgEQgJgHgHgWQgLgdgCgrQgCghADgmIADgoIgLAAQgEAMgHAFQgGAJgNACQhOAGgYAHIgDAoIgEAzQBEAWAUAlQAIAOAAAaQAAAcghATQgkAWg7AAQieAAhLgHgAmhJeQACA0AXAYQAcAeBSANIAUADQBbAKCWgIQApgDAagKQAUgIAAgGQAAgdgjgRQg4geiRAGIgQgOQgEgEAAgBQAAgQAIgDIAFgBIgUAAIgvAPIgdAKQgKAAgDgHIgBgIQAAgMAhgLIAzgQICVAAQANAEAAALQAAAFgDAEQgFAEgFAAIgPAAQAXACATADIAAhQIABgCIlMAAIgDAAQgdgGgggDQAGAkgBA/gAC9IpQACA6AHAfQBIAPBMgGIACgBIADAAQCbADBdgLQAsgFAMgQQAGgHgDgIQAAgUgPgIQgfgRheAAQgQAAhAAGQhEAHgTAEIgRgOQgDgEAAgGQAAgMAMgEQAMgEBIgSIAhAAIAAgcQgDgUABgQQhIAKiBANIhDAAQgCAVABA5gAooFtQAIA0AHAZQAoAFBHANIBEAMIFsAAIA3gJQAmgGALgBQADgKACgNQgCgMAAgXIAAgTIkmAEQhggEhsgTIg1gLIgegGQgfgHg5gSIAAAAIgEgBIAAAAIAIAwgAE9F4Ig/ABIhgABIAAAIIABABQACAQAAAeQAKABBJgEIALAAQBPgFAxgGQCdgTgBgNQAAgOgPgqIAAAAQh2AghZANgApSCGIAABJIAAAAIABABIAAAVIAAAJIAAABIAAABIAAABIAAAWIAEAAQAJAABrAWIA2ALIAYAEIA9AMQBlATAyADIGzgPQCaACBogrIACgDIAFAAQBIgfA/gLQgSg0gLhDQgQhwgNg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAfAKASAqQAKAZAWA9QAWAgAWAsQAYAuAPAnIANAmQAVgFAkgOQAlgPAbgbQgEhBg9hTQgQgWhwiCIhGhTQgpgsgxgeQh9hLkYgtIkaAAQh3APheBKQhdBJhGCEQg0BhgdByIAAACQgaBhAABFQABAZAFAKQAWAjA7gFIAIAAIEQADIAAgmIAAgFIAAgTIAAgEIAAgDIAAgEIAGhaIgCgDQAIhzAVhqQAOhCAJADQAKADAABeQAABPgJBwIgCAVQgJBugEA0IACADQAAADgCADIgBAIIgDAEQgFADgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAABJg");
	this.shape_9.setTransform(81.1,77.5);

	// Layer 7
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#421F10").s().p("AjWBNIiQAAQhNgIgLgFQhKgPgYgPQgWgOAAgZIABgJQACATATAMQAYAQBKAOQALAFBNAIICQAAIB2AAQAygRAFgHIAIgHQAEgEACgFQACAEAAADQAAAKgIAIIgIAHQgFAIgyARgAC3gGQghAAgkgFQgkgGgHgHQghgZAAgGQAAgHABgGQAGAIAaAUQAHAGAkAGQAkAGAhAAID4AAQArgEAxgTQALgEAHgFIAMgGQAKgHADgJIABAJQAAANgOAKQgFAEgHADQgHAEgLAFQgxATgrADg");
	this.shape_10.setTransform(93,150.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AovA7QgagXAAgqQAAgJALgPIADgDQAEgIAIgEIALgHQAdgSAFAkIABAKIAFAHIABACIgGgCQgNgEgHADQgJgLgIADQgKADgCASIAAAHQAAAbAWAOQAYAPBKAPQALAFBNAIICPAAIB2AAQAzgRAFgIIAIgHQAIgIAAgKQAAgDgDgEIgCgDIgNgLIgCABIgEgBQgHADgEAGIgFAGIgBABIgRALIlAAAIgQgDIgSgMQAlAKAxAEIDCAAIAKgDQAigJAcgEIACAAIAEgBIACgDIAHgMQAFgNAPgCQAKgJAJACQAOACADAYIAAABQADALgCAMQAAAWgJAKIgDAEQgaAmhOAAQhuAFgzAAQjPAAhCg5gACaAcQhWgMgggUQgagRAAgfQAAgtAygPQAUgGANAHQAQAHAAAVQAAAKgRASQgIAAgEACQADgLgBgGIgBgEQgDgJgMADQgSAEgFARQgBAGAAAHQAAAGAgAZQAIAHAkAGQAjADAiAAID3AAQAsgBAxgTQALgFAHgEQAHgDAFgEQAOgKAAgNIgCgJQgDgKgNgBQgSgBgMAPIgKAAIAVgSQANgMAIAAIACAAQAVABAGATQAEAKgCALQAAAegYAQQglAXhtAMg");
	this.shape_11.setTransform(92.6,149.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#65301B").s().p("AjWBLIiQAAQhNgIgLgFQhJgOgZgQQgSgMgDgUQACgRAKgDQAIgDAIALQAIgCAMADIAHACQAJAEALAFQAXANAfAUIASAMIAPADIFAAAIASgLIABgBIAEgGQAFgGAGgDIAFABIABgBIANALIADAEQgCAEgEAEIgHAHQgGAHgyARgAC4gHQgiAAgkgGQgjgGgIgGQgagUgFgIQAEgRASgEQANgCADAIIABAEQABAHgDAKQADgCAJAAQAJAAAFAHIACADIAwABQAsADAFABIADAAQAjgBAGgCICcgDQAogBAFgIQAEgHA7gBIAKgBQALgPASABQAOABADALQgDAIgJAHIgMAGQgHAFgLAEQgxATgsAEg");
	this.shape_12.setTransform(92.9,148.8);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,162.2,161.1);


(lib.WhiteHatBack = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E2DCC2").s().p("AgcBSQAEgMABgIIAAgrQAAgggHgiQgFgTgGgPQAWAFAOAJQAaASANAqQAKAggCAxIgTAIg");
	this.shape.setTransform(29,10.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F2EACF").s().p("ABpBWQhDgFgogIIgZgGIAAgBQgagWgIgIQgLgOAAgQIADgIQACgIAAgDIAAgCQAPgWAggVQAtgeArAEQAYACASAFQAHAPAEATQAIAhAAAhIgBAqQgBAJgEAMgAhgAhQgggUAAgNQAAgYAIgKQASgVA8gCQgdAVgFAUQgEADgBAHIgBAMQANAiANARQgNgHgbgRg");
	this.shape_1.setTransform(13.6,9.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ABIBfQhbgJgkgJQgNgDgVgJQgCAAgKgFQgegLgPgNQgWgUAAgQQAAgeAKgKQATgUBGgEIAGABIAHgFQA7gkBDANIAGABQA4AXAWArQAPAcAAAvIgBAdIABACIgBADQgCACgGADQgIAEgGABgAg1g5QggAVgPAWIAAACQAAADgCAIIgDAIQAAAQALAOQAIAIAaAWIAAABIAZAGQAoAIBDAFIASAAIA1AAIAUgIQABgygKggQgMgpgdgSQgOgJgWgFQgSgFgYgCIgKAAQgkAAgqAagAiZghQgIAKAAAXQAAAOAgAUQAbARANAHQgNgRgNgiIABgMQABgHAEgDQAFgUAdgVQg8ACgSAVg");
	this.shape_2.setTransform(16.9,9.6);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,33.9,19.1);


(lib.whiteDot = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMAMQgFgFAAgHQAAgGAFgGQAGgFAGAAQAHAAAFAFQAGAGAAAGQAAAHgGAFQgFAGgHAAQgGAAgGgGg");
	this.shape.setTransform(1.9,1.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,3.7,3.7);


(lib.WhiteCoverOpaque = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EguPAiiMAAAhFDMBcfAAAMAAABFDg");
	this.shape.setTransform(296.1,221.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,592.2,442.1);


(lib.WhiteCover = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.502)").s().p("EguPAiiMAAAhFDMBcfAAAMAAABFDg");
	this.shape.setTransform(296.1,221.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,592.2,442.1);


(lib.TW_butn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Aj6CdQAMACANAAQBIAAA4gsQgigBgagUQgagUgKgfQAIACALAAQAMAAAPgDQgjgIgYgcQgXgbAAglIAAgBQAWANAYAAQgVgOgMgWQgNgXAAgbQAAgcAOgYQAnAvA3AdQA4AdA/ADQgDgNAAgKQAAgrAcgeQAfgfAqAAQAtABAeAgQAhgGAhgTQgMAkghAVQAfgEAcgMQgVAfgfAWIABANQAABuhMBWQhUBgiEAAQhWAAhIgvg");
	this.shape.setTransform(19.2,19.2,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#23AADB").s().p("AiMC/QgxAAAAgzIAAkXQAAgzAxAAIEZAAQAxAAAAAzIAAEXQAAAzgxAAg");
	this.shape_1.setTransform(19,19.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.1,0.1,38.2,38.2);


(lib.TruckDoorAndRoof = function() {
	this.initialize();

	// Layer 3
	this.debug_txt = new cjs.Text("", "14px 'VAGRounded'", "#6B3855");
	this.debug_txt.name = "debug_txt";
	this.debug_txt.textAlign = "center";
	this.debug_txt.lineHeight = 16;
	this.debug_txt.lineWidth = 40;
	this.debug_txt.setTransform(100.4,135.4);

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6B3855").s().p("AhfHWIAHjiQAEiJAAgiIADmXQAAgFAEgMQADgIgEgCQAdhACSlnIAHAhQAFAWgEAOQgpCnASEiQAEBEAMCNQAKBsAAAkIgCB+QgBBYABAiIAAAAQgzBNgPAZQgwBUglBdQgpBpgVA6QAMh+AAi9g");
	this.shape.setTransform(91.4,266.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhuNXQgXgIgBgNQABhFAGh6QAIh6AAgwIAIj5IADiaQAAi+ABgpIADh0QABg0AIgVQgEAAAMgCIADgCIgFgCQgCgCAAgNQAAgZBSjPQBGizArhPIAAAGIAAABIACAcIgCAAIACARIACAXQiSFngcBAQADACgCAIQgFAMAAAFIgDGXQAAAigECJIgHDiQAAC9gMB+QAVg6AphpQAjhdAyhUQAPgZAzhNIAAAKIAJAFIACADQABADAFACIAAAFQhGCshMCgIhPCdQABABABABQAAAAAAABQAAAAAAAAQgBABAAAAQgFAAgPgHg");
	this.shape_1.setTransform(90.3,266.6);

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6B3855").s().p("As+hWIKpmBIAQgIIABgBIABABQAFAIAOALQAuAnCLBOQEFCQATAPQBXA9C6BvQCOBTA/AdQiCBBkgCgIkYCcg");
	this.shape_2.setTransform(195.8,68.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#848484").s().p("AhZgZIACg1QBwBIBBAaQgFAJAAALIACAnQhKgwhmg4g");
	this.shape_3.setTransform(87.6,303.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#A8A8A8").s().p("AgBABIADgBIgBABg");
	this.shape_4.setTransform(255.3,224.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#111111").s().p("AgEAeIAAgIQgBgTAKggIAAA7g");
	this.shape_5.setTransform(273.3,221.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AvoAtIgWgMIAFgGQACgDABgJIAGgEQFQjHIgk3IC7hrIPEI5QibBUhAAhIg4AeIgqAYQihBZj4CWQldDVAAAUIAAABQoElOmwjkg");
	this.shape_6.setTransform(106.8,120.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AqFV0IABg3QBoA7BKAwIgCgnQAAgLAFgJQhBgchyhIIAChAQCjBoA/AnIAHAEIAAlSQgiklAAkdQAAhFACgVQADgzAMggIAAhaQABgNADgKQgTgFiehbQj4iNh9hEQhYgxiEhcQhjhGhDgrIgfgTIghgNIAAgfIAJgKQABgHAJgLQA1g+E8ipIEzifQCrhZA/gmICaheQB6hNCghlQDoiMDphTIAGALQBKAQEoCzQA0AfIcFUIABgGQAEAFACAFIAlAXIg5AcQhWA5lBCxIkICQQjJBugVANIhYA1IgrAaQnlEjj0CPIADAIIADARIAAANIAAABIgBAcIABgBIgBASQgDAngIA1QgNBZAAAtIARFIQASFEAABSIgBCPIAAAGIAAALIAAAAIAAAXIgBAAIgBALIgJAEIgBADQgDAHgMAAIgCAAQgNAAgSgFIAAAbIAAAnQgFAZgeABQgPAAiihhgAGI25IgQAJIqnGBIi8BrQohE2lQDKIgGAEQAAAJgDADIgFAGIAXAMQGvDkIEFMIAAgBQABgUFejTQD2iWChhaIAqgXIA4geQBAghCchWIEYicQEgigCBhBQg+gdiOhVQi6hvhXg9QgTgPkGiQQiMhOgvgnQgNgLgGgJIAAAAIgBAAg");
	this.shape_7.setTransform(143.2,166.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#99CC00").s().p("AmCHqIgCgCIACACgAGEnnIgBgCIACACg");
	this.shape_8.setTransform(-38.4,296.9);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.debug_txt);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-77.4,17.6,362.8,335.3);


(lib.TP_logo_white = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AivCxQhKhJAAhoQAAhmBKhJQBJhKBmAAQBnAABKBKQBJBJAABmQAABohJBJQhKBJhnAAQhmAAhJhJgAh8BcIAdAAIAAgsQANAPATAAQAUAAANgOQAOgPAAgZIAAAAQAAgXgOgPQgOgNgTAAQgTAAgNAQIAAgOIgdAAgAA+gbQgOAQAAAUIAAABQAAAXAPAPQAPAPAYAAQAYABARgUIgRgRQgMALgLAAQgMAAgHgIQgIgIABgNIAAAAQgBgJAIgJQAHgIALAAQAMAAALAMIASgUQgQgRgZAAQgYAAgQAPgAAAA9IAeAAIAAiKIgeAAgAhYAfQgHgJAAgNIAAAAQAAgKAHgJQAIgHAKgBQALABAGAHQAIAJAAAKIAAAAQAAANgIAJQgGAHgLAAQgKAAgIgHg");
	this.shape.setTransform(558.2,25.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#231F20").s().p("ACcDsQg/g9ABhhIAAgCQAAhbA5g/QA7hBBbAAQBgAAA3BFQAzA+AABfIgBAbIk4AAQAIA0AiAcQAgAcAxAAQAiAAAcgNQAagMAbgZIA4AyQhDBQhqgBQhdABg+g+gADhghQgcAfgIAwIDaAAQgEgvgcgeQgdghgvABQgsAAgeAegEggiAEHQgtgkAAg9IAAgCQgBhEAxgkQAugjBOABQA+AAA3ARIAAgIQAAgqgbgYQgbgXgzAAQg5AAhAAbIgahOQAngSAhgIQApgJAwgBQBdABAvAuQAvAvgBBTIAAD8IhgAAIAAg1QgzA+hXgBQg/AAgqghgA/WBvQgZARAAAgIAAABQAAAdAYARQAWAQAkABQAwgBAggYQAhgZAAgoIAAgYQgtgRg1AAQguABgaARgEAggAEYQgwgRgngeIArhCQBGA0BGAAQAeAAASgNQARgMAAgVIAAgCQAAgWgcgOQgOgHg0gQQg+gRgcgWQgrgfAAg0IAAgCQAAg7AsgkQAqgiBBgBQAnABAqANQAoAMAiAXIgmBFQhCgog2AAQgcAAgPAMQgQALAAATIAAACQAAAUAdANQAQAIAyAQQA9ATAcAWQArAgAAAzIAAABQAABBguAjQgqAihFAAQguAAgvgQgAuwEYQgvgRgogeIAshCQBFA0BGAAQAfAAARgNQARgMAAgVIAAgCQAAgWgcgOQgOgHg0gQQg9gRgdgWQgqgfAAg0IAAgCQgBg7AtgkQAqgiBBgBQAnABAqANQAoAMAiAXIgmBFQhCgog2AAQgcAAgQAMQgPALAAATIAAACQAAAUAdANQAPAIAzAQQA8ATAdAWQArAgAAAzIAAABQAABBguAjQgqAihGAAQgtAAgwgQgA35EjIiumpIBoAAIByE2IBzk2IBmAAIitGpgAcpEgIAAjxQAAgwgYgaQgXgbgrAAQgrAAgaAcQgbAaAAAwIAADwIhhAAIAAmmIBhAAIAABCQAyhLBTAAQBGAAApAtQAoArAABJIAAEOgAVCEgIAAmmIBhAAIAAGmgASZEgIh/i3Ig8A+IAAB5IhhAAIAApIIBhAAIAAFcICwi6IB2AAIipCqICuD8gAJGEgIAAmmIBhAAIAABgQAUgyAlgcQAngdA1ADIAABmIgFAAQhCAAgmAoQgoAsAABSIAACigAl/EgIAAowIDeAAQBiAAA6AzQA3AzABBVIAAADQgBBYhAA1Qg9AwhhAAIhwAAIAAC1gAkcASIBzAAQA4AAAhgaQAggcABgsIAAgCQgBgwgggbQgggZg5AAIhzAAgAywEgIAAmmIBhAAIAAGmgEglWAEgIAAmmIBhAAIAABgQAVgzAkgbQAogdA2ADIAABmIgHAAQhCAAglAoQgpAsAABSIAACigEgp7AEgIAAnUIizAAIAAhcIHJAAIAABcIiyAAIAAHUgEAohgAvIAAiGIAdAAIAAAOQANgQATAAQATAAAOANQAOAPAAAZIAAAAQAAAZgOAPQgNAOgUAAQgTAAgNgPIAAAsgEApFgCXQgHAJAAAMIAAAAQAAANAHAJQAIAHAKAAQALAAAGgHQAIgJAAgNIAAAAQAAgMgIgJQgGgHgLgBQgKABgIAHgEAregBbQgPgPAAgXIAAgBQAAgWAOgQQAQgPAYAAQAZAAAQARIgSAUQgLgMgMAAQgLAAgHAIQgIAJABALIAAAAQgBANAIAIQAHAIAMAAQALAAAMgLIARARQgRAUgYgBQgYAAgPgPgEAqfgBOIAAiMIAeAAIAACMgAU+jHIAAhdIBoAAIAABdgAy0jHIAAhdIBpAAIAABdg");
	this.shape_1.setTransform(286.3,39.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,583.3,69);


(lib.Time = function() {
	this.initialize();

	// Layer 2
	this.time_txt = new cjs.Text("1:35", "35px 'Laffayette Comic Pro'", "#FFFFFF");
	this.time_txt.name = "time_txt";
	this.time_txt.textAlign = "center";
	this.time_txt.lineHeight = 37;
	this.time_txt.lineWidth = 91;
	this.time_txt.setTransform(134.6,23);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("AKIEPIAAAtIgwAAIyyAAIgtAAIAAgtIAAoYIAAgyIAtAAISyAAIAwAAIAAAtIAAAFg");
	this.shape.setTransform(138.6,35.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F4D96F").s().p("ApZEPIAAodISyAAIAAIdgAorgLIAACgIAABOIRaAAIAAjuIAAjVIxaAAg");
	this.shape_1.setTransform(138.5,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#474747").s().p("AotB3IAAhNIAAigIRaAAIAADtg");
	this.shape_2.setTransform(138.6,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#666666").s().p("AotBqIAAjTIRaAAIAADTg");
	this.shape_3.setTransform(138.6,23.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("AJYE8IyyAAIgtAAIAAgtIAAoYIAAgyIAtAAISyAAIAvAAIAAAuIAAAEIAAIYIAAAtgApaEPISyAAIAAoeIyyAAg");
	this.shape_4.setTransform(138.6,35.4);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.time_txt);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(72.3,2.2,132.6,66.3);


(lib.Thalf2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAdBCIgMgBIgFgBIAAgBIABgMQABgGgCgDQAQgDAFgCQAMgFAHgIQgCgGgFgCQgFAEgdAIQg/APgHgJQgIgMgDgOIgCgMIACgPQACgNACgGQABgCAHAAQAGAAAWgFQAegGAYgJIASgFQALARAKAXQAIAYACAOIAAAEQAAATgDAIQgFANgOAGQgIADgMAAIgCAAg");
	this.shape.setTransform(12.8,9.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF91").s().p("Ag9BoQgwgKgHghIAAgQQACACAEABIAQAAQAdgDALgNQADgEABgEQADgGAAgbIAAgFQgBgVgKgZQgLgbgOgHQAtgOAeAAQBEAAAeAsQAJAOAGASIAEARIADAbIAFBEQABADgKAEIgWAIQgoAOgxAAQglAAgVgFg");
	this.shape_1.setTransform(25.8,12.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B20400").s().p("AgGAqQgGgEgFgQQgGgQABgHQABgKAEgKIAHgSQAPgKAJAMIgEAIQgBAFAAATQAAATACADQABAGALAOIgBAAQgFAHgJABIgCAAQgGAAgGgDg");
	this.shape_2.setTransform(3.3,9.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ag0BVQgJgOAAgdQAAgSACgIIhAAFIgBABQgBADgTADQgSACgHgCQgSgXgCgFQgCgEAAgWQAAgUAGgQQAFgPAPgDQAPgDAQAIIAFACIABABQAJgEAXgHIAWgGIAIgDIAAgBIAMgFIACABQAVgHASgFQAqgMAiAAQAXAAAfAIIABgBIAJAEQAIAFARAQQAUAVAKAjQAGAUADAYIABAeIgBAqIABAEIgBACIAAABIgBABQgHAHghAKQgwAOgpAAQhXAAgaglgAgzA9QAIAhAtAKQAWAFAlAAQAyAAApgOIAVgIQALgEgBgDIgFhEIgEgbIgDgRQgGgSgKgOQgdgshHAAQgeAAgqAOQAOAHAJAbQAKAZABAVIAAAFQAAAbgDAGQgCAEgCAEQgKANgcADIgQAAQgEgBgDgCIAAAQgAgLgCQgHAGgMAFQgFACgQADQACADgBAGIgBAMIAAABIAFABIAMABIACAAQAMAAAIgDQAMgGAFgNQADgIAAgRIAAgEQgCgQgGgYQgKgXgLgRIgSAFQgaAJgeAGQgWAFgGAAQgHAAgBACQgCAGgCANIgCAPIACAOQADAOAIAKQAHAJBBgPQAdgGAFgEQAFACACAGgAiphCIgIASQgEAKAAAKQgCAJAGAQQAFAOAGAEQAHADAJAAQAJgBAGgHIAAAAQgLgMgBgGQgBgDgBgVQAAgTABgFIAEgIQgFgGgHAAQgGAAgHAEg");
	this.shape_3.setTransform(19.3,12.2);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,38.5,24.5);


(lib.Thalfcopy2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAeA6IgRgCIAAgBIABgOQABgHgCgDQAJgDAMgHQALgFAEgFQgCgGgFgCQgVAPgJACQhAAOgGgIQgNgTAAgFIABgNQACgNADgGIANABQARACASgFQARgFAHgEIASgJQALgFAHgCQALARAJAXQAHARACAMIgDATQgCAQgRAJIgBAAIgRACg");
	this.shape.setTransform(12.7,8.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#774736").s().p("AgGAiQgGgEgFgQQgCgFgCgBQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAQABgIAEgKIAHgSQAPgKAJAMIgEAIQgBAFAAARIABAIIABAAQABAGALAOIgBAAQgFAHgJABIgCAAQgGAAgGgDg");
	this.shape_1.setTransform(3.3,8.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF91").s().p("Ag9BoQgwgKgHghQgBgEABgcQACACAFABIAQAAQAcgDALgNIAFgIIACgEIABgNIAAgVQgBgIgCgGIgIgQQgLgbgPgHQAtgOAeAAQBDAAAdAsQAEAGACAGIADAOIAEARIAEAbIAGAjQAGAYABAPQAAADgKAEIgVAIQgpAOgxAAQglAAgVgFg");
	this.shape_2.setTransform(25.8,12.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ag0BNQgJgQAAgjQAAgVACgHIhAAHIgBABQgBADgTADQgSACgHgCIgUgUQgCgCAAgQQAAgUAGgQQAFgPAPgDQAPgDAQAIIAFACIABABQADgBALABQALABAagIQATgFAagQQANgHAGgCQAqgMAiAAQAXAAAfAIIABgBIALAJQANAJAIAHQAJAKAGAMQAEAJAFATQAHAUACAYIAIBOIABAEIgBACIAAABIgBABQgHAHghAKQgwAOgpAAQhXAAgagtgAgzA9QAIAhAtAKQAWAFAlAAQAyAAApgOIAVgIQALgEgBgDQgBgPgFgYIgHgjIgEgbIgEgRIgDgOQgCgGgEgGQgdgshFAAQgeAAgqAOQAOAHAJAbIAIAQQACAGABAIIAAAVIAAANIgDAEIgEAIQgKANgcADIgQAAQgFgBgCgCQgBAcABAEgAgPgXQgDAFgMAFQgMAHgJADQACADgBAFIgBAOIAAABIARACIACAAIARgCIABAAQAPgJACgOIAEgTQgDgNgEgSQgKgXgLgRQgHACgLAFIgSAJQgJAEgRAFQgSAFgRgCIgNgBQgCAGgCANIgCAPQAAAFANATQAGAIBCgOQAJgCAVgPQAFACACAGgAiphCIgIASQgEAKAAAKQAAABAAAAQAAABAAAAQAAABAAAAQAAAAABAAQABABACAFQAGAQAFADQAHACAJAAQAJgBAGgFIAAAAQgLgOgBgGIgBAAIgBgIQAAgTABgFIAEgIQgFgGgHAAQgGAAgHAEg");
	this.shape_3.setTransform(19.3,12.2);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,38.5,24.5);


(lib.Thalfcopy = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAeA6IgRgCIAAgBIABgOQABgHgCgDQAJgDAMgHQALgFAEgFQgCgGgFgCQgVAPgJACQhAAOgGgIQgNgTAAgFIABgNQACgNADgGIANABQARACASgFQARgFAHgEIASgJQALgFAHgCQALARAJAXQAHARACAMIgDATQgCAQgRAJIgBAAIgRACg");
	this.shape.setTransform(12.7,8.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A88541").s().p("AgGAiQgGgEgFgQQgCgFgCgBQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAQABgIAEgKIAHgSQAPgKAJAMIgEAIQgBAFAAARIABAIIABAAQABAGALAOIgBAAQgFAHgJABIgCAAQgGAAgGgDg");
	this.shape_1.setTransform(3.3,8.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhKAUIACgEIABgPIAAgTICPgIIAEARIAEAbIifAKg");
	this.shape_2.setTransform(28.3,11.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ag0BNQgJgQAAgjQAAgVACgHIhAAHIgBABQgBADgTADQgSACgHgCIgUgUQgCgCAAgQQAAgUAGgQQAFgPAPgDQAPgDAQAIIAFACIABABQADgBALABQALABAagIQATgFAagQQANgHAGgCQAqgMAiAAQAXAAAfAIIABgBIALAJQANAJAIAHQAJAKAGAMQAEAJAFATQAHAUACAYIAIBOIABAEIgBACIAAABIgBABQgHAHghAKQgwAOgpAAQhXAAgagtgAgzA9QAIAhAtAKQAWAFAlAAQAyAAApgOIAVgIQALgEgBgDQgBgPgFgYIgHgjIgEgbIgEgRIgDgOQgCgGgEgGQgdgshFAAQgeAAgqAOQAOAHAJAbIAIAQQACAGABAIIAAAVIAAANIgDAEIgEAIQgKANgcADIgQAAQgFgBgCgCQgBAcABAEgAgPgXQgDAFgMAFQgMAHgJADQACADgBAFIgBAOIAAABIARACIACAAIARgCIABAAQAPgJACgOIAEgTQgDgNgEgSQgKgXgLgRQgHACgLAFIgSAJQgJAEgRAFQgSAFgRgCIgNgBQgCAGgCANIgCAPQAAAFANATQAGAIBCgOQAJgCAVgPQAFACACAGgAiphCIgIASQgEAKAAAKQAAABAAAAQAAABAAAAQAAABAAAAQAAAAABAAQABABACAFQAGAQAFADQAHACAJAAQAJgBAGgFIAAAAQgLgOgBgGIgBAAIgBgIQAAgTABgFIAEgIQgFgGgHAAQgGAAgHAEg");
	this.shape_3.setTransform(19.3,12.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FE9932").s().p("Ag9BoQgwgKgHghQgBgEABgcQACACAFABIAQAAQAcgDALgNICfgKIAGAjQAGAYABAPQAAADgKAEIgVAIQgpAOgxAAQglAAgVgFgAgxgsIgIgQQgLgbgPgHQAtgOAeAAQBDAAAdAsQAEAGACAGIADAOIiPAIQgBgIgCgGg");
	this.shape_4.setTransform(25.8,12.2);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,38.5,24.5);


(lib.Thalf = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAdBCIgMgBIgFgBIAAgBIABgMQABgGgCgDQAQgDAFgCQAMgFAHgIQgCgGgFgCQgFAEgdAIQg/APgHgJQgIgMgDgOIgCgMIACgPQACgNACgGQABgCAHAAQAGAAAWgFQAegGAYgJIASgFQALARAKAXQAIAYACAOIAAAEQAAATgDAIQgFANgOAGQgIADgMAAIgCAAg");
	this.shape.setTransform(12.8,9.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A88541").s().p("AgGAqQgGgEgFgQQgGgQABgHQABgKAEgKIAHgSQAPgKAJAMIgEAIQgBAFAAATQAAATACADQABAGALAOIgBAAQgFAHgJABIgCAAQgGAAgGgDg");
	this.shape_1.setTransform(3.3,9.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhOAZQADgGAAgbIAAgGICXgSIAEARIAEAcIinAUQADgEACgEg");
	this.shape_2.setTransform(28.7,12.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ag0BVQgJgOAAgdQAAgSACgIIhAAFIgBABQgBADgTADQgSACgHgCQgSgXgCgFQgCgEAAgWQAAgUAGgQQAFgPAPgDQAPgDAQAIIAFACIABABQAJgEAXgHIAWgGIAIgDIAAgBIAMgFIACABQAVgHASgFQAqgMAiAAQAXAAAfAIIABgBIAJAEQAIAFARAQQAUAVAKAjQAGAUADAYIABAeIgBAqIABAEIgBACIAAABIgBABQgHAHghAKQgwAOgpAAQhXAAgaglgAgzA9QAIAhAtAKQAWAFAlAAQAyAAApgOIAVgIQALgEgBgDIgFhEIgEgbIgDgRQgGgSgKgOQgdgshHAAQgeAAgqAOQAOAHAJAbQAKAZABAVIAAAFQAAAbgDAGQgCAEgCAEQgKANgcADIgQAAQgEgBgDgCIAAAQgAgLgCQgHAGgMAFQgFACgQADQACADgBAGIgBAMIAAABIAFABIAMABIACAAQAMAAAIgDQAMgGAFgNQADgIAAgRIAAgEQgCgQgGgYQgKgXgLgRIgSAFQgaAJgeAGQgWAFgGAAQgHAAgBACQgCAGgCANIgCAPIACAOQADAOAIAKQAHAJBBgPQAdgGAFgEQAFACACAGgAiphCIgIASQgEAKAAAKQgCAJAGAQQAFAOAGAEQAHADAJAAQAJgBAGgHIAAAAQgLgMgBgGQgBgDgBgVQAAgTABgFIAEgIQgFgGgHAAQgGAAgHAEg");
	this.shape_3.setTransform(19.3,12.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FE9932").s().p("Ag9BoQgwgKgHghIAAgQQACACAEABIAQAAQAdgDALgNICmgUIAFBEQABADgKAEIgWAIQgoAOgxAAQglAAgVgFgAg6g8QgLgbgOgHQAtgOAeAAQBEAAAeAsQAJAOAGASIiYASQgBgVgKgZg");
	this.shape_4.setTransform(25.8,12.2);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,38.5,24.5);


(lib.Symbol1 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ahig9QA3ADArgoQAtAoA2gDQADB9hmAjQhlgjADh9g");
	this.shape.setTransform(23.9,24.2);

	this.text = new cjs.Text("Leaderboards", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(43.3,15.9,0.59,0.59);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4F2A3F").s().p("AwMDBQgygBAAgyIAAkbQAAgzAyABMAgZAAAQAygBAAAzIAAEbQAAAygyABg");
	this.shape_1.setTransform(113.8,24.2);

	this.addChild(this.shape_1,this.text,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(5,4.9,217.6,38.6);


(lib.Sure = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("are you sure you want to quit the game?", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 395;
	this.text.setTransform(135,-105.1,1.18,1.18);

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EgttAPaMBLlAAAQCOAAAAiNIAApIIP1zeIv1LUIAAi8QAAiNiOAAMhLlAAAQiNAAAACNIAAUOQAACNCNAAg");
	this.shape.setTransform(187.7,-76.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EDED5D").s().p("EgttAPaQiNAAAAiNIAA0OQAAiNCNgBMBLlAAAQCOABAACNIAAC8IP1rUIv1TeIAAJIQAACNiOAAg");
	this.shape_1.setTransform(187.7,-76.1);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-121.1,-176.8,617.6,201.3);


(lib.StaySafe = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AuaPJQhpAAguhrQgvhtBAhzQM42EA0hWQBEhwB2AFQB6AFBFB7IF8KWQF6KOBoCvQBBBtg3BoQg3Boh8AAgAlwKIIAeA6QAnA5A0gFQBEgFAugZQA1gbAMguQAOgzgUgxQgWg0gvgRQhGgRgegJQgzgOgFgdQgEggAXgFQATgFAOAOQAOAOADAcIBogcQgGgWgOgYQgdgxgpgIQgrgJg4APQg4AQgOAaQgXApACA3QACA3AaAcQAXAZAyAQQAbAJAwAJQAbAFAQAWQAOAWgIAQQgIAQgPACQgPABgSgPQgPgNgGgOIgDgLgAA3GcQALANgHARQgGAOghATQgqAagDADQgTAPgIAYQgKAeAJAmQAKAmAQASQANAOAYAJQAWAHATgHQAPgGAYgSQARgNAKgOIAHgLIAHAbIBugfQgfg6gKgqIgdhvQgchRgngEQg0gGgsAKQgwAKgUAbQgRAYgEAsQgBAWABARIBfgYQgCgLABgMQACgXANgCIAGgBQAMAAAJAJgAEfJiIBxgbIhBj5IAigKIgUhPIgjAKQgDgKAKgRQAGgLARgGIA8gVQAogRALgRQAFgIAJgpQALgtgLlTIhxAbIAYDPIhQjBIh1AjIBIB5QBKB7ANATQAPAYAAAOQAAAOgPAKQgKAHgpARQgvATgXAMQgcAPgBAwQAAAYAFAUIgfALIAYBLIAcgKgAIJDBQhUAWgYA0QgTAqARBRQASBXAjAmQAuAyBQgYQBEgUAZhDQAHgWADgXIABgTIhrAYIAEAWQABAXgTAGQgUAHgOgkIgLgkICbgtQgKgwgagsQgphJg7AAQgMAAgOADgAopDPIAeA6QAnA5A0gFQBEgFAugZQA1gbAMguQAOgzgVgxQgVg0gwgRQhFgRgegJQgzgOgFgbQgEggAWgFQAUgFAOAOQAOAOADAcIBogcQgGgWgOgYQgdgxgpgIQgrgKg4AQQg4AQgOAaQgXApACA1QACA3AaAcQAWAZAzAQQAbAJAwAJQAbAFAPAWQAPAWgJAQQgHAQgPABQgPACgSgPQgPgNgGgOIgDgLgAjcBIQAYBQAJAYQAKAcAYAOQAYAPAcgFQAmgHAtgSIgYhZQgIADgKAAQgUgBgLgTQgNgZgMg4IgJgxIAmgNIgRhJIgtAHIgchgIhrAbIAYBhIgbAOIAUBJIAZgEgABPhZQAMAMgIARQgGAOghAUIgtAaQgTAQgIAXQgKAeAJAmQAKAmARASQALAOAaAJQAVAHAUgHQAOgFAYgTQAcgVAHgRIAHAcIBugfQgfg7gKgpQgahmgDgIQgchRgogEQgzgFgtAJQgwALgTAaQgSAYgDAsQgCAWACARIBegYQgBgLAAgMQACgXANgCIAGgBQAMAAAJAKgAA2I+QgIggAlgqIAMAuQAIAugRAFIgJABQgSAAgFgYgAH6EyQgBgYANgGQANgHAPAYQAIALAFANIgwALQgEgLgBgLgABOBHQgIggAlgoIAMArQAJAvgRAEIgJABQgSAAgGgXg");
	this.shape.setTransform(109.4,96.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,218.9,193.8);


(lib.Share_Twitter = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Share on Twitter", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(60.3,21.4,0.8,0.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Aj6CdQAMACANAAQBIAAA4gsQgigBgagUQgagUgKgfQAIACALAAQAMAAAPgDQgjgIgYgcQgXgbAAglIAAgBQAWANAYAAQgVgOgMgWQgNgXAAgbQAAgcAOgYQAnAvA3AdQA4AdA/ADQgDgNAAgKQAAgrAcgeQAfgfAqAAQAtABAeAgQAhgGAhgTQgMAkghAVQAfgEAcgMQgVAfgfAWIABANQAABuhMBWQhUBgiEAAQhWAAhIgvg");
	this.shape.setTransform(31.4,31.9,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#23AADB").s().p("A5SExQgyAAAAgyIAAn9QAAgyAyAAMAylAAAQAyAAAAAyIAAH9QAAAygyAAg");
	this.shape_1.setTransform(166.9,30.6);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,333.9,61.2);


(lib.Share_FB = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Share on Facebook", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(56.6,20.6,0.8,0.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhDFDIAAknIhjAAIAAhxIBjAAIAAhUQAAhJApgpQAlgnBEABQA5gBAfAFIAABnIg9AAQghAAgNAPQgLANAAAcIAABJIByAAIgPBxIhjAAIAAEng");
	this.shape.setTransform(29,32.4,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3A5A94").s().p("A3aExQgyAAAAgyIAAn9QAAgyAyAAMAu1AAAQAyAAAAAyIAAH9QAAAygyAAg");
	this.shape_1.setTransform(154.9,30.6);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,309.9,61.2);


(lib.shadowCircle = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#331D2C").s().p("AhpFiIAAAAIgdgKIgCgBQhDgbg4g4QhshrAAiZQAAiXBshsQBshsCXAAQArAAAoAJIABAAIACABIABAAIABABIABAAQBgAWBLBLQBsBsAACXQAACZhsBrQhrBsiZAAQg2AAgzgOg");
	this.shape.setTransform(36.9,36.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,73.7,73.7);


(lib.sfx = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_10 = function() {
		playSound("forklift");
	}
	this.frame_11 = function() {
		playSound("correct");
	}
	this.frame_12 = function() {
		playSound("hitdude");
	}
	this.frame_13 = function() {
		playSound("lowerforks");
	}
	this.frame_14 = function() {
		playSound("music");
	}
	this.frame_15 = function() {
		playSound("pothole");
	}
	this.frame_16 = function() {
		playSound("wrong");
	}
	this.frame_17 = function() {
		playSound("raiseforks");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(10).call(this.frame_10).wait(1).call(this.frame_11).wait(1).call(this.frame_12).wait(1).call(this.frame_13).wait(1).call(this.frame_14).wait(1).call(this.frame_15).wait(1).call(this.frame_16).wait(1).call(this.frame_17).wait(134));

	// Layer 1
	this.text = new cjs.Text("sfx", "28px 'VAGRounded'", "#6B3855");
	this.text.textAlign = "center";
	this.text.lineHeight = 30;
	this.text.lineWidth = 100;
	this.text.setTransform(-6,-18.3);

	this.timeline.addTween(cjs.Tween.get(this.text).to({_off:true},150).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56,-18.3,104,36.4);


(lib.Score = function() {
	this.initialize();

	// Layer 2
	this.score_txt = new cjs.Text("000465", "35px 'Laffayette Comic Pro'", "#FFFFFF");
	this.score_txt.name = "score_txt";
	this.score_txt.textAlign = "center";
	this.score_txt.lineHeight = 37;
	this.score_txt.setTransform(121.9,23.3);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("AOoEPIAAAtIgwAAI7zAAIgsAAIAAgtIAAoYIAAgyIAsAAIbzAAIAwAAIAAAtIAAAFg");
	this.shape.setTransform(124.5,35.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F4D96F").s().p("At5EPIAAodIbzAAIAAIdgAtMgLIAACgIAABOIabAAIAAjuIAAjVI6bAAg");
	this.shape_1.setTransform(124.3,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#474747").s().p("AtNB3IAAhNIAAigIabAAIAADtg");
	this.shape_2.setTransform(124.5,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#666666").s().p("AtNBqIAAjTIabAAIAADTg");
	this.shape_3.setTransform(124.5,23.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("AN5E8I7zAAIgtAAIAAgtIAAoYIAAgyIAtAAIbzAAIAvAAIAAAuIAAAEIAAIYIAAAtgAt6EPIbzAAIAAoeI7zAAg");
	this.shape_4.setTransform(124.5,35.4);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.score_txt);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(29.3,2.2,190.3,66.3);


(lib.S_off = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAAARIgtAvIgTgSIAvguIgvgtIATgTIAtAvIAugvIASATIguAtIAuAuIgSASg");
	this.shape.setTransform(6.5,6.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,13,13);


(lib.replaybtn = function() {
	this.initialize();

	// game_icons
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AkgKqQhXgmhfhKQiQhvgfhtIFghLQAcAnAbAVQAZASAyAXQCWBFCehAQCfg/BGiYQARglAIgjIACgIQAEgQABgKIACgVIACgPQACgngFglQgWiliFhlQiEhliiAWQhOALhDAmQhAAlgwA6QArA1CCCEIpaBaIgYqEIA+A5QBIBAAwAmQCsjnE2gqQDQgdDBBXQC8BVB2CpIABgCQBIBoAiB5QAiB6gIB8IgBALQgEAqgDAWIgbBsQhBDKihCJQimCOjZAdQg0AHgzAAQiWAAiPg/g");
	this.shape.setTransform(35.1,35.3,0.26,0.26,0,0,0,-0.1,0.1);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4,1,1).p("AjtFBQAOAFANAEQAwAOA1AAQCPAABmhmQBmhmAAiPQAAiQhmhmQhIhIhagVQgBAAgCgB");
	this.shape_1.setTransform(45.9,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(4,1,1).p("ADWlHQgmgJgqAAQiPAAhmBmQhmBmAACPQAACQBmBnQA2A1A/Aa");
	this.shape_2.setTransform(21.4,33.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F4D96F").s().p("AhEFMQhBgag1g1QhmhmAAiRQAAiOBmhmQBmhmCOAAQAqAAAmAIIADABQBeBeAjBnQAjBngcBfQgcBhhXBJQhSBFh5AmIgbgJg");
	this.shape_3.setTransform(29,34.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("AjfFKQB7gnBShFQBVhJAchhQAchegjhnQgjhohchdQBaAVBHBHQBmBmAACRQAACOhmBmQhmBmiOAAQg1AAgwgNg");
	this.shape_4.setTransform(47.2,35.3);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.Reminder = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("reminder", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 158;
	this.text.setTransform(94.2,12.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AsvkbIZfAAQCOAAAACNIAAEdQAACNiOAAI5fAAQiOAAAAiNIAAkdQAAiNCOAAg");
	this.shape.setTransform(95.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("AsvEcQiOAAAAiNIAAkdQAAiNCOAAIZfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_1.setTransform(95.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AsvEcQiOAAAAiNIAAkdQAAiNCOAAIZfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_2.setTransform(104.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,202.2,67.5);


(lib.pothole_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7F8277").s().p("AhjA8QgbgCgLggIgGghQAAgnA6gWQAOgFAMgCQAdgMAngHIBsAAIAaAIIABABIAACrQgsAJg4AAQhiABgtgkg");
	this.shape.setTransform(-5.4,25.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#686B61").s().p("Ag6hVQAKABAMAEQAoALAUAPIAMAGQAXAOAAAiQAAAdguAcQgcATgrAKg");
	this.shape_1.setTransform(15,25.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AilBTQhFgiAAgxQAAgwBFgiQBFgjBgAAQArABAmAGQAuAJAmATQBGAiAAAwQAAAxhGAiQgmATguAJQgmAHgrAAQhgAAhFgjgAh6hLQgMACgOAFQg6AWAAAnIAGAhQALAgAaACQAuAkBjgBQA3AAAsgJQArgKAegTQAugdAAgcQAAgigXgPIgMgGQgUgOgqgMQgNgDgJgBIgCgBIgZgIIhqAAQgpAHgdAMg");
	this.shape_2.setTransform(1,25.7);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-22.5,13.9,47,23.6);


(lib.Postscopy = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#59291D").s().p("AjMB2IAAgDQACgVANgTIAGgIQAYggAnADIAKAHIABAMQAAAQgIARQgEAKgKAPQgXAOgTAAQgRAAgOgLgAhZA8QAAgdgSgMIADgGIACgGQAAgBAAAAQAAgBAAgBQABAAAAgBQAAAAABgBQAFgGAyAAQAaAAALAFQAIAHAAAUQAAALgnAuQgjADgWAEQAHgTAAgNgAAMgDQgEgHgIgKIAKgHIAGgFIARgOIA3AAIACAFIABAWQAAAQgHAFQAAACgNAJIg0AHQgBgNgGgKgABWg6IgBgBQAMgKgJgVIAOgMIARgQQAmgVArAYQAHATgCALQgCALgNANQgXAWg6AFQgFgRgSgHg");
	this.shape.setTransform(59.2,38.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#592A1D").s().p("AgvCxQhCgkg3giQg8gkg/giIgqgVQAKgCABgGQAAgMAGgNIFdC1QgOAWgBAfQgSgNgvgbgAB3ByQg8gshZguQh3g8gtgaIABgBIgBgGQAJgDADgGIAAgHIE4C4QgCAFAAAFIAAAHIgJgCgADhBIQidh3hbg3QgggTgcgMQgDgOACgKIACgGIFHDZQgPAIgBAHIAAADIgEAAgAD6gRQgygtgbgXQhJg8hcgmIAHgEIAHgdIE4DNIgGAIQgLASgBAGIgCAHQgfgUghgZg");
	this.shape_1.setTransform(34.2,27.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7C3B29").s().p("AlyAcQADgIAGgHIAagdQAlAAAcABIAIAAQABAHACAFQBRAdBdA5QAdARBfBBIgFACQgkAPgRAdgAjtgmQABgVADgLQATgJAbAAQAKAACiBpQBlBEAsAZQgWADgOAHQgOAIgFAKgAiJh0QAMgYA9ABICkB4ICUBpIgGAAQgZADgVAJIgGAEgAgfipIACgIQAGgUALgLIAQAAIAjgCQAjAACYBuICRBlQgfgEggAIQgOADgPAag");
	this.shape_2.setTransform(39.6,23.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgFEIIgFAAIgXgJIgIAAQAAAAgBABQAAAAAAAAQAAAAgBAAQAAAAAAAAQgBgBgBgGQh4g2hZg1QhJgqhJg1IAAgCQAAgSgEgMIgBgPQABgHACgIQADgNALgJQAWgRAPgRIAqACQAjADAKADIAAgTQAAhTBhAlIgBgLQAAgOAFgIIABgDQAIgMAegSQAkALAiAOQBaAlBIA9QAcAWAyAtQAgAZAgAVIABgHQACgGALgUIAFgJQAQgYANgDQAggKAfAHIiRhoQiYhugjAAIggACIgTABQgLAKgFAVIgDAHIgGAeIgHAEQgHgIgBgFQgBgFAAgQIABgOQAAgJADgGQAHgNAYgTQCOAcBnBPQAdAWA2AuQAzApAtAZQABAEgCADQAHAIABADQADAFgBAQQAAAZgPAYQgTAeggAAQgcAAgNgEIAAAHQAAAJgEANQgHASgPAPIhDAAQgCANgIAMIgjAuQg0AAgYgCQgKAYgLAMIhFAHIABgBgAABC3IgEAIQgMATgDAVIAAADQAdAWAqgZQAKgPAEgKQAIgRAAgQIgBgMIgKgHIgHAAQgiAAgWAdgAlZgxIgZAeQgHAIgDAHQgGAMgBANQgBAHgJABIApAWQA/AhA8AlQA4AhBBAlQAxAbASANQACgfAOgXQARgdAkgPIAFgCQhehAgegSQhdg3hRgfQgCgFgBgGIgIgBIglgBIgcABgABUB2QAAABgBAAQAAABAAAAQAAABgBAAQAAABAAABIgCAGIgCAGQARAMAAAdQAAANgHATQAWgEAjgDQApguAAgLQAAgUgKgHQgLgHgaAAQgyAAgFAIgADKBSIgGAFIgJAHQAHAKAEAHQAGAMACANIA0gHQAMgJAAgCQAHgHAAgQIgBgWIgBgFIg4AAIgRAOgAjzhnQgDAKAAAVIAAAIQgEAGgJACIABAHIgBAAQAuAbB2A7QBbAvA6AsIAJABIAAgGQAAgGACgEQAFgKAOgIQAOgHAWgDQgsgZhlhBQiihsgKAAQgaAAgUAKgAiSiVIgDAFQgBALADANQAbAMAgAUQBbA3CeB2IAEAAIAAgDQAAgHAPgHIAGgDQAVgKAZgDIAGAAIiUhpIikh4Qg9AAgLAYgAExgCIgRAOIgNAMQAIAVgMAKIABABQATAHAEARQA6gFAXgWQANgNADgLQABgLgHgSQgXgMgVAAQgTAAgSAKg");
	this.shape_3.setTransform(40.6,26.5);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,81.2,53.1);


(lib.Posts = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#964631").s().p("AjMB2IAAgDQACgVANgTIAGgIQAYggAnADIAKAHIABAMQAAAQgIARQgEAKgKAPQgXAOgTAAQgRAAgOgLgAhZA8QAAgdgSgMIADgGIACgGQAAgBAAAAQAAgBAAgBQABAAAAgBQAAAAABgBQAFgGAyAAQAaAAALAFQAIAHAAAUQAAALgnAuQgjADgWAEQAHgTAAgNgAAMgDQgEgHgIgKIAKgHIAGgFIARgOIA3AAIACAFIABAWQAAAQgHAFQAAACgNAJIg0AHQgBgNgGgKgABWg6IgBgBQAMgKgJgVIAOgMIARgQQAmgVArAYQAHATgCALQgCALgNANQgXAWg6AFQgFgRgSgHg");
	this.shape.setTransform(59.2,38.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#592A1D").s().p("AgvCxQhCgkg3giQg8gkg/giIgqgVQAKgCABgGQAAgMAGgNIFdC1QgOAWgBAfQgSgNgvgbgAB3ByQg8gshZguQh3g8gtgaIABgBIgBgGQAJgDADgGIAAgHIE4C4QgCAFAAAFIAAAHIgJgCgADhBIQidh3hbg3QgggTgcgMQgDgOACgKIACgGIFHDZQgPAIgBAHIAAADIgEAAgAD6gRQgygtgbgXQhJg8hcgmIAHgEIAHgdIE4DNIgGAIQgLASgBAGIgCAHQgfgUghgZg");
	this.shape_1.setTransform(34.2,27.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7C3B29").s().p("AlyAcQADgIAGgHIAagdQAlAAAcABIAIAAQABAHACAFQBRAdBdA5QAdARBfBBIgFACQgkAPgRAdgAjtgmQABgVADgLQATgJAbAAQAKAACiBpQBlBEAsAZQgWADgOAHQgOAIgFAKgAiJh0QAMgYA9ABICkB4ICUBpIgGAAQgZADgVAJIgGAEgAgfipIACgIQAGgUALgLIAQAAIAjgCQAjAACYBuICRBlQgfgEggAIQgOADgPAag");
	this.shape_2.setTransform(39.6,23.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgFEIIgFAAIgXgJIgIAAQAAAAgBABQAAAAAAAAQAAAAgBAAQAAAAAAAAQgBgBgBgGQh4g2hZg1QhJgqhJg1IAAgCQAAgSgEgMIgBgPQABgHACgIQADgNALgJQAWgRAPgRIAqACQAjADAKADIAAgTQAAhTBhAlIgBgLQAAgOAFgIIABgDQAIgMAegSQAkALAiAOQBaAlBIA9QAcAWAyAtQAgAZAgAVIABgHQACgGALgUIAFgJQAQgYANgDQAggKAfAHIiRhoQiYhugjAAIggACIgTABQgLAKgFAVIgDAHIgGAeIgHAEQgHgIgBgFQgBgFAAgQIABgOQAAgJADgGQAHgNAYgTQCOAcBnBPQAdAWA2AuQAzApAtAZQABAEgCADQAHAIABADQADAFgBAQQAAAZgPAYQgTAeggAAQgcAAgNgEIAAAHQAAAJgEANQgHASgPAPIhDAAQgCANgIAMIgjAuQg0AAgYgCQgKAYgLAMIhFAHIABgBgAABC3IgEAIQgMATgDAVIAAADQAdAWAqgZQAKgPAEgKQAIgRAAgQIgBgMIgKgHIgHAAQgiAAgWAdgAlZgxIgZAeQgHAIgDAHQgGAMgBANQgBAHgJABIApAWQA/AhA8AlQA4AhBBAlQAxAbASANQACgfAOgXQARgdAkgPIAFgCQhehAgegSQhdg3hRgfQgCgFgBgGIgIgBIglgBIgcABgABUB2QAAABgBAAQAAABAAAAQAAABgBAAQAAABAAABIgCAGIgCAGQARAMAAAdQAAANgHATQAWgEAjgDQApguAAgLQAAgUgKgHQgLgHgaAAQgyAAgFAIgADKBSIgGAFIgJAHQAHAKAEAHQAGAMACANIA0gHQAMgJAAgCQAHgHAAgQIgBgWIgBgFIg4AAIgRAOgAjzhnQgDAKAAAVIAAAIQgEAGgJACIABAHIgBAAQAuAbB2A7QBbAvA6AsIAJABIAAgGQAAgGACgEQAFgKAOgIQAOgHAWgDQgsgZhlhBQiihsgKAAQgaAAgUAKgAiSiVIgDAFQgBALADANQAbAMAgAUQBbA3CeB2IAEAAIAAgDQAAgHAPgHIAGgDQAVgKAZgDIAGAAIiUhpIikh4Qg9AAgLAYgAExgCIgRAOIgNAMQAIAVgMAKIABABQATAHAEARQA6gFAXgWQANgNADgLQABgLgHgSQgXgMgVAAQgTAAgSAKg");
	this.shape_3.setTransform(40.6,26.5);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,81.2,53.1);


(lib.Planter = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7AD5A").s().p("AhaAlIgBgBIABAAQAJAAAfgMQAmgPAhgSIAmgUIAdgRIADAPIABACQgUAMgfAQQhWAtgrADIgCgKg");
	this.shape.setTransform(33.6,29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#8C8545").s().p("AgeAAQgqgYgbgSIAAgPQgBgGgCgFIAAAAIApAYICIBQIABABIARAKIABgBIAEgFIAGAEIgBAAIgBAPIgBAJQgjgJhgg8g");
	this.shape_1.setTransform(12.5,28.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E2D670").s().p("AhwAtIhagxIAAgQIAIAFQACgCAHgEQAagWA1gZQAOgHBIgcIgBgCIAEAAIAHABQAGABAEAFIAFAGQApALB3AwIADABIANALIAFAEIABAFIgFADIgFgBIgBgCIgDABIAAgBIgDgEQgNgEhagjQhBgZgQgCIAFgEIgCgFIgBABIAAgBIgDACQgyASgvAYQg5AegFAIIgCgBIgCgCIgGADIAKAGIAAACIAEABIABAAIAxAZQBIAmAkAfIADgBIAAANQgogZg/gkgACwgSIAAgBIAAgBIgBAAIgBAAIgFgBIAHADgAAIBfIgDgLIAHABQAHgIAhgOQAhgPAQgEQAggJAXgWQAVgSAUgCIABgBIAAAFIAAACIAFgDIAAAHIgJAFQgMAQgpAVIgTAMQgJAGg+AXQgcAKgPAJIAAgKg");
	this.shape_2.setTransform(22.8,12.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#AAA154").s().p("AATC/IABgDIgIgCIgUgJQglgRg3gkIgxggIgGgEQgOgIgJgDIgEgBIABgGIABgcIAAgFIgBglIAAgJQAaAQAqAaQBjA8AgAKIgBALIgBAJIgEAaIgCAUQAAAOABABIADABIADABIAIgBIgEAHIgCgBgAAGA9Qgpgdg6ghIgzgeQgYgNgNgFIAAgDIAAhDQCgBnAfANIAAA9IAAAFIgEgCgAAUh9QAOASALAJQAWAQAeADQgfAOgwARQACgdAAgwgAA8h5IgogUIAGgyIANAGIAPAFIAXAMQAiAPAdAJQAhANAIAAIACAAIglAbQgIAGgNAHQgfgMgigSg");
	this.shape_3.setTransform(20.7,24.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCC165").s().p("AgMCxIACgIQACgNAAgXIAAgbIAAgCQApgCBYgvQAfgQAUgMIADAUIAFAcQADARgDACQg1AbgqATQgkARgaAOIgOAGIgJAFQgMAEgFAGgAgSBEIABg9IBkgpQBFghAYgTIAABLQgeAHg2AbIg2AfQgcAPgVAKIgIADIABgOgAgugvQgCACgEAAQgKAAgSgQIgGgGIgCgBQgMgHgCgHQAEgBAEABQAEgDAEAAIAcgMIAPgGIAAA6IgDgCgAi1h3QBlgtAjgbIAHAGIgFBBIgaAIQgiALgJALIgCgBIAAAAQgBACgFABQgHgBg2geg");
	this.shape_4.setTransform(25.6,23.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#664C3F").s().p("AgCAAIACAAIADAAIgBABIgEgBg");
	this.shape_5.setTransform(40.2,10.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#826151").s().p("AAAAHIAAABIgDABgAgEgFIACgDIACABIABgBIAEAEIgEAAg");
	this.shape_6.setTransform(21.6,2.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#A87D68").s().p("AgTgEIgWgMIAfAKIAsATIAIAEQgdgIgggNg");
	this.shape_7.setTransform(30.5,8.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgDDjQgNgFgOgJQgFgEglgRQglgSgegTQgVgPgVgQIgZgTIgHAAIgDgGIgBgBIAAgBIAAgUQAAhNACgRIABACQgIgNgCgaIAAhBQAAgEAJgGQAMgGADgGIABAAIADgCIAQgHIAAABIBEgmQAygbAxgOIADAFIAAgBQAGABAEADIAFAAQACAEAmAPIBGAZQBRAeAPARIgBADIADABIAIgBIAEAGIgCByIgCgCIAEAHIgBABIgPAIQAHgBAJABIAABWIgCAGIgJACIACADIhYA4QgyAfhDAgQgCAEgCABIgBAAIgBAAIgIgCgAjLgQQADAFABAGIAAANIAAAJIABAnIAAAFIgBAbIgBAGIAEABQAJAEAOAIIAGAEIAxAgQA3AjAlASIAWAJIAGACIgBADIACABIAEgIIgHACIgCgCIgDgBQgBAAAAgPIACgUIAEgaIAAgIIAAgMIABgJIABgPIABAAIgDgEIgFAFIgBABIgRgKIgBgBIiJhSIgqgWgAASBkIACAJIAAACIABAbQAAAWgCAOIgCAIIgFAOQAFgFALgFIAMgEIAOgGQAZgOAlgRQAqgTA0gbQAEgCgDgRIgFgcIgDgVIgBgCIgEgOIgcAQIgmAVQgjAUgmAOQgfAMgKABIAAAAIAAABgABygeIhnAqIgBA9IgBAOIAIgDQAYgLAcgPIA2geQA2gcAegGIAAhMQgZAUhEAggAjHghIAAADQANAFAYANIAzAcQA6AjArAcIAEADIAAgGIAAg/QghgMighlgAjHhuIBaAzQA/AjAoAYIAAgMIgDABQgjgfhIgmIgygaIgBAAIgDgCIgBgCIgJgFIAFgDIADACIABAAQAGgIA5gdQAugYAygTIAEgBIAAAAIACAEIgFAFQAQABBCAaQBZAjANADIADAFIAAAAIAEAAIAAABIAFACIAFgDIgBgFIgEgFIgOgLIgCAAQh4gwgpgMIgGgGQgDgEgGgCIAFAAIgFgEIgBABIgDgBIgDADIgDABIAAABQhIAdgOAGQg0AagbAVQgHAEgCADIgIgFgAALgKIAAAKQAPgIAcgLQA+gXAJgFIATgNQAqgVALgQIAKgEIAAgKIgGADIAAgBIAAgFIgBAAQgUACgUAUQgYAWggAJQgQAFghAOQghAOgHAJIgHgBIADAKgAAAgeQAwgQAfgOQgegDgWgQQgLgJgOgSQAAAwgCAcgAgNgoIAAg6IgOAGIgcAMQgEAAgEADQgEgBgEABQACAHALAGIADACIAFAFQATARAJAAQAFAAABgCIADACgAACh6IAoAUQAiASAfALQANgGAIgGIAlgbIgCAAQgIAAghgOIgIgDIgvgWIgfgKIgPgFIgNgGgAiWhyQA2AeAHAAQAEAAACgCIAAAAIACABQAJgLAigLIAZgIIAGhBIgIgGQgiAbhlAtgAgIjbIACAAIgDgDgACxh/IABAAIACAAIAAABIgDgBg");
	this.shape_8.setTransform(22.5,23);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,45,46);


(lib.PeppaHead = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BA9870").s().p("ACQHdQhsgsh6g9IALgaQATgugBgWQAAgDgYgWQgcAPgHAkQgHAlgNAHQjEh8gBgiQAAgOAQgZQAQgYAAgHQAAgMgEgDIgDgGQgWAGgPAHQgDgYAAguQAAhTAkhpQAoh5BJhjQBThwBqhAQApBXAkBPQBdDVAVB1QhQASABBMQAAAMACACIADAEQAQACATgCIApgIIAKgCQASB8AeBkQAjBvBJCYQhhgWhsgsg");
	this.shape.setTransform(38.2,62.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC0066").s().p("AgmAMIg6geQBFgNB8AfIgcAMQgdALgbAAQgaAAgZgLg");
	this.shape_1.setTransform(89.7,104.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ADeFPQjaAAjehuQhQgng7gqQgcgVgMgMQgLALgMAAQhPAAhfhhQgighgaglQgXgiAAgKQAAgtAFgJQAHgNAYgLQAPgHAWgGIADAFQADAEAAAMQAAAHgPAYQgQAZAAAOQAAAiDFB6QANgHAHglQAHgiAcgPQAYAWAAADQAAAUgSAuIgMAZQB9A+BsAsQBrAsBgAWQBaAUBSACQBuACBchSQAXgUAugxQAngsAWgQQAFgEAEgBQAFgBADADQAEACABAEIADAFQADAEAHgFQAIgGgkA3QgjA3haBKQhaBLh/AAIAAAAgABbCpQg9gqAAgTQAAguBvACQApABDOAZQAYADAAAZQAAASgvAkQg8AthHAAQhHAAhIgwgAB5B2IA5AgQA0AVA5gVIAcgOQhYgWg8AAQgaAAgUAEgADWATQg8gJgSgUQgJgKAAgXQAAg0AmAYQAqAlALAEQA9APA9gPIAAgSIAIgFQAIgFAMAAQANAAAGACIADACIADAFQADAEAAAMQAAAOgQARIgYAVQggAFgkAAQgjAAgngFgAIPi3QgPgBgSgJQgWgLgPgRQgOgPgJgEQgQgHgdAEQgMADgLAEQgHgCgEgEIgDgFIgCgEIgCgDIADgFQAagpBpACQBhACA4AeQAVALAMARQALAPgBAIIgBAHIAAABIgBAAQgLgGgQgDIgOgBIgIgBQgJABgHADIgOAFQgGACgPALQgMAJgKACQgLADgKAAIgGgBgAhDjeQgIgEgaADIgKABIgpAIQgUACgPgCIgDgDQgDgDAAgLQAAhOBPgSQAigJAvADQBwAGCYA5QANAFgPAKQgPAJgZADIgbAAIggAAQgdAiguAFQgRACgQAAQgxAAgogUgABFj/IAEgBIgEAAIAAABg");
	this.shape_2.setTransform(67.8,90.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhMFzQAbgkAKgTIARghQAWg8ARhAQAKgoACg3QACgbAAhGQgBg8ABgdQAAgLgVhsQgThtAAgLQAAgUAIgBQADgBAEAIQACgFAIALQAFAKATBiQARBXAGApIAIA1QAHA9gBAjQgEBpgWBMQgPAygOAjQgUAwgcAwQgDAFgIAAQgNAAgagMg");
	this.shape_3.setTransform(128.5,63.4);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D5AE81").s().p("AltD/QhJiwgai2QAAiMAzCJQAVA7A7DMQAPAgAYBGQAYBIAQAiQAOAfASAaIgShtQgOhZgehDQgnhYgpiFQgniBAAghQAAgEACgFQgMgDgHgEQgOgJAAgbQAAgIAKgGIAAgGQAAgKAEgHIg2gDIgZgYQgGgHAAACQgWhfBUgVQBUgVAbAGQAbAGAyhXQAzhXBZBSQBZBTCFAQQBXAKA0ATQAxARBNAWQA2ARAEATQAcAZAdCsQAOBXAJBSQAAAXgKA9QgKA+AAAVQAAAegXA9QgeBQgxBDQiPDDjqAAQjnAAiRlegABcIcIAOAAIANgEIgbAEg");
	this.shape_4.setTransform(84.2,60.5);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,136.3,124.1);


(lib.PeppaClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BA986F").s().p("AFfIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAAGGiIgOgcQAzAMA1AJIAmB3QgjAGgdAMQgghIggg6gAl6ATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUg");
	this.shape_1.setTransform(47.9,96.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D5AE81").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAIVirQACgCgKgQQgLgPgEAAIgBABIAAgBIgDgHQgCgFgFgXQgGgZgHgSIgPglQgFgMAAgDQAIgjACgVQADgegDgbQgHhDgGgWIAWgJQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APQgiAJgFACQgFABgEACQgohzghg7g");
	this.shape_2.setTransform(82.3,95.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#964F67").s().p("AAGLfQgzgFgHgDIAIgYQAGgPAPgIIAOgGIAGgCIAOgEIAAAHQAAAgBiAZIAaAGQgdABgiAAQgRAAgxgEgAEyLEQAAgNgEgNQAGAAAFgHQAIgKAFgEQAKgHAPgFQALAtANAPQAIAKAUAJIhIAFQgUABgHACQACgLAAgRgACxhmQhMgKh/gPQhkgMhVgRQAGgIAGgTQAOgmAIg7IAOhSQAFgaAAgeIAAgKIAAgCIgBgJIAAgHQAGgCAHgBQAFgBAJANQAIAKAEAMQADAGAIAwQAPA1AnAsQAnAsA2AWQAgANA+ALQA3AKAVALQAhASAIAnQgqgCgegEgAk+oAIg5gUQgZgKgCgJQgDgbAmgsQAfgjAkgZQAtgfBKgZQgVAPgjAvQgmA0gVAuQgGAOgFAgQgDAUgHAAIgBAAg");
	this.shape_3.setTransform(55.5,89.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#AF5D78").s().p("Ah3MoQhjgYAAghIABgHQA6gNCNABQASACAPAQQAQAPgBARIgFAFQgHAHgQAFQghAMg+ADIgagGgACQMSQgNgPgKgtQAcgJAyAAQBIAAAqATQAjAQAJAaQACAIghAEQg2AGhkAIQgTgIgJgKgAATgTQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgDgTgHgFQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgLgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6QArA7AFA/QAGA6gdArQgOAWgPANQAAALACACIACAEQADADATAAQAGAAAIgEIANgGQAAANAFALIALAdQAKAgAOAYIheAUQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_4.setTransform(78.6,81.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7A7A7A").s().p("AgaDYIiSg1IgOgDQAPgYAmhQIALgWQAVACAZABQAqACAigOIAHgGQALgFAQgOQAYgTAGgTQAFgWgQgxQgLgigJgPQgTgkgKgUQAQgfAGgRQApAZB0AQIAEABIgiDXQgeDIgCBEQhJgUhKgbg");
	this.shape_5.setTransform(29.7,103.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#8E8E8E").s().p("AoKDcQABhFAejHIAjjXQA+AIC8ASQBZAICugFQC8gGBYgRQAdgFATgHIBdEUQAnB0AKASQgRAKgQAFIgQAEQhtAhifAmQgXAFiOAQQiZASgwAAQitAAi9gyg");
	this.shape_6.setTransform(94.2,107.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgSgcgcg3QgzhuAAACIAAgFQigguichVIAAABQgPgEgFgHIgDgCIABgEIgBgPQAAgWAxhoQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQAHAGADASIABAHIABAJIAAACIAAAKQAAAegFAaIgOBSQgJA6gNAnQgHASgFAJQBUARBlAMQCBAOBLALQAfADAoADQBFAEBjgBQCkgCBAgOIBegUQgOgYgKgfIgLgeQgFgLAAgMIgNAGQgIADgGAAQgTAAgDgDIgCgEQgCgCAAgLQAPgNAOgWQAdgrgGg6QgFg/grg7Qgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgIABIAEAKQAxCcAAA7IAAAKQgGAMgFAEQgFAEgGAAIj0BHIgrANQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgABxIEQgqACgpABQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMQh0Aah0AKgAkuH/QAgA7AgBIQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJQhkAAhigPQg1gJg1gMIAQAbgApxDWQgmBQgQAYIAPADICSA1QBMAbBJAUQC9AyCtAAQAvAACagRQCPgQAWgFQCfgmBtgiIARgEQAPgFARgJQgKgTgmh0IhekTQgTAGgdAFQhXARi8AGQixAFhXgIQi8gRg+gJIgEgBQh1gQgogZQgGARgQAfQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgiAOgrgCQgYgBgVgCIgLAWgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAIvhnQAKAPgCADQAhA6AoB0QAEgDAFgCQAFgDAigIQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJQAGAXAHBCQADAcgDAeQgCAVgIAjQAAACAFAMIAPAmQAHASAGAYQAFAYACAEIADAHIAAABIABgBQAEAAALAQgAnWjiQgYA3gEAsQALgNAIgEIgCgEQgBgDAAgNQAAgIAKgkIAIggIgGAOgAF1rgIgDgFIADAFgAj5sUQAJgHAMgHQgMAHgJAHg");
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.pavingstones = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AALEwQgXgJgCAEIhfhBQgqgchHgkQBFATA4AeQBCAkA4A4gAAXEyIA+gbQAegNAPgMQA8gkBIgIQACAFgEAHQgFAGgGADQgbADgiAOIg3AaIgjALQggAJgcANQAIgGgXAFgAATD3IAAgOIAAACQADACAMAAQAUAABVgmQBUglANAAIADAFQg6AfgyAYQhEAhgOAAQgZAAgFgIgAhfCsIhcgxIgQgMQAWAKBVApQBRAnASAPQAAALANAJQgzgfg8ghgAEJDSQAJgUBNgwQAvgcAugXIACgDQACAAAAAMQAAADhOAtQhTAwgOASgAATDBQA2gUBBgmQA7ghAZgIIAFAHQgrAfggAVQg8AogrAHQgbgCgDgFgAACCmQgcgFg2gkQg1glhKgsQgQgNARAEQARAEAdAPQAdAPA1AjQA1AiAvAaQAGAEgJAAIgRgCgAlRB2QhRglgIgQIBlAoQBIAdAhAYQAAAHgGACIhvgxgAEJCLQApgLAngZQAkgXAQgDIAKACQAKACACAEQgNAIgiAPQgsAVgqAQQgRAAgEgGgAAaCDQAdgHAugRIAVgHIgTABQgpAAgFgJIAEgFQA4gDAqgVQAXgMAdgUIAKACQAKACADAEQgZAfgaANQgOAHgPAFIAHgBIAEAHQgJAKgqAOIgxAOQgjgCgEgGgAEJB0QANgHA4gkQAvgfALgBIAFAFQgXAchSAvQgXAAgEgFgAjyBqQgkgVgggOQgsgShAgOQARgQBMAcQBIAaAjAiIAGAKQgagSgEADgAD1BSQgCgBAAgMQAAgEBIgmQBOgoAogJIAEAGQgDAKhIAhIhzA2IgBACIgBgBgAABBJQgBADgDABQgzgdg4gjQhSgygigcQANABAfAQQAXAMA0AeQAxAbAWAPQAcATAIANQAMgHAwgKQA7gMAPgIIAOACQAPACACAEQgLABgiAUQgfARg3AAQgYAAgJgEgAkHA5QgcgFg0gWQgtgSgogXIAAgEQAGABgEgDQgEgEBRAgQBQAhAngDIACADQACAFAAADIgBAEIgGAGgAAaAYQAlgIBJggQA/gdATACQgPAUg3AYQgxAXgiAHQgjgBgEgGgADuAbQgGgDAKgHQAbgRA5gcQA6gdAcgKIANACQAOACADAEQg6AOg4AnQgzAhgfADQgFgBgDgCgAk+gXQg1gfhDgcIAdACQAlADA7AjQBSAwASAIIAAADQgCAEgDADQg3gYgtgXgAAagNQAMgRBVgkQBVgkAZACQgVAJg+AhQgpAWhPAcgAhihAQhVgwgLgIQAAgGACgDQANAIBZArQBVArAIAWIgDAFIhig4gAliheQgjgQgQgMQgSgNgDgNQAAAAAAAAQABAAAAAAQAAgBAAAAQAAgBAAAAQAGAAAVAMIAyAdIA/AgQAmATAbASQgYgIAaAPQhAgahIgjgAEYgrQAagNBCglQA4geAKAFIAEAEQhbBAg4AOQgLgCgEgFgAAThJQAtgNBHgbQA/gZAYgMIAFAGQgdAhiNAuQgigBgEgHgAhrh1Qg1gggjgQIAAgGQAOAABIAkQBMAmAPATIgCADQgCAEgEACIhRgwgADdhPQAlgEBQgkIBVgnIAEAGQgBACifBQQgpAAgFgJgAk9h7IiChHQBLARA6AhQAYANBIAwIgEAHQgbgLhEgkgADqh5QCqhhAIgDIANACQAOACADAEQgpAUg1AdQg8AggaARQgYAAgEgGgAk9ivQhCgig5gnQA/AVBCAhQBBAhAUAVIgBADQgDAEgDACQgIgGhMgmgADTivIAEgFQA8gKBRgzQBFgsAOAFQghAkhIAqQg9AjgIAAQgxAAgFgIgAk6jnQhDggg7gvIAuAWQALADBFAjQBBAhAmALQgBAGgTAAQgTAAhAgfg");
	this.shape.setTransform(45.8,62.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("ADWEqQAAg5gLhOQgMhPAAhOQAAgrABgTQADghAGgPIgGABQgJAAimhoIgbgSIjFBiIgDAAIAHADIAAANQAAA5gIB7QgHB/gBBPIAAA5IgEADQgFAEgGACQgQhrAEiiQAEi0AggTQAEgDgRACIAAgBQgDgBAAgMQBhg8BUgtQiBhShDgyQBGALCZBfIAKAGQCJhIBjgdIADgCQADABAAAMQAAAHjQBqQCqBpARAJIAGALQgEAKgKAEQAYBQAIB3QAEBFAACNQAAAxgFAXQgKAEgRAAIAChSg");
	this.shape_1.setTransform(47,51.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#727272").s().p("ACNEzIiMhVQhdg6grgYQg3gfgUgEQAEgJADgRQAEgYAAgXQgJiLgBhHIABhqQgBgsgPgZQAPAJBAAfICqBVQAHADBCAvIB4BUQABABgBAFQgDAjABAuIAJDXIgBBAQgCAtgGALIAAAaQgQgLg7gkg");
	this.shape_2.setTransform(24.3,62.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C4C4C4").s().p("AjABpQhlg4g0gaQgogUgXgEQBgg5CHhUIByhCQAqgaALgLQATAPBLArQBOAtAwAZQAYAOA8ApIBzBPQhEAfinBgQh5BHgjAWQgCgBgDAFQgGADgBACQiEhxhCgcg");
	this.shape_3.setTransform(45.4,26.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#9B9B9B").s().p("AjDESQADgrAAgcQAAgSgDgwQgDgzgCgHIAAjBIgBgBQASgGA5gfICMhOIBxg+QAygcAUgOQgEAkAAAlIAJC0IgJCaIABAaIADALQgHAChOAxIhlA+Qg5AjhfAvIg7AeIAFg9g");
	this.shape_4.setTransform(68.6,63.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AjlFtQimhqgjgTQgDADgEAAQgLAAgCgCQgBACAAgMQAAgIAGgaQAGgbAAgLQgHh3gCg3IgIiwIABg2QAHAFAFAHQABgEACgDIgBAAQgCgDAAgDQAAgVDZiBQCnhjAmgDQAJgPBOAtICdBZQBXA3BAArQAnAaAUAPQADgGADAHIACAEIAIgDIACADQABACAAAGIAAADIAAACQAAAEgCADQAJAXgDALQgFAeAFDIQAAAbgDBDQgDBLgEAYQABAAABABQABAAAAAAQABABAAAAQAAAAAAABQgCAFAAADQAAAPi7BrQhjA5gzAZQg8AegZADIgDACQgCACgHAAIgBAAQgCAJgMAAQgMAAjYiKgAmnieIAABqQABBFAICNQAAAXgEAYQgCARgEAJQAUAEA2AfQArAYBfA6ICNBVQA6AkAOALIAAgaQAHgLACgtIAAhAIgJjZQgBguADgjQABgFgBgBIh2hSQhCgvgGgDIithVQhAgfgPgJQAPAZABAsgAFkigIhxA+IiOBOQg5AdgSAGIABACIAADCQACAHADAzQADAxAAARQAAAdgDArIgFA8IA7gdQBfgvA7gjIBlg/QBOgxAHgBIgDgLIgBgaIAJicIgJizQAAglAEgjQgUAOgyAbgAhDnCIhxBDQiHBUhhA5QAXAFApAVQA0AaBkA4QBCAbCEBwQACgCAFgDQADgEADACQAigWB5hHQCnhgBEgfIhzhRQg8gqgYgNQgwgZhNgtQhMgrgTgPQgLAKgqAag");
	this.shape_5.setTransform(45.7,50.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,91.5,100.7);


(lib.pallette = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3D2800").s().p("AjVCfIgBggIABghIABAAIAAgBQAAgCAYgMIA0gaQAKgEAbgSQAYgPAMAAIgBAFQAAADATAHIATAJQAOgDAGgGQACgDAEgBIgBgLIgBgVQAAgRACgCQAEAJABAPIADAbIAAAJIgEACIgCgEIgFAEQgLAHgDAAIgEAAIAAABQAAAAgBAAQAAABAAAAQgBAAAAAAQgBAAgBAAIgugTQgEAAg+AhIhHAmQACAJAAAPQAAAYACAJIgBADQAAAAgBAAQAAABgBAAQAAAAgBAAQgBAAAAAAgACThWQgPgGgCgIQAHgCAEAFQAFAHADABQAFADANADQALADAAAGIAAADIgBAAQAAAAAAAAQAAABgBAAQAAAAgBAAQAAABgBAAQgIgIgTgJgADHhNQAAgEADgCQAEgCADACQgKgeAAghQAAgKAEgDIAMA9IAAARIgEACIAAgBQgDAFgHACIgBAAQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAgBAAgBg");
	this.shape.setTransform(70.5,42.1);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3D2800").s().p("AAGERIgBgKIAAg6QAAgBAAAAQgBgBAAAAQAAgBAAAAQgBgBAAgBQgJgEglgVQgmgWgcgTQgwggg0ggQhigugZgPQghgQgXgPQgugbAAgHQAAgOAVAQIAYAVQAJAFBIAkIBpA1QAuAYBAAvQAKAIA1AcQAeASAJAGQANgLA3gfQhEgmgXgSQgVgQhAgnQhFgogzgbQhrg6gUgUQgUgVAxAdIAyAcQAcAQAMAIQAuAaClBkIAUAPIAcARIAYAMQAWALAAAGQAAADACAEIAMgGIBzhBIAIgEIgBAAQgVgOhLg4QhHg1gSgKQgJgFjKiLQAAgOCiBxQBVA+CbBvIAAABIBKguQgDgGhWg0QhLgugmgWQgWgNhSg3QhihBAAgIQAAAAAAgBQAAAAAAgBQAAAAABAAQAAAAAAAAIACgCQAaAMBEAvQBPA6AdARQAQAMBgA7QBbA3AGAGIAUgLIAlgUIgDABQhLglgmgdIihhjQiOhYAAgKQAAgIAxAfQAWAQAaASIBoBAQBbA4ATANQAhAXAuAZQAfARAAADQAAABAAAAQAAABAAAAQAAABgBAAQAAAAAAABIAPgKQAfgSAagGIABACIAAADImkDzIAAA5QACADAAAGIAAAJQgCACgEAAQgFAAAAgFgAEqAVIAEgCIgDAAgAAFAXIithzQgogagTgNQgJgCgOgLQgQgMAAgCIAAgEQANABAOAIQAOAHAGAIIAYAOQAcAQAXAQQAZANAaATIAuAhIBbA+QAlAaBKAtQgGADgDAAQgLAAiChWg");
	this.shape_1.setTransform(45.4,31.4);

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#726836").s().p("ABMAeIAAgBIAyAcIgBAAIgzAegAh8hUIABgBIAAgBIAwAdIgxAcg");
	this.shape_2.setTransform(17.4,37.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FCE579").s().p("AmygDIGijxQAKgMBMAtQAxAcBbA4QAXARBtA2QBfAvgCAEQgEAFgLAIIAAgCImjDyg");
	this.shape_3.setTransform(45.9,26.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCBA62").s().p("AjQBTIGhjwIAAADIAAAEImXDqIAABEIgKAFg");
	this.shape_4.setTransform(67.1,42.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CCC165").s().p("AjZCcIAAhEIGYjqIAbgQIAABLIgYANIAAg3IgBgBIAAAAIgwAcIiCBKIACA5IgVANIgCg2IAAgBIgyAaIiJBQIAAA4IgYAOg");
	this.shape_5.setTransform(68.9,42.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#9A8C49").s().p("AgPCVIAAg5IiKhQIAAAAIgxgaIAAABIgCA3IgYgOIACg3IiBhLIgxgdIAAABIgBABIAAA3IgXgNIAAhLIGyD7IAABKgACxAOIAygaIAAABIACA3gAF8hlIAwgcIAAAAIABABIAAA4g");
	this.shape_6.setTransform(45.4,42.1);

	// Layer 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3D2800").s().p("AAEEtIgEABQgGAAgGgEQgKgBgIgHIgHgIIAAg5IgBAAQgFAAgpgWQgpgXgQgLIgJgGIgHgDIAAgCQgDABgWAJIgXAMQgMgBgMgGQgBgCgLgGQgHgGgDgJQgCgHACgRIAAgXQgLgEgzgdIgwgdQgcATgHADQgLAEgKgCQgEgEgMgIIgZgSQACgUgCgKIAAgBIAAgBIAAgCQAAghAEgIQgBgDADgGIACgHQAAgNAcgOIA9gdICYhWQCdhaAcgLIAWgCQAMABAaAPIAJAGQAUAIAkAVQBDAmAJAJIAKAFQAQAMAcARQAcAQAUAJQA1AZAvAZQA5AeAOAMQAHAEACALIAABCQgBAGgDAEIgEAFQgEAEgGADQgIAFgEAAIgCAAIgCAAIgCAAQgKAAgYgRIgOgKQgQALgmAWQguAbgRAIIABAWIAAAfQgCAGgDAEQAAAEgDABIgCAAQgEADgGACIgEADQgGADgEAAIgBAAIgCAAIgDAAIgDgBQAAAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAgBIgKgIQgJgDgKgIIgIgGIghAXQg2AigqATIABAHIAAANIgBABIABAFQgBALgDAKQgFARgNACIgUAMIgEgBgAgSC7IAEABIAGARIgCAMIAAAUQgBAUAKAMIAagTIACAQQAEgIAAgLIgCgdQAAgGAFgBIAAgHQgBgGBnhCIAGgEQgmASgIAHQgEADhYA3IgEgBQgBAAAAgFQAAgJBXgwIBwg9IBwhEQAugaAhgRQhAgghLgqQiDhJhVg+IgPgLIgHgFIgHgGIgVgTQhLBCiWBXQh4BGgaAMIgEANQgEAQgCAQIACAAIAHgHQAWgaAFgEIAKADQAEADAAAMQAAAHgCAGQAHAEACAHIgBADIAkATIA7AiQACgEADgEQAFgWAPgDQAMASgFAiIAAABIAHAEQgBAHgBADIgFAEIAAANIABALIAIAGIAYgDIgEgGQAGgDAIABIgHgEIAAgQQAYgJA3ApQAwAkAYALQABAAAAAAQABgBAAAAQABAAAAgBQAAAAAAAAIAGAGgADvBnIgrAZIAAABIABAEIgCACIABABQAZAMAPAEQAEgGABgJIABgVQAAgJABgHIgEADgAGCAPIAAABQAGACALAFIAYANIAGgCQAMgEgBgIQgGgdAAgOIAAACIABARQAAAJgFATIAAABIgQgGQgIgEgGACIAFgVIAEgLIgJgEQgMAJgXANIAHACQACABABAHIACAAIAEgBIABABgAGzggIAAAHIADADIAAgMIgDACg");
	this.shape_7.setTransform(45.9,30.3);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,92.1,60.6);


(lib.palletdescription_mc = function() {
	this.initialize();

	// Layer 1
	this.info_txt = new cjs.Text("concrete lintels", "24px 'Laffayette Comic Pro'");
	this.info_txt.name = "info_txt";
	this.info_txt.textAlign = "center";
	this.info_txt.lineHeight = 30;
	this.info_txt.lineWidth = 262;
	this.info_txt.setTransform(4.7,-98.9,1.18,1.18);

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AlPl/IrJAAIiAAAQiOAAAACNIAACxQAACMCOAAICAAAILJAAIAUAAICWAAIB7E1IBpk1IA4AAIAFAAIFGAAIHiAAIAeAAIDXAAQCOAAAAiMIAAixQAAiNiOAAIjZAAIgcAAIniAAIlGAAIgFAAImyAA");
	this.shape.setTransform(3.9,-72.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("AilBKIiWAAIgUAAIrJAAIiAAAQiOABAAiMIAAixQAAiNCOAAICAAAILJAAIAUAAIGyAAIAFAAIFGAAIHiAAIAcAAIDZAAQCOAAgBCNIAACxQABCMiOgBIjXAAIgeAAIniAAIlGAAIgFAAIg4AAIhpE2g");
	this.shape_1.setTransform(3.9,-72.3);

	// Layer 4
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AilBYIj7AAIpDAAIi1AAQiOAAAAiMIAAiwQAAiNCOAAIC1AAIJDAAIIvAAICRAAIJRAAIEoAAQCOAAgBCNIAACwQABCMiOAAIkoAAIpRAAIiRAAIhQAAIhpEag");
	this.shape_2.setTransform(12,-64.7);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.info_txt);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-149.8,-112.8,313.9,85.2);


(lib.pallet_hit_area = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(191,191,191,0.008)").s().p("AnuAAIHukZIHvEZInvEag");
	this.shape.setTransform(49.5,28.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,99,56.6);


(lib.pallet_highlight_full = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,0,0,0.192)").s().p("AgNIRIm/j1QgIgEgEgIQgEgHAAgIIAChZIAAAAIACmgQAAgIAEgHQAEgHAHgEIHIj9QAFgEAIAAQAIAAAHAEIGzD+QAHAEAEAIQAEAGAAAIIgBH2QAAAIgEAIQgFAHgHAEIm+D3QgHADgHAAQgGAAgHgDgAnSEBQAAAFADAFQADAFAFADIG/D1QAEACAEAAQAEAAAFgCIG/j3QAEgDADgEQADgFAAgGIABn2QAAgFgDgEQgDgFgEgDImzj+QgFgDgFAAQgFAAgFACInGD+QgFACgCAFQgDAFAAAFIgCGgIAAAAg");
	this.shape.setTransform(48.7,54.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,0,0,0.388)").s().p("AgDH/Im/j1QgDgBgBgDIgCgFIAChZIAAAAIACmgQAAgDABgCQACgCACgBIHGj+QABAAAAAAQABgBAAAAQABAAABAAQAAAAABAAIAFABIGzD/QACABACACIABAFIgBH2IgCAFIgDAEIm/D3IgEABIgDgBg");
	this.shape_1.setTransform(48.7,54.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,0,0,0.29)").s().p("AgIIIIm/j1QgFgDgDgFQgDgFAAgFIAChZIAAAAIACmgQAAgFADgFQACgFAFgCIHGj+QAFgCAFAAQAFAAAFADIGzD+QAEADADAFQADAEAAAFIgBH2QAAAGgDAFQgDAEgEADIm/D3QgFACgEAAQgEAAgEgCgAnIEBIACAFQABADADABIG/D1IADABIAEgBIG/j3IADgEIACgFIABn2IgBgFQgCgCgCgBImzj/IgFgBQgBAAAAAAQgBAAgBAAQAAAAgBABQAAAAgBAAInGD+QgCABgCACQgBACAAADIgCGgIAAAAg");
	this.shape_2.setTransform(48.7,54.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,0,0,0.094)").s().p("AgSIZIm/j1QgKgFgFgKQgGgJAAgLIAEn5QAAgLAGgJQAFgJAJgGIHIj9QAIgFAKAAQALAAAJAFIGzD/QAJAFAGAKQAFAJAAALIgBH2QAAAKgGAKQgFAJgKAFIm+D3QgJAFgKAAQgJAAgJgFgAncEBQAAAIAEAHQAEAIAIAEIG/D1QAHADAGAAQAHAAAHgDIG+j3QAHgEAFgHQAEgIAAgIIABn2QAAgIgEgGQgEgIgHgEImzj+QgHgEgIAAQgIAAgFAEInID9QgHAEgEAHQgEAHAAAIIgCGgIAAAAg");
	this.shape_3.setTransform(48.7,54.3);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,97.5,108.6);


(lib.pallet_highlight = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,0,0,0.094)").s().p("AgSGbIm/j1QgKgGgFgJQgGgKAAgLIAEj8QAAgKAGgKQAFgJAJgFIHIj+QAIgFAKAAQALAAAJAGIGzD+QAJAGAGAJQAFAJAAALIgBD5QAAALgGAJQgFAJgKAGIm+D3QgJAFgKAAQgJAAgJgFgAgBmRInID+QgHAEgEAHQgEAGAAAIIgED8QAAAJAEAHQAEAHAIAEIG/D1QAHAEAGAAQAHAAAHgEIG+j3QAHgEAFgHQAEgHAAgIIABj5QAAgIgEgHQgEgHgHgEImzj/QgHgEgIAAQgIAAgFAEg");
	this.shape.setTransform(48.7,41.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,0,0,0.192)").s().p("AgNGSIm/j1QgIgEgEgHQgEgHAAgJIAEj8QAAgIAEgGQAEgHAHgEIHIj+QAFgEAIAAQAIAAAHAEIGzD/QAHAEAEAHQAEAHAAAIIgBD5QAAAIgEAHQgFAHgHAEIm+D3QgHAEgHAAQgGAAgHgEgAACmIInGD9QgFADgCAFQgDAEAAAGIgED8QAAAFADAFQADAFAFACIG/D1QAEADAEAAQAEAAAFgDIG/j3QAEgCADgFQADgFAAgFIABj5QAAgFgDgFQgDgFgEgCImzj/QgFgDgFAAQgFAAgFADg");
	this.shape_1.setTransform(48.7,41.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,0,0,0.388)").s().p("AgDGBIm/j1QgDgCgBgCIgCgFIAEj8QAAgDABgCQACgDACgBIHGj9QABgBAAAAQABAAAAAAQABgBABAAQAAAAABAAIAFACIGzD+QACABACADIABAFIgBD5IgCAFIgDAEIm/D3IgEABIgDgBg");
	this.shape_2.setTransform(48.7,41.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,0,0,0.29)").s().p("AgIGJIm/j1QgFgCgDgFQgDgFAAgFIAEj8QAAgGADgEQACgFAFgDIHGj9QAFgDAFAAQAFAAAFADIGzD/QAEACADAFQADAFAAAFIgBD5QAAAFgDAFQgDAFgEACIm/D3QgFADgEAAQgEAAgEgDgAAHl/InGD9QgCABgCADQgBACAAADIgED8IACAFQABACADACIG/D1IADABIAEgBIG/j3IADgEIACgFIABj5IgBgFQgCgDgCgBImzj+IgFgCQgBAAAAAAQgBAAgBABQAAAAgBAAQAAAAgBABg");
	this.shape_3.setTransform(48.7,41.6);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,97.5,83.3);


(lib.Options_btn = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAeCrIAAiVICVAAIAACVgAiyCrIAAiVICVAAIAACVgAAegVIAAiVICVAAIAACVgAiygVIAAiVICVAAIAACVg");
	this.shape.setTransform(35.8,35.8);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4,1,1).p("AjtFBQAOAFANAEQAwAOA1AAQCPAABmhmQBmhmAAiPQAAiQhmhmQhIhIhagVQgBAAgCgB");
	this.shape_1.setTransform(45.9,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(4,1,1).p("ADWlHQgmgJgqAAQiPAAhmBmQhmBmAACPQAACQBmBnQA2A1A/Aa");
	this.shape_2.setTransform(21.4,33.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F4D96F").s().p("AhEFMQhBgag1g1QhmhmAAiRQAAiOBmhmQBmhmCOAAQAqAAAmAIIADABQBeBeAjBnQAjBngcBfQgcBhhXBJQhSBFh5AmIgbgJg");
	this.shape_3.setTransform(29,34.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("AjfFKQB7gnBShFQBVhJAchhQAchegjhnQgjhohchdQBaAVBHBHQBmBmAACRQAACOhmBmQhmBmiOAAQg1AAgwgNg");
	this.shape_4.setTransform(47.2,35.3);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.OK_btn = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("ok", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 104;
	this.text.setTransform(61.9,15.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AnvkbIPfAAQCOAAAACNIAAEdQAACNiOAAIvfAAQiOAAAAiNIAAkdQAAiNCOAAg");
	this.shape.setTransform(63.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("AnvEcQiOAAAAiNIAAkdQAAiNCOAAIPfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_1.setTransform(63.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AnvEcQiOAAAAiNIAAkdQAAiNCOAAIPfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_2.setTransform(72.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,138.2,67.5);


(lib.No_Btn = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("no", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 112;
	this.text.setTransform(79.2,13.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AqjkbIVHAAQCOAAAACNIAAEdQAACNiOAAI1HAAQiOAAAAiNIAAkdQAAiNCOAAg");
	this.shape.setTransform(81.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("AqjEcQiOAAAAiNIAAkdQAAiNCOAAIVHAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_1.setTransform(81.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AqjEcQiOAAAAiNIAAkdQAAiNCOAAIVHAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_2.setTransform(90.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.NewMessageStatic = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("New message!", "25px 'Arial'");
	this.text.textAlign = "center";
	this.text.lineHeight = 31;
	this.text.lineWidth = 185;
	this.text.setTransform(102.5,13.2);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("AOdioQBQAAAABQIAABMIAAAEAPtgFIAAALIAABTQAABQhQAAI85AAQhQAAAAhQIAAhTIAAgLIAAgDIAAgEIAAhMQAAhQBQAAIc5AA");
	this.shape.setTransform(104.5,26.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D3BB61").s().p("AucCpQhQAAAAhQIAAhTIAAgLIAAgDIAAgEIAAhMQAAhQBQAAIc5AAQBQAAAABQIAABMIAAAEIAAADIAAALIAABTQAABQhQAAg");
	this.shape_1.setTransform(104.5,26.1);

	this.addChild(this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,8.2,203,38.4);


(lib.Male_Head_01_Front = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AjSAqQgLgLAAgMQAAgGABgEIg/ABQgSAEgaAIQgTAGgKAAQgLAAgJgBIgGgCIgDgRIAEgKQACgHAGgFQASgLBKgIQA9gQB+ANQAWATABAEQAAADgJAIQgIAFgGAEIgnABQACAFAAAHQAAAVgNAKQgKAHgPAAQgZAAgQgQgAD/AZQgVgEgJgJQgCgCgBgDIgCgEIACgFIgFgBQgKgCgKgEIgIgDIAAgDIgBgBQgBgCACgLQAFggBbAEQAoACA5AJIACAAIABAFQACAEgCALQgCAJg3ALIgVAEIgCAAIAAACQgFAageAAIgPgBg");
	this.shape.setTransform(96.2,88.1);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EACFB7").s().p("AjCAwIgLgCQAAgBgEgDQgDgCAAgSIh5AKQgPABgKgCIgIgCQAAAAAAgBQAAAAAAAAQgBAAAAABQgBAAAAABQgDACgBgOQgBgOAFgEQAHgHAdgFQAtgIDZgQIAqgEQAAABBIgBICTgFQAzgDAkACQAyACAWAIQAXAIAAAQQAAAOgMADIgWAAIgOAAQgBAXgCAHIgdAGQgsgCgMgQQgDgDgBgUIghgCQkEAHgKgEIgBAAIgxAJQgBAOgLALQgNALgUACIgPAAIgOAAg");
	this.shape_1.setTransform(94.6,87.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DEBBA4").s().p("ADVJFQhIgbhMhLQgdgcgTgsIgZg6IAKgHQATgMAAgXQAAgMgBgGQgHgOgSAAQgLAAgiAMQghAKgMABQgngBgmgTQg1gdgjhCQgDgGAAgKIABgTQAAgkAagUQAKgHAHgIQAfAIAcAPQATALAaAAQAKAAAHgCQAIgFABgNQAkAXArAFQArAEgKgbQgnh5gSicQgTikAVhoQARhdAkgyIAFgGQBwCWAVCyQAOBsAHD6QATCbAZCEQAZCEAPBbQgbgHgYgKg");
	this.shape_2.setTransform(30.8,89.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#AA8462").s().p("ABhGpQgrgFgkgXIAAAAQAAgRgZgTQgXgQgkgKQgRgFgNgBQgnjbAAgKQAAhHAPhUQAWh/AwhiQAqhXA4g5QArA4AgA2IANAVIgBACIgFAFQgmAygRBdQgVBoATCiQASCcApB7QAJAXgfAAIgNAAg");
	this.shape_3.setTransform(19.9,58.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CEA077").s().p("AkkBDQgsgLgkArIgMgVQghg2grg2QCFiGDcAVID+AAQDIAtByBiQgjCNjYAGIgTAAQisAAk3hQg");
	this.shape_4.setTransform(70.8,19.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgKMJQg8gJgsgNQhsgghKg0Qh0hQgjiCIAAgBIgiABQhlAAg5hEQgogvgCgvIgJgFQgDgVAAgWQAAgsAFgTQAKgqAggKQgLgegKhqIgIhmQAAjkBXi1QAuhfBAg+QCRiNDxAXIAAgEID1AAQECAuCHCOQA8A/AjBjQAVA8AKA1QgDAAALAsQANAwABBvQAAB8gVA+QgDALACAmQACArgBAGQgIA6gMAvQgTBGgkBUQgDAHgEAbIgGAkQgIB1hLBUQhMBXhvAKIjPAEQhRgJglgFgAltGIQACAFAAAMQAAAYgTALIgNAHIAbA6QAUAsAdAdQBLBKBIAcQAYAJAbAIQCDAkDfgBQBmAABLgvQBlhAASiFIAAgCQgfAFgpAKQgsALggAGQhNAMhWgDQg4gChagOIhAgXQAAgdA0AHQCUAYCdgYQAwgHAjgJQAjgJAugSQAHgDALgYIAohaQAWgyAIgwQAHgqAAg5QAAgOACgJQgCgHAAgGQAKhOAFhPQAKidgWhKQgOgygZg9Qg0h4g9g2QhyhkjJgtIj+AAQjbgViFCGQg6A5gqBXQgwBigVB/QgPBWAABHQAAAKAmDZQANABARAFQAkAKAXAQQAbATAAARIAAAAQAAANgJAFQgGACgLAAQgZAAgUgLQgcgPgfgHQgGAHgKAIQgbATAAAlIgBATQAAAKADAFQAjBCA2AdQAlAUAnAAQAMAAAhgLQAigMALAAQAUAAAHAPgACyFPQgMgLgBgQIACgUQAAgQAKgVQALgZAQACQAQACAAAQIAAAqIA2AZICKAAQASgGAFgPQADgKgBgcQAAgrAZAPQAZAOAAAnQAAAmgQASQgfAhhgAAQiEAAgighg");
	this.shape_5.setTransform(69.4,79.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EAD0B7").s().p("AkGJ3QgPhcgZiDQgZiEgTicQgHj5gNhsQgWiyhwiWIABgBQAkgrAsALQFMBUCqgFQDYgGAjiNQA9A2A0B5QAZA8AOAzQAWBKgKCcQgFBQgKBNQAAAHACAGQgCAJAAAOQAAA6gHAqQgIAvgWAzIgoBaQgLAYgHADQguARgjAJQgjAKgwAHQidAXiSgYQg0gHAAAeIBAAXQBYAOA4ACQBWADBNgNQAggFAsgLQApgLAfgEIAAACQgSCFhlA/QhLAvhmABIgMAAQjVAAiBgkgAAkC1QgKAVAAARIgCAUQABAPAMALQAiAhCEAAQBgAAAfghQAQgRAAgnQAAgmgZgPQgZgOAAAqQABAdgDAJQgFAPgSAHIiKAAIg2gZIAAgqQAAgRgQgCIgCAAQgOAAgLAXgAhiA4QADADAAABIALACQAPABAOgBQAUgCANgLQAMgLABgOIAvgJIAAAAQAKAEEGgJIAhADQABAVADADQAMAQAtACIAcgGQADgHABgXIAOAAIAVAAQAMgDAAgQQAAgOgWgIQgWgIgzgCQgkgCgyADIiTAFQhKABAAgBIgrAEQjXAQgsAGQgdAFgIAIQgEAFABAOQABAOACgCQABgBAAAAQABAAAAAAQAAAAABAAQAAAAAAAAIAHACQALACAOgBIB6gKQAAASADACg");
	this.shape_6.setTransform(83.5,86.1);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,138.9,158.3);


(lib.Male_Greg_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BC9A72").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#44446F").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_1.setTransform(18,46.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6666A8").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_2.setTransform(85.2,58);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D5AE81").s().p("ArABdIgygRIABgHQAHgpABgWIDpAAIADBBQgBBDggAAQgdAAiFgtgAJQgNIgPhBQAhgKAxggIAZgRQAIAOAZAhQAmAyAAArQAAAXhaAIQgoAEgWAGIgLg5g");
	this.shape_3.setTransform(88.9,83);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_4.setTransform(86,143.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_5.setTransform(67.5,124.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_6.setTransform(86.9,128);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_7.setTransform(94.8,140.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.Male_Frank_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A4D30").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#975F3C").s().p("ArABdIgygRIABgHQAHgpABgWIDpAAIADBBQgBBDggAAQgdAAiFgtgAJQgNIgPhBQAhgKAxggIAZgRQAIAOAZAhQAmAyAAArQAAAXhaAIQgoAEgWAGIgLg5g");
	this.shape_1.setTransform(88.9,83);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#70656E").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_2.setTransform(18,46.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#91838F").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_3.setTransform(85.2,58);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_4.setTransform(86,143.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_5.setTransform(67.5,124.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_6.setTransform(86.9,128);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_7.setTransform(94.8,140.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.Male_Body_Frank_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4181C0").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape.setTransform(18,46.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#478DD3").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_1.setTransform(85.2,58);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_2.setTransform(86,143.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_3.setTransform(67.5,124.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_4.setTransform(9.8,86.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_5.setTransform(86.9,128);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_6.setTransform(94.8,140.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EAD0B7").s().p("ArABdIgygRIABgHQAHgpABgWIDpAAIADBBQgBBDggAAQgdAAiFgtgAJQgNIgPhBQAhgKAxggIAZgRQAIAOAZAhQAmAyAAArQAAAXhaAIQgoAEgWAGIgLg5g");
	this.shape_7.setTransform(88.9,83);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.Male_Body_02_Front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#497C59").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape.setTransform(18,46.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5A996E").s().p("Am0HwIgYgEQifghgLAAIgEAAIAAgWQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghCjAAC4IAAAoIkQgEIgFhHQAAiiBCi6QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmQgPgngYgtQgXgrgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B6QBECEAFAWQAMA6ARBwQALBDARA0Qg/ALhJAfQhpAuiegCIm0APQg/gFiUgdg");
	this.shape_1.setTransform(85.2,58);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_2.setTransform(86,143.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#39637F").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_3.setTransform(67.5,124.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_4.setTransform(9.8,86.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#477A9D").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_5.setTransform(86.9,128);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_6.setTransform(94.8,140.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EAD0B7").s().p("ArABdIgygRIABgHQAHgpABgWIDpAAIADBBQgBBDggAAQgdAAiFgtgAJQgNIgPhBQAhgKAxggIAZgRQAIAOAZAhQAmAyAAArQAAAXhaAIQgoAEgWAGIgLg5g");
	this.shape_7.setTransform(88.9,83);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Aj8MJQjFgig5gUQg7gUgBgmQg1kZAAg+IABgMIgQgGQgFACgKAAQgSAAgEggIAAgiQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgIQgIgLAAgtQAAhKAYhmQAdh/A1hqQCTkkEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA8BQAABDQAAAkgFAHQgLAQg6APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThBAaIgNAaQgSAsgCAjQgBATAEAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk6AAQguACgZgYQgTgRAAgSQgJgXgLg2IgDgMIgBAEQgUBegSAmIACAEIAAAAIABABIAAAEIAAAIQAAAYgdAWQgeAYgsAGgAoAKgQALAMBaAQQCRAZASAEIBiAAIB0AAQATgKAYgSIhbgXQh7gghdAAQgTAAhJAHIgDAAQhTAHgPAFIgZAAIAEAHgAgNKDQAdALAXAMQAFgkAUhtQARheAGgyIkuAEQhfgEhsgTQgpgIgrgJQgygLgigKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQCmAAB6AtgAGPIWQg/AIhbAsQgeAPgiATQAHAGAAADQAEADArAOIExAAIAJgBIADgBQASgDAcgCQg8hrhuAAQgOAAgPACgAEcF2IhAABIhWABIAAAKQAAATgDAbQAIASAIAeQAMAwADAuQAUgUAhgQQAQgIARgHQBAgXBQgDQAogBAjAEIgCgMQgCgMAAghQAAhOAFgbQhMAVhsAPgApyDvIAAAWIAEAAQALAACfAhIAYAEQCUAeA/AEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDQgRhwgMg4QgFgVhEiHQg+h6AAg1QAAgWABgRQAtAAAMAuIAJAtQAGAcAJATQAVAgAXAsQAYAuAPAnIANAmQASgOAbgOQArgXAhgKQgGg/gzhLQgUgbh0iHIhGhTQgpgsgxgeQh9hLkZgtIkaAAQh2APheBKQhdBJhGCEQg0BigeBzQgZBhAABFQAAAZAGAKQAVAjA7gFIAJAAIEQADIAAgmQAAi6AhijQAOhCAJADQAKADAABeQAABPgJBwQgLB+gEA5IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACATgCAWQAAAbgHAkQgMA8gZAOIhEAAQgsgRg3gRQACAPAAATgAsWBeQAAAGBRAdIAyARQCFAtAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgGIgWAtgALAhDQgxAgghAKIAPA/IALA7QAWgGAogEQBagIAAgXQAAgtgmgwQgZghgIgOIgZARg");
	this.shape_8.setTransform(84.4,77.7);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,155.5);


(lib.LucyHead = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC0066").s().p("AgmAMIg6geQBFgNB8AfIgcAMQgdALgbAAQgaAAgZgLg");
	this.shape.setTransform(89.7,104.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DDBAA4").s().p("ACQHdQhsgsh6g9IALgaQATgugBgWQAAgDgYgWQgcAPgHAkQgHAlgNAHQjEh8gBgiQAAgOAQgZQAQgYAAgHQAAgMgEgDIgDgGQgWAGgPAHQgDgYAAguQAAhTAkhpQAoh5BJhjQBThwBqhAQApBXAkBPQBdDVAVB1QhQASABBMQAAAMACACIADAEQAQACATgCIApgIIAKgCQASB8AeBkQAjBvBJCYQhhgWhsgsg");
	this.shape_1.setTransform(38.2,62.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ADeFPQjaAAjehuQhQgng7gqQgcgVgMgMQgLALgMAAQhPAAhfhhQgighgaglQgXgiAAgKQAAgtAFgJQAHgNAYgLQAPgHAWgGIADAFQADAEAAAMQAAAHgPAYQgQAZAAAOQAAAiDFB6QANgHAHglQAHgiAcgPQAYAWAAADQAAAUgSAuIgMAZQB9A+BsAsQBrAsBgAWQBaAUBSACQBuACBchSQAXgUAugxQAngsAWgQQAFgEAEgBQAFgBADADQAEACABAEIADAFQADAEAHgFQAIgGgkA3QgjA3haBKQhaBLh/AAIAAAAgABbCpQg9gqAAgTQAAguBvACQApABDOAZQAYADAAAZQAAASgvAkQg8AthHAAQhHAAhIgwgAB5B2IA5AgQA0AVA5gVIAcgOQhYgWg8AAQgaAAgUAEgADWATQg8gJgSgUQgJgKAAgXQAAg0AmAYQAqAlALAEQA9APA9gPIAAgSIAIgFQAIgFAMAAQANAAAGACIADACIADAFQADAEAAAMQAAAOgQARIgYAVQggAFgkAAQgjAAgngFgAIPi3QgPgBgSgJQgWgLgPgRQgOgPgJgEQgQgHgdAEQgMADgLAEQgHgCgEgEIgDgFIgCgEIgCgDIADgFQAagpBpACQBhACA4AeQAVALAMARQALAPgBAIIgBAHIAAABIgBAAQgLgGgQgDIgOgBIgIgBQgJABgHADIgOAFQgGACgPALQgMAJgKACQgLADgKAAIgGgBgAhDjeQgIgEgaADIgKABIgpAIQgUACgPgCIgDgDQgDgDAAgLQAAhOBPgSQAigJAvADQBwAGCYA5QANAFgPAKQgPAJgZADIgbAAIggAAQgdAiguAFQgRACgQAAQgxAAgogUgABFj/IAEgBIgEAAIAAABg");
	this.shape_2.setTransform(67.8,90.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhMFzQAbgkAKgTIARghQAWg8ARhAQAKgoACg3QACgbAAhGQgBg8ABgdQAAgLgVhsQgThtAAgLQAAgUAIgBQADgBAEAIQACgFAIALQAFAKATBiQARBXAGApIAIA1QAHA9gBAjQgEBpgWBMQgPAygOAjQgUAwgcAwQgDAFgIAAQgNAAgagMg");
	this.shape_3.setTransform(128.5,63.4);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EACFB7").s().p("AltD/QhJiwgai2QAAiMAzCJQAVA7A7DMQAPAgAYBGQAYBIAQAiQAOAfASAaIgShtQgOhZgehDQgnhYgpiFQgniBAAghQAAgEACgFQgMgDgHgEQgOgJAAgbQAAgIAKgGIAAgGQAAgKAEgHIg2gDIgZgYQgGgHAAACQgWhfBUgVQBUgVAbAGQAbAGAyhXQAzhXBZBSQBZBTCFAQQBXAKA0ATQAxARBNAWQA2ARAEATQAcAZAdCsQAOBXAJBSQAAAXgKA9QgKA+AAAVQAAAegXA9QgeBQgxBDQiPDDjqAAQjnAAiRlegABcIcIAOAAIANgEIgbAEg");
	this.shape_4.setTransform(84.2,60.5);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,136.3,124.1);


(lib.LucyHaircopy = function() {
	this.initialize();

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AmzJRQiNgYhlhSQhNg9gkhNQgig6ghg9QhBh5AAgbQAAhZAxhkQA1htBmhdQD2jiGxg5IGoAAQFGAMCEDKQBQB7AACxQAABvhHBuQhLBziMBbQlADTogAhg");
	mask.setTransform(107.7,143.2);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E5C45A").s().p("AAuIkQgfgcgXgUQimh+hIiMQhCiAAAikQAAjwB5inQBBhZBeg8QAgBgAtBrQBOC3BdCZQA7BfAmArQhGAHguAUQgPAKgHAKQgGAGgHATIgBACQgWAmgSBDQgSBDAAAqIAFBVQAKBjAYBRQg/goghgcg");
	this.shape.setTransform(36.2,111);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFDA64").s().p("AHuHlQj2hQi9grQjfgzh7AIIgiADQgmgrg6hhQhdiZhPi1QguhrghhhQAUgMAVgMQAVgLAWgKQBqgxBsgSQEYguDOA3QCRAmBiBTQBiBTBACpQAhBUAsC1QAEASALBeQASBeAmAtQgmAhgwAQQgfALgaAAQgRAAgPgFg");
	this.shape_1.setTransform(99.8,85.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AGKIgQgpgCgrgGQgSgChAgTQhSgYhTgTQhWgVhggZIhbgXQhYgQhhgBQhbgCgbANQgEACgUAHQgTAKgGASIgBABQggAcgJAAIgEAAIAIgRQAHgZgMgUIABgBQAHgUAGgGQAHgKAPgJQAugVBGgHIAhgDQB7gHDiAzQC6AqD3BQQAlAMA0gSQAvgQAmggQgmgtgRhfQgLhegFgSQgsi2gghSQhBiqhihTQhihTiPgmQjQg2kYAuQgFAAAAgCIAAgWQAAgUAXgNQAZgPA0gGQBbgLCMASQCKARByAkQB+AoAuAuQBLBLAkAwQAsA5AfBHQAXA0AWBbQAMAzASBaQAeCGAoBSQAfBBAAAFQAAAQgiAkQgwAzhGAMQgOADgZAAIgYgBg");
	this.shape_2.setTransform(113,84.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#020100").s().p("ADyLqQhKAAieiKQh7hugvhAQh6ingXhIQgMgmgCg0QgBgRAAhlQAAjxBuiwQBhiHBmhDIAUgNQAXgNAYgMQA5gbA8gTQBRgZBDgEIAAAAQgXAOAAAUIgBAWQABACAFAAQhsAShqAwQgWAKgVAMQgVALgTANQhdA9hABYQh5CnAADwQAACkBCCAQBICMCjB/QAaATAeAdQAhAcBAAnQgZhRgJhjIgFhUQAAgqAShDQAShDAWgnQAMAUgIAZIgIARIAFAAQAJAAAggcIABgBIgYBVQgJAogBA1QgBAuADAYQAFAkAYBUQAIAbAHAuQAGApAJAZIACAHIAAABQAAATgEgFQAAAAgBAAQAAAAAAABQgBAAAAABQAAABgBABQgEAEgMAAg");
	this.shape_3.setTransform(32.3,107.1);

	this.shape.mask = this.shape_1.mask = this.shape_2.mask = this.shape_3.mask = mask;

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(15.3,83.9,158.7,97.8);


(lib.LucyHair = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E5C45A").s().p("AAuIkQgfgcgXgUQimh+hIiMQhCiAAAikQAAjwB5inQBBhZBeg8QAgBgAtBrQBOC3BdCZQA7BfAmArQhGAHguAUQgPAKgHAKQgGAGgHATIgBACQgWAmgSBDQgSBDAAAqIAFBVQAKBjAYBRQg/goghgcg");
	this.shape.setTransform(36.2,111);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFDA64").s().p("AHuHlQj2hQi9grQjfgzh7AIIgiADQgmgrg6hhQhdiZhPi1QguhrghhhQAUgMAVgMQAVgLAWgKQBqgxBsgSQEYguDOA3QCRAmBiBTQBiBTBACpQAhBUAsC1QAEASALBeQASBeAmAtQgmAhgwAQQgfALgaAAQgRAAgPgFg");
	this.shape_1.setTransform(99.8,85.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AGKIgQgpgCgrgGQgSgChAgTQhSgYhTgTQhWgVhggZIhbgXQhYgQhhgBQhbgCgbANQgEACgUAHQgTAKgGASIgBABQggAcgJAAIgEAAIAIgRQAHgZgMgUIABgBQAHgUAGgGQAHgKAPgJQAugVBGgHIAhgDQB7gHDiAzQC6AqD3BQQAlAMA0gSQAvgQAmggQgmgtgRhfQgLhegFgSQgsi2gghSQhBiqhihTQhihTiPgmQjQg2kYAuQgFAAAAgCIAAgWQAAgUAXgNQAZgPA0gGQBbgLCMASQCKARByAkQB+AoAuAuQBLBLAkAwQAsA5AfBHQAXA0AWBbQAMAzASBaQAeCGAoBSQAfBBAAAFQAAAQgiAkQgwAzhGAMQgOADgZAAIgYgBg");
	this.shape_2.setTransform(113,84.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#020100").s().p("ADyLqQhKAAieiKQh7hugvhAQh6ingXhIQgMgmgCg0QgBgRAAhlQAAjxBuiwQBhiHBmhDIAUgNQAXgNAYgMQA5gbA8gTQBRgZBDgEIAAAAQgXAOAAAUIgBAWQABACAFAAQhsAShqAwQgWAKgVAMQgVALgTANQhdA9hABYQh5CnAADwQAACkBCCAQBICMCjB/QAaATAeAdQAhAcBAAnQgZhRgJhjIgFhUQAAgqAShDQAShDAWgnQAMAUgIAZIgIARIAFAAQAJAAAggcIABgBIgYBVQgJAogBA1QgBAuADAYQAFAkAYBUQAIAbAHAuQAGApAJAZIACAHIAAABQAAATgEgFQAAAAgBAAQAAAAAAABQgBAAAAABQAAABgBABQgEAEgMAAg");
	this.shape_3.setTransform(32.3,107.1);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.1,30,173.9,151.7);


(lib.LucyClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#45635D").s().p("AAGLfQgzgFgHgDIAIgYQAGgPAPgIIAOgGIAGgCIAOgEIAAAHQAAAgBiAZIAaAGQgdABgiAAQgRAAgxgEgAEyLEQAAgNgEgNQAGAAAFgHQAIgKAFgEQAKgHAPgFQALAtANAPQAIAKAUAJIhIAFQgUABgHACQACgLAAgRgACxhmQhMgKh/gPQhkgMhVgRQAGgIAGgTQAOgmAIg7IAOhSQAFgaAAgeIAAgKIAAgCIgBgJIAAgHQAGgCAHgBQAFgBAJANQAIAKAEAMQADAGAIAwQAPA1AnAsQAnAsA2AWQAgANA+ALQA3AKAVALQAhASAIAnQgqgCgegEgAk+oAIg5gUQgZgKgCgJQgDgbAmgsQAfgjAkgZQAtgfBKgZQgVAPgjAvQgmA0gVAuQgGAOgFAgQgDAUgHAAIgBAAg");
	this.shape_1.setTransform(55.5,89.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#648C83").s().p("Ah3MoQhjgYAAghIABgHQA6gNCNABQASACAPAQQAQAPgBARIgFAFQgHAHgQAFQghAMg+ADIgagGgACQMSQgNgPgKgtQAcgJAyAAQBIAAAqATQAjAQAJAaQACAIghAEQg2AGhkAIQgTgIgJgKgAATgTQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgDgTgHgFQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgLgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6QArA7AFA/QAGA6gdArQgOAWgPANQAAALACACIACAEQADADATAAQAGAAAIgEIANgGQAAANAFALIALAdQAKAgAOAYIheAUQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_2.setTransform(78.6,81.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7A7A7A").s().p("AgaDYIiSg1IgOgDQAPgYAmhQIALgWQAVACAZABQAqACAigOIAHgGQALgFAQgOQAYgTAGgTQAFgWgQgxQgLgigJgPQgTgkgKgUQAQgfAGgRQApAZB0AQIAEABIgiDXQgeDIgCBEQhJgUhKgbg");
	this.shape_3.setTransform(29.7,103.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB59F").s().p("AFfIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAAGGiIgOgcQAzAMA1AJIAmB3QgjAGgdAMQgghIggg6gAl6ATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUg");
	this.shape_4.setTransform(47.9,96.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#8E8E8E").s().p("AoKDcQABhFAejHIAjjXQA+AIC8ASQBZAICugFQC8gGBYgRQAdgFATgHIBdEUQAnB0AKASQgRAKgQAFIgQAEQhtAhifAmQgXAFiOAQQiZASgwAAQitAAi9gyg");
	this.shape_5.setTransform(94.2,107.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EACFB7").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAIVirQACgCgKgQQgLgPgEAAIgBABIAAgBIgDgHQgCgFgFgXQgGgZgHgSIgPglQgFgMAAgDQAIgjACgVQADgegDgbQgHhDgGgWIAWgJQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APQgiAJgFACQgFABgEACQgohzghg7g");
	this.shape_6.setTransform(82.3,95.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgSgcgcg3QgzhuAAACIAAgFQigguichVIAAABQgPgEgFgHIgDgCIABgEIgBgPQAAgWAxhoQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQAHAGADASIABAHIABAJIAAACIAAAKQAAAegFAaIgOBSQgJA6gNAnQgHASgFAJQBUARBlAMQCBAOBLALQAfADAoADQBFAEBjgBQCkgCBAgOIBegUQgOgYgKgfIgLgeQgFgLAAgMIgNAGQgIADgGAAQgTAAgDgDIgCgEQgCgCAAgLQAPgNAOgWQAdgrgGg6QgFg/grg7Qgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgIABIAEAKQAxCcAAA7IAAAKQgGAMgFAEQgFAEgGAAIj0BHIgrANQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgABxIEQgqACgpABQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMQh0Aah0AKgAkuH/QAgA7AgBIQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJQhkAAhigPQg1gJg1gMIAQAbgApxDWQgmBQgQAYIAPADICSA1QBMAbBJAUQC9AyCtAAQAvAACagRQCPgQAWgFQCfgmBtgiIARgEQAPgFARgJQgKgTgmh0IhekTQgTAGgdAFQhXARi8AGQixAFhXgIQi8gRg+gJIgEgBQh1gQgogZQgGARgQAfQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgiAOgrgCQgYgBgVgCIgLAWgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAIvhnQAKAPgCADQAhA6AoB0QAEgDAFgCQAFgDAigIQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJQAGAXAHBCQADAcgDAeQgCAVgIAjQAAACAFAMIAPAmQAHASAGAYQAFAYACAEIADAHIAAABIABgBQAEAAALAQgAnWjiQgYA3gEAsQALgNAIgEIgCgEQgBgDAAgNQAAgIAKgkIAIggIgGAOgAF1rgIgDgFIADAFgAj5sUQAJgHAMgHQgMAHgJAHg");
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.lorry_shadow = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(51,51,51,0.502)").s().p("A4iFUMAgsgS3IQYHiMgiWATlg");
	this.shape.setTransform(157.1,86.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,314.2,173.7);


(lib.lorry_loading_stripe = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,0,0.298)").s().p("ACKFyIA8gjIBMAsIA9gjIA8AjIh5BFgAGHE4ICvhkIA8AjIivBkgAhyDhIA8gjIC8BsIg8AjgAJpC3IAugaIhLgrIA9gjICHBOIhqA9gAl7BKIA9gjIDOB2Ig8AjgAFOgdIA9gjIC6BpIg9AigApUgwIA9gjICfBZIg7AjgABFi1IA9giIDIByIg7AjgAsPicIB5hFIA8AjIg8AiIBFAoIg8AjgAiSkwIA9gjICXBYIg8AigApekBICuhkIABABIA8AiIiuBkgAkSl6IgtAaIg9giIBqg9IB9BIIg8Aig");
	this.shape.setTransform(78.5,44.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,157,89.7);


(lib.LittleDude_02_WalkingBack = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.Little_Dude_Walking_02_Back__006();

	this.instance_1 = new lib.Little_Dude_Walking_02_Back__007();

	this.instance_2 = new lib.Little_Dude_Walking_02_Back__008();

	this.instance_3 = new lib.Little_Dude_Walking_02_Back__009();

	this.instance_4 = new lib.Little_Dude_Walking_02_Back__010();

	this.instance_5 = new lib.Little_Dude_Walking_02_Back__011();

	this.instance_6 = new lib.Little_Dude_Walking_02_Back__012();

	this.instance_7 = new lib.Little_Dude_Walking_02_Back__013();

	this.instance_8 = new lib.Little_Dude_Walking_02_Back__014();

	this.instance_9 = new lib.Little_Dude_Walking_02_Back__015();

	this.instance_10 = new lib.Little_Dude_Walking_02_Back__016();

	this.instance_11 = new lib.Little_Dude_Walking_02_Back__017();

	this.instance_12 = new lib.Little_Dude_Walking_02_Back__018();

	this.instance_13 = new lib.Little_Dude_Walking_02_Back__019();

	this.instance_14 = new lib.Little_Dude_Walking_02_Back__020();

	this.instance_15 = new lib.Little_Dude_Walking_02_Back__021();

	this.instance_16 = new lib.Little_Dude_Walking_02_Back__022();

	this.instance_17 = new lib.Little_Dude_Walking_02_Back__023();

	this.instance_18 = new lib.Little_Dude_Walking_02_Back__024();

	this.instance_19 = new lib.Little_Dude_Walking_02_Back__025();

	this.instance_20 = new lib.Little_Dude_Walking_02_Back__026();

	this.instance_21 = new lib.Little_Dude_Walking_02_Back__027();

	this.instance_22 = new lib.Little_Dude_Walking_02_Back__028();

	this.instance_23 = new lib.Little_Dude_Walking_02_Back__029();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,42,58);


(lib.LittleDude_01_WalkingBack = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.Little_Dude_Walking_01_Back__006();

	this.instance_1 = new lib.Little_Dude_Walking_01_Back__007();

	this.instance_2 = new lib.Little_Dude_Walking_01_Back__008();

	this.instance_3 = new lib.Little_Dude_Walking_01_Back__009();

	this.instance_4 = new lib.Little_Dude_Walking_01_Back__010();

	this.instance_5 = new lib.Little_Dude_Walking_01_Back__011();

	this.instance_6 = new lib.Little_Dude_Walking_01_Back__012();

	this.instance_7 = new lib.Little_Dude_Walking_01_Back__013();

	this.instance_8 = new lib.Little_Dude_Walking_01_Back__014();

	this.instance_9 = new lib.Little_Dude_Walking_01_Back__015();

	this.instance_10 = new lib.Little_Dude_Walking_01_Back__016();

	this.instance_11 = new lib.Little_Dude_Walking_01_Back__017();

	this.instance_12 = new lib.Little_Dude_Walking_01_Back__018();

	this.instance_13 = new lib.Little_Dude_Walking_01_Back__019();

	this.instance_14 = new lib.Little_Dude_Walking_01_Back__020();

	this.instance_15 = new lib.Little_Dude_Walking_01_Back__021();

	this.instance_16 = new lib.Little_Dude_Walking_01_Back__022();

	this.instance_17 = new lib.Little_Dude_Walking_01_Back__023();

	this.instance_18 = new lib.Little_Dude_Walking_01_Back__024();

	this.instance_19 = new lib.Little_Dude_Walking_01_Back__025();

	this.instance_20 = new lib.Little_Dude_Walking_01_Back__026();

	this.instance_21 = new lib.Little_Dude_Walking_01_Back__027();

	this.instance_22 = new lib.Little_Dude_Walking_01_Back__028();

	this.instance_23 = new lib.Little_Dude_Walking_01_Back__029();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,42,58);


(lib.Little_Dude_02_WalkingFront = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.Little_Dude_Walking_02__006();

	this.instance_1 = new lib.Little_Dude_Walking_02__007();

	this.instance_2 = new lib.Little_Dude_Walking_02__008();

	this.instance_3 = new lib.Little_Dude_Walking_02__009();

	this.instance_4 = new lib.Little_Dude_Walking_02__010();

	this.instance_5 = new lib.Little_Dude_Walking_02__011();

	this.instance_6 = new lib.Little_Dude_Walking_02__012();

	this.instance_7 = new lib.Little_Dude_Walking_02__013();

	this.instance_8 = new lib.Little_Dude_Walking_02__014();

	this.instance_9 = new lib.Little_Dude_Walking_02__015();

	this.instance_10 = new lib.Little_Dude_Walking_02__016();

	this.instance_11 = new lib.Little_Dude_Walking_02__017();

	this.instance_12 = new lib.Little_Dude_Walking_02__018();

	this.instance_13 = new lib.Little_Dude_Walking_02__019();

	this.instance_14 = new lib.Little_Dude_Walking_02__020();

	this.instance_15 = new lib.Little_Dude_Walking_02__021();

	this.instance_16 = new lib.Little_Dude_Walking_02__022();

	this.instance_17 = new lib.Little_Dude_Walking_02__023();

	this.instance_18 = new lib.Little_Dude_Walking_02__024();

	this.instance_19 = new lib.Little_Dude_Walking_02__025();

	this.instance_20 = new lib.Little_Dude_Walking_02__026();

	this.instance_21 = new lib.Little_Dude_Walking_02__027();

	this.instance_22 = new lib.Little_Dude_Walking_02__028();

	this.instance_23 = new lib.Little_Dude_Walking_02__029();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,42,58);


(lib.Little_Dude_01_WalkingFront = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.Little_Dude_Walking_01__006();

	this.instance_1 = new lib.Little_Dude_Walking_01__007();

	this.instance_2 = new lib.Little_Dude_Walking_01__008();

	this.instance_3 = new lib.Little_Dude_Walking_01__009();

	this.instance_4 = new lib.Little_Dude_Walking_01__010();

	this.instance_5 = new lib.Little_Dude_Walking_01__011();

	this.instance_6 = new lib.Little_Dude_Walking_01__012();

	this.instance_7 = new lib.Little_Dude_Walking_01__013();

	this.instance_8 = new lib.Little_Dude_Walking_01__014();

	this.instance_9 = new lib.Little_Dude_Walking_01__015();

	this.instance_10 = new lib.Little_Dude_Walking_01__016();

	this.instance_11 = new lib.Little_Dude_Walking_01__017();

	this.instance_12 = new lib.Little_Dude_Walking_01__018();

	this.instance_13 = new lib.Little_Dude_Walking_01__019();

	this.instance_14 = new lib.Little_Dude_Walking_01__020();

	this.instance_15 = new lib.Little_Dude_Walking_01__021();

	this.instance_16 = new lib.Little_Dude_Walking_01__022();

	this.instance_17 = new lib.Little_Dude_Walking_01__023();

	this.instance_18 = new lib.Little_Dude_Walking_01__024();

	this.instance_19 = new lib.Little_Dude_Walking_01__025();

	this.instance_20 = new lib.Little_Dude_Walking_01__026();

	this.instance_21 = new lib.Little_Dude_Walking_01__027();

	this.instance_22 = new lib.Little_Dude_Walking_01__028();

	this.instance_23 = new lib.Little_Dude_Walking_01__029();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,42,58);


(lib.LiftBitUpright = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#757575").s().p("ACIDaIABgCQAAgNgEgGQADguAIhQQAKhaAAgLIAAgtQAVASAQADQgFAWgCAgQgBAcAABGQAABAABAZQADAkAGAXIgFADgAi/AZQABgMgBgDQACg6gBhdQAAhJgBgSIAAgNQACgBACgCQAKAOAeACQgIAYgDAsQgEApAAAzQAAA6ACAYQABAQgBARg");
	this.shape.setTransform(22.4,59);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AC9ETIADgCIAFgCQgGgYgDgjQgBgZAAhAQAAhHABgbQACghAFgVQgQgDgVgSIAAAtQAAAIgKBdQgIBQgDAuQAEAGAAANIgBACIgBABIgkgWIACgBIgBgDQgCgKAAgiQAAjsAngqIABABQAOgEAbAWIAqAlIgEAMIgFAFQAGAXACAhIABBcQgBBhgBA9IACADIgCACIAAAKIgBAAIgEACgAicBEQABgRgBgQQgCgaAAg5QAAgyAEgpQADgsAIgYQgegCgKgOQgCACgCAAIAAAOQABARAABKQABBdgCA6QABADgBAMIgZgBQgIgtgCgZQgCgYABhAQgBhHACgcQAEhCAOgRIAEAJQAGgFAMAEIAMALQARAOAtASQABAYgEADIAAALIgEB4QgEBogBA+g");
	this.shape_1.setTransform(22,56.5);

	// Layer 3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#757575").s().p("AiOAqIhHgnQAhARAAgWQAAgUATgEQAPALAnAPQAyAXAcAPIBnA8QAtAZATAAQAPAAAHgHIAFAFQArAqAAAoQAAAWgNAGQiwiPihgugAC7gJIgigPIgOgGQg5gbhaguIg7geIgggOQgngRgWgGIAHARIgBgDQgRgZAAgXQAAgSALgIQAZABAzAYIAtAXQAmAVAuAeQA/AmA7AnIAOAKIARALQALAAAFgDQAAAMgGAJQgGAJgMAFg");
	this.shape_2.setTransform(22.3,59.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAQCZQhhg/iIhMIACgNIBIAmQCgAuCwCQIARAOQgEANgPAGQghgTiOhagABIBfIhng7QgbgQgzgUQgngSgOgLIgTgPQAsgQB1A7QA/AeB3BDIACAFQgDAJgDAEQgHAGgPAAQgSAAgugZgACZASQgkgLgmgPQghgQg7ggQhHgngTgMQgmgYgNgZIgIgQQAWAFAoASIAfAOIA8AdQBZAvA5AaIAOAHIAjAOIADAIIACAFQgCAHgJAGQgIAGgJAAIgKgCgACzgvIgOgJQg6gnhAgnQgugdgmgVIgtgYQgzgYgYgBIgDAAQgNAAgIAIQgKAKgFABQgLgEgIgHQAwhMCeBVQBAAjCpB8IgEAOIgEAEQgFAEgLAAIgRgMg");
	this.shape_3.setTransform(22.4,60);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.5,27.2,92.5,59);


(lib.LiftBitBack = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#AAAAAA").s().p("AhCDQIgIgEQA0gZCnhZQCvhbAvgjIAMgKQANAHALAKQAIAIAIAFIgEACQgiAOgtAaQhVA0g8AjQgkAVheAyQhAAjgPANQgagLgWgNgAmiADQA4gnA+gfIBqgzQAggPCNg9QAwgWATgPIABACQARALAeACIAAABIgJAGIAAABQgyAjirBaQiyBaggAcQgsgYgcgIg");
	this.shape.setTransform(45.6,59.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgeEOIgOABQgYAAgZgYQgUgUAAgHIAAgHQgDAAgCgCQgBgEgEgDQAbgeBFgpQAkgXBug6QAUgKBMgsQA+gmA3gbQAFgGAJgCQATgEAaAJQBBAVAAAmIgBAGIAEACIgEABIAAACQgEAKgOAAIgCAAQhSBJijBaQiwBigeAAQgLAAgDgBgAFrgjQgvAjivBbQinBZg0AZIAIAEQAWANAaALQAPgNBAgjQBegyAkgVQA8gjBVg0QAtgaAigOIAEgBQgIgGgIgIQgLgKgNgHIgMAKgAmYA3QgggagJgEIAAgBIgEgCQgFgFAAgPQAAgYBHgtQA9gmB9g6IBrgzQA6gcA1gWIAHgCIgBgEIAKAGIACgBIAIABQAKABAOAFQArAOAHATQAGAGAAADQgBAJgDAFIAAAEIAAADQgQAehUAlQhtAygqAdQgWAOhYAxIhOAsQgGAQggAAQgbAAgXgTgAgZjBQiNA9ggAPIhqAzQg+Afg4AnQAcAIAsAYQAggcCyhaQCrhaAygjIAAgBIAJgGIAAgBQgegBgRgMIgBgBQgTAOgwAWg");
	this.shape_1.setTransform(46,59);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.5,27.2,92.5,59);


(lib.LiftBit = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#AAAAAA").s().p("AhLDWIgIgDQA0gZCnhaQC6hgAqgiQAHgGADgEIACgDIA0AeIgCACIgTAHQgiAOguAbQhVA0g8AjQgkAVheAyQhAAjgPAMQgagKgWgOgAmrAKQA4gnA+gfIBqgzQAggQCNg8QA/gdANgVQAYAHAJALQgpAvi3BgQiyBaggAcQgsgYgcgIg");
	this.shape.setTransform(46.5,58.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgdEMIgPABQgXAAgZgYQgUgUgBgHIABgHQgEAAgBgCQgCgEgDgDQAbgeBFgpQAkgXBug6QAUgKBDgoQBEgmAagOQAagOADgDIAIgHIAAgBIApAaQgCAEgHAFQgqAii6BhQinBZg0AZIAHAEQAXANAaALQAPgNA/gjQBfgyAkgVQA8gjBUg0QAugZAjgPIASgIIAbANQgGAIgFABQhRBLivBhQiwBigeAAQgLAAgDgBgAmXA1QgggagJgEIAAgBIgEgCQgGgFAAgPQABgYBHgtQA9gmB9g6IBqgzQA7gcA1gWIAGgCIANASQAGADADAEQACACAEgEQgOAWg/AcQiMA9ggAPIhqAzQg+Afg4AnQAbAIAtAYQAggcCxhaQC3hgApgvQARAQAHAEIAQAKQgdAhhLAiQhtAygrAdQgVAOhYAxIhPAsQgFAQggAAQgbAAgXgTg");
	this.shape_1.setTransform(45.9,59.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,92.6,86.2);


(lib.levelText = function() {
	this.initialize();

	// Layer 1
	this.level_txt = new cjs.Text("LEVEL 1", "35px 'VAGRounded'", "#FFFFFF");
	this.level_txt.name = "level_txt";
	this.level_txt.lineHeight = 41;
	this.level_txt.lineWidth = 129;
	this.level_txt.setTransform(0,0,1.29,1.29);

	this.addChild(this.level_txt);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,171.5,57.4);


(lib.Lead = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Leaderboard", "25px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 31;
	this.text.setTransform(11.8,7.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#502A40").s().p("At5DCIAAmEIbzAAIAAGEg");
	this.shape.setTransform(89,19.5);

	this.addChild(this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,178.1,40.8);


(lib.LadyBoss_Bubble_Yard = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("this is not appropriate\nworkwear. safety comes\nfirst on a busy site, you\nmust wear something to\nprotect your head, hands\nand feet. choose\nsomething else.", "24px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 30;
	this.text.lineWidth = 346;
	this.text.setTransform(131.3,-154.3,1.227,1.227);

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EgjygZYMBBnAAAQCUAAAACTIAAWLIF9EZIl9ECIAAPlQAACTiUAAMhBnAAAQiTAAAAiTMAAAguLQAAiTCTAAg");
	this.shape.setTransform(154.4,-9.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("EgjyAZZQiTAAAAiTMAAAguLQAAiTCTAAMBBnAAAQCUAAAACTIAAWLIF9EZIl9ECIAAPlQAACTiUAAg");
	this.shape_1.setTransform(154.4,-9.4);

	// Layer 5
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("EgjxAZZQiUAAAAiTMAAAguLQAAiTCUAAMBBmAAAQCUAAAACTIAAWLIF9EZIl9ECIAAPlQAACTiUAAg");
	this.shape_2.setTransform(163.7,-0.1);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-91.4,-174,499,339.7);


(lib.LadyBoss = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgdAUIgZhNQAAhAAeAlQANAQAVAtQAFAVAQAiIAYA2IgBAGQgBgBgIAAQgDAAgHAEQgGADgEAAQgYAAgehOg");
	this.shape.setTransform(263.9,241.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BC8E67").s().p("AACApQgFAAgBgIIAAgHIAAAAIgBgCQgIgSAAgGIgBAAIADgCIAAgLIADgKQABgGAHAAQACAEAAAHQAAAGgCADIAAADIAGgBIAAgRQAAgLAEgEQAFAGAAAVIAAAEQgBAJgCAEQgDAJgDAQIADAGQgCAEgEABIgBAAg");
	this.shape_1.setTransform(162,262.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DAA577").s().p("AijBFQiigQhsgeQg6gRgagOQAEgBABgEIgCgGQADgQADgJQACgEAAgLIABAAQAJgFAUgEQAKAEADAJIgBACIAAAAIABAQIgBAUIAaAJQBNAVBNANIACgBQCAgBAqAFQAVADAIAIICwAAQgCgDAAgHQAAgJAEgCIAJgBIAsgDQAHgIAFgCQBpgFAbgFICJAAQAPAEAEAHQABACAAALQAAAIgDAGQADACAAAJIgBAIIAOgDQgFgBgBgBQAAgFgBgCQAKgCABAEQAiAAADgBQABgBAAAAQABAAAAAAQAAgBABAAQAAgBAAgBQAGgBAFAAIABAEQgFADgEAEQgEADgPABIgLACIg7AOIgcAAIgBAAIgBAAIgEACgAG8A0IgBABIAEgBIgCAAIgBAAgAGeAUQgLAAAAABQADAFALAAQAHgDAAgEIgKABg");
	this.shape_2.setTransform(214.2,267.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(27));

	// Layer 5
	this.instance = new lib.Health_and_Safety_Officer__000();
	this.instance.setTransform(68.5,0);

	this.instance_1 = new lib.Health_and_Safety_Officer__001();
	this.instance_1.setTransform(68.5,0);

	this.instance_2 = new lib.Health_and_Safety_Officer__002();
	this.instance_2.setTransform(68.5,0);

	this.instance_3 = new lib.Health_and_Safety_Officer__003();
	this.instance_3.setTransform(68.5,0);

	this.instance_4 = new lib.Health_and_Safety_Officer__004();
	this.instance_4.setTransform(68.5,0);

	this.instance_5 = new lib.Health_and_Safety_Officer__005();
	this.instance_5.setTransform(68.5,0);

	this.instance_6 = new lib.Health_and_Safety_Officer__006();
	this.instance_6.setTransform(68.5,0);

	this.instance_7 = new lib.Health_and_Safety_Officer__007();
	this.instance_7.setTransform(68.5,0);

	this.instance_8 = new lib.Health_and_Safety_Officer__008();
	this.instance_8.setTransform(68.5,0);

	this.instance_9 = new lib.Health_and_Safety_Officer__009();
	this.instance_9.setTransform(68.5,0);

	this.instance_10 = new lib.Health_and_Safety_Officer__010();
	this.instance_10.setTransform(68.5,0);

	this.instance_11 = new lib.Health_and_Safety_Officer__011();
	this.instance_11.setTransform(68.5,0);

	this.instance_12 = new lib.Health_and_Safety_Officer__012();
	this.instance_12.setTransform(68.5,0);

	this.instance_13 = new lib.Health_and_Safety_Officer__013();
	this.instance_13.setTransform(68.5,0);

	this.instance_14 = new lib.Health_and_Safety_Officer__014();
	this.instance_14.setTransform(68.5,0);

	this.instance_15 = new lib.Health_and_Safety_Officer__015();
	this.instance_15.setTransform(68.5,0);

	this.instance_16 = new lib.Health_and_Safety_Officer__016();
	this.instance_16.setTransform(68.5,0);

	this.instance_17 = new lib.Health_and_Safety_Officer__017();
	this.instance_17.setTransform(68.5,0);

	this.instance_18 = new lib.Health_and_Safety_Officer__018();
	this.instance_18.setTransform(68.5,0);

	this.instance_19 = new lib.Health_and_Safety_Officer__019();
	this.instance_19.setTransform(68.5,0);

	this.instance_20 = new lib.Health_and_Safety_Officer__020();
	this.instance_20.setTransform(68.5,0);

	this.instance_21 = new lib.Health_and_Safety_Officer__021();
	this.instance_21.setTransform(68.5,0);

	this.instance_22 = new lib.Health_and_Safety_Officer__022();
	this.instance_22.setTransform(68.5,0);

	this.instance_23 = new lib.Health_and_Safety_Officer__023();
	this.instance_23.setTransform(68.5,0);

	this.instance_24 = new lib.Health_and_Safety_Officer__024();
	this.instance_24.setTransform(68.5,0);

	this.instance_25 = new lib.Health_and_Safety_Officer__025();
	this.instance_25.setTransform(68.5,0);

	this.instance_26 = new lib.Health_and_Safety_Officer__026();
	this.instance_26.setTransform(68.5,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(68.5,0,260,369);


(lib.keys_mc = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Z KEY", "13px 'Laffayette Comic Pro'");
	this.text.lineHeight = 42;
	this.text.lineWidth = 272;
	this.text.setTransform(0,384,1.18,1.18);

	this.text_1 = new cjs.Text("A KEY", "13px 'Laffayette Comic Pro'");
	this.text_1.lineHeight = 42;
	this.text_1.lineWidth = 272;
	this.text_1.setTransform(0,317,1.18,1.18);

	this.text_2 = new cjs.Text("SPACE BAR", "13px 'Laffayette Comic Pro'");
	this.text_2.lineHeight = 42;
	this.text_2.lineWidth = 272;
	this.text_2.setTransform(0,258,1.18,1.18);

	this.text_3 = new cjs.Text("DOWN ARROW", "13px 'Laffayette Comic Pro'");
	this.text_3.lineHeight = 42;
	this.text_3.lineWidth = 272;
	this.text_3.setTransform(0,194,1.18,1.18);

	this.text_4 = new cjs.Text("UP ARROW", "13px 'Laffayette Comic Pro'");
	this.text_4.lineHeight = 42;
	this.text_4.lineWidth = 272;
	this.text_4.setTransform(0,133,1.18,1.18);

	this.text_5 = new cjs.Text("LEFT ARROW", "13px 'Laffayette Comic Pro'");
	this.text_5.lineHeight = 42;
	this.text_5.lineWidth = 272;
	this.text_5.setTransform(0,70,1.18,1.18);

	this.text_6 = new cjs.Text("RIGHT ARROW", "13px 'Laffayette Comic Pro'");
	this.text_6.lineHeight = 42;
	this.text_6.lineWidth = 270;
	this.text_6.setTransform(0,0,1.18,1.18);

	this.addChild(this.text_6,this.text_5,this.text_4,this.text_3,this.text_2,this.text_1,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,325.3,425.7);


(lib.KateClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#371E2D").s().p("AinAoQg1gEgHgDIAJgZQAFgMAPgJIAOgGIAGgCIARgEIgBAHQAAAeBjAZIAaAGQgdACgjgBQgRABgxgFgACDAOQAAgOgEgKQAGAAAGgIQAIgKAEgDQAKgIAQgFQAKAsANAPQAJAKATAIIhIAGQgUABgGABQABgKAAgRg");
	this.shape_1.setTransform(73,158.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#4D2A3E").s().p("Ai8AqQhjgYAAgfIABgHQA6gNCNABQATACAQAQQAQAOgBAQIgFAFQgHAHgQAFQgjAMg+ADIgagGgABLAUQgNgPgKgrQAcgJAyAAQBIAAAqATQAjAQAJAYQACAIghAEQg2AGhkAIQgTgIgJgKg");
	this.shape_2.setTransform(85.5,158.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7A7A7A").s().p("AgaDYIiSg1IgOgDQAPgYAmhQIALgWQAVACAZABQAqACAigOIAHgGQALgFAQgOQAYgTAGgTQAFgWgQgxQgLgigJgPQgTgkgKgUQAQgfAGgRQApAZB0AQIAEABIgiDXQgeDIgCBEQhJgUhKgbg");
	this.shape_3.setTransform(29.7,103.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB59F").s().p("AFfIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAAGGiIgOgcQAzAMA1AJIAmB3QgjAGgdAMQgghIggg6gAl6ATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUg");
	this.shape_4.setTransform(47.9,96.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#381E2D").s().p("AD+E6QhMgKiBgOQhigMhVgRQAGgJAGgSQAOgnAIg6IAOhSQAFgaAAgdIAAgKIAAgBIgBgJIAAgHQAGgDAHgBQAFAAAJAMQAIALAEAJQADAHAIAvQAPA2AlAsQAnAsA4AWQAgAMA+AMQA3AKAVAKQAhASAIAoQgqgDgegEgAjxheIg5gUQgZgJgCgJQgDgbAmgsQAfgjAkgZQAtggBKgZQgVAQgjAvQgmAzgVAuQgGAPgFAgQgDATgHAAIgBAAg");
	this.shape_5.setTransform(47.8,47.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#8E8E8E").s().p("AoKDcQABhFAejHIAjjXQA+AIC8ASQBZAICugFQC8gGBYgRQAdgFATgHIBdEUQAnB0AKASQgRAKgQAFIgQAEQhtAhifAmQgXAFiOAQQiZASgwAAQitAAi9gyg");
	this.shape_6.setTransform(94.2,107.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EACFB7").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAIVirQACgCgKgQQgLgPgEAAIgBABIAAgBIgDgHQgCgFgFgXQgGgZgHgSIgPglQgFgMAAgDQAIgjACgVQADgegDgbQgHhDgGgWIAWgJQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APQgiAJgFACQgFABgEACQgohzghg7g");
	this.shape_7.setTransform(82.3,95.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#4D2A3F").s().p("AATGLQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgDgTgHgFQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgJgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6QArA5AFA/QAGA6gdArQgOAWgPANQAAALACACIACAEQADADATAAQAGAAAIgEIANgGQAAANAFALIALAdQAKAgAOAYIheAUQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_8.setTransform(78.6,39.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgSgcgcg3QgzhuAAACIAAgFQigguichVIAAABQgPgEgFgHIgDgCIABgEIgBgPQAAgWAxhoQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQAHAGADASIABAHIABAJIAAACIAAAKQAAAegFAaIgOBSQgJA6gNAnQgHASgFAJQBUARBlAMQCBAOBLALQAfADAoADQBFAEBjgBQCkgCBAgOIBegUQgOgYgKgfIgLgeQgFgLAAgMIgNAGQgIADgGAAQgTAAgDgDIgCgEQgCgCAAgLQAPgNAOgWQAdgrgGg6QgFg/grg7Qgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgIABIAEAKQAxCcAAA7IAAAKQgGAMgFAEQgFAEgGAAIj0BHIgrANQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgABxIEQgqACgpABQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMQh0Aah0AKgAkuH/QAgA7AgBIQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJQhkAAhigPQg1gJg1gMIAQAbgApxDWQgmBQgQAYIAPADICSA1QBMAbBJAUQC9AyCtAAQAvAACagRQCPgQAWgFQCfgmBtgiIARgEQAPgFARgJQgKgTgmh0IhekTQgTAGgdAFQhXARi8AGQixAFhXgIQi8gRg+gJIgEgBQh1gQgogZQgGARgQAfQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgiAOgrgCQgYgBgVgCIgLAWgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAIvhnQAKAPgCADQAhA6AoB0QAEgDAFgCQAFgDAigIQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJQAGAXAHBCQADAcgDAeQgCAVgIAjQAAACAFAMIAPAmQAHASAGAYQAFAYACAEIADAHIAAABIABgBQAEAAALAQgAnWjiQgYA3gEAsQALgNAIgEIgCgEQgBgDAAgNQAAgIAKgkIAIggIgGAOgAF1rgIgDgFIADAFgAj5sUQAJgHAMgHQgMAHgJAHg");
	this.shape_9.setTransform(78.9,87.4);

	this.addChild(this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.Jason = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ai4AyQgLgLAAgMQAAgGABgEIg/ABQgSAEgaAIQgTAGgKAAQgLAAgJgBIgGgCIgDgRIAEgMQACgFAGgFQASgLBKgIQA9gQB+ANQAWATABAEQAAADgJAGQgIAHgGAEIgnABQACAFAAAHQAAAVgNAKQgKAHgOAAQgbAAgPgQgADlARQgVgEgJgJQgCgCgBgCIgBgDIABgHIgFgBQgLgCgJgEIgIgDIgBgDIAAgBQgCgCACgLQAGggBbAEQAoACA5AJIACAAIABAFQACAEgCALQgCAJg3ALIgVAEIgCAAIAAACQgFAageAAIgPgBg");
	this.shape.setTransform(96.2,87.3);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D3D37E").s().p("ABhGpQgrgFgkgXIAAAAQAAgRgZgTQgXgQgkgKQgRgFgNgBQgnjbAAgKQAAhHAPhUQAWh/AwhiQAqhXA4g5QArA4AgA2IANAVIgBACIgFAFQgmAygRBdQgVBoATCiQASCcApB7QAJAXgfAAIgNAAg");
	this.shape_1.setTransform(19.9,58.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF99").s().p("AAnDBQkehMhThUQgngmghgJQgcgIgTAPIgNgVQggg2grg4QCFiGDbAVID+AAQDJAtByBkQA9A2A0B2QAZA9AOAyIACAJQgVAfgDAAQgoARhJARQgOADgVAAQhcAAjrg9g");
	this.shape_2.setTransform(78.5,30.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EACFB7").s().p("AjCAwIgLgCQAAgBgEgDQgDgCAAgSIh5AKQgPABgKgCIgIgCQAAAAAAgBQAAAAAAAAQgBAAAAABQgBAAAAABQgDACgBgOQgBgOAFgEQAHgHAdgFQAtgIDZgQIAqgEQAAABBIgBICTgFQAzgDAkACQAyACAWAIQAXAIAAAQQAAAOgMADIgWAAIgOAAQgBAXgCAHIgdAGQgsgCgMgQQgDgDgBgUIghgCQkEAHgKgEIgBAAIgxAJQgBAOgLALQgNALgUACIgPAAIgOAAg");
	this.shape_3.setTransform(94.6,87.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DEBBA4").s().p("ADVJFQhIgbhMhLQgdgcgTgsIgZg6IAKgHQATgMAAgXQAAgMgBgGQgHgOgSAAQgLAAgiAMQghAKgMABQgngBgmgTQg1gdgjhCQgDgGAAgKIABgTQAAgkAagUQAKgHAHgIQAfAIAcAPQATALAaAAQAKAAAHgCQAIgFABgNQAkAXArAFQArAEgKgbQgnh5gSicQgTikAVhoQARhdAkgyIAFgGQBwCWAVCyQAOBsAHD6QATCbAZCEQAZCEAPBbQgbgHgYgKg");
	this.shape_4.setTransform(30.8,89.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgKMJQg8gJgsgNQhsgghKg0Qh0hQgjiCIAAgBIgiABQhlAAg5hEQgogvgCgvIgJgFQgDgVAAgWQAAgsAFgTQAKgqAggKQgLgegKhqIgIhmQAAjkBXi1QAuhfBAg+QCRiNDxAXIAAgEID1AAQECAuCHCOQA8A/AjBjQAVA8AKA1QgDAAALAsQANAwABBvQAAB8gVA+QgDALACAmQACArgBAGQgIA6gMAvQgTBGgkBUQgDAHgEAbIgGAkQgIB1hLBUQhMBXhvAKIjPAEQhRgJglgFgAltGIQACAFAAAMQAAAYgTALIgNAHIAbA6QAUAsAdAdQBLBKBIAcQAYAJAbAIQCDAkDfgBQBmAABLgvQBlhAASiFIAAgCQgfAFgoAKIgBAAQgsALggAGQhNAMhWgDQg4gChagOIhAgXQAAgdA0AHQCUAYCdgYQAwgHAjgJQAjgJAugSIAAAAIADgCQAGgHAJgSIAohaQAWgyAIgwQAHgqAAg5QAAgOACgJQgCgHAAgGQAKhOAFhPQAKiUgUhKIgCgJQgOgygZg9Qg0h4g9g2QhyhkjJgtIj+AAQjbgViFCGQg6A5gqBXQgwBigVB/QgPBWAABHQAAAKAmDZQANABARAFQAkAKAXAQQAbATAAARIAAAAQAAANgJAFQgGACgLAAQgZAAgUgLQgcgPgfgHQgGAHgKAIQgbATAAAlIgBATQAAAKADAFQAjBCA2AdQAlAUAnAAQAMAAAhgLQAigMALAAQAUAAAHAPgACyFPQgMgLgBgQIACgUQAAgQAKgVQALgZAQACQAQACAAAQIAAAqIA2AZICKAAQASgGAFgPQADgKgBgcQAAgrAZAPQAZAOAAAnQAAAmgQASQgfAhhgAAQiEAAgighg");
	this.shape_5.setTransform(69.4,79.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EAD0B7").s().p("AkGJKQgPhcgZiDQgZiEgTicQgHj5gNhsQgWiyhwiWIABgBQATgPAcAIQAhAJAnAnQBSBVEhBLQEfBMBJgRQBJgSAogQQADgBAVgeQAUBKgKCTQgFBQgKBPQAAAHACAGQgCAJAAAOQAAA4gHAqQgIAvgWAzIgoBaQgJASgGAGIgDADIAAAAQguARgjAJQgjAKgwAHQidAXiSgYQg0gHAAAeIBAAXQBYAOA4ACQBWADBNgNQAggFAsgLIABAAQAogLAfgEIAAACQgSCFhlA/QhLAvhmABIgMAAQjVAAiBgkgAAkCIQgKAVAAARIgCAUQABAPAMALQAiAhCEAAQBgAAAfghQAQgRAAgnQAAgmgZgPQgZgOAAAqQABAdgDAJQgFAPgSAHIiKAAIg2gZIAAgqQAAgRgQgCIgCAAQgOAAgLAXgAhiALQADADAAABIALACQAPABAOgBQAUgCANgLQAMgJABgOIAvgJIAAAAQAKAEEGgJIAhADQABAVADADQAMAOAtACIAcgGQADgFABgXIAOAAIAVAAQAMgDAAgQQAAgQgWgIQgWgIgzgCQgkgCgyADIiTAFQhKABAAgBIgrAEQjXAQgsAIQgdAFgIAIQgEAFABAOQABAMACgBQABAAAAAAQABAAAAAAQAAAAABAAQAAAAAAAAIAHAAQALACAOgBIB6gIQAAAQADACg");
	this.shape_6.setTransform(83.5,90.6);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,138.9,158.3);


(lib.JaneHead = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A4D30").s().p("ACQHdQhsgsh6g9IALgaQATgugBgWQAAgDgYgWQgcAPgHAkQgHAlgNAHQjEh8gBgiQAAgOAQgZQAQgYAAgHQAAgMgEgDIgDgGQgWAGgPAHQgDgYAAguQAAhTAkhpQAoh5BJhjQBThwBqhAQApBXAkBPQBdDVAVB1QhQASABBMQAAAMACACIADAEQAQACATgCIApgIIAKgCQASB8AeBkQAjBvBJCYQhhgWhsgsg");
	this.shape.setTransform(38.2,62.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC0066").s().p("AgmAMIg6geQBFgNB8AfIgcAMQgdALgbAAQgaAAgZgLg");
	this.shape_1.setTransform(89.7,104.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ADeFPQjaAAjehuQhQgng7gqQgcgVgMgMQgLALgMAAQhPAAhfhhQgighgaglQgXgiAAgKQAAgtAFgJQAHgNAYgLQAPgHAWgGIADAFQADAEAAAMQAAAHgPAYQgQAZAAAOQAAAiDFB6QANgHAHglQAHgiAcgPQAYAWAAADQAAAUgSAuIgMAZQB9A+BsAsQBrAsBgAWQBaAUBSACQBuACBchSQAXgUAugxQAngsAWgQQAFgEAEgBQAFgBADADQAEACABAEIADAFQADAEAHgFQAIgGgkA3QgjA3haBKQhaBLh/AAIAAAAgABbCpQg9gqAAgTQAAguBvACQApABDOAZQAYADAAAZQAAASgvAkQg8AthHAAQhHAAhIgwgAB5B2IA5AgQA0AVA5gVIAcgOQhYgWg8AAQgaAAgUAEgADWATQg8gJgSgUQgJgKAAgXQAAg0AmAYQAqAlALAEQA9APA9gPIAAgSIAIgFQAIgFAMAAQANAAAGACIADACIADAFQADAEAAAMQAAAOgQARIgYAVQggAFgkAAQgjAAgngFgAIPi3QgPgBgSgJQgWgLgPgRQgOgPgJgEQgQgHgdAEQgMADgLAEQgHgCgEgEIgDgFIgCgEIgCgDIADgFQAagpBpACQBhACA4AeQAVALAMARQALAPgBAIIgBAHIAAABIgBAAQgLgGgQgDIgOgBIgIgBQgJABgHADIgOAFQgGACgPALQgMAJgKACQgLADgKAAIgGgBgAhDjeQgIgEgaADIgKABIgpAIQgUACgPgCIgDgDQgDgDAAgLQAAhOBPgSQAigJAvADQBwAGCYA5QANAFgPAKQgPAJgZADIgbAAIggAAQgdAiguAFQgRACgQAAQgxAAgogUgABFj/IAEgBIgEAAIAAABg");
	this.shape_2.setTransform(67.8,90.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhMFzQAbgkAKgTIARghQAWg8ARhAQAKgoACg3QACgbAAhGQgBg8ABgdQAAgLgVhsQgThtAAgLQAAgUAIgBQADgBAEAIQACgFAIALQAFAKATBiQARBXAGApIAIA1QAHA9gBAjQgEBpgWBMQgPAygOAjQgUAwgcAwQgDAFgIAAQgNAAgagMg");
	this.shape_3.setTransform(128.5,63.4);

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#975F3C").s().p("AlvDGQhJivgai3QAAiMAzCJQAVA7A7DNQAPAfAYBHQAYBHAQAjQAOAfASAaIgShuQgOhYgehDQgnhZgpiFQgniAAAghQAAgFACgEQgNgDgGgEQgOgKAAgbQAAgHAKgGIAAgHQAAgJAEgIIg2gDIgZgYQgGgGAAABQAAgkCaAFQAfABAkgVQAKgGA0glQBUg8BIANQAoAHCFAQQBXAKA0ATQAxASBNAVQA2ASAEATQAcAYAdCtQAOBWAJBUQAAAXgKA8QgKA+AAAVQAAAdgXA+QgeBPgxBDQiPDDjqAAQjnAAiRlegABaHjIAOAAIANgDIgbADg");
	this.shape_4.setTransform(84.4,66.1);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,8.1,136.3,116);


(lib.JaneClothes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7A4D30").s().p("AFfIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAAGGiIgOgcQAzAMA1AJIAmB3QgjAGgdAMQgghIggg6gAl6ATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUg");
	this.shape_1.setTransform(47.9,96.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#975F3C").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAIVirQACgCgKgQQgLgPgEAAIgBABIAAgBIgDgHQgCgFgFgXQgGgZgHgSIgPglQgFgMAAgDQAIgjACgVQADgegDgbQgHhDgGgWIAWgJQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APQgiAJgFACQgFABgEACQgohzghg7g");
	this.shape_2.setTransform(82.3,95.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4B9FB5").s().p("AD+E6QhMgKiBgOQhigMhVgRQAGgJAGgSQAOgnAIg6IAOhSQAFgaAAgdIAAgKIAAgBIgBgJIAAgHQAGgDAHgBQAFAAAJAMQAIALAEAJQADAHAIAvQAPA2AlAsQAnAsA4AWQAgAMA+AMQA3AKAVAKQAhASAIAoQgqgDgegEgAjxheIg5gUQgZgJgCgJQgDgbAmgsQAfgjAkgZQAtggBKgZQgVAQgjAvQgmAzgVAuQgGAPgFAgQgDATgHAAIgBAAg");
	this.shape_3.setTransform(47.8,47.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#58BAD3").s().p("AATGLQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgDgTgHgFQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgJgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6QArA5AFA/QAGA6gdArQgOAWgPANQAAALACACIACAEQADADATAAQAGAAAIgEIANgGQAAANAFALIALAdQAKAgAOAYIheAUQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_4.setTransform(78.6,39.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#7A7A7A").s().p("AAcGqQgzgEgIgDIAJgYQAFgPAQgJIAMgGIAGgCIAQgDIgBAHQABAgBiAZIAaAGQgdABgjAAQgQAAgxgFgAFHGQQABgNgEgNQAGAAAFgHQAIgKAFgEQAKgHAPgFQALAtANAPQAIAKATAIIhHAGQgUABgHABQACgKgBgRgAkHAwIiSgzIgOgDQAPgYAmhQIALgWQAVACAZABQAqACAigOIAJgGQALgFAQgOQAYgVAGgTQAFgWgQgxQgLgigJgPQgTgkgKgUQAQgfAGgRQApAZB0AQIAEABIgiDXQgeDIgCBEQhJgUhMgbg");
	this.shape_5.setTransform(53.4,120);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#8E8E8E").s().p("AkTGWQhjgZAAggIABgHQA6gNCMABQAUACAQAQQAPAPAAARIgFAFQgIAGgPAGQgjALg+AEIgagGgAgKGAQgNgPgKguQAcgIAwAAQBIAAApASQAjARAJAaQADAIghAEQg2AGhkAIQgRgIgJgKgAoKBOQABhFAejHIAjjXQA+AIC8ASQBZAHCugFQC8gGBYgQQAdgGATgGIBdEVQAnB0AKASQgRAJgQAFIgQADQhtAiifAmQgXAFiOAQQiZARgwAAQitABi9gyg");
	this.shape_6.setTransform(94.2,121.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgSgcgcg3QgzhuAAACIAAgFQigguichVIAAABQgPgEgFgHIgDgCIABgEIgBgPQAAgWAxhoQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQAHAGADASIABAHIABAJIAAACIAAAKQAAAegFAaIgOBSQgJA6gNAnQgHASgFAJQBUARBlAMQCBAOBLALQAfADAoADQBFAEBjgBQCkgCBAgOIBegUQgOgYgKgfIgLgeQgFgLAAgMIgNAGQgIADgGAAQgTAAgDgDIgCgEQgCgCAAgLQAPgNAOgWQAdgrgGg6QgFg/grg7Qgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgIABIAEAKQAxCcAAA7IAAAKQgGAMgFAEQgFAEgGAAIj0BHIgrANQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgABxIEQgqACgpABQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMQh0Aah0AKgAkuH/QAgA7AgBIQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJQhkAAhigPQg1gJg1gMIAQAbgApxDWQgmBQgQAYIAPADICSA1QBMAbBJAUQC9AyCtAAQAvAACagRQCPgQAWgFQCfgmBtgiIARgEQAPgFARgJQgKgTgmh0IhekTQgTAGgdAFQhXARi8AGQixAFhXgIQi8gRg+gJIgEgBQh1gQgogZQgGARgQAfQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgiAOgrgCQgYgBgVgCIgLAWgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAIvhnQAKAPgCADQAhA6AoB0QAEgDAFgCQAFgDAigIQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJQAGAXAHBCQADAcgDAeQgCAVgIAjQAAACAFAMIAPAmQAHASAGAYQAFAYACAEIADAHIAAABIABgBQAEAAALAQgAnWjiQgYA3gEAsQALgNAIgEIgCgEQgBgDAAgNQAAgIAKgkIAIggIgGAOgAF1rgIgDgFIADAFgAj5sUQAJgHAMgHQgMAHgJAHg");
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,169.4);


(lib.InstBox = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("Egz4gUVMBnwAAAQCOAAAACOMAAAAkPQAACOiOAAMhnwAAAQiNAAAAiOIAAiGIAA/LIAAi+QAAiOCNAAg");
	this.shape.setTransform(380.3,164.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EDED5E").s().p("Egz4AUWQiNAAAAiOIAAiGIAA/LIAAi+QAAiOCNAAMBnwAAAQCOAAAACOMAAAAkPQAACOiOAAg");
	this.shape_1.setTransform(380.3,164.2);

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("Egz4AUWQiNAAAAiOIAAiGIAA/LIAAi+QAAiOCNAAMBnwAAAQCOAAAACOMAAAAkPQAACOiOAAg");
	this.shape_2.setTransform(389.3,173.2);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(32,32,703.6,271.5);


(lib.HS_Bubble = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EgpmgUCMBNdAAAQCOAAAACNIAAZOQAACNiOAAIlYAAINVKdI38qdMg9eAAAQiNAAAAiNIAA5OQAAiNCNAAg");
	this.shape.setTransform(116.4,39.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("AT3JnMg9cAAAQiOgBAAiNIAA5NQAAiOCOAAMBNdAAAQCNAAABCOIAAZNQgBCNiNABIlZAAINVKcg");
	this.shape_1.setTransform(116.4,39.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(2));

	// Layer 5
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AUPKEMg9hAAAQiNAAAAiNIAA5OQAAiNCNAAMBNdAAAQCOAAAACNIAAZOQAACNiOAAIlQAAIMlJhg");
	this.shape_2.setTransform(121.9,45.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-166.1,-90.8,566.5,261.3);


(lib.HInfront = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#352011").s().p("ApUKcQhfgJg4g9Qgxg2gBhLQgBhdA0hEQAggrAEgHQAQgcABghQABglgcg6Qgcg7AAghQAAgeA2g/QA5hEAHgoQAHgmAAg4QAAg8AEgkQARh/B3hyQAbgZAdgXQAWAyAiA8QATAiAsBGQAdAuAAAHIAFA/QAEAaAIAMQASAWBaAAQAvAABagNIBPgNIAvAMQAxAMAbACQCNAKBngnQAggMAZgQIATgNQAQAAAaAJQgPAGgLAXQgRAigGAIIgXAZQgTATgWAfQgbAnhQAkQhCAdhngJQgJgBgfgXIghgYQAbgTAZgaQAYgZAAgIQAAgQgEgFQgFgGAAgBQgagCgdAXQgRANgoAmQhXBMiKABQgOAAgpgVQgpgUgOAAIgIAAIgDAAQgQAAAAAEQAAABAAABQAAABAAAAQAAABAAAAQgBAAAAAAIgDAOIABADIgBAPQAAAbARApQAQAmAAAVQAAAagMAbIgcAzQgoBIAABYQAAA5AfBCQAgBCAAAuIgBAgQgFAggUAZQg0A+iLAAQggAAglgDgAHgisQAkgmAYgsQAWgoAAgVIAAgVQAhAMAWAAQBQAAAogzQAZggAfhUQAFAXAAAbQAAAqgPAjQgQAngnAqQgzA4hBAeQg2AZgzABIgHAAQgJAAgLgBg");
	this.shape.setTransform(84.8,88.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5B371E").s().p("AACEQQgZgCgxgMIgugMIhSANQhaANgvAAQhaAAgSgXQgIgLgEgaIgEg/QgBgHgdgvQgshGgTgfQgig8gWgyQBthTCYg0QCWgqCUAqQCEA7CbAvQCZArA4ASQBaAfAlAmQAdAcAKAvQggBUgZAgQgoAzhQAAQgWAAghgMIgBgEQgCgFgFACIgGACQgagIgQAAIgTANQgaAPggAMQhTAfhrAAQgaAAgcgBg");
	this.shape_1.setTransform(103.1,32.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AsuKlQgXgogGg1IAAg6QAAg8A1heQA0hdAAgDQAAgcgcg7Qgcg6AAgwQAAgeARgcQAJgOAegfQA8hBAGhDIAJhjQAEgxAAgxQARh7CPh+QAbgYAcgVQB3hZCQgvQCagpCpApQCWA8C0A2QC2A3AjAOQBWAhAhAxQAIAMAHAPQAVAxAABPQAABegmA1QgxBFieBWIhvAAIgagWIgBAAQh1BthyAAQhOABhDgsQgfgUgNgPQh9BDhhAAQgmABgegIQAJAsAEArQAABEgsBKQgtBJAAAtQAAATAiBLQAhBMAABGQAABOgdAqQg5BSimgBQjnAAhJh7gAijrUQiYA0htBTQgdAXgbAZQh3BygRB/QgEAkAAA7QAAA4gHAnQgHAog5BEQg2A9AAAgQAAAgAcA7QAcA7gBAlQgBAhgQAcQgEAHggAqQg0BEABBeQABBKAxA3QA4A9BfAIQDEATBAhNQAUgZAFghIABgfQAAgugghCQgfhCAAg5QAAhYAohIIAcgzQAMgbAAgaQAAgVgQgoQgRgpAAgZIABgQIgBgCIADgOQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgBAAgBQAAgFAQABIADAAIAIAAQAOAAApAUQApAVAOgBQCKgBBXhLQAogmARgNQAdgXAaACQAAABAFAFQAEAGAAAQQAAAHgYAZQgZAbgbASIAhAZQAfAWAJACQBnAJBCgeQBQgjAbgnQAWgfATgTIAXgZQAGgIARgjQALgXAPgFIAHgDQAFgCACAGIABADIAAAWQAAAVgWAoQgYArgkAnQAPACAMgBQAzgCA2gYQBBgeAzg4QAngqAQgoQAPgiAAgqQAAgbgFgYQgJgvgdgeQglgmhagfQg4gSiZgrQibguiGg8QhKgVhJAAQhKAAhLAVg");
	this.shape_2.setTransform(84.5,80.1);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,169,160.2);


(lib.HighlightCircle = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(3,1,1).p("AD4AAQAABmhKBIQhIBKhmAAQhlAAhJhKQhJhIAAhmQAAhlBJhJQBJhJBlAAQBmAABIBJQBKBJAABlg");
	this.shape.setTransform(24.8,24.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,52.6,52.6);


(lib.HeadG = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC0066").s().p("AgmAMIg6geQBFgNB8AfIgcAMQgdALgbAAQgaAAgZgLg");
	this.shape.setTransform(89.7,104.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DDBAA4").s().p("ACQHdQhsgsh6g9IALgaQATgugBgWQAAgDgYgWQgcAPgHAkQgHAlgNAHQjEh8gBgiQAAgOAQgZQAQgYAAgHQAAgMgEgDIgDgGQgWAGgPAHQgDgYAAguQAAhTAkhpQAoh5BJhjQBThwBqhAQApBXAkBPQBdDVAVB1QhQASABBMQAAAMACACIADAEQAQACATgCIApgIIAKgCQASB8AeBkQAjBvBJCYQhhgWhsgsg");
	this.shape_1.setTransform(38.2,62.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AjaDhQhQgng6gqQgcgVgNgMQgLALgLAAQhQAAhehhQgjghgZglQgXgiAAgKQAAgtAEgJQAHgNAZgLQAPgHAWgGIADAFQADAEAAAMQAAAHgQAYQgPAZAAAOQAAAiDFB6QANgHAGglQAHgiAdgPQAYAWAAADQAAAUgTAuIgLAZQB9A+BrAsQBsAsBgAWQBaAUBSACQBuACBchSQAXgUAtgxQAogsAWgQQAFgEAEgBQAEgBAEADQADACACAEIADAFQADAEAAAMQAAAYgfAoQgfAogyAmQh8Beh/AAQjaAAjfhugABcCpQg+gqAAgTQAAguBvACQApABDPAZQAYADAAAZQAAASgwAkQg7AthHAAQhHAAhIgwgAB5B2IA6AgQAzAVA6gVIAcgOQhYgWg8AAQgaAAgVAEgADXATQg8gJgTgUQgIgKAAgXQAAg0AlAYQArAlALAEQA9APA9gPIAAgSIAIgFQAIgFAMAAQAMAAAGACIAEACIADAFQADAEAAAMQAAAOgQARIgYAVQggAFgkAAQgjAAgngFgAIMipQgOgCgSgLQgVgMgOgRQgOgQgIgEQgQgIgdADQgNABgKAEQgHgCgEgEIgGgMQAmhMCVAvQA3ARAsAbQArAbgCAOIgCAHQgKgGgRgEIgOgCIgHgBQgKAAgHADIgPAEQgFACgQAKQgNAIgJACQgKACgIAAIgKgBgAhDjeQgIgEgZADIgLABIgoAIQgUACgPgCIgDgDQgDgDAAgLQgBhOBQgSQAhgJAwADQBvAGCYA5QAOAFgQAKQgPAJgYADIgbAAIggAAQgdAigvAFQgQACgQAAQgxAAgpgUgABFj/IAEgBIgEAAIAAABg");
	this.shape_2.setTransform(67.8,90.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EACFB7").s().p("AAUJSQhQgBhagVQhKiXgihwQgfhkgRh8QAagDAHAEQA1AaBHgIQAvgFAdgiIAgABIAbgBQAWgCAPgKQAQgKgNgFQiXg3hxgGQgwgCghAIQgUh1hejVQgkhPgqhXQCHhQCtAAQBlAABzAlQCBAoBpBKQEJC6AAEsQAACcgyBBQgIAMgrAqQgdAfgVAfQgEABgFADQgWARgoArQgtAxgXAVQhZBPhrAAIgGAAgAiuGjQAAATA+ApQBIAwBFAAQBHAAA7gsQAwgkAAgTQAAgZgYgDQjNgYgogBIgMAAQhkAAAAAsgAhMEJQAAAXAIAKQATAWA6AJQBNALBBgLIAYgXQAQgRAAgOQAAgLgDgEIgDgGIgDgCQgHgCgMAAQgMAAgIAFIgIAFIAAASQg9AQg9gQQgLgDgpglQgLgIgJAAQgRAAAAAjgADlBNQAIAFAOAQQAOARAVAMQASAKAOACQALACARgDQAJgBAOgJQAPgJAFgCIAQgFQAGgCAKAAIAHAAIAOACQARAEALAHIABgIQACgOgqgbQgtgag2gRQiWgugmBKIAGAMQAEAEAHADQALgEAMgCIAPgBQASAAAMAGg");
	this.shape_3.setTransform(88.4,59.5);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,139.6,124.1);


(lib.Handlecopy = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9E8C49").s().p("ArOM0IgBgOQADh+DWieQBRg7CLhWID7iYQE9jECViTQDVjUAAjbQABgsAAgYQAAgogIgeQgVhQhZgvIgOgCQATgEAVAAQBGAAA0B6QApBjAABGQAADpksELQjYC+kgCVQhkA0ibBtQicBthuA6QhaAvgXCIIAAgBg");
	this.shape.setTransform(80,103.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("ArRQDQgUgNgPgPQgkghgOgnQgUg4gBgUIABg9QAAiPDWimQBSg/CJhVQBRgyCrhnQFDjECSiMQDWjPAAjVQAAhIgEgTQgLg9gvgoIhiADQgNAOgFASQgEAOAAAbQAAAqATAuQAUAvAAA+IAAA2QgIAjguAAQgUAAgPgDIgKgDQAAgCgDgEQgDgDAAgKQhFglgjiOQgPg7gIhGIgEg1QAAiOBohFQBhhACEAeQCJAeBbB1QBoCFAADKQAAELjaD3QinC7k4DAQhrBCiUBRIjfB5QjaB6AAA0QAAAwBoAlIA3AUIAHgBQAaAWAAAiQAAAPgfAfQg8AVgxAUQg5AVg0AAQg0AAgvgVgAHps9IAOACQBZAvAVBQQAIAeAAAoQAAAYgBAsQAADbjVDUQiVCTk9DEIj7CYQiLBWhRA7QjWCegDB+IABAOIAAABQABAwAKAcQANAlAZATIACAGQBCAtAxgUIAGAAQAQAAAKgLQAXgCAOgGIACgBQANAAAEgCQAHgDAAgKIgGgGQAAgNhUgjQgmgUgUgkIgIgTQgCgMAAgMQAAhTB2hIQCyhcB6hLQA2ghCshhQB2hDBqhJQEKi2CQjXQB+i+AAifQAAhxg3h7QhRi2icAAQiaAAgzBSQgPAYgFAoIgFAHQgIAOAAAWIABAsIAPBzQAZBxAvAAQAHAAAEgDIADgDQABgIAAgJIgBgDIABgKQAAgRgUg1QgUg1AAg6QAAgjAhguQAmgYBAAAQAcAAAgAFg");
	this.shape_1.setTransform(82.8,104.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCB55E").s().p("Aq+OwIgCgGQgZgTgNgkQgKgdgBgwQAXiIBagvQBug5CchuQCbhsBkg0QEgiVDYi+QEskLAAjpQAAhHgphiQg0h7hGAAQgVAAgTAEQhtgOg1AiQghAtAAAjQAAA7AUA1QAUA1AAARIgBAJIABADQAAAKgBAHIgDAEQgEADgHAAQgvAAgZhxIgPh0IgBgsQAAgVAIgPIAFgHQAFgnAPgZQAzhRCaAAQCcAABRC2QA3B6AABxQAACfh+C+QiQDYkKC2QhqBIh0BDQiuBig2AhQh6BLiyBcQh2BIAABTQAAALACAMIAIAUQAUAjAmAUQBUAjAAANIAGAHQAAAKgHACQgEACgNAAIgCABQgOAGgXADQgKALgQAAIgGAAQgOAFgRAAQgmAAgugfg");
	this.shape_2.setTransform(83.5,105);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,165.7,209.7);


(lib.Handle = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9E8C49").s().p("AqcMDQg6gsgCg1QgFiDCvh4QBRg4CrhMQBigsCwhrQCvhsBFg3QDdi2BoiNQCXjQAckAQANA6AAA5QAACfh+C+QiQDZkKC0QhpBJh1BDQitBhg2AhQh6BLizBcQh1BIAABTQAAAMABAMIAIATQAUAkAmAUQBVAjAAANIAFAGQAAAKgGADQgFABgLABIgCAAIgQAAQg5AAg2gog");
	this.shape.setTransform(86,118.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("ArRQDQhAgrgVg5QgUg4gBgUIABg9QAAiPDWimQBSg/CJhVQBRgyCrhnQFDjECSiMQDWjPAAjVQAAhIgEgTQgLg9gvgoIhiADQgNAOgFASQgEAOAAAbQAAAqATAuQAUAvAAA+IAAA2QgIAjguAAQgUAAgPgDIgKgDQAAgCgDgEQgDgDAAgKQhFglgjiOQgPg7gIhGIgEg1QAAiOBohFQBhhACEAeQCJAeBbB1QAPAUAOAWQBLB6AACrQAAELjaD3QinC7k4DAQhrBCiUBRIjfB5QjaB6AAA0QAAAwBoAlIA3AUIAHgBQAaAWAAAiQAAAPgfAfQg8AVgxAUQg5AVg0AAQg0AAgvgVgAH3s7QBZAvAVBQQAIAeAAAoQAAAYgBAsQAADbjVDUQiVCTk9DEIj7CYQiLBWhRA7QjWCegDB+IABAOQABAwAKAdQANAlAZATIACAGQBCAtAxgUIAGAAQAQAAAKgLQAXgCAOgGIACgBIACAAQALgBAEgBQAHgDAAgKIgGgGQAAgNhUgjQgmgUgUgkIgIgTQgCgMAAgMQAAhTB2hIQCyhcB6hLQA2ghCshhQB2hDBqhJQEKi2CQjXQB+i+AAifQAAg4gOg7QgNg7gcg+QhRi2icAAQiaAAgzBSQgPAYgFAoIgFAHQgIAOAAAWIABAsIAPBzQAZBxAvAAQAHAAAEgDIADgDQABgIAAgJIgBgDIABgKQAAgRgUg1QgUg1AAg6QAAgjAhguQAmgYBAAAQAhAAApAHg");
	this.shape_1.setTransform(82.8,104.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCB55E").s().p("Aq2OwIgDgGQgZgTgMgkQgLgdAAgxIgBgNQACh/DXieQBQg7CLhVID7iYQE9jECWiUQDUjUAAjbQACgsgBgXQAAgpgHgdQgVhQhagwQh3gTg4AlQgiAtAAAjQAAA7AVA1QAUA1AAARIgCAJIACADQAAAKgCAHIgDAEQgDADgIAAQguAAgZhxIgQh0IgBgsQABgVAIgPIAEgHQAFgnAQgZQAyhRCaAAQCcAABSC2QAbA9AOA7QgcEBiXDPQhoCOjdCzQhFA6ivBrQiwBrhiAtQirBMhRA3QivB4AFCDQACA2A6ArQA2ApA5AAIAQgBIgBABQgOAGgYADQgKALgQAAIgFAAQgPAFgRAAQgmAAgtgfg");
	this.shape_2.setTransform(82.8,105);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,165.7,209.7);


(lib.GregHead = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AjSAqQgLgLAAgMQAAgGABgEIg/ABQgSAEgaAIQgTAGgKAAQgLAAgJgBIgGgCIgDgRIAEgKQACgHAGgFQASgLBKgIQA9gQB+ANQAWATABAEQAAADgJAIQgIAFgGAEIgnABQACAFAAAHQAAAVgNAKQgKAHgPAAQgZAAgQgQgAD/AZQgVgEgJgJQgCgCgBgDIgCgEIACgFIgFgBQgKgCgKgEIgIgDIAAgDIgBgBQgBgCACgLQAFggBbAEQAoACA5AJIACAAIABAFQACAEgCALQgCAJg3ALIgVAEIgCAAIAAACQgFAageAAIgPgBg");
	this.shape.setTransform(96.2,91.1);

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D5AE81").s().p("AgRAlQgVgEgIgJQgDgCgBgDIgBgEIABgHIgEgBQgLgCgKgEIgIgBIAAgDIgBgBQgBgCACgLQAFggA3ATQA1ATA3AKIgfAFIgVAEIgCAAIAAACQgFAcgcAAIgPgBg");
	this.shape_1.setTransform(123.6,85.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgKMJQg8gJgsgNQhsgghKg0Qh0hQgjiCIAAgBIgiABQhlAAg5hEQgQgTgTgpQgPgmgBgBQgDgVAAgWQAAgsAFgTQAKgqAggKQgLgegKhqIgIhmQAAjkBXi1QAuhfBAg+QCRiNDxAXIAAgEID1AAQECAuCHCOQA8A/AjBjQAVA8AKA1QgDAAALAsQANAwABBvQAAB8gVA+QgDALACAmQACArgBAGQgIA6gMAvQgTBGgkBUQgDAHgEAbIgGAkQgIB1hLBUQhMBXhvAKIjPAEQhRgJglgFgAltGIQACAFAAAMQAAAYgTALIgNAHIAbA6QAUAsAdAdQBLBKBIAcQAYAJAbAIQCDAkDfgBQBmAABLgvQBlhAASiFIAAgCQgfAFgpAKQgsALggAGQhNAMhWgDQg4gChagOIhAgXQAAgdA0AHQCUAYCdgYQAwgHAjgJQAjgJAugSQAHgDALgYIAohaQAWgyAIgwQAHgqAAg5QAAgOACgJQgCgHAAgGIADgSIAAgBQANhTAEgyQAOiZgfhTQgRg1gghTQgziGiEhMQhZgziSghIj+AAQjbgViFCGQg6A5gqBXQgwBigVB/QgPBWAABHQAAAKAmDZQANABARAFQAkAKAXAQQAbATAAARIAAAAQAAANgJAFQgGACgLAAQgZAAgUgLQgcgPgfgHQgGAHgKAIQgbATAAAlIgBATQAAAKADAFQAjBCA2AdQAlAUAnAAQAMAAAhgLQAigMALAAQAUAAAHAPg");
	this.shape_2.setTransform(69.4,79.1);

	// Layer 3
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AiTAnQgLgLgBgPIABgSQAAgRAKgVQALgYAQABQAQACABARIgBAqIA2AXICIAAQASgHAFgNQADgJAAgdQgBgqAZAOQAZAPAAAmQAAAlgPARQggAhhgAAQiCAAgighg");
	this.shape_3.setTransform(102.1,108.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#725942").s().p("ABDGpQgngFgjgXIAAgBQAAgQgbgTQgXgQgkgLQgRgFgNAAQgnjbAAgKQAAhIAPhUQAWh+AwhjQAqhWA4g6QASAYA3CZQA7CiAKASIgBABIgFAGQgQAUgNApIgaBPQgQAsgCBHQgBBcAWB0QAFAYgeAAIgNgBg");
	this.shape_4.setTransform(22.6,58.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#917154").s().p("Am2A4Qg3iXgSgYQCFiGDbAVID+AAQCSAhBZAzQCEBMAzCEQhaBgj5ArQjzAqjcgmQgugHgiAnQgKgRg7iig");
	this.shape_5.setTransform(76,28.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BC9A72").s().p("ADcHhQiRg1hAh/QgehAgJhQIgXAIQghALgMAAQgoAAglgUQg1gdgkhCQgCgFAAgKIABgTQAAgjAagTQAKgIAHgHQAfAHAbAPQAUALAZAAQALAAAGgCQAJgFAAgNQAmAYAnAEQAqADgGgaQgWhzABhcQAChHAPgsIAahRQAOgpAQgVIAEgFQAoBnAZB4QAWBpANCGQAKBcAaCQQACABgCAAIAGAjIADANQARBdAOBSIAJAxIgBAAIADASQgXgGgWgIg");
	this.shape_6.setTransform(30.9,101.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#D5AE81").s().p("AgoJGIAAAAQgZAEgXAAQhyABhYgYIgDgTIABAAIgIgwQgOhTgShcIgCgNIgHgkQACABgCgBQgaiRgJhdQgOiEgWhqQgZh3gnhoIABgBQAigoAuAIQDdAmDygqQD5grBahgQAgBTARA1QAfBTgOCZQgHBNgNBMQAAAHACAGQgCAKAAANQAAA4gHAqQgIAwgWAyIgoBaIgJARIAQAKQgFAWgDACIgBABIAAAAQAAAXgCgDIgFAGQgFAFgKAAIABACIADAGIACgBIAAACIgBAKIAAABIAAAMQAAAagBAEQgCAGgEAFQAHAKAAAIQAAARgYAdQgZAhgnAdQhkBPhnAAQg9gBgtgWgAiFFJIApAMIAaAAIgRgOQgFgFAAgBIAAAAQgVAFgYADgAESEvQACABAAANQAAAGgSAOIAagMQASgHAOAAIAFAAIgBgBQgTgLgegGQAAABAAAAQABABAAAAQAAAAABAAQAAABABAAgAAGBeQgIAWAAAQIgCAUQABAPAKAMQAiAgCEAAQBgAAAfggQAQgSAAgnQAAgmgZgOQgZgPAAArQABAcgDAKQgFAOgSAHIiKAAIg2gZIAAgqQAAgRgQgBIgCgBQgOAAgLAXg");
	this.shape_7.setTransform(86.5,94.8);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,138.9,158.3);


(lib.GotIt = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("got it", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 158;
	this.text.setTransform(94.2,15.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AsvkbIZfAAQCOAAAACNIAAEdQAACNiOAAI5fAAQiOAAAAiNIAAkdQAAiNCOAAg");
	this.shape.setTransform(95.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("AsvEcQiOAAAAiNIAAkdQAAiNCOAAIZfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_1.setTransform(95.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AsvEcQiOAAAAiNIAAkdQAAiNCOAAIZfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_2.setTransform(104.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,202.2,67.5);


(lib.Got_It = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("got it", "30px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 36;
	this.text.lineWidth = 158;
	this.text.setTransform(94.2,12.7,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AsvkbIZfAAQCOAAAACNIAAEdQAACNiOAAI5fAAQiOAAAAiNIAAkdQAAiNCOAAg");
	this.shape.setTransform(95.8,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7EEA9B").s().p("AsvEcQiOAAAAiNIAAkdQAAiNCOAAIZfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_1.setTransform(95.8,28.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("AsvEcQiOAAAAiNIAAkdQAAiNCOAAIZfAAQCOAAAACNIAAEdQAACNiOAAg");
	this.shape_2.setTransform(104.3,37);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,202.2,67.5);


(lib.GirlOutfit04 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#649191").s().p("AAvDRIg9gTQgHgWgVghQgTgegIggQgYhngGghQgIgvAAgwQAAgNACgWQADgVgBgJIApAUQAaALArAGIAMABQABA2AIAxIAXBzQAKA1AQAxIAfBZQgegFgfgKg");
	this.shape.setTransform(32,99.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E0E0AC").s().p("AgEK+Qg1gEgHgDIAIgYQAGgQAPgIIAOgGIAVgGIAAAHQAAAgBhAZIAaAGQgbABgkABQgRgBgvgEgAEmKjQAAgOgEgLQAGgBAFgGQAKgNADgCQAKgHAPgEQALAsANAQQAJAKATAIIhIAFQgTABgIACQACgKAAgSgACBgPQhfgPhUgJIgkgFQgfgEgcgIIg2gMQAGgJAGgSQAOgnAIg6IAOhSQAFgbAAgeIAAgKIAAgCIgBgJIAAgHQAGgCAHgBQAFgBAJANQAIALAEALQADAHgKAvQgDA2AnAsQAnArAyAXQAeAMA4AMQAxAJATALQAdASAIAnQgygDgWgDgAkympIg5gUQgZgJgCgJQgDgbAmgsQAfgkAkgZQAtgfBKgZQgVAPgjAwQgmAzgVAuQgGAOgFAhQgDATgHAAIgBAAgAE0oPQgLgbgOgPIgHgGIAwAAIAIgEIAeBAQgRAXgOAOQgIgNgPgkgAA8nzQgMgEgNgOIgcgbQgUgWgNgVQgOgWgJgQIgPgfIgHgXQgEgMgHgCIAAAAIAjgMQAVAhAnAqQAJAJAeApQAdAnAgAgQgSANgKAAQgRAAgIgDg");
	this.shape_1.setTransform(42.7,80.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFC4").s().p("AhFK/QhigZAAghIAAgHQA7gMCKAAQAVADAPAPQAQAPgBASQgDAGgQAHQgiAOhHAEgADDKoQgNgQgLgsQAdgJAyAAQBIABApASQAjAQAJAaQADAIghAFQhEAHhWAGQgTgHgJgLgABvgCQADgXABgbIgBhoQADirgKhTQgGgsgNg2IANARIAjAyQALARAMAAQA0AAAyhiQAqhSAAglIgBgRQARAGArAiQAyAmATAcQAdAqAPBKQABAFgUAJIgbAOQgghAglgaQgngcAAAwQAAAFApA6QArA7AGA/QAFA6gcArQgeAhAAACQAAALACACIACAEIAEACQAHACAMgBQAFAAAJgDIAMgGQABASAHAnQAHAqAGAJIgdAIQgWAHgXAFQhKAQiTAAIgaAAgAAygDIgRgCQgIgogbgSQgTgKgxgKQg4gLgegNQg0gWgngsQgngsADg2QAKgvgDgHQgEgLgIgKQgJgNgFAAQgHACgGACQgDgSgHgGQgKgGgQAVIgJAOIgQADIgBgDIgpgoQgLgMgKgFIgjgPQAIACADgVQAFggAGgPQAVgtAmg0QAjgvAVgQQALgEARgLQAQgMAYgIIgBAMQAAAIANAhQAPApAVAkQA/BrBNAAQAeAAA6g1QAPgNAFgQQAFgVACgCQAQA+AIA9QADANAJBhIAAE8IgUgBgAhypBQgegpgJgIQgpgqgVgiIAOgFQA0A8APAMQAiAbATALQAeAQAsAKIAaAAIgfAeQgXAXgSAMQgggfgdgogACkpAQARgKAlgkQAfgdALgQQADgBAHABQgTAdgNApQgJAcgWAkIgNAVg");
	this.shape_2.setTransform(59.5,80.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#76AAAA").s().p("AigDQQgigCg3gJIgfhZQgRgxgKg0IgWh0QgIgxgBg2IBRALQA+AHBLAIQBXAHCDgFQCNgFBYgRIAGgBQgBAGAGArQAEAxgIAfQgfB4ggBUIgZA+IgCAAQgTAFh6AJQh+ALgzgBQguAAgogEg");
	this.shape_3.setTransform(68,102.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AigLqQg8gKgIgLQgHgLgBgWQAAg0A1gjQBIgkAigFQAPgEASgBQAmgDAjADQArAEBCAhIABABQANgVAogMQAOgGARgCQAugKA3AFQAnACAhAJQAzAUAJAEQA/AlgDA0QgBAPgFAGQgFAHgTAFQgOADigAUQhCAHg2gHQgWgFgCgGQgDgIAAgIIgEAGQgVAZhbALQg3AEg1AAQg1AAgwgEgAh8JxIgWAGIgOAHQgPAHgGAQIgIAYQAHADA1AFQAxAEARAAQAkAAAbgCQBHgEAigOQAQgHADgGQABgSgQgPQgPgPgUgDIgLAAQiCAAg5AMgADWJfQgPAFgKAHQgCACgKAMQgGAHgFAAQADAMAAAOQAAARgCALQAJgDASAAIBIgGQBXgGBDgHQAhgFgDgIQgIgagjgQQgqgShIgBQgyAAgdAJgAhpHLIgWgCQgygJg4gMIAAABIgBgBQgYgGgCgFIAEgEQgJgDgPgQQgOgPAAgHQgWgggLgYQgWgrgLhEQgLhHgIg/IgEgyQAAgtANg5IAAgBIABgBQADgOAFgGIgBgJQAJg0gCAFQAIgrAJg+QgEhGACgFIgMgJQgugjgbgKIgSgIQgPgGgLgBQgFAAgIgEIgHgDQgOgMgcgJIAAgEQgJgEgFgGQgEgIgCgNIABgNIAAgBQAIhcCIhPQBJgrAggPQA2gZBCgMIAjgFQABgBABAGIABABIABADQAZAIAuAjIA7AsQAWARAWADICbAAQAYgNAbgoQASgcAWgEQAKgMAgAIQAHABADAGQAFAHAJAFQAaANAaARQAMAEALALIAaAVQBrBRAABoIgBAHIABAFIgGAKIgKAFQgtAfgGACQAHAXAHBCQADAcgDAeQgCAVgIAjQgBADAFAlQAEAkADAKIAMBGQAAAMgGAKIABARQAAA9gZCMQgYCIgQAlIgKAUQgKAUgMAMQhIARhHAIIh1AKIg8ADQhnAAhcgMgAlQgJQgDAUAAANQAAAwAIAuQAGAiAZBoQAHAhAUAeQAUAhAHAVIA/AUQAfAKAfAEQA2AJAjADQAmAEAtAAQAzAACBgKQB6gKATgFIACAAIAZg+QAghTAfh7QAIgegFgxQgFgsABgGIgGABQhYARiOAGQiEAFhYgIQhIgHg+gHIhRgJIgNgCQgtgFgagLIgogUQAAAJgCAVgACWmnQAKBTgCCrIAABoQgBAbgDAXQCnACBQgSQAXgFAWgHIAdgIQgGgJgHgqQgGgngBgSIgNAGQgJADgFAAQgMABgGgCIgFgCIgCgEQgCgCABgLQAAgCAdghQAdgrgGg6QgFg/gsg7Qgpg6AAgFQAAgwAnAcQAlAaAhBAIAbgOQATgJgBgFQgPhKgdgqQgTgcgygmQgrgigQgGIAAARQAAAlgqBSQgyBigzAAQgNAAgLgRIgjgyIgNgRQANA2AGAsgAkamIQAHAGADASIAAAHIABAJIAAACIAAAKQAAAegEAaIgOBSQgJA6gNAnQgHASgGAJIA2ANQAcAHAgAFIAkAEQBVAJBdAQQAWACAzAEIARACIATABIAAk8QgJhhgCgNQgJg9gQg+QgCACgFAVQgFAQgPANQg5A1gfAAQhNAAg/hrQgUgkgQgpQgNghAAgIIABgMQgYAIgQAMQgRALgLAEQhKAagtAfQgkAZgeAjQgnAsADAbQACAJAZAJIA5AUIAjAPQAKAFAMAMIAoAoIABADIAQgDIAJgOQAMgQAJAAQADAAACABgACepDQAPAPALAbQAOAkAIAOQAOgOARgYIANgVQAXgkAJgcQAMgpATgdQgHgBgDABQgKAQggAdQglAkgRAKIgIAFIgwAAgAisrLIgjANIAAAAQAHACAEALIAHAYIAQAeQAIARAOAVQANAVAWAWIAcAbQANAPAMADQAJADAQABQAKAAASgOQAQgMAYgXIAggeIgcAAQgqgKgegQQgTgLgigbQgPgMgzg8g");
	this.shape_4.setTransform(55.2,81.4);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,6.4,110.4,150);


(lib.Girl03_Yard = function() {
	this.initialize();

	// Layer 9
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF91").s().p("AATCdQgBgDAAgEIAHgaQgNAAABgPIACgRQAAgKgBgEQAAABgIAAQgOAAgGgnQgPAEgFAAQgFAAgHgWQgGgUAAgCQAMghAAgGQAAgoAcAXIAMAMQgIgbgQggQgWgnAAgCQAAgkAZAkQAXAlAYBHQAFAYAPAzQAHAYAAAmIAAAiQgEAcgSAAQgIAAgEgGg");
	this.shape.setTransform(111.3,28.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#543226").s().p("AoRCOQgTgLAAgbQAAgJAEgSIAEgWQAKATAEAdQACAWAHANIgLAEgApiCDQgagHAAgcQAAgYAHgSIAFgOQAGAQAGAZQALAdAXANIgIAHQgFADgHAAQgFAAgHgCgAqcBaQgSgUAAgSQgEgeAag/QAKgZAWgoIATgjIAYAIQggA0gMAYQgXAyAAAoQAAALABAJQgHAVAAAWgAJuAzIgLAAQgKg0gJgUQAYARAJARIASAjQgGADgJAAIgGAAgAKSghIgGgLQALAJAOAHQAHAEADAHQgEADgHACIgHACIgLgXg");
	this.shape_1.setTransform(65.4,85.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#774736").s().p("AoiBvQgDgdgLgTIAAgCQAAgigNATIgEAHQgRAfAAAcIABAPIgDACIgBABQgYgNgLgcQgGgZgGgQIACgHQAAgSgGgBQgGAAgIANQgHAMgFAOQgCgJAAgLQAAgpAXgxQAMgZAhgzQBNAWBFgFIAAAFQAAAJASAxQARAwAAATQAAALgMAIQgHAGgMAFIgNAFIgCgXQgHgXgMgLIgEgEQgKgHAAAWIAFAkQAFAfAAAOQAAAlgiAOIgGACQgHgMgDgWgAIhBTQAAgwgEgWIAAiVQAkAFAdgNIAgAtQAnA1AAAXQAAAIgJAGQgDgHgHgEQgOgIgLgIQgWgnAAAtQAAAEAKAeQAJAfAAAHQAAAKgHAEIgRgjQgKgTgYgPQgFgKgFAAQgRAAAAAWQAAAJAIAcQAHAdAAAKQAAAGgCABIgDABg");
	this.shape_2.setTransform(67.4,84.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AoWCzQgLAAgLgFQgMgFgFgJQgMAGgTAAQgYAAgRgHQgTgIgFgPIAAgBIgBAAQgYgCgQgmIgLgmQAAgoAEgRQAFgZAUgsQAVgxAYgcQAOgPAMAAQANgBgHAQIgGAMIgTAjQgVAogKAZQgaA/ADAeQAAASATAUIAFAHQAAgXAIgVQAFgNAHgMQAIgNAGAAQAGAAAAASIgCAIIgFANQgHASAAAYQAAAdAaAGQAPAEAKgFIAIgHIABgBIADgCIgBgQQAAgcARgfIAEgHQANgSAAAhIAAACIgDAWQgEASAAAJQAAAbATALIAAAAIAMgEIAGgCQAigPAAglQAAgOgFgfIgFgjQAAgWAKAGIAEAEQAMAMAHAWIACAYIANgGQAMgEAHgGQAMgIAAgMQAAgSgRgwQgSgyAAgIIAAgGQACg4AdBJQAfBMAAAnQAAASgOASQgTAagjACIAAADQAAAPgFAPIgHASQgHARgUANQgTALgSAAIgCAAgAIJAgIgBhWQgBgqAEgdQAGg6AYAuIAACWQAEAVAAAxIAKAAIADgBQACgCAAgGQAAgKgHgcQgIgcAAgJQAAgXARAAQAFAAAFALQAKARAKA2IAKAAQAOACAHgFQAHgEAAgJQAAgHgJggQgKgeAAgDQAAgtAWAmIAFALIALAYIAHgDQAHgCAFgCQAJgGAAgJQAAgWgng2IgggtQgHgMAAgDQAAgYARAHQAQAGAWAbQA4BDAAA5QAAAZgCAEQgJATglAAIAAAAQADAKAAAGQAAALgOARQgRAWgVAAQgLAAgJgCQgBAFgCADQgLATgoAAQgYAAgGhXg");
	this.shape_3.setTransform(66.2,84.5);

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#826631").s().p("AghCYQgKg1gKhhQgNh3gFgnQAUAGAQAGQAkAGAqACIALB0QgVAUgJAMQAIAIAMAEIAPgCIANCEIgrABQgFABgLAAQgPAAgfgEg");
	this.shape_4.setTransform(42.2,149.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#A88541").s().p("Ah6AaQAWgGAQgQQA6gTBBATQA3AYAVAwQALAcANAyIj4AEgAApgnQhEgRhGARQgFACgWAUIgDACIgLh0QArADAwABQBkABAegNQAGgCAEgGIAHgKQAAAHgFA3QgGA1AFAfQgXgQgegMg");
	this.shape_5.setTransform(60.5,148.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AiwDKQgbgfgSiGQgMhfAAg4IABgtIgBAAQAFgOADgOIARgGIABAAQAkAKAUACQA8AJBMAAQBKAAA7gJQALgMAGgFQAJgGAOgCQAHACAHAAQAGACAAAQIAAABIABACIgCAEIgWB+IgBADQABAAAAABQAAAAgBAAQAAAAgBAAQAAABgCAAIAEAQIAAAbQA6AmAPAuQAGAUAABAQAAATgDAFQgDAFgOAKIkHAEIguABgAi1AJQAKBhAKA2QAzAHANgFIArgBID4gEQgNgzgLgbQgVgwg3gYQhDgTg4ATQgQAQgWAGIgPADQgMgFgIgIQAJgNAVgSIADgDQAWgTAFgCQBEgSBGASQAeAMAXAPQgFgeAGg1QAFg4gBgGIgGAKQgFAGgFACQgeAMhmgBQguAAgrgDQgqgDgmgFQgQgHgUgFQAFAnANB2gAC2iiIABgRIAIAAIABAGQAAAIgEARIgCABQgEgEAAgLg");
	this.shape_6.setTransform(55,148.6);

	// Layer 7
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#826631").s().p("AgfCaQABgNgMh2QgBgMgCgHIABgEQAAgRgFglQgFglABgSIAFgvIAEADQATAQAQAAIAtAHIATEfIhWgDg");
	this.shape_7.setTransform(79.9,148.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#A88541").s().p("Ah1CSIgjgBIgSkgQCAAOBwgUQgJAMgBAcIgBA2QgwgLhEAfQhAAdAAAPQAAAZABgKIABAHQADAGAOgIQCFhOBKAxQA6AoAIBuIg7AAQh5AAhsgEg");
	this.shape_8.setTransform(100.7,149.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AACNCIhmgFQgLgCgEgCQgFgDAAgQIAAgHQgJgMgKhDQgJg6AAgSQAAgRAFgIIACgCQgCggAAg3IABgwQABgWAEgOIgBgCIACgUQATgCApAWQCSAcCJgcQANgNAHgEQAJgGAOgCQAGADAIAAQAGACAAAPQAAAUgWAOIgCACQAEAEAAAOQAABAgFAnIgBAIIAaAPQAbARAVAZQAIAKAYAkQAQApgQAqIADAAQgVAZgHARQizAHgVAAIh7gFgAh7IFQAAASAFAlQAEAlAAASIAAAEQACAHABANQALB2AAANIBXADIAhABQCFAFCdgBQgIhug6goQhKgziHBQQgOAIgDgGIgBgHQgBAKAAgZQAAgRBAgdQBGgfAwALIABg2QABgcAIgMQhxAUh/gOIgugGQgQAAgUgQIgDgDIgGAvgAiEHXIABgIIAAgGIgBgCIgBgCIABASgAkTrjQhMgYgFgYQgEgPAggRQAggRBUgCQBTgBBNATQBLATgJAUQgEAHgEAPQgGANgSALQgZANhaAAQhdgBgxgQg");
	this.shape_9.setTransform(86.8,85.9);

	// Layer 3
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#282828").s().p("AgyD+IAAgBIgCgBIgBAAIgHgGIgDAAQgNAAgGhHIgChJIAAg/QACgIAHgTQACgzAzhsQASgoAKgVQAAgOgCgIQASgSAVgOIAFgDIAWAUIAPATQgXAjgPAhQgGAOgFAgQgCAQgFADIAAACQgDAcgfCLQgIAlgDAyQgBAXgBA1IAEAAIAAAIIAAAKQACAIAKABQgfgBgRgLg");
	this.shape_10.setTransform(12.4,43.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#333333").s().p("AAQFjQACADgcgDIgsgGIg6gHIAAgwQgBhJgEgdIghjOQgFgkgQg4IgdhdQgqhWgogtIAtgRIAUgEIAagHIEuAAQARADAhAHQAfAHAKABQAtBLABC/IAMFRQAEA6AGAkIg/ABIguABQhXAAg6gEgAo+EsIgGAAQgKgCgCgHIAAgKIAAgIIgEgBQABg1ABgXQADgyAIgkQAhiMADgcIAAgCQAFgDACgQQAFggAGgOQAPghAXgjQAeAoARAwQAEANAIBNIACAPIgFAGIAAAEQAAALgGAbIgDALIAAAVQAAAJgEAXIAAABIgEAWIgCAHIgDANIgKAvIAEgJIgJAhQgKAjAAAJIABANIAAAIIAAACQgkAHgcgBIALgCIgRAAQgLADgJAAIgDAAgAH6EZQgGgBABgLQACgMAAgFQgEg9ACgGQAHgjACgVQADgXgBgWIgDgVIgBgPIgBgRIgBgDIgBgBIAAgRQAAgKgDgIIAAgCQAAgMgMg8QgLg6gCgFQgLgwgTgkQAmAaAOAOQAJAIAkBRQBBEegPA3QgVArgsAAQgLAAgMgDg");
	this.shape_11.setTransform(70.2,39.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#022F47").s().p("AhgD/QgWgBgOgEIgigNQgQgHgIgHIgGgFQhjkvgLgdQgWg9gNg/IBzAAIAdgHIAhgJQAGApAIAsQAEAWAQCoQAQCgALAwIAVAagADXD4IhAgKIAAgHQgCgagDiAIgDiLQAQAcAXAYQALAKA6CBQA0BzAnAKIgQAAQg4AAg3gGg");
	this.shape_12.setTransform(59.7,122.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#034E75").s().p("Ak0E/QgMgwgPigQgRiogEgWQgIgsgFgpIBQgUQAwgNAPgQQAOgQABgoIgBhLIA6AGQBfAJCxgGIA6gDQAFAWAIAYIAOAlQAYBAAhARQAYAMBHAAIAPgBQgJBNgXBDQgQAuggBUQgFALgLBoIgOBtIgsADQgwAGggAAQgngKgzhzQg4iCgLgLQgXgYgRgcIgFhWQAEg3gCgYQgnADgFAoQgDAUAFAVIAEBcIADCMIADCGIAAAKQgBAIgVADIiqANg");
	this.shape_13.setTransform(80.3,113.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E5E582").s().p("AnGEpIADgSIAHghQAFgXAGg1IACgPIAHgTQAGgQAEgUQAHgbAFgiIAOhQQAFgaAAgeIAAgKIAAgCIgBgJIgBgHIAAgBIAAgVQAAg1gBgDIgfhVQgHgagFgNQgJgVgRgLIgCgDQgMgOgRgIQAigSAsgPQAuA7AXA4QAcBGASB6QAIA1gEBlIgNDYQgCAlADAuIAFBFIgXAEIh+ACQgJg8AAg9gAlhC4IACAAIgCgBIAAABgAFxGHQgSgWgJglQgIgsgHgZQgRhUgEg6IAAgaQAQgRArgaQAtgcATgZQAMgRAIgXQAFgDAFgMQAGgPACgJIgBgBIAAgBQAHgUAAgJQAAgIgEgCIAAAAIgDghQAAgMAEAGIAAAAQAEAGAAANIADAaIABA1QABAQgCAJIAAACQAAACgEAKQgEAggTAbQgOAWgPANQAAALACADIACADQADAEATAAQAGAAAIgEIANgGIACA5QADAlAEAMIAEA0IgECfIAAAEQhQgJgigDgAHOhzIACAHQgBAKgDAMIACgdg");
	this.shape_14.setTransform(69.4,53.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFF91").s().p("AlNF0QgDguACglIANjYQAEhlgIg1QgSh6gchGQgXg4gug7IABAAIBTgcIAHAIQAdAgATAcQBJBqATCTQAQByANBNQAEAaABA3IABA5IABBaQAAAkgMATQgVAkhbAUIgfAGIgFhFgAEGi7QgEhjgHgsQgMhPgdgfQAAAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAIAEAAQAGAAAWAOQATAJAeAKQAaALAhASIAAAEIANAgIANAhIAFAdIACALIABAUIABAIQgIgFgHgCQgVgEAAAiQAAAFAqA5QAZAjAMAjIgCAdQADgMABgKIABgQIgCgVIgCgUQAAgNAJARIAAABIAAAAQgEgGAAAMIADAhIAAAAQAEACAAAIQAAAJgHAUIAAABIABABQgCAKgGAOQgFAMgFADQgIAVgMARQgTAbgtAcQgrAagQASg");
	this.shape_15.setTransform(72.6,50.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#371E2D").s().p("ABnAOIABgBIACABIgDAAgAAXAAIAAgLIAAgCIADAAIgCANIgBAAgAhpAAQAJAAALgDIARAAIgLACIgVABIgFAAg");
	this.shape_16.setTransform(23.5,70.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("ACQLvQg4gFgPgHIgRgIIgFgCIgBgBIgKAFQgqAVhKAEQgmAFgngGQgXgEhOgUQgZgFgDgDQgfgchGjbQgwiUgUhzIgFgFQgEgEAAgIQAAgIAFgEQgKg+AAg0QAAhBAFgVQACgKADgHIgpAAIgKAAIgTABQgJAAgNgCQgPgDgWgGIgNgEIgFgCQgXgHgKgEIgHABQgJgGgGgfQgEgWAAgNQgEgXACggQAGgsAAgPQAAhcAehjIABgCIAUgpIAOgbIAOghQAHgMAQgYIADgCIAAAAIAXglQACgDAOgKQAPgMAJgFQAngXA1gUQApgRAygOIAhgJIBHgQICGgTIDaAAQAiAFBHAUQBDATACADIABABIAsAQIBDAbQApASAeAWQBjBLARBpIAcCqQAMBHAAAWQABAxgBAHIABALQAAAfgWAWQgMANgTAGQgJAEgMACQgMACgOAAIgMgBIADAbQAFAoAAAOQAABcgCAdQgGBTgZBNQgOArgjC0QgeCZgRAYQABACgDAFQgDAGgGABQAEABgVAGIgrAIQg5AKgzAAQgyAAg6gKgAl1FXQAKAdBkExIAGAFQAIAHAQAHIAiANQAOAEAVABIAOAAICogNQAVgDABgIIAAgKIgDiGIgDiMIgEheQgFgVADgUQAFgoAngDQACAYgEA3IAFBYIAECNQACCAACAaIAAAHIBAAKQBAAHA/gBQAgAAAwgGIAsgDIAOhtQALhoAFgLQAghUAQgwQAXhDAJhNIgPABQhHAAgYgMQghgRgYhAIgOglQgIgYgFgWIg6ADQizAGhdgJIg6gGIABBLQgBAogOAQQgPAQgwANIhQAUIgiAJIgcAHIhzAAQAMA/AXA9gAmVhOIAAANIACAAQgGA2gFAUIgHAhIgDATQAAA9AJA7IB9gCIAXgEIAfgGQBbgUAVgjQAMgUABgkIgChYIAAg4QgCg3gDgaQgNhNgQh1QgUiThIhpQgUgdgcgfIgIgJIhSAcIgBABQgtAPghARQAQAJANAOIACACQAQAMAJAVQAFAMAIAaIAfBVQABADAAA1IAAAVIAAACIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgFAigGAbQgFAUgGAQIgHASIgDAAIAAACgAEPrBQAeAeAMBQQAGAsAEBjIAME5IAAAZQAEA6ARBTQAHAYAJAsQAJAlARAXQAiACBQAKIAAgEIAEigIgEgyQgEgLgCglIgDg6IgMAGQgJAEgFAAQgTAAgEgDIgCgEQgCgCAAgLQAQgNAOgWQASgcAEghQAFgKAAgDIAAgBQACgJgCgRIAAg0IgEgaQAAgNgDgGIgBgCQgJgQAAANIADAUIACAVIgBAPIgCgHQgNgjgZgiQgpg6AAgFQAAghAVAEQAHABAIAFIgBgIIgBgTIgDgMIgFgdIgNggIgNghIABgEQgigSgagKQgdgLgUgIQgWgOgFAAIgFgBIgBAAQAAAAgBAAQAAAAAAAAQAAAAAAAAQAAABAAAAgAD0gIIA/gBQgGglgEg6IgMlTQgBi+gthLQgKgBgfgHQghgIgRgCIkuAAIgaAGIgUAEIgtARQAoAtAqBXIAdBcQAQA5AFAkIAhDQQAEAcABBJIAAAwIA6AHIAqAGQAeAEgCgDQBIAEB3gBgApShSIABABIACABIAAAAQARALAfACIAGABIADAAIAEAAIAWgBQAcABAkgIIAAgCIAAgHIgBgOQAAgIAKgkIAJggIgEAJIAKgwIADgNIACgHIAEgVIAAgCQAEgXAAgJIAAgVIADgKQAGgbAAgLIAAgEIAFgIIgCgPQgIhNgEgOQgRgwgegnIgPgTIgWgVIgFAEQgVAOgSARQACAJAAAOQgMAVgSAnQgzBtgCA1QgHATgCAHIAABAIACBIQAGBIANAAIADgBIAHAGgAp3hFIAAAAIAAgBgAICoCQACAFALA7QAMA8AAAMIAAACQADAIAAALIABARIAAACIACAUIABAPIADAUQABAWgDAYQgCAUgHAjQgCAGAEA+QAAAEgCANQgBAKAGACQA+ANAag2QAPg3hBkgQgkhQgJgJQgOgOglgZQASAjALAwg");
	this.shape_17.setTransform(66.5,76.8);

	// Layer 1
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#748474").s().p("AE4FAIgtgGQhJgKh6gOIgJgBIgkgEIhKgLIAAAAQgXgNgegHIgPgCIAHgTQANgmAJg7IAOhSQAFgaAAgcIAAgKIAAgCIgBgJIgBgHQAHgCAHgBQAFgBAJANQAHAKAFAKQADAGAIAwQAOA1AlAsQBCBKB4AkIBMAXIAMAFQAYANAJARQAAABgGAAIgWgBgAkwD8IgBgBIgCgBIgBAAIgGgGIgEAAQgNAAgFhHIgDhIIABhAQABgHAHgTQACg0AzhsQATgoAMgUQAAgPgCgIQASgSAUgOQAtgfBKgZQgVAPgiAvQgnA0gUAuQgHAOgFAgQgCAQgFADIAAACQgDAcghCMQgHAkgDAyQgCAXAAA1IADABIAAAIIAAAKQADAHAKACQgfgCgRgLg");
	this.shape_18.setTransform(37.8,43.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#A5BCA5").s().p("AAQFjQgJgRgWgNIgMgFIhMgXQh3gkhChKQgngsgPg1QgIgwgDgGQgEgMgIgKQgJgNgFABQgHABgGACQgDgQgHgFQgKgHgQAVIgHAIIAAAEQAAALgGAbIgDALIAAAVQAAAJgEAXIAAABIgEAWIgCAHIgDANIgKAvIAEgJIgJAhQgKAjAAAJIABANIAAAIIAAACQgkAHgcgBIALgCIgRAAQgLADgJAAIgDAAIgGAAQgKgCgCgHIAAgKIAAgIIgEgBQABg1ABgXQADgyAIgkQAhiMADgcIAAgCQAFgDACgQQAFggAGgOQAVguAmg0QAjgvAVgPIB7gqQAmgNAQgHIAUgEIAagHIEuAAQBfAOAYAAQALAAAFACQAsATBZAxQAJAFAaATQBBApAUAUQAJAIAkBRQBBEegPA3QgaA2g+gOQgGgBABgLQACgMAAgFQgEg9ACgGQAHgjACgVQADgXgBgWIgDgVIgBgPIgBgRIgBgDQgCgMgJgeQgJgggLgSQgNgWgTgWQgbghgTgEQgVgEAAAiQAAAFApA5QArA5AGA/QAFA6gcArQgOAWgQANQAAALACADIACADQAEAEATAAQAFAAAJgEIAMgGIADA5QACAlAEAMIABADIgJACIgOAGQgRAGgXAFQhBAOikACIgmABQhLAAg3gEg");
	this.shape_19.setTransform(70.2,39.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#396086").s().p("AAqCaQg0ingRgcQhFh0gciBIAHghIAHglQAeAMBhARIAIABIAFAAIASADQABB6AcChQAFAVAQCpQAPCfAMAwQgigrgxigg");
	this.shape_20.setTransform(36.8,109.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#396087").s().p("AhYFPQgWAAgOgEIgigNQgQgIgIgGIgGgFQhjkwgLgcQgziLAAiQIADgSQAcCBBGB0QARAcA1CnQAyCgAhArIAVAagADfFJIhAgKIAAgIQgCgagDh/IgDiOQAQAdAXAYQALALA6CCQA0ByAnALIgQAAQg4AAg3gGg");
	this.shape_21.setTransform(58.9,114.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#371E2D").s().p("ABhAMIhKgMIAAgLIAAgCIADAAIAPACQAeAHAXALIAAAAIABAAIALAHIgJgCgAhpAAQAJAAALgDIARAAIgLACIgVABIgFAAg");
	this.shape_22.setTransform(23.5,70.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#4B7DAF").s().p("AkwFJQgMgwgPigQgRiogEgWQgdihAAh6QBEAJCjAPQBZAICggFQCtgGBXgRIAXgGIgECgQgHBkgdBVQgQAuggBUQgFALgLBoIgOBtIgsADQgwAGggAAQgngKgzhzQg4iCgLgLQgXgYgRgcIgFhWQAEg3gCgYQgnADgFAoQgDAUAFAVIAEBcIADCMIADCGIAAAKQgBAIgVADIiqANg");
	this.shape_23.setTransform(79.9,112.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("ACQLvQg4gFgPgHIgRgIIgFgCIgBgBIgKAFQgqAVhKAEQgmAFgngGQgXgEhOgUQgZgFgDgDQgfgchGjbQhRj5gBidQAAhBAFgVQACgKADgHIgpAAIgKAAIgTABQgJAAgNgCQgPgDgWgGIgNgEIgFgCQgXgHgKgEIgHABQgJgGgGgfQgEgWAAgNQgEgXACggQAGgsAAgPQAAhcAehjIABgCIAUgpIAOgbIAOghQAHgMAQgYIADgCIAAAAIAXglQACgDAOgKQAPgMAJgFQBwhCCvghICGgTIDaAAQAnAFA3AKQA4ALABACIACABIA2AaQA3AaAYAKQApASAeAWQBjBLARBpIAcCqQAMBHAAAWQABAxgBAHIABALQAAAfgWAWQgMANgTAGQgJAEgMACQgMACgOAAIgMgBIADAbQAFAoAAAOQAABcgCAdQgGBTgZBNQgOArgjC0QgeCZgRAYQABACgDAFQgDAGgGABQAEABgVAGIgrAIQg5AKgzAAQgyAAg6gKgAmeAJIgHAhIgDATQAACQAzCKQAKAdBkExIAGAFQAIAHAQAHIAiANQAOAEAVABIAOAAICogNQAVgDABgIIAAgKIgDiGIgDiMIgEheQgFgVADgUQAFgoAngDQACAYgEA3IAFBYIAECNQACCAACAaIAAAHIBAAKQBAAHA/gBQAgAAAwgGIAsgDIAOhtQALhoAFgLQAghUAQgwQAdhVAHhkIAEigIgXAGQhXARitAGQiiAFhXgIQijgPhEgJIgSgCIgFgBIgIgBQhjgOgegNIgHAjgADdgIQCkgCBBgOQAXgFARgHIAOgFIAJgDIgBgDQgEgLgCglIgDg6IgMAGQgJAEgFAAQgTAAgEgDIgCgEQgCgCAAgLQAQgNAOgWQAcgrgFg6QgGg/grg7Qgpg6AAgFQAAghAVAEQATAEAbAgQATAWANAXQALARAJAhQAJAgACAMIACAUIABAPIADAUQABAWgDAYQgCAUgHAjQgCAGAEA+QAAAEgCANQgBAKAGACQA+ANAag2QAPg3hBkgQgkhQgJgJQgUgUhBgpQgagSgJgFQhZgxgsgTQgFgCgLAAQgYAAhfgOIkuAAIgaAGIgUAEQgQAHgmANIh7AqQhKAZgtAgQgVAOgSARQACAJAAAOQgMAVgSAnQgzBtgCA1QgHATgCAHIAABAIACBIQAGBIANAAIADgBIAHAGIABABIACABIAAAAQARALAfACIAGABIADAAIAEAAIAWgBQAcABAkgIIAAgCIAAgHIgBgOQAAgIAKgkIAJggIgEAJIAKgwIADgNIACgHIAEgVIAAgCQAEgXAAgJIAAgVIADgKQAGgbAAgLIAAgEIAHgLQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAnIgHASIgDAAIAAACIAAANIBKANIAJACIgLgHIAAgBIBLALIAkAFIAJABQB7AOBJAJIAqAGQAeAEgCgDQBFAEBjgBgAp3hFIAAAAIAAgBg");
	this.shape_24.setTransform(66.5,76.8);

	this.addChild(this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-6,0.7,144.5,169.2);


(lib.Girl02copy = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#AF2B57").s().p("AAWAeQglgFgxABQgWAAgHgYIgFgeQgEgQBlAcQBkAaAAALIgBAGIgCADIgCACQgEACgIAAQgTAAgpgEg");
	this.shape.setTransform(72.3,3.1);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC3366").s().p("AEVBMIAAgKQAAgagbgdQggggg1gPIgEgCIg6gUIgygRIgMgEQBwAMBFAVQAgAKAdAQIATALQASAJAAAJQAFAJgGAOQgCACgDAQIgEATQgCAHgGAHIgIACIgEABQgHgEgGgGgAhUgJQgFgBgDgDQgDACgPgEIgzgMIgRgCIgTAAIgEgBIgBAAQgqgEgogJQglgJAAgDQAAgQBBgJQBBgKAnAMIABAAIAIAAQAcABASAHQATAIALAQQAKAJAJAWIADAMIABAHIgKACIgegPg");
	this.shape_1.setTransform(62,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5A986F").s().p("AgQgGIABAAQAVAGALAHQgRgHgQgGg");
	this.shape_2.setTransform(88.4,-0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhODQIgHgRQAAgMAKgFQgEgJgHgqQgIgwgEgQIgZhlIgFAAQgJAAgSgNIgngdQg5gmg0ALIgTgPQAAgRAaAAIABAAIAEAAIATABIARACIA0ALQAPAEACgBQAEACAFABIAeARIAKAGIABABIgBgCIgBgBIAAgBIAAgEIgBgJIgCgNQgJgVgLgJQgLgRgTgIQgRgHgdAAIgHAAIAGgGQAFgDAFgBQAFgIAUgBIAUACIgCgDQAAgBAAgBQAAAAAAAAQAAgBAAAAQAAgBABAAQAZgPBqAJQBzAJBaAkQASAHARAIIAFADQBGAjAAAkQAAAagHAYQgGAXgMALQAVAXACAEQAMAQAIAYQAaBHACAcQAPACAAAXQAAAOgCADQgGALgaAAIgVgCIgJgBIgCgMQAIgNALgKQgHgKgTg5QgYhIgPgjQgJAAgFgBIgDgBQAAgBAAAAQAAAAgBgBQAAAAAAAAQgBAAAAAAQgCgBAAgIIAKgRIAJgCQAFgIACgHIAEgSQAEgQABgDQAGgOgEgIQgBgLgSgKIgTgLQgcgPgggKQhGgWhtgMIALAFIAwAQIA6AUIAEACQA2APAgAjQAaAcAAAaIAAALIgCAVIgDACQgEACgKAAQgOAAgLghIgJgcIgPABQg6AMgdAAQhCAAgpgTQAFAJABAIIAoAbQAuAhAAAAQAAAIgJAEQgFADgIAAIg9giQAEATAAAdQACACAMA3QAIAfAFBDQANAIAAAIQAAAJgPAJQgMAIgGAAQgKAAgJgSgAg7huQATAHAOABQgNgHgKgDIgKACgAiRjGIABAAIABgCIgMgBIAKADg");
	this.shape_3.setTransform(71.4,16.9);

	// Layer 4
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAiBSQhVAAgtgZQhGgngFgjQgEgYAqgUQAqgUBTgBQBQAAA2AVQA2AVgJAgQgDAJgEAXQgGAVgSARQgWAVhQAAIgEgBg");
	this.shape_4.setTransform(77.8,11.5);

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#4E2A3F").ss(1,1,1).p("AD1AKQgZgFgegEQhSgKg4gGQhogLgvALQgTAFg1AHQgkADgQAJQgMAGgJAH");
	this.shape_5.setTransform(69.8,2.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#AF2B57").s().p("AELE6QhLgKiBgOQg7gHgzgJIAAAAQgXgNgegGIgPgDIAHgSQANgnAJg6IAOhSQAFgaAAgdIAAgKIAAgBIgBgJIgBgHQAHgDAHgBQAFAAAJAMQAHALAFAJQADAHAIAvQAOA2AlAsQAoAsA3AWQAgAMA+AMQA4AKAVAKQAhASAIAoQgqgDgfgEgAkwD8IgBAAIgCgBIgBgBIgGgGIgEABQgNAAgFhIIgDhIIABhAQABgHAHgTQACgzAzhtQATgnAMgVQAAgOgCgJQASgRAUgOQAtggBKgZQgVAQgiAvQgnAzgUAuQgHAPgFAgQgCAPgFAEIAAACQgDAcghCLQgHAlgDAxQgCAYAAA1IADAAIAAAIIAAAKQADAIAKABQgfgCgRgLg");
	this.shape_6.setTransform(37.8,43.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CC3366").s().p("AARF3QgHgngfgSQgWgLg3gKQg+gLgggNQg3gWgogsQgngsgPg1QgHgwgEgGQgEgMgHgKQgJgNgGABQgGABgHACQgDgSgHgFQgKgFgPATIgIAKIAAAEQAAALgGAbIgDALIAAAVQAAAJgDAXIAAABIgFAWIgBAHIgDANIgLAvIAEgJIgIAhQgLAjABAJIABANIgBAIIAAACQgkAHgcgBIALgCIgRAAQgLADgJAAIgDAAIgFAAQgLgCgCgHIAAgKIAAgIIgEgBQABg1ABgXQAEgyAHgkQAhiMADgcIAAgCQAFgDACgQQAFggAHgOQAUguAmg0QAjgvAVgPIB8gqQA5gUAHgGQADgCgBgFIgDgGQAQgJAlgFQA0gHAUgFQAvgLBnALQA5AGBRAMQAeAEAZAFQgCAEAAAFQAAATABgKQAbAbBwAuIAkAYQBAApAVAUQANANAVBAIAEAMIACAFIgBAPIABAFIgBAAIAhC+IAMBLQAGAjAFAQQgbA2g9gOQgHgBABgLQADgMgBgFQgDg9ABgGQAHgjACgVQADgXgBgWIgDgVIgBgPIgBgRIgBgDQgGgOgJgXQgPgrgBgMQgNgWgSgWQgbghgUgEQgVgEAAAiQAAAFAqA5QAqA5AGA/QAFA6gcArQgOAWgQANQABALACADIACADQADAEATAAQAGAAAIgEIAMgGIADA5QACArAFAJQgHABgQAHQgRAGgXAFQhBAOijACIgnABQhKAAg4gEg");
	this.shape_7.setTransform(70.1,37.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#396086").s().p("AgbAuQhFhxgciCIAHghIAHglQAeANBhAQIAIABIAFABIASACQABB6AcCfQARBfAfB+Qhbh2g9hog");
	this.shape_8.setTransform(36.8,100.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#396087").s().p("AhYD2QgygBgugRQgogTgDAAQgFgRgJgYQgchHgQgrQhAisAAhtIADgSQAcCBBGB0QA/BmBaB2IAVAaIgEAAIgFAAIgFAAgADfDvIhAgKIAAgHIgIh1QAQAdAXAYQA7A7BlAcIgQABQg4AAg3gHg");
	this.shape_9.setTransform(58.9,105.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#371E2D").s().p("ABhAMIhKgMIAAgLIAAgCIADAAIAPACQAeAHAXALIAAAAIABAAIALAHIgJgCgAhpAAQAJAAALgDIARAAIgLACIgVABIgFAAg");
	this.shape_10.setTransform(23.5,70.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#4B7DAF").s().p("AkwDvQgfh9gRhfQgdigAAh6QBEAJCjAPQBZAICggFQCtgGBXgQIAXgHIgECgQgHBkgdBUQgQAvggBUIgRAuIggABIgMACQguAFgvABQhlgcg4g7QgXgZgRgcIgFhYQAEg1gCgYQgnADgFAmQgDAVAFAVIAEBdIAGBgIAAAKQgBAIgVADQgoAChCAFIhAAFIgUgag");
	this.shape_11.setTransform(79.9,103.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("ACQKNQg5gFgOgHIgRgIIgFgCIgBgBIgKAFQgrAVhJAEQhOAKiLgjQgZgFgDgDQgbgZg0iaQg7itgGh7QgDg2AIghQACgKADgIIgpABIgKAAIgTAAQgJAAgNgCQgUgDgegKQgbgIgLgFIgHABQgJgFgGgdQgEgXAAgNQgEgXACgfQAGgsAAgQQAAhbAehkIABgCIAUgpIAOgbIAOghQAHgMAQgYIADgCIAAAAIAXglQACgCAOgLQAPgLAJgFQB/hLCogZQABAAACAFIABACIADAGQABAFgDACQgIAGg5AUIh7AqQhKAZgtAfQgVAOgSASQACAIAAAPQgMAUgSAoQgzBsgCA2QgHATgCAHIAABAIACBIQAGBHANAAIADAAIAHAGIABAAIACABIAAAAQARAKAfACIAGAAIADAAIAEAAIAWgBQAcABAkgHIAAgCIAAgGIgBgNQAAgJAKgjIAJghIgEAJIAKgvIADgNIACgHIAEgWIAAgBQAEgXAAgJIAAgVIADgLQAGgbAAgLIAAgEIAHgKQAQgVAKAHQAHAFADASIAAAHIABAJIAAACIAAAKQAAAegFAaIgOBSQgIA7gOAmIgHASIgDAAIAAABIAAANIBKAMIAJACIgLgHIAAAAQA1AIA6AHQCBAPBMAKQAcAEAqACQBFAEBjgBQCkgCBBgOQAXgFARgGQAQgHAHgBQgFgJgCgpIgDg5IgMAGQgJAEgFAAQgTAAgEgEIgCgDQgCgDAAgLQAQgNAOgWQAcgrgFg6QgGg/grg7Qgpg5AAgFQAAgiAVAEQATAEAbAhQATAWANAWQAAAMAQAtQAIAXAHAOIACAUIABAPIADAVQABAWgDAXQgCAVgHAjQgCAGAEA9QAAAFgCAMQgBALAGABQA+AMAag0QgEgQgGgjIgMhLIghjAIABAAIgBgFIABgPIgCgFIgEgMQgWhAgNgNQgUgUhBgpIgjgYQhwgugbgbQgBAKAAgTQAAgFACgEQAFgHAUAJIAyAZIBTAmQApARAeAXQBuBTAABhIAAAEQAHA1AWB9QASBnAAATIgBAPIABALQAAAegWAUQgbAbgzAAIgMAAIADAbQAFAqAAANQAABdgCAcQgGBTgZBOIgqCFQgVA8gUAcQABACgDAFQgDAGgGABQAEABgVAGQgVAFgWADQg5AJg5AAQg5AAg6gJgAmeBaIgHAhIgDASQAABtBACuQAPArAcBHQAKAYAFARQADAAAoATQAuARAxABIAKAAIAEAAIBAgGQBCgFAmgCQAVgDABgIIAAgKIgGhgIgEhdQgFgVADgVQAFgoAngDQACAYgEA4IAFBXIAIB1IAAAHIBAAKQBAAHA/gBQAvgBAugFIAMgBIAggCIARgtQAghUAQgwQAdhVAHhlIAEifIgXAGQhXAQitAGQiiAFhXgIQijgPhEgJIgSgCIgFAAIgIgBQhjgRgegMIgHAlgAp3AJIAAAAIAAAAgAjTqBQAJgHAMgGQgMAGgJAHg");
	this.shape_12.setTransform(66.5,68.7);

	// Layer 5
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#CC3366").s().p("Ak6CBQgZgOgEgXQgCgEAAggQAAhDA9g+QBOhQCRgPIDfACIBiAfQBWAcAAALQAAAdgZArQgbAugsApQhxBpiTABQjtAAhDgog");
	this.shape_13.setTransform(40.4,14.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AlzCIQggggAAg2QAAhQBNhEQBXhOCbgdQCFgHB0AHQAOAEB0AeQBtAhAAAWQgBAmgRAvQgYA6gvAxQiECGkPAAQjUAAhHhKgAkhhNQg9A/AABDQAAAfABAFQAFAWAZAOQBDAoDsAAQCTAAByhqQAsgpAagtQAagrAAgeQgBgKhVgcIhjggIjegBQiRAPhOBPg");
	this.shape_14.setTransform(41,14.9);

	this.addChild(this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-6.1,133,141.2);


(lib.Girl02_YardDriving = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#282828").s().p("AgjCaQgQgFgOgGQgrgOgigVQgUgQgRgTIgIgKQgegpgHg6IAAg/IAAgEQAAgjADgdQAUB5BIA8QAOAKAPAIQA5AlBhAPICHAKIADAIQACAKASAfIAEAAQAAAPAIAEQg/AGg3AAQhRAAg8gOg");
	this.shape.setTransform(31.8,54.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AA5DlQhhgPg5glQgOgIgPgKQhIg+gUh3QAEglAGgdQACgGAAgIQACgCAAgEQABgBAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBIAEgIQAEgOAJgRQAlgxApghIAGgEIAEAAQAUAEAHAEQApAXACBcIAEBKQAAANACAKIgCAAQAAABAAAAQgBABAAAAQAAABAAAAQgBABAAAAQgCAEAAAIIACAFQgBAAAAABQAAAAgBABQAAAAAAABQAAAAAAABQgGAQAAATQAAAGACACQAAACAIACIAAAEQAAAGAEAGQAIAJALAEQAQAGAdAAIACgCQAAAAABAAQAAgBAAAAQAAAAABAAQAAgBAAAAQAOAEAPAAICNAAQgOAbgGAeQgGAbAAAdIAAAlIiIgKg");
	this.shape_1.setTransform(31.5,40.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#A88541").s().p("AhBBVQgGgTgCgCIAAgOQAAg0ABgKQADggAQgoQAsgMAfAKQAgALALAcQAIARAAAOIAAANIABAMIgBAOQAAAhgEAGQgLAOg6APg");
	this.shape_2.setTransform(63.4,55.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ah8EhQgRgCgOgEIgCAAQgEgCgFAAQg1gRglgdIAAABIgQgPIgLgKQgMgPgIgOIgCgGQgGgIgFgLQgQglgEgtQgCgQAAg+QAAhAAEgaQAEg0ARgpIAEgMQAYg2AuglIAHgGQAugDAzAGQAlAYATBGQAOA5AABKIAAAHIAAAiQgEARgGAJIACABQAfAAAMAKQAfAFA1ACIAbAAIA8AAQACAEAEAAIAIgLQAEgEACgEIACAAQAdgLAdgDQAggCAaALQAeAMAQAbQAPAagBAhQAABCgEARQgNA3g1AMIhmAAQACAEAAAQQAAAPgCAEQgCAEgKAGIiKAAQgYACgYAAQgyAAgrgIgAjEkUQgpAhglAxQgJAQgEAPIgEAIQAAAAAAABQAAAAAAABQAAABgBAAQAAABgBAAQAAAEgCACQAAAIgCAHQgGAcgEAlQgCAdAAAjIAAACIAABAQAGA8AfApIAIAKQAQASAVARQAhAUArAPQAOAGARAEQBmAZCdgRQgJgEAAgOIgEAAQgSgfgCgKIgCgJIAAglQAAgcAGgbQAGgfAOgbIiLAAQgPAAgOgEQAAABAAAAQAAAAgBABQAAAAAAAAQgBAAAAAAIgCACQgdAAgQgGQgNgEgIgIQgEgGAAgGIAAgFQgIgCAAgCQgCgCAAgFQAAgRAGgRQAAAAAAgBQAAAAAAgBQABAAAAgBQAAAAABgBIgCgGQAAgIACgEQAAgBABAAQAAgBAAAAQABgBAAAAQAAgBAAAAIACAAQgCgLAAgMIgEhKQgChdgpgWQgHgEgUgEIgEAAIgGAEgACwAbQgQApgEAfQgBALAAA2IAAAOQACACAGASIBDAHQA6gPALgPQAFgFAAghIABgOIgBgPIAAgMQAAgOgIgRQgLgdgggKQgPgFgSAAQgVAAgXAGg");
	this.shape_3.setTransform(40.4,44.5);

	// Layer 9
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#826631").s().p("AgQAwIgLAAQgKg1gJgTQAYARAJARIAQAjQgEADgJAAIgGAAgAASglIgGgKQALAIAOAIQAHAEADAHQgEADgHACIgHACIgLgYg");
	this.shape_4.setTransform(129.4,85.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#A88541").s().p("Ag+ByQAAgwgEgWIAAiVQAjAFAegNIAdAtQAnA1AAAVQAAAIgJAGQgDgHgHgEQgOgIgLgGQgVgnAAAsQAAADAJAgQAJAfAAAHQAAAKgGAEIgQgjQgJgTgYgRQgGgKgFAAQgQAAAAAWQAAALAHAcQAHAdAAAKQAAAGgCABIgCABg");
	this.shape_5.setTransform(128.3,81.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AhiA+IgBhWQgBgqADgdQAHg5AYAuIAACVQAEAWAAAwIAKAAIACgBQACgBAAgGQAAgKgHgdQgHgcAAgLQAAgWAQAAQAGAAAFAKQAJATAKA3IALAAQAMABAHgEQAGgEAAgKQAAgHgJgfQgJggAAgCQAAgtAVAnIAGAIIALAYIAHgCQAHgCAEgDQAJgGAAgIQAAgVgng1IgdgtQgIgMAAgDQAAgYAQAGQAPAGAXAbQA3BEAAA3QAAAagCAFQgIATglAAIgBAAQADAKAAAGQAAAKgNASQgRAVgVAAQgKAAgIgCQgBAGgCADQgMASgoAAQgXAAgGhXg");
	this.shape_6.setTransform(128.3,81.4);

	// Layer 2
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#4D2414").s().p("AghCYQgKg1gKhhQgNh3gFgnQAUAGAQAGQAkAGAqACIALB0QgVAUgJAMQAIAIAMAEIAPgCIANCEIgrABQgFABgLAAQgPAAgfgEg");
	this.shape_7.setTransform(42.2,149.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#65301B").s().p("Ah6AaQAWgGAQgQQA6gTBBATQA3AYAVAwQALAcANAyIj4AEgAApgnQhEgRhGARQgFACgWAUIgDACIgLh0QArADAwABQBkABAegNQAGgCAEgGIAHgKQAAAHgFA3QgGA1AFAfQgXgQgegMg");
	this.shape_8.setTransform(60.5,148.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AiwDKQgbgfgSiGQgMhfAAg4IABgtIgBAAQAFgOADgOIARgGIABAAQAkAKAUACQA8AJBMAAQBKAAA7gJQALgMAGgFQAJgGAOgCQAHACAHAAQAGACAAAQIAAABIABACIgCAEIgWB+IgBADQABAAAAABQAAAAgBAAQAAAAgBAAQAAABgCAAIAEAQIAAAbQA6AmAPAuQAGAUAABAQAAATgDAFQgDAFgOAKIkHAEIguABgAi1AJQAKBhAKA2QAzAHANgFIArgBID4gEQgNgzgLgbQgVgwg3gYQhDgTg4ATQgQAQgWAGIgPADQgMgFgIgIQAJgNAVgSIADgDQAWgTAFgCQBEgSBGASQAeAMAXAPQgFgeAGg1QAFg4gBgGIgGAKQgFAGgFACQgeAMhmgBQguAAgrgDQgqgDgmgFQgQgHgUgFQAFAnANB2gAC2iiIABgRIAIAAIABAGQAAAIgEARIgCABQgEgEAAgLg");
	this.shape_9.setTransform(55,148.6);

	// Layer 7
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#4D2414").s().p("AgfCaQABgNgMh2QgBgMgCgHIABgEQAAgRgFglQgFglABgSIAFgvIAEADQATAQAQAAIAtAHIATEfIhWgDg");
	this.shape_10.setTransform(79.9,148.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#65301B").s().p("Ah1CSIgjgBIgSkgQCAAOBwgUQgJAMgBAcIgBA2QgwgLhEAfQhAAdAAAPQAAAZABgKIABAHQADAGAOgIQCFhOBKAxQA6AoAIBuIg7AAQh5AAhsgEg");
	this.shape_11.setTransform(100.7,149.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AACNCIhmgFQgLgCgEgCQgFgDAAgQIAAgHQgJgMgKhDQgJg6AAgSQAAgRAFgIIACgCQgCggAAg3IABgwQABgWAEgOIgBgCIACgUQATgCApAWQCSAcCJgcQANgNAHgEQAJgGAOgCQAGADAIAAQAGACAAAPQAAAUgWAOIgCACQAEAEAAAOQAABAgFAnIgBAIIAaAPQAbARAVAZQAIAKAYAkQAQApgQAqIADAAQgVAZgHARQizAHgVAAIh7gFgAh7IFQAAASAFAlQAEAlAAASIAAAEQACAHABANQALB2AAANIBXADIAhABQCFAFCdgBQgIhug6goQhKgziHBQQgOAIgDgGIgBgHQgBAKAAgZQAAgRBAgdQBGgfAwALIABg2QABgcAIgMQhxAUh/gOIgugGQgQAAgUgQIgDgDIgGAvgAiEHXIABgIIAAgGIgBgCIgBgCIABASgAkTrjQhMgYgFgYQgEgPAggRQAggRBUgCQBTgBBNATQBLATgJAUQgEAHgEAPQgGANgSALQgZANhaAAQhdgBgxgQg");
	this.shape_12.setTransform(86.8,85.9);

	// Layer 3
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#383838").s().p("AhgD/QgWgBgOgEIgigNQgQgHgIgHIgGgFQhjkvgLgdQgWg9gNg/IBzAAIAdgHIAhgJQAGApAIAsQAEAWAQCoQAQCgALAwIAVAagADXD4IhAgKIAAgHQgCgagDiAIgDiLQAQAcAXAYQALAKA6CBQA0BzAnAKIgQAAQg4AAg3gGg");
	this.shape_13.setTransform(59.7,122.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#494949").s().p("Ak0E/QgMgwgPigQgRiogEgWQgIgsgFgpIBQgUQAwgNAPgQQAOgQABgoIgBhLIA6AGQBfAJCxgGIA6gDQAFAWAIAYIAOAlQAYBAAhARQAYAMBHAAIAPgBQgJBNgXBDQgQAuggBUQgFALgLBoIgOBtIgsADQgwAGggAAQgngKgzhzQg4iCgLgLQgXgYgRgcIgFhWQAEg3gCgYQgnADgFAoQgDAUAFAVIAEBcIADCMIADCGIAAAKQgBAIgVADIiqANg");
	this.shape_14.setTransform(80.3,113.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#333333").s().p("AiLFjQACADgegDIgsgGIg6gHIAAgwIAAgJIgBgdIgBgWQgBgbgCgPIgei/IgDgPQgFgkgQg4IgdhdQgqhWgogtIAtgRIAUgEIAagHIEwAAIABAAIAFABQARAEAfALQAdAKAMADQAnBNABCyIADBCIAJEPQAEA6AGAkIg/ABIguABQhVAAg6gEgAFhEaIgBAAIgDgBQgGgBABgLQACgKAAgFIAAgCQgEg9ACgGQAHgjACgVQADgXgBgWIgDgVIgBgPIgBgRIgBgDIAAAAIAAgBIgBgRQAAgKgDgIIAAgCQAAgMgMg8QgLg6gCgFQgLgwgSgjIAQALIAiAbIABAAIAAABQAJAIAkBRQBBEegPA3IgBADIgEAHQgFAHgFAFIgTAPQgLAFgLABIgBAAIgCAAIgBAAIgBAAIgDAAQgKAAgKgCg");
	this.shape_15.setTransform(85.9,39.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#223825").s().p("AgPCrIgDgGIgYiEIgThjQAGgfACgLIAAgCQAEgEADgPQAEggAHgPQAOggAWgjQAeAnAQAwQAFAOAIBNIACAPIgFAIIAAABIAAADQAAALgGAbIgEAIIABAVQAAAJgEAXIAAACIgEAVIgCAHIgDANIgKAwIADgJIgIAgIgBACIgBAHIgDAJQgFAUAAAGIABAOIgBAHIAAACIgKACQAAgkgPglg");
	this.shape_16.setTransform(20.8,45);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E5E582").s().p("AAAAIIAAgEIAAgLQABAGAAAJIAAAAg");
	this.shape_17.setTransform(116.7,38.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E5E5E5").s().p("Am8B0QAGgbAFgiIAOhQQAFgaAAgfIAAgKIAAgBIgBgJIAAgGIBZgCIADAPQAIA1gEBlIgEA/Ih8AEIADgKgAETBvQARgRArgbQAsgbAUgcQAMgPAHgWQAFgEAFgLQAHgPABgKIAAAAIgBgCIADgJQAEgNAAgHQAAgFgBgCIAAgWIACAAIAEAaIAAA0QACARgCAJIAAABQAAADgFAKQgEAfgSAcQgOAWgQANQAAALACACIACAEQAEADATAAQAFAAAJgEIAMgGIABASIipAFg");
	this.shape_18.setTransform(72.5,51.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("Al5A9QAEhlgIg1IgCgQIB2gCIAAAEQAQByANBNIACAaIABAKIiUAEIAEg/gADRh7ICuAAIAAAVQACADgBAFQAAAHgDANIgDAJIAAABIABABQgCAJgGAPQgGAMgEADQgIAXgMAPQgUAbgsAcQgrAagQASg");
	this.shape_19.setTransform(78.3,51.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E88B2D").s().p("AnGEpIADgSIAHghQAFgXAGg1IACgPIAHgTIAIgaIB8gDIgKCZQgCAlADAuIAFBFIgXAEIh+ACQgJg8AAg9gAlhC4IACAAIgCgBIAAABgAFxGHQgSgWgJglQgIgsgHgZQgRhUgEg6IAAgFIAAgLICpgFIABAVIAAASQADAlAEAMIAEA0IgECfIAAAEQhQgJgigDgAmBh9IAAgBIAAgVQAAg1gBgDIgfhVQgHgagFgNQgJgVgRgLIgCgDQgMgOgRgIQAigSAsgPQAuA7AXA4QAaBCASBuIhaADIAAgCg");
	this.shape_20.setTransform(69.4,53.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FE9932").s().p("AlMF0QgDguACglIAKiZICTgEIABAJIAAAJIABAVIAAAGIABA5IABBaQAAAkgMATQgVAkhbAUIggAGIgEhFgAlykRQgXg4gug7IABAAIBSgcIAIAIQAdAgATAcQAPAWAMAXIAGAKQAsBVAPBtIh2ACQgShugahCgAEKhvIgDhMQgEhjgHgsQgMhPgdgfQgBAAAAAAQAAAAABAAQAAAAAAAAQABAAABAAIAEAAQAGAAAWAOQATAJAeAKQAaALAhASIAAACQAFAEAGAKQAZAkAYBHQAFAZAQA1QAFAUABAcIAAAOIAAAEg");
	this.shape_21.setTransform(72.5,50.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#A4C6C6").s().p("AgtgXIAOgaQAAgPgCgIQASgSASgNIAFgEIAWAVIAQASQgYAkgOAgQgHANgDAfQgCAQgFAEIAAACQgBALgGAfQgRhUgMgvg");
	this.shape_22.setTransform(16.3,27.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#371E2D").s().p("AAnAOIABgBIABABIgCAAgAgoAAIAAgLIAAgCIADAAIgCANIgBAAg");
	this.shape_23.setTransform(30,70.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("ABZLvQg4gFgPgHIgRgIIgDgCIgBgBIgKAFQgsAVhKAEQgmAFgngGQgXgEhOgUQgZgFgDgDQgfgchGjbQgwiUgUhzIgFgFQgEgEAAgIQAAgIAFgEQgKg+AAg0QAAhBAFgVQACgKADgHIgDAAIADgeIAAgLIALgCIAAgCIAAgHIgBgOQAAgGAGgUIACgJIACgHIAAgCIAJggIgEAJIAKgwIADgNIACgHIAEgVIAAgCQAEgXAAgJIAAgVIADgKQAGgbAAgLIAAgDIAAgBIAFgIIgCgPQgIhNgEgOQgRgwgegnIgPgTIgWgVIgFAEQgVAOgSARQACAJAAAOIgNAaIgHgbIAEgaIACgNIASgdQACgDAOgKQAPgMAJgFQAngXA1gUQApgRAygOIAhgJIBHgQICGgTIDaAAQAiAFBHAUQBDATACADIABABIAsAQIBDAbQApASAeAWQBjBLARBpIAcCqQAMBHAAAWQABAxgBAHIABALIAAAFIAAAFIgBAFIgNAdIAAAAIgCADIgGAGQgMANgTAGQgJAEgMACQgMACgOAAIgMgBIADAbQAFAoAAAOQAABcgCAdQgGBTgZBNQgOArgjC0QgeCZgRAYQABACgDAFQgDAGgGABQAEABgVAGIgrAIQg5AKgzAAQgyAAg6gKgAmsFXQAKAdBkExIAGAFQAIAHAQAHIAiANQAOAEAVABIAOAAICqgNQAVgDABgIIAAgKIgDiGIgDiMIgEheQgFgVADgUQAFgoAlgDQACAYgEA3IAFBYIAECNQACCAACAaIAAAHIBAAKQBAAHA/gBQAgAAAwgGIAsgDIAOhtQALhoAFgLQAghUAQgwQAXhDAJhNIgPABQhHAAgYgMQghgRgYhAIgOglQgIgYgFgWIg6ADQizAGhdgJIg6gGIABBLQgBAogOAQQgPAQgwANIhQAUIgiAJIgcAHIhzAAQAMA/AXA9gAnMhOIAAANIACAAQgGA2gFAUIgHAhIgDATQAAA9AJA7IB9gCIAXgEIAfgGQBbgUAVgjQAMgUABgkIgChYIAAg4IgBgHIAAgVIgBgJIAAgIIgBgLIgCgZQgNhNgQh1IgBgEQgPhsgshWIgFgKQgMgXgPgVQgUgdgcgfIgIgJIhSAcIgBABQgtAPghARQAQAJANAOIACACQAQAMAJAVQAFAMAIAaIAfBVQABADAAA1IAAAVIAAACIAAABIAAAGIABAJIAAABIAAAKQAAAfgFAaIgOBSQgFAigGAbIgDAKIgIAaIgHASIgDAAIAAACgADYrBQAeAeAMBQQAGAsAEBjIADBLIAJDuIAAAJIAAALIAAAFQAEA6ARBTQAHAYAJAsQAJAlARAXQAiACBQAKIAAgEIAEigIgEgyQgEgLgCglIgBgTIgBgVIgBgSIgMAGQgJAEgFAAQgTAAgEgDIgCgEQgCgCAAgLQAQgNAOgWQASgcAEghQAFgKAAgDIAAgBQACgJgCgRIAAg0IgEgaIAAAAQAAgLgCgGQgBgdgGgUQgPg1gFgYQgYhHgZglQgHgJgFgFIABgCQgigSgagKQgdgLgUgIQgWgOgFAAIgFgBIgBAAQAAAAgBAAQAAAAAAAAQAAAAAAAAQAAABAAAAgAC9gIIA/gBQgGglgEg6IgJkRIgDhCQgBixgnhNQgMgDgdgKQghgMgRgDIgFgBIgBAAIkuAAIgaAGIgUAEIgtARQAoAtAqBXIAdBcQAQA5AFAkIADAQIAeDAQACAPABAbIABAVIABAeIAAAIIAAAwIA6AHIAsAGQAcAEAAgDQBGAEB3gBgAHLoCQACAFALA7QAMA8AAAMIAAACQADAIAAALIABARIAAACIAAAAIACAUIABAPIADAUQABAWgDAYQgCAUgHAjQgCAGAEA+IAAABQAAAFgCALQgBAKAGACIADAAIABAAQAMADALgBIABAAIABAAIACAAIABAAQAMgBAKgFIATgOQAGgGAEgHIAEgGIABgEQAPg3hBkgQgkhQgJgJIAAAAIgBgBIgigbIgQgLQASAjALAwg");
	this.shape_24.setTransform(72,76.8);

	// Layer 1
	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#748474").s().p("AEXFAIgtgGQhJgKh6gOIgJgBIgigEIhMgLIAAAAQgXgNgegHIgPgCIAHgTQANgmAJg7IAOhSQAFgaAAgcIAAgKIAAgCIgBgJIgBgHQAHgCAHgBQAFgBAJANQAHAKAFAKQADAGAIAwQAOA1AlAsQBCBKB4AkIBMAXIAMAFQAYANAJARQAAABgGAAIgWgBgAkyieQAQghALgSQAAgPgCgIQASgSAUgOQAtgfBKgZQgVAPgiAvQgnA0gUAuQgHAOgFAgQgCAQgFADIAAACQgCAXgXBiQgXhTgBhng");
	this.shape_25.setTransform(41.1,43.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#A5BCA5").s().p("AAFFjQgHgRgYgNIgMgFIhMgXQh4gkhChKQgngsgOg1QgIgwgDgGQgFgMgHgKQgJgNgFABQgHABgHACQgDgQgHgFQgKgHgPAVIgHAIIAAAEQAAALgHAbIgDALIABAVQAAAJgEAXIAAABIgFAWIgBAHIgDANIgLAvIAEgJIgIAhQgKAjAAAJIABANIgBAIIAAACIgLACQgCglgOglIgkhPQgRglgLgqQAXhiACgXIAAgCQAFgDACgQQAFggAHgOQAUguAng0QAigvAVgPIB8gqQAlgNAQgHIAVgEIAZgHIEvAAQBfAOAYAAQALAAAEACQAtATBYAxQAKAFAaATQBBApAUAUQAIAIAlBRQBAEegOA3QgbA2g9gOQgHgBABgLQADgMgBgFQgDg9ABgGQAIgjACgVQACgXgBgWIgCgVIgBgPIgCgRIgBgDQgCgMgJgeQgJgggLgSQgNgWgSgWQgbghgUgEQgVgEAAAiQAAAFAqA5QArA5AFA/QAGA6gdArQgOAWgPANQAAALACADIACADQADAEATAAQAGAAAIgEIANgGIACA5QACAlAEAMIACADIgJACIgPAGQgRAGgXAFQhAAOikACIgnABQhKAAg3gEg");
	this.shape_26.setTransform(71.3,39.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#396086").s().p("AAqCaQg0ingRgcQhFh0gciBIAHghIAHglQAeAMBhARIAIABIAFAAIASADQABB6AcChQAFAVAQCpQAPCfAMAwQgigrgxigg");
	this.shape_27.setTransform(36.8,109.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#396087").s().p("AhYFPQgWAAgOgEIgigNQgQgIgIgGIgGgFQhjkwgLgcQgziLAAiQIADgSQAcCBBGB0QARAcA1CnQAyCgAhArIAVAagADfFJIhAgKIAAgIQgCgagDh/IgDiOQAQAdAXAYQALALA6CCQA0ByAnALIgQAAQg4AAg3gGg");
	this.shape_28.setTransform(58.9,114.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#371E2D").s().p("AAgAMIhIgMIAAgLIAAgCIADAAIAPACQAcAHAXALIAAAAIABAAIALAHIgJgCg");
	this.shape_29.setTransform(30,70.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#4B7DAF").s().p("AkwFJQgMgwgPigQgRiogEgWQgdihAAh6QBEAJCjAPQBZAICggFQCtgGBXgRIAXgGIgECgQgHBkgdBVQgQAuggBUQgFALgLBoIgOBtIgsADQgwAGggAAQgngKgzhzQg4iCgLgLQgXgYgRgcIgFhWQAEg3gCgYQgnADgFAoQgDAUAFAVIAEBcIADCMIADCGIAAAKQgBAIgVADIiqANg");
	this.shape_30.setTransform(79.9,112.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("ABcLvQg4gFgPgHIgRgIIgEgCIAAgBIgKAFQgsAVhKAEQgmAFgngGQgXgEhOgUQgZgFgDgDQgfgchGjbQhRj5gBidQAAhBAFgVQACgKADgHIgDAAIADgeIgBgLIAMgCIAAgCIAAgHIgBgOQAAgIAKgkIAJggIgEAJIAKgwIADgNIACgHIAEgVIAAgCQAEgXAAgJIAAgVIADgKQAGgbAAgLIAAgEIAHgLQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAnIgHASIgDAAIAAACIAAANIBKANIAJACIgLgHIAAgBIBLALIAkAFIAJABQB7AOBJAJIAsAGQAcAEgCgDQBFAEBjgBQCkgCBBgOQAXgFARgHIAOgFIAJgDIgBgDQgEgLgCglIgDg6IgMAGQgJAEgFAAQgTAAgEgDIgCgEQgCgCAAgLQAQgNAOgWQAcgrgFg6QgGg/grg7Qgpg6AAgFQAAghAVAEQATAEAbAgQATAWANAXQALARAJAhQAJAgACAMIACAUIABAPIADAUQABAWgDAYQgCAUgHAjQgCAGAEA+QAAAEgCANQgBAKAGACQA+ANAag2QAPg3hBkgQgkhQgJgJQgUgUhBgpQgagSgJgFQhZgxgsgTQgFgCgLAAQgYAAhfgOIkuAAIgaAGIgUAEQgQAHgmANIh7AqQhKAZgtAgQgVAOgSARQACAJAAAOQgKASgQAiIAAgJQAAgKAKg8IACgNIASgdQACgDAOgKQAPgMAJgFQBwhCCvghICGgTIDaAAQAnAFA3AKQA4ALABACIACABIA2AaQA3AaAYAKQApASAeAWQBjBLARBpIAcCqQAMBHAAAWQABAxgBAHIABALQAAAfgWAWQgNANgSAGQgJAEgMACQgMACgOAAIgMgBIADAbQAFAoAAAOQAABcgCAdQgGBTgZBNQgOArgjC0QgeCZgRAYQABACgDAFQgDAGgGABQAEABgVAGIgrAIQg5AKgzAAQgyAAg6gKgAnSAJIgHAhIgDATQAACQAzCKQAKAdBkExIAGAFQAIAHAQAHIAiANQAOAEAVABIAOAAICqgNQAVgDABgIIAAgKIgDiGIgDiMIgEheQgFgVADgUQAFgoAlgDQACAYgEA3IAFBYIAECNQACCAACAaIAAAHIBAAKQBAAHA/gBQAgAAAwgGIAsgDIAOhtQALhoAFgLQAghUAQgwQAdhVAHhkIAEigIgXAGQhXARitAGQiiAFhXgIQijgPhEgJIgSgCIgFgBIgIgBQhjgOgegNIgHAjg");
	this.shape_31.setTransform(71.7,76.8);

	this.addChild(this.shape_31,this.shape_30,this.shape_29,this.shape_28,this.shape_27,this.shape_26,this.shape_25,this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(6.4,0.7,132,169.2);


(lib.Girl02_Yard = function() {
	this.initialize();

	// Layer 9
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#826631").s().p("AoRCOQgTgLAAgbQAAgJAEgSIAEgWQAKATAEAdQACAWAHANIgLAEgApiCDQgagHAAgcQAAgYAHgSIAFgOQAGAQAGAZQALAdAXANIgIAHQgFADgHAAQgFAAgHgCgAqcBaQgSgUAAgSQgEgeAag/QAKgZAWgoIATgjIAYAIQggA0gMAYQgXAyAAAoQAAALABAJQgHAVAAAWgAJuAzIgLAAQgKg0gJgUQAYARAJARIASAjQgGADgJAAIgGAAgAKSghIgGgLQALAJAOAHQAHAEADAHQgEADgHACIgHACIgLgXg");
	this.shape.setTransform(65.4,85.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A88541").s().p("AoiBvQgDgdgLgTIAAgCQAAgigNATIgEAHQgRAfAAAcIABAPIgDACIgBABQgYgNgLgcQgGgZgGgQIACgHQAAgSgGgBQgGAAgIANQgHAMgFAOQgCgJAAgLQAAgpAXgxQAMgZAhgzQBNAWBFgFIAAAFQAAAJASAxQARAwAAATQAAALgMAIQgHAGgMAFIgNAFIgCgXQgHgXgMgLIgEgEQgKgHAAAWIAFAkQAFAfAAAOQAAAlgiAOIgGACQgHgMgDgWgAIhBTQAAgwgEgWIAAiVQAkAFAdgNIAgAtQAnA1AAAXQAAAIgJAGQgDgHgHgEQgOgIgLgIQgWgnAAAtQAAAEAKAeQAJAfAAAHQAAAKgHAEIgRgjQgKgTgYgPQgFgKgFAAQgRAAAAAWQAAAJAIAcQAHAdAAAKQAAAGgCABIgDABg");
	this.shape_1.setTransform(67.4,84.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AoWCzQgLAAgLgFQgMgFgFgJQgMAGgTAAQgYAAgRgHQgTgIgFgPIAAgBIgBAAQgYgCgQgmIgLgmQAAgoAEgRQAFgZAUgsQAVgxAYgcQAOgPAMAAQANgBgHAQIgGAMIgTAjQgVAogKAZQgaA/ADAeQAAASATAUIAFAHQAAgXAIgVQAFgNAHgMQAIgNAGAAQAGAAAAASIgCAIIgFANQgHASAAAYQAAAdAaAGQAPAEAKgFIAIgHIABgBIADgCIgBgQQAAgcARgfIAEgHQANgSAAAhIAAACIgDAWQgEASAAAJQAAAbATALIAAAAIAMgEIAGgCQAigPAAglQAAgOgFgfIgFgjQAAgWAKAGIAEAEQAMAMAHAWIACAYIANgGQAMgEAHgGQAMgIAAgMQAAgSgRgwQgSgyAAgIIAAgGQACg4AdBJQAfBMAAAnQAAASgOASQgTAagjACIAAADQAAAPgFAPIgHASQgHARgUANQgTALgSAAIgCAAgAIJAgIgBhWQgBgqAEgdQAGg6AYAuIAACWQAEAVAAAxIAKAAIADgBQACgCAAgGQAAgKgHgcQgIgcAAgJQAAgXARAAQAFAAAFALQAKARAKA2IAKAAQAOACAHgFQAHgEAAgJQAAgHgJggQgKgeAAgDQAAgtAWAmIAFALIALAYIAHgDQAHgCAFgCQAJgGAAgJQAAgWgng2IgggtQgHgMAAgDQAAgYARAHQAQAGAWAbQA4BDAAA5QAAAZgCAEQgJATglAAIAAAAQADAKAAAGQAAALgOARQgRAWgVAAQgLAAgJgCQgBAFgCADQgLATgoAAQgYAAgGhXg");
	this.shape_2.setTransform(66.2,84.5);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4C2414").s().p("AghCYQgKg1gKhhQgNh3gFgnQAUAGAQAGQAkAGAqACIALB0QgVAUgJAMQAIAIAMAEIAPgCIANCEIgrABQgFABgLAAQgPAAgfgEg");
	this.shape_3.setTransform(42.2,149.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#65301B").s().p("Ah6AaQAWgGAQgQQA6gTBBATQA3AYAVAwQALAcANAyIj4AEgAApgnQhEgRhGARQgFACgWAUIgDACIgLh0QArADAwABQBkABAegNQAGgCAEgGIAHgKQAAAHgFA3QgGA1AFAfQgXgQgegMg");
	this.shape_4.setTransform(60.5,148.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AiwDKQgbgfgSiGQgMhfAAg4IABgtIgBAAQAFgOADgOIARgGIABAAQAkAKAUACQA8AJBMAAQBKAAA7gJQALgMAGgFQAJgGAOgCQAHACAHAAQAGACAAAQIAAABIABACIgCAEIgWB+IgBADQABAAAAABQAAAAgBAAQAAAAgBAAQAAABgCAAIAEAQIAAAbQA6AmAPAuQAGAUAABAQAAATgDAFQgDAFgOAKIkHAEIguABgAi1AJQAKBhAKA2QAzAHANgFIArgBID4gEQgNgzgLgbQgVgwg3gYQhDgTg4ATQgQAQgWAGIgPADQgMgFgIgIQAJgNAVgSIADgDQAWgTAFgCQBEgSBGASQAeAMAXAPQgFgeAGg1QAFg4gBgGIgGAKQgFAGgFACQgeAMhmgBQguAAgrgDQgqgDgmgFQgQgHgUgFQAFAnANB2gAC2iiIABgRIAIAAIABAGQAAAIgEARIgCABQgEgEAAgLg");
	this.shape_5.setTransform(55,148.6);

	// Layer 7
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#4C2414").s().p("AgfCaQABgNgMh2QgBgMgCgHIABgEQAAgRgFglQgFglABgSIAFgvIAEADQATAQAQAAIAtAHIATEfIhWgDg");
	this.shape_6.setTransform(79.9,148.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#65301B").s().p("Ah1CSIgjgBIgSkgQCAAOBwgUQgJAMgBAcIgBA2QgwgLhEAfQhAAdAAAPQAAAZABgKIABAHQADAGAOgIQCFhOBKAxQA6AoAIBuIg7AAQh5AAhsgEg");
	this.shape_7.setTransform(100.7,149.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AACNCIhmgFQgLgCgEgCQgFgDAAgQIAAgHQgJgMgKhDQgJg6AAgSQAAgRAFgIIACgCQgCggAAg3IABgwQABgWAEgOIgBgCIACgUQATgCApAWQCSAcCJgcQANgNAHgEQAJgGAOgCQAGADAIAAQAGACAAAPQAAAUgWAOIgCACQAEAEAAAOQAABAgFAnIgBAIIAaAPQAbARAVAZQAIAKAYAkQAQApgQAqIADAAQgVAZgHARQizAHgVAAIh7gFgAh7IFQAAASAFAlQAEAlAAASIAAAEQACAHABANQALB2AAANIBXADIAhABQCFAFCdgBQgIhug6goQhKgziHBQQgOAIgDgGIgBgHQgBAKAAgZQAAgRBAgdQBGgfAwALIABg2QABgcAIgMQhxAUh/gOIgugGQgQAAgUgQIgDgDIgGAvgAiEHXIABgIIAAgGIgBgCIgBgCIABASgAkTrjQhMgYgFgYQgEgPAggRQAggRBUgCQBTgBBNATQBLATgJAUQgEAHgEAPQgGANgSALQgZANhaAAQhdgBgxgQg");
	this.shape_8.setTransform(86.8,85.9);

	// Layer 3
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#383838").s().p("AhgD/QgWgBgOgEIgigNQgQgHgIgHIgGgFQhjkvgLgdQgWg9gNg/IBzAAIAdgHIAhgJQAGApAIAsQAEAWAQCoQAQCgALAwIAVAagADXD4IhAgKIAAgHQgCgagDiAIgDiLQAQAcAXAYQALAKA6CBQA0BzAnAKIgQAAQg4AAg3gGg");
	this.shape_9.setTransform(59.7,122.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#494949").s().p("Ak0E/QgMgwgPigQgRiogEgWQgIgsgFgpIBQgUQAwgNAPgQQAOgQABgoIgBhLIA6AGQBfAJCxgGIA6gDQAFAWAIAYIAOAlQAYBAAhARQAYAMBHAAIAPgBQgJBNgXBDQgQAuggBUQgFALgLBoIgOBtIgsADQgwAGggAAQgngKgzhzQg4iCgLgLQgXgYgRgcIgFhWQAEg3gCgYQgnADgFAoQgDAUAFAVIAEBcIADCMIADCGIAAAKQgBAIgVADIiqANg");
	this.shape_10.setTransform(80.3,113.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#282828").s().p("AgyD+IAAgBIgCgBIgBAAIgHgGIgDAAQgNAAgGhHIgChJIAAg/QACgIAHgTQACgzAzhsQASgoAKgVQAAgOgCgIQASgSAVgOIAFgDIAWAUIAPATQgXAjgPAhQgGAOgFAgQgCAQgFADIAAACQgDAcgfCLQgIAlgDAyQgBAXgBA1IAEAAIAAAIIAAAKQACAIAKABQgfgBgRgLg");
	this.shape_11.setTransform(12.4,43.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#333333").s().p("AAQFjQACADgcgDIgsgGIg6gHIAAgwIAAgJIgBgdIgBgWQgBgbgCgPIgei/IgDgPQgFgkgQg4IgdhdQgqhWgogtIAtgRIAUgEIAagHIEuAAIABAAIAEABQASAEAhALQAdAKAMADQAnBNABCyIACBCIAKEPQAEA6AGAkIg/ABIguABQhXAAg6gEgAo+EsIgGAAQgKgCgCgHIAAgKIAAgIIgEgBQABg1ABgXQADgyAIgkQAhiMADgcIAAgCQAFgDACgQQAFggAGgOQAPghAXgjQAeAoARAwQAEANAIBNIACAPIgFAGIAAACIAAACQAAALgGAbIgDALIAAAVQAAAJgEAXIAAABIgEAWIgCAHIgDANIgKAvIAEgJIgJAhIAAACIgCAGIgDAKQgFAUAAAGIABANIAAAIIAAACQgkAHgcgBIALgCIgRAAQgLADgJAAIgDAAgAH9EaIgBAAIgCgBQgGgBABgLQACgKAAgFIAAgCQgEg9ACgGQAHgjACgVQADgXgBgWIgDgVIgBgPIgBgRIgBgDIAAAAIgBgBIAAgRQAAgKgDgIIAAgCQAAgMgMg8QgLg6gCgFQgLgwgTgjIARALIAiAbIABAAIAAABQAJAIAkBRQBBEegPA3IgCADIgDAHQgFAHgFAFIgUAPQgKAFgMABIgBAAIgBAAIgCAAIgBAAIgCAAQgKAAgLgCg");
	this.shape_12.setTransform(70.2,39.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E5E582").s().p("AAAAIIAAgEIAAgLQABAGAAAJIAAAAg");
	this.shape_13.setTransform(116.7,38.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E5E5E5").s().p("Am8B0QAGgbAFgiIAOhQQAFgaAAgfIAAgKIAAgBIgBgJIAAgGIBZgCIADAPQAIA1gEBlIgEA/Ih8AEIADgKgAETBvQARgRArgbQAsgbAUgcQAMgPAHgWQAFgEAFgLQAHgPABgKIAAAAIgBgCIADgJQAEgNAAgHQAAgFgBgCIAAgWIACAAIAEAaIAAA0QACARgCAJIAAABQAAADgFAKQgEAfgSAcQgOAWgQANQAAALACACIACAEQAEADATAAQAFAAAJgEIAMgGIABASIipAFg");
	this.shape_14.setTransform(72.5,51.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("Al5A9QAEhlgIg1IgCgQIB2gCIAAAEQAQByANBNIACAaIABAKIiUAEIAEg/gADRh7ICuAAIAAAVQACADgBAFQAAAHgDANIgDAJIAAABIABABQgCAJgGAPQgGAMgEADQgIAXgMAPQgUAbgsAcQgrAagQASg");
	this.shape_15.setTransform(78.3,51.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E88B2D").s().p("AnGEpIADgSIAHghQAFgXAGg1IACgPIAHgTIAIgaIB8gDIgKCZQgCAlADAuIAFBFIgXAEIh+ACQgJg8AAg9gAlhC4IACAAIgCgBIAAABgAFxGHQgSgWgJglQgIgsgHgZQgRhUgEg6IAAgFIAAgLICpgFIABAVIAAASQADAlAEAMIAEA0IgECfIAAAEQhQgJgigDgAmBh9IAAgBIAAgVQAAg1gBgDIgfhVQgHgagFgNQgJgVgRgLIgCgDQgMgOgRgIQAigSAsgPQAuA7AXA4QAaBCASBuIhaADIAAgCg");
	this.shape_16.setTransform(69.4,53.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FE9932").s().p("AlMF0QgDguACglIAKiZICTgEIABAJIAAAJIABAVIAAAGIABA5IABBaQAAAkgMATQgVAkhbAUIggAGIgEhFgAlykRQgXg4gug7IABAAIBSgcIAIAIQAdAgATAcQAPAWAMAXIAGAKQAsBVAPBtIh2ACQgShugahCgAEKhvIgDhMQgEhjgHgsQgMhPgdgfQgBAAAAAAQAAAAABAAQAAAAAAAAQABAAABAAIAEAAQAGAAAWAOQATAJAeAKQAaALAhASIAAACQAFAEAGAKQAZAkAYBHQAFAZAQA1QAFAUABAcIAAAOIAAAEg");
	this.shape_17.setTransform(72.5,50.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#371E2D").s().p("ABnAOIABgBIACABIgDAAgAAXAAIAAgLIAAgCIADAAIgCANIgBAAgAhpAAQAJAAALgDIARAAIgLACIgVABIgFAAg");
	this.shape_18.setTransform(23.5,70.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("ACQLvQg4gFgPgHIgRgIIgFgCIgBgBIgKAFQgqAVhKAEQgmAFgngGQgXgEhOgUQgZgFgDgDQgfgchGjbQgwiUgUhzIgFgFQgEgEAAgIQAAgIAFgEQgKg+AAg0QAAhBAFgVQACgKADgHIgpAAIgKAAIgTABQgJAAgNgCQgPgDgWgGIgNgEIgFgCQgXgHgKgEIgHABQgJgGgGgfQgEgWAAgNQgEgXACggQAGgsAAgPQAAhcAehjIABgCIAUgpIAOgbIAOghQAHgMAQgYIADgCIAAAAIAXglQACgDAOgKQAPgMAJgFQAngXA1gUQApgRAygOIAhgJIBHgQICGgTIDaAAQAiAFBHAUQBDATACADIABABIAsAQIBDAbQApASAeAWQBjBLARBpIAcCqQAMBHAAAWQABAxgBAHIABALIAAAFIAAAFIgBAFIgNAdIAAAAIgCADIgGAGQgMANgTAGQgJAEgMACQgMACgOAAIgMgBIADAbQAFAoAAAOQAABcgCAdQgGBTgZBNQgOArgjC0QgeCZgRAYQABACgDAFQgDAGgGABQAEABgVAGIgrAIQg5AKgzAAQgyAAg6gKgAl1FXQAKAdBkExIAGAFQAIAHAQAHIAiANQAOAEAVABIAOAAICogNQAVgDABgIIAAgKIgDiGIgDiMIgEheQgFgVADgUQAFgoAngDQACAYgEA3IAFBYIAECNQACCAACAaIAAAHIBAAKQBAAHA/gBQAgAAAwgGIAsgDIAOhtQALhoAFgLQAghUAQgwQAXhDAJhNIgPABQhHAAgYgMQghgRgYhAIgOglQgIgYgFgWIg6ADQizAGhdgJIg6gGIABBLQgBAogOAQQgPAQgwANIhQAUIgiAJIgcAHIhzAAQAMA/AXA9gAmVhOIAAANIACAAQgGA2gFAUIgHAhIgDATQAAA9AJA7IB9gCIAXgEIAfgGQBbgUAVgjQAMgUABgkIgChYIAAg4IgBgHIAAgVIgBgJIAAgIIgBgLIgCgZQgNhNgQh1IgBgEQgPhsgshWIgFgKQgMgXgPgVQgUgdgcgfIgIgJIhSAcIgBABQgtAPghARQAQAJANAOIACACQAQAMAJAVQAFAMAIAaIAfBVQABADAAA1IAAAVIAAACIAAABIAAAGIABAJIAAABIAAAKQAAAfgFAaIgOBSQgFAigGAbIgDAKIgIAaIgHASIgDAAIAAACgAEPrBQAeAeAMBQQAGAsAEBjIADBLIAJDuIAAAJIAAALIAAAFQAEA6ARBTQAHAYAJAsQAJAlARAXQAiACBQAKIAAgEIAEigIgEgyQgEgLgCglIgBgTIgBgVIgBgSIgMAGQgJAEgFAAQgTAAgEgDIgCgEQgCgCAAgLQAQgNAOgWQASgcAEghQAFgKAAgDIAAgBQACgJgCgRIAAg0IgEgaIAAAAQAAgLgCgGQgBgdgGgUQgPg1gFgYQgYhHgZglQgHgJgFgFIABgCQgigSgagKQgdgLgUgIQgWgOgFAAIgFgBIgBAAQAAAAgBAAQAAAAAAAAQAAAAAAAAQAAABAAAAgAD0gIIA/gBQgGglgEg6IgJkRIgDhCQgBixgnhNQgMgDgdgKQghgMgRgDIgFgBIgBAAIkuAAIgaAGIgUAEIgtARQAoAtAqBXIAdBcQAQA5AFAkIADAQIAeDAQACAPABAbIABAVIABAeIAAAIIAAAwIA6AHIAqAGQAeAEgCgDQBIAEB3gBgApShSIABABIACABIAAAAQARALAfACIAGABIADAAIAEAAIAWgBQAcABAkgIIAAgCIAAgHIgBgOQAAgGAGgUIACgJIACgHIAAgCIAJggIgEAJIAKgwIADgNIACgHIAEgVIAAgCQAEgXAAgJIAAgVIADgKQAGgbAAgLIAAgDIAAgBIAFgIIgCgPQgIhNgEgOQgRgwgegnIgPgTIgWgVIgFAEQgVAOgSARQACAJAAAOQgMAVgSAnQgzBtgCA1QgHATgCAHIAABAIACBIQAGBIANAAIADgBIAHAGgAp3hFIAAAAIAAgBgAICoCQACAFALA7QAMA8AAAMIAAACQADAIAAALIABARIAAACIAAAAIACAUIABAPIADAUQABAWgDAYQgCAUgHAjQgCAGAEA+IAAABQAAAFgCALQgBAKAGACIADAAIABAAQAMADALgBIABAAIABAAIACAAIABAAQAMgBAKgFIATgOQAGgGAEgHIAEgGIABgEQAPg3hBkgQgkhQgJgJIAAAAIgBgBIgigbIgQgLQASAjALAwg");
	this.shape_19.setTransform(66.5,76.8);

	// Layer 1
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#748474").s().p("AE4FAIgtgGQhJgKh6gOIgJgBIgkgEIhKgLIAAAAQgXgNgegHIgPgCIAHgTQANgmAJg7IAOhSQAFgaAAgcIAAgKIAAgCIgBgJIgBgHQAHgCAHgBQAFgBAJANQAHAKAFAKQADAGAIAwQAOA1AlAsQBCBKB4AkIBMAXIAMAFQAYANAJARQAAABgGAAIgWgBgAkwD8IgBgBIgCgBIgBAAIgGgGIgEAAQgNAAgFhHIgDhIIABhAQABgHAHgTQACg0AzhsQATgoAMgUQAAgPgCgIQASgSAUgOQAtgfBKgZQgVAPgiAvQgnA0gUAuQgHAOgFAgQgCAQgFADIAAACQgDAcghCMQgHAkgDAyQgCAXAAA1IADABIAAAIIAAAKQADAHAKACQgfgCgRgLg");
	this.shape_20.setTransform(37.8,43.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#A5BCA5").s().p("AAQFjQgJgRgWgNIgMgFIhMgXQh3gkhChKQgngsgPg1QgIgwgDgGQgEgMgIgKQgJgNgFABQgHABgGACQgDgQgHgFQgKgHgQAVIgHAIIAAAEQAAALgGAbIgDALIAAAVQAAAJgEAXIAAABIgEAWIgCAHIgDANIgKAvIAEgJIgJAhQgKAjAAAJIABANIAAAIIAAACQgkAHgcgBIALgCIgRAAQgLADgJAAIgDAAIgGAAQgKgCgCgHIAAgKIAAgIIgEgBQABg1ABgXQADgyAIgkQAhiMADgcIAAgCQAFgDACgQQAFggAGgOQAVguAmg0QAjgvAVgPIB7gqQAmgNAQgHIAUgEIAagHIEuAAQBfAOAYAAQALAAAFACQAsATBZAxQAJAFAaATQBBApAUAUQAJAIAkBRQBBEegPA3QgaA2g+gOQgGgBABgLQACgMAAgFQgEg9ACgGQAHgjACgVQADgXgBgWIgDgVIgBgPIgBgRIgBgDQgCgMgJgeQgJgggLgSQgNgWgTgWQgbghgTgEQgVgEAAAiQAAAFApA5QArA5AGA/QAFA6gcArQgOAWgQANQAAALACADIACADQAEAEATAAQAFAAAJgEIAMgGIADA5QACAlAEAMIABADIgJACIgOAGQgRAGgXAFQhBAOikACIgmABQhLAAg3gEg");
	this.shape_21.setTransform(70.2,39.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#396086").s().p("AAqCaQg0ingRgcQhFh0gciBIAHghIAHglQAeAMBhARIAIABIAFAAIASADQABB6AcChQAFAVAQCpQAPCfAMAwQgigrgxigg");
	this.shape_22.setTransform(36.8,109.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#396087").s().p("AhYFPQgWAAgOgEIgigNQgQgIgIgGIgGgFQhjkwgLgcQgziLAAiQIADgSQAcCBBGB0QARAcA1CnQAyCgAhArIAVAagADfFJIhAgKIAAgIQgCgagDh/IgDiOQAQAdAXAYQALALA6CCQA0ByAnALIgQAAQg4AAg3gGg");
	this.shape_23.setTransform(58.9,114.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#371E2D").s().p("ABhAMIhKgMIAAgLIAAgCIADAAIAPACQAeAHAXALIAAAAIABAAIALAHIgJgCgAhpAAQAJAAALgDIARAAIgLACIgVABIgFAAg");
	this.shape_24.setTransform(23.5,70.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#4B7DAF").s().p("AkwFJQgMgwgPigQgRiogEgWQgdihAAh6QBEAJCjAPQBZAICggFQCtgGBXgRIAXgGIgECgQgHBkgdBVQgQAuggBUQgFALgLBoIgOBtIgsADQgwAGggAAQgngKgzhzQg4iCgLgLQgXgYgRgcIgFhWQAEg3gCgYQgnADgFAoQgDAUAFAVIAEBcIADCMIADCGIAAAKQgBAIgVADIiqANg");
	this.shape_25.setTransform(79.9,112.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("ACQLvQg4gFgPgHIgRgIIgFgCIgBgBIgKAFQgqAVhKAEQgmAFgngGQgXgEhOgUQgZgFgDgDQgfgchGjbQhRj5gBidQAAhBAFgVQACgKADgHIgpAAIgKAAIgTABQgJAAgNgCQgPgDgWgGIgNgEIgFgCQgXgHgKgEIgHABQgJgGgGgfQgEgWAAgNQgEgXACggQAGgsAAgPQAAhcAehjIABgCIAUgpIAOgbIAOghQAHgMAQgYIADgCIAAAAIAXglQACgDAOgKQAPgMAJgFQBwhCCvghICGgTIDaAAQAnAFA3AKQA4ALABACIACABIA2AaQA3AaAYAKQApASAeAWQBjBLARBpIAcCqQAMBHAAAWQABAxgBAHIABALQAAAfgWAWQgMANgTAGQgJAEgMACQgMACgOAAIgMgBIADAbQAFAoAAAOQAABcgCAdQgGBTgZBNQgOArgjC0QgeCZgRAYQABACgDAFQgDAGgGABQAEABgVAGIgrAIQg5AKgzAAQgyAAg6gKgAmeAJIgHAhIgDATQAACQAzCKQAKAdBkExIAGAFQAIAHAQAHIAiANQAOAEAVABIAOAAICogNQAVgDABgIIAAgKIgDiGIgDiMIgEheQgFgVADgUQAFgoAngDQACAYgEA3IAFBYIAECNQACCAACAaIAAAHIBAAKQBAAHA/gBQAgAAAwgGIAsgDIAOhtQALhoAFgLQAghUAQgwQAdhVAHhkIAEigIgXAGQhXARitAGQiiAFhXgIQijgPhEgJIgSgCIgFgBIgIgBQhjgOgegNIgHAjgADdgIQCkgCBBgOQAXgFARgHIAOgFIAJgDIgBgDQgEgLgCglIgDg6IgMAGQgJAEgFAAQgTAAgEgDIgCgEQgCgCAAgLQAQgNAOgWQAcgrgFg6QgGg/grg7Qgpg6AAgFQAAghAVAEQATAEAbAgQATAWANAXQALARAJAhQAJAgACAMIACAUIABAPIADAUQABAWgDAYQgCAUgHAjQgCAGAEA+QAAAEgCANQgBAKAGACQA+ANAag2QAPg3hBkgQgkhQgJgJQgUgUhBgpQgagSgJgFQhZgxgsgTQgFgCgLAAQgYAAhfgOIkuAAIgaAGIgUAEQgQAHgmANIh7AqQhKAZgtAgQgVAOgSARQACAJAAAOQgMAVgSAnQgzBtgCA1QgHATgCAHIAABAIACBIQAGBIANAAIADgBIAHAGIABABIACABIAAAAQARALAfACIAGABIADAAIAEAAIAWgBQAcABAkgIIAAgCIAAgHIgBgOQAAgIAKgkIAJggIgEAJIAKgwIADgNIACgHIAEgVIAAgCQAEgXAAgJIAAgVIADgKQAGgbAAgLIAAgEIAHgLQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAnIgHASIgDAAIAAACIAAANIBKANIAJACIgLgHIAAgBIBLALIAkAFIAJABQB7AOBJAJIAqAGQAeAEgCgDQBFAEBjgBgAp3hFIAAAAIAAgBg");
	this.shape_26.setTransform(66.5,76.8);

	this.addChild(this.shape_26,this.shape_25,this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-6,0.7,144.5,169.2);


(lib.GHAIR_helmet = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3A1B00").s().p("AFtIOQAAhvgriJQgPgvgog7QgXgjhhiBQgbgjg0gyQg1gzgsgiQiLhdhogYQhmgXgJA8QgRAHgQAKQgngngmguIGNiTQDHg/C6hFIAUAVQB3B/AfCqQBCDsg6EnQAABbhmDvIAAhAg");
	this.shape.setTransform(114.5,114.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Ag4E/QiKhqhThzIAAiBQATgTAEgiQAEgYgEgiIgDgmQABgTAThUIAIgfIC2hDID4hcQAmAvAnAnQhQA0gjCeQgbB1AADEIACBHQAEBUAKBGQAKBCANAvQidhhhKg6g");
	this.shape_1.setTransform(45,131.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#020100").s().p("AG7MNQAAgbAchaIAchaQAVhBAHgiQALg0AAhhQAAhlgfhYQglh5hjh+QjHj/k3goIgDgCQgOALgNANIgOAQQhHBRgSB4QgHA4gGBlQgFBOAAAcQAABUA0C1QAZBWAYBJIACAGIAAACQAAASgEgEQAAAAAAAAQAAAAgBAAQAAABAAABQgBAAAAABQgEAFgNAAIgDAAIgFAAQhKgBjjiyQhBgygwgoQgagYgegjQgsgzgXg+QAEgJgBgJIAAgdQBTBzCLBqQBLA7CdBgQgNgugKhCQgKhGgEhUIgChHQAAjEAbh1QAjieBQg0QAQgLARgGQAJg9BmAYQBmAXCLBeQAuAhA1AzQA0AzAbAlQBhCAAXAjQAoA6APAvQArCIAABvIAABAQBmjvAAhaQA6knhCjtQgfiqh3h/IgUgVIA0gTIAJAJQB1CBAoCxQAdB8gBCYQgBBpgTCqQgRCYhgD2QhFCthXCrQgGAEgFAAQgMAAgBgag");
	this.shape_2.setTransform(92.4,134.6);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(16.8,53.9,151.4,161.4);


(lib.GHAIR = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3A1B00").s().p("AJQMLQAAhvgriIQgPgvgog8QgXgjhhiAQgbglg0gzQg1gzgughQiLhchmgXQhmgYgJA7QgRAGgQALQipiniTkyQCqhPD3gKIDfAAQEABBCRCbQB3B/AfCqQBCDrg6EpQAABahmDvIAAhAgAp5oNQhIhfgNhvIgCgVQBIg0BsgmIDjAAQCMAtAZAaQANANACATIAAAwQAAAwgIAYQgGASgUAMIgNAJQjPANiXBIQgYAMgWAMIgxg2g");
	this.shape.setTransform(91.8,89.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAEKaQlVkGgBk+QAAjuB6inQBNhrB6hDQASgLAXgKQCSEyCqCpQhRAygjCeQgaB1gBDGIACBHQAFBUAKBGQAJBCANAuQichghMg7gAlKm8QgIgQgBgZIABgzQABh3A8hSQAiguA1glIACAVQANBvBIBfIAxA2QhyBChNBoQhFgtgQgeg");
	this.shape_1.setTransform(38.8,96.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#020100").s().p("AIPQbQAAgbAbhaIAdhaQAVhBAHgiQALg0AAhiQAAhlgghYQglh4hjiAQjGj9k3goIgDgCQgPAKgMANIgPAQQhHBRgRB2QgIA4gGBmQgEBQAAAcQAABUAzC0QAZBXAZBJIACAGIAAACQAAASgEgFQAAAAgBAAQAAABAAAAQAAAAgBABQAAABgBABQgEAEgMAAIgEAAIgEAAQhLAAjjiyQjJiegpg4Qh6imgXhJQgMgmgCg0QgBgQAAhmQAAjwBuixQhPgLgXhbQgJghAAgoIABgkQAAiUBDhXQAqg4BPgwQBLgwBtgpIDtAAQCdAkArA8QAQAXAEAhQABAJAAAwQAAA8gHAYIgGATIANAAIAAgEIDjAAQEXBECcCtQB1CBApCwQAdB9gBCVQgBBpgTCtQgRCXhhD2QhFCuhWCqQgHAEgFAAQgMAAAAgZgAmrpzQgWAKgVAMQh6BChNBrQh5CnAADwQAAE8FXEHQBMA7CdBgQgOgvgJhCQgKhGgEhUIgChHQAAjFAah2QAkicBQg0QAPgLASgGQAJg9BkAYQBnAXCMBeQAuAiA0AzQA0AwAcAlQBgCBAYAiQAnA8APAvQAsCJAABvIgBBAQBnjvAAhbQA5kphBjqQgfirh3h+QiRibkAhBIjfAAQj3AJiqBPgAqDulQg1AmghAtQg9BTAAB3IgBAzQAAAYAJAQQAPAfBFAtQBNhpByhBQAXgNAYgLQCWhJDQgNIANgJQAUgLAGgTQAIgXAAgxIAAgvQgCgUgNgNQgagaiLgtIjjAAQhtAmhIA0g");
	this.shape_2.setTransform(84.1,107.7);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.1,215.3);


(lib.gameovertext_mc = function() {
	this.initialize();

	// Layer 1
	this.gameover_txt = new cjs.Text("TIME'S UP", "35px 'VAGRounded'", "#FFFFFF");
	this.gameover_txt.name = "gameover_txt";
	this.gameover_txt.textAlign = "center";
	this.gameover_txt.lineHeight = 41;
	this.gameover_txt.lineWidth = 368;
	this.gameover_txt.setTransform(237.1,0,1.29,1.29);

	this.addChild(this.gameover_txt);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,479.3,57.4);


(lib.FrankHead = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCCCCC").s().p("AlpBZIAAilID6AAIAACFQgOAFgtAJQg2AJhOAJgACKBMIAAiUIAcgNICxgDQADAGAIAMQAIANAAATQgBAZgIAfQgKAkgNAZg");
	this.shape.setTransform(98.6,88.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7A4D30").s().p("ADUJCQhIgchLhJQgdgdgTgsIgag6IAMgGQASgMAAgXQAAgMgBgFQgHgPgSAAQgMAAghAMQggAKgMABQgngBglgTQg1gdgkhBQgCgGAAgJIAAgUQAAgkAbgUQAKgHAGgHQAfAHAcAPQATALAZAAQALAAAGgCQAJgFAAgNQAlAXAqAFQArAFgJgcQgoh3gSicQgSijAUhoQAShcAjgxIAEgFQBwCUAWCxQANBrAGD4QAUCaAZCEQAYCCAPBbQgbgHgXgJg");
	this.shape_1.setTransform(30.8,89.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5B371E").s().p("ABgGmQgqgFglgXQAAgRgZgTQgWgQgjgKQgSgFgNAAQgljagBgKQABhHAPhTQAUh+AwhiQAqhWA3g5QArA4AgA2IAMAVIgBABIgEAGQglAxgRBcQgVBoASChQASCbAqB6QAIAYggAAIgMgBg");
	this.shape_2.setTransform(20,58.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#804E29").s().p("AkiBDQgrgLglAqIgMgVQggg2grg1QCFiFDZAVID9AAQDHAsBxBiQgjCLjVAGIgUABQirAAk1hPg");
	this.shape_3.setTransform(70.6,19.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#975F3C").s().p("AkEJzQgPhbgYiCQgZiEgUibQgGj3gNhrQgWixhwiVIABgBQAlgrArALQFJBUCqgFQDWgGAjiMQA9A1AzB4QAZA9AOAyQAWBJgKCcIgCAbQi1gYg2BOQgMATgFATIhXADIhAABIgEgbQgGgpgOgMIkqAAQgRAZgFAqQgCASgBAjQgDAEABANQABAMACAAQABAaAFAaQAJA1AWASIBXAAQCmABArhAQAPgWAAggIAAghICTgEIACAMIAAA/QACAxAXATIAiAAQgGAHAAAUQABAdgDAJQgFAPgSAHIiJAAIg2gaQACgfgBgKQgBgRgPgBQgQgCgLAYQgKAVAAAQIgCAUQABAQAMALQAiAhCCAAQBgAAAfghQAQgSAAgmQAAgmgZgOICYAAQgJApgTAqIgoBaQgLAYgGADQguARgjAJQgjAKgvAHQidAWiRgXQgzgHAAAdIBAAXQBXAOA4ACQBVADBMgMQAhgGArgLQAogKAggFIAAACQgSCEhkA/QhLAvhlAAIgSAAQjQAAh+gjg");
	this.shape_4.setTransform(83.2,85.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgSMEQg8gJgrgNQhrgghKgzQhyhQgkiCIAAAAIghAAQhlABg4hDQgogwgDguIgIgFQgDgUAAgXQAAgsAEgSQALgqAggKQgMgegJhpIgIhlQAAjiBWi0QAthfBAg9QCQiMDvAWIAAgDID0AAQEBAtCFCNQA8A/AiBhQAWA9AKA0QgEAAAMAsQANAwAABuQAAA4gEArQAOAMAEALQACAFAAAtQAAATgUBDQgLAkgKAWQgHAmgIAgQgTBGgkBUQgDAGgEAaIgGAkQgIB1hKBTQhMBXhuAKIjOAEIh1gOgAlyGFQABAGAAALQAAAYgSALIgOAHIAcA5QATAsAdAdQBLBJBIAdQAXAIAbAIQCCAlDegCQBlgBBLguQBkg/ASiEIAAgDQggAGgoAKQgrALghAGQhMAMhVgDQg4gChZgPIhAgWQAAgdAzAHQCTAXCdgXQAvgGAjgLQAjgIAugSQAGgDALgYIAohZQATgrAJgpIiYAAQAZAOAAAnQAAAmgQASQgfAghgABQiCgBgiggQgMgLgBgQIACgUQAAgQAKgVQALgYAQABQAPACABAQQABAKgCAgIA2AaICJAAQASgHAFgQQADgIgBgdQAAgUAGgIIgiAAQgXgSgCgxIAAg/IgCgNIiTAFIAAAgQAAAhgPAWQgrBAimgBIhXAAQgWgSgJg1QgFgagBgaQgCgBgBgMQgBgNADgDQABglACgSQAFgoARgZIEqAAQAOALAGAoIAEAbIBAgBIBXgDQAFgTAMgTQA2hNC1AZIACgcQAKibgWhJQgOgzgZg8Qgzh4g9g2QhxhjjIgsIj8AAQjZgUiFCEQg6A5gpBWQgwBhgVB+QgPBWAABHQAAAKAmDXQANABARAFQAkAKAWAQQAbATAAARQAAANgJAFQgGACgLAAQgZAAgTgLQgcgPgfgIQgGAIgKAHQgbAUAAAkIAAATQAAAKACAGQAkBAA1AdQAlAUAnAAQAMAAAggLQAhgLAMAAQAUAAAHAOgAhLC0IA6AAQBMgIA3gKQAtgIAOgFIAAiHIj4AAgAHEAFIgcAMIAACXIDAADQANgZAJgkQAJgiAAgYQAAgTgHgOQgIgLgDgGg");
	this.shape_5.setTransform(70,79);

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AjSAqQgLgLAAgMQAAgGABgEIg/ABQgSAEgaAIQgTAGgKAAQgLAAgJgBIgGgCIgDgRIAEgKQACgHAGgFQASgLBKgIQA9gQB+ANQAWATABAEQAAADgJAIQgIAFgGAEIgnABQACAFAAAHQAAAVgNAKQgKAHgPAAQgZAAgQgQgAD/AZQgVgEgJgJQgCgCgBgDIgCgEIACgFIgFgBQgKgCgKgEIgIgDIAAgDIgBgBQgBgCACgLQAFggBbAEQAoACA5AJIACAAIABAFQACAEgCALQgCAJg3ALIgVAEIgCAAIAAACQgFAageAAIgPgBg");
	this.shape_6.setTransform(96.2,88.1);

	// Layer 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EACFB7").s().p("AjCAwIgLgCQAAgBgEgDQgDgCAAgSIh5AKQgPABgKgCIgIgCQAAAAAAgBQAAAAAAAAQgBAAAAABQgBAAAAABQgDACgBgOQgBgOAFgEQAHgHAdgFQAtgIDZgQIAqgEQAAABBIgBICTgFQAzgDAkACQAyACAWAIQAXAIAAAQQAAAOgMADIgWAAIgOAAQgBAXgCAHIgdAGQgsgCgMgQQgDgDgBgUIghgCQkEAHgKgEIgBAAIgxAJQgBAOgLALQgNALgUACIgPAAIgOAAg");
	this.shape_7.setTransform(94.6,87.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#DEBBA4").s().p("ADVJFQhIgbhMhLQgdgcgTgsIgZg6IAKgHQATgMAAgXQAAgMgBgGQgHgOgSAAQgLAAgiAMQghAKgMABQgngBgmgTQg1gdgjhCQgDgGAAgKIABgTQAAgkAagUQAKgHAHgIQAfAIAcAPQATALAaAAQAKAAAHgCQAIgFABgNQAkAXArAFQArAEgKgbQgnh5gSicQgTikAVhoQARhdAkgyIAFgGQBwCWAVCyQAOBsAHD6QATCbAZCEQAZCEAPBbQgbgHgYgKg");
	this.shape_8.setTransform(30.8,89.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#AA8462").s().p("ABhGpQgrgFgkgXIAAAAQAAgRgZgTQgXgQgkgKQgRgFgNgBQgnjbAAgKQAAhHAPhUQAWh/AwhiQAqhXA4g5QArA4AgA2IANAVIgBACIgFAFQgmAygRBdQgVBoATCiQASCcApB7QAJAXgfAAIgNAAg");
	this.shape_9.setTransform(19.9,58.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#CEA077").s().p("AkkBDQgsgLgkArIgMgVQghg2grg2QCFiGDcAVID+AAQDIAtByBiQgjCNjYAGIgTAAQisAAk3hQg");
	this.shape_10.setTransform(70.8,19.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgKMJQg8gJgsgNQhsgghKg0Qh0hQgjiCIAAgBIgiABQhlAAg5hEQgogvgCgvIgJgFQgDgVAAgWQAAgsAFgTQAKgqAggKQgLgegKhqIgIhmQAAjkBXi1QAuhfBAg+QCRiNDxAXIAAgEID1AAQECAuCHCOQA8A/AjBjQAVA8AKA1QgDAAALAsQANAwABBvQAAB8gVA+QgDALACAmQACArgBAGQgIA6gMAvQgTBGgkBUQgDAHgEAbIgGAkQgIB1hLBUQhMBXhvAKIjPAEQhRgJglgFgAltGIQACAFAAAMQAAAYgTALIgNAHIAbA6QAUAsAdAdQBLBKBIAcQAYAJAbAIQCDAkDfgBQBmAABLgvQBlhAASiFIAAgCQgfAFgpAKQgsALggAGQhNAMhWgDQg4gChagOIhAgXQAAgdA0AHQCUAYCdgYQAwgHAjgJQAjgJAugSQAHgDALgYIAohaQAWgyAIgwQAHgqAAg5QAAgOACgJQgCgHAAgGQAKhOAFhPQAKidgWhKQgOgygZg9Qg0h4g9g2QhyhkjJgtIj+AAQjbgViFCGQg6A5gqBXQgwBigVB/QgPBWAABHQAAAKAmDZQANABARAFQAkAKAXAQQAbATAAARIAAAAQAAANgJAFQgGACgLAAQgZAAgUgLQgcgPgfgHQgGAHgKAIQgbATAAAlIgBATQAAAKADAFQAjBCA2AdQAlAUAnAAQAMAAAhgLQAigMALAAQAUAAAHAPgACyFPQgMgLgBgQIACgUQAAgQAKgVQALgZAQACQAQACAAAQIAAAqIA2AZICKAAQASgGAFgPQADgKgBgcQAAgrAZAPQAZAOAAAnQAAAmgQASQgfAhhgAAQiEAAgighg");
	this.shape_11.setTransform(69.4,79.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EAD0B7").s().p("AkGJ3QgPhcgZiDQgZiEgTicQgHj5gNhsQgWiyhwiWIABgBQAkgrAsALQFMBUCqgFQDYgGAjiNQA9A2A0B5QAZA8AOAzQAWBKgKCcQgFBQgKBNQAAAHACAGQgCAJAAAOQAAA6gHAqQgIAvgWAzIgoBaQgLAYgHADQguARgjAJQgjAKgwAHQidAXiSgYQg0gHAAAeIBAAXQBYAOA4ACQBWADBNgNQAggFAsgLQApgLAfgEIAAACQgSCFhlA/QhLAvhmABIgMAAQjVAAiBgkgAAkC1QgKAVAAARIgCAUQABAPAMALQAiAhCEAAQBgAAAfghQAQgRAAgnQAAgmgZgPQgZgOAAAqQABAdgDAJQgFAPgSAHIiKAAIg2gZIAAgqQAAgRgQgCIgCAAQgOAAgLAXgAhiA4QADADAAABIALACQAPABAOgBQAUgCANgLQAMgLABgOIAvgJIAAAAQAKAEEGgJIAhADQABAVADADQAMAQAtACIAcgGQADgHABgXIAOAAIAVAAQAMgDAAgQQAAgOgWgIQgWgIgzgCQgkgCgyADIiTAFQhKABAAgBIgrAEQjXAQgsAGQgdAFgIAIQgEAFABAOQABAOACgCQABgBAAAAQABAAAAAAQAAAAABAAQAAAAAAAAIAHACQALACAOgBIB6gKQAAASADACg");
	this.shape_12.setTransform(83.5,86.1);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,139.8,158.3);


(lib.ForkArm = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A4262").s().p("AhHCOIE+i3IBAAlIk/C3gAk2AFIE/i3IA/AkIk+C3g");
	this.shape.setTransform(31.1,42.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4D2A3E").s().p("ABXCuIAAglIithlIAAAlIhAgkIAAj3IBAAkIAAAkICtBlIAAgkIBAAlIAAD3gAhWgbICtBkIAAgoIithjg");
	this.shape_1.setTransform(47.2,21.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,62.3,60.9);


(lib.FinalScorecopy = function() {
	this.initialize();

	// Layer 2
	this.score_txt = new cjs.Text("20465", "36px 'Laffayette Comic Pro'", "#FFFFFF");
	this.score_txt.name = "score_txt";
	this.score_txt.textAlign = "center";
	this.score_txt.lineHeight = 38;
	this.score_txt.lineWidth = 183;
	this.score_txt.setTransform(114.2,23.4);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("ASTEPIAAAtIgwAAMgjIAAAIgtAAIAAgtIAAoYIAAgyIAtAAMAjIAAAIAwAAIAAAtIAAAFg");
	this.shape.setTransform(117.1,35.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9E8C49").s().p("ARjElIAAodMgjIAAAIAAIdIgtAAIAAoXIAAgyIAtAAMAjIAAAIAvAAIAAAtIAAAFIAAIXg");
	this.shape_1.setTransform(117.1,33.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#474747").s().p("Aw4B3IAAhNIAAigMAhwAAAIAADtg");
	this.shape_2.setTransform(117.1,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9E8C48").s().p("ARjAWMgjIAAAIgtAAIAAgrIAtAAMAjIAAAIAvAAIAAArg");
	this.shape_3.setTransform(117.1,64.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#666666").s().p("Aw4BqIAAjTMAhwAAAIAADTg");
	this.shape_4.setTransform(117.1,23.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CCB55E").s().p("AxkEPIAAodMAjIAAAIAAIdgAw2gLIAACgIAABOMAhwAAAIAAjuIAAjVMghwAAAg");
	this.shape_5.setTransform(117,35.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.score_txt);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1.5,2.2,237.2,66.3);


(lib.FB_butn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhDFDIAAknIhjAAIAAhxIBjAAIAAhUQAAhJApgpQAlgnBEABQA5gBAfAFIAABnIg9AAQghAAgNAPQgLANAAAcIAABJIByAAIgPBxIhjAAIAAEng");
	this.shape.setTransform(25.8,21.4,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3A5A94").s().p("AiMC/QgxAAAAgzIAAkXQAAgzAxAAIEZAAQAxAAAAAzIAAEXQAAAzgxAAg");
	this.shape_1.setTransform(19,19.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.1,0.1,38.2,38.2);


(lib.EndWhiteBoxcopy = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EA2wgbXMhtfAAAQiOAAAACOIAALTIAAJSIAAduQAACOCOAAMBtfAAAQCOAAAAiOIAA9uIAApSIAArTQAAiOiOAAg");
	this.shape.setTransform(363.7,195.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C1C1C1").s().p("Eg49ALZIAApSIAArRQAAiOCOAAMBtfAAAQCOAAAACOIAALRIAAJSg");
	this.shape_1.setTransform(363.7,92.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Eg2vAP/QiOAAAAiOIAA9vMBx7AAAIAAdvQAACOiOAAg");
	this.shape_2.setTransform(363.7,268.1);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2.9,18,733.3,354.5);


(lib.EndWhiteBox = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EA2wgbXMhtfAAAQiOAAAACOIAALTIAAMkIAAacQAACOCOAAMBtfAAAQCOAAAAiOIAA6cIAAskIAArTQAAiOiOAAg");
	this.shape.setTransform(363.7,195.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C1C1C1").s().p("Eg49ANCIAAskIAArRQAAiOCOAAMBtfAAAQCOAAAACOIAALRIAAMkg");
	this.shape_1.setTransform(363.7,103.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Eg2vAOWQiOAAAAiOIAA6cMBx7AAAIAAacQAACOiOAAg");
	this.shape_2.setTransform(363.7,278.6);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2.9,18,733.3,354.5);


(lib.endofleveltext_mc = function() {
	this.initialize();

	// Layer 1
	this.info_txt = new cjs.Text("click the\npallets for a\ndescription", "27px 'Laffayette Comic Pro'");
	this.info_txt.name = "info_txt";
	this.info_txt.textAlign = "center";
	this.info_txt.lineHeight = 34;
	this.info_txt.lineWidth = 447;
	this.info_txt.setTransform(263.6,0,1.18,1.18);

	this.addChild(this.info_txt);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,532,292);


(lib.DeckingFlipped = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8D6A4A").s().p("AjGCVQAFgggDgTIgEgWIAAgDIgBgBIAAgBQAPgGA6gfIAzgbIgCAhQABAbACAMQgMAFgTAMQgTAMgGAEQgPAHgVAOQgUANgKAKgAg/BIQABgMAAgUQAAgXgCgQIACgBIgDgBIAAgCIATgHQAQgHAQgIQAUgLAdgbQAcgZAHgDIgBARQAAAeAOAqQgEABgYAQQgZATgWAKQgSAJgWALIgeAQgABfgcQAAgNgFgYIgFgaIABgFQAVACA8gpQAHgFAMgFIAPgJIgBAkQAAAbADAUQgHABgSAKQgVALgNAGQgkAOgNALg");
	this.shape.setTransform(67.4,42.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#72553C").s().p("AAKAiQijhOg7gfQgFgDACgNIAEgXQAAgXgBgPIBSAyQBNAvBIAoQCPBMA2AgQABAFgCAYQgCAXABAIIjMh3g");
	this.shape_1.setTransform(23.8,41.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#A37956").s().p("AizB9Ih/hEQhGgmgtgSIgCgDIgBAAQAdgQAfgVQAcgTALgLQAGADAGAAIAFgFIBdA0QBLApA8AdIB+BBQA7AeAYASQgyAdg4AmIgBgGIgLAAIgBAJQhXg2hmg3gACSClIgCAAIABgEQgagZg+giQgfgRhqg2Qg3gahHglIhbgwQAHgFARgIQARgIALgIQAqgdAegRIAJAEQAAAIAFABQACAAACgCIABgBIBZAwQA/AiAwAfQAzAeBDAbQAmAQBJAcQgLABgWAWQgcAcgKAGIgiAUQgMAJgHAKIgCAAIAAABgADxApQgngKgZgPQgxgegqgWIgegQQgugcg1gdIhfgxIA/giQAZgOA5gbQBxA+BuBJQAOAKBCAcQBBAcAyAgQgWAIgSAJIgtAaQgZAOgQADQgRgJgpgKg");
	this.shape_2.setTransform(44.5,24.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAKEtIgFgGIACgDIgCABQgugaidhbQiNhUhXgsIgGAFIgDgJIgOgHIACgHIAJACIgBgFIgCgbQAAgdACgSIgCgCIACgDIgCgMQABgMAIgJQAJgJASgKQAUgJAWgNIAbgRIAAgGIAGADIAHgGIADgCQABgBABAGQAVgWAtgUQAfgOAggJIAMABQABABgEgGIAMADQAIgTAzgaQAugXAygRIAMACIACABIADgDQArAdC9BxQAvAdBHAeQA9AaAVAPIAAgBIABACIANAGQgFARgDAiIgFAuIAFAFIgGAFIAAABIgBAAIgZAUQgSAOgRAIQgyAWgHAFIgFAAIgXAYQgRAQgQAFQgaALgaANIghATIgPgBQgGAHgQAMQgSANgSAKQg8AlgUAGIgBADgABdCZQg6AfgPAGIAAABIABABIAAADIAEAWQADATgFAgIAAAHQAKgKAUgNQAVgOAPgHQAGgEATgMQATgMAMgFQgCgMgBgbIACghgAmogCIgDAVQgDANAFADQA7AeClBRIDLB2QgCgIACgWQACgYAAgFQg0ggiQhOQhKgohNgvIhSgwQABAOAAAYgAECA2QgdAbgWALQgQAIgQAHIgTAHIAAACIADADIgCABQACAQAAAXQAAAUgBAMIABAIIAegQQAWgLAUgJQAWgKAZgTQAYgSAEgBQgOgqAAgeIABgRQgHADgcAZgAlyhbQgeAUgdARIABAAIABAEQAuATBGAjIB/BEQBmA3BWA2IABgJIALAAIACAGQA3gmAygdQgYgSg7geIh9g/Qg9gdhKgqIhdg1IgFAGQgGgBgGgDQgLALgdAUgAj6iiQgLAIgSAIQgRAIgGAFIBbAwQBGAnA4AbQBpAzAfASQA+AhAaAZIAAAFIABAAIAEAAIAAAAIACAAQAGgLANgJIAigUQAKgGAcgcQAVgWAMAAQhJgbgngQQhDgcgzgfQgvgfg/giIhZgvIgBAAQgCADgDgBQgEAAAAgJIgJgEQgfASgpAcgAE0AaIAFAaQAFAYAAANIAAAKQANgLAkgOQANgGAVgLQASgKAHgBQgDgUAAgaIABgjIgPAJQgMAFgHAFQg8AngVgCgAhRj1Ig/AiIBfAyQA1AdAuAbIAfAQQApAXAyAfQAYAPAoAKQApAKAQAIQAQgEAagMIAtgaQARgIAWgJQgxghhCgdQhBgcgPgKQhuhJhwg+Qg5AbgaAOg");
	this.shape_3.setTransform(45.1,30.2);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,90.1,60.3);


(lib.Decking = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#72553C").s().p("AjGCVQAFgggDgTIgEgWIAAgDIgBgBIAAgBQAPgGA6gfIAzgbIgCAhQABAbACAMQgMAFgTAMQgTAMgGAEQgPAHgVAOQgUANgKAKgAg/BIQABgMAAgUQAAgXgCgQIACgBIgDgBIAAgCIATgHQAQgHAQgIQAUgLAdgbQAcgZAHgDIgBARQAAAeAOAqQgEABgYAQQgZATgWAKQgSAJgWALIgeAQgABfgcQAAgNgFgYIgFgaIABgFQAVACA8gpQAHgFAMgFIAPgJIgBAkQAAAbADAUQgHABgSAKQgVALgNAGQgkAOgNALg");
	this.shape.setTransform(67.4,42.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#8E6A4B").s().p("AAKAiQijhOg7gfQgFgDACgNIAEgXQAAgXgBgPIBSAyQBNAvBIAoQCPBMA2AgQABAFgCAYQgCAXABAIIjMh3g");
	this.shape_1.setTransform(23.8,41.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#A37956").s().p("AizB9Ih/hEQhGgmgtgSIgCgDIgBAAQAdgQAfgVQAcgTALgLQAGADAGAAIAFgFIBdA0QBLApA8AdIB+BBQA7AeAYASQgyAdg4AmIgBgGIgLAAIgBAJQhXg2hmg3gACSClIgCAAIABgEQgagZg+giQgfgRhqg2Qg3gahHglIhbgwQAHgFARgIQARgIALgIQAqgdAegRIAJAEQAAAIAFABQACAAACgCIABgBIBZAwQA/AiAwAfQAzAeBDAbQAmAQBJAcQgLABgWAWQgcAcgKAGIgiAUQgMAJgHAKIgCAAIAAABgADxApQgngKgZgPQgxgegqgWIgegQQgugcg1gdIhfgxIA/giQAZgOA5gbQBxA+BuBJQAOAKBCAcQBBAcAyAgQgWAIgSAJIgtAaQgZAOgQADQgRgJgpgKg");
	this.shape_2.setTransform(44.5,24.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAKEtIgFgGIACgDIgCABQgugaidhbQiNhUhXgsIgGAFIgDgJIgOgHIACgHIAJACIgBgFIgCgbQAAgdACgSIgCgCIACgDIgCgMQABgMAIgJQAJgJASgKQAUgJAWgNIAbgRIAAgGIAGADIAHgGIADgCQABgBABAGQAVgWAtgUQAfgOAggJIAMABQABABgEgGIAMADQAIgTAzgaQAugXAygRIAMACIACABIADgDQArAdC9BxQAvAdBHAeQA9AaAVAPIAAgBIABACIANAGQgFARgDAiIgFAuIAFAFIgGAFIAAABIgBAAIgZAUQgSAOgRAIQgyAWgHAFIgFAAIgXAYQgRAQgQAFQgaALgaANIghATIgPgBQgGAHgQAMQgSANgSAKQg8AlgUAGIgBADgABdCZQg6AfgPAGIAAABIABABIAAADIAEAWQADATgFAgIAAAHQAKgKAUgNQAVgOAPgHQAGgEATgMQATgMAMgFQgCgMgBgbIACghgAmogCIgDAVQgDANAFADQA7AeClBRIDLB2QgCgIACgWQACgYAAgFQg0ggiQhOQhKgohNgvIhSgwQABAOAAAYgAECA2QgdAbgWALQgQAIgQAHIgTAHIAAACIADADIgCABQACAQAAAXQAAAUgBAMIABAIIAegQQAWgLAUgJQAWgKAZgTQAYgSAEgBQgOgqAAgeIABgRQgHADgcAZgAlyhbQgeAUgdARIABAAIABAEQAuATBGAjIB/BEQBmA3BWA2IABgJIALAAIACAGQA3gmAygdQgYgSg7geIh9g/Qg9gdhKgqIhdg1IgFAGQgGgBgGgDQgLALgdAUgAj6iiQgLAIgSAIQgRAIgGAFIBbAwQBGAnA4AbQBpAzAfASQA+AhAaAZIAAAFIABAAIAEAAIAAAAIACAAQAGgLANgJIAigUQAKgGAcgcQAVgWAMAAQhJgbgngQQhDgcgzgfQgvgfg/giIhZgvIgBAAQgCADgDgBQgEAAAAgJIgJgEQgfASgpAcgAE0AaIAFAaQAFAYAAANIAAAKQANgLAkgOQANgGAVgLQASgKAHgBQgDgUAAgaIABgjIgPAJQgMAFgHAFQg8AngVgCgAhRj1Ig/AiIBfAyQA1AdAuAbIAfAQQApAXAyAfQAYAPAoAKQApAKAQAIQAQgEAagMIAtgaQARgIAWgJQgxghhCgdQhBgcgPgKQhuhJhwg+Qg5AbgaAOg");
	this.shape_3.setTransform(45.1,30.2);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,90.1,60.3);


(lib.CurlyArrowcopy = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgsBhIhXgOIAsg2IB7iZIBgD5g");
	this.shape.setTransform(12.2,5.1);

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AihCkQg7grAAglQAAgfAdgKQAkgNCWgOQgHgjgYgaQgOgQgigXQghgWgMgPQgTgZAAgiQAAg4CGAUQB9ATAxAsQAwA2AJAiQADAOAABCQAABAgZA1QgsBahnAAQiCAAhPg6g");
	mask.setTransform(20.2,18);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhKCxQgWgKgZgTQglgdgIgcIBbgUQAHAKAHAGQAHAEANAGQAnASAngQQAqgRASgnIAHgTIAAgCIABgHIABgJQABgJgBgJQgGgrgjgaQgigbgpAGQgUADgRAKQgRAJgMAPQALAOAhAiIicAWIgGimIAQAPIAfAbQAtg8BQgLQA1gIAyAXQAxAWAfAsIAAgBQATAbAIAgQAJAggCAeIAAADIgCARIgHAcQgRA0gqAkQgrAlg4AHQgOACgOAAQglAAglgQg");
	this.shape_1.setTransform(19.6,19.4);

	this.shape_1.mask = mask;

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1.1,-7.4,40.3,46.2);


(lib.ControlPanel = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F4D96F").s().p("EgBUAmdIgSAAIgFAAIgFAAIhsAAQhxABgBhyMAAAhJXQABhxBxgBIBsAAIAFAAIAFAAIASAAIGiAAMAAABM6g");
	this.shape.setTransform(33.5,212.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-34,67,492.4);


(lib.ConcreteLintelsFlipped = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#636363").s().p("AjGCVQAFgggDgTIgEgWIAAgDIgBgBIAAgBQAPgGA6gfIAzgbIgCAhQABAbACAMQgMAFgTAMQgTAMgGAEQgPAHgVAOQgUANgKAKgAg/BIQABgMAAgUQAAgXgCgQIACgBIgDgBIAAgCIATgHQAQgHAQgIQAUgLAdgbQAcgZAHgDIgBARQAAAeAOAqQgEABgYAQQgZATgWAKQgSAJgWALIgeAQgABfgcQAAgNgFgYIgFgaIABgFQAVACA8gpQAHgFAMgFIAPgJIgBAkQAAAbADAUQgHABgSAKQgVALgNAGQgkAOgNALg");
	this.shape.setTransform(67.4,42.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#999999").s().p("AizB9Ih/hEQhGgmgtgSIgCgDIgBAAQAdgQAfgVQAcgTALgLQAGADAGAAIAFgFIBdA0QBLApA8AdIB+BBQA7AeAYASQgyAdg4AmIgBgGIgLAAIgBAJQhXg2hmg3gACSClIgCAAIABgEQgagZg+giQgfgRhqg2Qg3gahHglIhbgwQAHgFARgIQARgIALgIQAqgdAegRIAJAEQAAAIAFABQACAAACgCIABgBIBZAwQA/AiAwAfQAzAeBDAbQAmAQBJAcQgLABgWAWQgcAcgKAGIgiAUQgMAJgHAKIgCAAIAAABgADxApQgngKgZgPQgxgegqgWIgegQQgugcg1gdIhfgxIA/giQAZgOA5gbQBxA+BuBJQAOAKBCAcQBBAcAyAgQgWAIgSAJIgtAaQgZAOgQADQgRgJgpgKg");
	this.shape_1.setTransform(44.5,24.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#878787").s().p("AAKAiQijhOg7gfQgFgDACgNIAEgXQAAgXgBgPIBSAyQBNAvBIAoQCPBMA2AgQABAFgCAYQgCAXABAIIjMh3g");
	this.shape_2.setTransform(23.8,41.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAKEtIgFgGIACgDIgCABQgugaidhbQiNhUhXgsIgGAFIgDgJIgOgHIACgHIAJACIgBgFIgCgbQAAgdACgSIgCgCIACgDIgCgMQABgMAIgJQAJgJASgKQAUgJAWgNIAbgRIAAgGIAGADIAHgGIADgCQABgBABAGQAVgWAtgUQAfgOAggJIAMABQABABgEgGIAMADQAIgTAzgaQAugXAygRIAMACIACABIADgDQArAdC9BxQAvAdBHAeQA9AaAVAPIAAgBIABACIANAGQgFARgDAiIgFAuIAFAFIgGAFIAAABIgBAAIgZAUQgSAOgRAIQgyAWgHAFIgFAAIgXAYQgRAQgQAFQgaALgaANIghATIgPgBQgGAHgQAMQgSANgSAKQg8AlgUAGIgBADgABdCZQg6AfgPAGIAAABIABABIAAADIAEAWQADATgFAgIAAAHQAKgKAUgNQAVgOAPgHQAGgEATgMQATgMAMgFQgCgMgBgbIACghgAmogCIgDAVQgDANAFADQA7AeClBRIDLB2QgCgIACgWQACgYAAgFQg0ggiQhOQhKgohNgvIhSgwQABAOAAAYgAECA2QgdAbgWALQgQAIgQAHIgTAHIAAACIADADIgCABQACAQAAAXQAAAUgBAMIABAIIAegQQAWgLAUgJQAWgKAZgTQAYgSAEgBQgOgqAAgeIABgRQgHADgcAZgAlyhbQgeAUgdARIABAAIABAEQAuATBGAjIB/BEQBmA3BWA2IABgJIALAAIACAGQA3gmAygdQgYgSg7geIh9g/Qg9gdhKgqIhdg1IgFAGQgGgBgGgDQgLALgdAUgAj6iiQgLAIgSAIQgRAIgGAFIBbAwQBGAnA4AbQBpAzAfASQA+AhAaAZIAAAFIABAAIAEAAIAAAAIACAAQAGgLANgJIAigUQAKgGAcgcQAVgWAMAAQhJgbgngQQhDgcgzgfQgvgfg/giIhZgvIgBAAQgCADgDgBQgEAAAAgJIgJgEQgfASgpAcgAE0AaIAFAaQAFAYAAANIAAAKQANgLAkgOQANgGAVgLQASgKAHgBQgDgUAAgaIABgjIgPAJQgMAFgHAFQg8AngVgCgAhRj1Ig/AiIBfAyQA1AdAuAbIAfAQQApAXAyAfQAYAPAoAKQApAKAQAIQAQgEAagMIAtgaQARgIAWgJQgxghhCgdQhBgcgPgKQhuhJhwg+Qg5AbgaAOg");
	this.shape_3.setTransform(45.1,30.2);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,90.1,60.3);


(lib.ConcreteLintels = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#636363").s().p("AAKAiQijhOg7gfQgFgDACgNIAEgXQAAgXgBgPIBSAyQBNAvBIAoQCPBMA2AgQABAFgCAYQgCAXABAIIjMh3g");
	this.shape.setTransform(23.8,41.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#999999").s().p("AizB9Ih/hEQhGgmgtgSIgCgDIgBAAQAdgQAfgVQAcgTALgLQAGADAGAAIAFgFIBdA0QBLApA8AdIB+BBQA7AeAYASQgyAdg4AmIgBgGIgLAAIgBAJQhXg2hmg3gACSClIgCAAIABgEQgagZg+giQgfgRhqg2Qg3gahHglIhbgwQAHgFARgIQARgIALgIQAqgdAegRIAJAEQAAAIAFABQACAAACgCIABgBIBZAwQA/AiAwAfQAzAeBDAbQAmAQBJAcQgLABgWAWQgcAcgKAGIgiAUQgMAJgHAKIgCAAIAAABgADxApQgngKgZgPQgxgegqgWIgegQQgugcg1gdIhfgxIA/giQAZgOA5gbQBxA+BuBJQAOAKBCAcQBBAcAyAgQgWAIgSAJIgtAaQgZAOgQADQgRgJgpgKg");
	this.shape_1.setTransform(44.5,24.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#878787").s().p("AjGCVQAFgggDgTIgEgWIAAgDIgBgBIAAgBQAPgGA6gfIAzgbIgCAhQABAbACAMQgMAFgTAMQgTAMgGAEQgPAHgVAOQgUANgKAKgAg/BIQABgMAAgUQAAgXgCgQIACgBIgDgBIAAgCIATgHQAQgHAQgIQAUgLAdgbQAcgZAHgDIgBARQAAAeAOAqQgEABgYAQQgZATgWAKQgSAJgWALIgeAQgABfgcQAAgNgFgYIgFgaIABgFQAVACA8gpQAHgFAMgFIAPgJIgBAkQAAAbADAUQgHABgSAKQgVALgNAGQgkAOgNALg");
	this.shape_2.setTransform(67.4,42.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAKEtIgFgGIACgDIgCABQgugaidhbQiNhUhXgsIgGAFIgDgJIgOgHIACgHIAJACIgBgFIgCgbQAAgdACgSIgCgCIACgDIgCgMQABgMAIgJQAJgJASgKQAUgJAWgNIAbgRIAAgGIAGADIAHgGIADgCQABgBABAGQAVgWAtgUQAfgOAggJIAMABQABABgEgGIAMADQAIgTAzgaQAugXAygRIAMACIACABIADgDQArAdC9BxQAvAdBHAeQA9AaAVAPIAAgBIABACIANAGQgFARgDAiIgFAuIAFAFIgGAFIAAABIgBAAIgZAUQgSAOgRAIQgyAWgHAFIgFAAIgXAYQgRAQgQAFQgaALgaANIghATIgPgBQgGAHgQAMQgSANgSAKQg8AlgUAGIgBADgABdCZQg6AfgPAGIAAABIABABIAAADIAEAWQADATgFAgIAAAHQAKgKAUgNQAVgOAPgHQAGgEATgMQATgMAMgFQgCgMgBgbIACghgAmogCIgDAVQgDANAFADQA7AeClBRIDLB2QgCgIACgWQACgYAAgFQg0ggiQhOQhKgohNgvIhSgwQABAOAAAYgAECA2QgdAbgWALQgQAIgQAHIgTAHIAAACIADADIgCABQACAQAAAXQAAAUgBAMIABAIIAegQQAWgLAUgJQAWgKAZgTQAYgSAEgBQgOgqAAgeIABgRQgHADgcAZgAlyhbQgeAUgdARIABAAIABAEQAuATBGAjIB/BEQBmA3BWA2IABgJIALAAIACAGQA3gmAygdQgYgSg7geIh9g/Qg9gdhKgqIhdg1IgFAGQgGgBgGgDQgLALgdAUgAj6iiQgLAIgSAIQgRAIgGAFIBbAwQBGAnA4AbQBpAzAfASQA+AhAaAZIAAAFIABAAIAEAAIAAAAIACAAQAGgLANgJIAigUQAKgGAcgcQAVgWAMAAQhJgbgngQQhDgcgzgfQgvgfg/giIhZgvIgBAAQgCADgDgBQgEAAAAgJIgJgEQgfASgpAcgAE0AaIAFAaQAFAYAAANIAAAKQANgLAkgOQANgGAVgLQASgKAHgBQgDgUAAgaIABgjIgPAJQgMAFgHAFQg8AngVgCgAhRj1Ig/AiIBfAyQA1AdAuAbIAfAQQApAXAyAfQAYAPAoAKQApAKAQAIQAQgEAagMIAtgaQARgIAWgJQgxghhCgdQhBgcgPgKQhuhJhwg+Qg5AbgaAOg");
	this.shape_3.setTransform(45.1,30.2);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,90.1,60.3);


(lib.column = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#999999").s().p("AhAlRIB4hSIAAFHQACAHAHAOIAAGwIgpAcQgLAHgYAJQgaAJgJAGg");
	this.shape.setTransform(16.8,51.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#757575").s().p("AAEF4QgSgOgMgDIgEgFIAAm5QgLhAACi+QAAgggBgcIA/AuIASL1Iglgag");
	this.shape_1.setTransform(7.9,53.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#AAAAAA").s().p("AhcAXIAAgHIBPgwQAvgeAOgHQAEADAMAGQAMAHAQAfQAAALABABIAAAAIh4BQg");
	this.shape_2.setTransform(13.1,10.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgYHtQgFgBgJgHQgNgKgRgOIgjgiIgEAAQgPAAgGgCIgDgDIAAm/QgHgOgBgHIAAlHIgBgIIAAgHQB/heA9gLIADAHQAQAAAUAWQASATAEARQALAhgBABQABAgAAAjQgCC+ALBAIAAG5IAJAOIgDALIAGgEQgXAogZAHQgaAHAbgGQgaAUgoASQgfAQgLABIgJACIgBgBgAgYmjIhPAxIABAIQABAcgBAgQgBC+AKBAIAAG5IAEAFQANADAUAOIAkAaQAJgFAYgJQAagKALgHIApgbIAAmwQgHgOgCgHIAAlHIAAgBQgBAAAAgLQgQgggMgGQgMgGgEgDQgOAGgvAfg");
	this.shape_3.setTransform(14.2,49.4);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,28.4,98.8);


(lib.Building = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A8AA9D").s().p("EAk6AVrQglgiguAAIgZABQghgWhmg2Qhlg1gXgHQgEgcgEgGIAAhKIGiDxQgFAlgCAkQgQgUgUgRgAQTKGQiZheqGlsIrimdQgmgYgWgMQgPgIgMgGQgEgUgEhSIaZPOIgGATIgGASQgDALgEAZIgmgYgA1argIhKgrIigheIj1iRIkzi1QidhchbgqQACgTAAgVIAAgyIQJJUIgCBFIADAPQABAHgCAAIgBAAg");
	this.shape.setTransform(245.5,191.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7D7F75").s().p("AMZNsIABgMINpn4IgDBYQlUCyneEYIg8AkIAHhCgA6BofIABgiIJ3lsIAuAaQgFAPAAAWQAAAaABAGQi2BrjzB6IjJBlIgxAaIABg1g");
	this.shape_1.setTransform(278.4,165.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666860").s().p("AMnMNQABhAAAkiQADjSgDhnIADidQgBhIgJguQA1AaCPBKQClBTCNBLQAuAYC5BmQBcAzAXAFQAAAZAIBbQAJBiAAAfIAAALItpH4QALh2ADiMgA55ssQANgpAIhUQAGhAAKglQDOBXB0A1QCOBBB6BDIp3FsIAImag");
	this.shape_2.setTransform(278.5,147.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#8C8E83").s().p("Ae0VcIAAkzQAemsAJizIACgaQACg5AAgZIAAgNQAAhqgCgaQgEg8gPgrQALgFgIgLIgLgFIgGgMQgFACgEADQhDgghfgyIjMhyQirhdhkgzQifhRhDgTIgDgKQAAACgGADIgEACIgHgCIAATHIgEAII6YvOQgEhXgFicIgIkkQgIglgDhyIgCgoIgDggQgEgXgFgTIACgCQAEgFACgGIgOgIIgDgHIgEADIgigUQi9hth2g5QiehLiRgoIgDgIIgKAFIgSgFIAAAVIABAAQgVAcgEA7IgEBLQgEAqgMAXIgHGzIwJpUIAAjbQgEiFgVhjQF5AfFFAuQBlAPG3BHQH8BSJkA3QDcATDrAQMAgWAAAIAAT8QgEAmgEATIAAIqIgJAzIAALZQgaB0gMBrg");
	this.shape_3.setTransform(247,165.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("EAk0AaPIgLgkQgRgihIgPQgDACgMAAQgJAAh2g+QiFhGgogpIAAgUIAGAAQgKgkgDgyIgBgaIgBhiQAAhIANibQANibAAhOQAFhhAGivIABgrQibBTk3C1QjNB3hsA+IhgA2QgMAHgEgFIAAgFQgBAAAAABQAAAAgBAAQAAAAAAAAQgBAAAAAAQgCgBAAgEIgBAAIhRgvImyj9Qogk9hyg/QmJjTg1gbQgRgDgfgUQgSgLgMgKQgNAAgEgBIgDgCIgDiPIgOn2QgJgzgIh3QgjAdhEAkIjRBqQidBQhzA+IgEACQhYAxgJAHIgDgBIgIACIgBAAIgEAAQgUAAhng7Qhkg5iwhuQl0jsgmgVQishchIg3IgIADIgEgLIgNgMIAMAFQgMgvgHg5QgGgqgCguQgCgygBigQAAhBACgZQACgYAEgSIgDgBQgEgDgBgOIABgOIADgGQFUAeEyAvQC1AcFRA7QIeBbLYA5QC8APDKANMAg3AACQAOAFAEAMIgHARIgCAAQAKA2AABNQgDB2ACA4QACBsgCDfQgBHZgIDtIAAIkQgDANgGAmIAALZIgeC5IgIAzQgPBUgNA6IgLADQgQgOgHgXgEAklAYzQAUARAQAVQACglAFglQALhrAah0IAArZIAJgzIAAooQAFgTADgoIAAz8MggWAAAQjrgQjcgTQpkg3n8hSQm3hHhkgPQlGgul5gfQAVBjAFCFIAADbIAAAzQAAAUgCATQBbAqCdBcIEzC1ID1CRICgBeIBKArQADACgBgJIgDgPIABhFIAHmzQAMgXAEgqIAEhLQAEg7AVgcIgBAAIAAgVIASAFIAKgFIAEAIQCQAoCeBLQB2A5C9BtIAiAUIAEgDIADAHIAOAIQgCAGgEAFIgBACQAFATADAXIADAgIACAoQADByAIAlIAIEkQAFCcAEBXQAEBRADAVQANAFAPAIQAWAMAmAYILiGeQKGFrCZBeIAmAYQAEgZADgLIAFgSIAHgSIADgIIAAzHIAHACIAEgCQAGgDAAgCIAEAKQBDATCfBRQBkAzCqBdIDMByQBgA0BDAeQADgDAGgCIAFAMIALAFQAIALgLAFQAPArAEA8QACAaAABqIAAANQAAAZgCA5IgCAaQgJCzgeGsIAAEzIAABKQAEAHAEAbQAXAIBkA1QBmA1AiAWIAZAAQAtAAAmAhgARgkbIgDCgQADBngDDPQAAEigCBAQgDCNgKB1IgCAMIgGBCIA8gjQHekZFUiyIADhYIAAgKQAAgggJhiQgIhaAAgZQgXgFhcgxQi6hngtgYQiOhLilhUQiOhKg2gaQAJAuACBHgA0wyZQgHBVgOApIgHGaIgBAiIgBA0IAwgZIDKhmQDyh5C3hrQgBgGAAgbQAAgWAFgPIgvgaQh6hCiNhBQh0g2jPhWQgKAlgGA/g");
	this.shape_4.setTransform(247.6,171.7);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,495.2,343.4);


(lib.btn_stop = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4D2A3E").s().p("AAcCOIAAkbIB3AAIAAEbgAiSCOIAAkbIB3AAIAAEbg");
	this.shape.setTransform(15.5,20.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AlQEkIAApIIKgAAIAAJIg");
	this.shape_1.setTransform(16.8,24.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0.8,6,29.5,28.5);


(lib.bricks = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#606082").s().p("AB4CkIAAgIIAAglIA4AaIgCAkIAAAIgABCCJQgMgIgGgCIAIgFIAAggIAiAPIgBAQIgCAVIAAAGQgIgCgNgJgAgtBCIAAgIIAAg0IAOAHQAOAHAKAHIADA2IAAAEgAiPAQIgBAAIABgJIADgpIgagMIgJgDIACgkIAHADIAeANIgEAjIA8AZIgDAuIgBALQgzgcgIgEgAiAibIABgbIAAgFQAJABAqAaIAAAgQgdgRgXgKg");
	this.shape.setTransform(16.9,44.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#896755").s().p("ACtDEIAAgIIACgkIAkAQQABAhADAOgAAxCLIAAgGIADgVIABgQIAZALIAFACIgBAmIABAHgAh5AnIAAAAIAAgMIAEgtIAjARIAAAyIAAAHgAjWgJIACgyIAIAEIAbALIgDAsIgBAHQgagNgHgDgAjLhbIgIgDIADgpIAGADIAgATIgCASIgCAQIgdgMgAg8hpIgSgLIgTgMIgQgJIAAghIAQAIQAKAFAHAEIAFAEIAfAVIABAAIgBAEIAAAcIgQgJgAjGiwIgIgCIAAgIIACgOQADgMgBgCIATAJIATAIIAAAGIgCAbQgRgIgPgEg");
	this.shape_1.setTransform(20.6,45.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#8C8C8C").s().p("ACaCqIAAgHIACglIAkARIgCAlIAAAHgAgYBDIAAgEIgDg3IAIAGIATAPIAEAFIAAAAQADAaAFAYgAgbhoIAAgdIABgEQAXAPADACIAAAFQAAAMAAAOIgbgPgAi5iMIgGgEIACgqIAIACQAPADASAIIgFAzIgggSg");
	this.shape_2.setTransform(18.9,46.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#999999").s().p("AiIBmIA/gmIgBAiIgXALQgbANgMAKgAgQgFIAcgTIAZgPIAGgDIAAADQgBAJAAAdIAAABIgMAFIgsAZIgCgjgABJgLIgCgaIAGgDIA6gbIACAgIAAAQIAAAAIgHAEIg4AeQABgHgCgTgABGhzIAHgBQAWgFAcgKIADAcIg1AXIgGADg");
	this.shape_3.setTransform(58.6,52.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#9D7663").s().p("AhqByIgBgQIAugZIANgHQAAAfADADQgVAHgmAVIgCgOgAhtA2IgCgeIA9gjIAFgFIgBARQABAMgCALIgHADIgZAQIgeATIAAgIgAAsAfIgCgfIBGgfIAAAfIgBARQgdALgmATIAAgQgAgUAaIAAgiIAHgDIAzgYIAAACIAEAhIg3AaIgHADIAAgDgAgUhQIAAgLQACgHgCgCIAxgbIACAaIgtAVIgGADg");
	this.shape_4.setTransform(67.8,45.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#6F6F96").s().p("AijBlIA7gjIAAAQIACAOIgCAAQgZAOgiAXIgBACIABgigAhugiIAAgPIABgcQApgRAWgNQAAAEADAJQACAJAAAEIAAACIgBAfIgGAFIg9AlIgBgcgABygeIAAghQAegNAOgFQACAbAFAQQgHABgQAIQgUANgIADIgBAAIABgRgAgShtIAGgDIAtgVIAEAnQgcAJgUAGIgHABg");
	this.shape_5.setTransform(67.6,48.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#B58872").s().p("ABDDCIgXgPQAegUAZgTIALgIIAkAUIAIAFIgIAEQgLAEgRAJIgYANQgGACgOAKIgHgFgAhoCrIgggTIgDgCIAQgHIBHgoIALAIIASAPIAfAaIg2AmIgLAHgADaB2IgbgTIAHgEIA0geIAbATQguAXgNAOgAkZBLIgogWIAHgEIA2gfIACACIAaAVIAZAVIAGAEIgrAYIgGADIgfgSgACiBNIgggXIAGgEIArgfIAmAZIguAeIgHAEIgCgBgAgZA0IgLgGIAsgaIAWAIIAYANIgxAdIgegSgAiBAYIBHgpIAGgEQAUALAXAKIgtAVQggAPgOAIIgdgUgAFkAkIgfgRIAzgeQAPAFAJAGIAIACQAGADACgBIgBACQgCAAgIAIQgIAKgKAGIgVALIgKgFgAArAGIA8gdIAJAEIAjATIADAAQggAUgUAGIgLACQgXgNgVgJgAmfAEIAqgPIAxgTQAQAHAPANIhDAhIgIAEQgngVgIgCgAjRgYQgNgIgMgJIAFgDIA4gbIAUgKIABAAIAZAPIgOALIgSANIglAaIgNgIgAAzglQgOgFgWgMQAegTAhgXIArAZIAKAGIgKAFQglAQgXAMQgGgEgEgBgAhIg+IgKgFIgGgDIAGgDIAbgZIAVAPIAUAMIgdAUIgEADIgZgOgAinh1IAngaIAEADIAkAWQgZAPgVAIIghgWgAARiIIgLgHIgdgUQAfgXAPgIIAFgEIAdAMIAFACIACABQAPAGALAHIAGAEIAFACIgFAEQgMAIgVATIgSAQIgcgTg");
	this.shape_6.setTransform(42.4,21.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#B7B7B7").s().p("Ag3DDIAMgHIA1gnQAVARAQALIg1AjIgJAGgAi7B3QATgFBEghIACgBIAOAEQAVAJAOAIIhHAnIgQAHIgzgcgAjyBeIgFgDIAFgDIAsgYQAQALAOAHIgtAZQAAgBAAAAQgBAAAAAAQgBgBAAAAQgBAAgBABQAAAAgBAAQgBAAAAAAQgBABAAAAQgBAAAAABIgVgOgADABfIgZgTIAHgEIAugfIAgAWIgzAdIgHAFgAicACIgJgEIAegSIAngVIAKgIIAGAEIAOAKIALAIIAGADIgGAEIhHApIgegTgACWgDIgkgTIgJgDIBBgjIABgBIAFACIAfAQIgZATQgQANgOAKgAAOgIQgEgGgdgUIAEgCIAggUQAXAMAOAEQAEABAGAFIgKAFIgmATIgBACIgBAAgAj7g+QAUgIAagQQAWgNACAAQAHAAAYAPIgUALIg4AbIgFADQgLgJgJgKgAB8hJIgrgZIALgIQAdgWAQgHIAMgFQAqAiAHACQgMAFgaAQIgbAQIgJgGgAiAhgIgDgBQAVgIAZgQIAgAVIgbAYIgFAEIgrgYgAgwi3IgIgFIAIgEIAbgMQARgHAHgHQAMALAKAEIAGADIgGAEQgOAIgfAWIgcgRg");
	this.shape_7.setTransform(42.1,21.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgeFdIgugXIgegPQgpgUgdgWQgQgMgZgZIgBAGQgaABhYg1IhyhEIAAgRIAJABQgEgOgCgVIgBg3QAAgXACgsQADguACgFIAAgCIAAgEIABgIQABgBACgFIAGgBIgBgGQC8hWArgVIAHADQABgJADgDIAIgEIAAgCQAAgNBTgwQBTgvAYAAIACAEQAWADBTAwQAyAdAyAjIAAgDQAkAKBHAkQBKAlAkAcQAFAEgNAIIAFADIACAAIAGALIAGDfQgFAFgIAEQgjAmiYA9QgeAhhAAjIh5A+QACAJgLADIgGABIgBAAIgMAAQgEAAgegPgAjGAnIAAARQABAeAFANIACAEIAAAuIAAAxIAAAgIgIAFQAGACAMAIQANAJAIADIAhAPIA2AZIAkAQIAoAUQgDgOgBgiIAAgLIAAg5IAAgNIAAgcIAAgIIADgYIADgOIgGAAIgIgFIgWgLIgogVIgggRIgJgFIgvgaIgogXgACUBFIgMAFQgUAIgUANQgOAJgOANIglAgIAAAXIAAAqIAAAkIAAAkIAAAfQALgKAbgNIAYgLIABgBQAigYAZgNIACAAQAmgVAVgHQgDgEAAgeIAAgBQgBgdACgJIAAgDQACgLAAgMIABgTIABggIAAgBQAAgFgCgJQgDgIAAgEQgWAMgpARgAmfhSIgCAOIAAAIIgCAqIgCAoIgCAjIgCAyQAIADAZAOIABAAQAIAEAzAdIABAAIAmARIApATIAmASQgFgZgDgaIAAAAQgEgYgBgcIAAAAQgBgSAAgUIAAgBIgBgaIABgHIAAAAQAAgPACgLIAAgGQgFgCgXgMIgBgBIgegUIgGgFQgGgEgLgFIgQgHQgqgagJgBIgTgIIgTgJQABACgDAMgAFsgvIgRAIIg5AdIgyAZQABACgCAHIAAALIAAADIAAAeIABAmIAAAkIAAAEIABAaQACAVgBAHIA5ggIAGgEIABAAQAlgUAdgLIABAAQAIgDAUgMQAQgJAHAAQgFgRgCgaQgBgJAAghIAAgTIAAgFIABglIAAgUQABgMACgJQgXANghASgAARBrQAMACACAEIAAAFIAJgHQAKgJAKgNQAEgEAPgLIADgBQANgKAGgDIAYgMQARgJAMgFIAIgDIARgLQARgMAKgDQgOgGgjgYIghgWIgRgMIgUgMIgUgKIgGgCIgYgNIgWgIIgsAaIgBAAIg+AjIgBABIgCABQhEAigUAFIAzAaIADABIAhAUIAuAaIAnAYIABAAIAPAMIADgBIAJABgADfjbQgBAEgOAKIggAUIgCABIhAAjIg7AfQAUAJAXANIACABQATAKAUANIACACIAgAXIACABIAaATIACACIAaATIAAADQANgOAugXIABAAIABgBQARgJAdgNIApgUIAVgLQALgGAIgKQAHgIACAAIABgCQgBABgHgDIgIgCQgJgIgPgFIgBgBQgRgHgNgJIgOgJQgUgMgXgMQgSgJgUgJIgggOIAAgCgAjIjSQgbAQgUAIIgEACIhCAcIgxATIgqARQAIACAnAVIAJAFIAnAVIAnAWIAfASIAGADIAVAOQAAgBAAAAQABAAAAgBQABAAAAAAQABAAABAAQABgBAAAAQABAAAAABQABAAAAAAQABAAAAABIAsgZIBHgoQAOgIAggPIAtgXQgXgKgUgLIgGgEIgLgIIgOgKIgFgDIgKgFQgRgLgJgCIgDgCIgYgPIgBgBQgYgOgIgBQgCAAgVANgAgRlIIgaAMIgIAEIgQAHQgPAGgjAbIgEADIgnAaIAiAWIACACIArAXIAGADIALAFIAYAOIAEACIAWAOQAdAVAEAFQAAAAAAAAQABAAAAAAQAAgBAAAAQABAAAAgBIAlgTIALgFQAXgMAkgQIAKgFIAbgQQAagPAMgFQgHgDgrghIAAgBIgLgIQgJgHgMgFIgNgGIgFgCIgGgEQgLgHgPgGIgBgBIgGgCIgcgMIgGgCQgLgFgMgLQgHAHgRAHg");
	this.shape_8.setTransform(41.7,33.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#7C7DA8").s().p("AAZDSQgBgEgMgBQgMgCAAACIgQgNIAAAAIAJgGIA1gjIAXAOIAHAFIgCACQgQALgDAEQgLANgJAIIgKAHgACUB7IgkgUQAXgRAQgPQAjAYAPAHQgLAEgRAMIgRAKIgIgFgAhVA6IgOgFIACgBIA+giIABgBIALAGIAdASIg4AiQgOgJgVgIgAD7AlIBMgtIAfAPIAJAGIgoAUQgdANgSAJIAAAAIgBABgACBAYQgUgNgTgLIgCAAIALgBQAVgFAfgVIAeATIgrAdIgHAFgAlmADIgIgDIAIgEIBDgjIAFAEIAcAaIg2AeIgHAEgAjCgtIAlgZIASgNIAOgLIACABQAJADARAKIAKAFIgKAHIgnAWIgeASIgcgRgAgphKIgFgDIAFgDIAdgUIAcASIggAUIgEACIgVgOgACvhVIgFgDIAggUQAOgJACgFIACgBIAAACIAgAOIguAmIgfgQgAAviSIASgPQAVgTAMgJIAFgDIAMAFQAMAGAJAGIALAJIABAAIgMAFQgQAHgdAVIgLAIIghgVgAh6ipIgFgCIAFgEQAigaAQgGIAPgHIAIAFIAcARIgRANQgcAUgUAMIgkgWg");
	this.shape_9.setTransform(42.2,24);

	// Layer 1
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#8C8EA8").s().p("AgtAJIA8ghIAfASIg4AfQgOgIgVgIg");
	this.shape_10.setTransform(38.3,28.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#8C8C8C").s().p("ACFClIACgkIAkARIgCAkIgkgRgACuBpIACgoIAfAQIAAAMIAAAdIghgRgAAPBKIAAgxIAXAJIALAFIABArIgBAIIgigQgAgtBBIgDg2IAIAGIATAPIAGAFQADAbAFAXIgmgWgABTA5IAEgnIAxAbIABANIgBAbIg1gcgACHAFIAAgiIAoAWIABAjIgpgXgAhIgBIgOgGIAAgoIghgNIABghIAhANIADgqIASALIAQAJIAAgcQAQAKANAGIAAACQgCANAAANIgBAIIABAZQgHgGgUgIIgTgHIgNgEIgFgCIgBASIAAAPIAQAHIAHAEIAOAHIABAoQgKgGgOgGgAANgXQgFgNgBgeIAAgRIAAgRIAoAYIABAhIAAApIgjgVgAi1gyIAEgjIA6AZIgCAXIAAANIg8gagAh3g8IAAAAgAjOiKIACgcIACgQQAPAEASAIIgFAzIgggTg");
	this.shape_11.setTransform(21,46.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#896756").s().p("ACoC5IACglIAkAQQABAYAEARIgrgUgAAtCBIACgVIACgQIAZALIAEADIAAAlIghgOgABOBqIAEguIAAgBIgigRIgLgGIgXgJIAAgsIgBgEIAjAVIALAFIAbAPIgEAnIA2AcIgCAsIg4gZgACIBXIAAgbIAAgNIgCgnIAqAWIAAAlIgCApIgmgVgACIBXIAAAAgAh4AaIgFgDIADgtIABgNIABgXIg6gZIgegMIAAgSIABgWIAhASQAbAPAcAMIgBAgIAiAOIgBAnIAAAyIghgTgAgWAiIgSgPIgJgGIAAgoIAcAPQACAaADAZIgGgFgABagQIgGgDIglgWIAAghIAuAaIAKAFIAfARIAAAiIgsgYgAjSgMIACgvIAaALIgDAsIgZgIgAhAgiIgHgDIgPgHIAAgQIABgSIAEACIANAEIATAHIAAAjIAAADIgPgHgAhBhtIgSgLIgSgMIgRgJIAAghQAQAKARAMIAFAEIASAMIANAIIABAdIgRgKgAjLizIABgKIgBgPQAFAEAPACIAPADIgCAbQgSgIgPgDg");
	this.shape_12.setTransform(21.1,45.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#606082").s().p("ABsCrIgfgOIAAglIgFgDIgZgLIAAgIIgBgqIAiARIAAABIgDAuIA4AZIgCAlIgXgLgACpCiIgkgRIABgsIAmAVIAhARIAAAbIAAAMIgkgQgAAFCAIAIgFIAAghIAiAQIgCAQIgCAVIgmgPgACuBRIAAglIgBglIAWALIAKAFIAGAAIgDANIgDAYIAAAIIAAAdIgfgQgAhNBBIgMgGIAAg0IAPAHQAOAHAJAGIAEA3IgegRgABVAiIgbgPIgMgHIgBgnIAlAWIAGADIAtAYIABAnIgxgbgAi7AIIAEgqIA7AaIgCAtQgigSgbgLgAgzgNIAAgDIAAgjQAVAIAGAGIAAACQAAATACAQIgdgNgAjRgtIAAgkIAdAMIgDAjIgagLgAh4hMQgdgMgbgPIAFgzIABgbIAOAEQASAGATALIAAAhIAQAJIATAMIgDAqIghgMg");
	this.shape_13.setTransform(21.3,44.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#B7B7B7").s().p("AhqC8IA3gnQAUARAQALIg2AjIglgYgAjhB1QAegIAtgYIACgBQAWAJANAIIhGAnIgqgXgAAJBpQAdgVASgKIAhAXQgQAPgXARIgpgYgAhlBtIgMgIIA7giIAxgdIAFADIATAKIgsAiIgOAJQgUAPgYAPIgSgPgAkaBkIgCADQgGgFgPgKIAsgYIBBgnIAHgEIgegTIgJgEIAegSIAngVQAQAGANAJIAMAGIhHApIAcAVIhHAoIgsAZIgGgHgAhxBlIAAAAgABuBIIAugfIAhAWIgzAeQgNgLgPgKgADegEIgpgUQAMgJASgRIAQgNQAXAMAUAMIAPAJQAMAJASAHIAAAAIgzAfIgqgVgABXgDIgkgTIA5giIAEgDIAgAQIgZAUQgQAMgOAKgAg4gOQgIgLgPgLIAjgUQAXAMAOAEIgvAYIgBACIgBAAgAk6g+QA0gVAJgKQAGAKAOAKIg4AbIgGADQgLgJgIgKgAhKhKQAjgXAagXIAfAWIAKgIQAdgWARgHIALAIQAdAXAJADIg+AcIgrgZQgfAWgfAUIgegSgAjAhkQAOgDAdgSIAhAVIgbAYIgxgYgAhvi3QAXgNAYgQQAEAFAYALQgPAHggAXIgcgRg");
	this.shape_14.setTransform(48.5,21.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#7C7DA8").s().p("AAaDQQgCgFgHgEQgIgEAAABIgPgMIA0gkQgQgLgUgQIgegaQAVgOAVgPIAOgKIALAHIAYAOIAqAYQAWgSARgPQAjAYAOAIIg0AVIgkgUIgLAIQgZATgfAUIAXAPIgOAPQgZAggEADgAD8AiIBLgsIAfAPIgpAUQgbALgUAKgABkgDQAVgGAfgVIAdAUIALgJQAYgSAWgNIAGgDIAoAUQggAaghAUIgmgXIgqAdQgVgOgSgIgAllgGIBDgjIAKgGIAwgZQAMAJANAIIANAIIAmgaIASgNIAQAGIAaAKIgoAVIgdATIgUANQgaARgTAKIgZgTIgCgCIg2AdIgugYgAgphSIAegUIgUgMIgWgPQARgOARgMIAbgTIALAHIAdATIASgQQAVgTALgIQAQAKAXARQgRAHgdAWIgLAIIgggVQgbAWggAYIAcARIghAUIgZgRgACvhYQAogZADgLIACgCIAAACIAgAOIgtAmIgggQgAh6irQAZgTAZgLIAYgLIAcASIgSAMQgbAUgVANIgkgWg");
	this.shape_15.setTransform(42.2,24.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#999999").s().p("Ai/CAIAAgkIBBgnIgCAmIgCATQghAYgcAZgAhHAUIAegTIAZgNQgDANgGAPQgCAGADAKIgtAYIgCgkgAAWgOIA5gaIACAgIgGADQgjAQgWAOQADgSABgVgAh9AOIAAgrIAigVIAQgJIAAAPIABAcIgJAGIgqAZIAAgBgABLhKIAAgCIBJgjIgCgZIAugWIAAAHIAAASQgNAEgQAKIgPAIIABAiIAAAFIhGAgIgEgigAAWhZQAWgFAcgKIADAcIg1AXIAAgkg");
	this.shape_16.setTransform(64.1,49.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#9E7663").s().p("AhHCLIAtgZQADAIAGAJQgYAIgeAQIAAgQgAjBBgIAogdIAagQIAAAtIAAACIhCApgAh/BiIArgbIAJgGIA8glIgBAOQAAAOgCANIgZAPIgeATIg3AhIABgmgABNApIg5AaIABghIAAgGIA1gXIAAACIADAiIBHggIgBgFIgBggIAPgJQARgJANgEQAAAhABAJQgOAFgeAMIAAAgQgeALgnAUIgCgfgAiAAUQAVgOATgGIANgEIgBAaIgRAJIgiAUIgBgfgABCg8QAsgVAfgMIAEAoIgbAMIgwAUIgEgngAAOg7IAygbIACAaIgvAWQgCgNgDgIgABCg8IAAAAgACKh7QAigRAWgOQgCAKAAAMIgBATQgSAGggAOIgDgeg");
	this.shape_17.setTransform(64.3,41.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#6F6F96").s().p("AiECLIACgmIhBAnIAAgkIBCgpIgBAmIA3ghIACAkIAAAQQgbAPggAWIgCABIACgTgAjDAmIAlgfQAOgLAOgJIABAeIgaAPIgoAegAhPACIAAgNIABgcQApgRAWgMIgBAbIAAABIgBAjIg9AjIgBgcgACRgYQAegOAOgEQACAVAFARQgUABgfAJIAAgegABEg4QgcAKgWAFIgBgfIAAgBIAvgWIAEAnIAwgUIAagMIACAZIhJAjIgDgcgACLiAQAggOASgGIgBAmIguAWIgDgogAA+h5IA5gcIARgJIADAeQgfANgsAUIgCgagACLiAIAAAAg");
	this.shape_18.setTransform(64.5,44.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#B58972").s().p("AAmC0QAegVAagTIAKgIIAkAUIg0AWQgMAFgPAQIgXgPgAhTCrIgMgGIghgWIBGgnIAMAIIARAPIAfAZIg2AnIgfgUgADTBuIgSgOIAygeIgggWQAhgWAggXIArAVIhMAuIATAMQgmATgNAQgAAnBfIgMgHIAvgiIAUAMIARALQgTAKgdAVIgYgNgAkfBEIgfgRIgBgBIA2ggIACADIAZAUQATgKAagSIAUgNIAJAFIAdATIgHAEIhBAnIgsAYIgkgXgACMA5IgJgGIAqgfIAmAYIguAfIgZgSgAgeA0IAsgZIAKACIAYAMIgwAdIgegSgAiHAYIBHgpIAjAUIhOAqIgcgVgAE/AUIAzgfQAOAGAKAFIAIADQAGACACAAIgBABQgKACgcARIgVALIgfgQgAA4AIIAxgbIAkATIACAAQAOgIARgMIAYgUIAugmQATAJATAJIgQANQgTARgLAJIgGADQgWANgYAQIgLAJIgegUQgfAVgUAFQgUgKgQgIgAmZADQAggNAvgTIBCgcIAEgCQAJAKALAJIgxAYIgJAGIhDAhQgigSgKgCgAE/AUIAAAAgAjXgXQgNgIgMgJIAFgDIA4gbQANAJAUAHIgSANIgmAZIgNgHgAAIg1QAfgUAhgWIArAZQgmARggAQQgNgEgYgMgAhNhDIgMgGIAbgYIAXAOIAUAMIgeAVIgcgRgAheh2QAVgMAbgUIARgNQAfgXAOgHIAJADIAaALQAUAIANAIIAGADQgMAJgUATIgSAPIgdgSIgLgHIgbATQgRAMgSAOIgggVgAilhxIAegYIAFgCIAkAVQgdASgOADIgcgQg");
	this.shape_19.setTransform(43.1,21);

	this.addChild(this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-3,-2.6,89.5,72.9);


(lib.BossAnims = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.Boss_01__000();
	this.instance.setTransform(400.9,0);

	this.instance_1 = new lib.Boss_01__001();
	this.instance_1.setTransform(400.9,0);

	this.instance_2 = new lib.Boss_01__002();
	this.instance_2.setTransform(400.9,0);

	this.instance_3 = new lib.Boss_01__003();
	this.instance_3.setTransform(400.9,0);

	this.instance_4 = new lib.Boss_01__004();
	this.instance_4.setTransform(400.9,0);

	this.instance_5 = new lib.Boss_01__005();
	this.instance_5.setTransform(400.9,0);

	this.instance_6 = new lib.Boss_01__006();
	this.instance_6.setTransform(400.9,0);

	this.instance_7 = new lib.Boss_01__007();
	this.instance_7.setTransform(400.9,0);

	this.instance_8 = new lib.Boss_01__008();
	this.instance_8.setTransform(400.9,0);

	this.instance_9 = new lib.Boss_01__009();
	this.instance_9.setTransform(400.9,0);

	this.instance_10 = new lib.Boss_01__010();
	this.instance_10.setTransform(400.9,0);

	this.instance_11 = new lib.Boss_01__011();
	this.instance_11.setTransform(400.9,0);

	this.instance_12 = new lib.Boss_01__012();
	this.instance_12.setTransform(400.9,0);

	this.instance_13 = new lib.Boss_01__013();
	this.instance_13.setTransform(400.9,0);

	this.instance_14 = new lib.Boss_01__014();
	this.instance_14.setTransform(400.9,0);

	this.instance_15 = new lib.Boss_01__015();
	this.instance_15.setTransform(400.9,0);

	this.instance_16 = new lib.Boss_01__016();
	this.instance_16.setTransform(400.9,0);

	this.instance_17 = new lib.Boss_01__017();
	this.instance_17.setTransform(400.9,0);

	this.instance_18 = new lib.Boss_01__018();
	this.instance_18.setTransform(400.9,0);

	this.instance_19 = new lib.Boss_01__019();
	this.instance_19.setTransform(400.9,0);

	this.instance_20 = new lib.Boss_01__020();
	this.instance_20.setTransform(400.9,0);

	this.instance_21 = new lib.Boss_01__021();
	this.instance_21.setTransform(400.9,0);

	this.instance_22 = new lib.Boss_01__022();
	this.instance_22.setTransform(400.9,0);

	this.instance_23 = new lib.Boss_01__023();
	this.instance_23.setTransform(400.9,0);

	this.instance_24 = new lib.Boss_01__024();
	this.instance_24.setTransform(400.9,0);

	this.instance_25 = new lib.Boss_01__025();
	this.instance_25.setTransform(400.9,0);

	this.instance_26 = new lib.Boss_01__026();
	this.instance_26.setTransform(400.9,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(400.9,0,400,472);


(lib.Boss_Bubble = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("EAltgRNMhCNAAAQiOAAAACOIAANtIpMFPIJMCZIAAIqQAACOCOAAMBCNAAAQCOAAAAiOIAA9/QAAiOiOAAg");
	this.shape.setTransform(77.3,85.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CCFF").s().p("A8gROQiOAAAAiOIAAoqIpMiZIJMlPIAAttQAAiOCOAAMBCNAAAQCOAAAACOIAAd/QAACOiOAAg");
	this.shape_1.setTransform(77.3,85.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(4,1,1).p("EAltgUVMhCNAAAQiOAAAACOIAAT7IpMFRIJMCZIAAIqQAACOCOAAMBCNAAAQCOAAAAiOMAAAgkPQAAiOiOAAg");
	this.shape_2.setTransform(77.3,65.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#66CCFF").s().p("A8gUWQiOAAAAiOIAAoqIpMiZIJMlRIAAz7QAAiOCOAAMBCNAAAQCOAAAACOMAAAAkPQAACOiOAAg");
	this.shape_3.setTransform(77.3,65.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},2).wait(1));

	// Layer 5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(0,0,0,0.502)").s().p("A8gQ/QiOAAAAiOIAAoqIpMiZIJMlPIAAtPQAAiOCOAAMBCNAAAQCOAAAACOIAAdhQAACOiOAAg");
	this.shape_4.setTransform(87.3,97.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(0,0,0,0.502)").s().p("A8gUHQiOAAAAiOIAAoqIpMiZIJMlRIAAzdQAAiOCOAAMBCNAAAQCOAAAACOMAAAAjxQAACOiOAAg");
	this.shape_5.setTransform(87.3,77.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4}]}).to({state:[{t:this.shape_5}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-180.2,-26.4,523.1,232.5);


(lib.blanksquare_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AxPSdMAAAgk5MAifAAAMAAAAk5g");
	this.shape.setTransform(46.6,35.7,1,0.898);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.blank_mc = function() {
	this.initialize();

}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,2,2);


(lib.BHalf2copy2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#034E75").s().p("AAWAnQgcgRgJAAQgNAAgFAFQgGAEgGAAIgDAAIgDAAQgMAAgFgJQgCgEAAgFQAAgIACgFQADgLAIgIIABgBQACgGAFgDQAMgSAhgGQASgDAPAFIAGADQAHAEADAHIABAGQAAAGgDAFIgBADQAGAGAAAJQAAAGgCAFIAEACQAQAKAEAIQABADAAADQAAAFgDAGQgEAJgLAAIgfgQg");
	this.shape.setTransform(15.1,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A88541").s().p("AAJAjQgPAAgNgHQgYAAgKgQIgDgFIAAgCQgDgFAAgHQAAgVAVgGQAGgBAFABIACgBQAJgFAKAFQgCAEAAAIIABALQABAKAFAAQABAAAAAAQABAAAAgBQABAAAAAAQAAAAAAgBIgCgTIAAAAQAIAHASABQACALAHAIIAEAGIALAJIAHAFIABABIACABIgBABQgHALgZAAQgKAAgIgDg");
	this.shape_1.setTransform(7.5,12.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhPBKQgTgKgDgIQgGgHgCgIQgFgDgCgEIgCgLQAAgZAogFQATgCAKAFIAAgHQAAgTALgOQAGgJALgHQAJgPAdgIQAYgGALAFQAZgCAAAVIgBAGQgCgIgIgEIgFgCQgPgGgVAEQgfAFgMASQgFAEgCAGIAAABQgJAHgDAMQgCAGAAAIQAAAFADADQAFAIAMAAIACAAIADAAQAHAAAFgEQADgEANAAQAKAAAdAQIAfAQQALAAAFgJQADgFAAgGQAAgDgCgCQASAKAAAGQAAASgZAAQgKAAgYgNQgZgNgaAAIgLADQgKADgEAAIgBAAQgSgBgIgHIAAAAIACATQAAABAAAAQAAAAAAAAQgBAAgBABQgBAAgCAAQgFAAgBgKIgBgLQAAgIACgEQgKgDgJADIgCABQgFgBgFABQgWAGAAAVQAAAHADAHIABACIACAFQALAQAXAAQANAHARAAQAIADAKAAQAYAAAGgLIABgBIgBgBIANAHQgCAEgFADQgNALgaAAQgjAAgZgOg");
	this.shape_2.setTransform(11.9,8.8);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,23.9,17.7);


(lib.BHalf2copy = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#65301B").s().p("AAJAjQgPAAgNgHQgYAAgKgQIgDgFIAAgCQgDgFAAgHQAAgVAVgGQAGgBAFABIACgBQAJgFAKAFQgCAEAAAIIABALQABAKAFAAQABAAAAAAQABAAAAgBQABAAAAAAQAAAAAAgBIgCgTIAAAAQAIAHASABQACALAHAIIAEAGIALAJIAHAFIABABIACABIgBABQgHALgZAAQgKAAgIgDg");
	this.shape.setTransform(7.5,12.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#494949").s().p("AAWAnQgcgRgJAAQgNAAgFAFQgGAEgGAAIgDAAIgDAAQgMAAgFgJQgCgEAAgFQAAgIACgFQADgLAIgIIABgBQACgGAFgDQAMgSAhgGQASgDAPAFIAGADQAHAEADAHIABAGQAAAGgDAFIgBADQAGAGAAAJQAAAGgCAFIAEACQAQAKAEAIQABADAAADQAAAFgDAGQgEAJgLAAIgfgQg");
	this.shape_1.setTransform(15.1,6.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhPBKQgTgKgDgIQgGgHgCgIQgFgDgCgEIgCgLQAAgZAogFQATgCAKAFIAAgHQAAgTALgOQAGgJALgHQAJgPAdgIQAYgGALAFQAZgCAAAVIgBAGQgCgIgIgEIgFgCQgPgGgVAEQgfAFgMASQgFAEgCAGIAAABQgJAHgDAMQgCAGAAAIQAAAFADADQAFAIAMAAIACAAIADAAQAHAAAFgEQADgEANAAQAKAAAdAQIAfAQQALAAAFgJQADgFAAgGQAAgDgCgCQASAKAAAGQAAASgZAAQgKAAgYgNQgZgNgaAAIgLADQgKADgEAAIgBAAQgSgBgIgHIAAAAIACATQAAABAAAAQAAAAAAAAQgBAAgBABQgBAAgCAAQgFAAgBgKIgBgLQAAgIACgEQgKgDgJADIgCABQgFgBgFABQgWAGAAAVQAAAHADAHIABACIACAFQALAQAXAAQANAHARAAQAIADAKAAQAYAAAGgLIABgBIgBgBIANAHQgCAEgFADQgNALgaAAQgjAAgZgOg");
	this.shape_2.setTransform(11.9,8.8);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,23.9,17.7);


(lib.BHalf2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2B4A60").s().p("AAWAnQgcgRgJAAQgNAAgFAFQgGAEgGAAIgDAAIgDAAQgMAAgFgJQgCgEAAgFQAAgIACgFQADgLAIgIIABgBQACgGAFgDQAMgSAhgGQASgDAPAFIAGADQAHAEADAHIABAGQAAAGgDAFIgBADQAGAGAAAJQAAAGgCAFIAEACQAQAKAEAIQABADAAADQAAAFgDAGQgEAJgLAAIgfgQg");
	this.shape.setTransform(15.1,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A88541").s().p("AAJAjQgPAAgNgHQgYAAgKgQIgDgFIAAgCQgDgFAAgHQAAgVAVgGQAGgBAFABIACgBQAJgFAKAFQgCAEAAAIIABALQABAKAFAAQABAAAAAAQABAAAAgBQABAAAAAAQAAAAAAgBIgCgTIAAAAQAIAHASABQACALAHAIIAEAGIALAJIAHAFIABABIACABIgBABQgHALgZAAQgKAAgIgDg");
	this.shape_1.setTransform(7.5,12.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhPBKQgTgKgDgIQgGgHgCgIQgFgDgCgEIgCgLQAAgZAogFQATgCAKAFIAAgHQAAgTALgOQAGgJALgHQAJgPAdgIQAYgGALAFQAZgCAAAVIgBAGQgCgIgIgEIgFgCQgPgGgVAEQgfAFgMASQgFAEgCAGIAAABQgJAHgDAMQgCAGAAAIQAAAFADADQAFAIAMAAIACAAIADAAQAHAAAFgEQADgEANAAQAKAAAdAQIAfAQQALAAAFgJQADgFAAgGQAAgDgCgCQASAKAAAGQAAASgZAAQgKAAgYgNQgZgNgaAAIgLADQgKADgEAAIgBAAQgSgBgIgHIAAAAIACATQAAABAAAAQAAAAAAAAQgBAAgBABQgBAAgCAAQgFAAgBgKIgBgLQAAgIACgEQgKgDgJADIgCABQgFgBgFABQgWAGAAAVQAAAHADAHIABACIACAFQALAQAXAAQANAHARAAQAIADAKAAQAYAAAGgLIABgBIgBgBIANAHQgCAEgFADQgNALgaAAQgjAAgZgOg");
	this.shape_2.setTransform(11.9,8.8);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,23.9,17.7);


(lib.BHalf = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#65301B").s().p("AAJAjQgPAAgNgHQgYAAgKgQIgDgFIAAgCQgDgFAAgHQAAgVAVgGQAGgBAFABIACgBQAJgFAKAFQgCAEAAAIIABALQABAKAFAAQABAAAAAAQABAAAAgBQABAAAAAAQAAAAAAgBIgCgTIAAAAQAIAHASABQACALAHAIIAEAGIALAJIAHAFIABABIACABIgBABQgHALgZAAQgKAAgIgDg");
	this.shape.setTransform(7.5,12.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#494949").s().p("AAWAnQgcgRgJAAQgNAAgFAFQgGAEgGAAIgDAAIgDAAQgMAAgFgJQgCgEAAgFQAAgIACgFQADgLAIgIIABgBQACgGAFgDQAMgSAhgGQASgDAPAFIAGADQAHAEADAHIABAGQAAAGgDAFIgBADQAGAGAAAJQAAAGgCAFIAEACQAQAKAEAIQABADAAADQAAAFgDAGQgEAJgLAAIgfgQg");
	this.shape_1.setTransform(15.1,6.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhPBKQgTgKgDgIQgGgHgCgIQgFgDgCgEIgCgLQAAgZAogFQATgCAKAFIAAgHQAAgTALgOQAGgJALgHQAJgPAdgIQAYgGALAFQAZgCAAAVIgBAGQgCgIgIgEIgFgCQgPgGgVAEQgfAFgMASQgFAEgCAGIAAABQgJAHgDAMQgCAGAAAIQAAAFADADQAFAIAMAAIACAAIADAAQAHAAAFgEQADgEANAAQAKAAAdAQIAfAQQALAAAFgJQADgFAAgGQAAgDgCgCQASAKAAAGQAAASgZAAQgKAAgYgNQgZgNgaAAIgLADQgKADgEAAIgBAAQgSgBgIgHIAAAAIACATQAAABAAAAQAAAAAAAAQgBAAgBABQgBAAgCAAQgFAAgBgKIgBgLQAAgIACgEQgKgDgJADIgCABQgFgBgFABQgWAGAAAVQAAAHADAHIABACIACAFQALAQAXAAQANAHARAAQAIADAKAAQAYAAAGgLIABgBIgBgBIANAHQgCAEgFADQgNALgaAAQgjAAgZgOg");
	this.shape_2.setTransform(11.9,8.8);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,23.9,17.7);


(lib.BGBox = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8E7E6").s().p("EgiUAL/IAA39MBEpAAAIAAX9g");
	this.shape.setTransform(521.9,629.9);

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A7A7A7").s().p("EgiUAL/IAA39MBEpAAAIAAX9g");
	this.shape_1.setTransform(529.4,632);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(302.1,553.2,447,155.6);


(lib.Behind = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5B371D").s().p("AhADYQAphmAAhDQAAhGgIiDIgGhxQARAkAOAYQAsBPAUAyQAcBFAAA4QAABKg0A8QgqAzhNAkIAVg0g");
	this.shape.setTransform(28.3,48.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#352011").s().p("AhPGVQgZgUgGhQQgCgaAAhaQgFhtgChHQAJgsAQg7QAJgfgFg9IgIhMQAKgpAOgrQAGgVAYgPQANgIAggNQAQAgAlBYQAoBiAMAbIAGBwQAICBAABJQAABDgpBmIgVA0QghAPgmANg");
	this.shape_1.setTransform(13.8,38);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AiYGwQgmgkgKhRQgDgZAAhmQAAhLAKg9IAHgsQADBHAFBtQAABaACAaQAGBRAZATIBCAAQAogNAggPQBMgkAsgzQA0g8AAhKQAAg6gchFQgUgwgvhPQgOgYgQgkQgNgbglhhQglhZgRggQghANgNAIQgYAPgHAVQgNArgKApIgBgbQgEh5BagTQAvgJA/CRQAhBLAhBcQAaA6AuBYQAjBKAAA9QAACjjgBeQgaALgdAKg");
	this.shape_2.setTransform(20.5,38);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-5.1,40.9,86.4);


(lib.Arrow_Butncopy = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("ABEC5IhEhgIhDBgIi1AAICnjAIiRixICsAAIA2BRIA3hRICsAAIiRCxICnDAg");
	this.shape.setTransform(35.4,35.6);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4,1,1).p("ADuFBQgOAFgOAEQgvAOg1AAQiPAAhmhmQhmhmAAiPQAAiQBmhmQBIhIBZgVQACAAACgB");
	this.shape_1.setTransform(23.8,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(4,1,1).p("AjVlHQAmgJAqAAQCPAABmBmQBmBmAACPQAACQhmBnQg2A1g/Aa");
	this.shape_2.setTransform(48.3,33.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F4D96F").s().p("AihDqQhXhJgchhQgchfAjhnQAjhnBeheIADgBQAmgIAqAAQCOAABmBmQBmBmAACOQAACRhmBmQg1A1hBAaIgbAJQh5gmhShFg");
	this.shape_3.setTransform(40.7,34.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("Ah5DxQhmhmAAiOQAAiRBmhmQBHhHBagVQhbBdgkBoQgjBnAcBeQAcBhBVBJQBSBFB7AnQgvANg2AAQiOAAhmhmg");
	this.shape_4.setTransform(22.5,35.3);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.ARR = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgzgHIBRg9QgDA6ARA4IAIAXg");
	this.shape.setTransform(-3,26);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AjKBOIgIgXQgSg5ADg6ID+i+IAACvIDFAAIAACXIjFAAIAACvg");
	this.shape_1.setTransform(22.7,25.1);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-8.2,0,53.6,50.2);


(lib._50Percent_white = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.502)").s().p("EhOHAsrMAAAhZVMCcOAAAMAAABZVg");
	this.shape.setTransform(500,286);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1000,571.9);


(lib.YesBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Yes_Btn();
	this.instance.setTransform(99,31.7,1,1,0,0,0,99,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AtLEyIAApkIaXAAIAAJkg");
	this.shape.setTransform(81.5,30.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1,x:99}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85,x:99.1}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.YardSteve03 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.StaySafe();
	this.instance.setTransform(121.3,33.3,0.08,0.08,-9,0,0,109.9,97.8);

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#826631").s().p("ApaA9QgCgngQgLIARAAQAKAKAUAlQAPAWAPAFQgJALgMAAQgkAAgCgjgAqmBdQgPAAgNgJIgGgLQgOgSAFgaQAEgRgFgGIALAAQAGAhARAXQAMARARAJQgHAFgKAAIgCAAgAsgBEQghgWAJghQAIgcAggfIALgOIAJgMQgFAHAgAGQgbAggIAMQgNAWAAAhQAAAMAIAPIAFAHIgHABQgLAAgKgHgAnkA0QAEgPgBgCIgBgKQAOAOAHAEQAKAFAUACIgIADQgKADgLAAQgMAAgMgEgALhAPQgIgEgHgPIgKgWIgHgJQgDgDgCgBIALgCQAIAAALAHQAMAIAGAMQAKAQAOAGQgDAGgEACQgHADgHAAQgHAAgHgEgAMXhRIgBAAQgEgIgMgEIgCAAQAFgDAEABQAHAAAPAIQAKAGAOACQgIAHgIAAQgJAAgLgJg");
	this.shape.setTransform(81.9,88.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A88541").s().p("ArIB1QgQgXgGghIgLAAQgCgDgFAAQgNAAgGAEIgHADQgMAkAMAWQgKAFgIABIgEgHQgJgPABgMQAAghANgYQAHgMAbgeQATADAhABQBcAGCtAAIAAACQAFALAJAOQAHAMgBAUQABAIgJAOQgHALgPAGQgUgBgKgFQgIgEgOgOIgCgEIgDgFQgDgEgGgCIgDAAIgEAAQgMAAgFAEQgDABgCACQAAAUgFAWQgFARgHAJQgPgFgPgWQgVglgJgKIgSAAIgCgBQgQAIgGAZQgIAjgDAEIgEAGQgQgJgNgRgAKPBEQgFgkgKggQgHgbgCgZQgFgNABgJQAAgBAngUQAvgYAegWQABAAAAAAQABgBAAAAQABAAAAABQAAAAABAAIAEAGQAgAfAbA8IgEAQIgCAEQgOgCgKgGQgPgIgHAAQgFgBgEACIgDABIgDACQgEADgDAAIgBADIgBAAQASAuAAAZQAAAPgDAHQgOgGgKgSQgHgMgMgIQgKgHgIAAIgLACIgIABQgHABACAEQAJAYgCAKQAAAMgQADg");
	this.shape_1.setTransform(84.2,82.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ApkHhQgJgGgKgMIgJAKQgHAGgHABQggAFgXgGQgHgCgagLQgNACgQAAQggACgSgKQgxgYAAhMQAAghA3g7QA4g7AAA7QAAAEgIAKIgBgDIgJAMIgLAOQggAfgIAeQgIAhAgAWQANAIAPgDQAIgBAJgEQgLgWALgkIAHgEQAHgEANAAQAEAAACADQAFAGgEARQgFAaAOATIAGAKQANAKAPAAQAMAAAHgGIAEgFQADgFAIgiQAFgZAQgJIADACQAQAKACAnQACAkAlAAQAMAAAIgMQAHgJAFgRQAGgWAAgTQABgCADgCQAGgEALAAIAFAAIACABQAGABAEAFIADAEIACAFIACAJQAAADgDAOQAWAIAWgHIAIgDQAPgHAHgLQAJgOAAgIQAAgTgHgNQgJgNgEgMIgBgCQgDgJACgFIAEgNQAmgDAPAoQAHAVAAAdQAAAegNAQQgOARgqANIglAFQgTAAgMgGIgCAJQgMATgPANQgSAOgSAAQgdAAgTgMgAKOGaQgahHgIgeIgPhDQgJgpAfgeQAfggAtgTQAXgJAOgDQAOgCARACQAYADAQAVQAeAmAKAQQAUAhAAAhQAAAbgOAQQgQATglgBIgEAAQABAPAAAQIgDAWQgDALgEAFQgFAFgLAEQgcAMgZgHQgHAKgMAEQgMAEgMAAQgNAAgMgEgAK4E4QACAAAEAEIAGAJIAKAVQAIASAHAEQANAGAPgGQAEgCADgGQADgHAAgOQABgZgSgwIABAAIABgEQACAAAFgDIADgBIACgBIADAAQALAEAEAIIABAAQAUARAQgPIADgDIADgQQgbg8ggggIgEgFQAAgBgBAAQAAAAAAAAQgBAAAAAAQgBAAAAABQgfAWgvAYQgmATAAABQgCAKAFANQADAZAGAaQAKAiAGAlIAAAAQAPgDABgMQABgJgIgZQgCgEAGgBIAGAAIACAAgAg9leQgzgPgbgVQgTgRgDgRQgDgVACgLQAFgUASgKQASgJAsgBIBTABQBVAAA3AXQA4AXgJAjQgEASgKAJQgKAKgeAPQgwAYgwABIgEAAQgnAAg9gRg");
	this.shape_2.setTransform(82.3,53);

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3A3A3A").s().p("AlOA1IgugMQgJgygOgyIgEgRQAhAKAzALQAqAJApAIIASBpIACAKQg6gKg4gOgAE7AyIgGgQQADgbAAgRIAAgKIBWgBIAAAHQAAANAKAwQgsADgcAAIgVAAg");
	this.shape_3.setTransform(66.6,117.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#494949").s().p("AmnA7IgBgKIgThpQBsATBgAEIEugEQgFAjgKA4IgHABIhGALQhUALhJAAQh7AAhygSgADDggIAAgHIBAgBQBsgPBNgVQgEAUgBAxQhpAYhiAJIgeADQgLgwAAgNg");
	this.shape_4.setTransform(86.9,119.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#442012").s().p("AkeB1QhWgTgSgxQgFgQgBgaIgBgmIgEgiQgFgjgGgTQAUAIAXAHQAwAOA2AJQgFC2AcAcQgvgKAFgCgAFeA0QgQgCgXgJQAAAAAAAAQAAAAAAgBQAAAAAAAAQAAgBAAAAIAAgEQAAgTgFgcIgDgPIgCgGQgGgbgIgbIgKgdIgDgIIAEgBIBYgDQAYCtAXAGIgnACIgYgBg");
	this.shape_5.setTransform(71.2,138);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#65301B").s().p("AmDCWQgrgDg1gMIgCAAQgcgcAEi2IAJABQBoARB/AAQB+AABGgVIASgGIACAxIgBAVIgCAWIgCgBQhKgwimAMQgxADgmAJIgdAIQgxAOAAAUIADANQAFAMANgGQBMgkBWgEIAcgBQAZAAATAEQAoAEAdAGIAYAHQAWAHATAOQAZASAEA9IAAARQgBAEgFAFgACsA5QgXgGgYiuIADAAQB7gIBogSIABAAQAAAbADANIgDgBQg4gLguAHQgpAHgIARQgCAEAAAFQAAAGAEAEIARAOQAEAAAcgMQAbgMAfAAQB3AAAsAwIAGAHQAWAcAFAwIgUAHg");
	this.shape_6.setTransform(95.3,137.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#282828").s().p("ACJG2QhqgVgKAAIgEAAIAAgWIAAgiIgBAAIAAgiQAiAIBnAQIgCBaIgOgDgAiTCGQgFgJAAgZQAAhFAZhgQAMgvAQgtQAXg/Aeg6QAyhhBAhBIAQAGQguBOgkBnQgOAsgMArQgmCJAAB8IAFBHIgJAAIgOABQgwAAgTghg");
	this.shape_7.setTransform(19.9,62.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FE9932").s().p("AAKE/IgWiHIgLg4QAtAeAEAaQAKBXAEAaQAMBEATAkIgfADQgTglgLgwgAhXi9IgKgtQgjiegGgLIAUAjQBPCXgMCwIgFACgABLh0QgpiPgihYQgagOgwgVQAQAHAmAMQAiALAKAFQAUAzApBeIAmBWIAQA5Qg5gigHgXg");
	this.shape_8.setTransform(119.5,50.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#333333").s().p("Am0HwIgYgEIgogJIAChaIAQADQCCAHBkgHQAOgCA3gCQA2gCA0gPQA0gPgHhuQgHhvgPiaQgOiZgQhaQgWh4g+g/Qg4g3hLghIEBAAQBrARBVAWIABABIAGAmQAIAeASAlQAVArAWBcQAIAlAfCXQAQBOAXCXQAXB/AjBQIAegEIACAEIBAgJQA0gJAfgJIARgFIAbgNQALA3AOAsQg/ALhJAfQhpAuiegCIhfADIgBABIlUALQg/gFiUgdgArHDQIgFhHQAAh9AniIQAMgrAPgsQAlhnAthOIAKAEQBIAfAgA9IAbAxQAWAvgBAZIgBAxQAAAdgDAUIgOB7QgHBJAAA1IAAAogAJHBFQg2jKg1h2QgrhQgCgFQgEgRgHgPQASAPAQARIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOIgNgmg");
	this.shape_9.setTransform(85.2,58);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF9933").s().p("AnoGyIgagEIAChZIAVAAIAAACQAagDAXABIAIAAQALAAAFgEIAEgEIAAgHQACgEAAgDIgBgCQACgxAIhdIEvgTQAJBhgDA9QgBAeAAAlQgCAYgKAIQgKAJgvAFQguAFhAACgAFuEzQgEgagKhWQgEgbgvgeIgBAAIABAAIDQgWIABAAIADANIABAFIAVBXIABACIAAABQALAxANBZIgKAHQgpAbhGAMIgqAEQgTglgMhEgAmFg5IgBgcQADgHgKg8QgMghgYglQgkg1gygqQgWgSgSgMIAGgJQBBgzBkgaIALADQBZAdBNBUQAgAkARAxQAMAiAHAwIABAGIATB+IkMAVIACg8gAERgeQAMixhPiWIgUgjQAhAKAcAKIAAABIABgBIACABQAwAUAcAOQAiBZApCPQAHAXA5AiIAAAAIAAAAIjFATg");
	this.shape_10.setTransform(86.9,49.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AnEBEQAFg2ACgsIABgcIEMgXIAKA+QAIA8AFAzIkvAUIAEgsgAD3BKIABAAIgThfIgMg7QgCgGAAgGIDFgTIAsCjIABABIgBAAIjQAVg");
	this.shape_11.setTransform(92.2,55.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#C1C16E").s().p("AgEAgQgggIgZgHIgHgBIAAg7ICJAAIgBBXQgwgHgYgFg");
	this.shape_12.setTransform(28.5,88.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#A7A7D1").s().p("AgDADIAHgGIgEAHIgDgBg");
	this.shape_13.setTransform(31,14.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("Aj5MTQhLgKgtgKQgcgFgqgOQgtgJgegmQgRgWAAg4QgBgPgPhFIgGgdQgNgqgCgSIgGgKQAAgFACgEQgOhdABgaIAAgNIgDgBIgNgEQgFACgJAAQgPAAgGgVIgCgLIAAgiQAAgdALiYIg/AAQgZgEhHgcQgKgIgFgIQgJgKAAguQAAhKAYhnQAJgmALgkQAZhUAmhKQA2htBIhIIAMgLQB1huChgQIEdgEQEsAwCDBMQA1AfAsAvIBMBYIBxCjQBYBeAABGIACAAIADAFQACADAAALQABAMgPAQQgtAkgOAJQgxAgghAKIAPA+QAHAfAJBCQAGAsAAAiQAAAzgXANIgDgHQgbAThBAaIgFAKIgHAQQgTArgCAjIAAALQAGAXAAAjIgBALQAvAPAjAbIAKAKIAMAKIgBAAQAlAmAHAzQADAVgEAKQgKASglACIlAAAQgFgCgogDQgsgEgXgFQgOgDgLgIQgKgFAAgEQAAgFACgFQgKgPgLgsIgCgJQgGgfgFgjIgCgTIgPBPIgBASQgBAjgDALQAHAOAFAaQAFAYAAAOQAAAhgCAIQgIAbgcAFgAn1INIAFAjIABAoQABAaAFAQQARAxBXATQgFACAvAKIABAAQA1AMAsADIFOAAQAFgFABgEIAAgRQgEg9gZgSQgTgOgWgHIgYgHQgbgGgogEQgUgDgYAAIgcAAQhWAFhMAjQgNAHgFgNIgDgNQAAgUAxgQIAdgIQAmgJAxgDQCkgLBKAxIABABIADgYIABgVIgCgxIgSAGQhEAWh+AAQh/gBhogQIgJgCQg2gJgvgPQgYgGgTgIQAFATAFAigAHwG1QhnASh7AHIgDAAIhYAEIgEABIADAIIAKAdQAIAaAGAcIACAGIACAPQAGAeAAATIAAADQgBABAAAAQAAABAAAAQAAAAABABQAAAAAAAAQAWAJARACQAVACAqgDIE+AAIAUgHQgFgwgWgeIgGgGQgsgxh3AAQggAAgbAMQgbANgEAAIgRgPQgEgEAAgGQAAgFACgEQAIgRApgHQAugHA4ALIADABQgDgNAAgaIgCAAgAokFGQAOAzAJA0IAuALQA4APA6AKQByASB7AAQBJAABSgMIBHgKIAIgCQAKg5AFgjIkuAEQhggFhsgTQgpgHgqgJQgzgLghgKIAEAQgAE9FrIhAACIhWABIAAAKQAAATgDAbIAGAPQAeACA/gFIAfgDQBigJBpgaQABgwAEgVQhNAVhsAPgApSDDIABABIAAAhIAAAWIAEAAQAJAABrAVIAOADIAoAJIAYAEQCUAdBAAFIFTgLIACgBIBegDQCeACBpguQBJgfBAgLQgPgsgKg3IgbANIgSAFQgeAJg1AJIhAAJIgCgEIgeAEQgjhQgXh9QgXiXgQhQQgfiXgIglQgVhcgWgrQgSglgIgeIgGgmIAAgBQhWgWhqgRIkCAAQBMAhA3A3QA+A/AWB4QAQBaAPCZQAMCcAHBtQAHBugxAPQg0APg3ACQg3ACgNACQhlAHiCgHIgQgDQhogQghgIgApSBuIAHABQAZAHAgAIQAaAGAwAGIAaAEIDWAAQBAgCAugFQAvgFAKgIQAKgJACgXQgBgmACgeQADg6gJhiQgFg0gIg+IgKg+IgTh+IgBgFQgHgxgMghQgRgxggglQhNhUhagcIgKgEQhkAahBAzIgJAIIACABQATAMAWASQAyAqAjA2QAZAkALAhQALA8gDAIIAAAbIgBA+IgBAcQgCAvgFA1IgEAsQgIBdgDAvIACACQAAADgCAEIgBAIIgDADQgFAEgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAAA9gAEdoDIAKAtIAeCVQABAHACAFIAMA8IATBgIgBABIABABIAKA4IAZCEQAKAxAUAlIAfgDIApgEQBHgMApgaIAKgIQgNhXgLgxIAAgBIgBgCIgVhXIgBgFIgEgNIAAAAIgsimIAAAAIAAAAIgQg5IgmhXQgqhdgTg0QgKgEgkgMQgmgLgRgHIgCgBIAAABIAAgBQgcgKghgKQAGALAjCegAqeAAIEYAEIAAgmQAAg1AHhJIAOh9QADgUAAgdIABgxQACgZgXgvIgbgxQgfg9hIgfIgKgEIgQgFQhBBBgzBhQgeA5gXA/QgQAtgMAvQgaBiAABFQABAZAFAKQAWAiA7gFgAHZogQABAFArBQQA1B2A2DMIANAmQASgOAbgOQArgXAhgLQgGg/gzhKQgTgbh1iHIhGhUQgQgRgRgPQAGAPAFARg");
	this.shape_14.setTransform(81.1,78.8);

	this.addChild(this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4.9,0,174.3,157.5);


(lib.YardSteve01 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.StaySafe();
	this.instance.setTransform(121.3,33.3,0.08,0.08,-9,0,0,109.9,97.8);

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#750000").s().p("ApaA9QgCgngQgLIARAAQAKAKAUAlQAPAWAPAFQgJALgMAAQgkAAgCgjgAqmBdQgPAAgNgJIgGgLQgOgSAFgaQAEgRgFgGIALAAQAGAhARAXQAMARARAJQgHAFgKAAIgCAAgAsgBEQghgWAJghQAIgcAggfIALgOIAJgMQgFAHAgAGQgbAggIAMQgNAWAAAhQAAAMAIAPIAFAHIgHABQgLAAgKgHgAnkA0QAEgPgBgCIgBgKQAOAOAHAEQAKAFAUACIgIADQgKADgLAAQgMAAgMgEgALhAPQgIgEgHgPIgKgWIgHgJQgDgDgCgBIALgCQAIAAALAHQAMAIAGAMQAKAQAOAGQgDAGgEACQgHADgHAAQgHAAgHgEgAMXhRIgBAAQgEgIgMgEIgCAAQAFgDAEABQAHAAAPAIQAKAGAOACQgIAHgIAAQgJAAgLgJg");
	this.shape.setTransform(81.9,88.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B20000").s().p("ArIB1QgQgXgGghIgLAAQgCgDgFAAQgNAAgGAEIgHADQgMAkAMAWQgKAFgIABIgEgHQgJgPABgMQAAghANgYQAHgMAbgeQATADAhABQBcAGCtAAIAAACQAFALAJAOQAHAMgBAUQABAIgJAOQgHALgPAGQgUgBgKgFQgIgEgOgOIgCgEIgDgFQgDgEgGgCIgDAAIgEAAQgMAAgFAEQgDABgCACQAAAUgFAWQgFARgHAJQgPgFgPgWQgVglgJgKIgSAAIgCgBQgQAIgGAZQgIAjgDAEIgEAGQgQgJgNgRgAKPBEQgFgkgKggQgHgbgCgZQgFgNABgJQAAgBAngUQAvgYAegWQABAAAAAAQABgBAAAAQABAAAAABQAAAAABAAIAEAGQAgAfAbA8IgEAQIgCAEQgOgCgKgGQgPgIgHAAQgFgBgEACIgDABIgDACQgEADgDAAIgBADIgBAAQASAuAAAZQAAAPgDAHQgOgGgKgSQgHgMgMgIQgKgHgIAAIgLACIgIABQgHABACAEQAJAYgCAKQAAAMgQADg");
	this.shape_1.setTransform(84.2,82.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ApkHhQgJgGgKgMIgJAKQgHAGgHABQggAFgXgGQgHgCgagLQgNACgQAAQggACgSgKQgxgYAAhMQAAghA3g7QA4g7AAA7QAAAEgIAKIgBgDIgJAMIgLAOQggAfgIAeQgIAhAgAWQANAIAPgDQAIgBAJgEQgLgWALgkIAHgEQAHgEANAAQAEAAACADQAFAGgEARQgFAaAOATIAGAKQANAKAPAAQAMAAAHgGIAEgFQADgFAIgiQAFgZAQgJIADACQAQAKACAnQACAkAlAAQAMAAAIgMQAHgJAFgRQAGgWAAgTQABgCADgCQAGgEALAAIAFAAIACABQAGABAEAFIADAEIACAFIACAJQAAADgDAOQAWAIAWgHIAIgDQAPgHAHgLQAJgOAAgIQAAgTgHgNQgJgNgEgMIgBgCQgDgJACgFIAEgNQAmgDAPAoQAHAVAAAdQAAAegNAQQgOARgqANIglAFQgTAAgMgGIgCAJQgMATgPANQgSAOgSAAQgdAAgTgMgAKOGaQgahHgIgeIgPhDQgJgpAfgeQAfggAtgTQAXgJAOgDQAOgCARACQAYADAQAVQAeAmAKAQQAUAhAAAhQAAAbgOAQQgQATglgBIgEAAQABAPAAAQIgDAWQgDALgEAFQgFAFgLAEQgcAMgZgHQgHAKgMAEQgMAEgMAAQgNAAgMgEgAK4E4QACAAAEAEIAGAJIAKAVQAIASAHAEQANAGAPgGQAEgCADgGQADgHAAgOQABgZgSgwIABAAIABgEQACAAAFgDIADgBIACgBIADAAQALAEAEAIIABAAQAUARAQgPIADgDIADgQQgbg8ggggIgEgFQAAgBgBAAQAAAAAAAAQgBAAAAAAQgBAAAAABQgfAWgvAYQgmATAAABQgCAKAFANQADAZAGAaQAKAiAGAlIAAAAQAPgDABgMQABgJgIgZQgCgEAGgBIAGAAIACAAgAg9leQgzgPgbgVQgTgRgDgRQgDgVACgLQAFgUASgKQASgJAsgBIBTABQBVAAA3AXQA4AXgJAjQgEASgKAJQgKAKgeAPQgwAYgwABIgEAAQgnAAg9gRg");
	this.shape_2.setTransform(82.3,53);

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#282828").s().p("ACJG2QhqgVgKAAIgEAAIAAgWIAAgiIgBAAIAAgiQAiAIBnAQIgCBaIgOgDgAiTCGQgFgJAAgZQAAhFAZhgQAMgvAQgtQAXg/Aeg6QAyhhBAhBIAQAGQguBOgkBnQgOAsgMArQgmCJAAB8IAFBHIgJAAIgOABQgwAAgTghg");
	this.shape_3.setTransform(19.9,62.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#333333").s().p("Am0HwIgYgEIgogJIAChaIAQADQCCAHBkgHQAOgCA3gCQA2gCA0gPQA0gPgHhuQgHhvgPiaQgOiZgQhaQgWh4g+g/Qg4g3hLghIEBAAQBrARBVAWIAHAnQAIAeASAlQAVArAWBcQAIAlAfCXQAQBOAXCXQAXB/AjBQIBQgJQBWgMAegLIAbgNQALA3AOAsQg/ALhJAfQhpAuiegCIhfADIgBABIlUALQg/gFiUgdgArHDQIgFhHQAAh9AniIQAMgrAPgsQAlhnAthOIAKAEQBIAfAgA9IAbAxQAWAvgBAZIgBAxQAAAdgDAUIgOB7QgHBJAAA1IAAAogAJHBFQg2jKg1h2QgrhQgCgFQgEgRgHgPQASAPAQARIBGBUQB0CHAUAbQAzBKAGA9QghALgrAXQgbAOgSAOQgKgagDgMg");
	this.shape_4.setTransform(85.2,58);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#826632").s().p("AkeB1QhWgTgSgxQgFgQgBgaIgBgmIgEgiQgFgjgGgTQAUAIAXAHQAwAOA2AJQgFC2AcAcQgvgKAFgCgAFeA0QgQgCgXgJQAAAAAAAAQAAAAAAgBQAAAAAAAAQAAgBAAAAIAAgEQAAgTgFgcIgDgPIgCgGQgGgbgIgbIgKgdIgDgIIAEgBIBYgDQAYCtAXAGIgnACIgYgBg");
	this.shape_5.setTransform(71.2,138);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#A88541").s().p("AmDCWQgrgDg1gMIgCAAQgcgcAEi2IAJABQBoARB/AAQB+AABGgVIASgGIACAxIgBAVIgCAWIgCgBQhKgwimAMQgxADgmAJIgdAIQgxAOAAAUIADANQAFAMANgGQBMgkBWgEIAcgBQAZAAATAEQAoAEAdAGIAYAHQAWAHATAOQAZASAEA9IAAARQgBAEgFAFgACsA5QgXgGgYiuIADAAQB7gIBogSIABAAQAAAbADANIgDgBQg4gLguAHQgpAHgIARQgCAEAAAFQAAAGAEAEIARAOQAEAAAcgMQAbgMAfAAQB3AAAsAwIAGAHQAWAcAFAwIgUAHg");
	this.shape_6.setTransform(95.3,137.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C1C16E").s().p("AgEAgQgggIgZgHIgHgBIAAg7ICJAAIgBBXQgwgHgYgFg");
	this.shape_7.setTransform(28.5,88.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFF91").s().p("AnoGyIgagEIAChZIAVAAIAAACQAagDAXABIAIAAQALAAAFgEIAEgEIAAgHQACgEAAgDIgBgCQADg8ALh+QAFg1ACgvQADgvAAgpIgBgcQADgHgKg8QgMghgYglQgkg1gygqQgWgSgSgMIAGgJQBBgzBkgaIALADQBZAdBNBUQAgAkARAxQAMAiAHAwIAeDAQAYCzgFBdQgBAeAAAlQgCAYgKAIQgKAJgvAFQguAFhAACgAFQFJIgYiGQgrjlgqi+QgjiegGgKQAhAKAcAKIAAABIABgBQAQAHAoAMQAkALAKAFQAUA0ApBdIAmBXQBHEAAQBJQALAxANBZQgpAghQAOIhJAGQgTgkgLgxg");
	this.shape_8.setTransform(86.9,49.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#A7A7D1").s().p("AgDADIAHgGIgEAHIgDgBg");
	this.shape_9.setTransform(31,14.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#1A2D3A").s().p("AlOA1IgugMQgJgygOgyIgEgRQAhAKAzALQAqAJApAIIASBpIACAKQg6gKg4gOgAE7AyIgGgQQADgbAAgRIAAgKIBWgBIAAAHQAAANAKAwQgsADgcAAIgVAAg");
	this.shape_10.setTransform(66.6,117.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#2B4B60").s().p("AmnA7IgBgKIgThpQBsATBgAEIEugEQgFAjgKA4IgHABIhGALQhUALhJAAQh7AAhygSgADDggIAAgHIBAgBQBsgPBNgVQgEAUgBAxQhpAYhiAJIgeADQgLgwAAgNg");
	this.shape_11.setTransform(86.9,119.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("Aj5MTQhLgKgtgKQgcgFgqgOQgtgJgegmQgRgWAAg4QgBgPgPhFIgGgdQgNgqgCgSIgGgKQAAgFACgEQgOhdABgaIAAgNIgQgFQgFACgJAAQgPAAgGgVIgCgLIAAgKIAAgYQAAgdALiYIg/AAQgZgEhHgcQgKgIgFgIQgJgKAAguQAAhKAYhnQAJgmALgkQAZhUAmhKQA2htBIhIIAMgLQB1huChgQIEdgEQEsAwCDBMQA1AfAsAvIBMBYIBxCjQBYBeAABGIACAAIADAFQACADAAALQABAMgPAQQgtAkgOAJQgxAgghAKIAPA+QAHAfAJBCQAGAsAAAiQAAAzgXANIgDgHQgWAPguAUIgYAKIgMAaQgTArgCAjIAAALQAGAXAAAjIgBALQAvAPAjAbIAKAKIAMAKIgBAAQAlAmAHAzQADAVgEAKQgKASglACIlAAAQgFgCgogDQgsgEgXgFQgOgDgLgIQgKgFAAgEQAAgFACgFQgKgPgLgsIgCgJQgGgfgFgjIgCgTIgPBPIgBASQgBAjgDALQAHAOAFAaQAFAYAAAOQAAAhgCAIQgIAbgcAFgAn1INIAFAjIABAoQABAaAFAQQARAxBXATQgFACAvAKIABAAQA1AMAsADIFOAAQAFgFABgEIAAgRQgEg9gZgSQgTgOgWgHIgYgHQgbgGgogEQgUgDgYAAIgcAAQhWAFhMAjQgNAHgFgNIgDgNQAAgUAxgQIAdgIQAmgJAxgDQCkgLBKAxIABABIADgYIABgVIgCgxIgSAGQhEAWh+AAQh/gBhogQIgJgCQg2gJgvgPQgYgGgTgIQAFATAFAigAHwG1QhnASh7AHIgDAAIhYAEIgEABIADAIIAKAdQAIAaAGAcIACAGIACAPQAGAeAAATIAAADQgBABAAAAQAAABAAAAQAAAAABABQAAAAAAAAQAWAJARACQAVACAqgDIE+AAIAUgHQgFgwgWgeIgGgGQgsgxh3AAQggAAgbAMQgbANgEAAIgRgPQgEgEAAgGQAAgFACgEQAIgRApgHQAugHA4ALIADABQgDgNAAgaIgCAAgAokFGQAOAzAJA0IAuALQA4APA6AKQByASB7AAQBJAABSgMIBHgKIAIgCQAKg5AFgjIkuAEQhggFhsgTQgpgHgqgJQgzgLghgKIAEAQgAE9FrIhAACIhWABIAAAKQAAATgDAbIAGAPQAeACA/gFIAfgDQBigJBpgaQABgwAEgVQhNAVhsAPgApSDDIABABIAAAhIAAAWIAEAAQAJAABrAVIAOADIAoAJIAYAEQCUAdBAAFIFTgLIACgBIBegDQCeACBpguQBJgfBAgLQgPgsgKg3IgbANQgfALhWAMIhQAJQgjhQgXh9QgXiXgQhQQgfiXgIglQgVhcgWgrQgSglgIgeIgGgnQhWgWhqgRIkCAAQBMAhA3A3QA+A/AWB4QAQBaAPCZQAMCcAHBtQAHBugxAPQg0APg3ACQg3ACgNACQhlAHiCgHIgQgDQhogQghgIgApSBuIAHABQAZAHAgAIQAaAGAwAGIAaAEIDWAAQBAgCAugFQAvgFAKgIQAKgJACgXQgBgmACgeQAFhagYi0IgejBQgHgxgMghQgRgxggglQhNhUhagcIgKgEQhkAahBAzIgJAIIACABQATAMAWASQAyAqAjA2QAZAkALAhQALA8gDAIIAAAbQABApgDAxQgCAvgFA1QgLB/gEA5IACACQAAADgCAEIgBAIIgDADQgFAEgMAAIgHAAQgXgBgaAEIAAgDIgWAAIiLAAIAAA9gAEdoDQApC9ArDnIAZCEQAKAxAUAlIBIgHQBRgOApggQgNhXgLgxQgQhJhHkCIgmhXQgqhdgTg0QgKgEgkgMQgpgMgQgHIAAABIAAgBQgcgKghgKQAGAKAjCfgAqeAAIEYAEIAAgmQAAg1AHhJIAOh9QADgUAAgdIABgxQACgZgXgvIgbgxQgfg9hIgfIgKgEIgQgFQhBBBgzBhQgeA5gXA/QgQAtgMAvQgaBiAABFQABAZAFAKQAWAiA7gFgAHZogQABAFArBQQA1B2A2DMQAEAMAJAaQASgOAbgOQArgXAhgLQgGg/gzhKQgTgbh1iHIhGhUQgQgRgRgPQAGAPAFARg");
	this.shape_12.setTransform(81.1,78.8);

	this.addChild(this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4.9,0,174.3,157.5);


(lib.YardSteve_03 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.YardSteve03();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4.9,0,174.3,157.5);


(lib.YardSteve_02 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape.setTransform(25.6,90.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_1.setTransform(9.8,86.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag3DAIAAgOQAAgdADgRIgxgNIgggKQgtgOgTgQQgWgUAAggQAAgZAQgkQgKgHgGgIQgIgMAAgvQAAgmAHgtIAsAEQgGAmAAAhQAAAZAGAJQAVAlA7gFIEXAEIAAgoIAAgUQAaAVATAaIgDAiIACACQAAADgCAEIgBAHIgEAEQgEAEgMAAIgHAAIAAABQACASgCAVQAAAcgHAkQgMA8gZAOIhEAAQgsgSg1gQQACAOAAAUIAAAPgAiuAgQAAAGBRAdIAyARQCDAsAdAAQAgAAABhDIgDg+IjnAAIgXAAQgZgFgUgFIgWArg");
	this.shape_2.setTransform(22.8,84);

	// Layer 1
	this.instance = new lib.YardSteve02();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#497C59").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_3.setTransform(18,46.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#5A996E").s().p("Ap6HrIAAgPQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghChAAC6IAAAoIkQgEIgFhHQAAikBCi4QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBIAGA/QghALgrAXQgbAOgSAOIgNgmQgPgngYgvQgXgpgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B4QBECGAFAWQAMA6ARBwQALBDARA0QgpAHgtAQg");
	this.shape_4.setTransform(85.2,54.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_5.setTransform(9.8,86.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EAD0B7").s().p("ArABdIgygRIABgHQAHgpABgWIDpAAIADBBQgBBDggAAQgdAAiFgtgAJQgNIgPhBQAhgKAxggIAZgRQAIAOAZAhQAmAyAAArQAAAXhaAIQgoAEgWAGIgLg5g");
	this.shape_6.setTransform(88.9,83);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AIUIEQAtgQApgIQgRg0gLhCQgRhwgMg6QgFgWhEiGQg+h4AAg2QAAgWABgRQAtABAMAtIAJAtQAGAdAJASQAVAeAXAsQAYAuAPAnIANAmQASgNAbgPQArgXAhgKQgGg/gzhJQgUgbh0iHIhGhTQgpgsgxgdQh9hMkZgtIkaAAQh2AQheBJQhdBKhGCDQg0BjgeBwQgZBhAABFQAAAaAGAJQAVAlA7gFIAJAAIEQADIAAgnQAAi6AhihQAOhDAJADQAKADAABfQAABNgJBvQgLB+gEA7IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACAUgCAVQAAAbgHAlQgMA7gZAOIhEAAQgsgRg3gRQACAPAAATIAAAQIgtAAIAAgPQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgHQgIgMAAgvQAAhKAYhmQAdh8A1hqQCTklEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBcABBHIABAAIADAEQADAEAAALQAAAMgPAQQAKAIAMARQA8BPAABFQAAAlgFAGQgLARg6AOIh1AAQAFAsAAAjQAAAZgGARgAsWFkQAAAFBRAdIAyASQCFAsAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgFIgWAtgALADBQgxAfghAKIAPBBIALA7QAWgGAogEQBagHAAgYQAAgtgmgyQgZgggIgOIgZARg");
	this.shape_7.setTransform(84.4,51.6);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,161.1);


(lib.YardSteve_01 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.YardSteve01();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4.9,0,174.3,157.5);


(lib.YardGreg_02 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BC9A72").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D5AE81").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag3DAIAAgOQAAgdADgRIgxgNIgggKQgtgOgTgQQgWgUAAggQAAgZAQgkQgKgHgGgIQgIgMAAgvQAAgmAHgtIAsAEQgGAmAAAhQAAAZAGAJQAVAlA7gFIEXAEIAAgoIAAgUQAaAVATAaIgDAiIACACQAAADgCAEIgBAHIgEAEQgEAEgMAAIgHAAIAAABQACASgCAVQAAAcgHAkQgMA8gZAOIhEAAQgsgSg1gQQACAOAAAUIAAAPgAiuAgQAAAGBRAdIAyARQCDAsAdAAQAgAAABhDIgDg+IjnAAIgXAAQgZgFgUgFIgWArg");
	this.shape_2.setTransform(22.8,84);

	// Layer 1
	this.instance = new lib.YardSGreg02();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D5AE81").s().p("AhJAgIgOg/QAggKAyggIAXgRQAIAOAZAhQAlAwAAAtQAAAXhYAIQgoAEgWAGIgLg7g");
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#497C59").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5A996E").s().p("Ap6HrIAAgPQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghChAAC6IAAAoIkQgEIgFhHQAAikBCi4QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBIAGA/QghALgrAXQgbAOgSAOIgNgmQgPgngYgvQgXgpgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B4QBECGAFAWQAMA6ARBwQALBDARA0QgpAHgtAQg");
	this.shape_5.setTransform(85.2,54.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_6.setTransform(9.8,86.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_7.setTransform(25.6,90.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AIUIEQAtgQApgIQgRg0gLhCQgRhwgMg6QgFgWhEiGQg+h4AAg2QAAgWABgRQAtABAMAtIAJAtQAGAdAJASQAVAeAXAsQAYAuAPAnIANAmQASgNAbgPQArgXAhgKQgGg/gzhJQgUgbh0iHIhGhTQgpgsgxgdQh9hMkZgtIkaAAQh2AQheBJQhdBKhGCDQg0BjgeBwQgZBhAABFQAAAaAGAJQAVAlA7gFIAJAAIEQADIAAgnQAAi6AhihQAOhDAJADQAKADAABfQAABNgJBvQgLB+gEA7IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACAUgCAVQAAAbgHAlQgMA7gZAOIhEAAQgsgRg3gRQACAPAAATIAAAQIgtAAIAAgPQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgHQgIgMAAgvQAAhKAYhmQAdh8A1hqQCTklEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBcABBHIABAAIADAEQADAEAAALQAAAMgPAQQAKAIAMARQA8BPAABFQAAAlgFAGQgLARg6AOIh1AAQAFAsAAAjQAAAZgGARgAsWFkQAAAFBRAdIAyASQCFAsAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgFIgWAtgALADBQgxAfghAKIAPBBIALA7QAWgGAogEQBagHAAgYQAAgtgmgyQgZgggIgOIgZARg");
	this.shape_8.setTransform(84.4,51.6);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,161.1);


(lib.YardFrank_02 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A4D30").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape.setTransform(9.8,86.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#975F3C").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_1.setTransform(25.6,90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag3DAIAAgOQAAgdADgRIgxgNIgggKQgtgOgTgQQgWgUAAggQAAgZAQgkQgKgHgGgIQgIgMAAgvQAAgmAHgtIAsAEQgGAmAAAhQAAAZAGAJQAVAlA7gFIEXAEIAAgoIAAgUQAaAVATAaIgDAiIACACQAAADgCAEIgBAHIgEAEQgEAEgMAAIgHAAIAAABQACASgCAVQAAAcgHAkQgMA8gZAOIhEAAQgsgSg1gQQACAOAAAUIAAAPgAiuAgQAAAGBRAdIAyARQCDAsAdAAQAgAAABhDIgDg+IjnAAIgXAAQgZgFgUgFIgWArg");
	this.shape_2.setTransform(22.8,84);

	// Layer 1
	this.instance = new lib.YardFrank02();
	this.instance.setTransform(84.3,77.7,1,1,0,0,0,84.3,77.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#975F3C").s().p("AhJAgIgOg/QAggKAyggIAXgRQAIAOAZAhQAlAwAAAtQAAAXhYAIQgoAEgWAGIgLg7g");
	this.shape_3.setTransform(155.6,78.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#497C59").s().p("AiAElQgFgKAAgZQAAhFAZhhQAdhxA0hiQBEiEBdhJQhBBcgwCJQhAC4AAClIAEBHIgIAAIgOAAQgwAAgTggg");
	this.shape_4.setTransform(18,46.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5A996E").s().p("Ap6HrIAAgPQAAgTgCgPQA3AQAsASIBEAAQAZgOAMg8QAHgkAAgcQACgVgCgTIAAgCIAHAAQAMAAAEgEIAEgDIABgIQACgEAAgDIgCgCQAEg7ALh/QAJhuAAhOQAAhegKgEQgJgCgOBCQghChAAC6IAAAoIkQgEIgFhHQAAikBCi4QAxiJBBhcQBehKB2gPIEaAAQEZAtB9BLQAxAdApAsIBGBUQB0CHAUAbQAzBIAGA/QghALgrAXQgbAOgSAOIgNgmQgPgngYgvQgXgpgVggQgJgTgGgdIgJgsQgMgugtgBQgBASAAAWQAAA1A+B4QBECGAFAWQAMA6ARBwQALBDARA0QgpAHgtAQg");
	this.shape_5.setTransform(85.2,54.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CCB59F").s().p("AgsAFIAWgrQAUAFAYAFIAXAAQgBAVgHAnIgBAHQhQgcAAgGg");
	this.shape_6.setTransform(9.8,86.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EAD0B7").s().p("AhHAVIgygSIABgFQAHgpABgVIDnAAIADBAQgBBBggAAQgdAAiDgsg");
	this.shape_7.setTransform(25.6,90.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AIUIEQAtgQApgIQgRg0gLhCQgRhwgMg6QgFgWhEiGQg+h4AAg2QAAgWABgRQAtABAMAtIAJAtQAGAdAJASQAVAeAXAsQAYAuAPAnIANAmQASgNAbgPQArgXAhgKQgGg/gzhJQgUgbh0iHIhGhTQgpgsgxgdQh9hMkZgtIkaAAQh2AQheBJQhdBKhGCDQg0BjgeBwQgZBhAABFQAAAaAGAJQAVAlA7gFIAJAAIEQADIAAgnQAAi6AhihQAOhDAJADQAKADAABfQAABNgJBvQgLB+gEA7IACADQAAADgCADIgBAIIgEAEQgEADgMAAIgHAAIAAACQACAUgCAVQAAAbgHAlQgMA7gZAOIhEAAQgsgRg3gRQACAPAAATIAAAQIgtAAIAAgPQAAgcADgSIgxgNIgggJQgtgOgTgRQgWgTAAghQAAgYAQgmQgKgIgGgHQgIgMAAgvQAAhKAYhmQAdh8A1hqQCTklEOgZIAAgFIEcAAQEtAwCDBNQA1AfAsAuIBLBYIByCjQBXBcABBHIABAAIADAEQADAEAAALQAAAMgPAQQAKAIAMARQA8BPAABFQAAAlgFAGQgLARg6AOIh1AAQAFAsAAAjQAAAZgGARgAsWFkQAAAFBRAdIAyASQCFAsAdAAQAgAAABhDIgDhBIjpAAIgXAAQgZgEgUgFIgWAtgALADBQgxAfghAKIAPBBIALA7QAWgGAogEQBagHAAgYQAAgtgmgyQgZgggIgOIgZARg");
	this.shape_8.setTransform(84.4,51.6);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.instance,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,161.1);


(lib.WinnersCup = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(272.5,380.2,0.48,0.48,0,0,0,291.4,34.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A0sLdIgPg6QgdjNC+itQAygjAagEIAEAAQAsgGAaBUIgFAKIgDAAIgegFQhvAhhpCfQgaBCAEAZIACASQALBICXgCQBdgOAjhqIgsgMQgXADhiBFQgOgWgDgWIAAgFQgLhICHgvIAHgBQBQgLAOBpQAPBrilBsIgoAOQgRADgPAAQhYAAgthMgAubLzIgEgcIgJlqQgSADgEgbQgLhTCGghICEgXQBpgOAKBJIABAEQASCAjNCIIABAEQB8BVBQAoIACAOQAFAjgVAOIgTACIkTiOIASCgQgJAYgZAEIgEAAQgPAAgLgOgAq7EnQhMAKhmAwIADB9IAFgBQDkhegKhIIAAgEQgCgMgrAAIgDAAgAoyKtQgHg0CenJIARgQIAQgDQAhgEBdEnQA8CKAHAzQgEAcgYAEQgeAEgiiPIgEgEIgEABQg0AIidBVQgDBdgkAFQgZgGgEgbgAmAEfQgjBcgaBlIAbgEQApgGA8gaIg7iegAiVKGQgQhtgDkoIgPgMQgKhDBdgaQBcgfBOgLIArgGQAfAOADAWQAAA2gnAGQhxAQhpAlIAEBjIAZABIASgCQBSgMBrgqIASAZQAABRg3AIQggAWicAgQAQDJgbAEIgSAHIgFABIgBAAQgKAAgFgQgAFVJEQgLkBAGijIiNArQgUADgJgoQgJhDB3gRQBGgcCigXQArgGAFAkIACAJQAGAxhMALIhfASQgLDrAYClQgHAlgMACIgPACIgJABQgRAAgFgKgAJWHFQgLhMACjbIgNgYQgMhYEOg1IA5gMQAYACAEAbQABA6gmAFQj6AjAKBGIABAnIADAWQCeg+A7gIIAJgCQAYAHAFAhQAGAwg/AJQjNA4AFAmIAEA+QAGAnAygDQBNgUBxguIAOgCQATAEADAXIACANQAHA2jfA9IgNACIgPABQhFAAgVhdgAPbHmIgEgbIgJlrQgSADgEgbQgMhQCHghICEgXQBogPALBJIAAAFQASB+jMCHIABAFQB7BVBRAoIACANQAFAkgWANIgSADIkUiPIASCgQgIAZgZADIgFABQgPAAgKgPgAS6AaQhLALhnAvIAEB9IAFgBQDkhegKhHIgBgFQgBgMgrAAIgEAAgAwSg5QgDgkBFjIQieiAgNhcQAVgeAPgCIB7CLIAyAeQA/iiAcgjIAUgDIATAQQAAAYiOFVQgjBfgMBJIgLAGQgegJgEgbgAsUi7IgJgoIgCgOQgejWCziKQAvgZAtgGQCQAAAVCVQAcDEiJCWQg7AtgyAIQgPACgNAAQhlAAgwhxgApGoQQhcANhKDFQgJA5ADAcQA+BGBUgMQCHgTAfinIgBgzQgQhxhxgEgAlBiwIAAgEQggnZAugGIAKgCQAWAGADAJQgEDxAPBrIAHAxQB+gSBBh/QgIkdAegEIAFgBQAcgBACALQgHEFAQCAIAKBHQAFAjgZASIgSACIgWgPIgBgJIgGhMQgRADiaBrIghAOIgCAAQg3AAgGgpgABIjIQginuAPgCIAVgRQAegEA6BrQBpCbB0BpQgMlMAIg8IAQgQIAOgCQANgCADASQAZHYgFAhIgaANQhIAKjXkcIgVgPIgDAOIAUDtIAJAbQABA3geAEQgdgGgHgQgAIFlMIgPg6QgdjNC/itQAxgjAbgEIADAAQAsgGAbBUIgGAKIgDAAIgdgFQhvAhhpCfQgaBCADAZIADASQAKBICXgCQBegOAihqIgsgMQgXADhhBFQgOgWgDgWIgBgFQgKhICHgvIAHgBQBPgLAPBpQAPBrilBsIgoAOQgRADgQAAQhXAAguhMg");
	this.shape.setTransform(285.8,165.2);

	// Layer 6
	this.instance_1 = new lib.Handle();
	this.instance_1.setTransform(82.5,164.8,1,1,0,0,180,82.8,103.8);

	this.instance_2 = new lib.Handlecopy();
	this.instance_2.setTransform(458.1,164.8,1,1,0,0,0,82.8,103.8);

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4,1,1).p("A6Cn+ILfhlMAi7gE0AqTOYIGkg7IdykP");
	this.shape_1.setTransform(242.1,147.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7A6C38").s().p("Ah8G5Qidifh2jOQjIlfhWj+QgPgvgMhYILdhlQAjB5AZBMQEKM2FuHEImkA8Qj6iainirg");
	this.shape_2.setTransform(146.8,163.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9E8C49").s().p("AjYRSQmuiIk+jFIGkg7QlunEkKs2QgYhNgkh4MAi7gE0QG1AiIzA9QAEAAALgCQAIgCgBADQgOB9gtCpQgZBaghBqQgjB8hWCtQgrBWhtDAQhpC8ikCiI9yEOIAOARQB4CRB5BNICGBUQA0AhA1AkgA+1qjQgSg9gHgqQgFgjgDgLQAAgogFgvQgIg5gCghIDmguQCvgiB4gSQAggEAxgBIBPgCQA6DOAqCPIrfBlQAAgNgCgGg");
	this.shape_3.setTransform(272.6,162.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("Ag/RcQg1glgzghIiHhTQh4hOh4iRIgOgRIdxkOQkFEBjqCQQlfDXmNAvgA1ExIQPygqOFA0QDCALDmATMgi7AE0QgqiPg6jNg");
	this.shape_4.setTransform(273.9,161.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AjdSpQnLhml7kMIgpgdQkzjjjolEQjZkuh4lVQhtk4AAj+IABgjIgIgCQAAAAAAAAQAAgBgBAAQAAAAgBAAQAAAAgBAAQgDABAAgTQAAgyEHgrQA8gKCSgUQBwgQAfgHICBgGQO4goPSAuQB2ARK/A1QBYAGD2AcQEwAiAdASQASACAGAFQAKAFADAQQADAUgEAWQAADbh8FAQiKFijpE6QhABWhDBOQjdECkICyQmPENnKA7gA03xRIhPACQgxAAggAFQh4ASivAiIjmAtQACAiAIA5QAFAvAAAoQADALAFAiQAHArASA9QACAGAAANQAMBYAQAuQBVD+DJFfQB1DOCeCgQCpCqD5CaQE+DFGuCIICmAAICoAAQGOguFfjYQDpiPEGkBQCkiiBpi8QBtjBArhWQBWisAjh8QAhhrAZhZQAtipAOh9QABgDgIACQgLACgEAAQozg9m1gjQjlgSjDgLQn2gdoWAAQmrAAnAASg");
	this.shape_5.setTransform(272.5,162.2);

	// Layer 7
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#7A6C38").s().p("AkfEpQiEgUgegGQAHgoBbgdQAxgQCbgeQCVgeBMgZQB1gnAug4QASg0Auh0QAuh1ANgmQAugCAoABQgMEKhHB3QhDByiIA7QhAAcjFAyIgGABQhdgKhbgMg");
	this.shape_6.setTransform(223,302.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#9E8C49").s().p("AjuFOQkrgCkOgcIAGgCQDHgxBAgcQCIg8BDhxQBHh4AMkJQC0AFAxA/QAXAcADA2QADAeADBDQAQB9CDBBQC6BdHSANIAAAKIgHATQgPAegaAHQhIATgYAFQgvAJg1ADQnMAWkeAAIg4AAg");
	this.shape_7.setTransform(293.4,303.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhLGjQl+gGlMgzIgQgCQiegZg7gYQg1gWAAgkQAAgoBXgxQBVgxBygbQBkgYCxgmQCGggAkgpQAhglAOhCIAPhyQAJhDAVggQAbgtA8gLQA3ADAvAGQERAeA1BkQATAjABBGQAABJANAdQAsBjCDAxQBdAjClAOIDrAUQBpAQAEAuQACAZgIAYQgKAbghATQhZAzj5AdQlDAnlbAAIhdgBgAiWlQQgNAmgtB1QguB0gSA2QgvA2h1AnQhLAZiXAeQibAegxAQQhbAdgHAoQAeAGCEAUQBbAMBdAKQEOAcErADQEoACH6gZQA1gCAvgKQAYgFBIgTQAagHAPgdIAHgUIAAgKQnSgNi6hcQiDhBgQh9QgDhDgDgfQgFg1gXgdQgxg/iygFIgjAAIg0ABg");
	this.shape_8.setTransform(274.5,304);

	// Layer 8
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#9E8C49").s().p("A2HBFIBuhCQBUgwA2gXIGfAVQAOAEEgAPIEdAPIAWAGIT9gVQBGALBZAfQBIAbA0Acg");
	this.shape_9.setTransform(272.5,332);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#CCB55E").s().p("A3qExIgKhHIgNkTQgNkGAAgQQAAgWAUgZQAagbAIgLMAu/AAEQAEAEAHARQAHARADADQAnFxgnFmQhoABAAAFQgmADusACQgXAGpMAHQgVAG0vAHQAEglgJg/g");
	this.shape_10.setTransform(270.4,385.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("A4uIFQAAgRAHgLIAGgEQAAgBABAAQABgBAAAAQAAgBAAAAQABAAgBgBQgaiqgJi6QgHiFABjgQAAg2ABgJQAIgzAlgMQAHgGAMgGIARAAQAtgkBcgwQBxg6BugqQG7ABATAGQIrAGAWAHIT3AAQEYBJCUBbIgCAAQAmBAAAFTIABD6QgDB+gKA2QAGAHgEAOQgEANgHgBQgIATgJAAQggAAgDgDIhKgEQhAgDhKAGImuADQmxAFgMAEIk2AFQk4AEgOAFI1LASQgngkAAgCgA37kUQgVAaAAAVQAAARANEDIAOEWIAJBHQAKA/gEAkQUugGAWgGQJMgHAWgHQOsgCAmgCQAAgGBpAAQAnlngnlwQgDgDgHgRQgHgSgEgEMgu/gADQgIAKgaAbgA0Fm0IhvBCMAsQAAAQg0gdhIgbQhYgghGgLIz9AVIgXgHIkdgOQkfgPgPgFImegVQg2AYhUAyg");
	this.shape_11.setTransform(270.5,376.1);

	this.addChild(this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.instance_2,this.instance_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.4,42.8,541.4,388.8);


(lib.TheCounter = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ArbCjIAAgFQgNhcB2hoQAXgMAMgCIALgCQATgCAIAeIAAAFQAHAsghAUIgFgEIAAgDIAFgnIgJgEQgzAHg5BpQgNAgACAPIAAACQAGAmAtgGIACAAQAsgHAygfIAGAAQAJACACANIABAJQglAzg8AIIgGABQhMAAgKhFgAoPCSIgGgcQgPhvBdhFQAYgNAXgDQBKgBALBOQAOBjhGBOQgfAXgaAEIgOABQg0AAgZg6gAmlgcQgvAHgmBjQgFAeACAOQAgAkArgGQBGgJAQhXIAAgbQgJg4g7gCgAkdCXIgBgCQgQjyAYgEIAFAAQALADACAEQgCB7AHA3IAEAaQBCgKAhhBQgEiSAPgCIADAAQAOgBABAGQgDCFAJBCIAFAkQADATgOAJIgKABIgLgHIgDgtQgJABhQA5IgRAHQgdAAgDgWgAhSCLQgRj+AHgBIALgJQAQgCAeA4QA0BOA8A2QgHipAFggIAIgIIAHgBQAHgBACAKQAMDygCARIgNAGQgmAGhtiRIgLgIIgBAHIAKB5IAEAOQABAcgPACQgPgDgEgIgADXBnQgGiDADhUIhJAWQgKACgFgVQgFgjA+gIQAkgPBUgLQAWgEADATIAAAEQAEAagnAGIgyAJQgFB4AMBVQgDATgHABIgHABIgFABQgJAAgCgGgAFbAmQgFgmABhxIgHgNQgGgtCLgcIAegHQAMABACAPQABAegUADQiBASAFAkIABAUIABAMQBSghAfgEIAEgBQANAEACARQAEAZghAEQhqAeACATIACAeQADAUAagBQAogLA7gYIAHAAQAKACABAMIABAHQAEAbhzAgIgHABIgIAAQgkAAgLgvgAIkA2IgCgOIgEi5QgJABgCgOQgGgqBFgRIBEgMQA2gIAGAmIAAACQAJBChpBGIAAADQBAAsAqATIABAHQACASgLAHIgKABIiOhIIAJBRQgEANgNACIgDAAQgHAAgGgIgAKYi1QgnAFg1AYIACBBIACAAQB2gxgFglIAAgDQgBgFgSAAIgGAAg");
	this.shape.setTransform(232.9,266.4,1.823,1.823);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AjTCnQgGiFAEhSIhJAWQgLACgFgVQgEgjA+gIQAjgPBVgLQAWgEACATIABAEQAEAagoAGIgxAJQgFB4AMBVQgEATgGABIgIABIgFAAQgIAAgDgFgAhICPQgTj/AggFIAEAAIAJAIQgFBYAGAmQBsgdgDgUIgFhlIAQgJQANABACANIAAADQATD9gIABIgCADIgHABQgNABgGgLQAAgigIg9IgDAAIhgAnIgHAKIAHAtQADAUgOAKIgFABIgDAAQgLAAgEgKgACCBIQgGgoABhvIgGgMQgHguCMgcIAegGQAMAAACAQQAAAdgTADQiBASAFAkIAAAVIACALQBRggAfgFIAFAAQAMAEADARQADAYghAFQhqAbADAUIACAgQADAUAagCQAogKA6gYIAHgBQAKACACAMIABAHQAEAch0AfIgHABIgIABQgjAAgLgwg");
	this.shape_1.setTransform(225.1,219.2);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7A6C38").s().p("AiPINIhkjCIgKgUIgYgvIgEgIQg9h7g2h9IgCgHIgOgeQhejhhDjqIJUhPICigVQCgJFDlH5IivAYIn7BDIgjhAg");
	this.shape_2.setTransform(80.9,262.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9E8C49").s().p("AkAZ0QnRjbk6jnQjVidiVjiQgYglgZgqIH3hDICvgXQEJI9FiHeIhrgxgAyfntMAongFZQAkgHBFgIQA2gGA9gOIHhhQQhGG6iEGBIgWA/Qg7CihGCZIg9AJMgnBAFNQjln8igpDgA/triIAAgDIgLg2QgWhxgQhzQghjvgFjbIADirIBnAAIAngIIETgEICigVIBogPQBWJfCSIfIiiAVIpXBQQgoiPgeiSg");
	this.shape_3.setTransform(217.9,252.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("EgH1AgDIgWgKQlindkJo+MAmqgFIIAbgFQgbA4gcA2QhBCAgjBCQg9Byg3BUQiVDijVCdQk6DnnPDbQifBLiCA3QhMgghVgngA8M1PQC7gcBUgVQDCgwEpiQQDHhgDgieQDyiqAlhhQAkBhDyCqQDeCeDIBgQEoCQDDAwQCHAiGSAzIESADIAnAIIBoAAQABB+gMCXIAAAHIgEApIgBAJIAAAJIglEqIAAAAInlBOIjkAjMgozAFcQiSofhWpeg");
	this.shape_4.setTransform(255.2,218.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#1C180C").s().p("A5AM5IH9hEICvgXMAnBgFNIA8gJIgfBBIgaAEMgmrAFJIivAXIn3BCIgfg2gEggigE1IJYhQICigWMAoygFcIDkgjIHmhNIgCAJIgDAXIgEAaInhBQQg9AOg3AGQhEAIglAHMgomAFZIijAVIpWBPIgQg4gEAgjgNuIAAABIgEApIgBAJIgDABIAIg0g");
	this.shape_5.setTransform(230.1,238.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("EgA0Ah+IgLgDQgggMAFgCIgCAAQj8hXkzi4QkOihlHj3QiNhrifkPIgHgLIgfg3QhKiGhAiSIgYg3IgbhDQihmUgfhQIgghYQhBitgmiFQgUhFgQhIIgMg3Qg6kjAAlKQgDh+AHhkIAEguIgCgCQgGgFAAgDQAAgPADgDQACgCAIAAQAFgeBIACQBqACAXgGIEVgCICLgWIBlgPIA8gIQCUgXBVgTQDcgzEiicQCphaDUiuQDujFAyggIAAAEIABgEQAyAgDuDFQDUCuCpBaQEiCcDdAzQBUATCUAXQDpAiBEALIEVACQAXAGBqgCQBHgCAGAeQAHAAACACQAEADAAAPQAAADgGAFIgDACQAMBpgDCnIgBAPIgEA5QgLCfgVCWIAAACIAAABIgIA0QgbC0gqCmQg1DThfEQIgWA+Qg2CVhBCmIgVA1IgFANQhdDmiHDgQijEMiNBvQpwHwoWC3QguARgtANIgDAAQgNAAgkgNgEghkgR2QAFDaAhDwQAQBzAWBxIALA2IAAADQAeCSAoCPIAQA4QBDDoBeDhIAOAhIACAGQA2B+A9B6IAEAIIAYAvIALAVIBkDBIAjBBIAfA2QAZArAYAkQCWDiDUCeQE6DnHRDbIBrAxIAWAKQBVAnBKAfQCCg3CfhKQHRjbE6jnQDVieCVjiQA3hUA9hyQAjhCBBh/QAcg3Abg3IAfhBQBGiaA7ikIAWg+QCEl/BGm6IAEgaIAEgXIABgJIAAgBIAlkpIAAgJIABgJIAEgpIAAgIQAMiXgBh9IhoAAIgngIIkSgEQmSgyiHgiQjDgwkoiRQjIhfjgifQjyiqgkhgQgjBgjyCqQjgCfjHBfQkpCRjCAwQhUAVi7AbIhoAPIiiAVIkSAEIgoAIIhnAAg");
	this.shape_6.setTransform(221,218.8);

	// Layer 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(0,0,0,0.502)").s().p("EgA0Ah/IgLgEQgggMAFgBIgCgBQj8hYkzi2QkOiilHj3QiNhrifkPIgHgLIgfg3QhKiGhAiRIgYg4IgbhEQihmUgfhPIgghXQhBiugmiFQgUhFgQhIIgLg3Qg7kjAAlJQgDiAAHhiIAEgvIgCgCQgGgGAAgCQAAgPADgDQACgBAIgBQAFgdBIAAQBqADAXgFIEVgDQAogHBjgPIBlgPIA8gJQCUgVBVgUQDcgzEiibQCphbDUivQDujEAyggIAAAFIABgFQAyAgDuDEQDUCvCpBbQEiCbDdAzQBUAUCUAVQDpAjBEALIEVADQAXAFBqgDQBHAAAGAdQAHABACABQAEADAAAPQAAACgGAGIgDACQAMBpgDCoIgBANIgEA6QgLCfgUCWIAAABIgBACIAAABIgDApIgBAJIgDABQgcCzgqCoQg0DShgERIgWA+Qg2CUhBCnIgVAzIgFAPQhdDliHDfQijENiNBvQpwHwoWC4QguAPgtANIgDABQgNAAgkgMg");
	this.shape_7.setTransform(230.5,228.3);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.instance,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,451.6,447);


(lib.SoundIconRoundBtncopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 3
	this.instance = new lib.S_off();
	this.instance.setTransform(38.6,26.4,1,1,0,0,0,6.5,6.5);

	this.text = new cjs.Text("Sound off", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.lineWidth = 243;
	this.text.setTransform(51.4,17.9,0.59,0.59);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAIBXQgCgDAAgEQAAgEACgDQAfgeAAgrQAAgpgfgfQgCgDAAgEQAAgEACgCQAEgDAEAAQADAAADADQAkAkAAAxQAAAzgkAkQgDACgDAAQgEAAgEgCgAgVA9QgEgDAAgEQAAgEAEgCQATgUAAgcQAAgbgTgTQgEgDAAgEQAAgEAEgDQACgDAEAAQAEAAADADQAXAZABAjQgBAkgXAZQgEADgDAAQgEAAgCgDgAg2AkQgDgDAAgEQAAgEADgDQAJgJAAgNQAAgMgJgJQgDgDAAgEQAAgEADgCQADgDAEAAQADAAADADQAPAOAAAUQAAAVgPAPQgDACgDAAQgEAAgDgCg");
	this.shape.setTransform(37.5,25.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text,p:{text:"Sound off",lineWidth:243}},{t:this.instance}]}).to({state:[{t:this.shape},{t:this.text,p:{text:"Sound on",lineWidth:214}}]},1).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAzByIhGhJIgkAAQAAAAgBgBQgBAAAAAAQgBAAAAAAQgBgBAAAAQgCgCAAgDIAAhDQAAgDACgCQAAgBABAAQAAAAABAAQAAgBABAAQABAAAAAAIAkAAIBGhJQACgCADAAIACAAQAEACAAAFIAADZQAAAFgEACIgCAAQgDAAgCgCg");
	this.shape_1.setTransform(23.2,26.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(2));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#4F2A3F").s().p("AwJC/QgxgBAAgyIAAkYQAAgyAxABMAgTAAAQAygBAAAyIAAEYQAAAygyABg");
	this.shape_2.setTransform(118.9,25.9);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(10.5,6.8,216.8,38.2);


(lib.ShareTwitter = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("Share on Twitter", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(87.1,20.9,0.59,0.59);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Aj6CdQAMACANAAQBIAAA4gsQgigBgagUQgagUgKgfQAIACALAAQAMAAAPgDQgjgIgYgcQgXgbAAglIAAgBQAWANAYAAQgVgOgMgWQgNgXAAgbQAAgcAOgYQAnAvA3AdQA4AdA/ADQgDgNAAgKQAAgrAcgeQAfgfAqAAQAtABAeAgQAhgGAhgTQgMAkghAVQAfgEAcgMQgVAfgfAWIABANQAABuhMBWQhUBgiEAAQhWAAhIgvg");
	this.shape.setTransform(67.5,30,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#23AADB").s().p("AwIC/QgzAAAAgyIAAkYQAAgzAzAAMAgSAAAQAxAAAAAzIAAEYQAAAygxAAg");
	this.shape_1.setTransform(156.4,29.1);

	this.instance = new lib.Share_Twitter();
	this.instance.setTransform(167.2,31.7,0.85,0.85,0,0,0,166.3,30.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3A5A94").s().p("A5SExQgyAAAAgyIAAn9QAAgyAyAAMAylAAAQAyAAAAAyIAAH9QAAAygyAAg");
	this.shape_2.setTransform(167.8,31.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).to({state:[{t:this.instance}]},2).to({state:[{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(48,10,216.8,38.2);


(lib.ShareFB = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.text = new cjs.Text("Share on Facebook", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(87,21.3,0.59,0.59);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhDFDIAAknIhjAAIAAhxIBjAAIAAhUQAAhJApgpQAlgnBEABQA5gBAfAFIAABnIg9AAQghAAgNAPQgLANAAAcIAABJIByAAIgPBxIhjAAIAAEng");
	this.shape.setTransform(64.1,29.5,0.51,0.51);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#395994").s().p("AwIC/QgzAAAAgyIAAkYQAAgzAzAAMAgSAAAQAxAAAAAzIAAEYQAAAygxAAg");
	this.shape_1.setTransform(156.4,29.1);

	this.instance = new lib.Share_FB();
	this.instance.setTransform(167.2,31.7,0.85,0.85,0,0,0,166.3,30.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3A5A94").s().p("A5SExQgyAAAAgyIAAn9QAAgyAyAAMAylAAAQAyAAAAAyIAAH9QAAAygyAAg");
	this.shape_2.setTransform(167.8,31.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.text}]}).to({state:[{t:this.instance}]},2).to({state:[{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(48,10,216.8,38.2);


(lib.REPLAY = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.replaybtn();
	this.instance.setTransform(34.9,34.9,1,1,0,0,0,34.9,34.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AkLEMQhvhvAAidQAAicBvhvQBPhPBmgXQAqgJAsAAQCdAABvBvQBvBvAACcQAACdhvBvQg6A7hHAbQhAAZhLAAQicAAhvhvg");
	this.shape.setTransform(34.8,34.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regX:34.9,regY:34.9,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regX:34.8,regY:34.8,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 2
	this.instance_1 = new lib.shadowCircle();
	this.instance_1.setTransform(39.1,39.1,1,1,0,0,0,36.9,36.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:true},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,77.9,77.9);


(lib.RemnderBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Reminder();
	this.instance.setTransform(99,31.7,1,1,0,0,0,99,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F4D96F").s().p("Av0FQIAAqfIfqAAIAAKfg");
	this.shape.setTransform(98.9,31.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,202.2,67.5);


(lib.PostRoomLeaderBoard = function() {
	this.initialize();

	// Layer 2
	this.text = new cjs.Text("SCORE", "49px 'VAGRounded'");
	this.text.lineHeight = 60;
	this.text.lineWidth = 163;
	this.text.setTransform(581.4,3.3,0.75,0.75);

	this.text_1 = new cjs.Text("NAME", "49px 'VAGRounded'");
	this.text_1.lineHeight = 60;
	this.text_1.lineWidth = 163;
	this.text_1.setTransform(44.5,0.4,0.75,0.75);

	this.text_2 = new cjs.Text("10,564\n9,324\n8,361\n7,183\n6,753\n6,650\n5,921", "40px 'Laffayette Comic Pro'");
	this.text_2.textAlign = "right";
	this.text_2.lineHeight = 54;
	this.text_2.lineWidth = 187;
	this.text_2.setTransform(689.2,74.8,0.88,0.88);

	this.text_3 = new cjs.Text("COOLJ\nMARCO_B\n259_BRYCE\nDAVESYKES32\nTOM93\nPAT_JONES\nJO_KINGSLEY", "40px 'Laffayette Comic Pro'");
	this.text_3.lineHeight = 54;
	this.text_3.lineWidth = 398;
	this.text_3.setTransform(45.4,74.8,0.88,0.88);

	// Layer 3
	this.instance = new lib.ARR();
	this.instance.setTransform(765.5,89.7,0.75,0.68,0,-90,90,22.6,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 193, 193, 193, 0)];
	this.instance.cache(-10,-2,58,54);

	this.instance_1 = new lib.ARR();
	this.instance_1.setTransform(765.5,360.7,0.75,0.68,-90,0,0,22.6,25.1);
	this.instance_1.filters = [new cjs.ColorFilter(0, 0, 0, 1, 193, 193, 193, 0)];
	this.instance_1.cache(-10,-2,58,54);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C1C1C1").s().p("AgyJGIAAyLIBlAAIAASLg");
	this.shape.setTransform(765.5,176.1);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4,1,1).p("Eg9lgdQIAAGIMAAAA0YQACA+AeAhQAWAYAjAJQAbAGAaAAMB2iAAAIAOAAQAcAAAYgGQAjgKAWgXQAJgKAHgNQADgGADgHQAIgVACgeQAAgIAAgIMAAAgu8IAAlUIAAgqIAAlVQAAgwgQggQgfg7hXgDMh3AAAAQiBAFgECAg");
	this.shape_1.setTransform(394.3,200.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C1C1C1").s().p("Eg9lAEGIAAmGQAEiACBgFMB3AAAAQBXADAfA7QAQAfAAAxIAAFTIAAAqg");
	this.shape_2.setTransform(394.3,26.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EA7KAbPMh2hAAAQgaAAgbgGQgjgJgWgXQgeghgCg+MAAAg0YMB7LAAAIAAFUMAAAAu8IAAAPQgCAfgJAUIgFAOQgHANgJAKQgWAXgjAJQgZAGgbAAgEA9GAapQAJgKAHgNIAAAXgEA9WAaSg");
	this.shape_3.setTransform(394.3,226.8);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance_1,this.instance,this.text_3,this.text_2,this.text_1,this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,792.5,429.7);


(lib.PeppaYard1 = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#421F10").s().p("AgSAvQgCgDgDgCIgBgEQgGgfAOgdQAHgPAJgLIAaAWIAAAAIgJAkIgEAUQgCAIgIAEIgDAEIAAABQgCACgGAAQgIAAgCgCg");
	this.shape.setTransform(109.3,155.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#65301B").s().p("ABFAsIgBAAQgEgBgCgOQgBgTgCgDQgEgIgLgHIgRADQgfAFgNAIQgXAEgJALIgbgWQAHgGAJgGQACgHAVgMIAbgNIAkAAIAVAIIAAABQAPABAIAZQAFAOABAUIAAAFIABADQAAAJgHABIgBAAg");
	this.shape_1.setTransform(117,150.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhMBSIgEAAQgVAAgFgYIAAgbQAAg+A5gqQAMgIAKgDQALgKAqAEQA2AFAEASIACABQAKAFACAOIAGAPQADASAAAOIAAAMQgBAKgFAAIgCAAIgCABQgFAAgBgJQAGgBAAgKIAAgDIAAgDQgBgTgFgRQgIgZgPgBIAAAAIgWgJIgmAAIgZAOQgUALgDAHQgJAGgGAIQgLAMgGAOQgOAeAGAfIAAADQAEADABADQADACAHAAQAGAAAEgCIABgCIADgEQAHgDACgJIAFgTIAIgkIABAAQAJgLAVgFQANgIAggGIARgDQAMAHAEAKQABACACATQABAMAEACIAAABQAAABAAAAQAAABAAAAQgBABAAABQAAAAgBABQAAAAgBAAQAAABAAAAQgBAAAAAAQAAABgBAAQgIAAAAgLIABgNQAAgFgEgJIgBgDQgTAGgRACQgEAAgVALIgEACQgHAKgJANQgCADgIAhQgMAfgVAAQgQAAgHgKg");
	this.shape_2.setTransform(114.9,153.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#421F10").s().p("AgFA4IgEgBIgEAAIgBABIgDgDQgFgIgDgIQgDgHAAgEQgBgOAIgUIAEgKQAJgVALgQIAUAWIAGAHQgJAMgDAOQgDALgBAUIgBARIgDAEIgGADIgEABIgEAAg");
	this.shape_3.setTransform(66.6,157.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#65301B").s().p("ABaAgIgDgHIgCgEIgFgJQgDgDgJgDQgMgDgNgBIgGgCIgBAAIgLAAIgFAAIgUgBIgMAAIgDABQgoAAgPAOIgIAJIgGgHIgUgUIAFgFQAOgPARgHQACgDAEgCIAYgIIAmAAQAdAEAmAZIADACIAAAAQAjAXAAASIAAAFIAAADQgBAJgFAAQgEAAgFgNg");
	this.shape_4.setTransform(77.5,152.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AhLBeQgEABgIAAIgVAAIgEAAQgVAAgEgXQAahhAFgGQAbgiApgTQANgHAOgBIAVgBQAGgCAbAFQAgAHAMAMIAbAYQAIAGAGALQAEAIAAADIAAAFQACACAAACIAAACQADAIAAAIQACAagJgBIgCgBIgDAAQgEAAgBgDIgBgEIABgDIgBAAIgBAAQgCADgDgBQgFABgGgbIgEAAQgRAAgWgGIgRAAQg5AAgRAyIgMAkQgHARgOAAQgGAAgEgBgAhdBOIAEAAIAEAAIAGAAIAGgDIAEgEIAAgRQABgUADgLQADgQAKgKIAIgJQAOgOAngCIACgBIAMAAIAXABIAEAAIAMACIAAAAIAGACQANABAMADQAKADACADIAFAJIACADIADAGQAGANADAAQAGAAAAgJIAAgDIAAgFQAAgQgjgZIAAAAIgDgCQglgZgegEIgmAAIgYAIQgEACgBADQgSAHgOAPIgFAFQgNAQgJATIgEAKQgHAVAAAPQAAAEADAHQADAIAGAIIACACIACgBIADABg");
	this.shape_5.setTransform(75,155.6);

	// Layer 8
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BA986F").s().p("AAjBjQgEAAgMgJIgQgKQgMgKgCgIIgCgDQgFgIgBgHIgFgXIAAgdQgCgEgBgLIgDgSIgCgHIgFgWQgEgMAAgJQAAAAAAgBQAAgBAAAAQAAAAAAgBQAAAAAAgBQACgDAIAAQANAAAIAHIALARQACADAEAIQAcALADASQAEACAAAGIAAABIgOATQgPARgEAKIgEAKIAFAKQAIANAMAJIAHAEIAAAAQAAAAABAAQAAAAAAABQABAAAAAAQAAABAAAAIABABIAAABIAAADQAAAOgHAEQAFADAAABQAAABAAAAQAAABgBAAQAAABgBAAQAAAAgBAAIAAAAgAgBhDIgBgBQgOgYgKAAIgIAAIAAAFIACAJIABACIAEAFQAHgCATAGIAAAAg");
	this.shape_6.setTransform(58.9,151.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#D5AE81").s().p("AhoA0QABAAAAAAQABAAAAgBQAAAAABAAQAAgBAAgBQAAgBgFgDQAGgEABgOIAAgDIAAgBIgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQgBAAAAAAIgBAAIgGgEQgMgJgIgLIgFgKIADgKQAFgKAPgTIAOgTIAAgBQAcgCAoAQQBKAIA0AhQAWAOALALIgDACIAGAVQAFANgHAIQgVAXiKAAQgmAAgmgWg");
	this.shape_7.setTransform(73,155.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AiGAyQgfgeAAgVQAAgpALgyIACASQACALACAEIAAAfIAEAXQACAFAFAIIACADQACAIAOAKIAPAKQANAJADAAIABAAQAmAWAmAAQCKAAAVgXQAHgIgFgNIgGgVIADgCQADgCAEAAIADABQAAAIACAFQAEAOADAFQgCAXgNAJQgkAbh5AAQhSAAgugrgACjAGQACADABAGIAAAHQgCgHgBgJg");
	this.shape_8.setTransform(71.3,156.3);

	// Layer 7
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#421F10").s().p("AA5AuIhkAAQg3gIgHgFQg0gOgSgQQgQgMAAgbIABgJQACAVANAMQASAOA0APQAHAFA3AHIBkAAIBUAAQAjgRAFgHIAFgFQADgEABgEIABAGQAAAIgFAJIgFAGQgFAIgjARg");
	this.shape_9.setTransform(71.3,162.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#65301B").s().p("AA6BDIhlAAQg2gIgIgFQg0gOgRgQQgOgMgBgTQABgQAGgEQACgIALgHIAYgKIADgBIAKgEIAGgDIARgGIA1AAIAeAFIALACIAuAHIAKACIAYAEIAkAHIAGACIAOAFIAKAFIAUAKIAKAGIADACIACABIADADQAOAHAEAMIABAJIAAABIAAAAIACAEQgBAEgCAEIgGAHQgEAHgjARg");
	this.shape_10.setTransform(71.2,158.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ai4AqQgTgXAAgrQAAgIAIgPIACgEQADgHAFgEIAIgIIAHgEQAJgLAggFIAWgGIAcAAIALgCIAeAAIASAHIABAAIAuAIIAqAIIARADIAEABIAYAFIAKAEQAmALASAMQAJAFAFAEQAHAFADAWIAAABQABAKgBALQAAAWgGALIgCAEQgTAlg3AAQhPAGgjAAQiSAAgug5gAh6hMIgHADIgJAEIgEACIgXAKQgMAHgCAHQgGAEgBAQIgBAJQAAAZAQAOQARAQA0AOQAIAFA3AIIBkAAIBUAAQAjgRAEgHIAGgHQAFgJAAgKIgBgGIgCgDIgBgBIAAAAIgBgIQgEgMgNgJIgEgCIgCgBIgDgDIgKgFIgUgLIgKgFIgOgFIgGgBIgjgHIgZgFIgKgBIgtgIIgMgCIgegFIg1AAIgQAGg");
	this.shape_11.setTransform(70.9,160.5);

	// Layer 10
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#BA986F").s().p("AA0A8IgGgCIgEAAIgCAAIgEAAQgkAAgVgNQgNgIgJgNQgIgNgEgWQgBgGABgNIACgUQAAgPAFAKQAGAKgCAIIAAAMQAGgFAKgGQAMgHAGABIgCgGQAAgOAGADQAGAEAAAMIAAACIAHAgQADANAMARQALARANALIAKAIQAAABAAABQAAAAAAABQgBAAAAAAQgBAAgBAAIgBAAg");
	this.shape_12.setTransform(93.4,154);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#D5AE81").s().p("AAUBJIgRgDIglAAIgDgBIg6gBIgFAAQABAAABAAQABgBAAAAQABAAAAgBQAAgBAAAAIgKgIQgNgLgLgRQgMgSgDgNIgHggIAAgCIAYgMIAVgIQABAFAFABIAJABIAPgEQAPgDATgCIAugBIAEgDQALgBAAgFQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQAsAEAiANQAhANAJAQQAMATAFANIgFgGQgGgGgEgDIgGgJQgNgNAAAOIASAcIAAACQAAAEAFAOQAFAMAAAFQAAADgRAJIg3ANg");
	this.shape_13.setTransform(108.8,153.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgKBLQgXgKgOgBIgDgCQgDgCAAgDQAAgBAAAAQAAgBAAgBQAAAAABgBQAAAAAAAAIAAgBQAAgFAKAAQAXANAjAAIAEgBIACABIAKACIAFABIA6ABIADABIAnAAIAQACIA4AAIA3gMQARgJAAgEQAAgEgFgNQgGgOAAgEIAAgCIgSgbQAAgOANANIAHAIQAEADAGAIIAEAFQANASAAAQQAAASgRAIIgBAAIgCABQgPALgfAHIgZAAIgFABIh/ACQhKgCgMgGgAkpg3IgBgDQAAgOAvgFQAvgHAAAJQAAAFgIACQgLACgnACQgGABgJAGQgIAFgFAAQgFAAgCgDgAAfg4QgGAAgBgFIAAgCQAAgFARgFQAcgGAFgDIBIACIADADQABAAAAAAQABABAAAAQAAABABAAQAAABAAAAQAAAGgLABIgEACIgwABQgUACgPAEIgPADIgIgBg");
	this.shape_14.setTransform(95.5,153.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#EACFB7").s().p("AAwAiIAEAAIAGACIgKgCgAguAkIAAgBIgCgCQgDgGgDgNQgCgGgBgIIAAgHQAAgSAFgHIADgDQADAXAJAMQAIANANAIQgJAAAAAFIAAABQgBAAAAAAQAAABAAAAQgBABAAABQAAAAAAABQAAADAEACgAgvAUIgBgHQgBgFgCgDQABAIADAHg");
	this.shape_15.setTransform(92.8,156.5);

	// Layer 9
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#421F10").s().p("AhgAiQgcAAgdgFQgdgGgHgHQgagXAAgGIABgNQAEAIAVASQAHAGAdAGQAdAGAcAAIDIAAQAkgEApgRQAJgEAFgFQAGgDAEgDQAIgHACgIIABAIQAAANgLAKQgEACgGADQgFAEgJAFQgpATgkADg");
	this.shape_16.setTransform(107.5,162.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#65301B").s().p("AhgA+QgcAAgdgGQgdgGgHgGQgVgUgEgIQACgJAFgGQADgOAWgQIAEgDIABgBIADgCQASgLAYgHIAMgDIASgEIAIgBIBfAAIAIABIAKABIAKABIABAAQAbAFApALIATAFQAzAPAXANIAEACIAGAEIADADQAJAAACAJQgCAIgIAHQgEADgGADQgFAFgJAEQgpATgkAEg");
	this.shape_17.setTransform(107.5,157.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("Ah1BcQhGgNgbgWQgVgRAAgeQAAggAXgRQADgMAfgPQAcgOAcgEIAagHIBEAAQAfACAMADQADAAABADQARABAAAJIAAACIgKgCIgKgBIgIAAIhgAAIgHABIgTADIgMAEQgYAGgSAMIgCABIgCABIgDADQgXAQgCARQgGAFgCAIIgBANQAAAGAbAZQAGAGAdAGQAdAGAcAAIDJAAQAkgEAogTQAJgEAGgFQAGgDAEgDQALgKAAgNIgBgHQgDgKgJgBIgDgCIgFgEIgFgDQgXgNgygOIgUgGQgogLgcgEIAnAAIAMABQALgFA3ATIAPAGQAtANAJAKQAJAEADAMQADAKgCAJQAAAfgUAQQgeAYhZANg");
	this.shape_18.setTransform(106.8,159.3);

	// Layer 2
	this.instance = new lib.Girl02copy();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#BA986F").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQAcgyAMg8QAogfAKh+QAEg0gChDIAAgHIAAgCIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape_19.setTransform(15,71);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#D5AE81").s().p("AouE4IgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAHoA6QACgBgGgRQgGgSgEgBIgBAAIAAgBIgBgHIAAgcQABgYgCgTIgFgpQgCgNABgCQARggAHgUQALgcAEgbQAIgxACgZIAAgQIAMgBIANADIAAAMQACAmAMBYQAJA7ABAXQAAANgBAOQAAAnAQAhQAGAQAZAqQApBEgRBCQgMAqg6ABQgjgBgGABIgJADQgJh8gQhCg");
	this.shape_20.setTransform(78.2,71.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AnzF6QgZgBgVgDQhbgWgYgTQgrgjgLhMQgIgxAJgcQAGgSAUgcQAdgoARg2QAGACAHAAQARAAANgIQgOA8gcAyQgKAUgRAcQgLAUADANQAPBBAqAoQAUAUAWAKIAOAFQAKADALABIAPABQBTAGAggeIAHgHQAPgSgFgVIgBgCQgDgLghg+IgJgRQgbgygCgIIAAgBQgDgVAYhtQAXhtAahAIAIgUIgMgJQgugigbgKIgTgIQgOgGgLgBQgFgBgIgDIgMgFIAAACIgDgzIAJADIA5AUIAjAPQAKAFALALIApApIABACIAQgCIAJgPQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAlQgGASgGAJQAuAJAyAIIAGA1QhYgPghgVQgHAQgPAgQAJATAUAlQAIAPALAiQAQAxgFAVQgFAUgYAVQgRAOgKAFIgKAFQgdANglAAIgKAAgAmRgrQgYA1gDAsQALgNAIgEIgDgFQgBgCAAgNQAAgIAKgiIAJggIgHAOgAIrE6Ig4ABIgBAAIgOjhQgRACgZgCIgFgyIAfABQgHgagCggIgDgfQgCgLADgNIgOACIgKABQABgXAEgWIAIgIQAngiAKg6QAEgYgCgZQAOgPARgPIAEgBIADABIASgXIAJgEQANgHALgDIgGgPIAKgKIAXgWQAGAWAAAWIAAAIIAAAEIgBAGIgBACIgCACIgBABIgLAFQgIADACAFIAHASIAMADQAGA3AKA4QAIAyABA0QACBCARAqQAJAVAZAgQAUAaAEAXQAFAigQA9QgJAgglAXQghAUgoAAQgQAAgRgDgAIFA/QAFARgCABQARBCAJB8IAJgDQAGgBAiABQA7gBALgqQAShCgqhEQgZgqgGgQQgQghAAgnQACgOgBgNQgBgXgJg7QgMhYgCgmIAAgMIgNgDIgLABIgBAQQgCAZgHAxQgEAbgMAcQgHAUgQAgQgBACACANIAFApQACATgBAWIgBAeIABAHIABABIABAAQAEABAGASg");
	this.shape_21.setTransform(74.9,69.1);

	// Layer 1
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#BA986F").s().p("AC3COQgWgOgFgpQgDgPAAgzQAAghADgLQACgJAKgIQAcgYAdgsIgVD6gAixALQgYhggXgtQAugJAigCIBEEWIgIAAQhAgqgdhUg");
	this.shape_22.setTransform(72.4,141.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#D5AE81").s().p("ABWCTIAVj6QANgTAOgYQA/ABAaADIAoAFQgJAVgcB3QgTBUgyA8gAkGiIQA2gEAxAOQBGATATADIAAB7QgCAEACAZIAEAkQgBAPgFAMIgIAMQgJALgPAGIhaACg");
	this.shape_23.setTransform(84.2,140.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAlB8QgHgdABgmIADgjIAAhNQAFgVAWgPIApgdQAHgHAQgjQAMgbAQgBIABAAQCggHAeAbQAKAJgEAPQgCAIgHANIgSBTQgMA3gOAkQgrBthoAdQgQAFgOABIgIAAQg8AAgPhFgACShqQgdAsgcAYQgKAIgDAJQgCALAAAhQAAAzACAPQAGApAWAOIAUAAIBIAAQAyg8AThTQAch4AIgVIgogFQgagCg/gCQgNAYgNATgAiSC1QgogBgXgMQgYgOgRAAQgKAAgDgCIgHgGQAAgegohvQgohtAAgmQAAgOAPgFQALgFBKgNIARAAQBTADA8AMQBBANATAWQANASABAYQAAAKgCAcQAAAdgCAUQgCAbgGAxQgBAHgMAKIgRAPQAGgMAAgPIgDgjQgDgZACgFIAAh7QgSgDhGgTQgygOg1AEQgjACguAJQAYAtAYBgQAcBUBAAqIAIAAIBagBQAPgHAKgLQgqA9g9AAIgCAAg");
	this.shape_24.setTransform(80.4,140.9);

	this.addChild(this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.instance,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,-2.6,143.9,173);


(lib.Peppa_Head = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#491616").s().p("AhFHRQhzhMgcgNIgTiIQgMhSABggQAAhWAHgoQAGgpAXhDQBckTByiAQAbggAggaQAkCJA9C2QAyCVAmBjIgrAAQgNAGgLAQQhFAYgMCbQgEA6AEBMIAFBRIADBOQADA6AFAUQhJgihshHg");
	this.shape.setTransform(29.7,75.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330F0F").s().p("ABNC3IgDAAQgYAAADhDQgBgJABgJQgHg3gJhbIgMgnIgBgEQgeBLgHAaIgcBmQgVBCgVABQgGAEgGAAIgBAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQABgPAEgVIACgJIABgGIAKgpQAFgUAHgWQALgpANghQAKgeAMgaIACgCQANggAOgZQAFgJAFgIIALgOIAEgHIADgDIAAABIAGALIAHARIAEALQAFARAEAVQAFATACAXIADAhIAAAGQAEBIAHAwQACATADAPQABAOADAKIAAAgIADAAIgLACQgFgBgEgCg");
	this.shape_1.setTransform(71.3,62.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6B2121").s().p("AD3FcQhvgTgmgCIjkAAQgDgPgCgTQgHgwgEhKIAAgGIgDghQgCgXgFgTQgEgVgFgSIgEgKIgHgRIgGgLIAAgBIgDADIgEAHIgLAOQgFAIgFAJQgQAZgNAgIgCACQgMAagKAeQgNAjgLApQgHAVgFAVIgKApIhTAAQgmhjgyiWQg9izgkiJQCjiKD4ABQCmAADCBZQDNBfBZCFQAeAuAXBUQAXBYAJBnQABALgDAvQgCAhAJAXQkLgRhAgMg");
	this.shape_2.setTransform(93.5,42.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AljKtQhDgchJgwQhTg6gxgfQgMgJgfgLQgYgJgNgOIgOgQIgKAAIgDgEQgFgDgDgTQgXhogGhiQgKizArigQBNkhDnimQCJhiC5gWQA1gHA1ABQB1gBB1AhQCjArB+BiQB/BjBBCHQBECMgICdIgGBWIgDgCIAAAKIgDANQAFAGACAIIgLAZIiKAAIi2gWQiWgUgtgCIjOAAIgDAAIAAggQgDgLgBgOIDmAAQAmACBvAUQBAAMELAQQgJgWACgiQADgvgBgJQgJhmgXhYQgXhXgegtQhZiGjNhfQjDhZilAAQj4gBijCKQggAagcAgQhzCBhcESQgXBBgGApQgHAqAABWQgBAgAMBSIATCIQAcANBzBMQBuBHBJAiQgFgTgDg7IgDhOIgFhRQgEhMAEg6QAMiaBFgYQALgRANgIIArAAIBTAAIgBAHIgCAJQgEAUgBAPQAAABgBAAQAAABAAAAQAAABAAAAQAAABAAAAIAAACIhwAAQgtBegJCIQgDAjADEPIADAIQgFAOgDAFQgGAGgJAAQgGAAgGgDg");
	this.shape_3.setTransform(77.4,68.9);

	this.instance = new lib.PeppaHead();
	this.instance.setTransform(79.6,108.3,1,1,0,0,0,69.8,62);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgHFXIg7gCQhAiGgHgpQgDgZAOh8IAKhfQAHg2ANgmQAihkBphIIBFAIQAUA4AFBoQAFB3gODAQgBAWgNBoQgGAwgJAKQgUAWhHAAIgPAAgAA3EyQARgaALg5QADgUAIhIQAPiAgBhLIgJjaQhzAMgsB0QgPApgIA6QgEAggEAzQgJBSAHArQAHAiAgB9QAUACAcgBIA9ABg");
	this.shape_4.setTransform(139,103.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#6B2121").s().p("AgLEqQgcAAgUgBQggh+gHgiQgIgrAJhSQAFgyAEggQAIg6AOgpQAsh1BzgLIAJDaQABBNgOB+QgIBIgEAUQgKA5gRAZIg9AAg");
	this.shape_5.setTransform(139.6,104.2);

	this.addChild(this.shape_5,this.shape_4,this.instance,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,154.9,170.3);


(lib.pallet_half_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.highlight_mc = new lib.pallet_highlight();
	this.highlight_mc.setTransform(48.7,41.6,1,1,0,0,0,48.7,41.6);
	this.highlight_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.highlight_mc).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.OptionsBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Options_btn();
	this.instance.setTransform(34.9,34.9,1,1,0,0,0,34.9,34.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AkLEMQhvhvAAidQAAicBvhvQBPhPBmgXQAqgJAsAAQCdAABvBvQBvBvAACcQAACdhvBvQg6A7hHAbQhAAZhLAAQicAAhvhvg");
	this.shape.setTransform(34.8,34.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regX:34.9,regY:34.9,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regX:34.8,regY:34.8,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 2
	this.instance_1 = new lib.shadowCircle();
	this.instance_1.setTransform(39.1,39.1,1,1,0,0,0,36.9,36.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:true},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,77.9,77.9);


(lib.OKbutton = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.OK_btn();
	this.instance.setTransform(98,31.7,1,1,0,0,0,67,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AqrFDIAAqFIVXAAIAAKFg");
	this.shape.setTransform(98.2,30.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(29,-2,138.2,67.5);


(lib.NoBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.No_Btn();
	this.instance.setTransform(99,31.7,1,1,0,0,0,99,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AtGEtIAApaIaNAAIAAJag");
	this.shape.setTransform(80,30.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1,x:99}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85,x:99.1}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,174.2,67.5);


(lib.newmessage_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.NewMessageStatic();
	this.instance.setTransform(150.5,18.8,1.52,1.52,0,0,0,103,21.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A5gFrIAArVMAzBAAAIAALVg");
	this.shape.setTransform(153.2,25.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1.52,scaleY:1.52,x:150.5}}]}).to({state:[{t:this.instance,p:{scaleX:1.29,scaleY:1.29,x:150.4}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,308.6,58.4);


(lib.Male_Body_03_Front_Driving = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#826632").s().p("AkeB1QhWgTgSgxQgFgQgBgaIgBgmIgEgiQgFgjgGgTQAUAIAXAHQAwAOA2AJQgFC2AcAcQgvgKAFgCgAFeA0QgQgCgXgJQAAAAAAAAQAAAAAAgBQAAAAAAAAQAAgBAAAAIAAgEQAAgTgFgcIgDgPIgCgGQgGgbgIgbIgKgdIgDgIIAEgBIBYgDQAYCtAXAGIgnACIgYgBg");
	this.shape.setTransform(71.2,138);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A88541").s().p("AmDCWQgrgDg1gMIgCAAQgcgcAEi2IAJABQBoARB/AAQB+AABGgVIASgGIACAxIgBAVIgCAWIgCgBQhKgwimAMQgxADgmAJIgdAIQgxAOAAAUIADANQAFAMANgGQBMgkBWgEIAcgBQAZAAATAEQAoAEAdAGIAYAHQAWAHATAOQAZASAEA9IAAARQgBAEgFAFgACsA5QgXgGgYiuIADAAQB7gIBogSIABAAQAAAbADANIgDgBQg4gLguAHQgpAHgIARQgCAEAAAFQAAAGAEAEIARAOQAEAAAcgMQAbgMAfAAQB3AAAsAwIAGAHQAWAcAFAwIgUAHg");
	this.shape_1.setTransform(95.3,137.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#1A2D3A").s().p("AlOA1IgugMQgJgygOgyIgEgRQAhAKAzALQAqAJApAIIASBpIACAKQg6gKg4gOgAE7AyIgGgQQADgbAAgRIAAgKIBWgBIAAAHQAAANAKAwQgsADgcAAIgVAAg");
	this.shape_2.setTransform(66.6,117.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#2B4B60").s().p("AmnA7IgBgKIgThpQBsATBgAEIEugEQgFAjgKA4IgHABIhGALQhUALhJAAQh7AAhygSgADDggIAAgHIBAgBQBsgPBNgVQgEAUgBAxQhpAYhiAJIgeADQgLgwAAgNg");
	this.shape_3.setTransform(86.9,119.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AkVEKQhLgKgsgKQgdgGgqgNQgtgJgegmQgRgXAAg3QAAgPgPhFIgHgcQgNgqgCgRIgFgKQAAgGACgEQgOhcAAgbIAAgMIgDgBIgMgFQgFADgKAAQgPAAgFgVIgCgLIgBgKICpAVIALAVIAeAQIAeAGQBoAABlAKQGKAFGHgvIAGAIIgYAJIgFAKIABAAIgEAJIgDAFIgBADQgSArgCAjIgBALQAGAXAAAjIgBALQAvAPAjAZIALAJIALALIgBAAQAlAmAIAzQADAVgFAKQgKASglABIlAABQgEgCgpgDQgsgEgXgFQgNgDgMgIQgKgGAAgDQAAgFACgFQgKgPgLgsIgCgJQgGgfgEgiIgDgSIgPBNIAAASQgBAigDAMQAGANAGAbQAEAYAAAOQAAAhgCAIQgHAagdAGgAoRAEIAFAjIABAnQABAbAFAQQASAxBWASQgFADAvAKIACAAQA1AMArADIFOAAQAFgFABgEIAAgRQgEg9gZgSQgTgOgUgHIgYgHQgdgGgogEQgTgDgZgBIgcABQhWAEhMAkQgNAGgFgMIgDgNQAAgUAxgQIAdgIQAmgJAxgDQCmgMBIAyIACABIACgYIABgVIgCgvIgSAGQhEATh+AAQh/AAhogPIgJgBQg2gKgvgOQgYgHgTgHQAFATAFAggAHVhSQhoASh7AHIgDAAIhYAEIgEAAIADAJIAKAdQAIAYAGAbIACAHIACAPQAGAeAAASIAAAEQgBABAAAAQAAABAAAAQAAAAABAAQAAABAAAAQAXAIAQACQAWADApgDIE+AAIAUgHQgFgwgWgeIgGgGQgsgvh3AAQgfAAgbAMQgcAKgEAAIgRgMQgEgEAAgGQAAgFACgEQAIgRApgHQAugHA4ALIADABQgDgNAAgbIgBABgApAjBQAOAzAJA0IAuALQA4APA6AJQByATB7AAQBJAABTgMIBHgKIAHgCQAKg5AFgkIkuAFQhggFhsgTQgpgHgqgKQgzgLghgJIAEAQgAEhicIhAABIhWACIAAAJQAAAUgDAaIAGAQQAeACBAgFIAegDQBigJBpgaQABgwAEgVQhNAVhsAPg");
	this.shape_4.setTransform(83.9,130.9);

	// Layer 1
	this.instance = new lib.StaySafe();
	this.instance.setTransform(56.5,31.6,0.08,0.08,-9,0,0,109.9,97.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#282828").s().p("ApbEvIgEAAIAAgWQAAgUgBgOIABAAQgCgMAAgPQAAgJACgLIABgCIAAgDIgCgBIgBgBQAAgFACgEIACgDIAAAAIACgHIAIglQA2ArBNAWIANADIgBBGQgCAggDAXQiHgbgLAAgApNDMIAAAAIAAAAgAmPCmQgYgGgWgHQg+gUgtggQgegWgWgaIgMgPQgug7gIhUIAAhcIAAgGQAAgxADgpQAdCtBoBYQASAPAVANQBUAyCOATIDCAPIACANQADAPAZArIAIAAIAAABQAAAUAMAHQhYAIhMAAQh4AAhagVgAJmhLIgEgKQgPgogYguQgWgrgVghQgHgPgGgVIgCgLIgHgjIAnAuQApAwAVAiQAbAqAlBSIgBAAIgBABQgcAPgSANIgJgbg");
	this.shape_5.setTransform(82.5,73.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#333333").s().p("Am/HwIgYgEIgZgGQAEgWABghIABhGIACACIABgBQAUAFAWAEQBbAPB1gIIDDAAQAPgJADgGQACgFAAgTQAAgYgBgHIAAgBICEAAQBNgPAThOQAFgUABhjIAAgCQABhbhWglQhRgmhJAkIgEACIgJAKIgNAPQgEgCgDgEIhXAAIABgFQABgIAAgiQAAgUgHgwIgDgUQgNhXgXhKQg3i2hXgjIEaAAQBIAMA/AOQANAUAdApQA6BTARAwQANAlAJBEQAFAwAHAeIAGAYQAMAlAfCdIACALQAZCAAmBHIDcAAIABAAIACAKQALBDASA0QhAALhJAfQhpAuiegCImzAPQhAgFiUgdgAkzDeQiOgThUg0QgVgNgTgQQhnhXgdisQAEg1AJgpIAIgcIACgIIAEgMQAJgVAMgWQAyhHA8gvIAIgGIAHABQAcAHALAGQA6AfAECEQABAnAEBEIACAeIAAAAIgCACIgCAEQgEAGABANIAAAJIgCAHQgIAWAAAbQAAAJACACQACACALADIgBAIQABAIAHAIQAKAJATAGQAYAJAnACIAFgEIABgDQAVAHAWAAIDIAAQgTAkgJAtQgIAlAAAsIABAzIjCgPgAI5gtQgWgigogwIgngtIgCgKQgDgLgFgIQgBg1grhmIgGgNQATAQAQARIBGBUQBJBTAjArIAcAkQAzBKAGA9QggAKgqAXQgkhRgbgqg");
	this.shape_6.setTransform(86.3,58);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFF91").s().p("AEhDYIgFgdIggi8IgEgXQgsjUhmiAQBzAdBIAlQAKAbAfBBQAdA8APAsIgBAAQgBASAAAWQAAAYAMAmQAPAsAjBDIAwBiQAWAuADAMQAJArALBHQg0AAiRAFQgWhBgThpgAlYBjQgRgPgsAAIgFgCQALgPAEgWQACgLAAgnIAAgIQAAhqgXhSQgih+hMgYQAcgQAegLIA0gCIAHgEIADgBQBwBkAnCQQAPA4AJBRIAAAAIALBwQhMgCgwgHg");
	this.shape_7.setTransform(89.9,45.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#B20400").s().p("AD5BaIgPhAQAhgLAxgdIAZgRQAIAOAZAfQAmAxAAAtQAAAYhaAIQgoADgWAGIgLg7gAmOBfIgLgeQgCgFAAgPQAAhGACgQIAAAAQAFgnAXg6QCBgjAiBQQAJATACAaIABARQABAUgCATIABAgQAAAOgHAJQgQAVhTAVg");
	this.shape_8.setTransform(123.2,72.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#192D3A").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_9.setTransform(67.5,124.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#2B4A60").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_10.setTransform(86.9,128);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#5A996E").s().p("AFIAnIAAgBIAAABgAk/gLIgIgTIABgCIABgGQAGAOAHANg");
	this.shape_11.setTransform(47,80.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_12.setTransform(86,143.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_13.setTransform(94.8,140.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#EAD0B7").s().p("ABNAEIACABIAAABIgCgCgAhOgBIABgEIACACIgBADIgCgBg");
	this.shape_14.setTransform(29.4,93.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("Ak1MJQjEgig5gUQg8gUgBgmQg0kZAAg+IAAgMIgPgGQgFACgKAAQgSAAgEggIAAgiQABhoANggQAIgTALgKIgOgQQgRgTgNgVIgEgIQgHgLgHgPQgVgzgHhAQgCgYgBhcQAAhaAEgpQAIhJAWg5IAIgTQAihMBBg0QBzhnCbgOIAAgFIEcAAQEtAwCCBNQA2AfArAuIBMBYIBxCiIAAABQBYBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA7BQAABDQAAAkgEAHQgMAQg5APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThCAaIgMAaQgSAsgCAjQgCATAFAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk7AAQgtACgagYQgSgRgBgSQgIgXgMg2IgCgMIgBAEQgUBegTAmIADAEIAAAAIABABIAAAEIAAAIQAAAYgbAWQgfAYguAGgAo4KgQALAMBZAQQCSAZASAEIBhAAIB1AAQASgKAagSIhdgXQh6gghdAAQgUAAhIAHIgEAAQhSAHgPAFIgZAAIAEAHgAhFKDQAfALAXAMQAFgkARhtQASheAGgyIkuAEQhggEhrgTQgpgIgrgJQgzgLghgKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQClAAB7AtgAFXIWQhAAIhaAsQgfAPgiATQAIAGgBADQAFADArAOIEwAAIAKgBIADgBQASgDAcgCQg9hrhtAAQgPAAgOACgADkF2IhAABIhXABIAAAKQAAATgCAbQAIASAIAeQAMAwADAuQATgUAhgQQARgIARgHQA/gXBRgDQAogBAjAEIgDgMQgBgMAAghQAAhOAFgbQhNAVhrAPgAqqDvIAAAWIADAAQALAACHAbIAZAGIAYAEQCTAeBAAEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDIgCgKIgBABIjdAAQgmhIgZh9IgCgLQgfiggMgkIgGgZQgGgegGgwQgIhEgNgkQgSgxg6hTQgdgpgMgUQg+gOhKgMIkaAAQBYAkA3C1QAWBKAOBYIACAUQAIAwgBATQAAAiAAAJIgBAEIBXABQACAEAEABIAOgPIAJgKIADgBQBJglBRAmQBXAngBBbIAAACQgBBjgGASQgTBOhMAQIiEAAIAAAAQABAHAAAYQAAATgCAFQgEAHgOAJIjDAAQh1AIhbgQQgWgEgVgFIgCgBIgNgDQhNgWg1gqIgJAlIgCAHIAAAAIgBADQgDADAAAFIABABIgBAEIADABIAAACQgDAKAAAJQAAAQACALIgBAAQACAPAAATgApIp+Qg9AvgyBIQgMAVgIAVIgFANQgFAQgEATQgJApgEA1QgDApAAAxIAAAGIAABdQAIBUAtA9IAMANQAXAaAdAVQAuAgA9AVQAWAHAZAGQCTAiDigWQgMgGABgVIAAAAIgJAAQgYgrgEgPIgBgNIgBgxQgBgrAIgmQAJgtAUgkIjJAAQgVAAgVgHIgCAEIgEADQgogCgYgIQgTgHgJgLQgHgHgBgJIABgIQgMgDgCgBQgCgDAAgJQAAgbAIgWIADgHIgBgJQAAgNADgGIADgEIABgCIAAAAIgBgeQgEhEgCgnQgDiEg6gfQgMgGgcgGIgGgCIgIAGgAKIhDQgyAgggAKIAOA/IALA7QAXgGAogEQBZgIAAgXQAAgtglgwQgZghgIgOIgZARgAhlA/IABAAIgBgBgAD2ldIAEAXIAgC+IAFAdQATBpAWA/QCRgFA0ABQgLhGgJgrQgDgMgWguIgwhiQgjhDgPguQgMgmAAgYQAAgWABgRIABAAQgPgtgdg8QgfhBgKgaQhIgmhzgdQBmCAAsDUgAgxi/QgXA7gFAnIAAAAQgCAPAABIQAAAOACAFIALAdIBUAKQBTgVAQgUQAGgJAAgNIgBgfQACgWAAgTIgBgSQgCgagJgTQgYg3hGAAQgcAAgnAKgAF4oqQArBmABA1QAFAJADAKIACAKIAHAjIACAKQAGAWAHAPQAVAgAXAsQAXAuAQAnIADALIAJAbQATgOAbgOIACgBIAAAAQApgWAhgKQgGg/gzhLIgdgkQgjgqhIhUIhGhTQgRgSgSgQIAFANgAmbrEIgHADIg0ADQgeALgcAQQBMAXAiB+QAXBTAABqIAAAHQAAAqgCAKQgEAXgLAOIAFACQAsAAARAPQAwAHBMACIgLhxIAAgBQgJhQgPg5QgniQhwhkIgDACgAqZCjIAAAAIABAAg");
	this.shape_15.setTransform(90.1,77.7);

	this.addChild(this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.instance,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(11.3,0,157.6,157.5);


(lib.Male_Body_02_Front_Driving = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3A3A3A").s().p("AlOA1IgugMQgJgygOgyIgEgRQAhAKAzALQAqAJApAIIASBpIACAKQg6gKg4gOgAE7AyIgGgQQADgbAAgRIAAgKIBWgBIAAAHQAAANAKAwQgsADgcAAIgVAAg");
	this.shape.setTransform(66.6,117.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#494949").s().p("AmnA7IgBgKIgThpQBsATBgAEIEugEQgFAjgKA4IgHABIhGALQhUALhJAAQh7AAhygSgADDggIAAgHIBAgBQBsgPBNgVQgEAUgBAxQhpAYhiAJIgeADQgLgwAAgNg");
	this.shape_1.setTransform(86.9,119.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#432010").s().p("AkeB1QhWgTgSgxQgFgQgBgaIgBgmIgEgiQgFgjgGgTQAUAIAXAHQAwAOA2AJQgFC2AcAcQgvgKAFgCgAFeA0QgQgCgXgJQAAAAAAAAQAAAAAAgBQAAAAAAAAQAAgBAAAAIAAgEQAAgTgFgcIgDgPIgCgGQgGgbgIgbIgKgdIgDgIIAEgBIBYgDQAYCtAXAGIgnACIgYgBg");
	this.shape_2.setTransform(71.2,138);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#65301B").s().p("AmDCWQgrgDg1gMIgCAAQgcgcAEi2IAJABQBoARB/AAQB+AABGgVIASgGIACAxIgBAVIgCAWIgCgBQhKgwimAMQgxADgmAJIgdAIQgxAOAAAUIADANQAFAMANgGQBMgkBWgEIAcgBQAZAAATAEQAoAEAdAGIAYAHQAWAHATAOQAZASAEA9IAAARQgBAEgFAFgACsA5QgXgGgYiuIADAAQB7gIBogSIABAAQAAAbADANIgDgBQg4gLguAHQgpAHgIARQgCAEAAAFQAAAGAEAEIARAOQAEAAAcgMQAbgMAfAAQB3AAAsAwIAGAHQAWAcAFAwIgUAHg");
	this.shape_3.setTransform(95.3,137.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AkuD3QhLgKgsgKQgdgFgqgOQgtgJgegmQgRgWAAg4QAAgPgPhEIgHgcQgNgqgCgSIgFgKQAAgFACgEQgOhdAAgaIAAgNIgDgBIAIgGQBgAHBdAVQBeATBiANQBtARBxgJQAmgFApAAIAIgFQCrALCighQA/gMBAgCIgIAQQgSArgCAjIgBALQAGAXAAAjIgBALQAvAPAjAbIALAIIALAKIgBAAQAlAmAIAzQADAVgFAKQgKASglACIlAAAQgEgCgpgDQgsgEgXgFQgNgDgMgIQgKgFAAgEQAAgFACgFQgKgPgLgsIgCgJQgGgfgEghIgDgTIgPBNIAAASQgBAjgDALQAGAOAGAaQAEAYAAAOQAAAhgCAIQgHAbgdAFgAoqgNIAFAhIABAoQABAaAFAQQASAxBWATQgFACAvAKIACAAQA1AMArADIFOAAQAFgFABgEIAAgRQgEg9gZgSQgRgOgWgHIgYgHQgdgGgogEQgTgDgZAAIgcAAQhWAFhMAjQgNAHgFgNIgDgNQAAgUAxgQIAdgIQAmgJAxgDQCmgLBIAxIACABIACgYIABgVIgCgvIgSAGQhEAWh+AAQh/gBhogQIgJgCQg2gJgvgPQgYgGgTgIQAFATAFAigAG8hlQhoASh7AHIgDAAIhYAEIgEABIADAIIAKAdQAIAaAGAaIACAGIACAPQAGAeAAATIAAADQgBABAAAAQAAABAAAAQAAAAABABQAAAAAAAAQAXAJAQACQAWACApgDIE+AAIAUgHQgFgwgWgeIgGgGQgsgvh3AAQgfAAgbAMQgcANgEAAIgRgPQgEgEAAgGQAAgFACgEQAIgRApgHQAugHA4ALIADABQgDgNAAgaIgBAAgApZjUQAOAzAJA0IAuALQA4APA6AKQByASB7AAQBJAABUgMIBGgKIAHgCQAKg5AFgjIkuAEQhggFhsgTQgpgHgqgJQgzgLghgKIAEAQgAEIivIhAACIhWABIAAAKQAAATgDAbIAGAPQAeACBAgFIAegDQBigJBpgaQABgwAEgVQhNAVhsAPg");
	this.shape_4.setTransform(86.4,132.8);

	// Layer 1
	this.instance = new lib.StaySafe();
	this.instance.setTransform(56.5,31.6,0.08,0.08,-9,0,0,109.9,97.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#282828").s().p("ApbEvIgEAAIAAgWQAAgUgBgOIABAAQgCgMAAgPQAAgJACgLIABgCIAAgDIgCgBIgBgBQAAgFACgEIACgDIAAAAIACgHIAIglQA2ArBNAWIANADIgBBGQgCAggDAXQiHgbgLAAgApNDMIAAAAIAAAAgAmPCmQgYgGgWgHQg+gUgtggQgegWgWgaIgMgPQgug7gIhUIAAhcIAAgGQAAgxADgpQAdCtBoBYQASAPAVANQBUAyCOATIDCAPIACANQADAPAZArIAIAAIAAABQAAAUAMAHQhYAIhMAAQh4AAhagVgAJmhLIgEgKQgPgogYguQgWgrgVghQgHgPgGgVIgCgLIgHgjIAnAuQApAwAVAiQAbAqAlBSIgBAAIgBABQgcAPgSANIgJgbg");
	this.shape_5.setTransform(82.5,73.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#333333").s().p("Am/HwIgYgEIgZgGQAEgWABghIABhGIACACIABgBQAUAFAWAEQBbAPB1gIIDDAAQAPgJADgGQACgFAAgTQAAgYgBgHIAAgBICEAAQBNgPAThOQAFgUABhjIAAgCQABhbhWglQhRgmhJAkIgEACIgJAKIgNAPQgEgCgDgEIhXAAIABgFQABgIAAgiQAAgUgHgwIgDgUQgNhXgXhKQg3i2hXgjIEaAAQBIAMA/AOQANAUAdApQA6BTARAwQANAlAJBEQAFAwAHAeIAGAYQAMAlAfCdIACALQAZCAAmBHIDcAAIABAAIACAKQALBDASA0QhAALhJAfQhpAuiegCImzAPQhAgFiUgdgAkzDeQiOgThUg0QgVgNgTgQQhnhXgdisQAEg1AJgpIAIgcIACgIIAEgMQAJgVAMgWQAyhHA8gvIAIgGIAHABQAcAHALAGQA6AfAECEQABAnAEBEIACAeIAAAAIgCACIgCAEQgEAGABANIAAAJIgCAHQgIAWAAAbQAAAJACACQACACALADIgBAIQABAIAHAIQAKAJATAGQAYAJAnACIAFgEIABgDQAVAHAWAAIDIAAQgTAkgJAtQgIAlAAAsIABAzIjCgPgAI5gtQgWgigogwIgngtIgCgKQgDgLgFgIQgBg1grhmIgGgNQATAQAQARIBGBUQBJBTAjArIAcAkQAzBKAGA9QggAKgqAXQgkhRgbgqg");
	this.shape_6.setTransform(86.3,58);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#A88541").s().p("AD5BaIgPhAQAhgLAxgdIAZgRQAIAOAZAfQAmAxAAAtQAAAYhaAIQgoADgWAGIgLg7gAmOBfIgLgeQgCgFAAgPQAAhGACgQIAAAAQAFgnAXg6QCBgjAiBQQAJATACAaIABARQABAUgCATIABAgQAAAOgHAJQgQAVhTAVg");
	this.shape_7.setTransform(123.2,72.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AD5BRIggi7ICEABQAPAuAiBBIAxBiIjBAHIgFgegAl8gFQgQgOgsAAIgGgCQAMgPAEgWQABgLABgpICiABIAAAAIALBwQhNgDgwgFg");
	this.shape_8.setTransform(93.4,55.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FE9932").s().p("AEhDYIDBgHQAWAuADAMQAJArALBHQg0AAiRAFQgWhBgThpgAD8gBIgEgXQgsjUhmiAQBzAdBIAlQAKAbAfBBQAdA8APAsIgBAAQgBASAAAWQAAAYAMAmgAmJgFIAAgIQAAhqgXhSQgih+hMgYQAcgQAegLIA0gCIAHgEIADgBQBwBkAnCQQAPA4AJBRg");
	this.shape_9.setTransform(89.9,45.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#192D3A").s().p("AmciDIgEgRQAhAKAzALQAqAJApAIIASBrQAQBcADAqIg3AFQhYAIgUAEQAAiRgliGgAE8ALQgHgcgJgTQADgbAAgTIAAgKIBWgBIAAAIQAAAQAQBFIAMA2QgRAGgQAIQghAQgTAUQgEgugMgvg");
	this.shape_10.setTransform(67.5,124.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#2B4A60").s().p("AglCMQh7gtilAAQgeAAgzAEQgDgrgPhbIgThsQBsATBgAFIEugFQgHAzgRBcQgUBtgFAkQgUgMgfgMgADTggQgQhGAAgQIAAgIIBAgBQBsgPBNgVQgFAcAABNQAAAiABALIADAMQgjgEgoACQhRACg/AWIgNg1g");
	this.shape_11.setTransform(86.9,128);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#5A996E").s().p("AFIAnIAAgBIAAABgAk/gLIgIgTIABgCIABgGQAGAOAHANg");
	this.shape_12.setTransform(47,80.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#421F11").s().p("AkIBHQgSgEiRgZQhagQgLgMIgEgHIAZAAQAPgFBTgFIADAAQAsAjCIAcIA7ALgADbgKQgrgOgEgDQAAgDgHgGQAhgTAfgPQAnAgCCAQQAZADBuAIIgJABg");
	this.shape_13.setTransform(86,143.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#65301B").s().p("Aj+BiIg8gLQiHgcgtgmQBJgGATAAQBeAAB6AgIBdAXQgaASgSAKgAE2ADQiBgPgngfQBagsA/gIQCDgPBFB2QgcACgSADIgEABQhtgIgagDg");
	this.shape_14.setTransform(94.8,140.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#EAD0B7").s().p("ABNAEIACABIAAABIgCgCgAhOgBIABgEIACACIgBADIgCgBg");
	this.shape_15.setTransform(29.4,93.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("Ak1MJQjEgig5gUQg8gUgBgmQg0kZAAg+IAAgMIgPgGQgFACgKAAQgSAAgEggIAAgiQABhoANggQAIgTALgKIgOgQQgRgTgNgVIgEgIQgHgLgHgPQgVgzgHhAQgCgYgBhcQAAhaAEgpQAIhJAWg5IAIgTQAihMBBg0QBzhnCbgOIAAgFIEcAAQEtAwCCBNQA2AfArAuIBMBYIBxCiIAAABQBYBeABBGIABAAIADAFQADAEAAALQAAALgPAQQAKAJAMAQQA7BQAABDQAAAkgEAHQgMAQg5APIh1AAQAFAsAAAiQAAAzgWANIgDgHQgbAThCAaIgMAaQgSAsgCAjQgCATAFAnQAAAQgFANQAjAIAdAOQBRAnAABNQAAARgSAOQgdAWhCgDIk7AAQgtACgagYQgSgRgBgSQgIgXgMg2IgCgMIgBAEQgUBegTAmIADAEIAAAAIABABIAAAEIAAAIQAAAYgbAWQgfAYguAGgAo4KgQALAMBZAQQCSAZASAEIBhAAIB1AAQASgKAagSIhdgXQh6gghdAAQgUAAhIAHIgEAAQhSAHgPAFIgZAAIAEAHgAhFKDQAfALAXAMQAFgkARhtQASheAGgyIkuAEQhggEhrgTQgpgIgrgJQgzgLghgKIAFARQAlCIAACRQATgFBZgHIA2gFQAzgDAeAAQClAAB7AtgAFXIWQhAAIhaAsQgfAPgiATQAIAGgBADQAFADArAOIEwAAIAKgBIADgBQASgDAcgCQg9hrhtAAQgPAAgOACgADkF2IhAABIhXABIAAAKQAAATgCAbQAIASAIAeQAMAwADAuQATgUAhgQQARgIARgHQA/gXBRgDQAogBAjAEIgDgMQgBgMAAghQAAhOAFgbQhNAVhrAPgAqqDvIAAAWIADAAQALAACHAbIAZAGIAYAEQCTAeBAAEIG0gPQCeACBpgtQBJggA/gLQgRg0gLhDIgCgKIgBABIjdAAQgmhIgZh9IgCgLQgfiggMgkIgGgZQgGgegGgwQgIhEgNgkQgSgxg6hTQgdgpgMgUQg+gOhKgMIkaAAQBYAkA3C1QAWBKAOBYIACAUQAIAwgBATQAAAiAAAJIgBAEIBXABQACAEAEABIAOgPIAJgKIADgBQBJglBRAmQBXAngBBbIAAACQgBBjgGASQgTBOhMAQIiEAAIAAAAQABAHAAAYQAAATgCAFQgEAHgOAJIjDAAQh1AIhbgQQgWgEgVgFIgCgBIgNgDQhNgWg1gqIgJAlIgCAHIAAAAIgBADQgDADAAAFIABABIgBAEIADABIAAACQgDAKAAAJQAAAQACALIgBAAQACAPAAATgApIp+Qg9AvgyBIQgMAVgIAVIgFANQgFAQgEATQgJApgEA1QgDApAAAxIAAAGIAABdQAIBUAtA9IAMANQAXAaAdAVQAuAgA9AVQAWAHAZAGQCTAiDigWQgMgGABgVIAAAAIgJAAQgYgrgEgPIgBgNIgBgxQgBgrAIgmQAJgtAUgkIjJAAQgVAAgVgHIgCAEIgEADQgogCgYgIQgTgHgJgLQgHgHgBgJIABgIQgMgDgCgBQgCgDAAgJQAAgbAIgWIADgHIgBgJQAAgNADgGIADgEIABgCIAAAAIgBgeQgEhEgCgnQgDiEg6gfQgMgGgcgGIgGgCIgIAGgAKIhDQgyAgggAKIAOA/IALA7QAXgGAogEQBZgIAAgXQAAgtglgwQgZghgIgOIgZARgAhlA/IABAAIgBgBgAD2ldIAEAXIAgC+IAFAdQATBpAWA/QCRgFA0ABQgLhGgJgrQgDgMgWguIgwhiQgjhDgPguQgMgmAAgYQAAgWABgRIABAAQgPgtgdg8QgfhBgKgaQhIgmhzgdQBmCAAsDUgAgxi/QgXA7gFAnIAAAAQgCAPAABIQAAAOACAFIALAdIBUAKQBTgVAQgUQAGgJAAgNIgBgfQACgWAAgTIgBgSQgCgagJgTQgYg3hGAAQgcAAgnAKgAF4oqQArBmABA1QAFAJADAKIACAKIAHAjIACAKQAGAWAHAPQAVAgAXAsQAXAuAQAnIADALIAJAbQATgOAbgOIACgBIAAAAQApgWAhgKQgGg/gzhLIgdgkQgjgqhIhUIhGhTQgRgSgSgQIAFANgAmbrEIgHADIg0ADQgeALgcAQQBMAXAiB+QAXBTAABqIAAAHQAAAqgCAKQgEAXgLAOIAFACQAsAAARAPQAwAHBMACIgLhxIAAgBQgJhQgPg5QgniQhwhkIgDACgAqZCjIAAAAIABAAg");
	this.shape_16.setTransform(90.1,77.7);

	this.addChild(this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.instance,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(11.3,0,157.6,157.5);


(lib.Lucy_Headcopy = function() {
	this.initialize();

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjWNcIoQAAQjYhQhFhvQgthHAAiLQAAhqALgpQADgOA/ixIgLgDQhgAYg3g6QgpgsAAg0QAAhYBiglQBvgrE9gTQB1iZAignQBmh5BphWQEfjqF9gbIGFAAQEhA5BnDTQBACBAADwQAACegvCZQg/DLiHCbQlnGdsYAAIgRAAg");
	mask.setTransform(78.3,127.5);

	// Layer 1
	this.instance = new lib.LucyHair();
	this.instance.setTransform(85.4,77.7,1,1,0,0,0,84,107.7);

	this.instance_1 = new lib.LucyHead();
	this.instance_1.setTransform(91.9,116.6,1,1,0,0,0,69.8,62);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AkTDQIgGgLQgcgygFhBIAAhNQABi7BiheQBWhSCBAPQB9APBbBdQBjBlAACKQAABfgmBdIgHAQQg/CKiEAAQkGAAhYiKgAirkEQg+BHgOBCQgGAaABBpQAABeAjBCQAMAWAQASQBBBLB+AAICFgDQAkgjAZglQAVgfANghQAYg+AAhNQAAhfg4hJQhShqiwAAQgzAAg8AJg");
	this.shape.setTransform(31.5,133.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E5C45A").s().p("Ai+DJQgPgSgNgWQgjhBAAhfQAAhpAGgaQAOhCA9hHQEIgnBqCIQA4BJAABhQAABMgZA9QgMAhgWAfQgZAmgkAjIiEACQh/AAhBhLg");
	this.shape_1.setTransform(31.6,134.6);

	this.instance.mask = this.instance_1.mask = this.shape.mask = this.shape_1.mask = mask;

	this.addChild(this.shape_1,this.shape,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,41.5,175.4,137.1);


(lib.Lucy_Head = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.LucyHair();
	this.instance.setTransform(85.4,77.7,1,1,0,0,0,84,107.7);

	this.instance_1 = new lib.LucyHead();
	this.instance_1.setTransform(91.9,116.6,1,1,0,0,0,69.8,62);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AkTDQIgGgLQgcgygFhBIAAhNQABi7BiheQBWhSCBAPQB9APBbBdQBjBlAACKQAABfgmBdIgHAQQg/CKiEAAQkGAAhYiKgAirkEQg+BHgOBCQgGAaABBpQAABeAjBCQAMAWAQASQBBBLB+AAICFgDQAkgjAZglQAVgfANghQAYg+AAhNQAAhfg4hJQhShqiwAAQgzAAg8AJg");
	this.shape.setTransform(31.5,133.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E5C45A").s().p("Ai+DJQgPgSgNgWQgjhBAAhfQAAhpAGgaQAOhCA9hHQEIgnBqCIQA4BJAABhQAABMgZA9QgMAhgWAfQgZAmgkAjIiEACQh/AAhBhLg");
	this.shape_1.setTransform(31.6,134.6);

	this.addChild(this.shape_1,this.shape,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,175.4,178.6);


(lib.lorry_loading_area = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.lorry_loading_stripe("synched",0);
	this.instance.setTransform(188.5,44.8,1,1,0,0,0,78.5,44.8);

	this.instance_1 = new lib.lorry_loading_stripe("synched",0);
	this.instance_1.setTransform(133.5,76.2,1,1,0,0,0,78.5,44.8);

	this.instance_2 = new lib.lorry_loading_stripe("synched",0);
	this.instance_2.setTransform(78.5,107.7,1,1,0,0,0,78.5,44.8);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(205,209,192,0.008)").s().p("A01CdIZHuWIQkJeI5IOVg");
	this.shape.setTransform(133.5,76.3);

	this.addChild(this.shape,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,267,152.5);


(lib.LogOutRoundBtncopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 4
	this.text = new cjs.Text("Logout", "32px 'Arial'", "#FFFFFF");
	this.text.lineHeight = 38;
	this.text.setTransform(35,-4.2,0.59,0.59);

	this.instance = new lib.ARR();
	this.instance.setTransform(9.6,5.4,0.348,0.348,0,0,0,22.7,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 255, 255, 0)];
	this.instance.cache(-10,-2,58,54);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{skewY:0}},{t:this.text,p:{text:"Logout",lineWidth:84}}]}).to({state:[{t:this.instance,p:{skewY:180}},{t:this.text,p:{text:"Login",lineWidth:67}}]},1).wait(1));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ahfh7IC/AAIAAD3Ii/AA");
	this.shape.setTransform(15.2,4.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4F2A3F").s().p("AwJC/QgyAAAAgzIAAkXQAAgzAyAAMAgTAAAQAxAAAAAzIAAEXQAAAzgxAAg");
	this.shape_1.setTransform(102.5,4);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.9,-15.1,216.8,38.2);


(lib.LittleDude01b = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// hit
	this.hit_mc = new lib.blank_mc();
	this.hit_mc.setTransform(29.2,48.3,1,1,0,0,0,1,1);

	this.timeline.addTween(cjs.Tween.get(this.hit_mc).wait(1).to({x:-27.5,y:47.1},0).wait(1).to({y:15.7},0).wait(1).to({x:27.5},0).wait(1));

	// dude
	this.anim = new lib.Little_Dude_02_WalkingFront();
	this.anim.setTransform(1.8,12,1,1,0,0,0,21,29);

	this.animB = new lib.LittleDude_02_WalkingBack();
	this.animB.setTransform(-0.2,13,1,1,0,0,0,21,29);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.anim,p:{skewY:0}}]}).to({state:[{t:this.anim,p:{skewY:180}}]},1).to({state:[{t:this.animB,p:{skewY:0,x:-0.2}}]},1).to({state:[{t:this.animB,p:{skewY:180,x:0.7}}]},1).wait(1));

	// hit
	this.hitarea_mc = new lib.pallet_hit_area();
	this.hitarea_mc.setTransform(0,31.4,1,1,0,0,0,49.5,28.2);

	this.timeline.addTween(cjs.Tween.get(this.hitarea_mc).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56,-17,112,80.9);


(lib.LittleDude01 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// hit
	this.hit_mc = new lib.blank_mc();
	this.hit_mc.setTransform(29.2,48.3,1,1,0,0,0,1,1);

	this.timeline.addTween(cjs.Tween.get(this.hit_mc).wait(1).to({x:-27.5,y:47.1},0).wait(1).to({y:15.7},0).wait(1).to({x:27.5},0).wait(1));

	// dude
	this.anim = new lib.Little_Dude_01_WalkingFront();
	this.anim.setTransform(1.8,12,1,1,0,0,0,21,29);

	this.animB = new lib.LittleDude_01_WalkingBack();
	this.animB.setTransform(-0.2,13,1,1,0,0,0,21,29);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.anim,p:{skewY:0}}]}).to({state:[{t:this.anim,p:{skewY:180}}]},1).to({state:[{t:this.animB,p:{skewY:0,x:-0.2}}]},1).to({state:[{t:this.animB,p:{skewY:180,x:0.7}}]},1).wait(1));

	// hit
	this.hitarea_mc = new lib.pallet_hit_area();
	this.hitarea_mc.setTransform(0,31.4,1,1,0,0,0,49.5,28.2);

	this.timeline.addTween(cjs.Tween.get(this.hitarea_mc).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56,-17,112,80.9);


(lib.LifeDead = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.whiteDot();
	this.instance.setTransform(11.3,8.1,1,1,0,0,180,1.9,1.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("AiLAAQAAA6ApAoQAPAPAQAKQAfARAkAAQA6AAAogqQAjgiAGguQABgJAAgJQAAgIgBgIQgGgvgjgjQgDgEgFgDQgmgig0AAQg5AAgpApQgpApAAA5g");
	this.shape.setTransform(14,14);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#889B88").s().p("AhJBrQgpgpAAg6QAAg4ApgpQApgpA4AAQA0AAAmAhQAPCuirA2QgRgJgOgPgAgOg/QgFAFAAAIQAAAHAFAGQAGAGAHgBQAGABAFgGQAGgGAAgHQAAgIgGgFQgFgGgGAAQgHAAgGAGg");
	this.shape_1.setTransform(11.6,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#606D60").s().p("AhnBqQCsg2gPiuIAJAIQAiAiAFAvIABASIgBAQQgFAugiAjQgpApg4AAQgmAAgfgRg");
	this.shape_2.setTransform(17.6,15.7);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,31,31);


(lib.Life = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.whiteDot();
	this.instance.setTransform(16.7,8.1,1,1,0,0,0,1.9,1.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("ACMAAQAAA6gqAoQgOAPgQAKQgfARglAAQg5AAgpgqQgigigGguQgBgJAAgJQAAgIABgIQAGgvAigjQAEgEAFgDQAmgiAzAAQA6AAAoApQAqApAAA5g");
	this.shape.setTransform(14,14);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#28A328").s().p("Ag9BSQgjgjgGguIgBgQIABgSQAGgvAjgiIAIgIQgPCuCrA2QgeARglAAQg5AAgogpg");
	this.shape_1.setTransform(10.4,15.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#33CC33").s().p("AhxhhQAmghA0AAQA4AAApApQApApAAA4QAAA6gpApQgOAPgRAJQirg2APiugAgJg/QgGAFAAAIQAAAHAGAGQAFAGAGgBQAHABAGgGQAFgGAAgHQAAgIgFgFQgGgGgHAAQgGAAgFAGg");
	this.shape_2.setTransform(16.4,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,31,31);


(lib.LeaderboardButton = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Lead();
	this.instance.setTransform(89,19.5,1,1,0,0,0,89,19.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("At5DCIAAmEIbzAAIAAGEg");
	this.shape.setTransform(89,19.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regY:19.5,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regY:19.4,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,178.1,40.8);


(lib.KateSkirt4 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.GirlOutfit04();
	this.instance.setTransform(82.1,89.6,1.12,1.12,0,0,0,62.6,83.9);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#371E2D").s().p("AinAoQg1gEgHgDIAJgZQAFgMAPgJIAOgGIAGgCIARgEIgBAHQAAAeBjAZIAaAGQgdACgjgBQgRABgxgFgACDAOQAAgOgEgKQAGAAAGgIQAIgKAEgDQAKgIAQgFQAKAsANAPQAJAKATAIIhIAGQgUABgGABQABgKAAgRg");
	this.shape_1.setTransform(73,158.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#4D2A3E").s().p("Ai8AqQhjgYAAgfIABgHQA6gNCNABQATACAQAQQAQAOgBAQIgFAFQgHAHgQAFQgjAMg+ADIgagGgABLAUQgNgPgKgrQAcgJAyAAQBIAAAqATQAjAQAJAYQACAIghAEQg2AGhkAIQgTgIgJgKg");
	this.shape_2.setTransform(85.5,158.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CCB59F").s().p("ABQIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAjqHGQgbhJgSgeQARAEAkAbQAjAaASADIAmB3QgjAGgdAMIgjhegAqJATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUgAKBAAQgfgWgOgjQgFgMgFgYQgCgUAAgXQgKhAgfg1IgGgKQgKgRgGgEIgJgfQgOg4gFgfIgPhHQA7BrAmBYQAiBSADAgQAEAzAWApQAaAxArAJIgTAGQgFACgEADIAAAAIgKADg");
	this.shape_3.setTransform(75,96.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#381E2D").s().p("AD+E6QhMgKiBgOQhigMhVgRQAGgJAGgSQAOgnAIg6IAOhSQAFgaAAgdIAAgKIAAgBIgBgJIAAgHQAGgDAHgBQAFAAAJAMQAIALAEAJQADAHAIAvQAPA2AlAsQAnAsA4AWQAgAMA+AMQA3AKAVAKQAhASAIAoQgqgDgegEgAjxheIg5gUQgZgJgCgJQgDgbAmgsQAfgjAkgZQAtggBKgZQgVAQgjAvQgmAzgVAuQgGAPgFAgQgDATgHAAIgBAAg");
	this.shape_4.setTransform(47.8,47.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EACFB7").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAI1hAQgWgpgEgzQgDgggihSQgmhYg7hqQgKgtgCgSIAOgEIABgBIAIgBQAVgEAXAAIADAAIAWgIQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APIgUAGQgrgKgagxg");
	this.shape_5.setTransform(82.3,95.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#4D2A3F").s().p("AATGLQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgBgLgDgGQgDgEgDgDQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgJgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6IAMAPIAKASIADAHIgBAAIgOAEQgRAHgFALQgFAOgGAZQgHAegBALIAAAkQAAAyASAxQAMAfAmBHIADAGIgtAKQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_6.setTransform(78.6,39.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgXghgnhnIgjheIAAgGIA7AQQASAeAbBJIAjBeQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJIAsgBQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMIA0gMQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgAo5DDQgYgBgVgCIg1gLQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABQgMAHgJAHQAJgHAMgHIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQADADACAEIhIDbQgYA3gEAsQgHAxATAWQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgeANglAAIgKgBgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAJZB/IgBAAIgCAAQgSAAgOgSQgUgSgQgvQgUg2AAgZIABgRQgBgSgOgnQgWg5AAgCIAAgJIAAAAIAAgBQAAgRAHAFQAGAEALARIAGAKQAfA1AKBAQAAAXACATQAFAWAFAMQAOAjAfAYIAcAAIAKgFIAAABQAEgDAFgCIATgGIAUgFQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJIgDgBQgXAAgVAFIgIABIgDgHIgLgSIgLgRQgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgFAAIg3gBgAF1rgIgDgFIADAFg");
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,171.8);


(lib.JaneYard1 = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#421F10").s().p("AgSAvQgCgDgDgCIgBgEQgGgfAOgdQAHgPAJgLIAaAWIAAAAIgJAkIgEAUQgCAIgIAEIgDAEIAAABQgCACgGAAQgIAAgCgCg");
	this.shape.setTransform(109.3,155.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#65301B").s().p("ABFAsIgBAAQgEgBgCgOQgBgTgCgDQgEgIgLgHIgRADQgfAFgNAIQgXAEgJALIgbgWQAHgGAJgGQACgHAVgMIAbgNIAkAAIAVAIIAAABQAPABAIAZQAFAOABAUIAAAFIABADQAAAJgHABIgBAAg");
	this.shape_1.setTransform(117,150.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhMBSIgEAAQgVAAgFgYIAAgbQAAg+A5gqQAMgIAKgDQALgKAqAEQA2AFAEASIACABQAKAFACAOIAGAPQADASAAAOIAAAMQgBAKgFAAIgCAAIgCABQgFAAgBgJQAGgBAAgKIAAgDIAAgDQgBgTgFgRQgIgZgPgBIAAAAIgWgJIgmAAIgZAOQgUALgDAHQgJAGgGAIQgLAMgGAOQgOAeAGAfIAAADQAEADABADQADACAHAAQAGAAAEgCIABgCIADgEQAHgDACgJIAFgTIAIgkIABAAQAJgLAVgFQANgIAggGIARgDQAMAHAEAKQABACACATQABAMAEACIAAABQAAABAAAAQAAABAAAAQgBABAAABQAAAAgBABQAAAAgBAAQAAABAAAAQgBAAAAAAQAAABgBAAQgIAAAAgLIABgNQAAgFgEgJIgBgDQgTAGgRACQgEAAgVALIgEACQgHAKgJANQgCADgIAhQgMAfgVAAQgQAAgHgKg");
	this.shape_2.setTransform(114.9,153.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#421F10").s().p("AgFA4IgEgBIgEAAIgBABIgDgDQgFgIgDgIQgDgHAAgEQgBgOAIgUIAEgKQAJgVALgQIAUAWIAGAHQgJAMgDAOQgDALgBAUIgBARIgDAEIgGADIgEABIgEAAg");
	this.shape_3.setTransform(66.6,157.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#65301B").s().p("ABaAgIgDgHIgCgEIgFgJQgDgDgJgDQgMgDgNgBIgGgCIgBAAIgLAAIgFAAIgUgBIgMAAIgDABQgoAAgPAOIgIAJIgGgHIgUgUIAFgFQAOgPARgHQACgDAEgCIAYgIIAmAAQAdAEAmAZIADACIAAAAQAjAXAAASIAAAFIAAADQgBAJgFAAQgEAAgFgNg");
	this.shape_4.setTransform(77.5,152.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AhLBeQgEABgIAAIgVAAIgEAAQgVAAgEgXQAahhAFgGQAbgiApgTQANgHAOgBIAVgBQAGgCAbAFQAgAHAMAMIAbAYQAIAGAGALQAEAIAAADIAAAFQACACAAACIAAACQADAIAAAIQACAagJgBIgCgBIgDAAQgEAAgBgDIgBgEIABgDIgBAAIgBAAQgCADgDgBQgFABgGgbIgEAAQgRAAgWgGIgRAAQg5AAgRAyIgMAkQgHARgOAAQgGAAgEgBgAhdBOIAEAAIAEAAIAGAAIAGgDIAEgEIAAgRQABgUADgLQADgQAKgKIAIgJQAOgOAngCIACgBIAMAAIAXABIAEAAIAMACIAAAAIAGACQANABAMADQAKADACADIAFAJIACADIADAGQAGANADAAQAGAAAAgJIAAgDIAAgFQAAgQgjgZIAAAAIgDgCQglgZgegEIgmAAIgYAIQgEACgBADQgSAHgOAPIgFAFQgNAQgJATIgEAKQgHAVAAAPQAAAEADAHQADAIAGAIIACACIACgBIADABg");
	this.shape_5.setTransform(75,155.6);

	// Layer 8
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#7A4D30").s().p("AAjBjQgEAAgMgJIgQgKQgMgKgCgIIgCgDQgFgIgBgHIgFgXIAAgdQgCgEgBgLIgDgSIgCgHIgFgWQgEgMAAgJQAAAAAAgBQAAgBAAAAQAAAAAAgBQAAAAAAgBQACgDAIAAQANAAAIAHIALARQACADAEAIQAcALADASQAEACAAAGIAAABIgOATQgPARgEAKIgEAKIAFAKQAIANAMAJIAHAEIAAAAQAAAAABAAQAAAAAAABQABAAAAAAQAAABAAAAIABABIAAABIAAADQAAAOgHAEQAFADAAABQAAABAAAAQAAABgBAAQAAABgBAAQAAAAgBAAIAAAAgAgBhDIgBgBQgOgYgKAAIgIAAIAAAFIACAJIABACIAEAFQAHgCATAGIAAAAg");
	this.shape_6.setTransform(58.9,151.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#975F3C").s().p("AhoA0QABAAAAAAQABAAAAgBQAAAAABAAQAAgBAAgBQAAgBgFgDQAGgEABgOIAAgDIAAgBIgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQgBAAAAAAIgBAAIgGgEQgMgJgIgLIgFgKIADgKQAFgKAPgTIAOgTIAAgBQAcgCAoAQQBKAIA0AhQAWAOALALIgDACIAGAVQAFANgHAIQgVAXiKAAQgmAAgmgWg");
	this.shape_7.setTransform(73,155.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AiGAyQgfgeAAgVQAAgpALgyIACASQACALACAEIAAAfIAEAXQACAFAFAIIACADQACAIAOAKIAPAKQANAJADAAIABAAQAmAWAmAAQCKAAAVgXQAHgIgFgNIgGgVIADgCQADgCAEAAIADABQAAAIACAFQAEAOADAFQgCAXgNAJQgkAbh5AAQhSAAgugrgACjAGQACADABAGIAAAHQgCgHgBgJg");
	this.shape_8.setTransform(71.3,156.3);

	// Layer 7
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#421F10").s().p("AA5AuIhkAAQg3gIgHgFQg0gOgSgQQgQgMAAgbIABgJQACAVANAMQASAOA0APQAHAFA3AHIBkAAIBUAAQAjgRAFgHIAFgFQADgEABgEIABAGQAAAIgFAJIgFAGQgFAIgjARg");
	this.shape_9.setTransform(71.3,162.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#65301B").s().p("AA6BDIhlAAQg2gIgIgFQg0gOgRgQQgOgMgBgTQABgQAGgEQACgIALgHIAYgKIADgBIAKgEIAGgDIARgGIA1AAIAeAFIALACIAuAHIAKACIAYAEIAkAHIAGACIAOAFIAKAFIAUAKIAKAGIADACIACABIADADQAOAHAEAMIABAJIAAABIAAAAIACAEQgBAEgCAEIgGAHQgEAHgjARg");
	this.shape_10.setTransform(71.2,158.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ai4AqQgTgXAAgrQAAgIAIgPIACgEQADgHAFgEIAIgIIAHgEQAJgLAggFIAWgGIAcAAIALgCIAeAAIASAHIABAAIAuAIIAqAIIARADIAEABIAYAFIAKAEQAmALASAMQAJAFAFAEQAHAFADAWIAAABQABAKgBALQAAAWgGALIgCAEQgTAlg3AAQhPAGgjAAQiSAAgug5gAh6hMIgHADIgJAEIgEACIgXAKQgMAHgCAHQgGAEgBAQIgBAJQAAAZAQAOQARAQA0AOQAIAFA3AIIBkAAIBUAAQAjgRAEgHIAGgHQAFgJAAgKIgBgGIgCgDIgBgBIAAAAIgBgIQgEgMgNgJIgEgCIgCgBIgDgDIgKgFIgUgLIgKgFIgOgFIgGgBIgjgHIgZgFIgKgBIgtgIIgMgCIgegFIg1AAIgQAGg");
	this.shape_11.setTransform(70.9,160.5);

	// Layer 10
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#7A4D30").s().p("AA0A8IgGgCIgEAAIgCAAIgEAAQgkAAgVgNQgNgIgJgNQgIgNgEgWQgBgGABgNIACgUQAAgPAFAKQAGAKgCAIIAAAMQAGgFAKgGQAMgHAGABIgCgGQAAgOAGADQAGAEAAAMIAAACIAHAgQADANAMARQALARANALIAKAIQAAABAAABQAAAAAAABQgBAAAAAAQgBAAgBAAIgBAAg");
	this.shape_12.setTransform(93.4,154);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#975F3C").s().p("AAUBJIgRgDIglAAIgDgBIg6gBIgFAAQABAAABAAQABgBAAAAQABAAAAgBQAAgBAAAAIgKgIQgNgLgLgRQgMgSgDgNIgHggIAAgCIAYgMIAVgIQABAFAFABIAJABIAPgEQAPgDATgCIAugBIAEgDQALgBAAgFQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQAsAEAiANQAhANAJAQQAMATAFANIgFgGQgGgGgEgDIgGgJQgNgNAAAOIASAcIAAACQAAAEAFAOQAFAMAAAFQAAADgRAJIg3ANg");
	this.shape_13.setTransform(108.8,153.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgKBLQgXgKgOgBIgDgCQgDgCAAgDQAAgBAAAAQAAgBAAgBQAAAAABgBQAAAAAAAAIAAgBQAAgFAKAAQAXANAjAAIAEgBIACABIAKACIAFABIA6ABIADABIAnAAIAQACIA4AAIA3gMQARgJAAgEQAAgEgFgNQgGgOAAgEIAAgCIgSgbQAAgOANANIAHAIQAEADAGAIIAEAFQANASAAAQQAAASgRAIIgBAAIgCABQgPALgfAHIgZAAIgFABIh/ACQhKgCgMgGgAkpg3IgBgDQAAgOAvgFQAvgHAAAJQAAAFgIACQgLACgnACQgGABgJAGQgIAFgFAAQgFAAgCgDgAAfg4QgGAAgBgFIAAgCQAAgFARgFQAcgGAFgDIBIACIADADQABAAAAAAQABABAAAAQAAABABAAQAAABAAAAQAAAGgLABIgEACIgwABQgUACgPAEIgPADIgIgBg");
	this.shape_14.setTransform(95.5,153.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#EACFB7").s().p("AAwAiIAEAAIAGACIgKgCgAguAkIAAgBIgCgCQgDgGgDgNQgCgGgBgIIAAgHQAAgSAFgHIADgDQADAXAJAMQAIANANAIQgJAAAAAFIAAABQgBAAAAAAQAAABAAAAQgBABAAABQAAAAAAABQAAADAEACgAgvAUIgBgHQgBgFgCgDQABAIADAHg");
	this.shape_15.setTransform(92.8,156.5);

	// Layer 9
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#421F10").s().p("AhgAiQgcAAgdgFQgdgGgHgHQgagXAAgGIABgNQAEAIAVASQAHAGAdAGQAdAGAcAAIDIAAQAkgEApgRQAJgEAFgFQAGgDAEgDQAIgHACgIIABAIQAAANgLAKQgEACgGADQgFAEgJAFQgpATgkADg");
	this.shape_16.setTransform(107.5,162.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#65301B").s().p("AhgA+QgcAAgdgGQgdgGgHgGQgVgUgEgIQACgJAFgGQADgOAWgQIAEgDIABgBIADgCQASgLAYgHIAMgDIASgEIAIgBIBfAAIAIABIAKABIAKABIABAAQAbAFApALIATAFQAzAPAXANIAEACIAGAEIADADQAJAAACAJQgCAIgIAHQgEADgGADQgFAFgJAEQgpATgkAEg");
	this.shape_17.setTransform(107.5,157.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("Ah1BcQhGgNgbgWQgVgRAAgeQAAggAXgRQADgMAfgPQAcgOAcgEIAagHIBEAAQAfACAMADQADAAABADQARABAAAJIAAACIgKgCIgKgBIgIAAIhgAAIgHABIgTADIgMAEQgYAGgSAMIgCABIgCABIgDADQgXAQgCARQgGAFgCAIIgBANQAAAGAbAZQAGAGAdAGQAdAGAcAAIDJAAQAkgEAogTQAJgEAGgFQAGgDAEgDQALgKAAgNIgBgHQgDgKgJgBIgDgCIgFgEIgFgDQgXgNgygOIgUgGQgogLgcgEIAnAAIAMABQALgFA3ATIAPAGQAtANAJAKQAJAEADAMQADAKgCAJQAAAfgUAQQgeAYhZANg");
	this.shape_18.setTransform(106.8,159.3);

	// Layer 2
	this.instance = new lib.Girl02copy();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#7A4D30").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQAcgyAMg8QAogfAKh+QAEg0gChDIAAgHIAAgCIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape_19.setTransform(15,71);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#975F3C").s().p("AouE4IgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAHoA6QACgBgGgRQgGgSgEgBIgBAAIAAgBIgBgHIAAgcQABgYgCgTIgFgpQgCgNABgCQARggAHgUQALgcAEgbQAIgxACgZIAAgQIAMgBIANADIAAAMQACAmAMBYQAJA7ABAXQAAANgBAOQAAAnAQAhQAGAQAZAqQApBEgRBCQgMAqg6ABQgjgBgGABIgJADQgJh8gQhCg");
	this.shape_20.setTransform(78.2,71.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AnzF6QgZgBgVgDQhbgWgYgTQgrgjgLhMQgIgxAJgcQAGgSAUgcQAdgoARg2QAGACAHAAQARAAANgIQgOA8gcAyQgKAUgRAcQgLAUADANQAPBBAqAoQAUAUAWAKIAOAFQAKADALABIAPABQBTAGAggeIAHgHQAPgSgFgVIgBgCQgDgLghg+IgJgRQgbgygCgIIAAgBQgDgVAYhtQAXhtAahAIAIgUIgMgJQgugigbgKIgTgIQgOgGgLgBQgFgBgIgDIgMgFIAAACIgDgzIAJADIA5AUIAjAPQAKAFALALIApApIABACIAQgCIAJgPQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAlQgGASgGAJQAuAJAyAIIAGA1QhYgPghgVQgHAQgPAgQAJATAUAlQAIAPALAiQAQAxgFAVQgFAUgYAVQgRAOgKAFIgKAFQgdANglAAIgKAAgAmRgrQgYA1gDAsQALgNAIgEIgDgFQgBgCAAgNQAAgIAKgiIAJggIgHAOgAIrE6Ig4ABIgBAAIgOjhQgRACgZgCIgFgyIAfABQgHgagCggIgDgfQgCgLADgNIgOACIgKABQABgXAEgWIAIgIQAngiAKg6QAEgYgCgZQAOgPARgPIAEgBIADABIASgXIAJgEQANgHALgDIgGgPIAKgKIAXgWQAGAWAAAWIAAAIIAAAEIgBAGIgBACIgCACIgBABIgLAFQgIADACAFIAHASIAMADQAGA3AKA4QAIAyABA0QACBCARAqQAJAVAZAgQAUAaAEAXQAFAigQA9QgJAgglAXQghAUgoAAQgQAAgRgDgAIFA/QAFARgCABQARBCAJB8IAJgDQAGgBAiABQA7gBALgqQAShCgqhEQgZgqgGgQQgQghAAgnQACgOgBgNQgBgXgJg7QgMhYgCgmIAAgMIgNgDIgLABIgBAQQgCAZgHAxQgEAbgMAcQgHAUgQAgQgBACACANIAFApQACATgBAWIgBAeIABAHIABABIABAAQAEABAGASg");
	this.shape_21.setTransform(74.9,69.1);

	// Layer 1
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#7A4D30").s().p("AC3COQgWgOgFgpQgDgPAAgzQAAghADgLQACgJAKgIQAcgYAdgsIgVD6gAixALQgYhggXgtQAugJAigCIBEEWIgIAAQhAgqgdhUg");
	this.shape_22.setTransform(72.4,141.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#975F3C").s().p("ABWCTIAVj6QANgTAOgYQA/ABAaADIAoAFQgJAVgcB3QgTBUgyA8gAkGiIQA2gEAxAOQBGATATADIAAB7QgCAEACAZIAEAkQgBAPgFAMIgIAMQgJALgPAGIhaACg");
	this.shape_23.setTransform(84.2,140.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAlB8QgHgdABgmIADgjIAAhNQAFgVAWgPIApgdQAHgHAQgjQAMgbAQgBIABAAQCggHAeAbQAKAJgEAPQgCAIgHANIgSBTQgMA3gOAkQgrBthoAdQgQAFgOABIgIAAQg8AAgPhFgACShqQgdAsgcAYQgKAIgDAJQgCALAAAhQAAAzACAPQAGApAWAOIAUAAIBIAAQAyg8AThTQAch4AIgVIgogFQgagCg/gCQgNAYgNATgAiSC1QgogBgXgMQgYgOgRAAQgKAAgDgCIgHgGQAAgegohvQgohtAAgmQAAgOAPgFQALgFBKgNIARAAQBTADA8AMQBBANATAWQANASABAYQAAAKgCAcQAAAdgCAUQgCAbgGAxQgBAHgMAKIgRAPQAGgMAAgPIgDgjQgDgZACgFIAAh7QgSgDhGgTQgygOg1AEQgjACguAJQAYAtAYBgQAcBUBAAqIAIAAIBagBQAPgHAKgLQgqA9g9AAIgCAAg");
	this.shape_24.setTransform(80.4,140.9);

	this.addChild(this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.instance,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,-2.6,143.9,173);


(lib.JaneSkirt4 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.GirlOutfit04();
	this.instance.setTransform(82.1,89.6,1.12,1.12,0,0,0,62.6,83.9);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#4E2A3F").ss(1,1,1).p("AE3BEQgBgCgBgDQg8hdhfgbQgfgKhDAAQhtAAhwAbQhRAUgrAWQgMAHgJAG");
	this.shape.setTransform(85,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7A4D30").s().p("ABQIzQABgHAAgVQAEgogFghQgDgWgIgOQApgBAqgDIAOBkQgRADgPAFQgoANgNAUIgBAAgAjqHGQgbhJgSgeQARAEAkAbQAjAaASADIAmB3QgjAGgdAMIgjhegAqJATQgqgngPhBQgDgMALgVQARgbALgUQAthTAJhvQACgZAShVIAUhdIAMAEQAJAEAEAAQgHAqgLCKQgNCdgBADQgPAsgcAtQgRAlAOA+IAMAkIAKAmQgVgJgVgUgAKBAAQgfgWgOgjQgFgMgFgYQgCgUAAgXQgKhAgfg1IgGgKQgKgRgGgEIgJgfQgOg4gFgfIgPhHQA7BrAmBYQAiBSADAgQAEAzAWApQAaAxArAJIgTAGQgFACgEADIAAAAIgKADg");
	this.shape_1.setTransform(75,96.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#975F3C").s().p("AhkIWQgmgDgkADQgSABgQADIgmh3QBiAPBkABQAAAJABAIIAIAzIAEArQgXgIgqgEgADBIOQg3gFgtAJIgOhkQB0gJB0gaQgGALgCAMQgDALgTA0QgNAlgCATQghgJgogCgAp1BDIgQgBQgLgBgKgEIgOgEIgKgmIgMglQgOg+ARglQAcgsAPgtQABgDANicQALiKAHgqQAMABAOAGIATAHQAbAKAtAjIAMAJIgIAUQgZA/gYBuQgXBvADAUIAAACQACAIAaAyIAJARQAhA+AEALIAAABQAFATgOATIgHAGQgbAahAAAIgYgBgAI1hAQgWgpgEgzQgDgggihSQgmhYg7hqQgKgtgCgSIAOgEIABgBIAIgBQAVgEAXAAIADAAIAWgIQAIAgArBmQAYA2AHAWQAEANACANQAKAmAYAfQAKANAkAiQA5A3AABEQAAAsg4APIgUAGQgrgKgagxg");
	this.shape_2.setTransform(82.3,95.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#371E2D").s().p("AinAoQg1gEgHgDIAJgZQAFgMAPgJIAOgGIAGgCIARgEIgBAHQAAAeBjAZIAaAGQgdACgjgBQgRABgxgFgACDAOQAAgOgEgKQAGAAAGgIQAIgKAEgDQAKgIAQgFQAKAsANAPQAJAKATAIIhIAGQgUABgGABQABgKAAgRg");
	this.shape_3.setTransform(73,158.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#4D2A3E").s().p("Ai8AqQhjgYAAgfIABgHQA6gNCNABQATACAQAQQAQAOgBAQIgFAFQgHAHgQAFQgjAMg+ADIgagGgABLAUQgNgPgKgrQAcgJAyAAQBIAAAqATQAjAQAJAYQACAIghAEQg2AGhkAIQgTgIgJgKg");
	this.shape_4.setTransform(85.5,158.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#381E2D").s().p("AD+E6QhMgKiBgOQhigMhVgRQAGgJAGgSQAOgnAIg6IAOhSQAFgaAAgdIAAgKIAAgBIgBgJIAAgHQAGgDAHgBQAFAAAJAMQAIALAEAJQADAHAIAvQAPA2AlAsQAnAsA4AWQAgAMA+AMQA3AKAVAKQAhASAIAoQgqgDgegEgAjxheIg5gUQgZgJgCgJQgDgbAmgsQAfgjAkgZQAtggBKgZQgVAQgjAvQgmAzgVAuQgGAPgFAgQgDATgHAAIgBAAg");
	this.shape_5.setTransform(47.8,47.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#4D2A3F").s().p("AATGLQgIgogfgSQgVgKg4gKQg+gMgggMQg3gWgogsQgngsgOg2QgIgvgDgHQgFgLgHgLQgJgMgFAAQgHABgHADQgBgLgDgGQgDgEgDgDQgKgGgPAUIgKAPIgPACIgBgCIgpgpQgLgJgLgFIgjgPQAIACADgVQAFggAHgPQAUguAngzQAigvAVgQIB8gqQA5gUAIgGQADgCgCgFIgDgGQArgYBSgTQBtgbBvAAQBDAAAfAJQBgAbA7BfQgLACAAALQAAAHA3AjQBBApAUAUQANANAVBAQAHAVAHAOQgLADgOAHQgYAMgOAJQgQgfgZgfQgbgggUgEQgVgEAAAhQAAAFAqA6IAMAPIAKASIADAHIgBAAIgOAEQgRAHgFALQgFAOgGAZQgHAegBALIAAAkQAAAyASAxQAMAfAmBHIADAGIgtAKQhAAOikACIgnAAQhKAAg3gDg");
	this.shape_6.setTransform(78.6,39.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AkAMmQg9gKgIgLQgHgMAAgWQAAgyA0gkQgXghgnhnIgjheIAAgGIA7AQQASAeAbBJIAjBeQAdgMAjgGQAQgDASgCQAkgDAmADQAqAEAXAJIgEgrIgIg0QgBgIAAgJIAsgBQAIAPADAWQAFAggEAoQAAAWgBAHIABAAQANgUAogNQAPgFARgEQAtgIA3AEQAoACAhAKQACgUANgkQATg0ADgLQACgMAGgMIA0gMQgCAYgRA8QgRA6gUAbIASAJQBAAkgEA1QgBAPgEAGQgGAHgSAFQgOADigAUQhCAHg3gHQgWgEgCgIIgDgPIgDAGQgWAYhZAMQg5ADg1AAQg1AAgvgDgAjcKuIgQADIgHACIgNAGQgQAJgFAPIgJAYQAHADA1AEQAxAFARAAQAjAAAdgBQA+gEAigLQAPgGAHgGIAFgFQABgSgQgPQgOgPgTgDIgUAAQh9AAg2ANgAB2KbQgQAFgJAHQgFAEgIAKQgFAHgGAAQADANAAANQAAARgBAKQAGgBAUgBIBIgGQBkgIA2gFQAhgFgCgHQgJgbgjgQQgqgShIAAIgFgBQguAAgbAJgAo5DDQgYgBgVgCIg1gLQgmgLgYgUQgrgigMhNQgHgvAIgcQAGgRAUgdQAzhGAOhwQAIg3AKguIAWhkIAAgbQgJgEgFgGQgEgIgBgNIAAgNIAAgBQAJhcCHhPQB/hLCpgZQABgBABAGIABABQgMAHgJAHQAJgHAMgHIADAHQACAEgDADQgIAFg5AVIh8ApQhKAagtAfQgkAZgeAjQgnAsAEAbQABAJAaAJIA4AUIAjAPQALAFALAMIApAoIABADIAPgDIAKgOQAPgVAKAGQADADACAEIhIDbQgYA3gEAsQgHAxATAWQAJAUAUAkQAJANALAiQAQAxgGAWQgFATgYAVQgQAOgLAFIgJAGQgeANglAAIgKgBgAp7l3QgSBVgCAZQgJBvgtBSQgLAVgRAbQgLAVADAKQAPBBAqAoQAVAVAVAJIAOAFQAKADALABIAQACQBTAFAggeIAHgHQAOgSgFgVIAAgBQgEgLghg/IgJgPQgagxgCgJIAAgBQgDgUAXhwQAYhtAZhAIAIgTIgMgJQgtgjgbgKIgTgIQgOgGgMgBQgEAAgJgEIgMgFIgUBegAJZB/IgBAAIgCAAQgSAAgOgSQgUgSgQgvQgUg2AAgZIABgRQgBgSgOgnQgWg5AAgCIAAgJIAAAAIAAgBQAAgRAHAFQAGAEALARIAGAKQAfA1AKBAQAAAXACATQAFAWAFAMQAOAjAfAYIAcAAIAKgFIAAABQAEgDAFgCIATgGIAUgFQA4gQAAgsQAAhBg5g4QgkgigKgNQgYgfgKglQgCgOgEgMQgHgWgYg3QgrhmgIggIgWAJIgDgBQgXAAgVAFIgIABIgDgHIgLgSIgLgRQgqg6AAgFQAAghAVAEQAUAEAbAgQAZAfAQAgQAOgKAYgLQAOgIALgDQgHgOgHgUQgVhAgNgOQgUgThBgqQg3giAAgHQAAgMALgCQAIAAAPAEQAiAKAqAfQBvBUAABgIgBAIIABAEIgCAGIAAACIgDADIgBABIgKAEQgIADACAFQAXA/AdA/QAWAtAOAzQATBBAcAkQAOASAgAZQAaAUAKAUQAOAgAAA+QAAAhgeAfQglAog9AEIgFAAIg3gBgAF1rgIgDgFIADAFg");
	this.shape_7.setTransform(78.9,87.4);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-1,157.8,171.8);


(lib.Jane_Head = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.HInfront();
	this.instance.setTransform(82,85.8,0.97,1.07,0,0,0,84.5,80.2);

	this.instance_1 = new lib.JaneHead();
	this.instance_1.setTransform(84.9,109.4,1,1,0,0,0,69.8,62);

	this.instance_2 = new lib.Behind();
	this.instance_2.setTransform(150,120.3,1.07,1.07,8.5,0,0,20.5,40.6);

	this.addChild(this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,178.8,171.4);


(lib.Highlight = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.HighlightCircle("synched",0);
	this.instance.setTransform(24.8,24.8,0.839,0.839,0,0,0,24.8,24.8);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({_off:false},0).to({scaleX:0.98,scaleY:0.98,alpha:1},2).to({scaleX:1.63,scaleY:1.63},9).to({scaleX:1.85,scaleY:1.85,alpha:0},3).to({_off:true},1).wait(1));

	// Layer 2
	this.instance_1 = new lib.HighlightCircle("synched",0);
	this.instance_1.setTransform(24.8,24.8,0.839,0.839,0,0,0,24.8,24.8);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:0.98,scaleY:0.98,alpha:1},2).to({scaleX:1.63,scaleY:1.63},9).to({scaleX:1.85,scaleY:1.85,alpha:0},3).to({_off:true},1).wait(5));

	// Layer 1
	this.instance_2 = new lib.HighlightCircle("synched",0);
	this.instance_2.setTransform(24.8,24.8,0.839,0.839,0,0,0,24.8,24.8);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX:0.98,scaleY:0.98,alpha:1},2).to({scaleX:1.63,scaleY:1.63},9).to({scaleX:1.85,scaleY:1.85,alpha:0},3).to({_off:true},1).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(2.8,2.8,44.1,44.1);


(lib.Health_and_Safety_advisor_SB = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.HS_Bubble();
	this.instance.setTransform(582.5,249.9,1,1,0,0,0,418.4,161.1);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,566.5,261.3);


(lib.HardHatWhite = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.StaySafe();
	this.instance.setTransform(42.1,14.5,0.07,0.07,-9,0,0,109.7,97.7);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C6C0AA").s().p("AhVBFQALg/AQgoQAXg6ApgrQApgpA3gWQgaA+gTBJQgTBIgIBPIgLAGQgjAXgiAgQgiAdgRAVQAEhEAMg+g");
	this.shape.setTransform(13,26.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F2EACF").s().p("AlUCpQANgMApgfQAegZAZgMQBJgpBNgeQCQg4CQAAQBNAAA4AkQAsAbAAAUQAAAMgSATQgUAVghAUQgTALgdAVIgggdQgSgNgdgJQhLgXhiAEQhVADg+AOQhUAShGApIgLAHIhUBAQAIgZAjgggAj/g9QAThIAag/QBFgdBIAAQB8AABQAwQA/AmApBLQiHgBhqAhQigAvh4BLQAIhOAThJg");
	this.shape_1.setTransform(44.4,26.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#212112").s().p("AmeEeQgEgEgCgJQgDgMAAgWQABgUgBgZIgCghQADjnB4h4QA2g1BFgYIAkgLQBagYBnASQA2AJAgAOQAZAMAjAaQAeAYAXAeQAUAcAUApIABACIADgBQBCAJAmAnQAdAfgBAbQgBAig3AqIhfBCQgKgBgDgCQgEgDAAgOIgIgHQgQgOgNgHQg0gghoAAQjTAAh+BPQgRALgQANQgoAggfAsIgJANQAAACgRAeQgIgDgDgEgAhBgRQhNAchJApQgZAMgeAZQgpAfgNAMQgjAggIAZIBUhAIALgHQBGgpBUgSQA+gOBVgDQBigEBLAXQAdAJASANIAgAdQAdgVATgLQAhgUAUgVQASgTAAgMQAAgSgsgdQg4gkhNAAQiQAAiQA6gAjCjpQg3AXgoApQgsArgXA6QgQAqgLA8QgMA/gDBEQAQgWAigdQAigfAlgXIALgHQB4hJCegxQBsghCHABQgphLg/gmQhQgwh8AAQhIAAhFAdg");
	this.shape_2.setTransform(42.8,30);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.2,0.7,85.2,58.7);


(lib.HardHatIsolatedWhite = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.StaySafe();
	this.instance.setTransform(46.1,13.4,0.07,0.07,-9,0,0,109.7,97.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F2EBCF").s().p("AjpAFQAThGAag/QBFgdBIAAQB9AABQAwQA+AmApBJQiGgBhtAhQieAxh4BLQAIhOAThLg");
	this.shape.setTransform(42.2,19.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E2DCC2").s().p("AlECaQANgMApgfQAdgYAagMQBJgqBMgeQCRg4CPAAQBOABA4AjQAsAbAAAVQAAALgSATQgUAVgiAUQgSAMgdAUIghgdQgRgNgdgJQhLgXhjAEQhUADg/AOQhTAThGAoIgLAHIhVBBQAJgaAjgggAmAA5QALg9ARgqQAXg6ArgrQApgpA2gWQgaA+gSBJQgTBKgJBNIgLAGQgkAXgjAgQghAdgRAVQAEhEALg+g");
	this.shape_1.setTransform(42.8,27.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#212112").s().p("AmeEeQgEgEgCgJQgDgMAAgWQABgUgBgZIgCghQADjnB4h4QA2g1BFgYIAkgLQBagYBnASQA2AJAgAOQAZAMAjAaQAeAYAXAeQAUAcAUApIABACIADgBQBCAJAmAnQAdAfgBAbQgBAig3AqIhfBCQgKgBgDgCQgEgDAAgOIgIgHQgQgOgNgHQg0gghoAAQjTAAh+BPQgRALgQANQgoAggfAsIgJANQAAACgRAeQgIgDgDgEgAhBgRQhNAchJApQgZAMgeAZQgpAfgNAMQgjAggIAZIBUhAIALgHQBGgpBUgSQA+gOBVgDQBigEBLAXQAdAJASANIAgAdQAdgVATgLQAhgUAUgVQASgTAAgMQAAgSgsgdQg4gkhNAAQiQAAiQA6gAjCjpQg3AXgoApQgsArgXA6QgQAqgLA8QgMA/gDBEQAQgWAigdQAigfAlgXIALgHQB4hJCegxQBsghCHABQgphLg/gmQhQgwh8AAQhIAAhFAdg");
	this.shape_2.setTransform(42.8,30);

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#A5A08D").s().p("AiUCSQgegBgagCQgegEgZgGQgvgLgIgIQgKgJgBgKQgPgBgFgNIgCgMIAAgCQAAgoCMhPIAJgGQCThUBDgDIDVAAQA2AIAkAnQAbAdACAYIAAADQAAAIgGAGQgGAGgIAAQgEAAgHgFQgRAXgkAYQgkAZgeAOIgRAIQhvAwhFAPQhPAQg9AAIgJAAg");
	this.shape_3.setTransform(36.5,46.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#212112").s().p("AiwBtIgcgDQhigMgggVQgUgLgBgbQAAgGADgIQAEgIAAgIIAAAAIACALQAFAOAQAAQABAKAKAJQAIAIAvAMQAZAGAdADQAbADAeABQA/ABBVgSQBFgOBwgxIAQgFQAfgOAjgaQAlgaAQgXQAHAFAEAAQAJAAAGgGQAGgFAAgJIgBgDQAIAAABACQABACgSAWQg3BEhnAvQhMAlhnAaQhPARg8AAQgVAAgSgCg");
	this.shape_4.setTransform(36.6,53.1);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.2,0.7,85.2,63.6);


(lib.HardHatIsolated = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.StaySafe();
	this.instance.setTransform(46.6,13.4,0.07,0.07,-9,0,0,109.7,97.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A0A056").s().p("AhVBFQALg/AQgoQAXg6ApgrQApgpA3gWQgaA+gTBJQgTBIgIBPIgLAGQgjAXgiAgQgiAdgRAVQAEhEAMg+g");
	this.shape.setTransform(13,26.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CECE6F").s().p("AlUBLQANgMApgfQAegYAZgKQBJgqBNgeQCQg6CQAAQBNABA4AjQAsAdAAAVQAAALgSATQgUATghAUQgTAMgdAUIgggdQgSgNgdgJQhLgVhiAEQhVADg+AMQhUAThGAoIgLAHIhUBBQAIgaAjggg");
	this.shape_1.setTransform(44.4,35.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF91").s().p("AjpAFQAThGAag/QBFgdBIAAQB9AABQAwQA+AmApBJQiGgBhtAhQieAxh4BLQAIhOAThLg");
	this.shape_2.setTransform(42.2,19.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#212112").s().p("AmeEeQgEgEgCgJQgDgMAAgWQABgUgBgZIgCghQADjnB4h4QA2g1BFgYIAkgLQBagYBnASQA2AJAgAOQAZAMAjAaQAeAYAXAeQAUAcAUApIABACIADgBQBCAJAmAnQAdAfgBAbQgBAig3AqIhfBCQgKgBgDgCQgEgDAAgOIgIgHQgQgOgNgHQg0gghoAAQjTAAh+BPQgRALgQANQgoAggfAsIgJANQAAACgRAeQgIgDgDgEgAhBgRQhNAchJApQgZAMgeAZQgpAfgNAMQgjAggIAZIBUhAIALgHQBGgpBUgSQA+gOBVgDQBigEBLAXQAdAJASANIAgAdQAdgVATgLQAhgUAUgVQASgTAAgMQAAgSgsgdQg4gkhNAAQiQAAiQA6gAjCjpQg3AXgoApQgsArgXA6QgQAqgLA8QgMA/gDBEQAQgWAigdQAigfAlgXIALgHQB4hJCegxQBsghCHABQgphLg/gmQhQgwh8AAQhIAAhFAdg");
	this.shape_3.setTransform(42.8,30);

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#444324").s().p("AhUhpIAzAAQA0AIAkAnQAcAdABAXIAAAEQAAAGgFAGQgHAGgIAAQgEAAgHgGQgRAXgkAbQgiAZgeAOIgQAHg");
	this.shape_4.setTransform(62.9,42.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#444424").s().p("AhACSQgegBgbgCQgdgEgZgGQgvgLgIgIQgKgJgBgKQgQgBgFgNIgCgMIAAgCQAAgoCNhPIAJgGQCRhUBEgDICjAAIADDUQhwAwhFAPQhRAQg7AAIgIAAg");
	this.shape_5.setTransform(28.2,46.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#212112").s().p("AiwBtIgcgDQhigMgggVQgUgLgBgbQAAgGADgIQAEgIAAgIIAAAAIACALQAFAOAQAAQABAKAKAJQAIAIAvAMQAZAGAdADQAbADAeABQA/ABBVgSQBFgOBwgxIAQgFQAfgOAjgaQAlgaAQgXQAHAFAEAAQAJAAAGgGQAGgFAAgJIgBgDQAIAAABACQABACgSAWQg3BEhnAvQhMAlhnAaQhPARg8AAQgVAAgSgCg");
	this.shape_6.setTransform(36.6,53.1);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.2,0.7,85.2,63.6);


(lib.HardHat = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.StaySafe();
	this.instance.setTransform(42.1,14.5,0.07,0.07,-9,0,0,109.7,97.7);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A0A056").s().p("AhVBFQALg/AQgoQAXg6ApgrQApgpA3gWQgaA+gTBJQgTBIgIBPIgLAGQgjAXgiAgQgiAdgRAVQAEhEAMg+g");
	this.shape.setTransform(13,26.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CECE6F").s().p("AlUBLQANgMApgfQAegYAZgKQBJgqBNgeQCQg6CQAAQBNABA4AjQAsAdAAAVQAAALgSATQgUATghAUQgTAMgdAUIgggdQgSgNgdgJQhLgVhiAEQhVADg+AMQhUAThGAoIgLAHIhUBBQAIgaAjggg");
	this.shape_1.setTransform(44.4,35.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF91").s().p("AjpAFQAThGAag/QBFgdBIAAQB9AABQAwQA+AmApBJQiGgBhtAhQieAxh4BLQAIhOAThLg");
	this.shape_2.setTransform(42.2,19.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#212112").s().p("AmeEeQgEgEgCgJQgDgMAAgWQABgUgBgZIgCghQADjnB4h4QA2g1BFgYIAkgLQBagYBnASQA2AJAgAOQAZAMAjAaQAeAYAXAeQAUAcAUApIABACIADgBQBCAJAmAnQAdAfgBAbQgBAig3AqIhfBCQgKgBgDgCQgEgDAAgOIgIgHQgQgOgNgHQg0gghoAAQjTAAh+BPQgRALgQANQgoAggfAsIgJANQAAACgRAeQgIgDgDgEgAhBgRQhNAchJApQgZAMgeAZQgpAfgNAMQgjAggIAZIBUhAIALgHQBGgpBUgSQA+gOBVgDQBigEBLAXQAdAJASANIAgAdQAdgVATgLQAhgUAUgVQASgTAAgMQAAgSgsgdQg4gkhNAAQiQAAiQA6gAjCjpQg3AXgoApQgsArgXA6QgQAqgLA8QgMA/gDBEQAQgWAigdQAigfAlgXIALgHQB4hJCegxQBsghCHABQgphLg/gmQhQgwh8AAQhIAAhFAdg");
	this.shape_3.setTransform(42.8,30);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.2,0.7,85.2,58.7);


(lib.Greg_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 3
	this.instance = new lib.HardHatWhite();
	this.instance.setTransform(92.7,33.6,1.89,1.89,0,0,0,43.1,29.6);

	this.instance_1 = new lib.HardHat();
	this.instance_1.setTransform(92.7,33.6,1.89,1.89,0,0,0,43.1,29.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[]},1).wait(1));

	// Layer 2
	this.instance_2 = new lib.GregHead();
	this.instance_2.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 1
	this.instance_3 = new lib.Male_Greg_Front();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.YardSteve_01();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.YardGreg_02();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_6 = new lib.YardSteve_03();
	this.instance_6.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_7 = new lib.YardGreg04();
	this.instance_7.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3}]}).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.GotItButton = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.GotIt();
	this.instance.setTransform(73,31.7,1,1,0,0,0,99,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AvnFHIAAqNIfPAAIAAKNg");
	this.shape.setTransform(100.1,32.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1,x:73}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85,x:99.1}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-28,-2,202.2,67.5);


(lib.GotItBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Got_It();
	this.instance.setTransform(99,31.7,1,1,0,0,0,99,31.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F4D96F").s().p("Av0FQIAAqfIfqAAIAAKfg");
	this.shape.setTransform(98.9,31.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,202.2,67.5);


(lib.Girl02_YardDrivingcopy = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#282828").s().p("AgjCaQgQgFgOgGQgrgOgigVQgUgQgRgTIgIgKQgegpgHg6IAAg/IAAgEQAAgjADgdQAUB5BIA8QAOAKAPAIQA5AlBhAPICHAKIADAIQACAKASAfIAEAAQAAAPAIAEQg/AGg3AAQhRAAg8gOg");
	this.shape.setTransform(31.8,54.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AA5DlQhhgPg5glQgOgIgPgKQhIg+gUh3QAEglAGgdQACgGAAgIQACgCAAgEQABgBAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBIAEgIQAEgOAJgRQAlgxApghIAGgEIAEAAQAUAEAHAEQApAXACBcIAEBKQAAANACAKIgCAAQAAABAAAAQgBABAAAAQAAABAAAAQgBABAAAAQgCAEAAAIIACAFQgBAAAAABQAAAAgBABQAAAAAAABQAAAAAAABQgGAQAAATQAAAGACACQAAACAIACIAAAEQAAAGAEAGQAIAJALAEQAQAGAdAAIACgCQAAAAABAAQAAgBAAAAQAAAAABAAQAAgBAAAAQAOAEAPAAICNAAQgOAbgGAeQgGAbAAAdIAAAlIiIgKg");
	this.shape_1.setTransform(31.5,40.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#774736").s().p("AhBBVQgGgTgCgCIAAgOQAAg0ABgKQADggAQgoQAsgMAfAKQAgALALAcQAIARAAAOIAAANIABAMIgBAOQAAAhgEAGQgLAOg6APg");
	this.shape_2.setTransform(63.4,55.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ah8EhQgRgCgOgEIgCAAQgEgCgFAAQg1gRglgdIAAABIgQgPIgLgKQgMgPgIgOIgCgGQgGgIgFgLQgQglgEgtQgCgQAAg+QAAhAAEgaQAEg0ARgpIAEgMQAYg2AuglIAHgGQAugDAzAGQAlAYATBGQAOA5AABKIAAAHIAAAiQgEARgGAJIACABQAfAAAMAKQAfAFA1ACIAbAAIA8AAQACAEAEAAIAIgLQAEgEACgEIACAAQAdgLAdgDQAggCAaALQAeAMAQAbQAPAagBAhQAABCgEARQgNA3g1AMIhmAAQACAEAAAQQAAAPgCAEQgCAEgKAGIiKAAQgYACgYAAQgyAAgrgIgAjEkUQgpAhglAxQgJAQgEAPIgEAIQAAAAAAABQAAAAAAABQAAABgBAAQAAABgBAAQAAAEgCACQAAAIgCAHQgGAcgEAlQgCAdAAAjIAAACIAABAQAGA8AfApIAIAKQAQASAVARQAhAUArAPQAOAGARAEQBmAZCdgRQgJgEAAgOIgEAAQgSgfgCgKIgCgJIAAglQAAgcAGgbQAGgfAOgbIiLAAQgPAAgOgEQAAABAAAAQAAAAgBABQAAAAAAAAQgBAAAAAAIgCACQgdAAgQgGQgNgEgIgIQgEgGAAgGIAAgFQgIgCAAgCQgCgCAAgFQAAgRAGgRQAAAAAAgBQAAAAAAgBQABAAAAgBQAAAAABgBIgCgGQAAgIACgEQAAgBABAAQAAgBAAAAQABgBAAAAQAAgBAAAAIACAAQgCgLAAgMIgEhKQgChdgpgWQgHgEgUgEIgEAAIgGAEgACwAbQgQApgEAfQgBALAAA2IAAAOQACACAGASIBDAHQA6gPALgPQAFgFAAghIABgOIgBgPIAAgMQAAgOgIgRQgLgdgggKQgPgFgSAAQgVAAgXAGg");
	this.shape_3.setTransform(40.4,44.5);

	// Layer 9
	this.instance = new lib.StaySafe();
	this.instance.setTransform(105.3,24.1,0.093,0.093,-9,0,0,110,97.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#774736").s().p("Ag+ByQAAgwgEgWIAAiVQAjAFAegNIAdAtQAnA1AAAVQAAAIgJAGQgDgHgHgEQgOgIgLgGQgVgnAAAsQAAADAJAgQAJAfAAAHQAAAKgGAEIgQgjQgJgTgYgRQgGgKgFAAQgQAAAAAWQAAALAHAcQAHAdAAAKQAAAGgCABIgCABg");
	this.shape_4.setTransform(128.3,81.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#826631").s().p("AgQAwIgLAAQgKg1gJgTQAYARAJARIAQAjQgEADgJAAIgGAAgAASglIgGgKQALAIAOAIQAHAEADAHQgEADgHACIgHACIgLgYg");
	this.shape_5.setTransform(129.4,85.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AhiA+IgBhWQgBgqADgdQAHg5AYAuIAACVQAEAWAAAwIAKAAIACgBQACgBAAgGQAAgKgHgdQgHgcAAgLQAAgWAQAAQAGAAAFAKQAJATAKA3IALAAQAMABAHgEQAGgEAAgKQAAgHgJgfQgJggAAgCQAAgtAVAnIAGAIIALAYIAHgCQAHgCAEgDQAJgGAAgIQAAgVgng1IgdgtQgIgMAAgDQAAgYAQAGQAPAGAXAbQA3BEAAA3QAAAagCAFQgIATglAAIgBAAQADAKAAAGQAAAKgNASQgRAVgVAAQgKAAgIgCQgBAGgCADQgMASgoAAQgXAAgGhXg");
	this.shape_6.setTransform(128.3,81.4);

	// Layer 2
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#A88541").s().p("Ah6AaQAWgGAQgQQA6gTBBATQA3AYAVAwQALAcANAyIj4AEgAApgnQhEgRhGARQgFACgWAUIgDACIgLh0QArADAwABQBkABAegNQAGgCAEgGIAHgKQAAAHgFA3QgGA1AFAfQgXgQgegMg");
	this.shape_7.setTransform(60.5,148.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#826631").s().p("AghCYQgKg1gKhhQgNh3gFgnQAUAGAQAGQAkAGAqACIALB0QgVAUgJAMQAIAIAMAEIAPgCIANCEIgrABQgFABgLAAQgPAAgfgEg");
	this.shape_8.setTransform(42.2,149.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AiwDKQgbgfgSiGQgMhfAAg4IABgtIgBAAQAFgOADgOIARgGIABAAQAkAKAUACQA8AJBMAAQBKAAA7gJQALgMAGgFQAJgGAOgCQAHACAHAAQAGACAAAQIAAABIABACIgCAEIgWB+IgBADQABAAAAABQAAAAgBAAQAAAAgBAAQAAABgCAAIAEAQIAAAbQA6AmAPAuQAGAUAABAQAAATgDAFQgDAFgOAKIkHAEIguABgAi1AJQAKBhAKA2QAzAHANgFIArgBID4gEQgNgzgLgbQgVgwg3gYQhDgTg4ATQgQAQgWAGIgPADQgMgFgIgIQAJgNAVgSIADgDQAWgTAFgCQBEgSBGASQAeAMAXAPQgFgeAGg1QAFg4gBgGIgGAKQgFAGgFACQgeAMhmgBQguAAgrgDQgqgDgmgFQgQgHgUgFQAFAnANB2gAC2iiIABgRIAIAAIABAGQAAAIgEARIgCABQgEgEAAgLg");
	this.shape_9.setTransform(55,148.6);

	// Layer 7
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#A88541").s().p("Ah1CSIgjgBIgSkgQCAAOBwgUQgJAMgBAcIgBA2QgwgLhEAfQhAAdAAAPQAAAZABgKIABAHQADAGAOgIQCFhOBKAxQA6AoAIBuIg7AAQh5AAhsgEg");
	this.shape_10.setTransform(100.7,149.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#826631").s().p("AgfCaQABgNgMh2QgBgMgCgHIABgEQAAgRgFglQgFglABgSIAFgvIAEADQATAQAQAAIAtAHIATEfIhWgDg");
	this.shape_11.setTransform(79.9,148.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AACNCIhmgFQgLgCgEgCQgFgDAAgQIAAgHQgJgMgKhDQgJg6AAgSQAAgRAFgIIACgCQgCggAAg3IABgwQABgWAEgOIgBgCIACgUQATgCApAWQCSAcCJgcQANgNAHgEQAJgGAOgCQAGADAIAAQAGACAAAPQAAAUgWAOIgCACQAEAEAAAOQAABAgFAnIgBAIIAaAPQAbARAVAZQAIAKAYAkQAQApgQAqIADAAQgVAZgHARQizAHgVAAIh7gFgAh7IFQAAASAFAlQAEAlAAASIAAAEQACAHABANQALB2AAANIBXADIAhABQCFAFCdgBQgIhug6goQhKgziHBQQgOAIgDgGIgBgHQgBAKAAgZQAAgRBAgdQBGgfAwALIABg2QABgcAIgMQhxAUh/gOIgugGQgQAAgUgQIgDgDIgGAvgAiEHXIABgIIAAgGIgBgCIgBgCIABASgAkTrjQhMgYgFgYQgEgPAggRQAggRBUgCQBTgBBNATQBLATgJAUQgEAHgEAPQgGANgSALQgZANhaAAQhdgBgxgQg");
	this.shape_12.setTransform(86.8,85.9);

	// Layer 3
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#022F47").s().p("AhgD/QgWgBgOgEIgigNQgQgHgIgHIgGgFQhjkvgLgdQgWg9gNg/IBzAAIAdgHIAhgJQAGApAIAsQAEAWAQCoQAQCgALAwIAVAagADXD4IhAgKIAAgHQgCgagDiAIgDiLQAQAcAXAYQALAKA6CBQA0BzAnAKIgQAAQg4AAg3gGg");
	this.shape_13.setTransform(59.7,122.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#034E75").s().p("Ak0E/QgMgwgPigQgRiogEgWQgIgsgFgpIBQgUQAwgNAPgQQAOgQABgoIgBhLIA6AGQBfAJCxgGIA6gDQAFAWAIAYIAOAlQAYBAAhARQAYAMBHAAIAPgBQgJBNgXBDQgQAuggBUQgFALgLBoIgOBtIgsADQgwAGggAAQgngKgzhzQg4iCgLgLQgXgYgRgcIgFhWQAEg3gCgYQgnADgFAoQgDAUAFAVIAEBcIADCMIADCGIAAAKQgBAIgVADIiqANg");
	this.shape_14.setTransform(80.3,113.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#333333").s().p("AiLFjQACADgegDIgsgGIg6gHIAAgwIAAgJIgBgdIgBgWQgBgbgCgPIgei/IgDgPQgFgkgQg4IgdhdQgqhWgogtIAtgRIAUgEIAagHIEwAAIABAAIAFABQARAEAfALQAdAKAMADQAnBNABCyIADBCIAJEPQAEA6AGAkIg/ABIguABQhVAAg6gEgAFhEaIgBAAIgDgBQgGgBABgLQACgKAAgFIAAgCQgEg9ACgGQAHgjACgVQADgXgBgWIgDgVIgBgPIgBgRIgBgDIAAAAIAAgBIgBgRQAAgKgDgIIAAgCQAAgMgMg8QgLg6gCgFQgLgwgSgjIAQALIAiAbIABAAIAAABQAJAIAkBRQBBEegPA3IgBADIgEAHQgFAHgFAFIgTAPQgLAFgLABIgBAAIgCAAIgBAAIgBAAIgDAAQgKAAgKgCg");
	this.shape_15.setTransform(85.9,39.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#223825").s().p("AgPCrIgDgGIgYiEIgThjQAGgfACgLIAAgCQAEgEADgPQAEggAHgPQAOggAWgjQAeAnAQAwQAFAOAIBNIACAPIgFAIIAAABIAAADQAAALgGAbIgEAIIABAVQAAAJgEAXIAAACIgEAVIgCAHIgDANIgKAwIADgJIgIAgIgBACIgBAHIgDAJQgFAUAAAGIABAOIgBAHIAAACIgKACQAAgkgPglg");
	this.shape_16.setTransform(20.8,45);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFF91").s().p("AlUF0QgDguACglIAKiZIADg/QAEhlgIg1IgCgQQgShugahCQgXg4gug7IABAAIBTgcIAHAIQAdAgATAcQAPAWANAXIAFAKQAsBVAPBtIAAAEQAQByANBNIADAaIAAAKIABAJIAAAJIABAVIAAAGIABA5IABBaQAAAkgMATQgVAkhbAUIgfAGIgFhFgAFKGkQgSgWgJglQgIgsgHgZQgRhUgEg6IAAgFIAAgLICpgFIABAVIAAASQADAmAEALIAEA0IgECfIAAAEQhQgJgigDgAD/i7QgEhjgHgsQgMhPgdgfQAAAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAIAEAAQAGAAAWAOQATAJAeAKQAaALAhASIAAACQAFAEAGAKQAZAkAYBHQAFAZAQA1QAFAUABAcIAAAOIAAAZQACADAAAFQAAAHgEANIgDAJIAAABIABABQgCAKgGAOQgFAMgFADQgIAVgMARQgTAbgtAcQgrAagQASg");
	this.shape_17.setTransform(73.3,50.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E5E582").s().p("AnBEpIADgSIAHghQAEgXAGg1IACgPIAHgTIAIgaIADgKQAGgbAFgiIAOhQQAFgaAAgeIAAgKIAAgCIgBgJIAAgFIAAgCIgBgBIABgVQAAg1gBgDIgfhVQgIgagFgNQgJgVgQgLIgDgDQgMgOgRgIQAigSAsgPQAuA7AXA4QAaBCASBuIADAQQAIA1gEBlIgEA/IgJCZQgCAlACAuIAFBFIgXAEIh9ACQgJg8AAg9gAldC4IACAAIgBgBIgBABgAE2BfQAQgRAsgaQAsgcAUgZQAMgRAHgXQAFgDAFgMQAHgPABgJIAAgBIgBgBIADgJQAEgNAAgHQAAgFgBgDIAAgZIgBgOQADAGAAALIAAABIAEAZIAAA1QABAQgBAJIAAACQAAACgFAKQgEAggSAbQgOAWgQANQAAALACADIACADQAEAEATAAQAFAAAJgEIAMgGIABASIipAFg");
	this.shape_18.setTransform(69,53.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#A4C6C6").s().p("AgtgXIAOgaQAAgPgCgIQASgSASgNIAFgEIAWAVIAQASQgYAkgOAgQgHANgDAfQgCAQgFAEIAAACQgBALgGAfQgRhUgMgvg");
	this.shape_19.setTransform(16.3,27.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#371E2D").s().p("AAnAOIABgBIABABIgCAAgAgoAAIAAgLIAAgCIADAAIgCANIgBAAg");
	this.shape_20.setTransform(30,70.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("ABZLvQg4gFgPgHIgRgIIgDgCIgBgBIgKAFQgsAVhKAEQgmAFgngGQgXgEhOgUQgZgFgDgDQgfgchGjbQgwiUgUhzIgFgFQgEgEAAgIQAAgIAFgEQgKg+AAg0QAAhBAFgVQACgKADgHIgDAAIADgeIAAgLIALgCIAAgCIAAgHIgBgOQAAgGAGgUIACgJIACgHIAAgCIAJggIgEAJIAKgwIADgNIACgHIAEgVIAAgCQAEgXAAgJIAAgVIADgKQAGgbAAgLIAAgDIAAgBIAFgIIgCgPQgIhNgEgOQgRgwgegnIgPgTIgWgVIgFAEQgVAOgSARQACAJAAAOIgNAaIgHgbIAEgaIACgNIASgdQACgDAOgKQAPgMAJgFQAngXA1gUQApgRAygOIAhgJIBHgQICGgTIDaAAQAiAFBHAUQBDATACADIABABIAsAQIBDAbQApASAeAWQBjBLARBpIAcCqQAMBHAAAWQABAxgBAHIABALIAAAFIAAAFIgBAFIgNAdIAAAAIgCADIgGAGQgMANgTAGQgJAEgMACQgMACgOAAIgMgBIADAbQAFAoAAAOQAABcgCAdQgGBTgZBNQgOArgjC0QgeCZgRAYQABACgDAFQgDAGgGABQAEABgVAGIgrAIQg5AKgzAAQgyAAg6gKgAmsFXQAKAdBkExIAGAFQAIAHAQAHIAiANQAOAEAVABIAOAAICqgNQAVgDABgIIAAgKIgDiGIgDiMIgEheQgFgVADgUQAFgoAlgDQACAYgEA3IAFBYIAECNQACCAACAaIAAAHIBAAKQBAAHA/gBQAgAAAwgGIAsgDIAOhtQALhoAFgLQAghUAQgwQAXhDAJhNIgPABQhHAAgYgMQghgRgYhAIgOglQgIgYgFgWIg6ADQizAGhdgJIg6gGIABBLQgBAogOAQQgPAQgwANIhQAUIgiAJIgcAHIhzAAQAMA/AXA9gAnMhOIAAANIACAAQgGA2gFAUIgHAhIgDATQAAA9AJA7IB9gCIAXgEIAfgGQBbgUAVgjQAMgUABgkIgChYIAAg4IgBgHIAAgVIgBgJIAAgIIgBgLIgCgZQgNhNgQh1IgBgEQgPhsgshWIgFgKQgMgXgPgVQgUgdgcgfIgIgJIhSAcIgBABQgtAPghARQAQAJANAOIACACQAQAMAJAVQAFAMAIAaIAfBVQABADAAA1IAAAVIAAACIAAABIAAAGIABAJIAAABIAAAKQAAAfgFAaIgOBSQgFAigGAbIgDAKIgIAaIgHASIgDAAIAAACgADYrBQAeAeAMBQQAGAsAEBjIAME5IAAAJIAAALIAAAFQAEA6ARBTQAHAYAJAsQAJAlARAXQAiACBQAKIAAgEIAEigIgEgyQgEgLgCglIgBgTIgBgVIgBgSIgMAGQgJAEgFAAQgTAAgEgDIgCgEQgCgCAAgLQAQgNAOgWQASgcAEghQAFgKAAgDIAAgBQACgJgCgRIAAg0IgEgaIAAAAQAAgLgCgGQgBgdgGgUQgPg1gFgYQgYhHgZglQgHgJgFgFIABgCQgigSgagKQgdgLgUgIQgWgOgFAAIgFgBIgBAAQAAAAgBAAQAAAAAAAAQAAAAAAAAQAAABAAAAgAC9gIIA/gBQgGglgEg6IgJkRIgDhCQgBixgnhNQgMgDgdgKQghgMgRgDIgFgBIgBAAIkuAAIgaAGIgUAEIgtARQAoAtAqBXIAdBcQAQA5AFAkIADAQIAeDAQACAPABAbIABAVIABAeIAAAIIAAAwIA6AHIAsAGQAcAEAAgDQBGAEB3gBgAHLoCQACAFALA7QAMA8AAAMIAAACQADAIAAALIABARIAAACIAAAAIACAUIABAPIADAUQABAWgDAYQgCAUgHAjQgCAGAEA+IAAABQAAAFgCALQgBAKAGACIADAAIABAAQAMADALgBIABAAIABAAIACAAIABAAQAMgBAKgFIATgOQAGgGAEgHIAEgGIABgEQAPg3hBkgQgkhQgJgJIAAAAIgBgBIgigbIgQgLQASAjALAwg");
	this.shape_21.setTransform(72,76.8);

	// Layer 1
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#748474").s().p("AEXFAIgtgGQhJgKh6gOIgJgBIgigEIhMgLIAAAAQgXgNgegHIgPgCIAHgTQANgmAJg7IAOhSQAFgaAAgcIAAgKIAAgCIgBgJIgBgHQAHgCAHgBQAFgBAJANQAHAKAFAKQADAGAIAwQAOA1AlAsQBCBKB4AkIBMAXIAMAFQAYANAJARQAAABgGAAIgWgBgAkyieQAQghALgSQAAgPgCgIQASgSAUgOQAtgfBKgZQgVAPgiAvQgnA0gUAuQgHAOgFAgQgCAQgFADIAAACQgCAXgXBiQgXhTgBhng");
	this.shape_22.setTransform(41.1,43.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#A5BCA5").s().p("AAFFjQgHgRgYgNIgMgFIhMgXQh4gkhChKQgngsgOg1QgIgwgDgGQgFgMgHgKQgJgNgFABQgHABgHACQgDgQgHgFQgKgHgPAVIgHAIIAAAEQAAALgHAbIgDALIABAVQAAAJgEAXIAAABIgFAWIgBAHIgDANIgLAvIAEgJIgIAhQgKAjAAAJIABANIgBAIIAAACIgLACQgCglgOglIgkhPQgRglgLgqQAXhiACgXIAAgCQAFgDACgQQAFggAHgOQAUguAng0QAigvAVgPIB8gqQAlgNAQgHIAVgEIAZgHIEvAAQBfAOAYAAQALAAAEACQAtATBYAxQAKAFAaATQBBApAUAUQAIAIAlBRQBAEegOA3QgbA2g9gOQgHgBABgLQADgMgBgFQgDg9ABgGQAIgjACgVQACgXgBgWIgCgVIgBgPIgCgRIgBgDQgCgMgJgeQgJgggLgSQgNgWgSgWQgbghgUgEQgVgEAAAiQAAAFAqA5QArA5AFA/QAGA6gdArQgOAWgPANQAAALACADIACADQADAEATAAQAGAAAIgEIANgGIACA5QACAlAEAMIACADIgJACIgPAGQgRAGgXAFQhAAOikACIgnABQhKAAg3gEg");
	this.shape_23.setTransform(71.3,39.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#396086").s().p("AAqCaQg0ingRgcQhFh0gciBIAHghIAHglQAeAMBhARIAIABIAFAAIASADQABB6AcChQAFAVAQCpQAPCfAMAwQgigrgxigg");
	this.shape_24.setTransform(36.8,109.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#396087").s().p("AhYFPQgWAAgOgEIgigNQgQgIgIgGIgGgFQhjkwgLgcQgziLAAiQIADgSQAcCBBGB0QARAcA1CnQAyCgAhArIAVAagADfFJIhAgKIAAgIQgCgagDh/IgDiOQAQAdAXAYQALALA6CCQA0ByAnALIgQAAQg4AAg3gGg");
	this.shape_25.setTransform(58.9,114.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#371E2D").s().p("AAgAMIhIgMIAAgLIAAgCIADAAIAPACQAcAHAXALIAAAAIABAAIALAHIgJgCg");
	this.shape_26.setTransform(30,70.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#4B7DAF").s().p("AkwFJQgMgwgPigQgRiogEgWQgdihAAh6QBEAJCjAPQBZAICggFQCtgGBXgRIAXgGIgECgQgHBkgdBVQgQAuggBUQgFALgLBoIgOBtIgsADQgwAGggAAQgngKgzhzQg4iCgLgLQgXgYgRgcIgFhWQAEg3gCgYQgnADgFAoQgDAUAFAVIAEBcIADCMIADCGIAAAKQgBAIgVADIiqANg");
	this.shape_27.setTransform(79.9,112.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("ABcLvQg4gFgPgHIgRgIIgEgCIAAgBIgKAFQgsAVhKAEQgmAFgngGQgXgEhOgUQgZgFgDgDQgfgchGjbQhRj5gBidQAAhBAFgVQACgKADgHIgDAAIADgeIgBgLIAMgCIAAgCIAAgHIgBgOQAAgIAKgkIAJggIgEAJIAKgwIADgNIACgHIAEgVIAAgCQAEgXAAgJIAAgVIADgKQAGgbAAgLIAAgEIAHgLQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAnIgHASIgDAAIAAACIAAANIBKANIAJACIgLgHIAAgBIBLALIAkAFIAJABQB7AOBJAJIAsAGQAcAEgCgDQBFAEBjgBQCkgCBBgOQAXgFARgHIAOgFIAJgDIgBgDQgEgLgCglIgDg6IgMAGQgJAEgFAAQgTAAgEgDIgCgEQgCgCAAgLQAQgNAOgWQAcgrgFg6QgGg/grg7Qgpg6AAgFQAAghAVAEQATAEAbAgQATAWANAXQALARAJAhQAJAgACAMIACAUIABAPIADAUQABAWgDAYQgCAUgHAjQgCAGAEA+QAAAEgCANQgBAKAGACQA+ANAag2QAPg3hBkgQgkhQgJgJQgUgUhBgpQgagSgJgFQhZgxgsgTQgFgCgLAAQgYAAhfgOIkuAAIgaAGIgUAEQgQAHgmANIh7AqQhKAZgtAgQgVAOgSARQACAJAAAOQgKASgQAiIAAgJQAAgKAKg8IACgNIASgdQACgDAOgKQAPgMAJgFQBwhCCvghICGgTIDaAAQAnAFA3AKQA4ALABACIACABIA2AaQA3AaAYAKQApASAeAWQBjBLARBpIAcCqQAMBHAAAWQABAxgBAHIABALQAAAfgWAWQgNANgSAGQgJAEgMACQgMACgOAAIgMgBIADAbQAFAoAAAOQAABcgCAdQgGBTgZBNQgOArgjC0QgeCZgRAYQABACgDAFQgDAGgGABQAEABgVAGIgrAIQg5AKgzAAQgyAAg6gKgAnSAJIgHAhIgDATQAACQAzCKQAKAdBkExIAGAFQAIAHAQAHIAiANQAOAEAVABIAOAAICqgNQAVgDABgIIAAgKIgDiGIgDiMIgEheQgFgVADgUQAFgoAlgDQACAYgEA3IAFBYIAECNQACCAACAaIAAAHIBAAKQBAAHA/gBQAgAAAwgGIAsgDIAOhtQALhoAFgLQAghUAQgwQAdhVAHhkIAEigIgXAGQhXARitAGQiiAFhXgIQijgPhEgJIgSgCIgFgBIgIgBQhjgOgegNIgHAjg");
	this.shape_28.setTransform(71.7,76.8);

	this.addChild(this.shape_28,this.shape_27,this.shape_26,this.shape_25,this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.instance,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(6.4,0.7,132,169.2);


(lib.Frank_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 3
	this.instance = new lib.HardHatWhite();
	this.instance.setTransform(91.9,33.6,1.89,1.89,0,0,0,43.1,29.6);

	this.instance_1 = new lib.HardHat();
	this.instance_1.setTransform(91.9,33.6,1.89,1.89,0,0,0,43.1,29.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[]},1).wait(1));

	// Layer 2
	this.instance_2 = new lib.FrankHead();
	this.instance_2.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 1
	this.instance_3 = new lib.Male_Frank_Front();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.YardSteve_01();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.YardFrank_02();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_6 = new lib.YardSteve_03();
	this.instance_6.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_7 = new lib.YardFrank04();
	this.instance_7.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3}]}).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.ForksFront = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.LiftBit();
	this.instance.setTransform(46.7,123.6,1,1,0,0,0,46.2,43.1);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.5,112.8,91.8,53.9);


(lib.ForksBack = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.LiftBitBack();
	this.instance.setTransform(36.4,30.1,1,1,0,0,180,46.2,43.1);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-9.3,18.8,91.9,54.4);


(lib.ForkLiftDriver01back = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{Male_Steve_01:0,Male_Steve_02:1,Male_Greg_01:2,Male_Greg_02:3,Male_Jason_01:4,Male_Jason_02:5,Male_Frank_01:6,Male_Frank_02:7,Female_Kate_01:8,Female_Kate_02:9,Female_Jane_01:10,Female_Jane_02:11,Female_Lucy_01:12,Female_Lucy_02:13,Female_Peppa_01:14,Female_Peppa_02:15});

	// Layer 5
	this.instance = new lib.YellHatBack();
	this.instance.setTransform(13.9,9.9,1,1,0,0,0,16.9,9.6);

	this.instance_1 = new lib.WhiteHatBack();
	this.instance_1.setTransform(13.9,9.9,1,1,0,0,0,16.9,9.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false,scaleX:1},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false,scaleX:1},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).wait(1));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#352010").s().p("AhABKQgOgBgHgEIgBAAQgMgCgIgFQgGgCgGgEIAFACIAMAFIBKAAQAjgBAUgMQARgKAAgOIgFgMQgFgLAAgHQAAgNAJgJQAIgKAAgFQAAgEgEgFQgEgGAAgEQAAgSARAIQAJAEAEAIQAIgHAQAAIAJABQANAMAAAPQAAAKgGAKQgGAKAAACQAAACACADIADADQAFAHAAAGQAAAXgTALQgNAHgkAIIAAABIgKACIgTAEIgoAAIgOACgAh3A4IABAAIgBAAg");
	this.shape.setTransform(18.6,24.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgwBXIgGgBIgGAAQgogEgVgTQgRgPAAgTQAAgKAFgJQAAgOgDgKQgEgNABgKQAAgHABgFQADgIACgJQADgMAHgBQAFgGAHgBIgDAHIAAABQgDAHgJAOQgFALAAAJQAAAPAIAQIABADIABACIgGAHQgEAIgBAHIAAAEQgBAMAMAOQAGAIAIAEIAAABQAGADAHACQAIAFALACIABAAQAHAEAOABIAgAAIANgCIAoAAIATgEIAKgCIAAAAQAkgIAOgIQASgLAAgXQAAgGgFgHIgCgEQgDgEABgCQAAAAAFgKQAGgKAAgKQAAgPgNgMIgMgJIgBgBIAAAAIABgDQAGADAIAFQATAOAAATQAAALgKASIAGAKQAFAGAAAOQAAAUgWAOQgaAQgzAFIgIAAIgKACg");
	this.shape_1.setTransform(17.5,24);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5B371E").s().p("AhSBXIgMgFIgFgBIgBgBQgHgEgGgIQgMgOAAgMIAAgEQACgHAEgIIAGgJIgBgCIgCgDQgIgOAAgPQAAgJAGgLQAJgOACgHIAAgBIADgHQAGgNAJgDQAHgCACAFQAGABASgCIAQABIAPABIA/AAIAOACIAzAAQAJACACAGIABAIIgBAKIgBADIAAAAIABABIAMAJIgJgBQgPAAgJAIQgEgJgJgEQgRgIAAASQAAAEAEAGQAEAFAAAEQAAAFgIAKQgJAHAAANQAAAJAFALIAFAMQAAAOgRAKQgUAMgjABg");
	this.shape_2.setTransform(16.7,22.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E5C45A").s().p("AgDAwIgLAAQgZAAgMgaIgBgBIAAgBIgDgGIgDgJIACABQARAJAYAAQAdAAAQgWQALgQAAgOIgCgKIAEACIADACIABABIAAAAIAFAHQAGAKAAAPIgFAdQgOAdgjAAIgHAAg");
	this.shape_3.setTransform(11.4,25.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgeA3QgRgFgIgPQgKgPAAgSIABgPQABgHAHgLQADgFAFgEQALgPARgDIAkAAIANAEIAEAAQAPAGAIANQAFAFABAFQADANABALQAAASgJANIgDAFQgEAHgHAEIgFADQgSAJgZAAQgOABgLgEgAACA0QAjAAANgdIAGgdQAAgPgHgLIgFgGIAAgBIgBAAIgCgDQgFgDgFgCQgLgEgOAAQgGAAgFABIgPAAQgXALgHAJIgEAHIgFAJQgCAEgBAGIAAACIACALIADAJIACAGIAAABIABABQAMAaAaAAIALAAIAHAAg");
	this.shape_4.setTransform(11.7,25.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFDA64").s().p("AgvAbIgCgBIgCgNIAAgCQABgGACgEIAFgHIAEgHQAHgKAXgKIANAAQAGgBAHAAQAOAAALAEQAFACAEADIgDgCIACAKQgBAOgKAOQgRAYgdAAQgXAAgRgIg");
	this.shape_5.setTransform(10.6,23.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},10).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]},2).to({state:[]},2).wait(2));

	// Layer 8
	this.instance_2 = new lib.Thalf();
	this.instance_2.setTransform(12.9,39.2,1,1,0,0,0,19.2,12.2);

	this.instance_3 = new lib.BHalf();
	this.instance_3.setTransform(3.8,50.3,1,1,0,0,0,11.9,8.8);

	this.instance_4 = new lib.Thalf2();
	this.instance_4.setTransform(12.9,39.2,1,1,0,0,0,19.2,12.2);

	this.instance_5 = new lib.BHalf2();
	this.instance_5.setTransform(3.8,50.3,1,1,0,0,0,11.9,8.8);

	this.instance_6 = new lib.Thalfcopy();
	this.instance_6.setTransform(12.9,39.2,1,1,0,0,0,19.2,12.2);

	this.instance_7 = new lib.BHalf2copy();
	this.instance_7.setTransform(3.8,50.3,1,1,0,0,0,11.9,8.8);

	this.instance_8 = new lib.Thalfcopy2();
	this.instance_8.setTransform(12.9,39.2,1,1,0,0,0,19.2,12.2);

	this.instance_9 = new lib.BHalf2copy2();
	this.instance_9.setTransform(3.8,50.3,1,1,0,0,0,11.9,8.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2}]}).to({state:[{t:this.instance_5},{t:this.instance_4}]},1).to({state:[{t:this.instance_3},{t:this.instance_2}]},1).to({state:[{t:this.instance_5},{t:this.instance_4}]},1).to({state:[{t:this.instance_3},{t:this.instance_2}]},1).to({state:[{t:this.instance_5},{t:this.instance_4}]},1).to({state:[{t:this.instance_3},{t:this.instance_2}]},1).to({state:[{t:this.instance_5},{t:this.instance_4}]},1).to({state:[{t:this.instance_7},{t:this.instance_6}]},1).to({state:[{t:this.instance_9},{t:this.instance_8}]},1).to({state:[{t:this.instance_7},{t:this.instance_6}]},1).to({state:[{t:this.instance_9},{t:this.instance_8}]},1).to({state:[{t:this.instance_7},{t:this.instance_6}]},1).to({state:[{t:this.instance_9},{t:this.instance_8}]},1).to({state:[{t:this.instance_7},{t:this.instance_6}]},1).to({state:[{t:this.instance_9},{t:this.instance_8}]},1).wait(1));

	// Layer 6
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3A1901").s().p("AAAAAIAAAAIAAgBIAAADIAAgCg");
	this.shape_6.setTransform(28.2,19);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3A1A00").s().p("AhxBFQgEgMgBgIIAAgDQgDgKAAgTQAAgKACgGQADgpACgHQAEgMABgRIAAgJIAIgCQAGgBARACIBHAFIA7AAQAzAOAMARQADAEABAKQADAFAAAGIgBAZIAAABQgFgCgGAAQgLABgKAAQgFAAgDgDQgCgDgFAAQgNAAgCAOQgCAIABANQgBAJgJAWIgKAAIgCgBIgdAAIgwAFQgGABgFADQgGADgDABQgFABgTAIIgRAHQgGgGgFgNg");
	this.shape_7.setTransform(16.1,18.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhtBfQgFgCgGgQQgFgOgCgNQgCgOgBgjQAAgVAEgeQACgWALgRQAIgOACALIgIACIAAAKQgBARgDALQgDAHgDAsQgCADABALQgBASADAKIAAAEQABAIAEALQAFANAGAHIASgIQASgIAFgBQAEAAAFgEQAFgDAHAAIAvgGIAdAAIADABIAJABQAKgXABgJQgCgNACgIQADgNANAAQAEAAACADQADADAFAAQAKABAMgCQAFAAAFAEIAAAAIABABIAAAAIACAEIAAgDQACgJAGgCIAAAVQgGAigUAUIgFAAQgDADgFAAIgQgDIhWAAQgfAAgNAFQgIAFgJACQgCABgQAKQgOAJgBAAQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAAAgBg");
	this.shape_8.setTransform(16.3,19.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E5C45A").s().p("Ag4AmIgDAAIgMgDIgMgDIAAAAIAAgCQAAgNAWgCQAhgDASgLQAMgHAHgKIAEgKQAFgJAIAAQAEAAAHAGQACAAANgHQAKgEAOAGIAIAEQgCAUgMANQgVAagtAJg");
	this.shape_9.setTransform(20.8,21.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgoA2IgpgIQgQgCgLgDIgCgKIABAAIAEACIARAEQAEAAApAJIAAAAIALADIANADIADABIA8AAQAtgKAUgZQANgQABgSIAAgGQAAgMgEgIQAEADADAHQADAJAAAIQAAAWgPATQgVAcgtAGIgbAAQgIACgNAAQgGAAgigIgAh+gdQAAgGgCgDIAAgLQAEgJAFgDQgCAEgCAGIgBAGIAAAcIAAACIgCgOg");
	this.shape_10.setTransform(16.9,20.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFDA64").s().p("AhXAxIgRgEIgEgBIgBgBIgFgQIgHgVIgCgJIAAgCIAAgcIABgFQACgHACgDQAGgIANAPIAFgBQAIAAAFALQAZgLA8gQIAjAAIAAABQA3gBATASQAFAEACAHQAEAIAAAMIAAAGIgIgEQgOgGgJAEQgNAHgDAAQgGgGgEAAQgJAAgEAIIgEAJQgIAKgOAJQgSALgeADQgWACAAANIAAACQgpgKgEAAg");
	this.shape_11.setTransform(16.8,19.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#481516").s().p("AAFArIgRgBIgcgBIgCgBIgBAAIAGgLQANgYAEgLQABgEAAgMQAAgJADgFQAQgWAEAoIAAADQAFgGAOgBQALgBAHACIAAADIAAAGIADAEQABAEgBAEIgEALIgFANIAAABQgDAJgHAIQABADgKAAIgLgCg");
	this.shape_12.setTransform(24.1,20.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#6B2221").s().p("AhxBFQgEgMgBgIIAAgDQgDgKAAgTQAAgKACgGQADgpACgHQAEgMABgRIAAgJIAIgCQAGgBARACIBHAFIA7AAQAzAOAMARQADAEABAKQADAFAAAGIgBAXQgHgCgLABQgOABgFAEIAAgCQgEgogSAWQgDAFAAAKQAAAKgBADQgEAOgNAXIgHAMIgbAAIgwAFQgGABgFADQgGADgDABQgFABgTAIIgRAHQgGgGgFgNg");
	this.shape_13.setTransform(16.1,18.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AhtBfQgFgCgGgQQgFgOgCgNQgCgOgBgjQAAgVAEgeQACgWALgRQAIgOACALIgIACIAAAKQgBARgDALQgDAHgDAsQgCADABALQgBASADAKIAAAEQABAIAEALQAFANAGAHIASgIQASgIAFgBQAEAAAFgEQAFgDAHAAIAvgGIAcAAIABAAIADABIAbACIATAAIALACQAKAAgBgDQAHgIADgJIAAAAIAGgOIADgNQABgEgBgEIAAgDQACgJAGgCIAAAVQgGAigUAUIgFAAQgDADgFAAIgQgDIhWAAQgfAAgNAFQgIAFgJACQgCABgQAKQgOAJgBAAQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAAAgBg");
	this.shape_14.setTransform(16.3,19.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},8).to({state:[]},2).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9}]},2).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12}]},2).wait(2));

	// Layer 6
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#EACFB7").s().p("AgEA2IAAAAIgRgmQgJgWgBgPIgEgXQAHAAAIgGQAJgFAIAAIABAAQgKAAgMAQQADADACAAQACAAAEgFQADgEACAAQAHAAAEAKQAEAJAAAIQAAADgDAIQgDAIgDAAQgCAAgEgBIgEgBQgBAAgBAAQAAAAgBAAQAAAAgBABQAAAAgBAAIAAACIADAGQACAEAIAAQAKAAAHgMQADgJAAgJQAAgTgJgMQgDgEgEgBQAQAFAEAlQAFAsAKAMQgSAFgTAHIgCgCg");
	this.shape_15.setTransform(8,23.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#CCA076").s().p("AgiBHQgEglgQgGIgDgBIgBAAIgBAAQgIAAgJAGQgIAFgHAAIgDgRQgEgYgBgPQAAggAbgfQAigoA7gGIAuAAQANAIAIATQAMAZAAAjQAAATgFAaQgFAdgGAUQgFAQgNAaIgJAVQgigBgqAMQgKgLgFgug");
	this.shape_16.setTransform(13.6,15.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#A98462").s().p("AgzB3IAJgVQANgaAFgQQAGgUAGgdQAEgaAAgTQAAgjgLgZQgJgTgMgIIAGAAQA1ATAUAnQANAZgBAqIAAAfQgDAQgHANIgOAcQgLAWgOAPIgBABQgdgHgXAAg");
	this.shape_17.setTransform(24.5,15.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AhkCNQAAgGgOgfQgNgggBgOIgHgoQgFgZAAgOQAAgnAcgfQAkgoBHgKIAyAAQBgAcAABsQAAAogOAcIgPAhQgJASgLAJIABABIgKgDQAOgQALgWIANgcQAIgNACgQIABgeQAAgrgNgZQgTgng3gTIgHAAIgrAAQg9AGgiAoQgbAfAAAgQAAAPAFAYIADARIADAXQABAPAJAYIARAnIAAAAIgLAGgAhjBZIgDgGIABgDQAAgBAAAAQABAAAAAAQABAAABgBQAAAAABAAIAFADQADACACAAQAFAAADgLQADgIAAgDQAAgIgEgIQgFgLgHAAQgCAAgEAFQgEAEgBAAQgDAAgDgDQANgPAJgBIABAAIAEABQADACADAEQAJAMAAASQAAAKgDAIQgGAOgNAAQgHAAgDgEg");
	this.shape_18.setTransform(16.4,15.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#725942").s().p("AgzB3IAJgVQANgaAFgQQAGgUAGgdQAEgaAAgTQAAgjgLgZQgJgTgMgIIAGAAQA1ATAUAnQANAZgBAqIAAAfQgDAQgHANIgOAcQgLAWgOAPIgBABQgdgHgXAAg");
	this.shape_19.setTransform(24.5,15.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#917054").s().p("AgiBHQgEglgQgGIgDgBIgBAAIgBAAQgIAAgJAGQgIAFgHAAIgDgRQgEgYgBgPQAAggAbgfQAigoA7gGIAuAAQANAIAIATQAMAZAAAjQAAATgFAaQgFAdgGAUQgFAQgNAaIgJAVQgigBgqAMQgKgLgFgug");
	this.shape_20.setTransform(13.6,15.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#D5AE81").s().p("AgEA2IAAAAIgRgmQgJgWgBgPIgEgXQAHAAAIgGQAJgFAIAAIABAAQgKAAgMAQQADADACAAQACAAAEgFQADgEACAAQAHAAAEAKQAEAJAAAIQAAADgDAIQgDAIgDAAQgCAAgEgBIgEgBQgBAAgBAAQAAAAgBAAQAAAAgBABQAAAAgBAAIAAACIADAGQACAEAIAAQAKAAAHgMQADgJAAgJQAAgTgJgMQgDgEgEgBQAQAFAEAlQAFAsAKAMQgSAFgTAHIgCgCg");
	this.shape_21.setTransform(8,23.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#D3D37E").s().p("AgzB3IAJgVQANgaAFgQQAGgUAGgdQAEgaAAgTQAAgjgLgZQgJgTgMgIIAGAAQA1ATAUAnQANAZgBAqIAAAfQgDAQgHANIgOAcQgLAWgOAPIgBABQgdgHgXAAg");
	this.shape_22.setTransform(24.5,15.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFF99").s().p("AgiBHQgEglgQgGIgDgBIgBAAIgBAAQgIAAgJAGQgIAFgHAAIgDgRQgEgYgBgPQAAggAbgfQAigoA7gGIAuAAQANAIAIATQAMAZAAAjQAAATgFAaQgFAdgGAUQgFAQgNAaIgJAVQgigBgqAMQgKgLgFgug");
	this.shape_23.setTransform(13.6,15.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#5B371E").s().p("AgzB3IAJgVQANgaAFgQQAGgUAGgdQAEgaAAgTQAAgjgLgZQgJgTgMgIIAGAAQA1ATAUAnQANAZgBAqIAAAfQgDAQgHANIgOAcQgLAWgOAPIgBABQgdgHgXAAg");
	this.shape_24.setTransform(24.5,15.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#804E2A").s().p("AgiBHQgEglgQgGIgDgBIgBAAIgBAAQgIAAgJAGQgIAFgHAAIgDgRQgEgYgBgPQAAggAbgfQAigoA7gGIAuAAQANAIAIATQAMAZAAAjQAAATgFAaQgFAdgGAUQgFAQgNAaIgJAVQgigBgqAMQgKgLgFgug");
	this.shape_25.setTransform(13.6,15.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#975F3C").s().p("AgEA2IAAAAIgRgmQgJgWgBgPIgEgXQAHAAAIgGQAJgFAIAAIABAAQgKAAgMAQQADADACAAQACAAAEgFQADgEACAAQAHAAAEAKQAEAJAAAIQAAADgDAIQgDAIgDAAQgCAAgEgBIgEgBQgBAAgBAAQAAAAgBAAQAAAAgBABQAAAAgBAAIAAACIADAGQACAEAIAAQAKAAAHgMQADgJAAgJQAAgTgJgMQgDgEgEgBQAQAFAEAlQAFAsAKAMQgSAFgTAHIgCgCg");
	this.shape_26.setTransform(8,23.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#774736").s().p("AgCABIACgCIADgBIAAACQAAADgDAAQAAAAAAAAQAAAAAAgBQgBAAAAAAQgBgBAAAAg");
	this.shape_27.setTransform(7.3,13.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#EACFB7").s().p("AhAAbIACgBIgBgBQgOgQgGgJQAEgJAGgFQAKgGAZgFIBHAAIAEgCIAdAAIAAABQAOACADAFIABACIgHAFQgIAEgBADQgEAJgFAGIACABIgHAEIgQAAIgEgCIgoABQgEAAgQAIQgPAGgRAAg");
	this.shape_28.setTransform(17.1,26.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("Ag/CNIghgqQgUgagBgPIgGgoQgFgZAAgOQAAgnAbgfQAhgoBCgKIAvAAQA0ARAUAsQAMAcAAAvQAAAogOAcQgGAOgMAQIgVAeIABABIgGgCIgDgBQAFgGAFgLQABgDAHgEIAIgGIAEgIIAMgcQAIgNABgQIABgeQAAgugJgWQgQgngzgTIgGAAIgoAAQg5AGggAoQgZAfAAAgQAAAPAFAYIADARIADAXQABAPAIAYQABACAKAEQAKAEABACQAHALAOAQIAAAAIgCACIgIAEgAhaBZIgCgGIABgDQAAgBAAAAQAAAAABAAQAAgBABAAQABAAABAAQAAAAABAAQAAABABAAQAAAAABABQAAAAABABQADACACAAQAEAAAEgLIACgLQAAgIgEgIQgFgLgGAAQgCAAgDAFQgEAEgBAAQgDAAgCgDQALgQAJAAIABAAIADABQAEACACAEQAJAMAAASQAAAKgEAIQgFAOgMAAQgHAAgDgEg");
	this.shape_29.setTransform(16.1,15.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15}]}).to({state:[{t:this.shape_18},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19}]},2).to({state:[{t:this.shape_18},{t:this.shape_15},{t:this.shape_23},{t:this.shape_22}]},2).to({state:[{t:this.shape_18},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24}]},2).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27}]},2).to({state:[]},2).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27}]},2).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.1,0.3,40.4,58.8);


(lib.Forklift_MC_back = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{down:0,up_start:1,up:7,down_start:8,down_end:14});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1C1C1C").s().p("AgDADIgGgGQgDgEAAgDQAAAAAAAAQAAgBAAAAQAAgBABAAQAAAAABAAQAEAFAKAIIAJAJIAAADQAAAAAAAAQAAAAAAAAQAAAAgBAAQAAAAgBAAg");
	this.shape.setTransform(88.4,53.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFE05B").s().p("AAwCPQgUgIhMgwIgVgOIgWgQQgWgSgMgQQgNgRgJgVQgGgRgFgUIAAgBQAKANAUARIADACIArAgIAcAVQAlAeBSApIAxAWQAsATASAAIAEAAQAGAIAFAFIACABIAIAFIghABQhCAAg2gVgAjIheQABgxAigUIAAAAQgIA0AAAHIAAAUIADAJQACAJAJAMIgEAEQgMgUgZgYg");
	this.shape_1.setTransform(95.7,67.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FAD34D").s().p("AAAD/QACgKABgMQABgagCgOQgCgWgUggQgjgzg6gfQghgRggAMQgMAHgIAKQgBgEgHASIgEALQgbgihHglIgPgIQgTgJgRgGIgCAAIABgCQAJgVAAgZIAAgBQAAgggKgsIgRg7IACgDQAJAYANARQAMAQAWARIAWARIAVANQBOAwAUAIQBCAbBWgGIAHAEIAEgCIAPgGIABgBQAQgEAZgIIAigNQAegMAdgTIA8goIAFgDQA8glAlgjIAXgaIArgvQAEAEAMAIIABABQgXBUh4BPQh2BNhsABIgZgBQAQAXAPAnQAXA4AAAtQAAAqgCAFIgSAwIgGAAg");
	this.shape_2.setTransform(118.6,85.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000066").s().p("AgGAAIACgBIALADg");
	this.shape_3.setTransform(117.4,113.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#757575").s().p("Ah9BCQhQg0ggAAIgMAAQAGgIAFgGQBahfBYg4QAbgRAVgKQA1AeBcA+QBbA9AaArIgJAFQhKAwgkAUQhAAigTAMQgTAPgKAbQhCg8hOg1g");
	this.shape_4.setTransform(115.2,9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#D6B13D").s().p("ABpDjQABgEAAgrQAAgsgWg5QgQgngSgYIAbABQBtgBB2hMQB3hPAXhUIAWAQQAbAeALAdQANAfAAApQAABMgDALQgOA/g+AnIiZBXQgrAeghARQguAVhBAGIgOABIATgwgAlbgRQgBhHgmg/QgSgdg7g2IgLgIQgXgOgUgFIgBAAIAKgMIAsgBIAFABIAOAMQAgAZAPAQQBGBIAGByQABAUgbAUQABgKAAgNg");
	this.shape_5.setTransform(111.9,83.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#595959").s().p("ADRCmQgLgPgEgVQADgMAIgDQASADAQAPQAQAPAAAOIAAATQgCADgKAAQgUAAgOgSgAjPhJQgYgJgNggQgKgYAAgYQgBgVANAAQAMgBASAPQARAOAMATQANAUAAAOQAAAPgGAPQgHAEgIAAQgIAAgIgFg");
	this.shape_6.setTransform(85.3,87.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AhdImQgSgkgGgaIAAAAQgPAAghgkIgpgsQgWgQg8gWIgHgCQgXASglADIgBAAQgqAWgqgWQhXgzggg7QgWgsgBhPIAAgOQADhDAjgUIAHgEQAJgEAKgBQAJgXAxgIQAVgEAOABQAHAAAKADIALADQAYAKAWAVIARAQQAZAYAMAUQAGAJACAJIAGATIARA7QAKArAAAgIAAACQAAAZgJAVIgBADIACABQARAGATAJIAPAHQBHAlAbAjIAEgMQAHgSABAEQAIgJAMgHQAggNAhASQA4AeAjA0QAVAfADAWQACAPgBAaQgBAMgCAKIANABIAGgBIANgBQBCgGAtgVQAigRArgeICYhXQA/gnAOg/QACgNAAhMQAAgpgMgfQgMgdgageIgWgQIgBgBQgMgIgEgEIgrAwIgXAaQglAjg8AkIgFAEIg8AoQgdATgeAMIgiAMQgZAJgQAEIgDAAIgPAHIgEACIgHgEIgHgGIgCgBQgGgFgFgIIgEAAQgSAAgqgSIgxgXQhUgpglgeIgcgVIgrgiIgDgCQgUgQgLgOIAAAAQgJgMgCgKIgDgJIAAgUQAAgGAIgzIAHgwIAAgDIAGgoIAPhpIALg+QAIgqADgXQAEgfAHgfQADgRAKgMQALgnAUgbQA4hMCthCQgCgCASABQASABAAAFQAvAHBxBMQBxBNANATIAOAVIABACQACAAADAFQADAFAAAGQACAFABAIQACAJgCAAIgCADIgBgBIANAjQAEATAEAjQAMBBAKAqIAAADQADAIAPBiQAPBgAAAIIACAEQAKgFAVALQAUALATAZQAOASAVAmQAKARAGAcQAGAbAAAbQAABVgIAfQgTBDhEAmQi0Brg0AaQhSAphIgFIAAAAIgCADIgKgEIgNgFIgCABQgSAKgdAAQhPAAgqhNgAhQHQQAKAwAYAgQAfApAqAAQAOAAAFgDIAJgFIAAgpQAAgggkghQgighgpgHQgQAGgIAbgAoEBnQgdABABAuQABA2AWA0QAdBIA2ATQAiAWAigTQAOghAAggQAAgggegtQgbgqgmgfQgngggZAAIgBAAgAnhAlIgJAMIABAAQAUAFAWAOIAMAIQA6A2ASAdQAmA/ACBHQAAANgCAMQAcgWgCgUQgGhyhFhIQgQgQgfgZIgOgMIgFgBgABoiUIgCApIgHEKQgFAXgDAbQgEAXgBAVIgBAdQAEAAAdgOIAngRQAkgMAqgWQAHgDAwgkIBSg+QAEgDApgsIAKgIIgCgDIABgFQgIhhgEgMQgGgTgQhMQgRhMAAgKQAAgGgFgfQgOhIgBggIgDAAIgGABIAAgBIgCACQgPADgRAKQgIAFgPALIgnAcQgoAagNALIgKAHQgXAOgIAHQgVAMgIAKIgLAOQgGAIAAACIABABIgBAEQAAABAAAAQAAABAAAAQAAABAAAAQAAAAAAAAIgBALIAAgBgAjVAeQAFANAGAEQgnAXgWALIAMAKQAeAZA9A3QAUANBvAzQA0AYAWAOIAAgJIABggIABgEQACgiAGgmQAFgYABgNIAKlFIgggdQgfgeheg5Qhhg5gFgFIgJgJQgEAegPBRIgWB+IgEAiIgDAnIgJBTQgDAYgEATIATgNQAJgCAQgJgAhBlpQBNA1BEA8QAJgbAUgPQASgMBBgiQAkgUBJgwIAJgFQgZgthcg9Qhbg+g3geQgVAKgaARQhYA4hZBhQgGAGgFAIIAMAAIAAAAQAhAABOA0gAFvliIAAACIAAgCIAAAAgAFnl4IAAgCIgBAAIABACgACokEIgBABIgBAAIACgBg");
	this.shape_7.setTransform(109.1,51.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#333333").s().p("ADFDIQgYghgKgvQAIgcAQgFQApAHAkAhQAkAhAAAfIAAAqIgJAEQgFADgOAAQgsAAgfgogADJCQQAEAVALAPQAOASAUAAQAKAAACgDIAAgTQAAgOgQgPQgQgPgSgDQgIADgDAMgAjBADQg2gSgdhHQgWg1gBg1QgBguAdgBQAagBAnAgQAmAfAbAqQAeAtAAAgQAAAggOAfQgQAJgQAAQgSAAgSgLgAjripQgNAAABAVQAAAYAKAYQANAgAYAJQAQAJAPgIQAGgPAAgPQAAgOgNgUQgMgTgRgOQgSgOgLAAIgBAAg");
	this.shape_8.setTransform(84.6,86.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(15));

	// Layer 5
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#1C1C1C").s().p("AhrCSQgJgGgDgIQgCgEAAgNQABgVAHgyIAKg7QAQhLAKgSIAEgFQAUgTATgKQASgGAUAAIARABQASgFAWAUQAJAIAJAMQAJAMACAHIAOAoQAGAQAAASIAAAIIAAAAIACALQAHgGACANQACAKgBAQQAAAlgOAWQgWAig5APIgTAAIgBgBQgMAEgQACIhLADQgIAAgFgDgABZBdIgDAEQAOgJACgNIAAAAQgGAKgHAIg");
	this.shape_9.setTransform(121.1,50.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AhaCiQgdAAgKgUIgEgVQAAgeALhBIAFggIABgIIAGgcIAFghQAFgXALgQIANgUQAHgLARgGQAdgLAigBQAegCAGAHQAaAIASAZQAiAsABA/QAIAZAAAKQAAASgDAPQADAHAAADQAAAsgNAOQgbAchjAOQgcAFgeAAQgPAAgMgDgAgfiQQgTAKgVAUIgDAFQgLASgPBLIgKA6QgHAzgBAUQAAANABAEQAEAJAIAGQAGACAIAAIBKgCQAQgDANgEIABABIATAAQA5gPAWgiQAOgWgBglQACgPgCgKQgDgOgGAIIgCgMIAAgBIAAgHQAAgSgHgRIgNgnQgDgIgJgMQgIgLgJgIQgXgVgRAGIgRgBQgVAAgRAFgABxBoIAAAAIACgEIgCAEgABbBcQAIgJAGgKIAAABQgDAMgOAJIADgDg");
	this.shape_10.setTransform(120.8,50.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9}]}).wait(15));

	// Layer 9
	this.avatar_mc = new lib.ForkLiftDriver01back();
	this.avatar_mc.setTransform(113,42.9,1,1,0,0,0,16.9,30.3);

	this.timeline.addTween(cjs.Tween.get(this.avatar_mc).wait(15));

	// Layer 7
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#1C1C1C").s().p("AgSAuQgIgPAAgfQAAgdACgFQACgFAXgmIArABQAhAlACAEQACAEAAAeIgFAnQgKAogaAAQgrAAgPgggAhAA4QgJgIgCgEQgFgHAAgSQAAgRACgCQAFgMAWgBIADACIAAA1IAFAKQAEAIAAACQAAABAAAAQAAABAAAAQAAAAgBAAQAAAAAAAAIgFABQgIAAgLgJgAhJAnQAEAHAXANIgEgIIgLgEQgJgDgEgIIABADg");
	this.shape_11.setTransform(94.5,48.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAQCwQgsgMgbgNIgOgHIgIgFIgKgKIgFgFQgIgMgCgOIAAgGQAAgVAKgLQANgQAjgKIA/AAQAGgBAEAEIgyAAQgqANgOAKQgOAKAAATIABAKQACAIAFAHIAHAJIALAIIAFADIAHAEIAAAAQAcAMA+APIBoAAQArgIAVgOQAUgOAAgQQAAgXgegPQAFgFAJAAQASAAAFARQADAJgBAKQAAAYgTASQgYAZgyADgAibAgIgGABQgmAAgUgeQgOgTAAgVQAAgZADgHQAJgRAhgFQAFgmASgWQAXgcAiAEQAhAEAXAeQAaAgAAAvQAABuhHAAQgkAAgWgQgAihhgQgCAFAAAfQAAAfAIAQQAPAdAtAAQAaAAAKgmIAFgoQAAgegCgFQgCgEghgkIgrgBQgZAmgCAEgAjXg9QgCAEAAARQAAARAFAHQACAFAJAHQALAHAIAAIAFgBQAAABABAAQAAAAAAgBQAAAAAAAAQAAgBAAAAQAAgBgEgHIgFgLIAAg2IgDgDQgWACgFAMgAjSgVIgBgCQAEAIAJADIALAEIAEAIQgXgOgEgHg");
	this.shape_12.setTransform(108.2,54.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#333333").s().p("AgbA/QhBgOgcgMIAAgBIgGgDIgGgDIgLgJIgHgJQgEgGgCgGIgBgKQAAgTAOgLQAOgJAqgNIA0AAIAjAAQBbAWAaAKIALAFQAeAQAAAUQAAARgTAOQgWAOgqAHg");
	this.shape_13.setTransform(114.9,64.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11}]}).wait(15));

	// Layer 10
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFE05B").s().p("AioBoQgDgGAAgPQgTACgIgOQgEgHgBgGIACABIAEgCQAjgQAhgaIAIgGIAPgLIAKgJIABgCIAAgEIACgBQAHgCAGAAIAAgDQAAgMAggTIgBALQAkgWAtgTQBaglBGADQAJAEADAJIgfAXQgbAUgHAIIgEAHIgDAEIAAABQgMAThJAwIgJAFQgUAOgfATIgJAGQgYAQgNADQAAACAFAFIAPgGQgXANgPAKQgPgCgHgNIgCgQQAAgHAHgPQgEACgDAAIgFAAQgCALgEANQgKAigTAAQgVAAgGgPgAgGBHIAAAAIgFACIAFgCg");
	this.shape_14.setTransform(89.5,47.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0000FF").s().p("AgYAKIAAgFIABgiQAwAdAAAQQAAAGgGAEIAAABQgFADgGAAIgggUg");
	this.shape_15.setTransform(122,40);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#545454").s().p("Ag+AyIgQgJIAAhxQAQgDAVgDIBogMIAMgCIAEC4IgJABQg1AAhPgrg");
	this.shape_16.setTransform(110.7,72.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#494949").s().p("Ag8hQIAuAaQABAEAWAQQAdATAWABIABAGQgBASheA6IgVANg");
	this.shape_17.setTransform(136.5,68.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#D6B13D").s().p("AiRBuIgTgTQAUgFAzgbQAfgRARgNIAEACIAMgGQAQgKAVgPQAtghAOgRQAHgIASgnIAFgLQAFgJAGgIQAHgIAIgGIACgBIADAAQASgEATACIAAAGQgigHgSAbQgIAGgGAKIgBACQgLAPgDASIgGAlIAAAWIgfATIgUAMQgJAHgEANIgBACQgTAVgHAAQgFAAgDgEIAAABIgJAsQgRAkgugWQgCAVgQAAIgigig");
	this.shape_18.setTransform(98.9,54.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#333333").s().p("AAvhNQA9gKAwgPIAIgDIADACIAFChQhNAsgtAEgAirgFQAAgcAcgOQANgGAYgFIAAByQhBgmAAgXg");
	this.shape_19.setTransform(113.6,70.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AnjCAQgFgIAAgHQAAgNANgOQAFgHAIgHQAUgRAdgIQAagIATAEIAIgDQAagKAegXQASgKAcgWIAigYIgGAkIAAAEIAAACIgLAHIgPANIgIAGQghAagjAQIgEACIgCgBIgGgCQAGAHAAAKQAAAKgGACQgDAJgJACIgVACIgRAEQgFABgYASQgVAQgPAAQgQAAgIgNgAFfAuIgkgfIgDgCIgQgJIgngVIgwgdIAAAAQAGgEAAgGQAAgSgygdIgBAkIgKgCIgOgDIgQgDQgTgCgTAEIgCAAIgCAAIgNAHIgHAFIgYAXQgRAPgMAJIAAAAIgNAOIgiAfIgXAUIgOAKQgDgCgFgBQgRgDgpAVIAAgBIgEADIgGACIgXANIgOAGQgGgFAAgCQANgCAYgRIAJgGQAhgSAUgPIAJgEQBJgxAMgTIABAAIAAgFIAEgGQAIgJAagUIAfgXIAQgKQgNgCgPgBQhEgDhaAlQgvATgkAWIABgLIADgbIAYgNQAzgXBpgaIAAgCIBeAAIAlATIAAACIARACIAJACQAKACAIADQAEAAAFADIAGADQAfARCVBpQALAHBCAjQAqAWAAAMQAAAFgCADIgCADQgFAFgIgBQgsABhNg/g");
	this.shape_20.setTransform(103,47.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FAD34D").s().p("AEMCHQgWgBgdgTQgYgQgBgEIgugaIgDgCIhOgsQgCgegJgfIgMgqQgJgagRgPIgDgCIgGgDIgIgCIAAgGIAPADIANADIAKACIAAAFIAiAUQAGAAAFgEIAxAdIAmAXIAQAJIADACIAkAfQBNA9AsAAQAJAAAFgFIABgCIABAFQAAAOgKAGQgWAMgPAPQABAKgJANQgNARgVAAIgEAAgAlmBcQgCgEAAgGIAAgBIABgCQADgIASgOQAOgKAXgNIAXgMIAGgDIAEgCQApgSASABQAEAAADADIAOgJIAYgUIAhgfIAPgQIAAAAQAMgIARgQIAYgXIAIgFIAMgGQgIAGgHAIQgGAIgFAJIgFALQgSAngHAIQgOARgtAfQgXARgQAKIgMAGIgEgCQgRANgfARQgzAbgUAFIgJABQgMAAgFgIg");
	this.shape_21.setTransform(115.7,53.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14}]}).wait(15));

	// Layer 3
	this.instance = new lib.column();
	this.instance.setTransform(58.9,29.6,1,1,0,0,180,14.2,49.4);

	this.instance_1 = new lib.column();
	this.instance_1.setTransform(76.7,17.3,1,1,0,0,180,14.2,49.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(15));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.3,-32.1,176.9,146.9);


(lib.FemaleYard1 = function() {
	this.initialize();

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#421F10").s().p("AgSAvQgCgDgDgCIgBgEQgGgfAOgdQAHgPAJgLIAaAWIAAAAIgJAkIgEAUQgCAIgIAEIgDAEIAAABQgCACgGAAQgIAAgCgCg");
	this.shape.setTransform(109.3,155.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#65301B").s().p("ABFAsIgBAAQgEgBgCgOQgBgTgCgDQgEgIgLgHIgRADQgfAFgNAIQgXAEgJALIgbgWQAHgGAJgGQACgHAVgMIAbgNIAkAAIAVAIIAAABQAPABAIAZQAFAOABAUIAAAFIABADQAAAJgHABIgBAAg");
	this.shape_1.setTransform(117,150.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhMBSIgEAAQgVAAgFgYIAAgbQAAg+A5gqQAMgIAKgDQALgKAqAEQA2AFAEASIACABQAKAFACAOIAGAPQADASAAAOIAAAMQgBAKgFAAIgCAAIgCABQgFAAgBgJQAGgBAAgKIAAgDIAAgDQgBgTgFgRQgIgZgPgBIAAAAIgWgJIgmAAIgZAOQgUALgDAHQgJAGgGAIQgLAMgGAOQgOAeAGAfIAAADQAEADABADQADACAHAAQAGAAAEgCIABgCIADgEQAHgDACgJIAFgTIAIgkIABAAQAJgLAVgFQANgIAggGIARgDQAMAHAEAKQABACACATQABAMAEACIAAABQAAABAAAAQAAABAAAAQgBABAAABQAAAAgBABQAAAAgBAAQAAABAAAAQgBAAAAAAQAAABgBAAQgIAAAAgLIABgNQAAgFgEgJIgBgDQgTAGgRACQgEAAgVALIgEACQgHAKgJANQgCADgIAhQgMAfgVAAQgQAAgHgKg");
	this.shape_2.setTransform(114.9,153.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#421F10").s().p("AgFA4IgEgBIgEAAIgBABIgDgDQgFgIgDgIQgDgHAAgEQgBgOAIgUIAEgKQAJgVALgQIAUAWIAGAHQgJAMgDAOQgDALgBAUIgBARIgDAEIgGADIgEABIgEAAg");
	this.shape_3.setTransform(66.6,157.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#65301B").s().p("ABaAgIgDgHIgCgEIgFgJQgDgDgJgDQgMgDgNgBIgGgCIgBAAIgLAAIgFAAIgUgBIgMAAIgDABQgoAAgPAOIgIAJIgGgHIgUgUIAFgFQAOgPARgHQACgDAEgCIAYgIIAmAAQAdAEAmAZIADACIAAAAQAjAXAAASIAAAFIAAADQgBAJgFAAQgEAAgFgNg");
	this.shape_4.setTransform(77.5,152.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AhLBeQgEABgIAAIgVAAIgEAAQgVAAgEgXQAahhAFgGQAbgiApgTQANgHAOgBIAVgBQAGgCAbAFQAgAHAMAMIAbAYQAIAGAGALQAEAIAAADIAAAFQACACAAACIAAACQADAIAAAIQACAagJgBIgCgBIgDAAQgEAAgBgDIgBgEIABgDIgBAAIgBAAQgCADgDgBQgFABgGgbIgEAAQgRAAgWgGIgRAAQg5AAgRAyIgMAkQgHARgOAAQgGAAgEgBgAhdBOIAEAAIAEAAIAGAAIAGgDIAEgEIAAgRQABgUADgLQADgQAKgKIAIgJQAOgOAngCIACgBIAMAAIAXABIAEAAIAMACIAAAAIAGACQANABAMADQAKADACADIAFAJIACADIADAGQAGANADAAQAGAAAAgJIAAgDIAAgFQAAgQgjgZIAAAAIgDgCQglgZgegEIgmAAIgYAIQgEACgBADQgSAHgOAPIgFAFQgNAQgJATIgEAKQgHAVAAAPQAAAEADAHQADAIAGAIIACACIACgBIADABg");
	this.shape_5.setTransform(75,155.6);

	// Layer 8
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CCB59F").s().p("AAjBjQgEAAgMgJIgQgKQgMgKgCgIIgCgDQgFgIgBgHIgFgXIAAgdQgCgEgBgLIgDgSIgCgHIgFgWQgEgMAAgJQAAAAAAgBQAAgBAAAAQAAAAAAgBQAAAAAAgBQACgDAIAAQANAAAIAHIALARQACADAEAIQAcALADASQAEACAAAGIAAABIgOATQgPARgEAKIgEAKIAFAKQAIANAMAJIAHAEIAAAAQAAAAABAAQAAAAAAABQABAAAAAAQAAABAAAAIABABIAAABIAAADQAAAOgHAEQAFADAAABQAAABAAAAQAAABgBAAQAAABgBAAQAAAAgBAAIAAAAgAgBhDIgBgBQgOgYgKAAIgIAAIAAAFIACAJIABACIAEAFQAHgCATAGIAAAAg");
	this.shape_6.setTransform(58.9,151.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AiGAyQgfgeAAgVQAAgpALgyIACASQACALACAEIAAAfIAEAXQACAFAFAIIACADQACAIAOAKIAPAKQANAJADAAIABAAQAmAWAmAAQCKAAAVgXQAHgIgFgNIgGgVIADgCQADgCAEAAIADABQAAAIACAFQAEAOADAFQgCAXgNAJQgkAbh5AAQhSAAgugrgACjAGQACADABAGIAAAHQgCgHgBgJg");
	this.shape_7.setTransform(71.3,156.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#EACFB7").s().p("AhoA0QABAAAAAAQABAAAAgBQAAAAABAAQAAgBAAgBQAAgBgFgDQAGgEABgOIAAgDIAAgBIgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQgBAAAAAAIgBAAIgGgEQgMgJgIgLIgFgKIADgKQAFgKAPgTIAOgTIAAgBQAcgCAoAQQBKAIA0AhQAWAOALALIgDACIAGAVQAFANgHAIQgVAXiKAAQgmAAgmgWg");
	this.shape_8.setTransform(73,155.9);

	// Layer 7
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#421F10").s().p("AA5AuIhkAAQg3gIgHgFQg0gOgSgQQgQgMAAgbIABgJQACAVANAMQASAOA0APQAHAFA3AHIBkAAIBUAAQAjgRAFgHIAFgFQADgEABgEIABAGQAAAIgFAJIgFAGQgFAIgjARg");
	this.shape_9.setTransform(71.3,162.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#65301B").s().p("AA6BDIhlAAQg2gIgIgFQg0gOgRgQQgOgMgBgTQABgQAGgEQACgIALgHIAYgKIADgBIAKgEIAGgDIARgGIA1AAIAeAFIALACIAuAHIAKACIAYAEIAkAHIAGACIAOAFIAKAFIAUAKIAKAGIADACIACABIADADQAOAHAEAMIABAJIAAABIAAAAIACAEQgBAEgCAEIgGAHQgEAHgjARg");
	this.shape_10.setTransform(71.2,158.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ai4AqQgTgXAAgrQAAgIAIgPIACgEQADgHAFgEIAIgIIAHgEQAJgLAggFIAWgGIAcAAIALgCIAeAAIASAHIABAAIAuAIIAqAIIARADIAEABIAYAFIAKAEQAmALASAMQAJAFAFAEQAHAFADAWIAAABQABAKgBALQAAAWgGALIgCAEQgTAlg3AAQhPAGgjAAQiSAAgug5gAh6hMIgHADIgJAEIgEACIgXAKQgMAHgCAHQgGAEgBAQIgBAJQAAAZAQAOQARAQA0AOQAIAFA3AIIBkAAIBUAAQAjgRAEgHIAGgHQAFgJAAgKIgBgGIgCgDIgBgBIAAAAIgBgIQgEgMgNgJIgEgCIgCgBIgDgDIgKgFIgUgLIgKgFIgOgFIgGgBIgjgHIgZgFIgKgBIgtgIIgMgCIgegFIg1AAIgQAGg");
	this.shape_11.setTransform(70.9,160.5);

	// Layer 10
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#CCB59F").s().p("AA0A8IgGgCIgEAAIgCAAIgEAAQgkAAgVgNQgNgIgJgNQgIgNgEgWQgBgGABgNIACgUQAAgPAFAKQAGAKgCAIIAAAMQAGgFAKgGQAMgHAGABIgCgGQAAgOAGADQAGAEAAAMIAAACIAHAgQADANAMARQALARANALIAKAIQAAABAAABQAAAAAAABQgBAAAAAAQgBAAgBAAIgBAAg");
	this.shape_12.setTransform(93.4,154);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgKBLQgXgKgOgBIgDgCQgDgCAAgDQAAgBAAAAQAAgBAAgBQAAAAABgBQAAAAAAAAIAAgBQAAgFAKAAQAXANAjAAIAEgBIACABIAKACIAFABIA6ABIADABIAnAAIAQACIA4AAIA3gMQARgJAAgEQAAgEgFgNQgGgOAAgEIAAgCIgSgbQAAgOANANIAHAIQAEADAGAIIAEAFQANASAAAQQAAASgRAIIgBAAIgCABQgPALgfAHIgZAAIgFABIh/ACQhKgCgMgGgAkpg3IgBgDQAAgOAvgFQAvgHAAAJQAAAFgIACQgLACgnACQgGABgJAGQgIAFgFAAQgFAAgCgDgAAfg4QgGAAgBgFIAAgCQAAgFARgFQAcgGAFgDIBIACIADADQABAAAAAAQABABAAAAQAAABABAAQAAABAAAAQAAAGgLABIgEACIgwABQgUACgPAEIgPADIgIgBg");
	this.shape_13.setTransform(95.5,153.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#EACFB7").s().p("AA0BJIgQgDIglAAIgDgBIg6gBIgFAAQABAAABAAQAAgBABAAQAAAAABgBQAAgBAAAAIgLgIQgNgLgKgRQgMgSgDgNIgHggIgBgCIAYgMIAVgIQABAFAGABIAIABIAPgEQAPgDAUgCIAugBIAEgDQALgBAAgFQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQAsAEAhANQAhANAKAQQALATAGANIgFgGQgGgGgEgDIgHgJQgNgNAAAOIASAcIAAACQAAAEAGAOQAFAMAAAFQAAADgRAJIg3ANgAhNBBIAEABIAGACIgKgDgAitBEIgBgBIgBgDQgDgGgEgNQgCgFAAgJIgBgIQAAgTAFgFIADgCQAEAVAJAOQAIAMANAIQgKAAAAAGIAAAAQAAABAAAAQgBAAAAABQAAAAAAABQAAABAAAAQAAADADADgAivAzIAAgHQgBgFgCgDQABAIACAHg");
	this.shape_14.setTransform(105.6,153.3);

	// Layer 9
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#421F10").s().p("AhgAiQgcAAgdgFQgdgGgHgHQgagXAAgGIABgNQAEAIAVASQAHAGAdAGQAdAGAcAAIDIAAQAkgEApgRQAJgEAFgFQAGgDAEgDQAIgHACgIIABAIQAAANgLAKQgEACgGADQgFAEgJAFQgpATgkADg");
	this.shape_15.setTransform(107.5,162.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#65301B").s().p("AhgA+QgcAAgdgGQgdgGgHgGQgVgUgEgIQACgJAFgGQADgOAWgQIAEgDIABgBIADgCQASgLAYgHIAMgDIASgEIAIgBIBfAAIAIABIAKABIAKABIABAAQAbAFApALIATAFQAzAPAXANIAEACIAGAEIADADQAJAAACAJQgCAIgIAHQgEADgGADQgFAFgJAEQgpATgkAEg");
	this.shape_16.setTransform(107.5,157.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("Ah1BcQhGgNgbgWQgVgRAAgeQAAggAXgRQADgMAfgPQAcgOAcgEIAagHIBEAAQAfACAMADQADAAABADQARABAAAJIAAACIgKgCIgKgBIgIAAIhgAAIgHABIgTADIgMAEQgYAGgSAMIgCABIgCABIgDADQgXAQgCARQgGAFgCAIIgBANQAAAGAbAZQAGAGAdAGQAdAGAcAAIDJAAQAkgEAogTQAJgEAGgFQAGgDAEgDQALgKAAgNIgBgHQgDgKgJgBIgDgCIgFgEIgFgDQgXgNgygOIgUgGQgogLgcgEIAnAAIAMABQALgFA3ATIAPAGQAtANAJAKQAJAEADAMQADAKgCAJQAAAfgUAQQgeAYhZANg");
	this.shape_17.setTransform(106.8,159.3);

	// Layer 2
	this.instance = new lib.Girl02copy();
	this.instance.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	// Layer 3
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CCB59F").s().p("AgTEUQgqgogPhBQgDgNALgUQARgcALgUQAcgyAMg8QAogfAKh+QAEg0gChDIAAgHIAAgCIAMAFQAJADAEABQgHAqgLCKQgNCbgBACQgPAtgcAsQgPAlANA+IALAnIAKAmQgVgKgTgUg");
	this.shape_18.setTransform(15,71);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#EACFB7").s().p("AouE4IgQgBQgLgBgKgDIgOgFIgKgmIgMgnQgOg+ARglQAcgsAPgtQABgCANibQALiKAHgqQAMABAOAGIATAIQAbAKAtAiIAMAJIgIAUQgZBAgYBtQgXBtADAVIAAABQACAIAaAyIAJARQAhA+AEALIAAACQAFAVgOASIgHAHQgbAZhAAAIgYgBgAHoA6QACgBgGgRQgGgSgEgBIgBAAIAAgBIgBgHIAAgcQABgYgCgTIgFgpQgCgNABgCQARggAHgUQALgcAEgbQAIgxACgZIAAgQIAMgBIANADIAAAMQACAmAMBYQAJA7ABAXQAAANgBAOQAAAnAQAhQAGAQAZAqQApBEgRBCQgMAqg6ABQgjgBgGABIgJADQgJh8gQhCg");
	this.shape_19.setTransform(78.2,71.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AnzF6QgZgBgVgDQhbgWgYgTQgrgjgLhMQgIgxAJgcQAGgSAUgcQAdgoARg2QAGACAHAAQARAAANgIQgOA8gcAyQgKAUgRAcQgLAUADANQAPBBAqAoQAUAUAWAKIAOAFQAKADALABIAPABQBTAGAggeIAHgHQAPgSgFgVIgBgCQgDgLghg+IgJgRQgbgygCgIIAAgBQgDgVAYhtQAXhtAahAIAIgUIgMgJQgugigbgKIgTgIQgOgGgLgBQgFgBgIgDIgMgFIAAACIgDgzIAJADIA5AUIAjAPQAKAFALALIApApIABACIAQgCIAJgPQAQgUAKAGQAHAFADATIAAAHIABAJIAAABIAAAKQAAAfgFAaIgOBSQgIA6gOAlQgGASgGAJQAuAJAyAIIAGA1QhYgPghgVQgHAQgPAgQAJATAUAlQAIAPALAiQAQAxgFAVQgFAUgYAVQgRAOgKAFIgKAFQgdANglAAIgKAAgAmRgrQgYA1gDAsQALgNAIgEIgDgFQgBgCAAgNQAAgIAKgiIAJggIgHAOgAIrE6Ig4ABIgBAAIgOjhQgRACgZgCIgFgyIAfABQgHgagCggIgDgfQgCgLADgNIgOACIgKABQABgXAEgWIAIgIQAngiAKg6QAEgYgCgZQAOgPARgPIAEgBIADABIASgXIAJgEQANgHALgDIgGgPIAKgKIAXgWQAGAWAAAWIAAAIIAAAEIgBAGIgBACIgCACIgBABIgLAFQgIADACAFIAHASIAMADQAGA3AKA4QAIAyABA0QACBCARAqQAJAVAZAgQAUAaAEAXQAFAigQA9QgJAgglAXQghAUgoAAQgQAAgRgDgAIFA/QAFARgCABQARBCAJB8IAJgDQAGgBAiABQA7gBALgqQAShCgqhEQgZgqgGgQQgQghAAgnQACgOgBgNQgBgXgJg7QgMhYgCgmIAAgMIgNgDIgLABIgBAQQgCAZgHAxQgEAbgMAcQgHAUgQAgQgBACACANIAFApQACATgBAWIgBAeIABAHIABABIABAAQAEABAGASg");
	this.shape_20.setTransform(74.9,69.1);

	// Layer 1
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#CCB59F").s().p("AC3COQgWgOgFgpQgDgPAAgzQAAghADgLQACgJAKgIQAcgYAdgsIgVD6gAixALQgYhggXgtQAugJAigCIBEEWIgIAAQhAgqgdhUg");
	this.shape_21.setTransform(72.4,141.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#EACFB7").s().p("ABWCTIAVj6QANgTAOgYQA/ABAaADIAoAFQgJAVgcB3QgTBUgyA8gAkGiIQA2gEAxAOQBGATATADIAAB7QgCAEACAZIAEAkQgBAPgFAMIgIAMQgJALgPAGIhaACg");
	this.shape_22.setTransform(84.2,140.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AAlB8QgHgdABgmIADgjIAAhNQAFgVAWgPIApgdQAHgHAQgjQAMgbAQgBIABAAQCggHAeAbQAKAJgEAPQgCAIgHANIgSBTQgMA3gOAkQgrBthoAdQgQAFgOABIgIAAQg8AAgPhFgACShqQgdAsgcAYQgKAIgDAJQgCALAAAhQAAAzACAPQAGApAWAOIAUAAIBIAAQAyg8AThTQAch4AIgVIgogFQgagCg/gCQgNAYgNATgAiSC1QgogBgXgMQgYgOgRAAQgKAAgDgCIgHgGQAAgegohvQgohtAAgmQAAgOAPgFQALgFBKgNIARAAQBTADA8AMQBBANATAWQANASABAYQAAAKgCAcQAAAdgCAUQgCAbgGAxQgBAHgMAKIgRAPQAGgMAAgPIgDgjQgDgZACgFIAAh7QgSgDhGgTQgygOg1AEQgjACguAJQAYAtAYBgQAcBUBAAqIAIAAIBagBQAPgHAKgLQgqA9g9AAIgCAAg");
	this.shape_23.setTransform(80.4,140.9);

	this.addChild(this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.instance,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(3,-2.6,143.9,173);


(lib.FemaleYard_03 = function() {
	this.initialize();

	// Layer 4
	this.instance = new lib.StaySafe();
	this.instance.setTransform(115,31.2,0.08,0.08,-9,0,0,109.9,97.8);

	// Layer 2
	this.instance_1 = new lib.Girl03_Yard();
	this.instance_1.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(2.2,4.2,144.7,169.2);


(lib.FemaleYard_02Drivingcopy = function() {
	this.initialize();

	// Layer 4
	this.instance = new lib.StaySafe();
	this.instance.setTransform(115,31.2,0.08,0.08,-9,0,0,109.9,97.8);

	// Layer 2
	this.instance_1 = new lib.Girl02_YardDrivingcopy();
	this.instance_1.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(14.7,4.2,132,169.2);


(lib.FemaleYard_02Driving = function() {
	this.initialize();

	// Layer 4
	this.instance = new lib.StaySafe();
	this.instance.setTransform(115,31.2,0.08,0.08,-9,0,0,109.9,97.8);

	// Layer 2
	this.instance_1 = new lib.Girl02_YardDriving();
	this.instance_1.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(14.7,4.2,132,169.2);


(lib.FemaleYard_02 = function() {
	this.initialize();

	// Layer 4
	this.instance = new lib.StaySafe();
	this.instance.setTransform(115,31.2,0.08,0.08,-9,0,0,109.9,97.8);

	// Layer 2
	this.instance_1 = new lib.Girl02_Yard();
	this.instance_1.setTransform(74.8,85.6,1,1,0,0,0,66.5,82);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(2.2,4.2,144.7,169.2);


(lib.fadeFromWhite = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{play:0,stop:18});

	// Layer 1
	this.instance = new lib.WhiteCoverOpaque();
	this.instance.setTransform(499.4,292.2,1.686,1.321,0,0,0,296.1,221.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({alpha:0},13).to({_off:true},1).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,998.7,584.2);


(lib.ControlsExplanation = function() {
	this.initialize();

	// Layer 1
	this.keys_mc = new lib.keys_mc();
	this.keys_mc.setTransform(215,277.3,1,1,0,0,0,162.6,212.8);

	this.text = new cjs.Text("forklift down", "28px 'Laffayette Comic Pro'");
	this.text.lineHeight = 57;
	this.text.lineWidth = 272;
	this.text.setTransform(42,408.8,1.18,1.18);

	this.text_1 = new cjs.Text("forklift up", "28px 'Laffayette Comic Pro'");
	this.text_1.lineHeight = 57;
	this.text_1.lineWidth = 272;
	this.text_1.setTransform(42,341.8,1.18,1.18);

	this.text_2 = new cjs.Text("stop", "28px 'Laffayette Comic Pro'");
	this.text_2.lineHeight = 57;
	this.text_2.lineWidth = 272;
	this.text_2.setTransform(42,282.8,1.18,1.18);

	this.text_3 = new cjs.Text("move backwards", "28px 'Laffayette Comic Pro'");
	this.text_3.lineHeight = 57;
	this.text_3.lineWidth = 272;
	this.text_3.setTransform(42,218.8,1.18,1.18);

	this.text_4 = new cjs.Text("move forwards", "28px 'Laffayette Comic Pro'");
	this.text_4.lineHeight = 57;
	this.text_4.lineWidth = 272;
	this.text_4.setTransform(42,157.8,1.18,1.18);

	this.text_5 = new cjs.Text("turn Left", "28px 'Laffayette Comic Pro'");
	this.text_5.lineHeight = 57;
	this.text_5.lineWidth = 272;
	this.text_5.setTransform(42,94.8,1.18,1.18);

	this.text_6 = new cjs.Text("turn right ", "28px 'Laffayette Comic Pro'");
	this.text_6.lineHeight = 57;
	this.text_6.lineWidth = 270;
	this.text_6.setTransform(42,24.8,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(4,1,1).p("EgfYgmRMA+xAAAMAAABMjMg+xAAAg");
	this.shape.setTransform(200.9,245.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F4D96F").s().p("EgfYAmSMAAAhMjMA+xAAAMAAABMjg");
	this.shape_1.setTransform(200.9,245.1);

	this.addChild(this.shape_1,this.shape,this.text_6,this.text_5,this.text_4,this.text_3,this.text_2,this.text_1,this.text,this.keys_mc);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,405.9,494.1);


(lib.CloseX = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Arrow_Butncopy();
	this.instance.setTransform(34.9,34.9,1,1,0,0,0,34.9,34.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AkLEMQhvhvAAidQAAicBvhvQBPhPBmgXQAqgJAsAAQCdAABvBvQBvBvAACcQAACdhvBvQg6A7hHAbQhAAZhLAAQicAAhvhvg");
	this.shape.setTransform(34.8,34.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regX:34.9,regY:34.9,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regX:34.8,regY:34.8,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 2
	this.instance_1 = new lib.shadowCircle();
	this.instance_1.setTransform(31.4,39.1,1,1,0,0,0,36.9,36.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:true},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.5,-2,77.2,77.9);


(lib.btn_turnR = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.CurlyArrowcopy();
	this.instance.setTransform(19.6,26.9,1,1,0,0,180,19.6,19.4);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 78, 42, 63, 0)];
	this.instance.cache(-3,-9,44,50);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AlQEkIAApIIKgAAIAAJIg");
	this.shape.setTransform(16.8,24.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.shape}]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3,0,44.2,47.8);


(lib.btn_turnL = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.CurlyArrowcopy();
	this.instance.setTransform(19.6,26.9,1,1,0,0,0,19.6,19.4);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 78, 42, 63, 0)];
	this.instance.cache(-3,-9,44,50);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AlQEkIAApIIKgAAIAAJIg");
	this.shape.setTransform(16.8,24.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.shape}]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.9,0,44.2,47.8);


(lib.btn_reverse = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ARR();
	this.instance.setTransform(13.5,20.5,0.68,0.68,-90,0,0,19.6,19.4);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 78, 42, 63, 0)];
	this.instance.cache(-10,-2,58,54);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AlQEkIAApIIKgAAIAAJIg");
	this.shape.setTransform(16.8,24.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.shape}]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0.3,3,34.2,36.4);


(lib.btn_lower = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ARR();
	this.instance.setTransform(3.5,25.8,0.599,0.543,-90,0,0,22.6,25.2);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 77, 42, 62, 0)];
	this.instance.cache(-10,-2,58,54);

	this.instance_1 = new lib.ForkArm();
	this.instance_1.setTransform(15.6,16.5,0.774,0.774,0,0,0,31.1,30.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AkLEkIAApIIIXAAIAAJIg");
	this.shape.setTransform(16.8,24.4,1.255,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).to({state:[{t:this.shape}]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.2,-7.1,49.9,51.4);


(lib.btn_lift = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ARR();
	this.instance.setTransform(3.5,27.9,0.599,0.543,0,-90,90,22.6,25.2);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 77, 42, 62, 0)];
	this.instance.cache(-10,-2,58,54);

	this.instance_1 = new lib.ForkArm();
	this.instance_1.setTransform(15.6,16.5,0.774,0.774,0,0,0,31.1,30.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AlQEkIAApIIKgAAIAAJIg");
	this.shape.setTransform(16.8,24.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).to({state:[{t:this.shape}]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.2,-7.1,49.9,48.5);


(lib.btn_forward = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ARR();
	this.instance.setTransform(13.5,20.5,0.68,0.68,0,-90,90,19.6,19.4);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 78, 42, 63, 0)];
	this.instance.cache(-10,-2,58,54);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AlQEkIAApIIKgAAIAAJIg");
	this.shape.setTransform(16.8,24.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.shape}]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0.3,1.6,34.2,36.4);


(lib.boss_mc = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{boss:0,boss_stop:32});

	// Layer 3
	this.gotit_btn = new lib.GotItButton();
	this.gotit_btn.setTransform(580,270,1,1,0,0,0,99,31.7);
	this.gotit_btn._off = true;
	new cjs.ButtonHelper(this.gotit_btn, 0, 1, 2, false, new lib.GotItButton(), 3);

	this.timeline.addTween(cjs.Tween.get(this.gotit_btn).wait(19).to({_off:false},0).wait(16));

	// Layer 4
	this.info_txt = new cjs.Text("", "28px 'Laffayette Comic Pro'");
	this.info_txt.name = "info_txt";
	this.info_txt.textAlign = "center";
	this.info_txt.lineHeight = 34;
	this.info_txt.lineWidth = 365;
	this.info_txt.setTransform(548.3,4.5,1.18,1.18);
	this.info_txt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.info_txt).wait(9).to({_off:false},0).wait(26));

	// Layer 2
	this.instance = new lib.Boss_Bubble("single",2);
	this.instance.setTransform(268.6,173.5,0.107,0.12,0,0,0,-176.2,115.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({_off:false},0).to({regX:-176.3,regY:115,scaleX:1,scaleY:1,x:268.5},4).wait(26));

	// Layer 1
	this.instance_1 = new lib.BossAnims("single",0);
	this.instance_1.setTransform(-146.2,591.2,0.87,0.87,0,0,0,232.8,228.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({y:199.2},4).wait(1).to({mode:"synched",loop:false},0).wait(30));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,392,348,410.7);


(lib.BadgeYard = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlGCiQgCgSAkhnQhShBgGgvQALgQAIgBIA/BIIAaAQQAghUAPgTIAKgBIAKAIQABANhKCuQgSAxgGAmIgGADQgPgFgDgOgAjPCUQgEgbBSjrIAJgIIAJgBQAQgDAwCXQAgBHADAbQgCAOgMACQgQACgRhJIgDgCIgCAAQgaAEhSAtQgBAvgTADQgNgDgCgOgAhyg4QgTAwgNAyIAOgCQAVgDAfgOIgehQgAAFB/IgCgOIgDi5QgIABgCgOQgGgqBDgRIBEgMQA2gIAGAmIAAACQAJBChpBEIAAADQBAAsAqAVIABAHQACASgLAHIgKABIiOhKIAJBTQgEANgNACIgDAAQgHAAgGgIgAB5hsQgnAFg1AYIACBBIACAAQB2gxgFglIAAgDQgBgFgSAAIgGAAgADRBbIgIjMQgMACgBgHIgBgFQgFglAhgFIAxgMIALgCQBKgKAaA5IAEASQARB0ikBkIgKAEIgCABIAAAAQgFAAgGgQgAD5h8IgTACQgCB6AGAqIACAMQB5hSgIg5IAAgCQgFgigSADQgQgJgcAAQgPAAgSADg");
	this.shape.setTransform(232.2,271.2,1.646,1.646);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AjTCnQgGiFAEhSIhJAWQgLACgFgVQgEgjA+gIQAjgPBVgLQAWgEACATIABAEQAEAagoAGIgxAJQgFB4AMBVQgEATgGABIgIABIgFAAQgIAAgDgFgAhICPQgTj/AggFIAEAAIAJAIQgFBYAGAmQBsgdgDgUIgFhlIAQgJQANABACANIAAADQATD9gIABIgCADIgHABQgNABgGgLQAAgigIg9IgDAAIhgAnIgHAKIAHAtQADAUgOAKIgFABIgDAAQgLAAgEgKgACCBIQgGgoABhvIgGgMQgHguCMgcIAegGQAMAAACAQQAAAdgTADQiBASAFAkIAAAVIACALQBRggAfgFIAFAAQAMAEADARQADAYghAFQhqAbADAUIACAgQADAUAagCQAogKA6gYIAHgBQAKACACAMIABAHQAEAch0AfIgHABIgIABQgjAAgLgwg");
	this.shape_1.setTransform(225.1,219.2);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7A6C38").s().p("AiPINIhkjCIgKgUIgYgvIgEgIQg9h7g2h9IgCgHIgOgeQhejhhDjqIJUhPICigVQCgJFDlH5IivAYIn7BDIgjhAg");
	this.shape_2.setTransform(80.9,262.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9E8C49").s().p("AkAZ0QnRjbk6jnQjVidiVjiQgYglgZgqIH3hDICvgXQEJI9FiHeIhrgxgAyfntMAongFZQAkgHBFgIQA2gGA9gOIHhhQQhGG6iEGBIgWA/Qg7CihGCZIg9AJMgnBAFNQjln8igpDgA/triIAAgDIgLg2QgWhxgQhzQghjvgFjbIADirIBnAAIAngIIETgEICigVIBogPQBWJfCSIfIiiAVIpXBQQgoiPgeiSg");
	this.shape_3.setTransform(217.9,252.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("EgH1AgDIgWgKQlindkJo+MAmqgFIIAbgFQgbA4gcA2QhBCAgjBCQg9Byg3BUQiVDijVCdQk6DnnPDbQifBLiCA3QhMgghVgngA8M1PQC7gcBUgVQDCgwEpiQQDHhgDgieQDyiqAlhhQAkBhDyCqQDeCeDIBgQEoCQDDAwQCHAiGSAzIESADIAnAIIBoAAQABB+gMCXIAAAHIgEApIgBAJIAAAJIglEqIAAAAInlBOIjkAjMgozAFcQiSofhWpeg");
	this.shape_4.setTransform(255.2,218.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#1C180C").s().p("A5AM5IH9hEICvgXMAnBgFNIA8gJIgfBBIgaAEMgmrAFJIivAXIn3BCIgfg2gEggigE1IJYhQICigWMAoygFcIDkgjIHmhNIgCAJIgDAXIgEAaInhBQQg9AOg3AGQhEAIglAHMgomAFZIijAVIpWBPIgQg4gEAgjgNuIAAABIgEApIgBAJIgDABIAIg0g");
	this.shape_5.setTransform(230.1,238.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("EgA0Ah+IgLgDQgggMAFgCIgCAAQj8hXkzi4QkOihlHj3QiNhrifkPIgHgLIgfg3QhKiGhAiSIgYg3IgbhDQihmUgfhQIgghYQhBitgmiFQgUhFgQhIIgMg3Qg6kjAAlKQgDh+AHhkIAEguIgCgCQgGgFAAgDQAAgPADgDQACgCAIAAQAFgeBIACQBqACAXgGIEVgCICLgWIBlgPIA8gIQCUgXBVgTQDcgzEiicQCphaDUiuQDujFAyggIAAAEIABgEQAyAgDuDFQDUCuCpBaQEiCcDdAzQBUATCUAXQDpAiBEALIEVACQAXAGBqgCQBHgCAGAeQAHAAACACQAEADAAAPQAAADgGAFIgDACQAMBpgDCnIgBAPIgEA5QgLCfgVCWIAAACIAAABIgIA0QgbC0gqCmQg1DThfEQIgWA+Qg2CVhBCmIgVA1IgFANQhdDmiHDgQijEMiNBvQpwHwoWC3QguARgtANIgDAAQgNAAgkgNgEghkgR2QAFDaAhDwQAQBzAWBxIALA2IAAADQAeCSAoCPIAQA4QBDDoBeDhIAOAhIACAGQA2B+A9B6IAEAIIAYAvIALAVIBkDBIAjBBIAfA2QAZArAYAkQCWDiDUCeQE6DnHRDbIBrAxIAWAKQBVAnBKAfQCCg3CfhKQHRjbE6jnQDVieCVjiQA3hUA9hyQAjhCBBh/QAcg3Abg3IAfhBQBGiaA7ikIAWg+QCEl/BGm6IAEgaIAEgXIABgJIAAgBIAlkpIAAgJIABgJIAEgpIAAgIQAMiXgBh9IhoAAIgngIIkSgEQmSgyiHgiQjDgwkoiRQjIhfjgifQjyiqgkhgQgjBgjyCqQjgCfjHBfQkpCRjCAwQhUAVi7AbIhoAPIiiAVIkSAEIgoAIIhnAAg");
	this.shape_6.setTransform(221,218.8);

	// Layer 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(0,0,0,0.502)").s().p("EgA0Ah/IgLgEQgggMAFgBIgCgBQj8hYkzi2QkOiilHj3QiNhrifkPIgHgLIgfg3QhKiGhAiRIgYg4IgbhEQihmUgfhPIgghXQhBiugmiFQgUhFgQhIIgLg3Qg7kjAAlJQgDiAAHhiIAEgvIgCgCQgGgGAAgCQAAgPADgDQACgBAIgBQAFgdBIAAQBqADAXgFIEVgDQAogHBjgPIBlgPIA8gJQCUgVBVgUQDcgzEiibQCphbDUivQDujEAyggIAAAFIABgFQAyAgDuDEQDUCvCpBbQEiCbDdAzQBUAUCUAVQDpAjBEALIEVADQAXAFBqgDQBHAAAGAdQAHABACABQAEADAAAPQAAACgGAGIgDACQAMBpgDCoIgBANIgEA6QgLCfgUCWIAAABIgBACIAAABIgDApIgBAJIgDABQgcCzgqCoQg0DShgERIgWA+Qg2CUhBCnIgVAzIgFAPQhdDliHDfQijENiNBvQpwHwoWC4QguAPgtANIgDABQgNAAgkgMg");
	this.shape_7.setTransform(230.5,228.3);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.instance,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,451.6,447);


(lib.BadgeDeliveryTruck = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ai8KWQgJjaAFiJIh3AkQgRACgIghQgIg5BlgOQA7gYCJgTQAlgFAEAeIABAIQAFAphAAJIhQAQQgJDGATCMQgFAfgKACIgNACIgIABQgOAAgEgJgAAlJzIgDgXIgIkyQgPACgDgXQgIhFBvgcIBwgUQBYgMAJA+IABADQAPBtitByIABAEQBoBIBEAhIACAMQAEAegSALIgQACIjoh4IAPCHQgIAVgVADIgEAAQgMAAgJgMgADiDuQhAAJhWAoIADBqIAEgBQDBhPgJg9IAAgEQgCgKgkAAIgDAAgAF3IhIAAgDQgbmPAngGIAIgBQATAFADAHQgEDMANBaIAFAqQBrgPA3hsQgHjxAZgDIAEgBQAYgBACAJQgGDeAPBrIAJA8QAEAegWAPIgQACIgSgNIgGhIQgOACiDBcIgbALIgCAAQgvAAgFgjgAK2GhIgBgIQgViVDAitQAmgVAUgDIARgCQAfgFANAxIABAIQALBMg2AgIgIgHIAAgEIAIhEIgPgFQhTAMheCuQgUA0ADAZIABAEQAIA9BLgKIADgBQBHgKBSgzIALgBQAOAEADAVIACAQQg9BThhANIgLABQh7AAgQhxgAQXHgIgKi2IgKgtQANgCgOiqQALghAWgDIANAOIAJCTIDTieQARAQAEAbQgfA8iUBqIABAEQBXBVBnA0IACAHQAHA2glAFIjQh/QAKCbgLABIgSAHQgUgDgDgRgArQBGIgNlPQgUADgCgMIgBgHQgJg+A2gHIBRgUIARgCQB5gRApBcIAIAfQAbDAkLCiIgQAGIgEABIAAAAQgIAAgJgZgAqQkbIgeAEQgDDKAKBEIACASQDGiHgNheIAAgEQgIg2geAEQgbgNgsAAQgZAAgeAEgAmbgiQgJhBACi4IgLgVQgKhKDjguIAygLQAUACADAZQABAwggAEQjTAeAIA7IABAiIACASQCFg1AzgHIAHgBQAVAGAEAcQAFAog2AIQitAwAFAgIABAlIACAOQAFAhAqgCQBBgQBggoIALgBQARADACAUIACALQAGAti8AxIgMACIgNABQg6AAgShMgA2zAJQgehrgOhGIg4AgQgIADgIgQQgLgbAygTQAbgTBDgaQASgHAFAPIACAEQAIAUggAMIgnARQATBmAaBEQABAPgFACIgGACIgIACQgEAAgCgDgAhTgRQgbmiAOgCIALgJIADgBQATgCAPARQAADFAKCaIDShLIAJAKIADAWQAHA0jlBJQgrgBgCgRgA1FgjQhBjRAZgKIAEgCIAJAGQAMBJANAgQBVgugHgRIgWhSIALgLQALgBAEALIABABQBADQgGACIgBADIgGACQgKAEgHgIQgHgdgTgwIgBAAIhLAzIgDAKIAOAkQAGAQgJALIgEABIgHACQgGAAgEgGgADpgzQgUkJAFieIAPgSIAagEQANgCACAQQAFD/AOBhIAGAqQgDAsgjAFIgJABgAGoiYQhwj/gIg7QADgTAPgLIAPgDQAVgCAsCeIBFChIADAAQAZhtBij0IALgJIALgCQAQAEADAVQAEAchXDmQgjCngNAPIgkAMIgCABQgYAAgVhUgAyqiFQgMgggVhdIgIgKQgOgkBugyIAXgLQALgBAEAMQAGAYgPAGQhnAoALAdIAHAaQA9grAZgJIAEgCQALABAFAOQAIAUgbAKQhSAsAHAPIAEATIADAIQAGAQAVgHQAggQArgfIAGgCQAIAAAEAKIACAFQAJAWhaAwIgFACQgIAEgIAAQgXAAgPgggAKsi8QgJhBACi4IgLgVQgKhKDjguIAygLQAUACADAZQABAwggAEQjTAeAIA7IABAiIACASQCFg1AzgHIAHgBQAVAGAEAcQAFAog2AIQitAwAFAgIABAlIACAOQAFAhAqgCQBBgQBggoIALgBQARADACAUIACALQAGAti8AzIgMACIgNABQg6AAgShOgAP1ihIgDgXIgIkyQgPADgDgXQgKhGBxgcIBwgTQBYgNAJA+IABAEQAPBsitBzIABADQBoBIBEAiIACALQAEAegSAMIgQACIjoh5IAPCIQgIAVgVACIgEABQgMAAgJgNgASyolQhAAJhWAoIADBpIAEAAQDBhQgJg8IAAgEQgCgKgkAAIgDAAgAVjjoQgCgeA6ioQiFhsgLhOQASgaAMgCIBoB2IAqAaQA1iJAZgeIARgDIAQAOQAAAUh5EgQgdBRgKA9IgKAGQgZgIgEgYg");
	this.shape.setTransform(201.3,252);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7A6C38").s().p("AiPINIhkjCIgKgUIgYgvIgEgIQg9h7g2h9IgCgHIgOgeQhejhhDjqIJUhPICigVQCgJFDlH5IivAYIn7BDIgjhAg");
	this.shape_1.setTransform(80.9,262.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#9E8C49").s().p("AkAZ0QnRjbk6jnQjVidiVjiQgYglgZgqIH3hDICvgXQEJI9FiHeIhrgxgAyfntMAongFZQAkgHBFgIQA2gGA9gOIHhhQQhGG6iEGBIgWA/Qg7CihGCZIg9AJMgnBAFNQjln8igpDgA/triIAAgDIgLg2QgWhxgQhzQghjvgFjbIADirIBnAAIAngIIETgEICigVIBogPQBWJfCSIfIiiAVIpXBQQgoiPgeiSg");
	this.shape_2.setTransform(217.9,252.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CCB55E").s().p("EgH1AgDIgWgKQlindkJo+MAmqgFIIAbgFQgbA4gcA2QhBCAgjBCQg9Byg3BUQiVDijVCdQk6DnnPDbQifBLiCA3QhMgghVgngA8M1PQC7gcBUgVQDCgwEpiQQDHhgDgieQDyiqAlhhQAkBhDyCqQDeCeDIBgQEoCQDDAwQCHAiGSAzIESADIAnAIIBoAAQABB+gMCXIAAAHIgEApIgBAJIAAAJIglEqIAAAAInlBOIjkAjMgozAFcQiSofhWpeg");
	this.shape_3.setTransform(255.2,218.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#1C180C").s().p("A5AM5IH9hEICvgXMAnBgFNIA8gJIgfBBIgaAEMgmrAFJIivAXIn3BCIgfg2gEggigE1IJYhQICigWMAoygFcIDkgjIHmhNIgCAJIgDAXIgEAaInhBQQg9AOg3AGQhEAIglAHMgomAFZIijAVIpWBPIgQg4gEAgjgNuIAAABIgEApIgBAJIgDABIAIg0g");
	this.shape_4.setTransform(230.1,238.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("EgA0Ah+IgLgDQgggMAFgCIgCAAQj8hXkzi4QkOihlHj3QiNhrifkPIgHgLIgfg3QhKiGhAiSIgYg3IgbhDQihmUgfhQIgghYQhBitgmiFQgUhFgQhIIgMg3Qg6kjAAlKQgDh+AHhkIAEguIgCgCQgGgFAAgDQAAgPADgDQACgCAIAAQAFgeBIACQBqACAXgGIEVgCICLgWIBlgPIA8gIQCUgXBVgTQDcgzEiicQCphaDUiuQDujFAyggIAAAEIABgEQAyAgDuDFQDUCuCpBaQEiCcDdAzQBUATCUAXQDpAiBEALIEVACQAXAGBqgCQBHgCAGAeQAHAAACACQAEADAAAPQAAADgGAFIgDACQAMBpgDCnIgBAPIgEA5QgLCfgVCWIAAACIAAABIgIA0QgbC0gqCmQg1DThfEQIgWA+Qg2CVhBCmIgVA1IgFANQhdDmiHDgQijEMiNBvQpwHwoWC3QguARgtANIgDAAQgNAAgkgNgEghkgR2QAFDaAhDwQAQBzAWBxIALA2IAAADQAeCSAoCPIAQA4QBDDoBeDhIAOAhIACAGQA2B+A9B6IAEAIIAYAvIALAVIBkDBIAjBBIAfA2QAZArAYAkQCWDiDUCeQE6DnHRDbIBrAxIAWAKQBVAnBKAfQCCg3CfhKQHRjbE6jnQDVieCVjiQA3hUA9hyQAjhCBBh/QAcg3Abg3IAfhBQBGiaA7ikIAWg+QCEl/BGm6IAEgaIAEgXIABgJIAAgBIAlkpIAAgJIABgJIAEgpIAAgIQAMiXgBh9IhoAAIgngIIkSgEQmSgyiHgiQjDgwkoiRQjIhfjgifQjyiqgkhgQgjBgjyCqQjgCfjHBfQkpCRjCAwQhUAVi7AbIhoAPIiiAVIkSAEIgoAIIhnAAg");
	this.shape_5.setTransform(221,218.8);

	// Layer 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(0,0,0,0.502)").s().p("EgA0Ah/IgLgEQgggMAFgBIgCgBQj8hYkzi2QkOiilHj3QiNhrifkPIgHgLIgfg3QhKiGhAiRIgYg4IgbhEQihmUgfhPIgghXQhBiugmiFQgUhFgQhIIgLg3Qg7kjAAlJQgDiAAHhiIAEgvIgCgCQgGgGAAgCQAAgPADgDQACgBAIgBQAFgdBIAAQBqADAXgFIEVgDQAogHBjgPIBlgPIA8gJQCUgVBVgUQDcgzEiibQCphbDUivQDujEAyggIAAAFIABgFQAyAgDuDEQDUCvCpBbQEiCbDdAzQBUAUCUAVQDpAjBEALIEVADQAXAFBqgDQBHAAAGAdQAHABACABQAEADAAAPQAAACgGAGIgDACQAMBpgDCoIgBANIgEA6QgLCfgUCWIAAABIgBACIAAABIgDApIgBAJIgDABQgcCzgqCoQg0DShgERIgWA+Qg2CUhBCnIgVAzIgFAPQhdDliHDfQijENiNBvQpwHwoWC4QguAPgtANIgDABQgNAAgkgMg");
	this.shape_6.setTransform(230.5,228.3);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,451.6,447);


(lib.Badge = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Au8D+IgHjEIgCABQgJACgDgWQgFgiAngJIBNgVQBEgJAGAmQALBPiMBUQgLACACAOQAJBOgOABIgLACQgEAAgGgKgAtSAVQhXARACALIABBIQBIgeAhgzIACgPIgBgEIgLgBgAr/CpIgGgcQgPhvBdhFQAYgNAXgDQBKgBALBMQAOBlhGBOQgfAXgaAEIgOABQg1AAgYg6gAqVgFQgvAFgmBlQgFAdACAPQAgAkArgGQBGgJAQhXIAAgbQgJg4g7gCgAoVCWIgCgOQgEgZAMgCQATAqAhgEQABAEAbgEQAogFAggwQgEgah1AIQgrgFgDgXQgIg2BohIIAbgJIADAAQAYAAADAUIABAEQAFAjgUAfIgDAAQgEAAgFgIIAEgkIgIgDQgZAFg8A9IABACIB6AGQAVAMAFAiIACAOQAGAug3ArQgWAMgWADIgEABIgQABQgxAAgSgugAj/CfQgGiFAEhSIhJAWQgLACgFgVQgEgjA+gIQAjgPBVgLQAWgEACATIABAEQAEAagoAGIgxAJQgFB4AMBVQgEATgGABIgIABIgFAAQgJAAgCgFgAAbB1IgCgOIgFi5QgJABgCgOQgGgqBGgRIBEgMQA2gIAFAmIAAACQAKBChqBEIABADQA/AsAqAUIABAIQADASgMAHIgJABIiPhKIAKBTQgFANgNACIgCAAQgIAAgFgIgACOh2QgnAFg0AYIACBBIACAAQB2gxgFgmIgBgCQgBgGgSAAIgGABgADhAeIgFgdQgQhtBdhGQAYgNAXgEQBLAAALBNQAOBmhHBLQgeAYgaAEIgPAAQg0AAgZg5gAFMiRQgwAHgmBlQgFAeACANQAgAjArgFQBGgKAQhVIAAgaQgIg6g7gCgAHLgBIgFgdQgQhuBdhHQAYgNAXgEQBLAAALBNQAOBmhHBLQgeAYgaAEIgPAAQg0AAgZg3gAI2iyQgwAHgmBlQgFAeACAPQAgAiArgGQBGgIAQhXIAAgaQgIg6g7gCgAKwAWIgCgMQgEgdAphwQAXhzAMgFIALgEIAEAAQAMAEABANIABAFQALBMAcBkIACgBQAujKANgCIAHgBQAKgBAnCDQAeBBAGAuQgEASgNABQgJABgfhwIgdhIQgPAhgfCIQgMAQgKACQgdAEgLhMIgYhgIgDAAIgtCfQgBAkgFABIgJABg");
	this.shape.setTransform(232.2,266.7,1.561,1.561);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AjTCnQgGiFAEhSIhJAWQgLACgFgVQgEgjA+gIQAjgPBVgLQAWgEACATIABAEQAEAagoAGIgxAJQgFB4AMBVQgEATgGABIgIABIgFAAQgIAAgDgFgAhICPQgTj/AggFIAEAAIAJAIQgFBYAGAmQBsgdgDgUIgFhlIAQgJQANABACANIAAADQATD9gIABIgCADIgHABQgNABgGgLQAAgigIg9IgDAAIhgAnIgHAKIAHAtQADAUgOAKIgFABIgDAAQgLAAgEgKgACCBIQgGgoABhvIgGgMQgHguCMgcIAegGQAMAAACAQQAAAdgTADQiBASAFAkIAAAVIACALQBRggAfgFIAFAAQAMAEADARQADAYghAFQhqAbADAUIACAgQADAUAagCQAogKA6gYIAHgBQAKACACAMIABAHQAEAch0AfIgHABIgIABQgjAAgLgwg");
	this.shape_1.setTransform(225.1,219.2);

	// Layer 3
	this.instance = new lib.TP_logo_white();
	this.instance.setTransform(224.1,144.2,0.48,0.48,-8,0,0,291.6,34.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7A6C38").s().p("AiPINIhkjCIgKgUIgYgvIgEgIQg9h7g2h9IgCgHIgOgeQhejhhDjqIJUhPICigVQCgJFDlH5IivAYIn7BDIgjhAg");
	this.shape_2.setTransform(80.9,262.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9E8C49").s().p("AkAZ0QnRjbk6jnQjVidiVjiQgYglgZgqIH3hDICvgXQEJI9FiHeIhrgxgAyfntMAongFZQAkgHBFgIQA2gGA9gOIHhhQQhGG6iEGBIgWA/Qg7CihGCZIg9AJMgnBAFNQjln8igpDgA/triIAAgDIgLg2QgWhxgQhzQghjvgFjbIADirIBnAAIAngIIETgEICigVIBogPQBWJfCSIfIiiAVIpXBQQgoiPgeiSg");
	this.shape_3.setTransform(217.9,252.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("EgH1AgDIgWgKQlindkJo+MAmqgFIIAbgFQgbA4gcA2QhBCAgjBCQg9Byg3BUQiVDijVCdQk6DnnPDbQifBLiCA3QhMgghVgngA8M1PQC7gcBUgVQDCgwEpiQQDHhgDgieQDyiqAlhhQAkBhDyCqQDeCeDIBgQEoCQDDAwQCHAiGSAzIESADIAnAIIBoAAQABB+gMCXIAAAHIgEApIgBAJIAAAJIglEqIAAAAInlBOIjkAjMgozAFcQiSofhWpeg");
	this.shape_4.setTransform(255.2,218.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#1C180C").s().p("A5AM5IH9hEICvgXMAnBgFNIA8gJIgfBBIgaAEMgmrAFJIivAXIn3BCIgfg2gEggigE1IJYhQICigWMAoygFcIDkgjIHmhNIgCAJIgDAXIgEAaInhBQQg9AOg3AGQhEAIglAHMgomAFZIijAVIpWBPIgQg4gEAgjgNuIAAABIgEApIgBAJIgDABIAIg0g");
	this.shape_5.setTransform(230.1,238.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("EgA0Ah+IgLgDQgggMAFgCIgCAAQj8hXkzi4QkOihlHj3QiNhrifkPIgHgLIgfg3QhKiGhAiSIgYg3IgbhDQihmUgfhQIgghYQhBitgmiFQgUhFgQhIIgMg3Qg6kjAAlKQgDh+AHhkIAEguIgCgCQgGgFAAgDQAAgPADgDQACgCAIAAQAFgeBIACQBqACAXgGIEVgCICLgWIBlgPIA8gIQCUgXBVgTQDcgzEiicQCphaDUiuQDujFAyggIAAAEIABgEQAyAgDuDFQDUCuCpBaQEiCcDdAzQBUATCUAXQDpAiBEALIEVACQAXAGBqgCQBHgCAGAeQAHAAACACQAEADAAAPQAAADgGAFIgDACQAMBpgDCnIgBAPIgEA5QgLCfgVCWIAAACIAAABIgIA0QgbC0gqCmQg1DThfEQIgWA+Qg2CVhBCmIgVA1IgFANQhdDmiHDgQijEMiNBvQpwHwoWC3QguARgtANIgDAAQgNAAgkgNgEghkgR2QAFDaAhDwQAQBzAWBxIALA2IAAADQAeCSAoCPIAQA4QBDDoBeDhIAOAhIACAGQA2B+A9B6IAEAIIAYAvIALAVIBkDBIAjBBIAfA2QAZArAYAkQCWDiDUCeQE6DnHRDbIBrAxIAWAKQBVAnBKAfQCCg3CfhKQHRjbE6jnQDVieCVjiQA3hUA9hyQAjhCBBh/QAcg3Abg3IAfhBQBGiaA7ikIAWg+QCEl/BGm6IAEgaIAEgXIABgJIAAgBIAlkpIAAgJIABgJIAEgpIAAgIQAMiXgBh9IhoAAIgngIIkSgEQmSgyiHgiQjDgwkoiRQjIhfjgifQjyiqgkhgQgjBgjyCqQjgCfjHBfQkpCRjCAwQhUAVi7AbIhoAPIiiAVIkSAEIgoAIIhnAAg");
	this.shape_6.setTransform(221,218.8);

	// Layer 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(0,0,0,0.502)").s().p("EgA0Ah/IgLgEQgggMAFgBIgCgBQj8hYkzi2QkOiilHj3QiNhrifkPIgHgLIgfg3QhKiGhAiRIgYg4IgbhEQihmUgfhPIgghXQhBiugmiFQgUhFgQhIIgLg3Qg7kjAAlJQgDiAAHhiIAEgvIgCgCQgGgGAAgCQAAgPADgDQACgBAIgBQAFgdBIAAQBqADAXgFIEVgDQAogHBjgPIBlgPIA8gJQCUgVBVgUQDcgzEiibQCphbDUivQDujEAyggIAAAFIABgFQAyAgDuDEQDUCvCpBbQEiCbDdAzQBUAUCUAVQDpAjBEALIEVADQAXAFBqgDQBHAAAGAdQAHABACABQAEADAAAPQAAACgGAGIgDACQAMBpgDCoIgBANIgEA6QgLCfgUCWIAAABIgBACIAAABIgDApIgBAJIgDABQgcCzgqCoQg0DShgERIgWA+Qg2CUhBCnIgVAzIgFAPQhdDliHDfQijENiNBvQpwHwoWC4QguAPgtANIgDABQgNAAgkgMg");
	this.shape_7.setTransform(230.5,228.3);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.instance,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,451.6,447);


(lib.Arrow_Butncopy2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.ARR();
	this.instance.setTransform(37.3,34.5,1,1,0,0,0,22.7,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 0, 0, 0, 0)];
	this.instance.cache(-10,-2,58,54);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("ADuFBQgOAFgNAEQgwAOg1AAQiPAAhmhmQhmhmAAiPQAAiQBmhmQBIhIBagVQABAAACgB");
	this.shape.setTransform(23.8,35.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(4,1,1).p("AjVlHQAmgJAqAAQCPAABmBmQBmBmAACPQAACQhmBnQg2A1g/Aa");
	this.shape_1.setTransform(48.3,33.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F4D96F").s().p("AihDqQhXhJgchhQgchfAjhnQAjhnBeheIADgBQAmgIAqAAQCOAABmBmQBmBmAACOQAACRhmBmQg1A1hBAaIgbAJQh5gmhShFg");
	this.shape_2.setTransform(40.7,34.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CCB55E").s().p("Ah5DxQhmhmAAiOQAAiRBmhmQBHhHBagVQhcBdgjBoQgjBnAcBeQAcBhBVBJQBSBFB7AnQgwANg1AAQiOAAhmhmg");
	this.shape_3.setTransform(22.5,35.3);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.Arrow_Butn = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.ARR();
	this.instance.setTransform(37.3,34.5,1,1,0,0,0,22.7,25.1);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 0, 0, 0, 0)];
	this.instance.cache(-10,-2,58,54);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("AjtFBQAOAFANAEQAwAOA1AAQCPAABmhmQBmhmAAiPQAAiQhmhmQhIhIhagVQgBAAgCgB");
	this.shape.setTransform(45.9,35.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(4,1,1).p("ADWlHQgmgJgqAAQiPAAhmBmQhmBmAACPQAACQBmBnQA2A1A/Aa");
	this.shape_1.setTransform(21.4,33.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F4D96F").s().p("AhEFMQhBgag1g1QhmhmAAiRQAAiOBmhmQBmhmCOAAQAqAAAmAIIADABQBeBeAjBnQAjBngcBfQgcBhhXBJQhSBFh5AmIgbgJg");
	this.shape_2.setTransform(29,34.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CCB55E").s().p("AjfFKQB7gnBShFQBVhJAchhQAchegjhnQgjhohchdQBaAVBHBHQBmBmAACRQAACOhmBmQhmBmiOAAQg1AAgwgNg");
	this.shape_3.setTransform(47.2,35.3);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,73.7,73.7);


(lib.WinnersCupMC = function() {
	this.initialize();

	// Layer 1
	this.close_btn = new lib.CloseX();
	this.close_btn.setTransform(837.7,149,0.712,0.712,0,0,180,34.9,34.9);
	new cjs.ButtonHelper(this.close_btn, 0, 1, 2, false, new lib.CloseX(), 3);

	this.instance = new lib.TheCounter();
	this.instance.setTransform(155.4,283.6,0.151,0.151,10.8,0,0,221.5,219);

	this.instance_1 = new lib.BadgeYard();
	this.instance_1.setTransform(390.4,283.6,0.151,0.151,10.8,0,0,221.2,219.1);

	this.instance_2 = new lib.BadgeDeliveryTruck();
	this.instance_2.setTransform(312.7,283.7,0.151,0.151,10.8,0,0,221.5,218.7);

	this.instance_3 = new lib.Badge();
	this.instance_3.setTransform(233.7,283.5,0.151,0.151,10.8,0,0,220.8,218.5);

	this.twitter_btn = new lib.TW_butn();
	this.twitter_btn.setTransform(833,445,1,1,0,0,0,19.2,19.2);
	new cjs.ButtonHelper(this.twitter_btn, 0, 1, 1);

	this.facebook_btn = new lib.FB_butn();
	this.facebook_btn.setTransform(832.9,394,1,1,0,0,0,19.1,19.1);
	new cjs.ButtonHelper(this.facebook_btn, 0, 1, 1);

	this.text = new cjs.Text("you've earned a\nyoung grafters\nwinner's cup!", "45px 'Laffayette Comic Pro'");
	this.text.lineHeight = 62;
	this.text.lineWidth = 416;
	this.text.setTransform(126.4,331.4,0.77,0.77);

	this.text_1 = new cjs.Text("YOU HAVE ALL\nFOUR BADGES", "49px 'VAGRounded'");
	this.text_1.lineHeight = 43;
	this.text_1.lineWidth = 358;
	this.text_1.setTransform(126.4,156.6,0.75,0.75);

	this.instance_4 = new lib.WinnersCup();
	this.instance_4.setTransform(601.4,297,0.78,0.78,3,0,0,221.1,218.8);

	this.instance_5 = new lib.EndWhiteBox();
	this.instance_5.setTransform(482.4,288.3,1.082,1.05,0,0,0,365.2,185.9);
	this.instance_5.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.addChild(this.instance_5,this.instance_4,this.text_1,this.text,this.facebook_btn,this.twitter_btn,this.instance_3,this.instance_2,this.instance_1,this.instance,this.close_btn);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(83.1,111,799,379);


(lib.Truck = function() {
	this.initialize();

	// Layer 3
	this.debug_txt = new cjs.Text("", "14px 'VAGRounded'", "#FFFFFF");
	this.debug_txt.name = "debug_txt";
	this.debug_txt.textAlign = "center";
	this.debug_txt.lineHeight = 16;
	this.debug_txt.lineWidth = 40;
	this.debug_txt.setTransform(-21.3,257.6);

	// door
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9E9E9E").s().p("AD6JZQgMgHgIgDIgdgIInPAAIAFhzIAEhkIgIinIgJioQAAhBAOi/IANjBQAHhFACglQABgugNghIAKACQAVAFAGACIFcAEIALAFIB5AAIAAF9IgSEpQgJCrAACBIgIBqQgDBUATARIgCgBg");
	this.shape.setTransform(-26.1,185.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("ADrJ1IgTgIInOgEIAAACIgFADIgKADIgBgJQgKgEgGgGQAAgFAEgHQAEgGAFgDQgEgkgBgyIAAiCQgQk/AAgWQAAiXAMjjQALjUANgyQgDgGAAgHQAGgGALgBQAOgCAgACIFcAFQAKAEB4AEIAAAjIh5AAIgLgEIlcgEQgGgDgVgEIgKgCQANAggBAuQgCAlgHBFIgNDBQgOC/AABCIAJCnIAICnIgEBkIgFB0IHPAAIAdAHQAIADAMAIIgOgDQAHAKARAPQgDALgNAAQgNAAgNgFg");
	this.shape_1.setTransform(-27.8,185.2);

	// lorry
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6B3855").s().p("AicCEQAGg4AFhcQAGhVAAgTIAAg2QAPAMAuASQAzAUAbASQAoAZAmAbIAvAhQAJAGAIAMQAJALAGAGIiwBWIhTAoQglAUgRAOg");
	this.shape_2.setTransform(296.9,98.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4F2A3F").s().p("ABPHWQgNgUgYgMIgrAAQgTgIgUgCQgLgMgUgHQgSgHgTgCQhTgRg/A0IACg8IABgSQAlgRCzhiQDFhuBag5QAFA7AAAoIgIBMQgJBFAAAhQgNApg/A3QgsAjgXAVIgSgigAj4EGQBegzCFhGQB6hDCMhTIAIA9QhwAjjSB8Qh/BKgyAhIACg4gAkABdIAAmkIA/ghQA+ghA/geQA2gZAZgPIBHgoQANAOAmA3QATAcAKAYIATA8QAQA0ANBDQAIApANBQIAHAyQhhAdjPBwQh/BGg3AlQgBhSgHgpgAARm1IgBAOIACACQgfAJhEAmQhZAxAAAKQAAAPAFAEIACACQgEAVgCAeQgBAPAABLQAAB0AGAyIAAACQACACANAAIAGAAIAHAAQD7hiAoggIABgBIAFgDIAAgCIACgEIgBgLIABgwQAAhSgbg/QgkhVhQgfQgCAFgBABg");
	this.shape_3.setTransform(306.9,152.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#878787").s().p("AiiCVQAAgBAAgBQAAAAAAgBQAAAAgBAAQAAgBAAAAIgBgFIFojAQgCAPiPBaQiXBjgyAAQgNAAABgDgAi/A5QAGgKAAgKIF2i8IAHAAIAAAbQiGBKgvAYQhpAzhpAng");
	this.shape_4.setTransform(197,265.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#595959").s().p("ArNHYQgLgNgDgGQgDgHAAgMQAAgUAXgrQAYgvAVABQAfgFAPAWQAIANAAAPQAAAbgSAeQgYAngqAGgAJ/k5IgTgTQgBgCAAgNQAAgUAIgUIAVgrQAagpAfgBQANABAIAHQAJAHAAANQAAAWgTAwQgYA9gdABg");
	this.shape_5.setTransform(227.3,258.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#848484").s().p("ACaB0QiThPhDglQjbh6hhhmIAEgVQCiBmB1A0QAnARDsCXQB+BQBFAeQgEAJAAALIABAnQhYg6iEhIg");
	this.shape_6.setTransform(58.9,287);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#A8A8A8").s().p("AlwD2IgBgJIAWAAQB+hGCShEQAfgRAYgVQAHAIAHADQAEABAAAGIAAABIlrDCIgDgcgAmCCmQAAgFgGgKIFAirIAEgCIAGADIA1ADIl4C8IgBgGgAAQCQQgMgFgOgCQAognAAgWQAAgdgDgNIgWgBQARgWAIgXQABgEgEgLQgFgOgJgNIgWAAQDxiECZhXQgCALAAANIgHApQgGAfAEAZQAIBDALAaQgkAPg6AlIh+BPQhmBBgdAVQgPgKgLgEg");
	this.shape_7.setTransform(217,252.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#AAAA9A").s().p("AgzhDIAtAqQAbAXAfAHQg1AmgsAZQgGgtAAhag");
	this.shape_8.setTransform(298.4,145);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#333333").s().p("Ar7IpQgVgbgFgNQgGgOgBgYQAAgqAthXQAyhhAsABQA/gJAeAtQARAcAAAeQABA3gmA9QgyBOhUAPgArFF2QgXArAAAUQAAAMADAHQADAGAKANIAWAAQApgHAZgmQASgeAAgbQAAgPgJgNQgOgWgfAEIAAAAQgVAAgYAvgAJZjkQgigigFgGQgCgDAAgbQAAgpARgoIAqhYQA3hSA/gCQAagBARAQQAQAPAAAaQABAsgmBiQgwB+g9AAgAKJmrIgUArQgJAUABATQAAAOABABIASAUIAZAAQAdAAAYg+QATgvAAgWQAAgNgJgHQgIgIgNAAQgfABgbApg");
	this.shape_9.setTransform(227.2,258.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#111111").s().p("AsDIeQgZgjAAgvQALg6Ahg6QAPgcATgVQAyg7BIgGIAJgBQg3AagoA2Qg6BQAABuIABAZQADAaAHAUIAHAQQgfgPgSgdgAJUjmQgYgwgCglQgBgmAvhaIAGgJIAVglIAMgTIAGgJQAigyAegRIBHAAIgCABQhKAegxBNQg4BYAAB0IACA+QABALgDAAQgFAAgOgfg");
	this.shape_10.setTransform(215.6,255.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("Aj6CcQAygiB/hKQDSh5BwgjIADAWQhaA6jFBsQizBiglARIABgngAj4BeIAAgoQA3glB/hEQDPhyBhgdIADAXQiMBVh6BDQiFBDheAzg");
	this.shape_11.setTransform(306.9,169.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AjeS4QhDgkgdgvQgYgmAAg0QAAggATg3QAJgYAKgVIkyCfIgBAKIgBABIADgjQCKhTDQh1IAEgEQA4hCBIgDIDtiEQKXlqEeifQAMgdAOgaIABgBQALgUANgRQgLhIAChaQALipAAhVIAAmqQAAiPAFhLQAFhOAMg8IA3gcIgDAMIgBABQElBMCAExQBbDbAAE7QAABxgnBkQg2CJhpACIgDgBIgKABQgNAAgJgHQABANAAAPQAABXgoBgQg/CUh/AAQgMABgLgCQgDACgLAAQhkAAgthSQgbgxAAgyQAAgXAOgyIABgDQgzAghMArQAJAggCBCQgCBHAEAXIAAAnIgBABQABAFAAAHQjrCriGA7IgMgBQgGABgMAAQg1AAgCgbQgkAcg2AjQieBlhDAAQgmAAgIgfIAAgCIgBgkQAAgGAEgLIgMAAQgOgNgHgNQgGgJAAgEIAAgBQABgOAIgOIiJBKQAKADAKAEQA3gJArAeQA6AnAABbQAABHgsBOQhGB9iOAAIgCAAIAAABgAhrMgQhIAFgzA7QgSAWgQAbQggA6gLA6QAAAvAYAkQATAcAfAPIgHgPQgIgVgCgaIgBgYQAAhvA5hPQAog3A4gZIgJABgAiSOsQgtBXAAAqQAAAYAGAOQAFAMAWAbIAsAAQBUgOAwhPQAmg8AAg4QAAgdgSgcQgegtg9AJIgBAAQgrAAgxBggACFMLIADAcIABAFQABAAAAABQAAAAAAAAQAAABAAAAQAAABAAABQAAADAMAAQAyAACZhjQCQhcACgPIAAgBQAAgGgEgCQgHgDgIgHQgXAUggARQiRBFh+BFIgWAAIABAKgAGtH/IlACtQAFAJABAGIABAFQgBAKgFAKIgFAHQBqgnBqg1QAwgYCGhKIAAgbIgIAAIg0gCIgGgDIgEACgAHsHdIAYAAQAJAMAFAPQAEALgBAEQgIAYgRAXIAWABQADANAAAdQAAAWgqAmQAQACAMAFQALAEAPALQAdgVBmhCIB+hRQA6glAjgPQgKgagJhDQgDgZAGgeIAGgqQAAgMACgLQiYBXjzCEgAR+AzIgGAJIgMATIgWAlIgFAJQgwBaACAlQACAlAXAxQAXAxgBgdIgCg+QAAh1A4hYQAxhMBKgdIACgBIhHAAQgeAQgiAygAU9AoQg/ADg3BSIgqBYQgRAoAAApQAAAbADADQAEAGAiAhIAyABQA8ABAwh/QAmhhAAgsQAAgagRgQQgQgPgZAAIgCAAgAULg5QATACASAGQAUAIAMAMQAUABASAJIAtAAQAZAMAMASIATAiQAXgVAsghQA/g4ANgpQAAggAIhFIAJhMQAAgogGg8IgCgWIgIg/IgDgXIgIgyQgMhQgIgpQgNhDgQg0IgUg9QgJgYgTgbQgng4gNgNIhGAnQgcAPg1AaQg/Aeg+AhIhAAhIAAGlQAHAqACBSIAAAoIAAAFIgCA5IgBAnIgBASIgCA8QAvgnA6AAQAUAAAUAEgASIxiQAAATgFBVQgGBegGA4IAAAqQASgOAkgUIBUgoICyhWQgGgGgKgNQgIgMgJgGIgughQgmgbgqgZQgbgSgzgUQgvgSgPgMIAAA2gAxeOlImAjwQAAgIAHgUIADgGIgCgHQAAgVAEgIQABgFAEgFQh8hPgzgqIgBAAIgCgCQgUgRgIgLIAQADQgTgQADhVIAIhqQAAiBAJipIASkrIAAmkIAiAgIAAGMIgRElQgJCsAAB+IAAC7QAAANgCACIgEACIACADQAAAAAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAIgFAAIADABIAIACQAUAGAVAKQAUAJAWAMQAoAYBPA7QBTA/AvAdQBQAxBQAZQB+AyCMBaQCuBvBCAoIABACIgHAjIgNgEIABAbIAAAnQgGAagdAAQgZAAmGjwgA2uKQQBgBmDcB7QBFAmCTBPQCEBJBYA5IgCgnQAAgLAFgIQhFgeh+hRQjuiYgngRQh1g1iihlIgEAUgATom4IgFAAQgNAAgDgCIABgDQgGg0AAhzQAAhLABgPQABgfAFgVIgDgBQgEgFAAgOQAAgLBZgxQBDglAhgJIgBgCIAAgOQACgBACgGQBQAgAkBUQAaBAAABSIgBAwIABAKIgBAFIgBACIgFADIAAAAQgoAhj9BkIgIAAgAT2nUQAsgaA3glQgfgKgdgWIgtgrQAABdAGAtgAVXs2QhRAwgfAUQAIArABA/QAWAAAzAgQAdATAgAZIASAOQA1gbA+gPQgGgUgBgbIgFhRQgMgwgug6IgbgcIhDAog");
	this.shape_12.setTransform(166.7,198.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#99CC00").s().p("AmCHqIgCgCIACACgAGEnnIgBgCIACACg");
	this.shape_13.setTransform(-38.4,296.9);

	// guides
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("rgba(255,255,0,0.608)").ss(1,1,1).p("AjFnSIB6BLAosABICTBaAkQCwICRBZAsFiDIBQAxAJyApICVBcAFYiDICQBZAAIFeIC+B1AA8kyICSBa");
	this.shape_14.setTransform(144.2,224.7);

	// bottom layer
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#A8A8A8").s().p("AtxJTIAEgCQADgCAAgNIAAi7QAAh+AJiuIAQkjIAAmMIAfATQBDAqBjBGQCEBdBYAwQB9BED4CQQCcBaASAFQgCALgBALIAAAOQgcAUgKAMQhtArhrBEQhSA1iyB4QgtAchUAuQhLAphqA4Qh2A9goAZIgIAGgACWgYIgCgIQD0iQHnklIADAfQhiA6lZDHQjBBwhdA9IgDgQg");
	this.shape_15.setTransform(89.4,185.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#C6C6C6").s().p("AmCMRQhDgpiuhvQiMhah9gyQhRgZhQgxQgugchUhAQhOg7gpgXQgVgNgVgJQBZgdDviNIB9hKQDFh4ERitIABAAQAFAAATgIIAAAYQgNAggDAzQgCAVABBFQgBEbAjElIAAFSIgHgEgAlYJnQgBhSgRlEIgRlGQAAgtAMhZQAIg1ADgnIABgSQBqg4ECiYQAsgbC+hrQBog6AkgbIAOAMIACACIAAABQA0AqB8BPQDFB7B1A1QAmARDvCYQBhA/BSAnIgBABQgOAagLAdQkfCcqXFrIjuCDQhJAEg4BBIgCAFQjQB1iKBTQAChAAAhgg");
	this.shape_16.setTransform(140.4,225.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#848484").s().p("AD9HUQjBhxg2gWQgkgLhqg+Qhlg6gtgiQhPg8gogXQgWgNgUgJIgagKIgSgGIgFgBIgDgBIAFAAQABAAAAgBQABAAAAAAQAAAAAAAAQABgBAAAAIgDgCIAEgCQACgCAAgJIAAgEIAAhFIBYg1QAVgNDJhtIEGiQQFBiyBWg5QgLA9gFBNQgFBLAACQIAAGnQAABVgLCqQgCBaALBKQhVg1iGhPg");
	this.shape_17.setTransform(230.2,140.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("A1cIeIgIgDIABgBQABAAAAAAQABAAAAAAQAAgBABAAQAAAAAAAAIAIgFQAogZB2g+QBqg4BLgpQBUguAtgcQCyh4BSg1QBrhEBvgpQAKgMAcgUIAAAxQgTAJgEAAIgCAAQkQCtjGB4Ih9BLQjvCNhZAeQgVgJgTgGgASaAZQjuiWgngRQh1g1jFh7Qh8hPgzgrIgBAAIgCgCIgOgNQgkAchnA6Qi+BrgtAbQkCCYhpA4IgCABIACgcIAAgBIgBgNQBdg+DBhvQFXjHBig7IgDgfIArgaIAABHIAAAEQAAAKgCABIgEACIACADQAAAAAAAAQAAAAAAABQgBAAAAAAQgBAAAAAAIgFABIADAAIAFACIASAGIAaAKQAUAJAWAMQAoAYBPA7QAsAiBmA7QBqA9AmAMQA2AVDBByQCGBNBUA0QgMARgMAVQhSgnhhg/g");
	this.shape_18.setTransform(139.2,191.8);

	// shadow
	this.hit_mc = new lib.lorry_shadow();
	this.hit_mc.setTransform(168.5,247,1,1,0,0,0,157.1,86.8);

	// floor
	this.loading_mc = new lib.lorry_loading_area();
	this.loading_mc.setTransform(256.7,310,1,1,0,0,0,133.4,76.2);

	this.addChild(this.loading_mc,this.hit_mc,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.debug_txt);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-77.4,78,467.6,308.4);


(lib.Sure_you_wanna_quit = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_23 = function() {
		this.stop();
	}
	this.frame_29 = function() {
		if (quitConfirm) quitGame();
		this.gotoAndStop(0);
		this.visible = false;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(23).call(this.frame_23).wait(6).call(this.frame_29).wait(1));

	// NoBtn
	this.no_btn = new lib.NoBtn();
	this.no_btn.setTransform(609.6,94.4,0.17,0.17,0,0,0,85.2,31.8);
	this.no_btn._off = true;
	new cjs.ButtonHelper(this.no_btn, 0, 1, 2, false, new lib.NoBtn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.no_btn).wait(1).to({_off:false},0).to({regX:85,regY:31.7,scaleX:1,scaleY:1,x:236.3,y:250.3},4).wait(20).to({regX:85.2,regY:31.8,scaleX:0.17,scaleY:0.17,x:609.6,y:94.4},4).wait(1));

	// YesBtn
	this.yes_btn = new lib.YesBtn();
	this.yes_btn.setTransform(647.3,94.4,0.17,0.17,0,0,0,85.2,31.8);
	this.yes_btn._off = true;
	new cjs.ButtonHelper(this.yes_btn, 0, 1, 2, false, new lib.YesBtn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.yes_btn).wait(1).to({_off:false},0).to({regX:85,regY:31.7,scaleX:1,scaleY:1,x:458.3,y:250.3},4).wait(20).to({regX:85.2,regY:31.8,scaleX:0.17,scaleY:0.17,x:647.3,y:94.4},4).wait(1));

	// Sure
	this.instance = new lib.Sure();
	this.instance.setTransform(685.9,62.5,0.17,0.17,0,0,0,481.6,-167.3);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({regX:481.8,scaleX:1,scaleY:1},4).wait(20).to({regX:481.6,scaleX:0.17,scaleY:0.17},4).wait(1));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AjGCqIAAlTIGNAAIAAFTg");
	this.shape.setTransform(800,126);

	this.mc_bg = new lib._50Percent_white();
	this.mc_bg.setTransform(281,263.4,1,1,0,0,0,500,285.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.mc_bg}]},1).wait(29));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(780,109,40,34);


(lib.Steve_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 3
	this.instance = new lib.HardHat();
	this.instance.setTransform(91.9,33.6,1.89,1.89,0,0,0,43.1,29.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({_off:true},3).wait(1));

	// Layer 2
	this.instance_1 = new lib.Male_Head_01_Front();
	this.instance_1.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5));

	// Layer 1
	this.instance_2 = new lib.Male_Body_02_Front();
	this.instance_2.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_3 = new lib.YardSteve_01();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.YardSteve_02();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.YardSteve_03();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_6 = new lib.YardSteve04copy3();
	this.instance_6.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.PeppaOptionsYard = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 3
	this.instance = new lib.HardHatWhite();
	this.instance.setTransform(100.2,84.1,2.17,2.354,0,-3.3,0,43.1,29.6);

	this.instance_1 = new lib.HardHat();
	this.instance_1.setTransform(100.2,84.1,2.17,2.354,0,-3.3,0,43.1,29.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance_1}]},1).to({state:[]},2).wait(1));

	// Layer 1
	this.instance_2 = new lib.Peppa_Head();
	this.instance_2.setTransform(89.1,123.5,1,1,0,0,0,77.4,85.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 2
	this.instance_3 = new lib.PeppaClothes();
	this.instance_3.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_4 = new lib.FemaleYard_03();
	this.instance_4.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_5 = new lib.FemaleYard_02();
	this.instance_5.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_6 = new lib.PeppaYard1();
	this.instance_6.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_7 = new lib.JaneSkirt4();
	this.instance_7.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3}]}).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(11.7,38.3,159.9,318.6);


(lib.PanelStrip = function() {
	this.initialize();

	// Layer 3
	this.lower_btn = new lib.btn_lower();
	this.lower_btn.setTransform(35.3,417.6,1,1,0,0,0,19.1,23.9);
	new cjs.ButtonHelper(this.lower_btn, 0, 1, 2, false, new lib.btn_lower(), 3);

	this.lift_btn = new lib.btn_lift();
	this.lift_btn.setTransform(35.3,344.6,1,1,0,0,0,19.1,23.9);
	new cjs.ButtonHelper(this.lift_btn, 0, 1, 2, false, new lib.btn_lift(), 3);

	this.stop_btn = new lib.btn_stop();
	this.stop_btn.setTransform(35.3,284.6,1,1,0,0,0,19.1,23.9);
	new cjs.ButtonHelper(this.stop_btn, 0, 1, 2, false, new lib.btn_stop(), 3);

	this.reverse_btn = new lib.btn_reverse();
	this.reverse_btn.setTransform(35.3,219.6,1,1,0,0,0,19.1,23.9);
	new cjs.ButtonHelper(this.reverse_btn, 0, 1, 2, false, new lib.btn_reverse(), 3);

	this.forward_btn = new lib.btn_forward();
	this.forward_btn.setTransform(35.3,159.6,1,1,0,0,0,19.1,23.9);
	new cjs.ButtonHelper(this.forward_btn, 0, 1, 2, false, new lib.btn_forward(), 3);

	this.turnL_btn = new lib.btn_turnL();
	this.turnL_btn.setTransform(35.3,91.6,1,1,0,0,0,19.1,23.9);
	new cjs.ButtonHelper(this.turnL_btn, 0, 1, 2, false, new lib.btn_turnL(), 3);

	this.turnR_btn = new lib.btn_turnR();
	this.turnR_btn.setTransform(35.3,21.6,1,1,0,0,0,19.1,23.9);
	new cjs.ButtonHelper(this.turnR_btn, 0, 1, 2, false, new lib.btn_turnR(), 3);

	// Layer 2
	this.instance = new lib.ControlPanel();
	this.instance.setTransform(33.3,154.7,1,1,0,0,180,33.4,142.7);

	this.addChild(this.instance,this.turnR_btn,this.turnL_btn,this.forward_btn,this.reverse_btn,this.stop_btn,this.lift_btn,this.lower_btn);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.3,-22,67,492.4);


(lib.pallette_mc = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{Bricks:2,ConcreteLintels:3,TopSoil:4,Fabrics:5,PavingStones:6,SandBags:7,Planters:8,Posts:9,Decking:10});

	// debug
	this.debug_txt = new cjs.Text("", "14px 'VAGRounded'", "#FFFFFF");
	this.debug_txt.name = "debug_txt";
	this.debug_txt.textAlign = "center";
	this.debug_txt.lineHeight = 16;
	this.debug_txt.lineWidth = 40;
	this.debug_txt.setTransform(-2.4,2.6);

	this.timeline.addTween(cjs.Tween.get(this.debug_txt).wait(15));

	// button
	this.btn1 = new lib.pallet_half_btn();
	this.btn1.setTransform(0.7,10.7,1,1,0,0,0,48.7,41.6);
	this.btn1._off = true;
	new cjs.ButtonHelper(this.btn1, 0, 1, 2, false, new lib.pallet_half_btn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.btn1).wait(2).to({_off:false},0).wait(3).to({scaleY:1.28,y:-1.7},0).wait(10));

	// highlight
	this.highlight_mc = new lib.pallet_highlight();
	this.highlight_mc.setTransform(0.7,10.7,1,1,0,0,0,48.7,41.6);

	this.highlightB_mc = new lib.pallet_highlight_full();
	this.highlightB_mc.setTransform(0.7,-2,1,1,0,0,0,48.7,54.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.highlight_mc}]},2).to({state:[{t:this.highlightB_mc}]},3).wait(10));

	// Layer 3
	this.instance = new lib.bricks();
	this.instance.setTransform(1.1,11.5,1,1,0,0,0,42.1,33.2);

	this.instance_1 = new lib.ConcreteLintelsFlipped();
	this.instance_1.setTransform(-0.2,4.4,1,1,0,0,180,45.1,29.6);

	this.instance_2 = new lib.ConcreteLintels();
	this.instance_2.setTransform(-0.1,18.4,1,1,0,0,0,45.1,29.6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AgtDoIgXgMQACgeAEgKQADgDAFgFIAgALIAFACIASAVQAbAhAPAiQgfgJg5gggAgyB4IgJgIQAEgYANgLIAKgFQBCAlAJA7QgrgHgygpgAEKB5QAagRADgLIAAAOQAAAQgBAQIgFAAQgJgPgOgDgAkRBmQgJgEgMgLQAJgWAIgKIAKgHQAUAOAPAJIAXAWQASATAMAEQhCAAgcgOgAgNAgIgIgGQAKgUAQgQQA2AjALA4QgcgMg3glgAj+AkQgKgEgHgGQAAghAZgcIADgCQAUAJAWAOQAjATALARQgHAJAAASIAAALQg6gIgigQgAEYA3QgWgrgfABIgFABQATgRAAgHQAAgGgGgMIAIABQAhAeAEA0gAjYhYIgJgEQAHgSAMgQQA5AeAEAoIAAABIhHghgADwhBQgXgIgKAAIgBAAQgQgVgUgSIgGgGIAKgRQBJAjAUAmIgDAEIgYgHgAAHj2IgQgHIAGgTQAsAVAPAlQgRgSgggOg");
	this.shape.setTransform(-6.2,14.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CEAF37").s().p("AjPADIABgBIAAABgADLAAIADgCIACADIgFgBg");
	this.shape_1.setTransform(-0.1,8.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC0000").s().p("AgaDhQg5gbgoAEIAEgDQATgTAOgHQAWAVAsAaIAIAEIgIAFIgGgEgAk/CpIgLgBQAGgEAGgBIAFgCIASAOQgIgEgQgCgAAJCLQg1gigmAJQABgMACgDQAFgHAvgdQALgCAogeQArgZAdAOQAQAHAoApQhDAZgbARQgYAPgRATIgIgFgAkHBoQgngMgSgCIgLAAQAMgOAVgdIAAgBIA1AlIAfAUIgRAOQgRgIgPgFgAjXASQgvgTghgCQAOgOAfgYQBBg0A4gBQAVgBAWAFQAMAMAZgBIABAAQALAFAKAGQhPAegcAQQglAWgWAcIgWgKgAC6ATQgpgSgbgGQABgmArgaQAXgOAlgUIAygBQAdALAfAOQgsAPgkAXQgqAbgYAigAACh1QgigPgkgBIgBgKQAAgTAbgbQAmglA6gBQAsgBAjAaQgzAGgTAKQglATgTAzIgFgBg");
	this.shape_2.setTransform(-7.4,-1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#930400").s().p("AgTDkIgBAAQgXgRAAgLQAAgKAOgNIAIgHIADACQASAVAlAPQgGAEgCAEQgFAJgCAeQgZgPgQgMgAgrBRQAogEA3AbIAGAEIgJAEQgOALgDAYQg1gygWgQgAkJA+QAGgKAJgFIALABQAQACAIAEIADACIAmAbIgKAIQgJAJgJAXQgYgUgngpgAjCAxIgaghQgRgSgcgOQAEgBAKgMIALAAQASACAnAMQAPAFARAIIgDACQgYAagBAkQgJgGgGgHgAgDAHIABgMQAkgJA3AgIAIAFQgRASgLAUQhIgvAAgHgAjhhuQAAgEAKgKQAhACAvAVIAWAKQgMAQgIASQhcgsAAgJgADFh8IABgBIAAgBQAbAIApASIAAABIgKAQQgfgdgcgMgAAWjpIgKgKQgCgCAAgJQAkABAkAPIAFABIgGATQgcgKgfgFg");
	this.shape_3.setTransform(-15.4,11.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#AAAAAA").s().p("AgjCbIgNgPQAGgDAMgLQAMgMAEgBIARAAIARANQAJAGAAAIIgvAaQgHAAgKgLgAjsAgQgKgKAAgCQAAgMANgIIAfgVIAQAAIAQALQAKAHAAADIg4AsIgBAAQgHAAgMgMgACtARQgPgLgDgBQAFgFAOgKIAZgRQANgKAMAKIAXAUIAAACIgwAhQgMAAgOgLgAgfhxQgGAAgKgJQgMgLgCgBQAFgIAMgKIADgCIAHgEIAQgGQASADADAHIAHAPIAAADIgKADIAAAAIgbAPIgDAFgAAAiHIABgBIgBAAIAAABgAgYilIABAAIgCAAg");
	this.shape_4.setTransform(4.5,-2.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F7F7F7").s().p("AhEE8QgPgigbghIgUgVQAqAMAhABQAyAEBegvIAogUQAFAKAPAJQAIAFAEAJIgZgBQhFAChDArQgzAhgOAdIgDgBgAhEDTIAAgBQgJg7hEgkIAIgEQAtAbAWATIACAAQAZAHAIAAIAFgBQAlgTArgeQAsgeAAgQIAAAGQAIAEAXAVQAXAWAHAKIgBAFQgVgBgfACQgeACggAIQghAIhKA4IgBAAgAC1C1QABgPAAgQIAAgOIAAgDIgFgNIgGgOQAIACAMAAQBigCA2gmIAOARIgHAAQgFAGgkALQgoAMgOAJQgTANgXAQQgYATgHAKgAlCCHIgXgVQAXAOANAFIAvgBQAFAJAIAJQgHADgFAGIgBAAIgeAAQgMgFgSgTgAhqAfQARgUAXgMQAcgRBBgaIAUAVIA0AzQg8ACgoAbQgsAdAEAmQgLg4g2glgAgjAAQgNAKgGADIANAPQAKALAIAAIAugaQAAgIgJgFIgPgMIgTAAQgEABgLALgACCAOQANABAPgBQBPgBA/gwQATgPAMgNQABAKAOAQIAFAFQgeAJhCAnQhVA2AAAbIgDABQgEg0ghgggAk6AbQgWgOgUgJIARgNQBEAoAegBIAJgBQACAJAIAKQgdABgNALIgEAEQgLgQgjgVgAk/hTQAXgeAlgWQAbgRBQgeQAOAHAOAKQAUAPAcAaIgKAAQgxACgxAkQgcAWguAzQgEgog5gegAjvh+QgNAJAAAMQAAACALAKQAMAMAHAAIA4gsQAAgFgKgHIgQgLIgQAAIgfAWgAA9hcQAYgjAqgcQAjgXAtgOQAVAKAXANQAvAaARAVQgmAJhDAqQgZAOgRATIgOATQgUgnhJgigADBiaIgZARQgOAKgFAHQADABAPALQAOALAMAAIAxgjIgBgCIgWgUQgGgFgHAAQgGAAgHAFgAg3irQgPgmgugUQATg0AlgSQATgKAxgHQARANAPATIAbAlIgOgCIgRgCQgUABgIAGQgGADghAiQgNANgGAcIAAABIgFgGgAgokdIgHAEIgDACQgMAKgFAIQACABAMALQAKAJAGAAIABAAIAEgFIAbgPIABAAIAIgDIAAgDIgFgPQgEgHgSgDIgRAGgAgfkkIACAAIgBAAgAgFkHIACAAIgCABIAAgBg");
	this.shape_5.setTransform(5.1,10.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#2D260C").s().p("AhZFKQgYgOgbgVIgggYIgOgLIgPgOQgQgPgDgJIgCgGIACgMQgOAGgIABQg6ABgvgYQgWgLgTgRQgigjgLgTQgLgSAAgFQAAgYAKgHQAGgFAXgGQgnggAAgJQAAgKAVgaQAWgZAQgGQgTgPAAgFQAAhABCgkQA5gfBXAAIAbAAIAAgbQAAgqApghQAogfA5gGQA7gGAzAcQA5AgAdBFQAlgCAnAOQAQAMAhATIBDAiQAvAXAAApQAAAPgQAUQAagGAQAbQANAVAAAPQAAAfgZAcQAJAEAHAMQAJAPAAATQAAApgnARIgcALIgVAIQgkAOgfATIgwABIABAKQAAADgLAJQgKAIgQALIg9AmQhnA8goABIgCAAQgkAAg1gdgAi0DDQgOANAAAKQAAALAXARIAAAAQAQAMAcAPIAWAMQA8AgAeAJIAEABQAKADAEAAQAYgBBZgrQBPgmAVgPQAAgHgCgFQgDgIgIgFQgPgJgFgLIgoAVQhhAugvgDQghgCgqgMIgFgCIgggKQglgPgUgVIgCgCIgIAHgAi/BgIgDADQAVAQA3AyIAJAIQAzApArAHIABAAIABAAQAIACAFgBQAiAABVgjIBYgmIABgCIABgGQgHgKgYgWQgWgUgIgEIAAgGQAAAQgsAeQgrAegnATIgFAAQgIAAgXgGIgDgBQgVgTgtgbIgJgEQgsgagWgVQgNAHgUATgADnCCIAFAOIAAACQgDALgaARQAOAEAJAOIAEAAIACAAIAogEQgHgBBIgbQBIgbACgNQADgOgGgJIgBgBIgOgSQg2AmhiADQgMAAgIgDIAGAOgAmFA8QgHABgFAEQgJAFgHAKQAoApAYAUQAMAKAJAEQAcAOBCABIAegBIABAAQAFgGAGgCQgHgKgFgJIgvABQgNgFgXgNQgPgKgUgOIgmgbIgCgCIgSgOIgFACgAguhGQgpAegLACQgvAdgFAHQgDACgBALIAAAOQAAAHBJAvIAJAGQA5AmAZAMQANAFAGgBQASgBA6guQA6gtgEgDIgHgGIg0gzIgUgUQgngpgQgHQgLgFgMAAQgUAAgcAQgAlxg2QgVAdgMAOQgLALgEAAQAcAOARAUIAaAhQAGAHAJAGQAIAFAKAEQAiAQA6AIIgBgLQAAgSAIgJIAEgFQANgKAdgCQgIgJgDgJIgIAAQgfABhEgpIgegSIg1glIAAABgADdBsIAAAAIACgBQA5gXBRgbQA5gWAAgiQAAgGgOgRIgEgFQgOgQgBgKQgMANgTAPQg/AwhPACQgPAAgNgBIgIgBQAGAMAAAGQAAAIgTASIAFgBIABAAQAeAAAWAqgAjJjFQg4ABhBA0QgfAYgNAOQgKAKAAAEQAAAJBcAsIAJAEIBGAhIABAAIAaAJQAGgjAZgSICAg+IACgBIgFgFQgcgagVgPQgNgJgOgHQgLgGgLgFIAAAAQgaABgMgMQgTgEgTAAIgFAAgACWjOQglAUgXAOQgqAagBAmIAAABIgBABQAbAMAgAdIAGAGQAUATAQAUIABAAQAJAAAXAIIAZAHIAFABIACAAQAugBA/glQA5giAAgNQAAgKgJgMQgRgVgvgbQgXgNgWgKQgegOgdgLgAgPlKQg9ABglAlQgbAbAAATIAAAKQAAAJACACIAKAKQAgAFAbAKIATAHQAgAOARASIADAGQAMARAAATIAAAFIALACIgCgFQgDgKAAgFQAAgOAQgVQATgXAcgRQAWgOAYgGIgGgGIgCgDIgbglQgQgTgRgNQghgZgoAAIgDAAg");
	this.shape_6.setTransform(-0.3,9.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#DDDDDD").s().p("Ah1EbQANgcA1giQBCgrBEgBIAZAAQACAFgBAGQgUAPhQAmQhWAsgYAAQgGAAgKgCgAh3CxQBMg4AhgHQAegJAegCQAegCAVACIgBACIhYAmQhSAjgiAAIgEAAIgLgBgAChB2QAWgQAUgNQANgIAogNQAkgKAFgHIAIAAIABACQAFAIgCAPQgCANhIAaQhIAcAHABIgpAEQAHgKAZgUgAheBaQgEglAtgdQApgaA6gCIAHAEQAEADg5AtQg5AtgTACIgBAAQgGAAgLgFgADLgQQBBgpAegJQAOAQAAAIQAAAjg5AUQhRAag5AYQAAgbBWg0gAk3guIAAgBQAvgzAcgVQAwglAygBIAKgBIAFAFIgCABIiAA9QgZASgHAlIgagKgABogwIgDgFIAPgTQARgTAYgOQBEgqAlgIQAKAMAAAKQAAANg5AhQg/AmguABIgCAAgAhaieIAAgFQgBgTgMgRIAAAAQAHgdANgNQAjgiAFgDQAIgGAUgBIAPACIAPADIACADIAGAGQgXAGgWANQgcARgSAYQgQAVgBAOQAAAFAEAKIACAFIgLgCg");
	this.shape_7.setTransform(10.4,13.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AjlFtQimhqgjgTQgDADgEAAQgLAAgCgCQgBACAAgMQAAgIAGgaQAGgbAAgLQgHh3gCg3IgIiwIABg2QAHAFAFAHQABgEACgDIgBAAQgCgDAAgDQAAgVDZiBQCnhjAmgDQAJgPBOAtICdBZQBXA3BAArQAnAaAUAPQADgGADAHIACAEIAIgDIACADQABACAAAGIAAADIAAACQAAAEgCADQAJAXgDALQgIAtAAAHIAICyQAAAbgDBDQgDBLgEAYQABAAABABQABAAAAAAQABABAAAAQAAAAAAABQgCAFAAADQAAAPi7BrQhjA5gzAZQg8AegZADIgDACQgCACgHAAIgBAAQgCAJgMAAQgMAAjYiKgAmnieIAABqQABBFAICNQAAAXgEAYQgCARgEAJQAUAEA2AfQArAYBfA6ICNBVQA6AkAOALIAAgaQAHgLACgtIAAhAIgJjZQgBguADgjQABgFgBgBIh2hSQhCgvgGgDIithVQhAgfgPgJQAPAZABAsgAFkigIhxA+IiOBOQg5AdgSAGIABACIAADCQACAHADAzQADAxAAARQAAAdgDArIgFA8IA7gdQBfgvA7gjIBlg/QBOgxAHgBIgDgLIgBgaIAJicIgJizQAAglAEgjQgUAOgyAbgAhDnCIhxBDQiHBUhhA5QAXAFApAVQA0AaBkA4QBCAbCEBwQACgCAFgDQADgEADACQAigWB5hHQCnhgBEgfIhzhRQg8gqgYgNQgwgZhNgtQhMgrgTgPQgLAKgqAag");
	this.shape_8.setTransform(-0.4,-1.6);

	this.instance_3 = new lib.pavingstones();
	this.instance_3.setTransform(-0.4,-1.6,1,1,0,0,0,45.7,50.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#666666").ss(1,1,1).p("ADlg8QgIgegSgMQgBgBgVgHQgEgCgEgBAD2ByQgCgFgCgEQgSgigdgMQgXgEgJgCQgEgBgBgBAj1hYQACgCABgDQAEgHACgCIAAAAQABgCAFgD");
	this.shape_9.setTransform(4.8,19);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#897524").s().p("AAyGrQgWgNhSgjQg4gYAAgRIABgEQAvAJAmAPQBBAbAcAqgAhbEAQgBgGgBgBIgBABQgCgJgBgJIAVAAQAZACAZAPQALAIAvAaQAjAUAKARQiPgCgZg+gAkXDuQgwgXAAgYQAZAFAvAaQA3AfAWAbIgSAAQgJgHhKgjgAFWEWQgdAAgPgNQgMgPgIgDIgKAAQAEgJAEgMIAgAGQAdALASAjIgNAAgABPDuQhAAAhIgrQhEgnAAgUQABgGADgGIAZgBQAeADAcAUQAPAKA5AjQAqAaAGAVIgDAAgAE3DBIgkgPIgBgCIgBgDIgCABIACgCIALgMQAKAEAPAFQAOAHAMASIgYgBgAkfCgQg2geAAgRIABgGQAdADAvATQBKAcAGAlQgzgFg0gdgAgqBgQhagwAAgVQAAgKAOgMIAJgGIACABQAcAdA8ATIAGACIASAWQAaAhAPAjQgegKg6gigAFRBuQgUgIgNgIQgNgHAAgCIgBABQgHgIgMAAIgDAAQARgKAJgIIAWAJQATAMAHAeIgFgBgAkqAiIgygcIAJgQQAYADAbAQQAlAZgBAgIAAAKQgQgXgegTgAgugOQg/g7gXgRQAogDA5AcQBIAnAJA8QgrgIgxgogAElALQgJgNgPgDQAagRADgKIAAAOQAAAQgBANgAkOgmQgYgMg8hAQAHgKAIgFIAMABQAPACAIAFIADABQAsAiAdAUIAWAVQATAUAMAEQhCgCgdgPgAgKhlQhRg3AAgIIAAgNQAmgIA1AjQA+AoALA9QgbgNg4gngAEchHQgXgrgeAAIgGAAQAUgRgBgIQABgGgGgMIAIABQAhAiAEAzgAkbh+IgaghQgRgUgcgPQAEgBAKgMIALAAQASACAnAOQAiALAoAaQAjAXALAQQgHAJAAATIABALQhfgPgegjgADzjCQgXgJgJAAIgBAAQgRgUgTgUQgjgigfgOIABgBIAAgBQAbAIApAUQBKAjATAoIgCAEIgZgIgAjVjkQhkgxgBgKQABgEAJgKQAiADAuAWQBOAjAEAvIAAABIhHgjgAALl9QgigQgqgHIgKgLQgCgCAAgJQAlACAkAQQAwAVAQAoQgRgSgggQg");
	this.shape_10.setTransform(-6.5,1.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#CEAF37").s().p("AgGHYQgcgqhDgaQgmgPgvgKQADgGAJgLQAGgGADgGQA1AsB5ABQAzgBBMgyIALgHIAAACQALAKAGANIADAIQgkgDgxAYQgtAXgSARQgVATAAAWgAAAFtQgIgQgjgVQgxgagLgHQgZgQgZgBIgVAAIgBgHQABgHACgNIABgDIAFAFQBPA8BZAAQBFAABQg7IAKgHQAQADACAUQgZACgbAKQgOAFgwAUQgaAMgUAZQgJAMgFAJIgFAAgAlMEMQgvgagZgGIAAgBQAAgFALgIIABABQBDAyBZAAQAKAAAUgXIAGgIQgCAFAAAXQgSAMgMAKQgMALgFAVIgGABQgWgbg3gegADnEWIgggHIADgJIgBgCQgHgOgRgMIAFgFIAAAIQALAHAKARQAPAMAzAAQASAABJgtIANgJIAsAXIAAACIgEAAQgoASgbAPQgqAWgNAXIgNACQgSgjgdgLgAAFEbQgFgUgpgbQg7gigPgLQgcgTgegDIgZABQABgEACgDQAIgJAAgJQAAgLgFgFQgBgCgJgDIgCACIgBgCIAFgJQASAQAbAUQBaBDA3AAQAmABBng7IAzgeIAAAGIAIAVQADAGABAIIgiAAQgSgChbBBQgeAUgFAJQgCADgEARIgFAAgAEGDwIgEAAQgMgTgOgGQgPgFgKgEQAOgQAAgLIACgJIAMAJQAhAOA3gOQAZgTA3gXQAGAAAAAJQgcAOgnAZQg0AggUAXIgIAAgAkFDwQgGgmhKgcQgvgSgdgDQAEgIAJgKIANgQQAgAWAkASIBjAAQgFALAAAOIAAACQgTAHgHAUQgCAEgDAXIgBAAgAgdC5QgPgjgaggIgUgWQAqANAhACQAvAFBggtIApgTQAFAKAPAKQAHAFAEAIIgZgBQhEAAhEAqQgzAggNAcIgEgBgAEJCcQgHgdgTgMIgWgJQAMgJgBgEIgBgJIAxAAQAfgSAkgNIAUgIIAAADQAIAJATARIAKAKIgdAAQgSgDgqAjQgkAdgHAMIgDgBgAlDCBIgGgIIAAgKQABgfglgZQgbgTgYgCIAHgMIAHgMQA8A2BWAAQAIAAAOgGIgCAMIACAFQglAIgZATQgNALgOAQgAgdBQIAAAAQgJg/hKgkQg5gdgoAEIAEgDQAUgTANgHQAWAWAsAaQA0AeAXAVIADABQAZAGAFABIAGAAQAngTArgbQAsgcAAgRIAAAGQAIAEAWAWQAYAWAGAIIgBAFQgVgCgeACQgeACggAHQghAHhKA2IgCAAgADcA4QABgPAAgPIAAgPIAAgCIgEgMIgHgOQAJACAMAAQBiABA1gmIAPASIgIAAQgFAGgkAKQgoAMgNAGQgUAMgWAQQgZASgHAKgAjeAYIgegBQgMgFgTgSIgWgVQAXAOANAGIAvAAQAEAGAIAKQgHACgFAHgAhLhpQg3gigmAHQABgMACgDQAGgGAugcQALgCAqgdQArgYAbAPQAUAJA4A9IA0A2Qg8ABgpAaQgrAcAEAmQgLg9g+gogACqhvQAMACAQAAQBOAABAgxQASgOAMgNQACALAOAPIAEAGQgeAJhBAnQhWAzAAAcIgCABQgEg0ghgigAkThsQgogagigLQgngOgSgCIgLAAQAMgOAVgdIAAgBIA2AmQBbBAAlAAIAJgBQADAKAHAJQgcABgNALIgFADQgLgPgjgXgAmUhSIgMgCQAGgDAHgBIAEgCIASAOQgIgEgPgCgAC+iNIADgDQgTgohKgkQgpgUgbgIQACgmAqgZQAXgNAlgTIAzAAQAxASA2AgQAwAcAQAWQglAHhEAoQgYAOgRATIgPATIADAEIgGgBgAjbiUIAAgBIAAABgAktjnQgugVgigEQAOgOAfgXQBBgyA4AAQAVAAAWAFQAMANAaAAIAAAAQAZAKAYASQAVAPAcAbIgKAAQgyABgwAiQgcAWgvAxQgEgvhOgjgAgPkuQgQgogygVQgkgPglgDIgBgJQAAgUAbgaQAmgjA8gBQA/AAAvA8IAbAlIgPgCIgRgCQgUAAgIAGQgFADgjAhQgLANgHAcIAAABIgEgHg");
	this.shape_11.setTransform(1.2,-2.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#2D260C").s().p("AgTH1QgfgQgrgQIg8gWQg3gXAAgqQAAgKAQgZQAJgNAGgGIgCgEQgfAdgFABIg2AAQhNgkgVgNQg5gjAAggQAAgUADgGQADgGAHgGQgbgZAAgRQAAgYADgHQAHgQAZgGQgqggAAgTQAAgVAMgUQAIgNALgIQgPgRgLgTQgLgSAAgFQAAgYAKgHQAGgFAXgFQgnghAAgLQAAgKAVgZQAWgZAQgGQgTgQAAgFQAAhABCgiQA5gdBXABIAbABIAAgbQAAgqApgfQAogfA5gEQA7gFAzAdQA5AiAdBGQAlgCAnAPQAdAYBXAsQAvAYAAApQAAAPgQATQAagFAQAcQANAVAAAPQAAAhgZAbQAJAFAHALQAJAQAAATQAAAngnAQQATAEAJAKQALAMAAAeQAAAWgJAMIADgBQAIAHADAHQACAHAAAOQAAAPgmAcQAKABAKAFQAZAMAAAVQAAAig3AmQg7AqhFAAQgsAAgSgRQgLgKAAgQQAAgHgDgDQgHAMgJAHQAVACAEAWQADANgDAOQAAAhg/AjQhiA4gIAGgAinFqQgJAKgDAHIgBAEQAAARA4AYQBUAiAWAOIASAAIADAAIAWAAQAFgFBLgsQBGgpAAgIIgBgFIgDgIQgGgMgLgLIAAgCIgLAHQhMAzgzAAQh5AAg1gtQgDAGgGAHgAihEtQAaA9COACIAGAAIADAAQAMAABGgiQBFgiASgPIABgMQgBgTgRgEIgKAIQhQA6hFAAQhYAAhPg8IgGgFIgBADQgCAOAAAGIAAAHQABAJACAJIABgBQABABABAGgAEhFMIgEgJIAEAJgAmNDrIAAAAQAAAZAwAXQBKAjAKAHIASgBIAGAAIATgBIANgUQAGgIALgCQgCgMAAgMQAAgXACgFIgHAIQgUAXgKAAQhZAAhDgyIgBgBQgLAJAAAFgADREkQAHADAMAPQAPANAeAAIAMAAIANgCQAtgFAjgZIAtgqIABgGIABgCIgsgWIgOAIQhJAtgSAAQgzAAgPgMQgJgQgMgIIABgHIgGAFQARAMAIANIABACIgDAKIgGgCIAGACQgEAMgFAJIAEAAIAHAAgAi4CBQAEAEAAALQAAAJgHAJQgDADgBAEQgDAGAAAFQAAAVBDAnQBKArA+AAIAEAAIAEAAQA8gDBCgxQAkgaAXgaIgBgJQgBgHgCgHIgJgUIAAgGIgzAdQhnA7gmAAQg3AAhZhEQgcgUgSgQIAAgBIAHgEIgHAEIAAABQgdgagFgNIgCgGIACgMQgOAGgIAAQhWAAg8g2IgHAMIgGAMIgKASIAyAcQAfASAQAXIAFAJIAAAAQA0gBAbgGIAagFQALAAABACIgBACIACACIgDAFIADgFIABADIADgDQAIAEACACgAmNCGQgJALgDAHIgCAGQAAARA3AeQAzAdAzAFIABAAIAUABQAJAAAGgNQAFgLAJgGIgBgCQgPgRgCgGIAAgBQAAgOAFgMIhjAAQgjgSghgWIgNAQgADMDdIABACIAkAPIAYABIAEAAIAIAAQA1gDAoghQArgkADgPIAAgHQAAgIgGAAQg3AXgZASQg3AOghgOIgMgJIgBAJQAAALgPAQIgLAMIgBABIABgBgAi8DqQADgCADAAIgDgCIgDAEgAi7AxQgOANAAAKQAAAUBZAxQA8AhAeAKIAEABQAKADAGAAQAWAABZgpQBPgkAVgPQAAgGgCgFQgDgJgIgFQgPgJgFgLIgoATQhhAtgvgFQghgCgqgNIgFgCQg9gUgcgcIgCgDIgIAHgADqCLQANAIAUAHIAGACIACAAQASACAdgKIAugXQArgWAAgMQAAgDgDgEIgKgKQgUgSgHgJIAAgDIgVAIQgkANgfASIgwAAIABAKQAAADgLAJIgJgDIAJADQgKAIgQAKIADAAQALAAAHAIIABgBQAAABANAIgAjGgwIgDADQAXARA+A5QAzAqArAIIABAAIABAAIAPACQAgAABVghIBYgkIABgCIABgGQgHgIgYgWQgWgVgIgEIAAgGQAAAQgsAdQgrAagnATIgFAAQgIAAgXgHIgDgBQgXgVg0gdQgsgbgWgWQgNAHgUATgADggEIAFAMIAAACQgDALgaAQQAOAEAJAPIAEAAIACAAIAogDQgHgCBIgZQBIgZACgLQADgOgGgJIgBgBIgOgSQg2AlhiAAQgMAAgIgDIAGAOgAmMhZQgHABgFAEQgJAFgHAJQA8BBAZAKQAcAPBCACIAeABIABAAQAFgGAGgDQgHgJgFgHIgvAAQgNgGgXgOQgdgTgsgiIgCgCIgSgOIgFACgAg1jVQgpAdgLACQgvAcgFAGQgDADgBANIAAANQAAAIBSA3QA5AnAZAMQANAFAGAAQASgBA6gsQA6gsgEgDIgHgGIg0g2Qg4g8gTgKQgMgGgLAAQgVAAgbAPgADWgaIAAAAIACgBQA5gWBRgZQA5gVAAgiQAAgIgOgRIgEgFQgOgQgBgKQgMAMgTAPQg/AwhPAAQgPAAgNgBIgIgCQAGAMAAAHQAAAHgTASIAFgBQAeAAAXAsgAl4jMQgVAcgMAOQgLAMgEABQAcAOARAVIAaAhQAfAiBeAPIgBgLQAAgSAIgJIAEgEQANgLAdAAQgIgKgDgJIgIAAQglAAhcg/Ig1gmIAAABgAlJkmQgfAYgNAOQgKAJAAAFQAAAJBlAyIBGAiIABAAIAaAMQAGglAZgRICAg7IACgBIgFgFQgcgbgVgPQgYgSgZgKIAAAAQgaAAgMgNQgVgFgWAAQg4AAhBAygABTk3QgqAYgBAmIAAACIgBABQAeAOAjAiQAUATAQAVIABAAQAJAAAXAJIAZAHIAFABIACAAQAuAAA/gjQA5ghAAgNQAAgKgJgMQgRgWgvgbQg3gggxgTIgzAAQglATgXAOgAh4m0QgbAaAAATIAAAKQAAAJACACIAKALQArAHAjAQQAgAPARATIAFAGQAKAQAAAUIAAAFIALACIgCgFQgDgKAAgFQAAgOAQgVQATgXAcgQQAWgNAYgGIgGgGIgCgDIgbgmQgvg7g+AAQg9AAglAkgADPEPIAAAAg");
	this.shape_12.setTransform(0.4,-2.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#AF952E").s().p("AgwGqQAAgXAWgTQATgRArgWQAxgYAkADIAAAEQAAAJhGAoQhJAsgFAFgAgnE/QAFgJAIgMQAVgaAYgLQAvgUAOgFQAcgKAYgCIgBALQgSAPhFAiQhDAjgNAAIgDAAgAkXD2QANgKASgNQAAANACALQgLACgGAJIgMATIgUABQAEgVAMgLgAEsDmQAagPAogRIAFAAIgBAFIgtArQgkAYgsAGQANgXAqgXgAgdDZQAGgJAbgVQBchBARACIAjAAIAAAJQgXAagkAaQhCAxg6ADQAEgSACgCgAkzDBQADgWADgFQAGgTAUgIQABAGAPASIACACQgKAGgFAKQgFANgKAAIgUgBgAEnCLQAngZAdgOIgBAGQgCAPgrAkQgoAig2ACQAVgWAzgggAhICMQAOgcA1ghQBBgpBFAAIAZABQABAFAAAHQgVAOhPAkQhXAqgYAAQgFAAgLgDgADdBvQAHgMAlgeQAqgiASADIAdAAQADAFAAADQAAALgrAXIguAWQgYAJgQAAIgHAAgAlXA4QAagUAkgHQAFAOAeAZIgGAJIgCgCIABgBQgBgCgLAAIgaAFQgbAFg0ABQAOgRANgKgAg6AjIgQgBQBKg0AhgHQAggIAegBQAfgCAVACIgBADIhYAiQhRAggiAAIgBAAgADOgQQAXgQATgMQAOgJAogLQAkgKAFgGIAHAAIABACQAGAIgCAOQgDAOhHAZQhJAXAHACIgoACQAHgKAYgQgAgwgyQgEgmAsgcQAogaA8gBIAGAGQAFADg7AsQg4AsgSABIgBAAQgGAAgLgFgAD4iXQBCgoAegIQANAQAAAIQAAAig4AWQhRAYg5AWQAAgbBVgzgAkJjCIAAgBQAugyAcgVQAxgjAxAAIAKAAIAGAFIgDABIiAA6QgZARgGAlIgagLgACVi6IgCgFIAOgTQARgSAZgOQBDgoAmgIQAKANAAAKQgBANg5AgQg/AkguAAIgCAAgAgtktIAAgFQAAgTgMgRIAAAAQAGgdANgMQAjghAEgDQAIgGAUAAIARACIAOACIACAEIAGAGQgXAGgXANQgaAQgSAXQgRAUAAAPQAAAFADAKIACAEIgLgCg");
	this.shape_13.setTransform(5.8,1.8);

	this.instance_4 = new lib.Planter();
	this.instance_4.setTransform(0.8,0.5,1,1,0,0,0,22.5,22.9);

	this.instance_5 = new lib.Planter();
	this.instance_5.setTransform(-21.9,-12.4,1,1,0,0,0,22.5,22.9);

	this.instance_6 = new lib.Planter();
	this.instance_6.setTransform(21.8,-13.4,1,1,0,0,0,22.5,22.9);

	this.instance_7 = new lib.Planter();
	this.instance_7.setTransform(-0.9,-26.3,1,1,0,0,0,22.5,22.9);

	this.instance_8 = new lib.Planter();
	this.instance_8.setTransform(0.8,24.5,1,1,0,0,0,22.5,22.9);

	this.instance_9 = new lib.Planter();
	this.instance_9.setTransform(-21.9,11.6,1,1,0,0,0,22.5,22.9);

	this.instance_10 = new lib.Planter();
	this.instance_10.setTransform(23.3,11.2,1,1,0,0,0,22.5,22.9);

	this.instance_11 = new lib.Planter();
	this.instance_11.setTransform(0.6,-0.2,1,1,0,0,0,22.5,22.9);

	this.instance_12 = new lib.Postscopy();
	this.instance_12.setTransform(-0.1,-22.9,1.07,1.07,0,0,180,40.6,26.5);

	this.instance_13 = new lib.Posts();
	this.instance_13.setTransform(-0.1,-9.4,1.07,1.07,0,0,0,40.6,26.5);

	this.instance_14 = new lib.Postscopy();
	this.instance_14.setTransform(-0.1,3.9,1.07,1.07,0,0,180,40.6,26.5);

	this.instance_15 = new lib.Posts();
	this.instance_15.setTransform(-0.1,17.4,1.07,1.07,0,0,0,40.6,26.5);

	this.instance_16 = new lib.Decking();
	this.instance_16.setTransform(-0.2,-20.6,1,1,0,0,180,45.1,29.6);

	this.instance_17 = new lib.DeckingFlipped();
	this.instance_17.setTransform(-0.1,-7.6,1,1,0,0,0,45.1,29.6);

	this.instance_18 = new lib.Decking();
	this.instance_18.setTransform(-0.2,5.4,1,1,0,0,180,45.1,29.6);

	this.instance_19 = new lib.DeckingFlipped();
	this.instance_19.setTransform(-0.1,18.4,1,1,0,0,0,45.1,29.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance,p:{scaleY:1,x:1.1,y:11.5}}]},2).to({state:[{t:this.instance_2,p:{y:18.4}},{t:this.instance_1,p:{y:4.4}}]},1).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.instance_3,p:{y:-1.6}}]},1).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9}]},1).to({state:[{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4}]},1).to({state:[{t:this.instance_15},{t:this.instance_14},{t:this.instance_13},{t:this.instance_12}]},1).to({state:[{t:this.instance_19,p:{y:18.4}},{t:this.instance_18,p:{y:5.4}},{t:this.instance_17,p:{y:-7.6}},{t:this.instance_16,p:{y:-20.6}}]},1).to({state:[{t:this.instance_2,p:{y:-10}},{t:this.instance_1,p:{y:-21.3}}]},1).to({state:[{t:this.instance,p:{scaleY:0.96,x:1,y:-16.3}}]},1).to({state:[{t:this.instance_3,p:{y:-58.6}}]},1).to({state:[{t:this.instance_19,p:{y:-37.9}},{t:this.instance_18,p:{y:-50.9}},{t:this.instance_17,p:{y:-63.9}},{t:this.instance_16,p:{y:-76.9}}]},1).wait(1));

	// Layer 2
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("At2EyQgEgHACgJQADgIAHgEIP8pOQAKgGAKAGILNGcQAHAEADAJQACAIgFAHQgEAHgIACQgIACgHgEIrDmWIvyJIQgGADgEAAQgMAAgGgKg");
	this.shape_14.setTransform(-26.6,-32.6,0.21,0.21);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AiYE0IrNmcQgHgEgDgIQgCgIAEgIQAEgHAJgCQAIgCAHAEILDGWIPgo+QAHgEAJACQAIACAEAIQAEAHgCAIQgCAIgIAEIvqJEQgFADgFAAQgGAAgEgDg");
	this.shape_15.setTransform(-26.8,-23.8,0.21,0.21);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("At1EyQgFgHADgIQACgJAHgEIP8pOQAKgFAKAFILNGcQAIAEACAJQACAIgEAHQgEAHgJADQgIACgHgFIrDmWIvyJIQgDADgHAAQgMAAgFgKg");
	this.shape_16.setTransform(-5.1,-45,0.21,0.21);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AiYE0IrNmcQgIgEgCgIQgCgJAEgHQAEgHAJgCQAIgDAHAFILDGWIPgo+QAHgEAIACQAJACAEAHQAEAHgCAJQgDAIgHAEIvqJEQgFADgFAAQgGAAgEgDg");
	this.shape_17.setTransform(-5.3,-36.2,0.21,0.21);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CBBBA3").s().p("AtkBYIP8pNILNGeIv8JNg");
	this.shape_18.setTransform(-26.6,-28.2,0.21,0.21);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#CBBBA3").s().p("AtkBZIP8pOILNGeIv8JOg");
	this.shape_19.setTransform(-5.1,-40.7,0.21,0.21);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("At2EyQgEgHACgIQADgJAHgEIP8pOQAKgGAKAGILNGcQAHAEADAJQACAIgEAHQgFAHgIACQgIACgHgEIrDmWIvyJIQgDADgHAAQgMAAgGgKg");
	this.shape_20.setTransform(-11.5,-23.9,0.21,0.21);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AiYE0IrNmcQgHgEgDgIQgCgIAEgIQAFgHAIgCQAIgCAHAEILDGWIPgo+QAHgEAJACQAIADAEAHQAEAHgCAIQgCAIgHAEIvrJEQgDADgHAAQgGAAgEgDg");
	this.shape_21.setTransform(-11.7,-15.1,0.21,0.21);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("At2ExQgEgHACgIQACgIAIgEIP7pOQALgGAKAGILNGcQAHAEADAIQACAIgFAIQgEAHgIACQgIACgHgEIrDmWIvyJIQgFADgFAAQgLAAgHgLg");
	this.shape_22.setTransform(10,-36.3,0.21,0.21);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AiYE0IrNmcQgIgEgCgIQgCgJAEgHQAEgHAJgCQAIgCAHAEILDGWIPgo+QAHgEAIACQAJACAEAHQAEAIgCAIQgCAIgIAEIvqJEQgEADgGAAQgGAAgEgDg");
	this.shape_23.setTransform(9.8,-27.5,0.21,0.21);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#CBBBA3").s().p("AtkBYIP7pNILOGeIv8JNg");
	this.shape_24.setTransform(-11.5,-19.5,0.21,0.21);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#CBBBA3").s().p("AtkBZIP8pOILNGeIv7JOg");
	this.shape_25.setTransform(9.9,-32,0.21,0.21);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AjJCWQgUgCAAgUQAAgUAUgaQAUgZAcgQIEzixQAcgQAUADQAUACAAAUQAAAUgUAZQgUAagcAQIkzCxQgYAOgSAAIgGgBg");
	this.shape_26.setTransform(15.8,-6.2,0.21,0.21);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#F9FE9F").s().p("AhSByQgUgLAAggIAAgXQAAggAUggQAUgjAcgQIBGgqQAcgPATALQAUALAAAhIAAAWQAAAggUAhQgTAigcAQIhGAqQgQAJgOAAQgKAAgIgFg");
	this.shape_27.setTransform(13.3,-1.1,0.21,0.21);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AhSByQgUgLAAghIAAgWQAAggAUghQAUgiAcgQIBGgqQAcgQATAMQAUALAAAhIAAAWQAAAhgUAfQgTAjgcAQIhGAqQgQAJgOAAQgKAAgIgFg");
	this.shape_28.setTransform(18.3,-3.9,0.21,0.21);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#E6E7E8").s().p("AAjBtIhGgpQgbgQgUgjQgUggAAggIAAgXQAAgfAUgMQAUgLAbAQIBGApQAcAQAUAiQAUAhAAAgIAAAWQAAAggUAMQgIAFgKAAQgNAAgRgKg");
	this.shape_29.setTransform(-11.3,5.6,0.21,0.21);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("At1EyQgFgHACgIQADgJAHgEIP7pOQALgFAKAFILNGcQAIAEABAJQACAIgDAHQgFAHgIADQgIACgHgFIrDmWIvyJIQgDADgHAAQgMAAgFgKg");
	this.shape_30.setTransform(3.6,-15.2,0.21,0.21);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("Ag0BWIARgKIAAhYIgNAIQgBAAgBAAQgBABAAAAQgBAAAAgBQAAAAAAAAIACgGIAQgpQACgGADgBQAAgBAAAAQABAAAAABQABAAAAAAQABABAAABIARAWIACAEQAAACgDACIgOAIIAABYIAwgdIAAhZIgNAIQgBABgBAAQAAAAgBAAQAAAAAAAAQgBAAAAgBIACgFIARgqQACgGACgBQABAAAAAAQABAAAAAAQABAAAAABQABAAAAABIARAWIACAEQAAACgEACIgOAIIAABYIARgJIAAAJIhoA9g");
	this.shape_31.setTransform(7.9,8,0.21,0.21);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgdBQQAOgNAIgEIAAhSQgRAHgNgEQgOgEgGgOIATgLQAGAZAIgDIAAgeIBSgwQgIAXgMARQgNATgQANIAABRIARgHQATgGAHADIhmA9QAGgJAPgOg");
	this.shape_32.setTransform(5.3,9.4,0.21,0.21);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgUBOQgFgDAAgHQAAgFAEgCQADgCAAAFQAAAEADABQABABAEgBQADgCACgEQADgFAAgCIAAhTQgJAFgGAWQgGgQgLAGQgNAIgHATQACgXAQgaQAQgaAUgMQAVgNAQAIQAPAHADAYQgIgOgMAIQgLAFgHAYQgGgPgIAFIAABSQAAAHgDAIQgEAIgGADQgDACgEAAIgDgBg");
	this.shape_33.setTransform(2.9,10.8,0.21,0.21);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgNGPQgGgGAAgJIAAr/QAAgJAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAJIAAL/QAAAJgGAGQgGAGgIAAQgHAAgGgGg");
	this.shape_34.setTransform(0.4,7.8,0.21,0.21);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AiYE0IrNmcQgIgEgCgIQgCgJAEgHQAEgHAJgCQAIgDAHAFILDGWIPgo+QAHgEAIACQAJACAEAHQAEAHgCAJQgDAIgHAEIvqJEQgFADgFAAQgGAAgEgDg");
	this.shape_35.setTransform(3.4,-6.4,0.21,0.21);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AihK6IrNmeQgKgGAAgMIAAr/QAAgJAGgGQAGgGAIAAQAJAAAGAGQAGAGAAAJIAAL0IK4GSIPopEIAAr0QAAgJAGgGQAGgGAIAAQAIAAAGAGQAGAGAAAJIAAMAQAAAMgKAFIv7JQQgGADgFAAQgFAAgFgDg");
	this.shape_36.setTransform(3.5,1.6,0.21,0.21);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.lf(["#E3B967","#FFD889"],[0.231,1],-67.7,39.2,67.8,-39.1).s().p("An9hXIP7pQIAAMAIv7JPg");
	this.shape_37.setTransform(11.1,1.6,0.21,0.21);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#BFA26A").s().p("AllCxIAAr/ILLGeIAAL/g");
	this.shape_38.setTransform(-7.2,3.5,0.21,0.21);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#CBBBA3").s().p("AtkBYIP8pNILNGeIv8JNg");
	this.shape_39.setTransform(3.6,-10.8,0.21,0.21);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AjJCWQgUgCAAgUQAAgUAUgZQAUgaAcgQIEzixQAcgQAUADQAUACAAAUQAAAUgUAaQgUAZgcAQIkzCxQgYAOgSAAIgGgBg");
	this.shape_40.setTransform(15.8,9.9,0.21,0.21);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#F9FE9F").s().p("AhSByQgUgLAAghIAAgWQAAggAUghQAUgiAcgQIBGgqQAcgPATALQAUALAAAgIAAAXQAAAggUAgQgTAjgcAQIhGApQgQAKgOAAQgKAAgIgFg");
	this.shape_41.setTransform(13.3,15.1,0.21,0.21);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AhSByQgUgLAAghIAAgWQAAggAUghQAUgiAcgQIBGgqQAcgPATALQAUALAAAgIAAAXQAAAggUAgQgTAjgcAQIhGApQgQAKgOAAQgKAAgIgFg");
	this.shape_42.setTransform(18.3,12.2,0.21,0.21);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#E6E7E8").s().p("AAjBtIhGgpQgbgQgUgjQgUghAAggIAAgWQAAggAUgLQAUgLAbAQIBGApQAcAQAUAiQAUAhAAAgIAAAWQAAAggUAMQgIAFgKAAQgNAAgRgKg");
	this.shape_43.setTransform(-11.3,21.8,0.21,0.21);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("At1EyQgFgIACgIQADgIAHgEIP7pOQALgGAKAGILNGcQAIAEABAIQACAJgDAHQgFAHgIACQgIACgHgEIrDmWIvyJIQgFADgFAAQgMAAgFgKg");
	this.shape_44.setTransform(3.6,1,0.21,0.21);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("Ag0BWIARgKIAAhYIgNAHQgBABgBAAQgBAAAAAAQgBABAAgBQAAAAAAAAIACgGIAQgqQACgFADgCQAAAAAAAAQABAAAAABQABAAAAAAQABABAAAAIARAXIACADQAAADgDABIgOAIIAABZIAwgeIAAhYIgNAIQgBAAgBABQAAAAgBAAQAAAAAAAAQgBAAAAgBIACgGIARgpQACgGACgBQABAAAAAAQABAAAAAAQABAAAAABQABAAAAABIARAWQABABAAAAQAAABABAAQAAAAAAABQAAAAAAABQAAABgEADIgOAIIAABYIARgKIAAAKIhoA9g");
	this.shape_45.setTransform(7.9,24.2,0.21,0.21);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AgdBQQAOgNAIgEIAAhSQgSAHgMgEQgOgEgGgOIATgLQAGAZAIgDIAAgeIBSgwQgHAWgNASQgNAUgQALIAABSIARgHQATgGAHAEIhmA7QAGgHAPgPg");
	this.shape_46.setTransform(5.3,25.5,0.21,0.21);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AgUBOQgFgCAAgIQAAgFAEgCQADgCAAAFQAAAEADABQACABADgCQADgBACgEQADgEAAgEIAAhSQgJAFgGAVQgGgPgLAGQgNAIgHATQACgWAQgbQARgbATgMQAVgMAQAIQAQAHACAXQgIgNgMAHQgLAHgHAWQgGgOgIAEIAABTQAAAHgDAIQgDAHgHAEQgDACgDAAIgEgBg");
	this.shape_47.setTransform(2.9,27,0.21,0.21);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AgNGOQgGgFAAgJIAAsAQAAgHAGgGQAGgHAHAAQAIAAAGAHQAGAFAAAIIAAMAQAAAJgGAFQgGAHgIgBQgHABgGgHg");
	this.shape_48.setTransform(0.4,24,0.21,0.21);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AiYE0IrNmcQgIgEgCgIQgCgIAEgIQAEgHAJgCQAIgCAHAEILDGWIPgo+QAHgEAIACQAJACAEAIQAEAHgCAIQgDAIgHAEIvqJEQgGADgEAAQgFAAgFgDg");
	this.shape_49.setTransform(3.4,9.8,0.21,0.21);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("AihK6IrNmeQgKgGAAgMIAAsAQAAgIAGgFQAGgHAIAAQAJAAAGAHQAGAGAAAHIAAL0IK4GSIPopDIAAr0QAAgJAGgGQAGgFAIgBQAJABAFAFQAGAGAAAJIAAL/QAAAMgKAGIv7JQQgFACgGAAQgFAAgFgCg");
	this.shape_50.setTransform(3.5,17.8,0.21,0.21);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.lf(["#E3B967","#FFD889"],[0.231,1],-67.7,39.2,67.8,-39.1).s().p("An9hYIP7pPIAAL/Iv7JQg");
	this.shape_51.setTransform(11.1,17.8,0.21,0.21);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#BFA26A").s().p("AllCxIAAr/ILLGeIAAL/g");
	this.shape_52.setTransform(-7.2,19.6,0.21,0.21);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#CBBBA3").s().p("AtkBYIP8pNILNGeIv8JNg");
	this.shape_53.setTransform(3.6,5.3,0.21,0.21);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AjJCWQgUgCAAgUQAAgUAUgaQAUgZAcgQIEzixQAcgQAUADQAUACAAAUQAAAUgUAZQgUAagcAQIkzCxQgYAOgSAAIgGgBg");
	this.shape_54.setTransform(15.8,26.1,0.21,0.21);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#F9FE9F").s().p("AhSByQgUgLAAggIAAgXQAAggAUggQAUgjAcgQIBGgqQAcgPATALQAUALAAAhIAAAWQAAAggUAhQgTAigcAQIhGAqQgQAJgOAAQgKAAgIgFg");
	this.shape_55.setTransform(13.3,31.3,0.21,0.21);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AhSByQgUgLAAghIAAgWQAAggAUghQAUgiAcgQIBGgqQAcgQATAMQAUALAAAhIAAAWQAAAhgUAfQgTAjgcAQIhGAqQgQAJgOAAQgKAAgIgFg");
	this.shape_56.setTransform(18.3,28.4,0.21,0.21);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#E6E7E8").s().p("AAjBtIhGgpQgbgQgUgjQgUggAAggIAAgXQAAgfAUgMQAUgLAbAQIBGApQAcAQAUAiQAUAhAAAgIAAAWQAAAggUAMQgIAFgKAAQgNAAgRgKg");
	this.shape_57.setTransform(-11.3,38,0.21,0.21);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("At1ExQgFgHACgIQADgIAHgEIP7pOQALgFAKAFILNGcQAIAEABAIQACAIgDAIQgFAHgIACQgIACgHgEIrDmWIvyJIQgEADgGAAQgMAAgFgLg");
	this.shape_58.setTransform(3.6,17.1,0.21,0.21);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#000000").s().p("Ag0BWIARgKIAAhYIgNAIQgBAAgBAAQgBABAAAAQgBAAAAgBQAAAAAAAAIACgGIAQgpQACgFADgCQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABIARAWQABABAAAAQAAAAABABQAAAAAAABQAAAAAAABQAAACgDACIgOAIIAABYIAwgdIAAhZIgNAIQgBABgBAAQAAAAgBAAQAAAAAAAAQgBAAAAgBIACgFIARgqQACgGACgBQAAAAABAAQABAAAAAAQABAAAAABQABAAAAABIARAWQABABAAAAQAAABABAAQAAABAAAAQAAABAAAAQAAACgEACIgOAIIAABYIARgJIAAAJIhoA9g");
	this.shape_59.setTransform(7.9,40.4,0.21,0.21);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("AgdBQQAOgNAIgEIAAhSQgQAHgOgEQgOgEgGgOIATgLQAGAZAIgDIAAgeIBSgwQgIAXgMARQgNATgQANIAABRIARgHQATgGAHADIhmA9QAGgJAPgOg");
	this.shape_60.setTransform(5.3,41.7,0.21,0.21);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#000000").s().p("AgUBOQgFgDAAgHQAAgEAEgDQADgCAAAFQAAAEADABQABABAEgBQADgCACgEQADgFAAgCIAAhTQgJAEgGAXQgGgQgLAGQgNAIgHATQACgXAQgaQAQgaAUgMQAVgNAQAIQAPAHADAYQgIgOgMAIQgLAFgHAYQgGgPgIAFIAABSQAAAHgDAIQgEAIgGADQgDACgEAAIgDgBg");
	this.shape_61.setTransform(2.9,43.1,0.21,0.21);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#000000").s().p("AgNGPQgGgGAAgJIAAr/QAAgJAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAJIAAL/QAAAJgGAGQgGAGgIAAQgHAAgGgGg");
	this.shape_62.setTransform(0.4,40.1,0.21,0.21);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#000000").s().p("AiYE0IrNmcQgIgEgCgIQgCgJAEgHQAEgHAJgCQAIgDAHAFILDGWIPgo+QAHgEAIACQAJACAEAHQAEAIgCAIQgDAIgHAEIvqJEQgFADgFAAQgGAAgEgDg");
	this.shape_63.setTransform(3.4,25.9,0.21,0.21);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#000000").s().p("AihK6IrNmeQgKgGAAgMIAAsAQAAgIAGgGQAGgGAIAAQAJAAAGAGQAGAGAAAIIAAL0IK4GSIPopEIAAr0QAAgIAGgFQAGgGAIAAQAIAAAGAGQAGAFAAAIIAAMAQAAAMgKAGIv7JQQgGACgFAAQgFAAgFgCg");
	this.shape_64.setTransform(3.5,33.9,0.21,0.21);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.lf(["#E3B967","#FFD889"],[0.231,1],-67.7,39.2,67.8,-39.1).s().p("An9hXIP7pQIAAMAIv7JPg");
	this.shape_65.setTransform(11.1,33.9,0.21,0.21);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#BFA26A").s().p("AllCyIAAsAILLGeIAAL/g");
	this.shape_66.setTransform(-7.2,35.8,0.21,0.21);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#CBBBA3").s().p("AtkBYIP8pNILNGdIv8JOg");
	this.shape_67.setTransform(3.6,21.5,0.21,0.21);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AjJCWQgUgCAAgUQAAgUAUgaQAUgZAcgQIEzixQAcgQAUADQAUACAAAUQAAAUgUAZQgUAagcAQIkzCxQgYAOgSAAIgGgBg");
	this.shape_68.setTransform(37.3,-18.7,0.21,0.21);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#F9FE9F").s().p("AhSByQgUgMAAgfIAAgXQAAggAUggQAUgjAbgQIBGgpQAcgRAUAMQAUAMAAAfIAAAXQAAAggUAhQgTAigdAQIhGApQgQAKgNAAQgKAAgIgFg");
	this.shape_69.setTransform(34.8,-13.5,0.21,0.21);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AhSByQgUgMAAgfIAAgXQAAggAUggQAUgjAbgQIBGgqQAdgPATALQAUALAAAhIAAAWQAAAggUAhQgTAigdAQIhGAqQgQAJgNAAQgKAAgIgFg");
	this.shape_70.setTransform(39.7,-16.4,0.21,0.21);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#E6E7E8").s().p("AAjBuIhFgqQgcgQgUgjQgUgfAAghIAAgXQAAgfAUgMQAUgMAcAQIBFAqQAdAQATAiQAUAhAAAgIAAAWQAAAhgUALQgIAFgKAAQgNAAgRgJg");
	this.shape_71.setTransform(10.1,-6.8,0.21,0.21);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#000000").s().p("At2ExQgEgHACgIQADgIAHgEIP8pOQAEgDAGAAQAFAAAFADILNGcQAHAEADAIQACAIgEAIQgFAHgIACQgIACgHgEIrDmWIvyJIQgFADgFAAQgLAAgHgLg");
	this.shape_72.setTransform(25,-27.6,0.21,0.21);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#000000").s().p("Ag0BWIARgKIAAhYIgNAIQgBAAgBABQgBAAAAAAQgBAAAAAAQAAgBAAAAIASgvQADgGACgBQAAgBABAAQAAAAABAAQAAABABAAQAAABABABIARAWQAAABABAAQAAAAAAABQAAAAAAABQAAAAAAABQAAABgEADIgNAIIAABYIAxgdIAAhZIgNAIQgBABgBAAQgBAAgBAAQAAAAAAAAQgBAAAAgBIACgFIARgqQADgGACgBQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABIARAWIACAEQAAACgDACIgOAIIAABYIAQgJIAAAJIhoA9g");
	this.shape_73.setTransform(29.4,-4.4,0.21,0.21);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#000000").s().p("AgcBQQAPgOAGgDIAAhSQgQAHgOgEQgNgEgHgOIATgLQAFAZAJgDIAAgeIBSgxQgGAVgNAUQgOAUgQALIAABSIARgGQATgIAHAEIhlA8QAEgIARgOg");
	this.shape_74.setTransform(26.8,-3.1,0.21,0.21);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#000000").s().p("AgUBOQgEgDAAgGQAAgGADgCQAEgCAAAFQAAAEACABQACACADgCQADgCADgEQACgDAAgEIAAhTQgJAFgGAWQgGgQgLAHQgMAHgIATQADgXAPgaQAQgZAUgNQAVgNAQAIQAQAIACAXQgHgOgNAIQgLAGgGAXQgGgPgJAFIAABSQAAAHgDAIQgEAIgGADQgDACgDAAIgEgBg");
	this.shape_75.setTransform(24.4,-1.6,0.21,0.21);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#000000").s().p("AgNGPQgGgGAAgIIAAsAQAAgJAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAJIAAMAQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
	this.shape_76.setTransform(21.8,-4.6,0.21,0.21);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#000000").s().p("AiYE0IrNmcQgHgEgDgIQgCgJAEgHQAFgHAIgCQAIgCAHAEILDGWIPgo+QAHgEAJACQAIACAEAHQAEAIgCAIQgCAIgHAEIvrJEQgEADgGAAQgFAAgFgDg");
	this.shape_77.setTransform(24.8,-18.8,0.21,0.21);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#000000").s().p("AihK6IrNmeQgKgGAAgMIAAr/QAAgJAGgGQAGgGAIAAQAIAAAGAGQAGAGAAAJIAAL0IK5GSIPopEIAAr0QAAgIAGgHQAGgFAIAAQAIAAAGAFQAGAGAAAJIAAL/QAAAMgKAGIv8JQQgFACgFAAQgEAAgGgCg");
	this.shape_78.setTransform(25,-10.8,0.21,0.21);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.lf(["#E3B967","#FFD889"],[0.231,1],-67.7,39.2,67.8,-39.1).s().p("An9hXIP7pQIAAL/Iv7JQg");
	this.shape_79.setTransform(32.5,-10.8,0.21,0.21);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#BFA26A").s().p("AllCxIAAr/ILLGeIAAL/g");
	this.shape_80.setTransform(14.3,-9,0.21,0.21);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#CBBBA3").s().p("AtkBZIP8pOILNGeIv8JOg");
	this.shape_81.setTransform(25,-23.3,0.21,0.21);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AjJCWQgUgCAAgUQAAgUAUgZQAUgaAcgQIEzixQAcgPAUACQAUADAAATQAAAUgUAaQgUAZgcAQIkzCxQgYANgSAAIgGAAg");
	this.shape_82.setTransform(37.3,-2.5,0.21,0.21);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#F9FE9F").s().p("AhSByQgUgLAAghIAAgWQAAggAUghQATgiAcgQIBGgqQAdgQATAMQAUALAAAhIAAAWQAAAggUAgQgTAjgdAQIhGAqQgQAJgNAAQgKAAgIgFg");
	this.shape_83.setTransform(34.8,2.7,0.21,0.21);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AhSByQgUgLAAghIAAgWQAAggAUghQAUgiAbgQIBGgqQAdgPATALQAUAMAAAfIAAAXQAAAggUAgQgUAjgcAQIhGApQgQAKgNAAQgKAAgIgFg");
	this.shape_84.setTransform(39.7,-0.2,0.21,0.21);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#E6E7E8").s().p("AAjBtIhFgpQgcgQgUgiQgUghAAggIAAgXQAAggAUgLQAUgMAcARIBFApQAdAQATAjQAUAfAAAhIAAAXQAAAggUALQgIAFgKAAQgNAAgRgKg");
	this.shape_85.setTransform(10.1,9.4,0.21,0.21);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#000000").s().p("At2EyQgEgHACgJQADgIAHgEIP8pOQAKgGAKAGILNGcQAHAEADAJQACAIgEAHQgFAHgIACQgIACgHgEIrDmWIvyJIQgEADgGAAQgMAAgGgKg");
	this.shape_86.setTransform(25,-11.5,0.21,0.21);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#000000").s().p("Ag0BWIARgKIAAhYIgNAIQgBAAgBABQgBAAAAAAQgBAAAAAAQAAgBAAAAIACgGIAQgqQADgGACAAQAAgBABAAQABAAAAABQABAAAAAAQABABAAABIARAWIABADQAAACgEADIgNAHIAABZIAxgdIAAhZIgNAIQgBAAgBABQgBAAgBAAQAAAAAAAAQgBAAAAgBIACgFIARgqQADgGACgBQAAAAABAAQAAAAAAAAQABAAAAABQABAAAAABIARAWIACAEQAAACgDACIgOAIIAABYIAQgJIAAAJIhoA9g");
	this.shape_87.setTransform(29.4,11.8,0.21,0.21);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#000000").s().p("AgcBQQAOgNAHgEIAAhSQgRAHgNgFQgNgDgHgOIATgLQAGAZAIgDIAAgeIBSgwQgGAVgNASQgMATgSAOIAABRIARgHQAUgGAGAEIhlA8QAEgIARgPg");
	this.shape_88.setTransform(26.8,13.1,0.21,0.21);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#000000").s().p("AgUBOQgEgDAAgHQAAgFADgCQAEgCAAAFQAAAEACABQACABADgBQAEgDACgDQACgEAAgEIAAhSQgJAFgGAVQgGgPgLAGQgMAIgIATQACgXAQgaQAQgaAUgMQAVgNAQAIQAQAHACAYQgHgOgNAHQgLAHgGAXQgGgPgJAEIAABTQAAAHgDAIQgDAHgHAEQgDACgDAAIgEgBg");
	this.shape_89.setTransform(24.4,14.5,0.21,0.21);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#000000").s().p("AgNGPQgGgHAAgIIAAr/QAAgJAGgFQAGgGAHgBQAIABAGAGQAGAFAAAJIAAL/QAAAIgGAHQgGAFgIAAQgHAAgGgFg");
	this.shape_90.setTransform(21.8,11.5,0.21,0.21);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#000000").s().p("AiYE0IrNmcQgHgEgDgIQgCgIAEgIQAFgHAIgCQAIgCAHAEILDGWIPgo+QAHgEAJACQAIACAEAIQAEAHgCAIQgCAIgHAEIvrJEQgFADgFAAQgEAAgGgDg");
	this.shape_91.setTransform(24.8,-2.7,0.21,0.21);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#000000").s().p("AihK6IrNmeQgKgGAAgMIAAr/QAAgJAGgGQAGgFAIgBQAJABAFAFQAGAGAAAJIAAL0IK5GSIPopEIAAr0QAAgIAGgHQAFgFAJAAQAIAAAGAFQAGAHAAAIIAAL/QAAAMgKAGIv8JQQgEACgGAAQgFAAgFgCg");
	this.shape_92.setTransform(25,5.3,0.21,0.21);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.lf(["#E3B967","#FFD889"],[0.231,1],-67.7,39.2,67.8,-39.1).s().p("An9hXIP7pQIAAL/Iv7JQg");
	this.shape_93.setTransform(32.5,5.3,0.21,0.21);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#BFA26A").s().p("AllCxIAAr/ILLGeIAAL/g");
	this.shape_94.setTransform(14.3,7.2,0.21,0.21);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#CBBBA3").s().p("AtkBYIP8pOILNGeIv8JOg");
	this.shape_95.setTransform(25,-7.1,0.21,0.21);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFFFFF").s().p("AjJCWQgUgCAAgUQAAgUAUgaQAUgZAcgQIEzixQAcgQAUADQAUACAAAUQAAAUgUAaQgUAZgcAQIkzCxQgYAOgSAAIgGgBg");
	this.shape_96.setTransform(37.3,13.7,0.21,0.21);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#F9FE9F").s().p("AhSByQgUgMAAggIAAgWQAAggAUggQAUgjAbgQIBGgpQAdgQATALQAUAMAAAgIAAAWQAAAggUAhQgTAigdAQIhGApQgQAJgNAAQgKAAgIgEg");
	this.shape_97.setTransform(34.8,18.8,0.21,0.21);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFFF").s().p("AhSByQgUgLAAghIAAgWQAAggAUghQAUgiAbgQIBGgqQAdgPATALQAUAMAAAfIAAAXQAAAggUAgQgTAjgdAQIhGApQgQAKgNAAQgKAAgIgFg");
	this.shape_98.setTransform(39.7,16,0.21,0.21);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#E6E7E8").s().p("AAjBtIhFgpQgcgQgUgjQgUggAAggIAAgXQAAggAUgLQAUgLAcAPIBFAqQAdAQATAiQAUAhAAAgIAAAWQAAAhgUALQgIAFgKAAQgNAAgRgKg");
	this.shape_99.setTransform(10.1,25.5,0.21,0.21);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#000000").s().p("At2EyQgEgIACgIQADgIAHgEIP8pOQAEgDAGAAQAFAAAFADILNGcQAHAEADAIQACAIgEAIQgFAHgIACQgIACgHgEIrDmWIvyJIQgFADgFAAQgMAAgGgKg");
	this.shape_100.setTransform(25,4.7,0.21,0.21);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#000000").s().p("Ag0BWIARgJIAAhZIgNAIQgBAAgBABQgBAAAAAAQgBAAAAAAQAAgBAAAAIASgvQADgGACgBQAAgBABAAQABAAAAABQABAAAAAAQABABAAABIARAWQAAABABAAQAAAAAAABQAAAAAAABQAAAAAAABQAAABgEADIgNAIIAABYIAxgdIAAhZIgNAIQgBABgBAAQgBAAgBAAQAAAAAAAAQgBAAAAgBIACgFIARgqQACgFADgCQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABIARAWIACAEQAAACgDACIgOAIIAABZIAQgKIAAAJIhoA9g");
	this.shape_101.setTransform(29.4,27.9,0.21,0.21);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#000000").s().p("AgcBQQAPgOAGgDIAAhSQgRAHgNgEQgNgEgHgOIATgLQAFAZAJgDIAAgdIBSgyQgGAVgNAUQgNATgRAMIAABSIARgGQASgHAIADIhlA8QAEgIARgOg");
	this.shape_102.setTransform(26.8,29.2,0.21,0.21);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#000000").s().p("AgUBOQgEgCAAgHQAAgGADgCQAEgCAAAGQAAABAAABQAAAAAAABQABAAAAABQAAAAABAAQACACADgCQADgCADgEQACgDAAgEIAAhTQgJAFgGAWQgGgQgLAHQgMAHgIAUQACgXAQgbQAQgZAUgNQAWgNAPAIQAQAIACAXQgHgOgNAIQgLAGgGAXQgGgPgJAFIAABSQAAAGgDAJQgEAIgGADQgDACgDAAIgEgBg");
	this.shape_103.setTransform(24.4,30.7,0.21,0.21);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#000000").s().p("AgNGOQgGgFAAgJIAAsAQAAgIAGgFQAGgHAHABQAIgBAGAHQAGAFAAAIIAAMAQAAAJgGAFQgGAHgIAAQgHAAgGgHg");
	this.shape_104.setTransform(21.8,27.7,0.21,0.21);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#000000").s().p("AiYE0IrNmcQgHgEgDgIQgCgIAEgHQAFgIAIgCQAIgCAHAEILDGWIPgo9QAHgFAJACQAIADAEAHQAEAHgCAIQgCAJgHAEIvrJDQgEADgGAAQgFAAgFgDg");
	this.shape_105.setTransform(24.8,13.5,0.21,0.21);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#000000").s().p("AihK6IrNmeQgKgHAAgLIAAsAQAAgIAGgFQAGgHAIABQAJgBAFAHQAGAGAAAHIAAL0IK5GTIPopEIAAr1QAAgHAGgGQAGgHAIAAQAIAAAGAHQAGAFAAAIIAAMAQAAALgKAHIv8JQQgEADgGAAQgFAAgFgDg");
	this.shape_106.setTransform(25,21.5,0.21,0.21);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.lf(["#E3B967","#FFD889"],[0.231,1],-67.8,39.2,67.8,-39.1).s().p("An9hYIP7pPIAAL/Iv7JQg");
	this.shape_107.setTransform(32.5,21.5,0.21,0.21);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#BFA26A").s().p("AllCxIAAr/ILLGdIAAMAg");
	this.shape_108.setTransform(14.3,23.4,0.21,0.21);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#CBBBA3").s().p("AtkBZIP8pOILNGeIv8JNg");
	this.shape_109.setTransform(25,9.1,0.21,0.21);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#E6E7E8").s().p("AAjBtIhGgpQgbgQgVgjQgTgfAAghIAAgXQAAggATgLQAVgLAbAQIBGApQAcAQAUAiQAUAhAAAgIAAAWQAAAhgUALQgIAFgKAAQgOAAgQgKg");
	this.shape_110.setTransform(-26.4,-3.1,0.21,0.21);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#000000").s().p("AFcJhIrLmeQgKgGAAgMIAAr/QAAgJAGgGQAGgGAIAAQAJAAAFAGQAGAGAAAJIAAL0ILBGYQAIAEACAIQACAIgEAIQgGAKgMAAQgFAAgFgDg");
	this.shape_111.setTransform(-22.2,-5.2,0.21,0.21);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#E6E7E8").s().p("AAjBtIhGgpQgbgQgVgiQgTghAAggIAAgWQAAghATgLQAVgLAbAQIBGApQAcAQAUAjQAUAgAAAgIAAAWQAAAhgUALQgIAFgKAAQgOAAgQgKg");
	this.shape_112.setTransform(-26.4,13.1,0.21,0.21);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#000000").s().p("AFcJhIrLmeQgKgGAAgLIAAsBQAAgIAGgFQAGgHAIAAQAJAAAFAHQAGAGAAAHIAAL1ILBGYQAIADACAJQACAIgEAHQgGAKgMAAQgFAAgFgCg");
	this.shape_113.setTransform(-22.2,10.9,0.21,0.21);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#E6E7E8").s().p("AAjBtIhGgpQgbgQgVgjQgTgfAAghIAAgXQAAggATgLQAVgLAbAQIBGApQAcAQAUAiQAUAhAAAgIAAAWQAAAggUAMQgIAFgKAAQgNAAgRgKg");
	this.shape_114.setTransform(-26.4,29.3,0.21,0.21);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#000000").s().p("AFcJhIrLmeQgKgGAAgMIAAr/QAAgJAGgFQAGgHAIAAQAJAAAFAHQAGAFAAAJIAAL0ILBGYQAIAEACAIQACAIgEAHQgGALgMgBQgGABgEgDg");
	this.shape_115.setTransform(-22.2,27.1,0.21,0.21);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#BFA26A").s().p("AllCxIAAr/ILLGeIAAL/g");
	this.shape_116.setTransform(-22.2,-5.2,0.21,0.21);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#BFA26A").s().p("AllCyIAAsAILLGeIAAL/g");
	this.shape_117.setTransform(-22.2,10.9,0.21,0.21);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#BFA26A").s().p("AllCxIAAr/ILLGdIAAMAg");
	this.shape_118.setTransform(-22.2,27.1,0.21,0.21);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#E6E7E8").s().p("AAjBtIhGgpQgbgQgUgjQgUggAAggIAAgXQAAgfAUgMQAUgLAbAQIBGApQAcAQAVAiQATAhAAAgIAAAWQAAAhgTALQgJAFgKAAQgNAAgRgKg");
	this.shape_119.setTransform(-41.4,-11.8,0.21,0.21);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#000000").s().p("AFcJhIrLmeQgKgGAAgMIAAr/QAAgJAGgGQAFgGAJAAQAIAAAGAGQAGAGAAAJIAAL0ILBGYQAHAEADAIQACAIgEAIQgGAKgMAAQgEAAgGgDg");
	this.shape_120.setTransform(-37.3,-13.9,0.21,0.21);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#E6E7E8").s().p("AAjBtIhGgpQgbgQgUgiQgUghAAggIAAgWQAAggAUgMQAUgLAbAQIBGApQAcAQAVAjQATAgAAAgIAAAWQAAAhgTALQgJAFgKAAQgNAAgRgKg");
	this.shape_121.setTransform(-41.4,4.4,0.21,0.21);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#000000").s().p("AFcJhIrLmeQgKgGAAgMIAAsAQAAgIAGgGQAGgGAIAAQAIAAAGAGQAGAGAAAIIAAL0ILBGZQAHAEADAIQACAIgEAHQgHALgLAAQgFAAgFgDg");
	this.shape_122.setTransform(-37.3,2.2,0.21,0.21);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#E6E7E8").s().p("AAjBtIhGgpQgbgQgUgjQgUggAAggIAAgWQAAggAUgMQAUgLAbAQIBGApQAcAQAVAiQATAgAAAhIAAAWQAAAhgTALQgJAFgKAAQgNAAgRgKg");
	this.shape_123.setTransform(-41.4,20.6,0.21,0.21);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#000000").s().p("AFcJhIrLmeQgKgGAAgMIAAr/QAAgJAGgFQAFgHAJAAQAIAAAGAHQAGAFAAAJIAAL0ILBGYQAHADADAJQACAIgEAHQgGALgMgBQgEAAgGgCg");
	this.shape_124.setTransform(-37.3,18.4,0.21,0.21);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#BFA26A").s().p("AllCxIAAr/ILLGeIAAL/g");
	this.shape_125.setTransform(-37.3,-13.9,0.21,0.21);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#BFA26A").s().p("AllCyIAAsBILLGeIAAMBg");
	this.shape_126.setTransform(-37.3,2.2,0.21,0.21);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#BFA26A").s().p("AllCxIAAr/ILLGeIAAL/g");
	this.shape_127.setTransform(-37.3,18.4,0.21,0.21);

	this.instance_20 = new lib.pallette();
	this.instance_20.setTransform(-1.2,-2,1,1,0,0,0,44.9,30.1);

	this.instance_21 = new lib.ConcreteLintelsFlipped();
	this.instance_21.setTransform(-0.2,7.6,1,1,0,0,180,45.1,29.6);

	this.instance_22 = new lib.ConcreteLintels();
	this.instance_22.setTransform(-0.1,19.4,1,1,0,0,0,45.1,29.6);

	this.instance_23 = new lib.bricks();
	this.instance_23.setTransform(1.1,12.6,1,1,0,0,0,42.1,33.2);

	this.instance_24 = new lib.pavingstones();
	this.instance_24.setTransform(-0.4,-1.6,1,1,0,0,0,45.7,50.3);

	this.instance_25 = new lib.Decking();
	this.instance_25.setTransform(-0.2,-20.6,1,1,0,0,180,45.1,29.6);

	this.instance_26 = new lib.DeckingFlipped();
	this.instance_26.setTransform(-0.1,-7.6,1,1,0,0,0,45.1,29.6);

	this.instance_27 = new lib.Decking();
	this.instance_27.setTransform(-0.2,5.4,1,1,0,0,180,45.1,29.6);

	this.instance_28 = new lib.DeckingFlipped();
	this.instance_28.setTransform(-0.1,18.4,1,1,0,0,0,45.1,29.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14}]},5).to({state:[]},1).to({state:[{t:this.instance_22},{t:this.instance_21},{t:this.instance_20,p:{y:-2}}]},5).to({state:[{t:this.instance_23},{t:this.instance_20,p:{y:-1.5}}]},1).to({state:[{t:this.instance_24},{t:this.instance_20,p:{y:-30}}]},1).to({state:[{t:this.instance_28},{t:this.instance_27},{t:this.instance_26},{t:this.instance_25},{t:this.instance_20,p:{y:-28.6}}]},1).wait(1));

	// Layer 1
	this.instance_29 = new lib.pallette();
	this.instance_29.setTransform(-1.2,28.3,1,1,0,0,0,44.9,30.1);
	this.instance_29._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(1).to({_off:false},0).wait(14));

	// hit
	this.hit_mc = new lib.pallet_hit_area();
	this.hit_mc.setTransform(0,32.4,1,1,0,0,0,49.5,28.2);
	this.hit_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.hit_mc).wait(1).to({_off:false},0).wait(14));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56,0,112,63.9);


(lib.OptionsMenu = function() {
	this.initialize();

	// Layer 2
	this.leaderboard_btn = new lib.Symbol1();
	this.leaderboard_btn.setTransform(36.1,100.1,1.26,1.26,0,0,0,24.1,24.1);
	new cjs.ButtonHelper(this.leaderboard_btn, 0, 1, 1);

	this.twitter_btn = new lib.ShareTwitter();
	this.twitter_btn.setTransform(163,343.4,1.26,1.26,0,0,0,167.8,31.7);
	new cjs.ButtonHelper(this.twitter_btn, 0, 1, 2, false, new lib.ShareTwitter(), 3);

	this.fb_btn = new lib.ShareFB();
	this.fb_btn.setTransform(163,283.3,1.26,1.26,0,0,0,167.8,31.7);
	new cjs.ButtonHelper(this.fb_btn, 0, 1, 2, false, new lib.ShareFB(), 3);

	this.sound_mc = new lib.SoundIconRoundBtncopy();
	this.sound_mc.setTransform(49.1,232,1.26,1.26,0,0,0,39.9,35.3);

	this.logout_mc = new lib.LogOutRoundBtncopy();
	this.logout_mc.setTransform(36.8,171.3,1.26,1.26,0,0,0,13.7,12.8);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("A1z9mMArnAAAQBkAAAABkMAAAA4FQAABkhkAAMgrnAAAQhkAAAAhkMAAAg4FQAAhkBkAAg");
	this.shape.setTransform(149.6,189.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F4D96F").s().p("A1zdmQhkAAAAhjMAAAg4FQAAhjBkgBMArnAAAQBkABAABjMAAAA4FQAABjhkAAg");
	this.shape_1.setTransform(149.6,189.5);

	// Layer 3
	this.mc_bg = new lib._50Percent_white();
	this.mc_bg.setTransform(480,270.4,1,1,0,0,0,500,285.9);

	this.addChild(this.mc_bg,this.shape_1,this.shape,this.logout_mc,this.sound_mc,this.fb_btn,this.twitter_btn,this.leaderboard_btn);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-20,-15.5,1000,571.9);


(lib.menuMC = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 1
	this.menu_btn = new lib.OptionsBtn();
	this.menu_btn.setTransform(24.9,24.9,0.712,0.712,0,0,0,34.9,34.9);
	new cjs.ButtonHelper(this.menu_btn, 0, 1, 2, false, new lib.OptionsBtn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.menu_btn).wait(2));

	// Layer 2
	this.menu_mc = new lib.OptionsMenu();
	this.menu_mc.setTransform(127.6,176,1,1,0,0,0,139.6,185.5);
	this.menu_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.menu_mc).wait(1).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.4,-1.4,55.4,55.4);


(lib.LucyOptionsYard = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 3
	this.instance = new lib.HardHatWhite();
	this.instance.setTransform(102.3,81,2.17,2.354,0,-3.3,0,43.1,29.6);

	this.instance_1 = new lib.HardHat();
	this.instance_1.setTransform(102.2,81.1,2.17,2.354,0,-3.3,0,43.1,29.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance_1}]},1).to({state:[]},2).wait(1));

	// Layer 1
	this.instance_2 = new lib.Lucy_Head();
	this.instance_2.setTransform(86.1,119.3,1,1,0,0,0,87.7,89.3);

	this.instance_3 = new lib.LucyHaircopy();
	this.instance_3.setTransform(83.8,107.7,1,1,0,0,0,84,107.7);

	this.instance_4 = new lib.LucyHead();
	this.instance_4.setTransform(90.3,146.6,1,1,0,0,0,69.8,62);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E5C45A").s().p("Ai+DJQgQgSgMgWQgjhBAAhfQAAhpAFgaQAOhCA/hHQEHgnBqCIQA4BJAABhQAABMgZA9QgNAhgUAfQgZAmglAjIiFACQh9AAhChLg");
	this.shape.setTransform(30,164.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AkSDQIgHgLQgbgygGhBIAAhNQAAi7BjheQBXhSCAAPQB9APBbBdQBiBlAACKQAABfglBdIgHAQQg/CKiDAAQkIAAhWiKgAirkEQg+BHgOBCQgFAagBBpQABBeAjBCQAMAWAQASQBBBLB+AAICEgDQAlgjAZglQAVgfANghQAZg+AAhNQgBhfg4hJQhShqiwAAQgzAAg8AJg");
	this.shape_1.setTransform(29.9,163.9);

	this.instance_5 = new lib.LucyHair();
	this.instance_5.setTransform(83.8,107.7,1,1,0,0,0,84,107.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance_4},{t:this.instance_3}]},1).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance_4},{t:this.instance_5}]},3).wait(1));

	// Layer 2
	this.instance_6 = new lib.LucyClothes();
	this.instance_6.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_7 = new lib.FemaleYard_03();
	this.instance_7.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_8 = new lib.FemaleYard_02();
	this.instance_8.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_9 = new lib.FemaleYard1();
	this.instance_9.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_10 = new lib.KateSkirt4();
	this.instance_10.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6}]}).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.6,30,175.4,326.9);


(lib.Lives = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Life
	this.instance = new lib.Life();
	this.instance.setTransform(110.5,35.5,1,1,0,0,180,14,14);

	this.instance_1 = new lib.LifeDead();
	this.instance_1.setTransform(110.5,35.5,1,1,0,0,180,14,14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},3).wait(1));

	// Life
	this.instance_2 = new lib.Life();
	this.instance_2.setTransform(154.3,35.5,1,1,0,0,180,14,14);

	this.instance_3 = new lib.LifeDead();
	this.instance_3.setTransform(154.3,35.5,1,1,0,0,180,14,14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},2).wait(2));

	// Life
	this.instance_4 = new lib.Life();
	this.instance_4.setTransform(198.2,35.5,1,1,0,0,180,14,14);

	this.instance_5 = new lib.LifeDead();
	this.instance_5.setTransform(198.2,35.5,1,1,0,0,0,14,14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4}]}).to({state:[{t:this.instance_5}]},1).wait(3));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("AMlEPIAAAtIgvAAI3uAAIgsAAIAAgtIAAoYIAAgyIAsAAIXuAAIAvAAIAAAtIAAAFg");
	this.shape.setTransform(154.4,35.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F4D96F").s().p("Ar2EPIAAodIXtAAIAAIdgArJgLIAACgIAABOIWVAAIAAjuIAAjVI2VAAg");
	this.shape_1.setTransform(154.2,35.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#474747").s().p("ArKB3IAAhNIAAigIWVAAIAADtg");
	this.shape_2.setTransform(154.4,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#666666").s().p("ArKBqIAAjTIWVAAIAADTg");
	this.shape_3.setTransform(154.4,23.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCB55E").s().p("AL1E8I3tAAIgsAAIAAgtIAAoYIAAgyIAsAAIXtAAIAwAAIAAAuIAAAEIAAIYIAAAtgAr4EPIXtAAIAAoeI3tAAg");
	this.shape_4.setTransform(154.4,35.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(72.3,2.2,164.1,66.3);


(lib.LadyBossMC2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"default":0,start:1,stop_talk:42,close:52,stop_close:64});

	// boss
	this.instance = new lib.LadyBoss("single",0);
	this.instance.setTransform(743.4,542.7,0.94,0.94,0,0,0,216.6,210.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:131.3},4).wait(4).to({mode:"synched",loop:false},0).wait(22).to({startPosition:21},0).wait(25).to({startPosition:26},0).to({y:551.3},8).to({_off:true},1).wait(1));

	// Layer 1
	this.gotit_btn = new lib.GotItButton();
	this.gotit_btn.setTransform(414.3,253.6,1,1,0,0,0,99,31.7);
	this.gotit_btn._off = true;
	new cjs.ButtonHelper(this.gotit_btn, 0, 1, 2, false, new lib.GotItButton(), 3);

	this.timeline.addTween(cjs.Tween.get(this.gotit_btn).wait(8).to({_off:false},0).to({_off:true},44).wait(13));

	// Layer 2
	this.instance_1 = new lib.LadyBoss_Bubble_Yard();
	this.instance_1.setTransform(667.1,101.9,0.142,0.142,0,0,0,394.4,13.4);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regY:13.3,scaleX:1,scaleY:1,x:668.2,y:101.7},4,cjs.Ease.get(1)).wait(44).to({regY:13.4,scaleX:0.14,scaleY:0.14,x:667.1,y:101.9},4).to({_off:true},1).wait(8));

	// Layer 4
	this.instance_2 = new lib.WhiteCover();
	this.instance_2.setTransform(554.3,105.5,1,1.04,0,0,0,296.1,221.1);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({_off:false},0).to({alpha:1},4).wait(50).to({alpha:0},5).to({_off:true},1).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(604.2,344.6,244.4,346.9);


(lib.KateOptionsYard = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 3
	this.instance = new lib.HardHat();
	this.instance.setTransform(105.5,82,2.17,2.354,0,-3.3,0,43.1,29.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({_off:true},3).wait(1));

	// Layer 4
	this.instance_1 = new lib.GHAIR();
	this.instance_1.setTransform(84,107.7,1,1,0,0,0,84,107.7);

	this.instance_2 = new lib.GHAIR_helmet();
	this.instance_2.setTransform(84,107.7,1,1,0,0,0,84,107.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_1}]},3).wait(1));

	// Layer 1
	this.instance_3 = new lib.HeadG();
	this.instance_3.setTransform(90.5,146.6,1,1,0,0,0,69.8,62);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// Layer 2
	this.instance_4 = new lib.KateClothes();
	this.instance_4.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_5 = new lib.FemaleYard_03();
	this.instance_5.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_6 = new lib.FemaleYard_02();
	this.instance_6.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_7 = new lib.FemaleYard1();
	this.instance_7.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_8 = new lib.KateSkirt4();
	this.instance_8.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4}]}).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,171.6,356.9);


(lib.Jason_options = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 3
	this.instance = new lib.HardHatWhite();
	this.instance.setTransform(91.9,33.6,1.89,1.89,0,0,0,43.1,29.6);

	this.instance_1 = new lib.HardHat();
	this.instance_1.setTransform(91.9,33.6,1.89,1.89,0,0,0,43.1,29.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_1}]},1).to({state:[]},1).wait(1));

	// Layer 2
	this.instance_2 = new lib.Jason();
	this.instance_2.setTransform(76.9,79.1,1,1,0,0,0,69.4,79.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 1
	this.instance_3 = new lib.Male_Body_Frank_Front();
	this.instance_3.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_4 = new lib.YardSteve_01();
	this.instance_4.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_5 = new lib.YardSteve_02();
	this.instance_5.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_6 = new lib.YardSteve_03();
	this.instance_6.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.instance_7 = new lib.YardSteve04copy2();
	this.instance_7.setTransform(84.4,209.4,1,1,0,0,0,84.4,77.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3}]}).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,168.9,287.2);


(lib.JaneOptionsYard = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 3
	this.instance = new lib.HardHatWhite();
	this.instance.setTransform(100.2,84.1,2.17,2.354,0,-3.3,0,43.1,29.6);

	this.instance_1 = new lib.HardHat();
	this.instance_1.setTransform(100.2,84.1,2.17,2.354,0,-3.3,0,43.1,29.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance_1}]},1).to({state:[]},2).wait(1));

	// Layer 1
	this.instance_2 = new lib.Jane_Head();
	this.instance_2.setTransform(94.6,122.9,1,1,0,0,0,89.4,85.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5));

	// Layer 2
	this.instance_3 = new lib.JaneClothes();
	this.instance_3.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.instance_4 = new lib.FemaleYard_03();
	this.instance_4.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_5 = new lib.FemaleYard_02();
	this.instance_5.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_6 = new lib.JaneYard1();
	this.instance_6.setTransform(93.8,272.8,1,1,0,0,0,78.9,83.9);

	this.instance_7 = new lib.JaneSkirt4();
	this.instance_7.setTransform(92.7,272.5,1,1,0,0,0,78.9,83.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3}]}).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(5.2,37.2,178.8,319.7);


(lib.InstructionsControls = function() {
	this.initialize();

	// Layer 1
	this.gotit_btn = new lib.GotItBtn();
	this.gotit_btn.setTransform(97.3,185.8,1,1,0,0,0,99,31.7);
	new cjs.ButtonHelper(this.gotit_btn, 0, 1, 2, false, new lib.GotItBtn(), 3);

	this.text = new cjs.Text("here are your\n forklift truck\n controls", "28px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 35;
	this.text.lineWidth = 243;
	this.text.setTransform(96,26,1.18,1.18);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("A4dtuMAw6AAAQCOAAAACNIAAXDQAACNiOAAMgw6AAAQiNAAAAiNIAAiGIAAx+IAAi/QAAiNCNAAg");
	this.shape.setTransform(98.6,91.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EDED5E").s().p("A4dNvQiNAAAAiNIAAiGIAAx+IAAi/QAAiNCNAAMAw6AAAQCOAAAACNIAAXDQAACNiOAAg");
	this.shape_1.setTransform(98.6,91.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.502)").s().p("A4dNuQiNAAAAiOIAAiGIAAx7IAAi+QAAiOCNAAMAw6AAAQCOAAAACOIAAUsIAACTQAACOiOAAg");
	this.shape_2.setTransform(107.1,100.1);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.text,this.gotit_btn);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-74.2,1.8,352.1,217.8);


(lib.Instructions = function() {
	this.initialize();

	// Layer 1
	this.gotit_btn = new lib.GotItBtn();
	this.gotit_btn.setTransform(204.3,294.4,1,1,0,0,0,99,31.7);
	new cjs.ButtonHelper(this.gotit_btn, 0, 1, 2, false, new lib.GotItBtn(), 3);

	this.text = new cjs.Text("load the required pallets into the delivery\ntruck before the time runs out. avoid\nthe pot holes and people. you can load\n2 small pallets or 1 large pallet at a time.\nyou only have 3 lives, so be careful!", "24px 'Laffayette Comic Pro'");
	this.text.textAlign = "center";
	this.text.lineHeight = 31;
	this.text.lineWidth = 564;
	this.text.setTransform(198.4,62.1,1.18,1.18);

	this.instance = new lib.InstBox();
	this.instance.setTransform(203.6,143.8,1,1,0,0,0,380.2,147.2);

	this.addChild(this.instance,this.text,this.gotit_btn);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-144.6,28.6,703.6,299.5);


(lib.infopanel_mc = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{endoflevel:4});

	// Layer 3
	this.gotit_btn = new lib.GotItBtn();
	this.gotit_btn.setTransform(99,285.7,1,1,0,0,0,99,31.7);
	new cjs.ButtonHelper(this.gotit_btn, 0, 1, 2, false, new lib.GotItBtn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.gotit_btn).wait(10));

	// Layer 2
	this.info_txt = new cjs.Text("", "26px 'Laffayette Comic Pro'");
	this.info_txt.name = "info_txt";
	this.info_txt.textAlign = "center";
	this.info_txt.lineHeight = 33;
	this.info_txt.lineWidth = 245;
	this.info_txt.setTransform(97.4,26,1.18,1.18);

	this.endoflevel_mc = new lib.endofleveltext_mc();
	this.endoflevel_mc.setTransform(116,172,1,1,0,0,0,265.9,146);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.info_txt}]}).to({state:[{t:this.endoflevel_mc}]},4).wait(6));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4,1,1).p("A4dtuMAw6AAAQCOAAAACNIAAKiIAAMhQAACNiOAAMgw6AAAQiNAAAAiNIAAiGIAAqbIAAnjIAAi/QAAiNCNAAg");
	this.shape.setTransform(98.6,91.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EDED5E").s().p("A4dNvQiNAAAAiNIAAiGIAAqbIAAnjIAAi/QAAiNCNAAMAw6AAAQCOAAAACNIAAKiIAAMhQAACNiOAAg");
	this.shape_1.setTransform(98.6,91.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(4,1,1).p("A4d1OMAw6AAAQCOAAAACNIAAKKIAAPWIAAMjQAACNiOAAMgw6AAAQiNAAAAiNIAAiGIAAqdIAAvWIAAnLIAAi/QAAiNCNAAg");
	this.shape_2.setTransform(98.6,139.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EDED5E").s().p("A4dVPQiNAAAAiNIAAiGIAAqdIAAvWIAAnLIAAi/QAAiNCNAAMAw6AAAQCOAAAACNIAAKKIAAPWIAAMjQAACNiOAAg");
	this.shape_3.setTransform(98.6,139.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(4,1,1).p("EgrNgdCMApLAAAMAtPAAAQCOAAAACNIAAKKIAAJcIAAS4IAACqIAAMjQAACNiOAAMgtPAAAMgpLAAAQiNAAAAiNIAAiGIAAqdIAAiqIAAy4IAApcIAAnLIAAi/QAAiNCNAAg");
	this.shape_4.setTransform(116.6,189.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EDED5E").s().p("AiCdDMgpLAAAQiNAAAAiNIAAiGIAAqdIAAiqIAAy4IAApcIAAnLIAAi/QAAiNCNAAMApLAAAMAtPAAAQCOAAAACNIAAKKIAAJcIAAS4IAACqIAAMjQAACNiOAAg");
	this.shape_5.setTransform(116.6,189.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).to({state:[{t:this.shape_5},{t:this.shape_4}]},3).wait(6));

	// Layer 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(0,0,0,0.502)").s().p("A4dNuQiNAAAAiOIAAiGIAArsIAAmPIAAi+QAAiOCNAAMAw6AAAQCOAAAACOIAAJNIAALfIAACTQAACOiOAAg");
	this.shape_6.setTransform(107.1,100.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(0,0,0,0.502)").s().p("A4dVOQiNAAAAiOIAAiGIAAruIAAvXIAAl2IAAi+QAAiOCNAAMAw6AAAQCOAAAACOIAAI0IAAPXIAALhIAACTQAACOiOAAg");
	this.shape_7.setTransform(107.1,148.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(0,0,0,0.502)").s().p("AjXdCMgn2AAAQiNAAAAiOIAAiGIAAruIAAyRIAAhpIAAhpIAApcIAAl2IAAi+QAAiOCNAAMAn2AAAMAukAAAQCOAAAACOIAAI0IAAJcIAADSIAASRIAALhIAACTQAACOiOAAg");
	this.shape_8.setTransform(125.1,198.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6}]}).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},3).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-74.2,1.8,352.1,317.6);


(lib.healthandsafety_mc = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{"default":0,talk:8,"stop":39});

	// Layer 6
	this.info_txt = new cjs.Text("don't travel or turn with\nyour forks raised - it's a\nsafety hazard.", "26px 'Laffayette Comic Pro'");
	this.info_txt.name = "info_txt";
	this.info_txt.textAlign = "center";
	this.info_txt.lineHeight = 32;
	this.info_txt.lineWidth = 414;
	this.info_txt.setTransform(-276.6,-158,1.18,1.18);

	this.ok_btn = new lib.OKbutton();
	this.ok_btn.setTransform(-279.9,9.1,1,1,0,0,0,95.5,31.7);
	new cjs.ButtonHelper(this.ok_btn, 0, 1, 2, false, new lib.OKbutton(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.ok_btn},{t:this.info_txt}]},13).wait(33));

	// Layer 5
	this.instance = new lib.Health_and_Safety_advisor_SB();
	this.instance.setTransform(26.2,74,0.091,0.091,0,0,0,560.6,252.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({_off:false},0).to({regX:560.3,scaleX:1,scaleY:1},5).wait(33));

	// Layer 4
	this.instance_1 = new lib.LadyBoss("single",0);
	this.instance_1.setTransform(68.7,98.7,0.455,0.455,0,0,0,216.5,210.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(8).to({mode:"synched",loop:false},0).wait(22).to({startPosition:21},0).wait(16));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3,1,1).p("APWAAQAAGWkgEfQkgEhmWAAQmVAAkgkhQkgkfAAmWQAAmWEgkfQEgkgGVAAQGWAAEgEgQEgEfAAGWg");
	this.shape.setTransform(90.1,90.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ap7J8QkJkHAAl1QAAl0EJkIQEHkHF0gBQF0ABEIEHQEJEIAAF0QAAF1kJEHQkIEJl0gBQl0ABkHkJg");
	this.shape_1.setTransform(90.1,90.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCB55E").s().p("Aq1K1QkfkfgBmWQABmWEfkfQEgkfGVgBQGWABEgEfQEgEfAAGWQAAGWkgEfQkgEhmWgBQmVABkgkhgAp7p8QkJEIAAF0QAAF1EJEHQEHEJF0gBQF0ABEIkJQEJkHAAl1QAAl0kJkIQkIkHl0gBQl0ABkHEHg");
	this.shape_2.setTransform(90.1,90.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(46));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.6,-9.6,199.4,199.4);


(lib.ForkLiftDriver01 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{"Male_Steve_01":0,"Male_Steve_02":1,"Male_Greg_01":2,"Male_Greg_02":3,"Male_Jason_01":4,"Male_Jason_02":5,"Male_Frank_01":6,"Male_Frank_02":7,"Female_Kate_01":8,"Female_Kate_02":9,"Female_Jane_01":10,"Female_Jane_02":11,"Female_Lucy_01":12,"Female_Lucy_02":13,"Female_Peppa_01":14,"Female_Peppa_02":15});

	// Layer 1
	this.instance = new lib.HardHat();
	this.instance.setTransform(16.2,11.1,0.383,0.383,0,0,180,43.1,29.7);

	this.instance_1 = new lib.Male_Head_01_Front();
	this.instance_1.setTransform(19,20,0.195,0.195,0,0,180,69.4,79.2);

	this.instance_2 = new lib.Male_Body_02_Front_Driving();
	this.instance_2.setTransform(17.5,45.5,0.195,0.195,0,0,180,84.5,77.9);

	this.instance_3 = new lib.HardHatWhite();
	this.instance_3.setTransform(16.2,11.1,0.383,0.383,0,0,180,43.1,29.7);

	this.instance_4 = new lib.Male_Body_03_Front_Driving();
	this.instance_4.setTransform(17.5,45.5,0.195,0.195,0,0,180,84.5,77.9);

	this.instance_5 = new lib.GregHead();
	this.instance_5.setTransform(19,20,0.195,0.195,0,0,180,69.4,79.2);

	this.instance_6 = new lib.Jason();
	this.instance_6.setTransform(19,20,0.195,0.195,0,0,180,69.4,79.2);

	this.instance_7 = new lib.FrankHead();
	this.instance_7.setTransform(19,20,0.195,0.195,0,0,180,69.4,79.2);

	this.instance_8 = new lib.GHAIR_helmet();
	this.instance_8.setTransform(19.4,16.6,0.178,0.178,0,0,180,83.9,107.2);

	this.instance_9 = new lib.HeadG();
	this.instance_9.setTransform(18.2,23.5,0.178,0.178,0,0,180,70,62.2);

	this.instance_10 = new lib.FemaleYard_02Drivingcopy();
	this.instance_10.setTransform(17.6,45.9,0.178,0.178,0,0,180,79.4,84.2);

	this.instance_11 = new lib.FemaleYard_02Driving();
	this.instance_11.setTransform(17.6,45.9,0.178,0.178,0,0,180,79.4,84.2);

	this.instance_12 = new lib.Jane_Head();
	this.instance_12.setTransform(17.5,19.3,0.178,0.178,0,0,180,89.4,85.7);

	this.instance_13 = new lib.Lucy_Headcopy();
	this.instance_13.setTransform(19,18.6,0.178,0.178,0,0,180,87.7,89.4);

	this.instance_14 = new lib.Peppa_Head();
	this.instance_14.setTransform(18.6,19.4,0.178,0.178,0,0,180,77.8,85.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance,p:{regX:43.1,scaleX:0.383,scaleY:0.383,skewX:0,x:16.2,y:11.1}}]}).to({state:[{t:this.instance_4},{t:this.instance_1},{t:this.instance_3,p:{regX:43.1,scaleX:0.383,scaleY:0.383,skewX:0,x:16.2,y:11.1}}]},1).to({state:[{t:this.instance_2},{t:this.instance_5},{t:this.instance,p:{regX:43.1,scaleX:0.383,scaleY:0.383,skewX:0,x:16.2,y:11.1}}]},1).to({state:[{t:this.instance_4},{t:this.instance_5},{t:this.instance_3,p:{regX:43.1,scaleX:0.383,scaleY:0.383,skewX:0,x:16.2,y:11.1}}]},1).to({state:[{t:this.instance_2},{t:this.instance_6},{t:this.instance,p:{regX:43.1,scaleX:0.383,scaleY:0.383,skewX:0,x:16.2,y:11.1}}]},1).to({state:[{t:this.instance_4},{t:this.instance_6},{t:this.instance_3,p:{regX:43.1,scaleX:0.383,scaleY:0.383,skewX:0,x:16.2,y:11.1}}]},1).to({state:[{t:this.instance_2},{t:this.instance_7},{t:this.instance,p:{regX:43.1,scaleX:0.383,scaleY:0.383,skewX:0,x:16.2,y:11.1}}]},1).to({state:[{t:this.instance_4},{t:this.instance_7},{t:this.instance_3,p:{regX:43.1,scaleX:0.383,scaleY:0.383,skewX:0,x:16.2,y:11.1}}]},1).to({state:[{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_3,p:{regX:43.3,scaleX:0.386,scaleY:0.418,skewX:3.3,x:15.6,y:12.1}}]},1).to({state:[{t:this.instance_11},{t:this.instance_9},{t:this.instance_8},{t:this.instance,p:{regX:43.3,scaleX:0.386,scaleY:0.418,skewX:3.3,x:15.6,y:12.1}}]},1).to({state:[{t:this.instance_10},{t:this.instance_12},{t:this.instance_3,p:{regX:43.3,scaleX:0.386,scaleY:0.418,skewX:3.3,x:16.1,y:12.5}}]},1).to({state:[{t:this.instance_11},{t:this.instance_12},{t:this.instance,p:{regX:43.3,scaleX:0.386,scaleY:0.418,skewX:3.3,x:16.1,y:12.5}}]},1).to({state:[{t:this.instance_10},{t:this.instance_13},{t:this.instance_3,p:{regX:43.3,scaleX:0.402,scaleY:0.418,skewX:3.3,x:16.5,y:10.9}}]},1).to({state:[{t:this.instance_11},{t:this.instance_13},{t:this.instance,p:{regX:43.3,scaleX:0.402,scaleY:0.418,skewX:3.3,x:16.5,y:10.9}}]},1).to({state:[{t:this.instance_10},{t:this.instance_14},{t:this.instance_3,p:{regX:43.3,scaleX:0.386,scaleY:0.418,skewX:3.3,x:16.3,y:12.1}}]},1).to({state:[{t:this.instance_11},{t:this.instance_9},{t:this.instance_8},{t:this.instance,p:{regX:43.3,scaleX:0.386,scaleY:0.418,skewX:3.3,x:16.3,y:12.1}}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,32.6,61);


(lib.Forklift_MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"down":0,"up_start":1,"up":7,"down_start":8,"down_end":14});

	// Layer 7
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("ABPBQQgMAAAAgKQAAgEADgHQACgFABgEIAAgCIAAgBQgBgGgGgRIgDAAQgPAAgMgTQgEgFgKgXQgFgLgKgLQgLgOgLgCQgagDgeADQgWABAAgCQAAgMAcgFQAbgFAiAHQBFAOAOAtQACACABAFQACAEABAHIABABQADAGACAMQACALgBACIABAJQACAVgEAJQAAALgIAAQgCAAgCgCg");
	this.shape.setTransform(115.7,99);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(15));

	// Layer 3
	this.instance = new lib.column();
	this.instance.setTransform(85.3,79.8,1,1,0,0,0,14.2,49.4);

	this.instance_1 = new lib.column();
	this.instance_1.setTransform(67.5,67.5,1,1,0,0,0,14.2,49.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(15));

	// Layer 6
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FBD34D").s().p("AkVDYIgTgDIBXgSIAUgIQAUgFAYgIIAigNQBLgeBLgvIAFgDQCQhYAchDQAFgNAAgNIABgEQAAgPgEgCIACgDIgJgOQABgRgHg9QA8AYAgAoQgwAPgGAYIgGAfIgFARIgDANQgGAUgGAHQgWAaghAdIhCA4Qg2AthaAoQhHAfg7AKQgbAFgZAAQgnAAgIgBg");
	this.shape_1.setTransform(134.1,65.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AhGA6IgCgEQgFgIAAgOQAAgLABgCIADgDIAGADIAGADIAAAAQASgLArgnQAsgpAMAAIALAVIABAAIAKANQgHAGg9AoQgZATgNALQgNAKgBADIAAACQABAKABAEQgUgBgKgLg");
	this.shape_2.setTransform(98.9,63.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#757575").s().p("Ah9BCQhQg0ggAAIgMAAQAGgIAFgGQBahfBYg4QAbgRAVgKQA1AeBcA+QBbA9AaArIgJAFQhKAwgkAUQhAAigTAMQgTAPgKAbQhCg8hOg1g");
	this.shape_3.setTransform(115.2,9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D6B13D").s().p("AhJEMQgEhKgpgSQglgQg1AhIgMAIQgUgNgjgQQgsgTgXgFIgBgNQgEgTAFgHQAFgJAXgHQAQgFAXgEIATADQAIABAnAAQAZAAAbgFQA7gKBHgeQBagoA2gsIBCg4QAhgdAWgbQAGgIAGgUIADgNIAFgRIAGgfQAGgYAwgOQACAOAOADQgDAQADAXQADAeAIA0IABAIQAABGgXAuIgEAIQgIgKgMgGQgggNghARQg6AdgjAzQgVAggDAWQgCAOABAbIADAVQgpAAhBAjQhRAugFAaIAAgNg");
	this.shape_4.setTransform(132,78.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#595959").s().p("Aj2CsQgGgPAAgPQAAgOANgUQAMgTARgOQASgPAMABQANAAgBAVQAAAYgKAYQgNAggYAJQgIAFgIAAQgIAAgHgEgADEhrIgEgCIAAgTQAAgQAVgQQAPgMAOgDQAIADADAMQgDAPgHANQgPAbgYAAQgGAAgCgCg");
	this.shape_5.setTransform(132.5,102.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AkAK7IgEAAQgIAAg3gpIhKg3IAAgWIAAgDQAAg0AphIQAhg8AigYIgUgOIgBAAQgGAEgFACQgJgKgFgSQgFgNABgHQAAgrAVgTIABAAQAlggBoABIAGgFQgCgNAAgSIABgeQADgtALhEIgPACQgQAAgJgEIgDgCQAAgBgBgBQAAAAgBAAQAAAAgBAAQAAAAAAAAIg2AmIgaARIgdASQgLgCgDgCIgCgEQgCgFAAgJIAAgCQAAgDANgLQANgKAcgTQA9gqAGgEIgKgNIAAAAIgLgWQgMAAgtAoQgsApgSALIgBAAIgFgDIgHgEIgGgFQAPgRAYgYIANgKIAJgIIABgBQAhgeARgMIgEgFIgCgDIgEADIg9AmQg8AhgOAAQgPAAgDgDQgCgDgEgDQAUgMB+hfIAPgFQASgHANAAQAJAAAWAFIAYAFQASAJANARQAHAHAFAJQACgJAGg4IAEgZIAJh/IgfgeQgfgehhg4Qhgg5gFgFQgGgGgEgHQgHAMgCAQQgGCQADAnQAIBXgBANQgIAFgJACQgNgTgFgfQgBgOAAgkQAAg2AIieQAKg6AagiQA5hNCvhBQgCgDASACQARABABAFQAvAGBvBNQBxBMANAUIAOAUIABADQACAAACAEQAEAGAAAGQACAEABAJQACAIgCABIgCACIgBAAIANAjQAEASADAkQANBBAKApIAAAEIALAhQBTA4BAA+QABgBAFAFQAFAFACAEIACAEQAOAjADAeQABAKAABBQAAA9gGAgQgLA7geAbQgCAwgZAwQgqBOhRAAQggAAgRgMQg9AWgxAgQhDArgPAHQgKgBgEgCQgDgDgBgJQgBBEgXAsQgfA7hXAzQgVALgVAAQgVAAgVgLgAiiG9QglAfgcAqQgdAtAAAgQAAAgAOAhQAiATAigWQA2gTAdhIQAVg0ACg2QABgugdgBIgBAAQgaAAgnAggAksHCQgpBCgNBCQAbAFAZAVIAJAKIAAgPQAAhEAnhCQATgdAVgXQgUgJgagRQgKAQgeArgAhLFpQApASAEBJIAAAOQAFgbBRgtQBAgjAqAAIgDgWQgBgaACgOQADgWAVggQAjgzA6gfQAhgRAfAMQAMAHAJAJIAEgIQAXgtAAhHIgBgHQgIgzgEgeQgCgWACgQQgNgDgCgPQghgog7gYQAHA9gBARIAIAOIgBADQAEACgBAPIAAAEQgBAMgFAOQgbBBiQBaIgFADQhNAvhKAeIghANQgYAIgVAFIgTAIIhXASQgXAEgQAFQgXAHgFAJQgFAHAEATIABANQAXAEAsAUQAjAPAUANIAMgHQAjgWAdAAQAOAAAMAFgAD5DrQgvAkAAAlIAAAqIAJAEQAEADAPAAQA1AAAhg6QARgfAFggQgHgcgRgEQggAFghAagAh6BTQgLA2gCAnIgBARIAXgEQAYgGAZgIQAmgOAmgUIAegQIAYgNIApgWQAhgMAqg5IAJgMIAFgIQAUgcASgRIAKgIIgCgEIABgEIgIgkIgLgqQgSg5gDgXQgEgjgMgmIACgCQgFgMAAgTQAAgGgFgfQgOhGAAgdIgDAAIgGAAIAAAAIgCABQgPADgRAKQgJAFgPAMIgnAcQgnAZgNAMIgIAGQgXAPgIAHQgVAMgJAKIgLAOQgFAHAAACIABACIgBAEQgBAAAAABQAAAAAAABQAAAAABAAQAAAAAAABIgBAKIAAAAgAjghRQAPAEAPANQARAPAFARIAAAgIAIABIABgGIAAgKIAAgLQAAgTgJgPQgNgWgZAAQgIAAgGABgAj7m7QBPA0BEA8QAJgaAUgPQASgNA+ghQAkgVBKgwIAJgFQgZgshcg+Qhag+g3gdQgUAKgbARQhZA3hZBhQgGAHgFAIIALAAIABAAQAhAABOA0gAC1m0IAAABIAAgCIAAABgACtnLIAAgBIgBAAIABABgAgQlXIgCABIAAAAIACgBg");
	this.shape_6.setTransform(127.8,60.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#333333").s().p("AjpFvQgPghAAggQAAggAegtQAcgqAlgfQAoggAZAAQAdABgBAuQgBA2gVA0QgdBIg3ATQgSAMgSAAQgQAAgPgJgAiZDMQgSAOgLATQgOAUAAAPQAAAOAHAPQAPAJAPgKQAYgJANggQAKgYAAgYQABgUgNgBIgBAAQgLAAgRAOgAkmE7QgYgVgcgFQAOhCAohCQAfgrAKgQQAaARAUAJQgWAXgSAdQgoBCAABEIAAAPIgJgKgADbA9IgJgEIAAgqQABgjAvgkQAhgaAggFQAQAFAIAbQgGAhgRAcQghA6g0AAQgPAAgFgDgAENgfQgVAQAAAPIAAATIAEACQACABAGAAQAZAAAOgYQAHgOADgOQgDgNgIgCQgOACgPAMgAijklIAAggQgGgRgRgPQgOgNgPgEQAFgBAIAAQAaAAANAWQAJAPAAATIAAALIAAAKIgCAIIgHgDgAlBkmIgBgNQABgJACgFIA9gmIAFgDIACADIADAFQgRANggAdIgCABIgIABIAAABIAAAGIgNAMIgBgDg");
	this.shape_7.setTransform(126.9,89.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(15));

	// avatar
	this.avatar_mc = new lib.ForkLiftDriver01();
	this.avatar_mc.setTransform(113,42.9,1,1,0,0,0,16.9,30.3);

	this.timeline.addTween(cjs.Tween.get(this.avatar_mc).wait(15));

	// Layer 10
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#494949").s().p("AhOAJQgNgzgCgmQAKALAVABIACAEQADACALACIAdgSIBuBAIgBAcQAAASACANIgGAFQhmgBglAgIgBAAQgPgZgLgvg");
	this.shape_8.setTransform(101.2,77.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#D6B13D").s().p("AhVBhQgFgPgDgEQgEgFgGgEQAUgJAggYQAsghANgPQAHgIARgnQANgcATgOIACgBIACAAQATgEATACIAAAGQgigHgTAbQgIAGgFAKIgBACQgLAPgDASIgGAjIgBAWIgdAVIgTAMQgJAHgFANIgBACQgUAVgIAAQgHAAgDgJg");
	this.shape_9.setTransform(121.2,43.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#5B5B5B").s().p("AhDAbICIhNQgpA3gfALIgqAXIgXAMg");
	this.shape_10.setTransform(139,67.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#757575").s().p("AgzBsIhWgyIAWADQBAADAggYQAegZAAgbIAAgCQABg2gCgoICABIQgTASgUAbIgFAIIgJAMIiIBQg");
	this.shape_11.setTransform(137.4,59.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#333333").s().p("AghB+QADgngNhDQgMhCAUACQAGABAFgCQAEgBADABIAOANQAHAJAJAEQATAQAdAEIBVAxIAAABIgBAYIgeAQQgpAVglANQgZAJgWAFIgXAFIAAgSgAi0AqIAagRIA1gkQABgBAAAAQAAAAABAAQAAABABAAQAAAAABABIADADQAJADAQAAIAPgBQgLBCgDAtgAAcg7QgLgNAAgOQAAgTAMgOQANgRAagEIACgDIAEACIACAAIABABIAAABIBoA4IgGAIQgFAGgKAFIgGADQgEAFgQAHIgKADQgPAFgQAAIgMABQgkAAgRgTg");
	this.shape_12.setTransform(117.5,65.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#1C1C1C").s().p("AgvAiIgBgBIgBgBIgBAAIgEgCIACgCIAAAAQAAAAgBAAQAAAAAAgBQAAAAAAgBQAAgBAAgBQAFgWAAgYQAAgUALgWQARgjAgAPIABACIAAABQAFADADAGQAHAKAIAhIACAMQAEARAEAkQAFAsAEAKg");
	this.shape_13.setTransform(130.1,47.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAGC7IgUgDQgdgFgTgQQgJgEgJgKIgOgMQgDgFABgCIADgFQAAgLAEAEIAAAAIgGgXQAAgWAJgZIABgCQAFgNAIgHIATgMIAggTIABgWIAFglQADgSALgPIABgCQAEgKAIgGQATgbAhAHIAKACIAGADIADACQASAPAIAaIANAqQAIAfACAeQADAogCA2IAAACQAAAdgfAZQgcAVg1AAIgPAAgAgCAlIgCACIgDADQgaADgNARQgMAOAAATQAAAPALAMQARATAiAAIAMAAQAQAAAPgFIAKgEQAQgGAEgFIAGgDQAKgFAFgGIAGgJQgEgKgFgsQgEgjgDgRIgDgMQgIghgGgLQgEgFgEgEIgBAAIgBgCQghgPgSAiQgKAWAAAUQAAAYgEAWQAAACAAAAQAAABAAABQABAAAAABQAAAAAAAAgAjnAuQgBAAAAgFQAAgBAAgBQAAgBABAAQAAgBAAAAQAAAAAAgBQACgCAHAAQALAAAYgMIgCAAIgGgFIAAgBQgGgFAAgCQgdASgSAGQgGgFAAgCIAngVQAsgWAXgRIAYgUIAVgSQAVgSAHgDIAKgDIABABIAAACIACgBIABgHIABgFIADgCIgBgCQAAgFAmgdQAkgdAVgKQAXgGAcAGIAKABIASAFQAEAAAEADIAHADIBkA3QAGALAIABQADAOACAPIAFAZQgPgHgqgaIg0geIglgTIgOgDIgPgDIgQgDQgTgCgTAEIgBAAIgCABIgVALQgHAKgPANQgRAQgMAIIABAAIgQAQQgWAVgjAeQgPAMgiAUIgCACIgMAIQgfASgIAAgAi4AJIABgBIAAAAIgBABgAhahAQADACABAEIAJgHIAHgFIAAAAIgGgBIgBgDIAAAAIgNAKg");
	this.shape_14.setTransform(125.2,46.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FAD34D").s().p("ABOAuQgCgggIgdIgNgqQgIgbgSgOIgDgDIgGgCIgKgCIAAgGIARADIAOADIAOACIAlATIA0AfQAqAaAPAGIACAOQACALAFAJQAFAMADALQADAMADAaQACAKgGAXQABABgFACIgKAIgAjTA4QgKgCgCgFQAhgWAPgNQAjgbAWgVIAQgQIgBAAQAMgJARgPQAPgOAJgJIAVgMQgUAOgMAcQgSAngHAJQgOAQgsAgQghAYgTAJQgGgEgJgCgAiDgkIAAgIIABADIAGAAIAAAAIgHAGgAh0h1IAAAEQABAJAFADIgBAHIgDASIgBAEIgBAHIgCACIAAgDIgBgBIgKAEQADgYAKgeg");
	this.shape_15.setTransform(130.6,43.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8}]}).wait(15));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.7,-10.8,173.1,166.7);


(lib.Fork_Lift_Forks = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// pallet
	this.pallet_mc = new lib.pallette_mc();
	this.pallet_mc.setTransform(-10,86.4,1,1,0,0,0,44.9,55);

	this.timeline.addTween(cjs.Tween.get(this.pallet_mc).wait(1).to({x:99.9},0).wait(1).to({x:99.7,y:23.7},0).wait(1).to({x:-10,y:23.5},0).wait(1));

	// hit
	this.hit_mc = new lib.blank_mc();
	this.hit_mc.setTransform(-64.3,61.9,1,1,0,0,0,1,1);

	this.timeline.addTween(cjs.Tween.get(this.hit_mc).wait(1).to({x:63.4},0).wait(1).to({x:65.4,y:-2.4},0).wait(1).to({x:-65,y:-2},0).wait(1));

	// forks
	this.forks_mc = new lib.ForksFront();
	this.forks_mc.setTransform(-91.9,-54.5,0.81,0.81,0,0,0,2.6,2.3);

	this.forksB_mc = new lib.ForksBack();
	this.forksB_mc.setTransform(84.3,-33,0.81,0.81,0,0,180,2.6,2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.forks_mc,p:{skewY:0,x:-91.9,y:-54.5}}]}).to({state:[{t:this.forks_mc,p:{skewY:180,x:90.5,y:-54.2}}]},1).to({state:[{t:this.forksB_mc,p:{skewY:180,x:84.3,y:-33}}]},1).to({state:[{t:this.forksB_mc,p:{skewY:0,x:-84.8,y:-32.3}}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-93.6,-1,149.7,79.6);


(lib.Fork_Lift = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.debugF_txt = new cjs.Text("", "16px 'VAGRounded'", "#FF00FF");
	this.debugF_txt.name = "debugF_txt";
	this.debugF_txt.textAlign = "center";
	this.debugF_txt.lineHeight = 18;
	this.debugF_txt.lineWidth = 47;
	this.debugF_txt.setTransform(0.2,-46.1);

	this.debug_txt = new cjs.Text("", "16px 'VAGRounded'", "#00FF00");
	this.debug_txt.name = "debug_txt";
	this.debug_txt.textAlign = "center";
	this.debug_txt.lineHeight = 18;
	this.debug_txt.lineWidth = 47;
	this.debug_txt.setTransform(0.2,-60.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.debug_txt},{t:this.debugF_txt}]}).wait(4));

	// forkbase
	this.lift_mc = new lib.LiftBitUpright();
	this.lift_mc.setTransform(-17.2,21.9,0.81,0.81,0,0,0,46.3,43.1);

	this.timeline.addTween(cjs.Tween.get(this.lift_mc).wait(1).to({skewY:180,x:15.9,y:22.1},0).to({_off:true},1).wait(2));

	// truck
	this.truck_mc = new lib.Forklift_MC();
	this.truck_mc.setTransform(-23.4,5.5,0.81,0.81,0,0,0,84.2,65.5);

	this.truckB_mc = new lib.Forklift_MC_back();
	this.truckB_mc.setTransform(18.3,18.3,0.81,0.81,0,0,180,84.1,65.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.truck_mc,p:{regX:84.2,skewY:0,x:-23.4,y:5.5}}]}).to({state:[{t:this.truck_mc,p:{regX:84.1,skewY:180,x:21.8,y:6.3}}]},1).to({state:[{t:this.truckB_mc,p:{skewY:180,x:18.3,y:18.3}}]},1).to({state:[{t:this.truckB_mc,p:{skewY:0,x:-18.7,y:18.6}}]},1).wait(1));

	// forkbase
	this.liftB_mc = new lib.LiftBitUpright();
	this.liftB_mc.setTransform(57,-10.4,0.81,0.81,0,0,0,46.3,43.1);
	this.liftB_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.liftB_mc).wait(2).to({_off:false},0).wait(1).to({skewY:180,x:-57.4,y:-9.9},0).wait(1));

	// hitloaded
	this.hitL_mc = new lib.blank_mc();
	this.hitL_mc.setTransform(-78.9,74.5,1,1,0,0,0,1,1);

	this.timeline.addTween(cjs.Tween.get(this.hitL_mc).wait(1).to({x:80.3,y:74.6},0).wait(1).to({x:78.1,y:-12.9},0).wait(1).to({x:-76.6,y:-15},0).wait(1));

	// hit
	this.hit_mc = new lib.blank_mc();
	this.hit_mc.setTransform(-28.3,46.6,1,1,0,0,0,1,1);

	this.timeline.addTween(cjs.Tween.get(this.hit_mc).wait(1).to({x:34.6,y:48.7},0).wait(1).to({x:27.3,y:11.9},0).wait(1).to({x:-27,y:12.2},0).wait(1));

	// hitreverse
	this.hitR_mc = new lib.blank_mc();
	this.hitR_mc.setTransform(25.7,13.8,1,1,0,0,0,1,1);

	this.timeline.addTween(cjs.Tween.get(this.hitR_mc).wait(1).to({x:-23.4,y:13.1},0).wait(1).to({x:-29.3,y:47.4},0).wait(1).to({x:30.7,y:47.1},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56,-60.4,112,124.3);


(lib.DidntGetBadge = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{nobadge:0,alreadygot:9,badge:19});

	// fbtwitter
	this.twitter_btn = new lib.TW_butn();
	this.twitter_btn.setTransform(833,440.4,1,1,0,0,0,19.2,19.2);
	new cjs.ButtonHelper(this.twitter_btn, 0, 1, 1);

	this.facebook_btn = new lib.FB_butn();
	this.facebook_btn.setTransform(832.9,385.7,1,1,0,0,0,19.1,19.1);
	new cjs.ButtonHelper(this.facebook_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.facebook_btn},{t:this.twitter_btn}]}).wait(30));

	// stuff
	this.leaderboard_btn = new lib.LeaderboardButton();
	this.leaderboard_btn.setTransform(111.1,416.7);
	new cjs.ButtonHelper(this.leaderboard_btn, 0, 1, 2, false, new lib.LeaderboardButton(), 3);

	this.score_mc = new lib.FinalScorecopy();
	this.score_mc.setTransform(232,215.2,1.38,1.21,0,0,0,91.1,33.5);

	this.text = new cjs.Text("FINAL SCORE", "49px 'VAGRounded'");
	this.text.lineHeight = 60;
	this.text.lineWidth = 310;
	this.text.setTransform(105.3,127.8,0.75,0.75);

	this.instance = new lib.BadgeDeliveryTruck();
	this.instance.setTransform(652.7,290.8,0.78,0.78,10.8,0,0,221.1,218.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance},{t:this.text},{t:this.score_mc},{t:this.leaderboard_btn}]},9).wait(21));

	// stuff
	this.score_mc_1 = new lib.FinalScorecopy();
	this.score_mc_1.setTransform(608.7,193.7,1.38,1.21,0,0,0,91.1,33.5);

	this.text_1 = new cjs.Text("FINAL SCORE", "49px 'VAGRounded'");
	this.text_1.textAlign = "center";
	this.text_1.lineHeight = 60;
	this.text_1.lineWidth = 317;
	this.text_1.setTransform(298.7,167.6,0.97,0.97);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_1},{t:this.score_mc_1}]}).to({state:[]},9).wait(21));

	// text
	this.text_2 = new cjs.Text("You've earned a\ndelivery truck badge", "42px 'Laffayette Comic Pro'");
	this.text_2.lineHeight = 55;
	this.text_2.lineWidth = 527;
	this.text_2.setTransform(108.4,291.3,0.77,0.77);
	this.text_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_2).wait(19).to({_off:false},0).wait(11));

	// text
	this.text_3 = new cjs.Text("You've already earned\na delivery truck badge", "42px 'Laffayette Comic Pro'");
	this.text_3.lineHeight = 55;
	this.text_3.lineWidth = 527;
	this.text_3.setTransform(108.4,291.3,0.77,0.77);
	this.text_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text_3).wait(9).to({_off:false},0).to({_off:true},10).wait(11));

	// text
	this.text_4 = new cjs.Text("you haven't gained enough\npoints to earn a badge.\nbetter luck next time.", "49px 'Laffayette Comic Pro'");
	this.text_4.textAlign = "center";
	this.text_4.lineHeight = 60;
	this.text_4.lineWidth = 844;
	this.text_4.setTransform(481.5,301.6,0.86,0.86);

	this.timeline.addTween(cjs.Tween.get(this.text_4).to({_off:true},9).wait(21));

	// panel
	this.instance_1 = new lib.EndWhiteBoxcopy();
	this.instance_1.setTransform(482.4,288.3,1.082,1.05,0,0,0,365.2,185.9);
	this.instance_1.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(30));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(83.1,111,799,379);


(lib.BackArrowcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Arrow_Butncopy2();
	this.instance.setTransform(34.9,34.9,1,1,0,0,0,34.9,34.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AkLEMQhvhvAAidQAAicBvhvQBPhPBmgXQAqgJAsAAQCdAABvBvQBvBvAACcQAACdhvBvQg6A7hHAbQhAAZhLAAQicAAhvhvg");
	this.shape.setTransform(34.8,34.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regX:34.9,regY:34.9,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regX:34.8,regY:34.8,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 2
	this.instance_1 = new lib.shadowCircle();
	this.instance_1.setTransform(31.4,39.1,1,1,0,0,0,36.9,36.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:true},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.5,-2,77.2,77.9);


(lib.BackArrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Arrow_Butn();
	this.instance.setTransform(34.9,34.9,1,1,0,0,0,34.9,34.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AkLEMQhvhvAAidQAAicBvhvQBPhPBmgXQAqgJAsAAQCdAABvBvQBvBvAACcQAACdhvBvQg6A7hHAbQhAAZhLAAQicAAhvhvg");
	this.shape.setTransform(34.8,34.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{regX:34.9,regY:34.9,scaleX:1,scaleY:1}}]}).to({state:[{t:this.instance,p:{regX:34.8,regY:34.8,scaleX:0.85,scaleY:0.85}}]},2).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 2
	this.instance_1 = new lib.shadowCircle();
	this.instance_1.setTransform(39.1,39.1,1,1,0,0,0,36.9,36.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:true},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-2,77.9,77.9);


(lib.topbar_mc = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{workwear:0,intro:4,game:9,gameover:14,leaderboard:19});

	// Close Game
	this.confirm_mc = new lib.Sure_you_wanna_quit();
	this.confirm_mc.setTransform(506.7,184.7,1,1,0,0,0,301.7,173.7);
	this.confirm_mc.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.confirm_mc).wait(50));

	// menu + close
	this.menu_mc = new lib.menuMC();
	this.menu_mc.setTransform(49.9,38.6,1,1,0,0,0,26.3,26.3);

	this.quit_btn = new lib.CloseX();
	this.quit_btn.setTransform(919.7,38.2,0.712,0.712,0,0,180,34.9,34.9);
	new cjs.ButtonHelper(this.quit_btn, 0, 1, 2, false, new lib.CloseX(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.quit_btn},{t:this.menu_mc}]}).wait(50));

	// topbar stuff
	this.btnHighlight = new lib.Highlight();
	this.btnHighlight.setTransform(844.4,36.9,1,1,0,0,0,24.8,24.8);

	this.next_btn = new lib.BackArrowcopy();
	this.next_btn.setTransform(844.5,37.2,0.712,0.712,0,0,180,34.9,34.9);
	new cjs.ButtonHelper(this.next_btn, 0, 1, 2, false, new lib.BackArrowcopy(), 3);

	this.text = new cjs.Text("CHOOSE APPROPRIATE WORKWEAR", "27px 'VAGRounded'", "#FFFFFF");
	this.text.textAlign = "center";
	this.text.lineHeight = 33;
	this.text.lineWidth = 432;
	this.text.setTransform(478.8,19.7,1.29,1.29);

	this.back_btn = new lib.BackArrow();
	this.back_btn.setTransform(118.3,37.2,0.712,0.712,0,0,0,34.9,34.9);
	new cjs.ButtonHelper(this.back_btn, 0, 1, 2, false, new lib.BackArrow(), 3);

	this.level_mc = new lib.levelText();
	this.level_mc.setTransform(373.5,45.5,1,1,0,0,0,231.9,28.7);

	this.lives_mc = new lib.Lives();
	this.lives_mc.setTransform(704,36.1,0.789,0.789,0,0,0,91,33.5);

	this.timer_mc = new lib.Time();
	this.timer_mc.setTransform(551.4,36.1,1,0.789,0,0,0,91.1,33.5);

	this.score_mc = new lib.Score();
	this.score_mc.setTransform(382.4,36.1,1,0.789,0,0,0,91.1,33.5);

	this.gameover_mc = new lib.gameovertext_mc();
	this.gameover_mc.setTransform(495.3,45.5,1,1,0,0,0,239.7,28.7);

	this.replay_btn = new lib.REPLAY();
	this.replay_btn.setTransform(116.8,36,0.712,0.712,0,0,0,34.9,34.9);
	new cjs.ButtonHelper(this.replay_btn, 0, 1, 2, false, new lib.REPLAY(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.back_btn},{t:this.text,p:{x:478.8,y:19.7,text:"CHOOSE APPROPRIATE WORKWEAR",font:"27px 'VAGRounded'",lineHeight:33,lineWidth:432}},{t:this.next_btn},{t:this.btnHighlight}]}).to({state:[{t:this.back_btn},{t:this.text,p:{x:485.9,y:12.1,text:"THE DELIVERY TRUCK",font:"41px 'VAGRounded'",lineHeight:47,lineWidth:462}}]},4).to({state:[{t:this.score_mc},{t:this.timer_mc},{t:this.lives_mc},{t:this.level_mc}]},5).to({state:[{t:this.replay_btn},{t:this.gameover_mc}]},5).to({state:[{t:this.back_btn},{t:this.text,p:{x:513.5,y:19,text:"THE DELIVERY TRUCK LEADERBOARD",font:"28px 'VAGRounded'",lineHeight:34,lineWidth:535}}]},5).wait(31));

	// panel
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#4F3F5B","#4F2A40"],[0,1],0,-35,0,35.1).s().p("EhLxAF3IAArtMCXjAAAIAALtg");
	this.shape.setTransform(485,37.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(50));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1025,154);


// stage content:
(lib.deliverytruck = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{ww0:4,ww1:5,instructions:9,instructions_stop:14,controls:19,controls_stop:24,"game":34,"gameover":44,"leaderboard":54});

	// timeline functions:
	this.frame_0 = function() {
		InitGame();
	}
	this.frame_4 = function() {
		initAvatar(true);
	}
	this.frame_5 = function() {
		initAvatar(false);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4).call(this.frame_4).wait(1).call(this.frame_5).wait(65));

	// white fade
	this.fade_mc = new lib.fadeFromWhite();
	this.fade_mc.setTransform(473.9,271.2,1,1,0,0,0,499.3,292.1);

	this.timeline.addTween(cjs.Tween.get(this.fade_mc).wait(70));

	// topbar
	this.topbar_mc = new lib.topbar_mc();
	this.topbar_mc.setTransform(483,35.5,1,1,0,0,0,485,37.5);

	this.timeline.addTween(cjs.Tween.get(this.topbar_mc).wait(70));

	// grafters cup
	this.newmessage_btn = new lib.newmessage_btn();
	this.newmessage_btn.setTransform(484.4,486.6,1,1,0,0,0,152.7,27.7);
	new cjs.ButtonHelper(this.newmessage_btn, 0, 1, 2, false, new lib.newmessage_btn(), 3);

	this.cup_mc = new lib.WinnersCupMC();
	this.cup_mc.setTransform(0,480);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.cup_mc},{t:this.newmessage_btn}]},44).wait(26));

	// controls panel
	this.controls_mc = new lib.PanelStrip();
	this.controls_mc.setTransform(29.2,234.1,1,1,0,0,0,33.5,149.7);

	this.badges_mc = new lib.DidntGetBadge();
	this.badges_mc.setTransform(488.1,271.5,1,1,0,0,0,488.1,271.5);

	this.instance = new lib.PostRoomLeaderBoard();
	this.instance.setTransform(477.8,308.3,1,1,0,0,0,393.4,204.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.controls_mc}]},19).to({state:[{t:this.badges_mc}]},25).to({state:[{t:this.instance}]},10).wait(16));

	// info stuff
	this.controlsinfo_mc = new lib.InstructionsControls();
	this.controlsinfo_mc.setTransform(668.5,308.4,1,1,0,0,0,107.7,109);

	this.reminder_btn = new lib.RemnderBtn();
	this.reminder_btn.setTransform(165.9,109.7,1,1,0,0,0,99,31.7);
	new cjs.ButtonHelper(this.reminder_btn, 0, 1, 2, false, new lib.RemnderBtn(), 3);

	this.instance_1 = new lib.healthandsafety_mc();
	this.instance_1.setTransform(1188.9,-70.3,1,1,0,0,0,90,90.1);

	this.debuginfo_txt = new cjs.Text("", "16px 'VAGRounded'", "#FFFFFF");
	this.debuginfo_txt.name = "debuginfo_txt";
	this.debuginfo_txt.lineHeight = 18;
	this.debuginfo_txt.lineWidth = 219;
	this.debuginfo_txt.setTransform(440,78);

	this.instance_2 = new lib.boss_mc();
	this.instance_2.setTransform(-674.4,-95.2,0.5,0.5,0,0,0,320.6,205.5);

	this.instance_3 = new lib.infopanel_mc();
	this.instance_3.setTransform(-361.6,242,1,1,0,0,0,107.7,109);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.controlsinfo_mc}]},19).to({state:[]},10).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.debuginfo_txt},{t:this.instance_1},{t:this.reminder_btn}]},5).wait(36));

	// forklift
	this.wwinfo_mc = new lib.LadyBossMC2();
	this.wwinfo_mc.setTransform(600.2,284.5,1,1,0,0,0,493.3,84.7);

	this.forks_mc = new lib.Fork_Lift_Forks();
	this.forks_mc.setTransform(500.4,299.6,1,1,0,0,0,0.4,-0.4);

	this.forklift_mc = new lib.Fork_Lift();
	this.forklift_mc.setTransform(500,300);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.wwinfo_mc}]},4).to({state:[]},5).to({state:[{t:this.forklift_mc},{t:this.forks_mc}]},25).wait(36));

	// game bits
	this.ww_btn4 = new lib.blanksquare_btn();
	this.ww_btn4.setTransform(771.1,421.3,1,1,0,0,0,48.6,41.8);
	new cjs.ButtonHelper(this.ww_btn4, 0, 1, 2, false, new lib.blanksquare_btn(), 3);

	this.ww_btn3 = new lib.blanksquare_btn();
	this.ww_btn3.setTransform(529.1,421.3,1,1,0,0,0,48.6,41.8);
	new cjs.ButtonHelper(this.ww_btn3, 0, 1, 2, false, new lib.blanksquare_btn(), 3);

	this.ww_btn2 = new lib.blanksquare_btn();
	this.ww_btn2.setTransform(771.1,195.8,1,1,0,0,0,48.6,41.8);
	new cjs.ButtonHelper(this.ww_btn2, 0, 1, 2, false, new lib.blanksquare_btn(), 3);

	this.ww_btn1 = new lib.blanksquare_btn();
	this.ww_btn1.setTransform(529.1,195.8,1,1,0,0,0,48.6,41.8);
	new cjs.ButtonHelper(this.ww_btn1, 0, 1, 2, false, new lib.blanksquare_btn(), 3);

	this.dude2_mc = new lib.LittleDude01b();
	this.dude2_mc.setTransform(-126.5,95.1,1,1,0,0,180);

	this.dude1_mc = new lib.LittleDude01();
	this.dude1_mc.setTransform(-74.3,145,1,1,0,0,180);

	this.pothole_mc = new lib.pothole_mc();
	this.pothole_mc.setTransform(-84.2,-43.6,1.11,1.11);

	this.instance_4 = new lib.palletdescription_mc();
	this.instance_4.setTransform(-363.4,100.7);

	this.instance_5 = new lib.pallette_mc();
	this.instance_5.setTransform(-84.1,84.5,1.08,1.08,0,0,0,44.9,55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.ww_btn1},{t:this.ww_btn2},{t:this.ww_btn3},{t:this.ww_btn4}]},4).to({state:[]},5).to({state:[{t:this.instance_5},{t:this.instance_4},{t:this.pothole_mc},{t:this.dude1_mc},{t:this.dude2_mc}]},25).wait(36));

	// gui
	this.frank_mc = new lib.Frank_options();
	this.frank_mc.setTransform(187.5,302.3,1.14,1.14,0,0,0,84.4,143.7);

	this.jason_mc = new lib.Jason_options();
	this.jason_mc.setTransform(187.5,302.3,1.14,1.14,0,0,0,84.4,143.7);

	this.greg_mc = new lib.Greg_options();
	this.greg_mc.setTransform(187.5,302.3,1.14,1.14,0,0,0,84.4,143.7);

	this.steve_mc = new lib.Steve_options();
	this.steve_mc.setTransform(187.5,302.3,1.14,1.14,0,0,0,84.4,143.7);

	this.peppa_mc = new lib.PeppaOptionsYard();
	this.peppa_mc.setTransform(180,296.9,1,1,0,0,0,85.8,178.5);

	this.lucy_mc = new lib.LucyOptionsYard();
	this.lucy_mc.setTransform(180,296.9,1,1,0,0,0,85.8,178.5);

	this.jane_mc = new lib.JaneOptionsYard();
	this.jane_mc.setTransform(180,296.9,1,1,0,0,0,85.8,178.5);

	this.kate_mc = new lib.KateOptionsYard();
	this.kate_mc.setTransform(180,296.9,1,1,0,0,0,85.8,178.5);

	this.instructions_mc = new lib.Instructions();
	this.instructions_mc.setTransform(486.9,278.3,0.16,0.16,0,0,0,203.2,151.2);
	this.instructions_mc._off = true;

	this.instance_6 = new lib.ControlsExplanation();
	this.instance_6.setTransform(-109.9,311.1,1,1,0,0,0,228.1,245.1);
	this.instance_6._off = true;

	this.instance_7 = new lib.WhiteCover();
	this.instance_7.setTransform(481.5,279.3,1.686,1.321,0,0,0,296.1,221.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.steve_mc},{t:this.greg_mc},{t:this.jason_mc},{t:this.frank_mc}]},4).to({state:[{t:this.kate_mc},{t:this.jane_mc},{t:this.lucy_mc},{t:this.peppa_mc}]},1).to({state:[]},1).to({state:[{t:this.instructions_mc}]},3).to({state:[{t:this.instructions_mc}]},4).to({state:[{t:this.instance_6}]},6).to({state:[{t:this.instance_6}]},4).to({state:[]},6).to({state:[{t:this.instance_7}]},15).wait(26));
	this.timeline.addTween(cjs.Tween.get(this.instructions_mc).wait(9).to({_off:false},0).to({regY:151.3,scaleX:1,scaleY:1,x:486.8},4).to({_off:true},6).wait(51));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(19).to({_off:false},0).to({x:274.1},4).to({_off:true},6).wait(41));

	// lorry
	this.instance_8 = new lib.YardSteve03();
	this.instance_8.setTransform(784.5,441.3,1.026,1.026,0,0,0,84.3,77.7);

	this.instance_9 = new lib.YardSteve02NoFeet();
	this.instance_9.setTransform(769.1,215.8,1.039,1.039,0,0,0,84.4,77.8);

	this.instance_10 = new lib.YardSteve04copy2();
	this.instance_10.setTransform(537.9,441.3,1.026,1.026,0,0,0,84.4,77.7);

	this.instance_11 = new lib.YardSteve01();
	this.instance_11.setTransform(530.5,223.7,1.026,1.026,0,0,0,84.4,77.5);

	this.instance_12 = new lib.HardHatIsolated();
	this.instance_12.setTransform(762.9,122.7,1.46,1.46,0,0,0,43.1,29.6);

	this.instance_13 = new lib.HardHatIsolated();
	this.instance_13.setTransform(779.5,349.3,1.46,1.46,0,0,0,43.1,29.6);

	this.instance_14 = new lib.HardHatIsolatedWhite();
	this.instance_14.setTransform(528.2,131.2,1.46,1.46,0,0,0,43.1,29.6);

	this.instance_15 = new lib.FemaleYard_02();
	this.instance_15.setTransform(776.9,436.5,0.95,0.95,0,0,0,78.9,83.9);

	this.instance_16 = new lib.FemaleYard_03();
	this.instance_16.setTransform(532.2,206.1,0.95,0.95,0,0,0,78.9,83.9);

	this.instance_17 = new lib.FemaleYard1();
	this.instance_17.setTransform(533.7,443.8,0.95,0.95,0,0,0,78.9,83.9);

	this.instance_18 = new lib.GirlOutfit04();
	this.instance_18.setTransform(773.7,216.5,1.064,1.064,0,0,0,62.6,83.9);

	this.lorryRoof_mc = new lib.TruckDoorAndRoof();
	this.lorryRoof_mc.setTransform(282.4,161.5,1,1,0,0,0,169.3,151.1);

	this.lorry_mc = new lib.Truck();
	this.lorry_mc.setTransform(282.4,161.5,1,1,0,0,0,169.3,151.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_14,p:{scaleX:1.46,scaleY:1.46,skewX:0,x:528.2,y:131.2}},{t:this.instance_13,p:{scaleX:1.46,scaleY:1.46,skewX:0,x:779.5,y:349.3}},{t:this.instance_12,p:{scaleX:1.46,scaleY:1.46,skewX:0,x:762.9,y:122.7}},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8}]},4).to({state:[{t:this.instance_18},{t:this.instance_13,p:{scaleX:1.173,scaleY:1.273,skewX:-3.3,x:528.5,y:348}},{t:this.instance_17},{t:this.instance_14,p:{scaleX:1.173,scaleY:1.273,skewX:-3.3,x:534.3,y:119.1}},{t:this.instance_12,p:{scaleX:1.173,scaleY:1.273,skewX:-3.3,x:781.7,y:348}},{t:this.instance_16},{t:this.instance_15}]},1).to({state:[]},1).to({state:[{t:this.lorry_mc,p:{mode:"independent",startPosition:undefined}},{t:this.lorryRoof_mc,p:{mode:"independent",startPosition:undefined}}]},3).to({state:[{t:this.lorry_mc,p:{mode:"single",startPosition:0}},{t:this.lorryRoof_mc,p:{mode:"synched",startPosition:0}}]},35).wait(26));

	// bg
	this.sfx_mc = new lib.sfx();
	this.sfx_mc.setTransform(-106.7,82,1,1,0,0,0,11.1,11.1);

	this.instance_19 = new lib.BGBox();
	this.instance_19.setTransform(200.6,227.3,0.687,2.517,0,0,0,543.5,600.5);

	this.instance_20 = new lib.Building();
	this.instance_20.setTransform(746.9,93.8,1,1,0,0,0,247.6,171.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#3399CC").ss(0.5,1,1).p("EhL5grCMCXzAAAMAAABWFMiXzAAAg");
	this.shape.setTransform(478.4,271.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CDD1C0").s().p("EhL5ArDMAAAhWFMCXzAAAMAAABWFg");
	this.shape_1.setTransform(478.4,271.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.sfx_mc}]}).to({state:[{t:this.instance_19}]},4).to({state:[]},2).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance_20}]},3).wait(61));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(306.2,249.1,1196.8,584.2);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;