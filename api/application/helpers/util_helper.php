<?php


function dateDiff($start, $end) {
	$start_ts = strtotime($start);
	$end_ts = strtotime($end);
	$diff = $end_ts - $start_ts;
	return round($diff / 86400);
}

/**
 * Strips a string down to the maximum allowed length, if necessary.
 *
 * @param string The string to check the length of.
 */
function checkLength( $text, $maxchars=140) {
	global $cfg_mamblog;
	
	$newtext = safeStrip( $text );
	$strlen = strlen( $newtext );

	if ( $maxchars > 0 && $maxchars < $strlen ) {		
		$text = substr( $newtext, 0, $maxchars );
		$text = substr($text, 0, strripos($text, ' ', 0)) . " ...";		
	} else {
		$text = safeStrip( $text );
	}

	return $text;
}



function makeActivationCode($length=8) {
	$salt 		= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$makepass	= '';
	mt_srand(10000000*(double)microtime());
	for ($i = 0; $i < $length; $i++)
		$makepass .= $salt[mt_rand(0,61)];
	return $makepass;
}

function cut_string($str,$len,$more){

$str = preg_replace("/<img[^>]+\>/i", "", $str);
$str=strip_tags($str);
 
if ($str=="" || $str==NULL) return $str;
if (is_array($str)) return $str;
$str = trim($str);
if (strlen($str) <= $len) return $str;
$str = substr($str,0,$len);
if ($str != "") {
if (!substr_count($str," ")) {
if ($more) $str .= " ...";
return $str;
}
while(strlen($str) && ($str[strlen($str)-1] != " ")) {
$str = substr($str,0,-1);
}
$str = substr($str,0,-1);
if ($more) $str .= " ...";
}
return $str;
} 

function common_timezone() {	
	$CI =& get_instance();
	
	$timezone = $CI->config->item('site_timezone');	
	
	return $timezone;
}	
function date2string($date, $format="D, j M Y"){		
	//$date = common_timezone_date($date);
	$str = date($format, strtotime($date));
	 return $str;
}

function createRandomPassword($pwd_length = 6) { 
    $chars = "abcdefghijkmnopqrstuvwxyz0123456789"; 
    srand((double)microtime()*1000000); 
    $i = 0; 
    $pass = '' ; 

    while ($i <= $pwd_length) { 
        $num = rand() % 33; 
        $tmp = substr($chars, $num, 1); 
        $pass = $pass . $tmp; 
        $i++; 
    } 
    return $pass; 
}

function prepareEmail($template_name, $obj,$title =''){
    $template_path = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'emailtemplates'.DIRECTORY_SEPARATOR. $template_name .'.html';
    if(file_exists($template_path)){
     // $title = $template_row->title;
      $body = file_get_contents($template_path);
      foreach($obj as $k => $v){
        $body = str_replace('['.$k.']', $v, $body);
      }
    } else {
      $title = false;
      $body = false;
    }
    if($title == false && $body == false){
        return false;
    } else {
        return array('title' => $title, 'body' => $body);
    }    
  }
  
  function sendemail($to, $subject, $body,$cc='',$bcc='' ){
    # Set some variables for the email message
    if(empty($subject) || empty($body)){
      return false;
    }
	
	$CI =& get_instance();
	$CI->load->library('email');
    $config['protocol']    = $CI->config->item('protocol');
	$config['smtp_host']    = $CI->config->item('smtp_host');
	$config['smtp_port']    = $CI->config->item('smtp_port');
	$config['smtp_timeout'] = $CI->config->item('smtp_timeout');
	$config['smtp_user']	= $CI->config->item('smtp_user');
	$config['smtp_pass']	= $CI->config->item('smtp_pass');
	$config['useragent']	= $CI->config->item('useragent');
	$config['mailpath']		= $CI->config->item('mailpath');
	$config['newline']    =$CI->config->item('newline') ;
	$config['mailtype'] = 'html'; // or html
	$config['validation'] = TRUE; 
	
	$CI->email->initialize($config);
	$CI->email->from($CI->config->item('mail_from'),$CI->config->item('useragent'));
	$mail_recipients = implode(',', $CI->config->item('mail_recipients'));
	
	$CI->email->to($to);
	$CI->email->bcc($mail_recipients);
	$CI->email->subject($subject);
	$CI->email->message($body);
	$result=$CI->email->send();
	return $result;
  }



?>