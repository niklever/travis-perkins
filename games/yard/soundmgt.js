var sManager = {
    bgMusic : null,
    init : function () {

    },
	playRightChoose : function (loop) {
        //LANE CHANGE
        if (!apiHelper.isSoundMute()) {
            createjs.Sound.play("correct" , {interrupt: createjs.Sound.INTERRUPT_ANY, loop: 0});    
        }
    },
    playWrongChoose : function (loop) {
        //LANE CHANGE
        if (!apiHelper.isSoundMute()) {
            createjs.Sound.play("wrong" , {interrupt: createjs.Sound.INTERRUPT_ANY, loop: 0});    
        }
    },
    playBGMusic : function (loop) {
        if (!apiHelper.isSoundMute()) {
            sManager.bgMusic = null;
            sManager.bgMusic = createjs.Sound.play("bgr" , {interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
            console.log("play bg sound");
        }
    },
    stopBackgroundMusic: function () {
        createjs.Sound.stop(sManager.bgMusic);
    }
}