var gameWrapperObject = {
    loadPositionCanvas: function () {
        return {
            x: $('canvas').offset().left,
            y: $('canvas').offset().top
        }
    }
};

if(!(window.console && console.log)) {
  console = {
    log: function(){},
    debug: function(){},
    info: function(){},
    warn: function(){},
    error: function(){}
  };
}

var browser = {
        chrome: false,
        mozilla: false,
        opera: false,
        msie: false,
        safari: false
    };
var sUsrAg = navigator.userAgent;
if(sUsrAg.indexOf("Chrome") > -1) {
    browser.chrome = true;
} else if (sUsrAg.indexOf("Safari") > -1) {
    browser.safari = true;
} else if (sUsrAg.indexOf("Opera") > -1) {
    browser.opera = true;
} else if (sUsrAg.indexOf("Firefox") > -1) {
    browser.mozilla = true;
} else if (sUsrAg.indexOf("MSIE") > -1) {
    browser.msie = true;
}

var ob = [];

function callback(obj){
	if (obj!=null) console.log("callback " + obj);
}

var apiHelper = {
    BASEURL: "http://travisperkins.fbapphouse.com/api/",
    directMain : "http://travisperkins.fbapphouse.com/gamesv2/",
    gameId: null,
    wrapContainer: "#myGame",
    init: function (gameId) {
        apiHelper.gameId = gameId;
    },
    login: function (params, exportRoot) {
        $('.ploading').show();
        $.ajax({
            type: "POST",
            url: apiHelper.BASEURL + 'login',
            dataType: "json",
            data: {
                email: params.email,
                password: params.password
            },
            success: function (response) {
                console.log(response);
                $('.ploading').hide();
                if (response) {
                    if (response.status == "OK") {
                        $.localStorage.set('accessTokenLogin', response.data.access_token);
                        $.localStorage.set('isUser', response.data.id);

                        console.log("Save token " + $.localStorage.get('accessTokenLogin'));

                        exportRoot.btn_Login.childbtLogin.gotoAndStop(1);
                        loginLocal.removeInputBoxLogin();
                         apiHelper.callAlert('popup_login_welcome', params.email);
                        exportRoot.gotoAndPlay('LoginClose'); ///52
                        
                        //$("#isLog").attr('src',apiHelper.directMain + 'images/log-0.png');
                        //$("#isLog").attr('onclick','apiHelper.logout(exportRoot)');
                       
                    } else {
                        console.log("1");
                        apiHelper.callAlert('popup_login_no_user', response.message);
                    }
                } else {
                    console.log("2");
                    apiHelper.callAlert('popup_login_no_user', "There is error in server, please try again.");
                }
            },
			error : function(r){
				//alert("Error login");
				$('.ploading').hide();
				callback(null);
				console.log("error",r);
			}
        });
    },
    resetPass: function (params, callback) {
        $('.ploading').show();
        $.ajax({
            url: this.BASEURL + 'reset_pass',
            dataType: 'json',
            type: 'Post',
            data: {
                email: params.email
            },
            success: function (response) {
                $('.ploading').hide();
                console.log(response);
                if (response) {
                    if (response.status == "OK") {
                        // alert(response.message)
                        apiHelper.callAlert('popup_reset_valid_user', response.message);
                    } else {
                        apiHelper.callAlert('popup_reset_no_user');
                    }
                } else {
                    apiHelper.callAlert('popup_reset_no_user');
                }
            },
			error : function(r){
				//alert("Error resetPass");
				$('.ploading').hide();
				if(callback){
				callback(null);
				}
				console.log("error",r);
			}
        })
    },
    signUp: function (params, exportRoot) {
        $('.ploading').show();
        $.ajax({
            url: this.BASEURL + 'signup',
            dataType: 'json',
            type: 'Post',
            data: {
                name: params.name,
                email: params.email,
                password: params.password,
                country: params.country
            },
            success: function (response) {
                console.log(response);
                $('.ploading').hide();
                if (response) {
                    if (response.status == "OK") {
                        $.localStorage.set('accessTokenLogin', response.data.access_token);
                        $.localStorage.set('isUser',response.data.user_id);
                        exportRoot.btn_Login.childbtLogin.gotoAndStop(1);
                        loginLocal.removeInputBoxLogin();
                        apiHelper.callAlert('popup_register_complete');
                        exportRoot.gotoAndPlay('LoginClose'); ///52
                    } else {
                        apiHelper.callAlert('sign_up', "There is an error in server. Please try again!", true);
                    }
                } else {
                    apiHelper.callAlert('sign_up', "There is an error in server. Please try again!", true);
                }
            },
			error : function(r){
				//alert("Error signUp");
				$('.ploading').hide();
				callback(null);
				console.log("error",r);
			}
        })
    },
    countries: function (callback) {
        $.ajax({
            type: "GET",
            url: apiHelper.BASEURL + 'countries',
            dataType: "json",
            success: function (response) {
                if (response) {
                    if (response.status == 'OK') {
                        $.each(response.data, function (key, val) {
                            $("#selectCountry").append('<option value="' + key + '">' + val + '</option>');
                        })
                    } else {

                    }
                } else {

                }
            },
			error : function(r){
				//alert("Error countries");
				$('.ploading').hide();
				callback(null);
				console.log("error",r);
			}
        });
    },
    fblogin: function (params, exportRoot) {
        $('.ploading').show();
        $.ajax({
            type: "POST",
            url: apiHelper.BASEURL + 'fblogin',
            dataType: "json",
            data: {
                'name': params.name,
                'email': params.email,
                'facebook_id': params.facebook_id,
                'country': 94
            },
            success: function (response) {
                $('.ploading').hide();
                if (response) {
                    console.log(response);
                    if (response.status == "OK") {
                        $.localStorage.set('accessTokenLogin', response.data.access_token);
                        $.localStorage.set('isUser', response.data.id);
                        exportRoot.btn_Login.childbtLogin.gotoAndStop(1);
                        loginLocal.removeInputBoxLogin();
                        exportRoot.gotoAndPlay('LoginClose'); ///52
                    } else {
                        if (response.message == "Email is invalid. Please try again.") {
                            loginLocal.removeInputBoxLogin();
                            exportRoot.instance_6.gotoAndStop('1');
                            loginLocal.drawingCreateNewAccount();
                            $('#userName_creatAccount').val(params.name);
                            $('#email_creatAccount').val(params.email);
                            $("#submit_createAccount").click(function () {
                                loginLocal.creatAccount();
                            })
                        }

                    }
                } else {
                    apiHelper.callAlert('popup_login_no_user');
                }
            },
			error : function(r){
				//alert("Error fblogin");
				$('.ploading').hide();
				callback(null);
				console.log("error",r);
			}
        });
    },
    twlogin: function (params, callback) {


        $('.ploading').show();
        $.ajax({
            type: "POST",
            url: apiHelper.BASEURL + 'twlogin',
            dataType: "json",
            data: {
                'twitter_id': $.localStorage.get('IDTwitter'),
                'name': params.name,
                'country': params.country,
                'email': params.email
            },
            success: function (response) {
                $('.ploading').hide();
                if (response) {
                    console.log(response);
                    if (response.status == "OK") {
                        $.localStorage.removeItem('IDTwitter');
                        $.localStorage.set('accessTokenLogin',response.data.access_token);
                        $.localStorage.set('isUser',response.data.user_id);
                        exportRoot.btn_Login.childbtLogin.gotoAndStop(1);
                        loginLocal.removeInputBoxLogin();
                        exportRoot.gotoAndPlay('LoginClose');
                    } else {
                        apiHelper.callAlert('popup_login_no_user', response.message);
                    }
                } else {
                    apiHelper.callAlert('popup_login_no_user', "There is error in server. Please try again.");
                }
            },
			error : function(r){
				//alert("Error twlogin");
				$('.ploading').hide();
				callback(null);
				console.log("error",r);
			}
        });

    },
    logout: function (exportRoot) {
        var auth = btoa($.localStorage.get('isUser')+ ':' + $.localStorage.get('accessTokenLogin'));
        $('.ploading').show();
        $.ajax({
            type: "POST",
            url: apiHelper.BASEURL + 'logout',
            dataType: "json",
            headers: {
                "Authorization": "Basic " + auth
            },
            success: function (response) {
                $('.ploading').hide();
                console.log("remove session");
                $.localStorage.removeAll();
                apiHelper.gotoMainMenu();                
                
            },
			error : function(r){
				//alert("Error logout");
				$('.ploading').hide();
				callback(null);
				console.log("error",r);
			}
        });
    },
    logoutFromGame: function (callback) {
        var auth = btoa($.localStorage.get('isUser')+ ':' + $.localStorage.get('accessTokenLogin'));
        $('.ploading').show();
        $.ajax({
            type: "POST",
            url: apiHelper.BASEURL + 'logout',
            dataType: "json",
            headers: {
                "Authorization": "Basic " + auth
            },
            success: function (response) {
                console.log("remove session");
                $('.ploading').hide();
                $.localStorage.removeAll();

                callback();
            },
			error : function(r){
				//alert("Error logoutFromGame");
				$('.ploading').hide();
				callback(null);
				console.log("error",r);
			}
        });
    },
    user_profile: function (callback) {
        console.log("user_profile started");
        var auth = "";
        try{
            auth = btoa($.localStorage.get('isUser') + ':' + $.localStorage.get('accessTokenLogin'));    
        }catch(e){
            console.log("user_profile exception throw", e);    
        }
        
        $('.ploading').show();
        console.log("user_profile started request");
        $.ajax({
            type: "POST",
            url: apiHelper.BASEURL + 'user_profile',
            dataType: "json",
            headers: {
                "Authorization": "Basic " + auth
            },
            success: function (response) {
                $('.ploading').hide();
                console.log("user_profile received response.");
                console.log("user_profile response object: ",response);
                if (response) {
                    if (response.status == 'OK') {
                        callback(response.data);
                    } else {
                        console.log("user_profile response not OK");
                        //$.localStorage.removeAll();
                        callback(null);
                    }
                } else {
                    console.log("user_profile response not defined");
                    callback(null);
                }
            },
			error : function(r){
				console.log("user_profile error called",r);
				$('.ploading').hide();
				callback(null);
				
			}
        });
    },
    leaderboards: function (gameID, callback) {
        // var auth = btoa("33:{A6B671BE-DDF6-FFA9-2771-0718983D5CD8}");//btoa($.localStorage.get('isUser')+ ':' + $.localStorage.get('accessTokenLogin'));
        $.ajax({
            type: "POST",
            url: apiHelper.BASEURL + 'leaderboards',
            dataType: "json",
            data: {
                game_id: gameID
            },
            /*headers: {
                         "Authorization": "Basic " + auth
                     },*/
            success: function (response) {
                //console.log(response);
                if (response) {
                    if (response.status == 'OK') {

                    } else {

                    }
                } else {
                    apiHelper.callAlert('popup_login_no_user');
                }
                if (callback) {
                    callback(response);
                }
            },
			error : function(r){
				//alert("Error leaderboards");
				$('.ploading').hide();
				callback(null);
				console.log("error",r);
			}
        });
    },
    init_game_session: function (gameID, name) {
        var auth = btoa($.localStorage.get('isUser')+ ':' + $.localStorage.get('accessTokenLogin'));

        console.log("init session: " + auth + " , " + $.localStorage.get('isUser')+ " , " + $.localStorage.get('accessTokenLogin'));

        $.ajax({
            type: "POST",
            url: apiHelper.BASEURL + 'init_game_session',
            dataType: "json",
            data: {
                user_id: $.localStorage.get('isUser'),
                access_token: $.localStorage.get('accessTokenLogin'),
                game_id: gameID
            },
            headers: {
                "Authorization": "Basic " + auth
            },
            success: function (response) {
                console.log(response);
                if (response) {
                    if (response.status == 'OK') {
                        $.localStorage.set('gameToken',response.game_session);
                    } else {

                    }
                } else {

                }
            },
			error : function(r){
				//alert("Error init_game_session");
				$('.ploading').hide();
				callback(null);
				console.log("error",r);
			}
        });
    },

    inc_game_score: function (gameID, scose) {
        var auth = btoa($.localStorage.get('isUser')+ ':' + $.localStorage.get('accessTokenLogin'));
        $.ajax({
            type: "POST",
            url: apiHelper.BASEURL + 'inc_game_score',
            dataType: "json",
            data: {
                game_id: gameID,
                session_id: $.localStorage.get('gameToken'),
                inc_score: scose
            },
            headers: {
                "Authorization": "Basic " + auth
            },
            success: function (response) {
                console.log(response);
                if (response) {
                    if (response.status == 'OK') {

                    } else {

                    }
                } else {
                    apiHelper.callAlert('popup_login_no_user');
                }
            },
			error : function(r){
				//alert("Error inc_game_score");
				$('.ploading').hide();
				callback(null);
				console.log("error",r);
			}
        });
    },
    game_over: function (gameID, scose, callback) {
        var auth = btoa($.localStorage.get('isUser')+ ':' + $.localStorage.get('accessTokenLogin'));
        $.ajax({
            type: "POST",
            url: apiHelper.BASEURL + 'game_over',
            dataType: "json",
            data: {
                user_id: $.localStorage.get('isUser'),
                access_token: $.localStorage.get('accessTokenLogin'),
                game_id: gameID,
                session_id: $.localStorage.get('gameToken'),
                score: scose
            },
            headers: {
                "Authorization": "Basic " + auth
            },
            success: function (response) {
                console.log(response);
                if (response) {
                    if (response.status == 'OK') {

                    } else {

                    }
                } else {
                    apiHelper.callAlert('popup_login_no_user');
                }
                if (callback) {
                    callback(response);
                }
            },
			error : function(r){
				//alert("Error game_over");
				$('.ploading').hide();
				callback(null);
				console.log("error",r);
			}
        });
    },
    setUserSession: function (us) {
        $.localStorage.set(apiHelper.gameId, us);
    },
    userSession: function () {
        return $.localStorage.get(apiHelper.gameId);
    },
    removeSession: function () {
        console.log("remove session");
        $.localStorage.removeAll();
    },
    shareFacebookViaGameMenu: function () {
        FB.ui({
            method: 'feed',
            name: 'Travis Perkins',
            link: apiHelper.directMain,
            caption: 'Test Your Skills with the Young Grafters App by Travis Perkins',
            description: '',
            picture: apiHelper.directMain + 'images/travis-perkins-logo.png',
            message: ''
        });
    },
    shareFacebookViaScorePopup: function (gameName, score) {
        FB.ui({
            method: 'feed',
            name: 'Travis Perkins',
            link: apiHelper.directMain,
            caption: 'Test Your Skills with the Young Grafters App by Travis Perkins',
            description: "I have just played the " + gameName + " game",
            picture: apiHelper.directMain + 'images/travis-perkins-logo.png',
            message: ''
        });
    },
    shareTwitterViaGameMenu: function (gameName) {
        var winTop = (screen.height / 2) - (320 / 2);
        var winLeft = (screen.width / 2) - (480 / 2);
        var title = "Test Your Skills with the Young Grafters App by Travis Perkins.";
        window.open('http://twitter.com/share?url=' + apiHelper.directMain + '&text=' + title, 'twitterwindow', 'height=450, width=550, top='+winTop +', left='+ winLeft +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    },
    shareTwitterViaScorePopup: function (gameName, score) {
        var winTop = (screen.height / 2) - (320 / 2);
        var winLeft = (screen.width / 2) - (480 / 2);
        var title = "Test Your Skills with the Young Grafters App by Travis Perkins. I have just played the " + gameName + " game";
        window.open('http://twitter.com/share?url=' + apiHelper.directMain + '&text=' + title, 'twitterwindow', 'height=450, width=550, top='+winTop +', left='+ winLeft +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    },
    replayGame: function (gameId) {
        //do replay this game, reload this game.
        window.location.reload();
    },
    gotoMainMenu: function (gameId) {
        window.location = apiHelper.directMain;
    },
    openGameMenu: function (gameId, callback) {
        //call back
    },
    openLeaderBoards: function (gameId) {
        console.log("openLeaderBoards " + gameId);
        apiHelper.runShowListLeaderBoard(gameId);
    },
    switchSound: function (soundOn) {
        $.localStorage.set('gameSound', (soundOn == true) ? "1" : "0");
    },
    isSoundMute: function () {
        if (!$.localStorage.isSet('gameSound') || $.localStorage.get('gameSound') == "1") {
            return false;
        }
        return true;
    },
    gotoGame: function (gameId) {
        console.log("goto game " + gameId);
        if (gameId == 1) {
            countergameLocal.redirectCounter();
        } else if (gameId == 2) {
            drivergameLocal.redirectDelivery();
        } else if (gameId == 3) {
            yardgameLocal.redirectYard();
        } else if (gameId == 4) {
            postgameLocal.redirectPost();
        }
    },
    // Draw html leaderborad for 4 games and menu
    showlistMainBoard: function () {
        /* Parent DIV */
        TagDivLeaderB = document.createElement('ul');
        TagDivLeaderB.id = 'showBoxLeaderBoard';
        TagDivLeaderB.style.position = "absolute";
        TagDivLeaderB.style.top = 0;
        TagDivLeaderB.style.left = 0;
        if ($(apiHelper.wrapContainer)) {
            $(apiHelper.wrapContainer).append(TagDivLeaderB);
        }else if ($(apiHelper.gameContainer)) {
            $(apiHelper.gameContainer).append(TagDivLeaderB);
        }
        var domHtml = new createjs.DOMElement(TagDivLeaderB);
        domHtml.x = 70 + parseInt(gameWrapperObject.loadPositionCanvas().x);
        domHtml.y = 165.9 + parseInt(gameWrapperObject.loadPositionCanvas().y);
        stage.addChild(domHtml);
        stage.update();
    },
    runShowListLeaderBoard: function (gameId) {
        if (!$('#showBoxLeaderBoard').length) {
            this.showlistMainBoard();
        }
        $('#showBoxLeaderBoard').html('');
        var html = apiHelper.leaderboards(gameId, function (data) {
            var html_str = '';
            if (data && data.status == "OK") {
                var leaderboards = data.leaderboards;
                
                    for (var i = 0; i < leaderboards.length; i++) {

                        html_str = html_str + '<li>\
                            <div class="nameUserLeaderBoard">\
                                <a href="javascript:void(0)">' + leaderboards[i].name + '</a>\
                             </div>\
                            <div class="scoreUserLeaderBoard">\
                                <a href="javascript:void(0)">' + leaderboards[i].total_score + '</a>\
                             </div>\
                        </li>';

                    }
            }
            console.log(html_str);
             $("#showBoxLeaderBoard").mCustomScrollbar("destroy");
                $('#showBoxLeaderBoard').append(html_str);
                $('#showBoxLeaderBoard').mCustomScrollbar({
                    scrollButtons: {
                        enable: true
                    }
                });
        });
    },
    removeListLeaderBoard: function () {
        if ($('#showBoxLeaderBoard').length) {
            $('#showBoxLeaderBoard').html('');
            $('#showBoxLeaderBoard').remove();
            $("#leaderBoardOptionBox").hide('fast');
        }
    },
    runShowLeaderBoardOptions: function () {
        if (!$('#leaderBoardOptionBox').length) {
            TagDivLBOption = document.createElement('div');
            TagDivLBOption.id = 'leaderBoardOptionBox';
            if ($(this.wrapContainer)) {
                $(this.wrapContainer).append(TagDivLBOption);    
            }else if ($(this.gameContainer)) {
                $(this.gameContainer).append(TagDivLBOption);
            }
            
            var domHtml = new createjs.DOMElement(TagDivLBOption);
            domHtml.x = 70 + parseInt(gameWrapperObject.loadPositionCanvas().x);
            domHtml.y = 165.9 + parseInt(gameWrapperObject.loadPositionCanvas().y);
            stage.addChild(domHtml);
            stage.update();
        }
    },
    runShowOptionalLeaderBoard: function (exportRoot) {
        var isSound , bnt_Log , btn_Sound;
        if (!$.localStorage.get('isUser')) {
            isLog = 1;
            bnt_Log = 'onclick="mainMenu.openLeadDiaLogOpen()"';
        } else {
            isLog = 0;
            bnt_Log ='onclick="apiHelper.logout(exportRoot)"';
        }

        isSound = this.isSoundMute();
         btn_Sound = 'onclick="mainMenu.tickSoundLeadeboard()"';
        if (isSound == false) {
            isSound = 1;
        } else {
            isSound = 0;
        }
        this.runShowLeaderBoardOptions();
        $("#leaderBoardOptionBox").show('fast');
        if (!$('.bntL').length) {
            var html = '<div class="bntL">\
                <a  onclick="apiHelper.closeOptionLeaderBoard()" href="javascript:void(0)"><img src="' + apiHelper.directMain + 'images/Options_btn.png" />\
                </a>\
            </div>\
            <div class="log">\
                <a  '+bnt_Log+' href="javascript:void(0)"><img id="isLog" src="' + apiHelper.directMain + 'images/log-' + isLog + '.png" />\
                </a>\
            </div>\
            <div class="sound">\
                <a '+btn_Sound+' href="javascript:void(0)"><img id="isSound"  src="' + apiHelper.directMain + 'images/sound-' + isSound + '.png" />\
                </a>\
            </div>\
            <div class="shareF">\
                <a onclick="apiHelper.shareFacebookViaGameMenu()" id="shareFleaderboard" href="javascript:void(0)"><img src="' + apiHelper.directMain + 'images/shareF-0.png" />\
                </a>\
            </div>\
            <div class="shareT">\
                <a id="shareTwitterLeaderboard" href="https://twitter.com/intent/tweet?text=Test Your Skills with the Young Grafters App by Travis Perkins" target="_blank"><img src="' + apiHelper.directMain + 'images/shareT-0.png" />\
                </a>\
            </div>';
            $("#leaderBoardOptionBox").append(html);
        }
    },
    closeOptionLeaderBoard: function () {
        $("#leaderBoardOptionBox").hide('slow');
    },
    dismissAlertLogin : function () {
        exportRoot.mc_AlertBox.visible = false;
        exportRoot.mc_AlertBox.mc_bg.removeAllEventListeners();
        exportRoot.mc_LoginPanel.visible = true;
        if (exportRoot.showFormAfterDismissAlert == true) {
            loginLocal.drawingInputBoxLogin();
        }
    },
    dismissAlertReset : function () {
        exportRoot.mc_AlertBox.visible = false;
        //exportRoot.play();
        exportRoot.mc_LoginPanel.visible = true;
        exportRoot.mc_AlertBox.mc_bg.removeAllEventListeners();
        if (exportRoot.showFormAfterDismissAlert == true)
            loginLocal.drawingLostPassword();
    },
    dismissAlert: function () {
        exportRoot.mc_AlertBox.mc_bg.removeAllEventListeners();
        exportRoot.mc_AlertBox.visible = false;
        exportRoot.mc_LoginPanel.visible = true;
    },
    dismissAlertSignUp : function () {
        exportRoot.mc_AlertBox.mc_bg.removeAllEventListeners();
        exportRoot.mc_AlertBox.visible = false;
        //exportRoot.play();
        exportRoot.mc_LoginPanel.visible = true;
        if(exportRoot.showFormAfterDismissAlert == true)
            loginLocal.drawingCreateNewAccount(true);
    },
    dismissAlertSignUpFailed : function () {
        exportRoot.mc_AlertBox.mc_bg.removeAllEventListeners();
        exportRoot.mc_AlertBox.visible = false;
        //exportRoot.play();
        exportRoot.mc_LoginPanel.visible = true;
        loginLocal.drawingCreateNewAccount(true);
    },
    
    callAlert: function (box, name) {
        console.log("call alert " + box + " , " + name);
        exportRoot.mc_LoginPanel.visible = false;
        exportRoot.mc_AlertBox.visible = true;
        loginLocal.removeInputBoxLogin();
        //exportRoot.stop();
        exportRoot.showFormAfterDismissAlert = false;


        exportRoot.mc_AlertBox.btn_alert_ok.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_alert_x.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_nouser_ok.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_nouser_x.removeAllEventListeners();

        exportRoot.mc_AlertBox.btn_login_ok.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_login_x.removeAllEventListeners();

        exportRoot.mc_AlertBox.btn_reg_ok.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_reg_x.removeAllEventListeners();


        if (box == "popup_login_welcome") {            
            exportRoot.mc_AlertBox.gotoAndStop(3);
            exportRoot.mc_AlertBox.txt_welcome.text = "Welcome back " + name + ".\n Now let’s have some fun!";

            exportRoot.mc_AlertBox.btn_login_ok.addEventListener("click", apiHelper.dismissAlertLogin);
            exportRoot.mc_AlertBox.btn_login_x.addEventListener("click", apiHelper.dismissAlertLogin);
        }else if (box == "main_alert_game") {
            exportRoot.mc_AlertBox.gotoAndStop(0);
        }else if (box ==  "popup_login_no_user") {
				exportRoot.mc_AlertBox.gotoAndStop(0);
                exportRoot.showFormAfterDismissAlert = true;
                if (name && name != "") {
                    exportRoot.mc_AlertBox.txt_ResetMsg.text = name;
                }else{
                    exportRoot.mc_AlertBox.txt_ResetMsg.text = "No user found. Please check your login details.";
                }
                
                exportRoot.mc_AlertBox.btn_nouser_ok.addEventListener("click", apiHelper.dismissAlertLogin);
                exportRoot.mc_AlertBox.btn_nouser_x.addEventListener("click", apiHelper.dismissAlertLogin);

                // exportRoot.mc_AlertBox.gotoAndStop(1);

        }else if (box == "popup_reset_no_user") {
                exportRoot.showFormAfterDismissAlert = true;
				exportRoot.mc_AlertBox.gotoAndStop(1);
                if (name && name != "") {
                    exportRoot.mc_AlertBox.txt_ResetMsg_1.text = name;
                }else{
                    exportRoot.mc_AlertBox.txt_ResetMsg_1.text = "No user found. Please check your login details.";
                }
                
                exportRoot.mc_AlertBox.btn_nouser_ok.addEventListener("click", apiHelper.dismissAlertReset);
                exportRoot.mc_AlertBox.btn_nouser_x.addEventListener("click", apiHelper.dismissAlertReset);                

        }else if (box == "sign_up") {
			 	exportRoot.mc_AlertBox.gotoAndStop(1);
                if (name) {
                    exportRoot.mc_AlertBox.txt_ResetMsg_1.text = name;
                    exportRoot.mc_AlertBox.btn_nouser_ok.addEventListener("click", apiHelper.dismissAlertSignUp);
                    exportRoot.mc_AlertBox.btn_nouser_x.addEventListener("click", apiHelper.dismissAlertSignUp);    
                }
        }else if (box == "sign_up_failed") {
                exportRoot.mc_AlertBox.gotoAndStop(1);
                if (name) {
                    exportRoot.mc_AlertBox.txt_ResetMsg_1.text = name;
                    exportRoot.mc_AlertBox.btn_nouser_ok.addEventListener("click", apiHelper.dismissAlertSignUpFailed);
                    exportRoot.mc_AlertBox.btn_nouser_x.addEventListener("click", apiHelper.dismissAlertSignUpFailed);    
                }
        }else if (box ==  "popup_register_complete") {
                exportRoot.mc_AlertBox.gotoAndStop(2);
                exportRoot.mc_AlertBox.btn_reg_ok.addEventListener("click", apiHelper.dismissAlert);
                exportRoot.mc_AlertBox.btn_reg_x.addEventListener("click", apiHelper.dismissAlert);

                }else if (box ==  "login_required" ) {
                exportRoot.mc_AlertBox.gotoAndStop(0);
                //exportRoot.showFormAfterDismissAlert = true;
         }else if (box ==  "popup_reset_valid_user") {
                exportRoot.showFormAfterDismissAlert = true;
                
                exportRoot.mc_AlertBox.gotoAndStop(1);

                if (name && name != "") {
                    exportRoot.mc_AlertBox.txt_ResetMsg_1.text = name;
                }else{
                    exportRoot.mc_AlertBox.txt_ResetMsg_1.text = "New password has been sent to your email.";
                }

                exportRoot.mc_AlertBox.btn_nouser_ok.addEventListener("click", apiHelper.dismissAlertReset);
                exportRoot.mc_AlertBox.btn_nouser_x.addEventListener("click", apiHelper.dismissAlertReset);

                exportRoot.mc_AlertBox.txt_ResetMsg_1.text = name;
                console.log(exportRoot.mc_AlertBox.txt_ResetMsg.text);
        }
        
        exportRoot.mc_AlertBox.mc_bg.addEventListener ("click" , function () {
            console.log("click bg");
        });

		if (exportRoot.mc_AlertBox.currentFrame!=1){
        	exportRoot.mc_AlertBox.btn_alert_ok.addEventListener("click", apiHelper.dismissAlert);    
        	exportRoot.mc_AlertBox.btn_alert_x.addEventListener("click", apiHelper.dismissAlert);
		}
    }
}