var sprites = new Object();
sprites.avatar = null;
sprites.loaded = false;

function initPerson(name){
    gamePost.avatarName = name;
    exportRoot.mc_MainAvatar.visible = false;

    showSprite(name);

    setTimeout(checkSpriteLoaded, 200);
}

function showSprite(name){  
    if (sprites.avatar==null){
        loadMainAvatar(name);
    }
        
    if (sprites.loaded) sprites.avatar.visible = true;
}


function checkSpriteLoaded(){
    //dbLog("checkSpriteLoaded");
    console.log(gamePost.avatarName + " : " + sprites.loaded);
    if (sprites.loaded){
        exportRoot.mc_MainAvatar.visible = true;
        gamePost.allSpritesLoaded();
    }else{
        setTimeout(checkSpriteLoaded, 200);
    }
}

var gamePost = {
    GAME_NAME : "The Post Room",
    avatarMode: "male",//male , female
    GAME_ID : "4",
    GAME_NAME : "post",
    SOUND_ENABLED : true,
    selectedWorkwear : 0,//0, 1: Steve, 2,3: Kate
    gamePlaying : false,
    gamePause : false,
    currentScore : 0,
    gameRemainingTime : 60, // in second
    MAX_PLAY_TIME : 60,
    SCORE_INC_STEP : 500,
    mc_currentAvatar : null,
    avatarName : null,
    letterIsPlaying : false,
	letterCount : 0,
	level : 1,
    playingLetter : 0,
    selectedLetter : 0,
    playingAvatar: null,
    init: function (gmode) {
        exportRoot.mc_JasonOptions.visible = false;
        exportRoot.mc_FrankOptions.visible = false;
        exportRoot.mc_GregOptions.visible = false;
        exportRoot.mc_SteveOptions.visible = false;
        exportRoot.mc_KateOptions.visible = false;
        exportRoot.mc_PeppaOptions.visible = false;
        exportRoot.mc_LucyOptions.visible = false;
        exportRoot.mc_JaneOptions.visible = false;

        console.log("storageIDAvatar " + $.localStorage.get("storageIDAvatar"));
        if ($.localStorage.get("storageIDAvatar") == 1 || $.localStorage.get("storageIDAvatar") == 5 || $.localStorage.get("storageIDAvatar") == 2 || $.localStorage.get("storageIDAvatar") == 6) {
            gamePost.avatarMode = "male";    
        }else{
            gamePost.avatarMode = "female";    
        }
        //gamePost.avatarMode = gmode;
        gamePost.doUpdateAvatar($.localStorage.get("storageIDAvatar"));
        sManager.playBGMusic(true);
        this.doInitSelectWorkwear();
        exportRoot.mc_Timer.mc_TimerCounter.text = "00:59";
        exportRoot.mc_ladyBossMC.instance_1.addEventListener("click", this.doGotItWrongSteveWear); 

        this.addClickHandleGameToolbars();

        gamePost.enableStartGame(false);
        apiHelper.init(gamePost.GAME_ID);  
        gamePost.enableCursorType();  

        apiHelper.init_game_session(gamePost.GAME_ID, gamePost.GAME_NAME);    



        exportRoot.mc_ConfirmQuitGame.mc_bg.addEventListener ("click" , function () {

        });

        exportRoot.mc_MainMenu.mc_bg.addEventListener ("click" , function () {

        });
        gamePost.fixBrowserCompatible();
        gamePost.bossSpeechFrame = 0;
        exportRoot.mc_Scoring.mc_Scoring.visible = false;   
    },
    enableCursorType : function () {
        exportRoot.mc_GameMenu.cursor = "pointer";
        exportRoot.mc_GameMenu_1.cursor = "pointer";
        exportRoot.mc_GameMenu_2.cursor = "pointer";
        exportRoot.mc_GameMenu_3.cursor = "pointer";
        
        exportRoot.mc_EnterGame.cursor = "pointer";
        exportRoot.mc_QuitGame.cursor = "pointer";
        exportRoot.mc_QuitGame_1.cursor = "pointer";
        exportRoot.mc_QuitGame_2.cursor = "pointer";
        exportRoot.mc_QuitGame_3.cursor = "pointer";
        // exportRoot.mc_QuitGame_4.cursor = "pointer";
        
        exportRoot.mc_Instructions.mc_InstructionGotIt.cursor = "pointer";
        exportRoot.mc_Boss.mc_BossGotItBtn.cursor = "pointer";
        
        exportRoot.mc_CloseGame.cursor = "pointer";
        exportRoot.mc_ReplayGame.cursor = "pointer";
    },
    enableStartGame: function (enabled) {
        exportRoot.mc_EnterGame.visible = enabled;
        exportRoot.mc_Highlighter.visible = enabled;
        if (enabled) {
            exportRoot.mc_Highlighter.gotoAndPlay(0);
        }
    },
    doUpdateAvatar: function () {
        if ($.localStorage.get("storageIDAvatar") == 1) {
            // exportRoot.mc_SteveOptions.mc_SteveAvatar.visible = true;
            exportRoot.mc_SteveOptions.visible = true;
            gamePost.playingAvatar = exportRoot.mc_SteveOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 2) {
            // exportRoot.mc_SteveOptions.mc_GregAvatar.visible = true;
            exportRoot.mc_GregOptions.visible = true;
            gamePost.playingAvatar = exportRoot.mc_GregOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 5) {
            // exportRoot.mc_SteveOptions.mc_JasonAvatar.visible = true;
            exportRoot.mc_JasonOptions.visible = true;
            gamePost.playingAvatar = exportRoot.mc_JasonOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 6) {
            // exportRoot.mc_SteveOptions.mc_FrankAvatar.visible = true;
            exportRoot.mc_FrankOptions.visible = true;
            gamePost.playingAvatar = exportRoot.mc_FrankOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 3){
            //jane
            // exportRoot.mc_KateOptions.mc_JaneAvatar.visible = true;
            exportRoot.mc_JaneOptions.visible = true;
            gamePost.playingAvatar = exportRoot.mc_JaneOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 4){
            //kate
            // exportRoot.mc_KateOptions.mc_KateAvatar.visible = true;
            exportRoot.mc_KateOptions.visible = true;
            gamePost.playingAvatar = exportRoot.mc_KateOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 7){
            //lucy
            // exportRoot.mc_KateOptions.mc_LucyAvatar.visible = true;
            exportRoot.mc_LucyOptions.visible = true;
            gamePost.playingAvatar = exportRoot.mc_LucyOptions;
        }else if ($.localStorage.get("storageIDAvatar") == 8){
            //jane
            // exportRoot.mc_KateOptions.mc_PeppaAvatar.visible = true;
            exportRoot.mc_PeppaOptions.visible = true;
            gamePost.playingAvatar = exportRoot.mc_PeppaOptions;
        }
        
    },
    addClickHandleGameToolbars : function () {
        exportRoot.mc_GameMenu.addEventListener("click", gamePost.doOpenGameMenu);    
        exportRoot.mc_GameMenu_1.addEventListener("click", gamePost.doOpenGameMenu);    
        exportRoot.mc_GameMenu_2.addEventListener("click", gamePost.doOpenGameMenu);    
        exportRoot.mc_GameMenu_3.addEventListener("click", gamePost.doOpenGameMenu);    
        //exportRoot.mc_TimeupGameMenu.addEventListener("click", this.doOpenGameMenu);    
        //exportRoot.mc_PostGameMenu.addEventListener("click", this.doOpenGameMenu);    

        exportRoot.mc_EnterGame.addEventListener("click", gamePost.doStartGame);    
        //exportRoot.mc_PostCloseGame.addEventListener("click", this.doQuitGame);    
        exportRoot.mc_QuitGame.addEventListener("click", gamePost.doQuitGame);      
        exportRoot.mc_QuitGame_1.addEventListener("click", gamePost.doQuitGame);      
        exportRoot.mc_QuitGame_2.addEventListener("click", gamePost.doQuitGame);      
        exportRoot.mc_QuitGame_3.addEventListener("click", gamePost.doQuitGame);      
        // exportRoot.mc_QuitGame_4.addEventListener("click", gamePost.doQuitGame);      
        //exportRoot.mc_QuitGameIns.addEventListener("click", this.doQuitGame);      

        exportRoot.mc_HealthAndSafetyLadyInit.cursor = "pointer";
        exportRoot.mc_HealthAndSafetyLadyInit.addEventListener("click", gamePost.playHealthAndSafety);
        
        exportRoot.mc_Instructions.mc_InstructionGotIt.addEventListener("click", gamePost.doStartGameAfterInstruction);    
        exportRoot.mc_Boss.mc_BossGotItBtn.addEventListener("click", gamePost.doGotItBossHandle); 
        gamePost.addClickHandleLetters();

        exportRoot.mc_CloseGame.addEventListener("click", this.doQuitGame);      

        exportRoot.mc_ReplayGame.addEventListener("click", function (e) {
            console.log("replace game");
            apiHelper.replayGame();
        });

    },
    addClickHandleLetters : function () {
        gamePost.removeClickHandleLetters();

        exportRoot.mc_Destination1.mouseEnabled = false;
        exportRoot.mc_Destination2.mouseEnabled = false;
        exportRoot.mc_Destination3.mouseEnabled = false;
        exportRoot.mc_Destination4.mouseEnabled = false;
        exportRoot.mc_Destination5.mouseEnabled = false;
        exportRoot.mc_Destination6.mouseEnabled = false;
        
        exportRoot.mc_BtnFinnance.addEventListener("click", this.doSelectFinnace) ;
        exportRoot.mc_BtnIT.addEventListener("click", this.doSelectIT) ;
        exportRoot.mc_BtnHR.addEventListener("click", this.doSelectHR) ;
        exportRoot.mc_BtnCommercial.addEventListener("click", this.doSelectCommercial) ;
        exportRoot.mc_BtnMarketing.addEventListener("click", this.doSelectMarketing) ;
        exportRoot.mc_BtnSupply.addEventListener("click", this.doSelectSupplyChain) ;
    },
    removeClickHandleLetters : function () {
        exportRoot.mc_BtnFinnance.removeAllEventListeners();
        exportRoot.mc_BtnIT.removeAllEventListeners();
        exportRoot.mc_BtnHR.removeAllEventListeners();
        exportRoot.mc_BtnCommercial.removeAllEventListeners();
        exportRoot.mc_BtnMarketing.removeAllEventListeners();
        exportRoot.mc_BtnSupply.removeAllEventListeners();
    },
    healthAndSafetyDoneTalking : function () {
        exportRoot.mc_HealthAndSafetyLadyInit.addEventListener("click", gamePost.playHealthAndSafety);
        
    },
    HnSGotItPressed: function () {
        exportRoot.mc_HealtySafetyMessage.btn_HnSGotIt.removeAllEventListeners();
        exportRoot.mc_HealtySafetyMessage.gotoAndPlay(24)  ;
    },
    HnSMessageDone : function () {
        exportRoot.mc_HealtySafetyMessage.visible = false;
    },
    playHealthAndSafety : function () {
        //mc_HealtySafetyMessage
        exportRoot.mc_HealtySafetyMessage.instance.text.text = comments.HnS[chance.natural({min: 0, max: comments.HnS.length - 1})];
        var h = exportRoot.mc_HealtySafetyMessage.instance.text.getMeasuredHeight();
        if (h > 100) {
            exportRoot.mc_HealtySafetyMessage.instance.text.font = "20px 'Laffayette Comic Pro'";    
            exportRoot.mc_HealtySafetyMessage.instance.text.lineHeight = 28;
        }else{
            exportRoot.mc_HealtySafetyMessage.instance.text.font = "25px 'Laffayette Comic Pro'";    
            exportRoot.mc_HealtySafetyMessage.instance.text.lineHeight = 34;
        }
        
        
        exportRoot.mc_HealthAndSafetyLadyInit.removeAllEventListeners();
        exportRoot.mc_HealthAndSafetyLadyInit.gotoAndPlay("StartTalking");
        exportRoot.mc_HealtySafetyMessage.visible = true;
        exportRoot.mc_HealtySafetyMessage.mc_bg.addEventListener("click", function () {});
        exportRoot.mc_HealtySafetyMessage.gotoAndPlay(0);
        exportRoot.mc_HealtySafetyMessage.btn_HnSGotIt.addEventListener("click", gamePost.HnSGotItPressed);
    },
    doStartGameAfterInstruction : function () {
        console.log("start game");

        gamePost.newLetter();

        gamePost.currentScore = 0;

        exportRoot.gotoAndStop("Game");
        gamePost.gamePlaying = true;
        gamePost.gamePause = false;
        gamePost.letterIsPlaying = true;

        
        gamePost.incScore(gamePost.currentScore);
        
        gamePost.startGameTimer();
    },
    allSpritesLoaded : function () {
        if (sprites.loaded) {
            gamePost.showLoader(false);
            exportRoot.gotoAndPlay("Instructions");       
        }
    },
    showLoader : function (visible) {
        exportRoot.mc_loader.visible = visible;
        if (visible) {
            exportRoot.mc_loader.gotoAndPlay(0);
        }else{
            exportRoot.mc_loader.stop();    
            //remove from stage
            exportRoot.removeChild(exportRoot.mc_loader);
        }
    },


    startGameTimer: function() {
        gameTimer.seconds = gamePost.MAX_PLAY_TIME;
        gameTimer.updateStatus = function(sec) {
                gamePost.gameRemainingTime = sec;
                if (!gamePost.gamePlaying) {
                    return;
                }
                gamePost.updateGameTimer(sec);
            };
        gameTimer.counterEnd = function() {
                console.log("GAME OVER");
                gamePost.gamePlaying = false;
                gamePost.gamePause = true;
                gamePost.gameOver();
            };
        gameTimer.start();  
    },
    updateGameTimer : function (sec) {
        // var firstNum = "";
        var nextNum = "";
        var m = ~~(sec / 60);
        var s = sec % 60;
        if (m > 9) {
            nextNum = "" + m + ":";
        }else{
            nextNum = "0" + m + ":";
        }
        if (s > 9) {
            nextNum = nextNum + s;
        }else{
            nextNum = nextNum + "0" + s;
        }
        exportRoot.mc_Timer.mc_TimerCounter.text = nextNum;

    },
    addLeadingZeros : function (n, length){
        var str = (n == 0 ? "" : n) + "";
        var zeros = "";
        for (var i = length - str.length; i > 0; i--)
            zeros += "0";
        return zeros;
        
    },
    incScore : function (score) {
        if (score > 0) {
            apiHelper.inc_game_score(gamePost.GAME_ID, score);        
        }
        
        gamePost.currentScore += score;
        exportRoot.mc_Scoring.mc_ScoringCounter.text = gamePost.addLeadingZeros(gamePost.currentScore, 6);
        if (gamePost.currentScore == 0) {
            
            exportRoot.mc_Scoring.mc_Scoring.visible = false;   
        }else{
            exportRoot.mc_Scoring.mc_Scoring.text = gamePost.currentScore;    
            exportRoot.mc_Scoring.mc_Scoring.visible = true;   
        }
    },
    gameOver : function () {

        gamePost.removeClickHandleLetters();

        exportRoot.mc_HealtySafetyMessage.visible = false;

        gamePost.gamePlaying = false;
        gamePost.gamePause = true;
        gameTimer.stop();
        sManager.stopBackgroundMusic();


        apiHelper.game_over(gamePost.GAME_ID, gamePost.currentScore, function (data) {
            //handle badge info
            console.log(data);
            if (!data) {
                gamePost.didNotEarnBadge();          
            }else if (data.status == "OK" && data.badge_info){
                if (data.badge_info.badge_status == 1) {
                    gamePost.didNotEarnBadge();          
                }else if (data.badge_info.badge_status == 2) {
                    gamePost.alreadyGotBadge();
                }else if (data.badge_info.badge_status == 3) {
                    gamePost.earnedBadge();          
                }else if (data.badge_info.badge_status == 4) {
                    gamePost.earnedAllBadge();          
                }else{
                    gamePost.didNotEarnBadge();
                }
            }else{
                gamePost.didNotEarnBadge();
            }
        });    
    
        //testing
        //gamePost.didNotEarnBadge();
        //gamePost.alreadyGotBadge();
        //gamePost.earnedBadge();
        //gamePost.earnedAllBadge();
    },
    addClickHandleWorkWear : function () {
        exportRoot.mc_Steve_Shirt_1.addEventListener("click", this.doSelectShirt1);    
        exportRoot.mc_Steve_Shirt_2.addEventListener("click", this.doSelectShirt2);    
        exportRoot.mc_Steve_Shirt_3.addEventListener("click", this.doSelectShirt3);    
        exportRoot.mc_Steve_Shirt_4.addEventListener("click", this.doSelectShirt4);

        exportRoot.mc_Kate_Suit_1.addEventListener("click", this.doSelectSuit1);    
        exportRoot.mc_Kate_Suit_2.addEventListener("click", this.doSelectSuit2);    
        exportRoot.mc_Kate_Suit_3.addEventListener("click", this.doSelectSuit3);    
        exportRoot.mc_Kate_Suit_4.addEventListener("click", this.doSelectSuit4);        
    },
    removeClickHandleWorkWear : function () {
        exportRoot.mc_Steve_Shirt_1.removeAllEventListeners();    
        exportRoot.mc_Steve_Shirt_2.removeAllEventListeners();    
        exportRoot.mc_Steve_Shirt_3.removeAllEventListeners();    
        exportRoot.mc_Steve_Shirt_4.removeAllEventListeners();

        exportRoot.mc_Kate_Suit_1.removeAllEventListeners();    
        exportRoot.mc_Kate_Suit_2.removeAllEventListeners();    
        exportRoot.mc_Kate_Suit_3.removeAllEventListeners();    
        exportRoot.mc_Kate_Suit_4.removeAllEventListeners();
    },
    doLogout : function () {
        apiHelper.logoutFromGame(function () {
            apiHelper.gotoMainMenu();
        });
    },
    switchSound : function () {
        apiHelper.switchSound(!apiHelper.isSoundMute() ? false : true);
        if (!apiHelper.isSoundMute()) {
            //sound is on
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(0);
            sManager.playBGMusic();
        }else{
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(1);
            //stop bg music
            sManager.stopBackgroundMusic();
        }
    },
    addHandlersMenuButton : function () {
        exportRoot.mc_MainMenu.btn_Leaderboard.addEventListener("click", gamePost.showLeaderBoards);    
        exportRoot.mc_MainMenu.btn_Logout.addEventListener("click", gamePost.doLogout);    
        exportRoot.mc_MainMenu.btn_Sound.addEventListener("click", gamePost.switchSound);    
        exportRoot.mc_MainMenu.btn_Facebook.addEventListener("click", function () {
            apiHelper.shareFacebookViaGameMenu();
        }) ;
        exportRoot.mc_MainMenu.btn_Twitter.addEventListener("click", function () {
            apiHelper.shareTwitterViaGameMenu();
        });    

        exportRoot.mc_MainMenu.btn_Leaderboard.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Logout.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Sound.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Facebook.cursor = "pointer";
        exportRoot.mc_MainMenu.btn_Twitter.cursor = "pointer";;
    },
    removeHandlersMenuButton : function () {
        exportRoot.mc_MainMenu.btn_Leaderboard.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Logout.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Sound.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Facebook.removeAllEventListeners();
        exportRoot.mc_MainMenu.btn_Twitter.removeAllEventListeners();
    },

    doOpenGameMenu : function () {
        
        console.log("Open game menu");
        exportRoot.mc_GameMenu.removeAllEventListeners();    
        exportRoot.mc_GameMenu_1.removeAllEventListeners(); 
        exportRoot.mc_GameMenu_2.removeAllEventListeners(); 
        exportRoot.mc_GameMenu_3.removeAllEventListeners(); 

        exportRoot.mc_MainMenu.visible = true;
        exportRoot.mc_GameMenuOn.visible = true;

        exportRoot.mc_GameMenuOn.cursor = "pointer";;

        exportRoot.mc_GameMenuOn.addEventListener("click" , function () {
            exportRoot.mc_MainMenu.visible = false;
            exportRoot.mc_GameMenuOn.visible = false;
            exportRoot.mc_GameMenuOn.removeAllEventListeners();

            exportRoot.mc_GameMenu.addEventListener("click", gamePost.doOpenGameMenu);    
            exportRoot.mc_GameMenu_1.addEventListener("click", gamePost.doOpenGameMenu);    
            exportRoot.mc_GameMenu_2.addEventListener("click", gamePost.doOpenGameMenu);    
            exportRoot.mc_GameMenu_3.addEventListener("click", gamePost.doOpenGameMenu);    
            
        });

        gamePost.removeHandlersMenuButton();
        gamePost.addHandlersMenuButton();


        //checking sound
        if (!apiHelper.isSoundMute()) {
            //sound is on
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(0);
        }else{
            exportRoot.mc_MainMenu.btn_Sound.gotoAndStop(1);
        }

    },
    doStartGame : function () {
        console.log("Do start game");

        gamePost.showLoader(true);
        var name = "";
        var storageIDAvatar = "" + $.localStorage.get('storageIDAvatar');
        if (gamePost.avatarMode == "male") {
            if (gamePost.selectedWorkwear == 0) {
                switch(storageIDAvatar) {
                    case "1"://Steve
                        name = "MaleSteveShirt1";
                        break;
                    case "2"://Greg
                        name = "MaleGregShirt1";
                        break;
                    case "5"://Jason
                        name = "MaleJasonShirt1";
                        break;
                    case "6"://Frank
                        name = "MaleFrankShirt1";
                        break;
                }
                // initPerson("MaleSteveShirt1"); 
            }else{
                // initPerson("MaleSteveShirt2"); 
                switch(storageIDAvatar) {
                    case "1"://Steve
                        name = "MaleSteveShirt2";
                        break;
                    case "2"://Greg
                        name = "MaleGregShirt2";
                        break;
                    case "5"://Jason
                        name = "MaleJasonShirt2";
                        break;
                    case "6"://Frank
                        name = "MaleFrankShirt2";
                        break;
                }
            }
        }else{
            if (gamePost.selectedWorkwear == 2) {
                switch(storageIDAvatar) {
                    case "3"://Jane
                        name = "FemaleJaneSuit1";
                        break;
                    case "4"://Kate
                        name = "FemaleKateSuit1";
                        break;
                    case "7"://Lucy
                        name = "FemaleLucySuit1";
                        break;
                    case "8"://Peppa
                        name = "FemalePeppaSuit1";
                        break;
                }
                // initPerson("FemaleKateSuit1"); 
                
            }else{
                // initPerson("FemaleKateSuit2"); 
                switch(storageIDAvatar) {
                    case "3"://Jane
                        name = "FemaleJaneSuit2";
                        break;
                    case "4"://Kate
                        name = "FemaleKateSuit2";
                        break;
                    case "7"://Lucy
                        name = "FemaleLucySuit2";
                        break;
                    case "8"://Peppa
                        name = "FemalePeppaSuit2";
                        break;
                }
            }
        }
        initPerson(name);
        console.log("Do start loader until all sprites loaded for avatar: " + name);

    },
    doConfirmQuitGame : function () {
        exportRoot.gotoAndStop("GameQuit");
        exportRoot.mc_QuitConfirmInit.mc_No.addEventListener ("click", function () {

        });
        exportRoot.mc_QuitConfirmInit.mc_Yes.addEventListener ("click", function () {

        });
    },
    doQuitGame : function () {
        exportRoot.mc_ConfirmQuitGame.visible = true;
        exportRoot.mc_ConfirmQuitGame.gotoAndPlay(0);
        exportRoot.mc_ConfirmQuitGame.btn_No.removeAllEventListeners();
        exportRoot.mc_ConfirmQuitGame.btn_No.addEventListener("click", function () {
            gamePost.sureQuitGame = false;
            exportRoot.mc_ConfirmQuitGame.visible = false;
            exportRoot.mc_ConfirmQuitGame.gotoAndPlay(24);
        });
        exportRoot.mc_ConfirmQuitGame.btn_Yes.removeAllEventListeners();
        exportRoot.mc_ConfirmQuitGame.btn_Yes.addEventListener("click", function () {
            gamePost.sureQuitGame = true;
            exportRoot.mc_ConfirmQuitGame.visible = false;
            exportRoot.mc_ConfirmQuitGame.gotoAndPlay(24);
        });
    },
    confirmedQuitGame : function () {
        if (gamePost.sureQuitGame == true) {
            apiHelper.gotoMainMenu();
        }
    },
    doGotItWrongSteveWear : function () {
        exportRoot.mc_ladyBossMC.gotoAndPlay("PressGotIt");
    },
    doGotItWrongWearPressedDone : function () {
        gamePost.doInitSelectWorkwear();
    },
    doSelectShirt1: function () {
        gamePost.doSelectShirt(1);
    },
    doSelectShirt2: function () {
        gamePost.doSelectShirt(2);
    },
    doSelectShirt3: function () {
        gamePost.doSelectShirt(3);
    },
    doSelectShirt4: function () {
        gamePost.doSelectShirt(4);
    },
    doSelectSuit1: function () {
        gamePost.doSelectSuit(1);
    },
    doSelectSuit2: function () {
        gamePost.doSelectSuit(2);
    },
    doSelectSuit3: function () {
        gamePost.doSelectSuit(3);
    },
    doSelectSuit4: function () {
        gamePost.doSelectSuit(4);
    },
    doSelectShirt: function (shirtNo) {
        console.log("do click shirt " + shirtNo);
        gamePost.playingAvatar.gotoAndStop(shirtNo);
        //wrong shirt: The avatar can't wear the motif top or the hoodie. They have to choose one of the two suitable workwear choices to play the game. The Health and Safety Officer tells you to pick something else if you've chosen something inappropriate.
        if (shirtNo == 2 || shirtNo == 3) {
            sManager.playWrongChoose(false);
            gamePost.removeClickHandleWorkWear();
            exportRoot.gotoAndPlay("NotSuitableSteve");
            exportRoot.mc_ladyBossMC.gotoAndPlay("Default");
            gamePost.enableStartGame(false);
        }else{
            sManager.playRightChoose(false);
            if (shirtNo == 1) {
                gamePost.selectedWorkwear = 0;
            }else{
                gamePost.selectedWorkwear = 1;
            }
            gamePost.enableStartGame(true);
        }
    },
    doSelectSuit: function (shirtNo) {
        console.log("do click suit " + shirtNo);
        gamePost.playingAvatar.gotoAndStop(shirtNo);
        //wrong shirt: The avatar can't wear the motif top or the hoodie. They have to choose one of the two suitable workwear choices to play the game. The Health and Safety Officer tells you to pick something else if you've chosen something inappropriate.
        if (shirtNo == 2 || shirtNo == 3) {
            sManager.playWrongChoose(false);
            gamePost.removeClickHandleWorkWear();
            exportRoot.gotoAndPlay("NotSuitableKate");
            exportRoot.mc_ladyBossMC.gotoAndPlay("Default");
            gamePost.enableStartGame(false);
        }else{
            sManager.playRightChoose(false);
            if (shirtNo == 1) {
                gamePost.selectedWorkwear = 2;
            }else{
                gamePost.selectedWorkwear = 3;
            }
            gamePost.enableStartGame(true);
        }
    },
    doInitSelectWorkwear : function () {
        if (gamePost.avatarMode == "male") {
            exportRoot.gotoAndStop("ChooseSteveWorkwear");
            
        }else{
            exportRoot.gotoAndStop("ChooseKateWorkwear");
        }
        gamePost.addClickHandleWorkWear();
    },
    letterFinishedRoller : function () {
        exportRoot.mc_Letter.stop();
        console.log("letter finish roller");
        gamePost.letterIsPlaying = false;
        //available to get new letter
        gamePost.playingLetter = 0;
    },
    letterFinishPickup : function () {
        console.log("letterFinishPickup");

        gamePost.letterIsPlaying = false;
        gamePost.playingLetter = 0;
    },
    cardSelectDone : function () {
        console.log("letterFinishPickup");
        // gamePost.mc_currentAvatar.gotoAndPlay("lookAtLetters");
        gamePost.letterIsPlaying = false;
        gamePost.playingLetter = 0;  
    },
    newLetter : function () {
        gamePost.playingLetter = gamePost.randLetterNo();
        exportRoot.mc_Letter.mc_LetterInside.gotoAndStop(gamePost.playingLetter-1);
        try{
            gamePost.mc_currentAvatar.gotoAndPlay("lookAtLetters");    
        }catch(e){}  
    },
    lookAtLettersDone : function () {
        gamePost.mc_currentAvatar.gotoAndPlay("lookAtBack2");    
    },
    frameTicker : function (e) {
        if (gamePost.letterIsPlaying == false && gamePost.gamePause == false) {
            //get new letter object
            gamePost.newLetter();
            gamePost.addClickHandleLetters();
            gamePost.letterIsPlaying = true;
			gamePost.letterCount++;
			gamePost.level = Math.min(5, Math.floor(gamePost.letterCount/6) + 1);
            exportRoot.mc_Letter.gotoAndPlay("Level" + gamePost.level);
            //gamePost.mc_currentAvatar.gotoAndPlay("lookAtLetters");
        }
    },
    randLetterNo : function () {
        //we only got 6 letters type
        return chance.natural({min: 1, max: 6});
    },
    doSelectFinnace : function () {
        gamePost.removeClickHandleLetters();
        gamePost.doSelectLetter(1);
    },
    doSelectIT : function () {
        gamePost.removeClickHandleLetters();
        gamePost.doSelectLetter(2);  
    },
    doSelectHR : function () {
        gamePost.removeClickHandleLetters();
        gamePost.doSelectLetter(3);  
    },
    doSelectCommercial : function () {
        gamePost.removeClickHandleLetters();
        gamePost.doSelectLetter(4);  
    },
    doSelectMarketing: function () {
        gamePost.removeClickHandleLetters();
        gamePost.doSelectLetter(5);  
    },
    doSelectSupplyChain : function () {
        gamePost.removeClickHandleLetters();
        gamePost.doSelectLetter(6);  
    },
    doSelectLetter : function (letterNo) {
        console.log("select " + letterNo);
        if (gamePost.gamePlaying && gamePost.letterIsPlaying && gamePost.gamePause == false) {
            
            clearTimeout(gamePost.lookAtLettersTs);

            gamePost.selectedLetter = letterNo;

            if (letterNo == gamePost.playingLetter) {
                //right letter
                gamePost.selectCorrectLetter();
            }else{
                //wrong letter
                gamePost.selectWrongLetter();
            }
        }
    },
    selectCorrectLetter : function () {
        console.log("right letter");
        sManager.playRightChoose();
        
        gamePost.incScore(gamePost.SCORE_INC_STEP);

        if (gamePost.playingLetter == 1) {
            gamePost.mc_currentAvatar.gotoAndPlay("finance");
            exportRoot.mc_Letter.gotoAndPlay("PickUpFinance");
        }else if (gamePost.playingLetter == 2) {
            gamePost.mc_currentAvatar.gotoAndPlay("it");
            exportRoot.mc_Letter.gotoAndPlay("PickUpIT");
        }else if (gamePost.playingLetter == 3) {
            gamePost.mc_currentAvatar.gotoAndPlay("hr");
            exportRoot.mc_Letter.gotoAndPlay("PickUpHR");
        }else if (gamePost.playingLetter == 4) {
            gamePost.mc_currentAvatar.gotoAndPlay("commercial");
            exportRoot.mc_Letter.gotoAndPlay("PickUpCommercial");
        }else if (gamePost.playingLetter == 5) {
            gamePost.mc_currentAvatar.gotoAndPlay("marketing");
            exportRoot.mc_Letter.gotoAndPlay("PickUpMarketingComms");
        }else if (gamePost.playingLetter == 6) {
            gamePost.mc_currentAvatar.gotoAndPlay("supply");
            exportRoot.mc_Letter.gotoAndPlay("PickUpSupplyChain");
        } 
    },
    selectWrongLetter : function () {
        console.log("wrong letter : " +  gamePost.selectedLetter);
        sManager.playWrongChoose();
        exportRoot.mc_Letter.visible = false;
        var wrongText = [];
        var widx = 0;
        if (gamePost.playingLetter-1 == 0) {
            wrongText = comments.Finance;
            widx = 5;
        }else if (gamePost.playingLetter-1 == 1) {
            wrongText = comments.IT;
            widx = 10;
        }else if (gamePost.playingLetter-1 == 2) {
            wrongText = comments.HR;
            widx = 15;
        }else if (gamePost.playingLetter-1 == 3) {
            wrongText = comments.Commercial;
            widx = 0;
        }else if (gamePost.playingLetter-1 == 4) {
            wrongText = comments.Marketing;
            widx = 20;
        }else if (gamePost.playingLetter-1 == 5) {
            wrongText = comments.SupplyChain;
            widx = 25;
        }
        var idx = chance.natural({min: 0, max: wrongText.length - 1});

        //exportRoot.mc_Boss.mc_BossBubble.txt_WrongLetter.text = wrongText[idx];
        gamePost.bossSpeechFrame = widx + idx;
        
        exportRoot.mc_Boss.mc_BossBubble.gotoAndStop(gamePost.bossSpeechFrame);
        gamePost.gamePause = true;
        exportRoot.gotoAndPlay("Boss");
        
    },
    bossTalkDone : function() {
        console.log("boss talk done");
        gamePost.gamePause = false;
        exportRoot.gotoAndStop("Game");

    },
    bossDoneShowing : function () {
        console.log("boss done showing");
        //exportRoot.mc_Boss.play();
        exportRoot.mc_Boss.gotoAndPlay("StartTalking");
    },
    doGotItBossHandle : function () {
        console.log("got it");
        exportRoot.mc_Boss.gotoAndPlay("StopTalking");
    },
    didNotEarnBadge : function () {
        exportRoot.mc_DidntGetBadge.mc_FinalScoreNotEnoughtPts.mc_Score.text = "" + gamePost.currentScore;
        exportRoot.mc_DidntGetBadge.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gamePost.GAME_NAME, gamePost.currentScore);
        });
        exportRoot.mc_DidntGetBadge.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gamePost.GAME_NAME, gamePost.currentScore);
        });
        exportRoot.gotoAndStop("NotEnoughForBadge");
    },
    alreadyGotBadge : function () {
        
        exportRoot.mc_PostRoomBadgeAlreadyEarned.mc_FinalScore.mc_Score.text = "" + gamePost.currentScore;
        exportRoot.mc_PostRoomBadgeAlreadyEarned.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gamePost.GAME_NAME, gamePost.currentScore);
        });
        exportRoot.mc_PostRoomBadgeAlreadyEarned.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gamePost.GAME_NAME, gamePost.currentScore);
        });
        exportRoot.mc_PostRoomBadgeAlreadyEarned.mc_Leaderboard.addEventListener("click" , gamePost.showLeaderBoards);
        
        exportRoot.gotoAndStop("AlreadyGotBadge");
    },
    earnedBadge : function () {
        exportRoot.mc_PostRoomBadgeEarned.mc_FinalScore.mc_Score.text = "" + gamePost.currentScore;
        exportRoot.mc_PostRoomBadgeEarned.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gamePost.GAME_NAME, gamePost.currentScore);
        });
        exportRoot.mc_PostRoomBadgeEarned.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gamePost.GAME_NAME, gamePost.currentScore);
        });
        exportRoot.mc_PostRoomBadgeEarned.mc_Leaderboard.addEventListener("click" , gamePost.showLeaderBoards);
        exportRoot.gotoAndStop("EarnedBadge");
    },
    earnedAllBadge : function () {

        //
        exportRoot.mc_WinnersCup.mc_Close.addEventListener("click" , function (e) {
            //show leader board
            console.log("close winner cup");
            exportRoot.gotoAndPlay("CloseEarnedBadge");
        });
        exportRoot.mc_WinnersCup.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gamePost.GAME_NAME, gamePost.currentScore);
        });
        exportRoot.mc_WinnersCup.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gamePost.GAME_NAME, gamePost.currentScore);
        });


        exportRoot.mc_PostRoomBadgeEarned.mc_FinalScore.mc_Score.text = "" + gamePost.currentScore;
        exportRoot.mc_PostRoomBadgeEarned.mc_Facebook.addEventListener("click" , function (e) {
            apiHelper.shareFacebookViaScorePopup(gamePost.GAME_NAME, gamePost.currentScore);
        });
        exportRoot.mc_PostRoomBadgeEarned.mc_Twitter.addEventListener("click" , function (e) {
            apiHelper.shareTwitterViaScorePopup(gamePost.GAME_NAME, gamePost.currentScore);
        });
        exportRoot.mc_PostRoomBadgeEarned.mc_Leaderboard.addEventListener("click" , gamePost.showLeaderBoards);

        exportRoot.gotoAndPlay("EarnedBadge");
    },
    showLeaderBoards : function () {
        apiHelper.openLeaderBoards(gamePost.GAME_ID);
        //exportRoot.gotoAndStop("PostRoomLeaderboard");
    },
    fixBrowserCompatible : function () {
        if (browser.mozilla) {
            try{
                exportRoot.txt_ChooseWorkWear.setTransform(474,25.7,1.29,1.29);
                exportRoot.txt_GameTitle.setTransform(474,20.7,1.29,1.29);
                exportRoot.mc_TimeUpTitle.setTransform(469.4,20.7,1.29,1.29);
            }catch(e){}
        }
        
    }
}



var gameTimer = {
    timer : null,
    seconds : 10,
    updateStatus : null,
    counterEnd : null,
    decrementCounter : function () {
        gameTimer.updateStatus(gameTimer.seconds);
        if (gameTimer.seconds === 0) {
            gameTimer.counterEnd();
            gameTimer.stop();
        }
        gameTimer.seconds--;
    },

    start : function() {
        clearInterval(gameTimer.timer);
        gameTimer.timer = 0;
        //seconds = options.seconds;
        gameTimer.timer = setInterval(gameTimer.decrementCounter, 1000);
    },
    stop : function() {
        gameTimer.updateStatus = null;
        gameTimer.counterEnd = null;
        gameTimer.seconds = -1;
        clearInterval(this.timer);
    },
    increaseTimer : function (newTime) {
        gameTimer.seconds = parseInt(gameTimer.seconds) + parseInt(newTime);
        //console.log("new timer " + seconds);
    },
    getRemainTime : function () {
        return gameTimer.seconds;
    }
}