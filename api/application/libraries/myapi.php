<?php

class myapi
{
	var $xw = null;
	
	
function myapi(){
	$this->obj =& get_instance();
		$this->xw = new XMLWriter();
        $this->xw->openURI('php://output');
        $this->xw->setIndent(true);
       // $this->obj->load->library('myuser');
	}
	
function requires_auth(&$my_profile){
    	$result='';
    	$this->obj->load->model('user_model');
    	if (!isset($_SERVER['PHP_AUTH_USER'])) {
        	# This header makes basic auth go
           	header('WWW-Authenticate: Basic realm="Keyneem API"');
			 
            # If the user hits cancel -- bam!
            $result='User has no profile.';
   		} else {
          	$nickname = $_SERVER['PHP_AUTH_USER'];
          	$password = $_SERVER['PHP_AUTH_PW'];                
          	
            $profile =$this->obj->user_model->is_exist_user($nickname, $password);
          	if ($profile ){ 
             	$my_profile =$profile ;
             	return $result;
            }
            $result='Authentication failed';
       }
      	return $result;
    }
    function update_user_role($profile){ 
         $api_user = new Myuser();       
         $api_user->update_user_obj($profile);
         return $api_user;
    }
    function twitter_story_array($story, $add_photo=true){
    	$twitter_story = array();
    	if (is_array($story)){ 
    		$twitter_story = $story;
    		$story_id = $story['id'];
    	}
    	else {
	    	$twitter_story['id'] = intval($story->id);
	        $twitter_story['name'] = $story->name;
	        $twitter_story['picture'] = $story->picture;
	        $twitter_story['description'] = $story->description;
	        $twitter_story['topic_id'] = $story->topic_id;
	        $twitter_story['user_id'] = $story->user_id;
	        $twitter_story['glance'] = $story->glance;
	        $twitter_story['slogan'] = $story->slogan;
	        $twitter_story['template_type'] = $story->template_type;
	        $twitter_story['created'] = $story->created;
	        $twitter_story['published'] = $story->published;
	        $twitter_story['modified'] = $story->modified;
	        
	        $story_id = intval($story->id);
    	}
        
    	if($add_photo){
        	$this->obj->load->model('story_photo_model');
        	$photos =$this->obj->story_photo_model->get_story_photos(array('story_id'=>$story_id));

        	if($photos && count($photos)>0){
        	   $twitter_story['photos']=$this->story_photos_array($photos);
        	}
        }
        
 
	    return $twitter_story;
    }  

	function story_photos_array($photos){
     	$photo_arr = array();
	    if (is_array($photos)) {
            foreach ($photos as $ph) {
                $twitter_story_photo = $this->twitter_story_photo_array($ph);
                array_push($photo_arr, $twitter_story_photo);
            }
        } else {
            while ($photos->fetch()) {
                $twitter_story_photo = $this->twitter_story_photo_array($photos);
                array_push($photo_arr, $twitter_story_photo);
            }
        }
        return $photo_arr;
    }
    
	function twitter_story_photo_array($photo){
    	$twitter_story_photo = array();
    	$twitter_story_photo['id'] = intval($photo->id);
        $twitter_story_photo['story_id'] = $photo->story_id;
        $twitter_story_photo['url'] = $photo->url;
        $twitter_story_photo['name'] = $photo->name;
        $twitter_story_photo['description'] = $photo->description; 
        $twitter_story_photo['created'] = $photo->created;
        $twitter_story_photo['modified'] = $photo->modified;
		
	    return $twitter_story_photo;
    }
    
     function twitter_photo_array($photo,$add_comment=0){
       	$twitter_photo['id'] = intval($photo->id);
        $twitter_photo['url'] = $photo->url;
        $twitter_photo['name'] = $photo->name;     
        $twitter_photo['thumbnail'] = $photo->thumbnail;
        $twitter_photo['description'] = $photo->description;
        $twitter_photo['price'] = $photo->price;
        //if(isset($photo->rating))
		$twitter_photo['rating'] = isset($photo->rating) ?$photo->rating :0;
		$twitter_photo['num_comment']= isset($photo->num_comment) ?$photo->num_comment :0;
		
        $twitter_photo['restaurant_id'] = $photo->restaurant_id;  
        
        $restaurant_name=isset($photo->restaurant_name) ? $photo->restaurant_name : null;
		$twitter_photo['restaurant_name'] = $restaurant_name;
		
		$restaurant_address=isset($photo->restaurant_address) ? $photo->restaurant_address : null;
		$twitter_photo['restaurant_address'] =$restaurant_address;
		        	    
		$twitter_photo['user_id'] = $photo->user_id;
		$twitter_photo['fullname'] = isset($photo->fullname)?$photo->fullname:null;
        
		
        $twitter_photo['created'] = $photo->created;     
        $twitter_photo['modified'] = $photo->modified;
        $twitter_photo['priority'] = $photo->priority;
        
     	if($add_comment){
        	$this->obj->load->model('comment_model');
        	$comments =$this->obj->comment_model->get_comments(array('photo_id'=>intval($photo->id)));

        	//$twitter_photo['num_comment']= count($comments);
        	
        	if($comments && count($comments)>0){
        	   $twitter_photo['comments']=$this->photo_comments_array($comments);
        	}
        }
        
        
		  return $twitter_photo;
    } 
    
function photo_comments_array($comments){
     $comment_arr = array();
	    if (is_array($comments)) {
            foreach ($comments as $ph) {
                $twitter_comment = $this->twitter_comment_array($ph);
                array_push($comment_arr, $twitter_comment);
            }
        } else {
            while ($comments->fetch()) {
                $twitter_comment = $this->twitter_comment_array($comments);
                array_push($comment_arr, $twitter_comment);
            }
        }
        return $comment_arr;
    }
    function twitter_comment_array($comment){
    	$twitter_comment = array();
    	$twitter_comment['id'] = intval($comment->id);
        $twitter_comment['photo_id'] = $comment->photo_id;
        $twitter_comment['user_id'] = intval($comment->user_id);     
        $twitter_comment['text'] = $comment->text; 
        $twitter_comment['video'] = $comment->video;
        $twitter_comment['created'] = $comment->created;
	    $twitter_comment['rate'] = $comment->rate;  
	    
	    $twitter_comment['name'] = $comment->name;
        $twitter_comment['facebook_id'] = $comment->facebook_id;
        $twitter_comment['gender'] = $comment->gender;
        $twitter_comment['location'] = $comment->location;
        $twitter_comment['avatar'] = $comment->avatar;
		
	    return $twitter_comment;
    }
    function twitter_hot_deal_array($hot_deal){
       	$twitter_hot_deal['id'] = intval($hot_deal->id);
        $twitter_hot_deal['restaurant_id'] = $hot_deal->restaurant_id;
        $twitter_hot_deal['user_id'] = $hot_deal->user_id;  
        $twitter_hot_deal['deal'] = $hot_deal->deal;
        $twitter_hot_deal['terms'] = $hot_deal->terms; 
        $twitter_hot_deal['description'] = $hot_deal->description;    
        $twitter_hot_deal['expiration_date'] = $hot_deal->expiration_date;
        $twitter_hot_deal['created'] = $hot_deal->created;     
        $twitter_hot_deal['modified'] = $hot_deal->modified;
 
        return $twitter_hot_deal;
    } 
    
	function twitter_story_hot_deal_array($hot_deal){
       	$twitter_story_hot_deal['id'] = intval($hot_deal->id);
        $twitter_story_hot_deal['topic_id'] = $hot_deal->topic_id;
        $twitter_story_hot_deal['user_id'] = $hot_deal->user_id;  
        $twitter_story_hot_deal['deal'] = $hot_deal->deal;
        $twitter_story_hot_deal['terms'] = $hot_deal->terms; 
        $twitter_story_hot_deal['description'] = $hot_deal->description;    
        $twitter_story_hot_deal['expiration_date'] = $hot_deal->expiration_date;
        $twitter_story_hot_deal['created'] = $hot_deal->created;     
        $twitter_story_hot_deal['modified'] = $hot_deal->modified;
 
        return $twitter_story_hot_deal;
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////	
function show_json_objects($objects)
    {
        print(json_encode($objects));
    }
	
function init_document($type='xml')
    {
        switch ($type) {
         case 'xml':
            header('Content-Type: application/xml; charset=utf-8');
            $this->startXML();
            break;
         case 'json':
            header('Content-Type: application/json; charset=utf-8');

//            $callback = $this->arg('callback');
//            if ($callback) {
//                print $callback . '(';
//            }
            break;
         case 'rss':
            header("Content-Type: application/rss+xml; charset=utf-8");
            $this->init_twitter_rss();
            break;
         case 'atom':
            header('Content-Type: application/atom+xml; charset=utf-8');
            $this->init_twitter_atom();
            break;
         default:
            $this->client_error(_('Not a supported data format.'));
            break;
        }

        return;
    }

 function end_document($type='xml')
    {
        switch ($type) {
         case 'xml':
            $this->endXML();
            break;
         case 'json':

            // Check for JSONP callback
//            $callback = $this->arg('callback');
//            if ($callback) {
//                print ')';
//            }
            break;
         case 'rss':
            $this->end_twitter_rss();
            break;
         case 'atom':
            $this->end_twitter_rss();
            break;
         default:
            $this->client_error(_('Not a supported data format.'));
            break;
        }
        return;
    }

 
 function init_twitter_rss()
    {
        $this->startXML();
        $this->elementStart('rss', array('version' => '2.0'));
    }

 function end_twitter_rss()
    {
        $this->elementEnd('rss');
        $this->endXML();
    }

 function init_twitter_atom()
    {
        $this->startXML();
        $this->elementStart('feed', array('xmlns' => 'http://www.w3.org/2005/Atom', 'xml:lang' => 'en-US'));
    }

 function end_twitter_atom()
    {
        $this->endXML();
        $this->elementEnd('feed');
    }
    
 function client_error($msg, $code = 400, $content_type = 'json')
    { 
         $header=$this->get_header($code);
//        $header='HTTP/1.1 401 Unauthorized';
//         header('HTTP/1.1 '.$code. ' '. $header);

        if ($content_type == 'xml') {
            $this->init_document('xml');
            $this->elementStart('hash');
            $this->element('error', null,$msg);
            $this->element('request', null, $_SERVER['REQUEST_URI']);
            $this->elementEnd('hash');
            $this->end_document('xml');
        } else {
            $this->init_document('json');
            $error_array = array('error' => $msg, 'request' => $_SERVER['REQUEST_URI']);
            print(json_encode($error_array));
            $this->end_document('json');
        }

    }
    
    
function get_header($code = 400)
    { 
      $status_string='HTTP/1.1 401 Unauthorized';
      $status = array(400 => 'Bad Request',
                      401 => 'Unauthorized',
                      402 => 'Payment Required',
                      403 => 'Forbidden',
                      404 => 'Not Found',
                      405 => 'Method Not Allowed',
                      406 => 'Not Acceptable',
                      407 => 'Proxy Authentication Required',
                      408 => 'Request Timeout',
                      409 => 'Conflict',
                      410 => 'Gone',
                      411 => 'Length Required',
                      412 => 'Precondition Failed',
                      413 => 'Request Entity Too Large',
                      414 => 'Request-URI Too Long',
                      415 => 'Unsupported Media Type',
                      416 => 'Requested Range Not Satisfiable',
                      417 => 'Expectation Failed');

    if (!array_key_exists($code, $status)) {
      $code = 401;
    }

    $status_string = $status[$code];
        
    return $status_string;
  }     
 //xmloutput
 
 
 function startXML($doc=null, $public=null, $system=null)
    {
        $this->xw->startDocument('1.0', 'UTF-8');
        if ($doc) {
            $this->xw->writeDTD($doc, $public, $system);
        }
    }
    
    
    
 function endXML()
    {
        $this->xw->endDocument();
        $this->xw->flush();
    }
    
    
    
    
 function element($tag, $attrs=null, $content=null)
    {
        $this->elementStart($tag, $attrs);
        if (!is_null($content)) {
            $this->xw->text($content);
        }
        $this->elementEnd($tag);
    }
    
    
    
function elementStart($tag, $attrs=null)
    {
        $this->xw->startElement($tag);
        if (is_array($attrs)) {
            foreach ($attrs as $name => $value) {
                $this->xw->writeAttribute($name, $value);
            }
        } else if (is_string($attrs)) {
            $this->xw->writeAttribute('class', $attrs);
        }
    }
    
    
    
function elementEnd($tag)
    {
        static $empty_tag = array('base', 'meta', 'link', 'hr',
                                  'br', 'param', 'img', 'area',
                                  'input', 'col');
        // XXX: check namespace
        if (in_array($tag, $empty_tag)) {
            $this->xw->endElement();
        } else {
            $this->xw->fullEndElement();
        }
    }
    
    
 function text($txt)
    {
        $this->xw->text($txt);
    }
    
    
 function raw($xml)
    {
        $this->xw->writeRaw($xml);
    }

    
 function date_twitter($dt)
    {
        $t = strtotime($dt);
        return date("D M d G:i:s O Y", $t);
    }
}
?>