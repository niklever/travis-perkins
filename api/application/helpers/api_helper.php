<?php
//define('APP_API_KEY', '045a0e95083f071a09d96526a07a0756');
function get_error_msg($error_code){
$ERR = array(
		'100' => 'Missing API key',
		'101' => 'Permission denied. API KEY is not correct.',
		'201' => 'Missing API function name',
		'202' => 'API function do not exist',
		'203' => 'Missing required parameters.',
		'204' => 'Empty data query based on the key refs',
		'302' => 'Email is not correct',
		'303' => 'There was an error submitting your information<br/> Your email already exists in our system.',
		'304' => 'Email is not exist',
		'305' => 'Email or password is not correct',
        '306' => 'Task category id is not exist',
        '307' => 'User id is not exist',
        '308' => 'Task id is not exist',
        '309' => 'Review id is not exis',
        '501' => 'Token is not exist',
		'502' => 'Can not process post request. Please try again later.',
		'404' => 'Authentication failed'
	);
	$eror_msg=isset($ERR[$error_code])?$ERR[$error_code]:'Eror';
	return $eror_msg;
}	
function api_return($status, $message = null, $data = null){
   header('Access-Control-Allow-Origin: *');
	header("Access-Control-Allow-Methods: *");
		//header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
	header('Access-Control-Allow-Headers: Authorization');
	header('Content-Type: application/json');
	$_json = new stdClass();
	if(!empty($status)){
		$_json->status = $status;
		if(isset($message) && !empty($message))
		          $_json->message = $message;
		if($status=='OK'){
		     if(!empty($data)){
				$_json->data = $data;
				echo json_encode($_json);
				die;
			//} else {
			//	die('Missing return data');
			}
		}else{
			 echo json_encode($_json);
			die;
		}		
	} else {
		die('Missing API status');
	}
	die;
}

function api_return_message($json){
    header('Access-Control-Allow-Origin: *');
	header("Access-Control-Allow-Methods: *");
		//header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
	header('Access-Control-Allow-Headers: Authorization');
    header('Content-Type: application/json');
	if(!empty($json)){
		 echo json_encode($json);
		die;
		
	} else {
		die('Missing API return');
	}
	
}

function create_acesstoken(){
mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
        return $uuid;
}

function get_win_msg($win_badge){
   if($win_badge==1) return 'Did not earn badge';
   if($win_badge==2) return 'Already earn badge';
   if($win_badge==3) return 'Earned New Badge';
   if($win_badge>=4) return 'Earned all 4 badges';
   return '';
}
function get_country($country_code=''){
		$country_array = array( 		
		"Avon" => "Avon",
		"Bedfordshire" => "Bedfordshire",
		"Berkshire" => "Berkshire",
		"Borders" => "Borders",
		"Buckinghamshire" => "Buckinghamshire",
		"Cambridgeshire" => "Cambridgeshire",
		"Central" => "Central",
		"Cheshire" => "Cheshire",
		"Cleveland" => "Cleveland",
		"Clwyd" => "Clwyd",
		"Cornwall" => "Cornwall",
		"County Antrim" => "County Antrim",
		"County Armagh" => "County Armagh",
		"County Down" => "County Down",
		"County Fermanagh" => "County Fermanagh",
		"County Londonderry" => "County Londonderry",
		"County Tyrone" => "County Tyrone",
		"Cumbria" => "Cumbria",
		"Derbyshire" => "Derbyshire",
		"Devon" => "Devon",
		"Dorset" => "Dorset",
		"Dumfries and Galloway" => "Dumfries and Galloway",
		"Durham" => "Durham",
		"Dyfed" => "Dyfed",
		"East Sussex" => "East Sussex",
		"Essex" => "Essex",
		"Fife" => "Fife",
		"Gloucestershire" => "Gloucestershire",
		"Grampian" => "Grampian",
		"Greater Manchester" => "Greater Manchester",
		"Gwent" => "Gwent",
		"Gwynedd County" => "Gwynedd Count",
		"Hampshire" => "Hampshire",
		"Herefordshire" => "Herefordshire",
		"Highlands and Islands" => "Highlands and Islands",
		"Humberside" => "Humberside",
		"Isle of Wight" => "Isle of Wight",
		"Kent" => "Kent",
		"Lancashire" => "Lancashire",
		"Leicestershire" => "Leicestershire",
		"Lincolnshire" => "Lincolnshire",
		"Lothian" => "Lothian",
		"Merseyside" => "Merseyside",
		"Mid Glamorgan" => "Mid Glamorgan",
		"Norfolk" => "Norfolk",
		"North Yorkshire" => "North Yorkshire",
		"Northamptonshire" => "Northamptonshire",
		"Northumberland" => "Northumberland",
		"Nottinghamshire" => "Nottinghamshire",
		"Oxfordshire" => "Oxfordshire",
		"Powys" => "Powys",
		"Rutland" => "Rutland",
		"Shropshire" => "Shropshire",
		"Somerset" => "Somerset",
		"South Glamorgan" => "South Glamorgan",
		"South Yorkshire" => "South Yorkshir",
		"Staffordshire" => "Staffordshire",
		"Strathclyde" => "Strathclyde",
		"Suffolk" => "Suffolk",
		"Surrey" => "Surrey",
		"Tayside" => "Tayside",
		"Tyne and Wear" => "Tyne and Wear",
		"Warwickshire" => "Warwickshire",
		"West Glamorgan" => "West Glamorgan",
		"West Midlands" => "West Midlands",
		"West Sussex" => "West Sussex",
		"West Yorkshire" => "West Yorkshire",
		"Wiltshire" => "Wiltshire",
		"Worcestershire" => "Worcestershire"
		);
	if(!empty($country_code)){
         $country=isset($country_array[$country_code])?$country_array[$country_code]:'';
		 return $country;
	}
	return 	$country_array; 
}		
?>