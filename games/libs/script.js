/**
@ var = parent : GLOBAL value Flash;
*/
var mc_AvatarPanel;
var mnMap;
var defineValueDefault = {
    wrapContainer: "#myGame",
    loadPositionCanvas: function () {
        return {
            x: $('canvas').offset().left,
            y: $('canvas').offset().top
        }
    },
    initLoadLocationtheCanvas: function (_object) {
        loadLocationCanvas.setPositionCanvasLogin(_object);
    },
    checkButtonLogIn: function () {
        if ($.localStorage.isSet('isUser')) {
            exportRoot.btn_Login.childbtLogin.gotoAndStop(1);
        }
    },
    checkUserisLogin: function () {
        if (!$.localStorage.isSet('isUser')) {
            return false;
        } else return true;
    },
    checkexitsAvatar: function () {
        if ($.localStorage.isSet('storageIDAvatar')) {
            exportRoot.btn_SelectAvatar.AvatarDefault.gotoAndStop($.localStorage.get('storageIDAvatar') - 1);
        } else return true;
    },
    checkexitsTwitter: function () {
        if ($.localStorage.get('IDTwitter')) {
            exportRoot.gotoAndPlay('LoginOpen'); //40
            exportRoot.mc_LoginPanel.gotoAndStop(1);
            loginLocal.drawingCreateNewAccount(false);
            $("#userName_creatAccount").val($.localStorage.get('userNameTwitter'));
            $("#submit_createAccount").click(function () {
                loginLocal.createNewAccountbyTwitter();
            })
        }
        return false;
    },
    checkdefaultAvatar: function () {

        exportRoot.mc_ChooseAvatarPanel.mc_AvatarSteve.cursor = "pointer";
        exportRoot.mc_ChooseAvatarPanel.mc_AvatarKate.cursor = "pointer";

        exportRoot.mc_ChooseAvatarPanel.mc_AvatarGreg.cursor = "pointer";
        exportRoot.mc_ChooseAvatarPanel.mc_AvatarJane.cursor = "pointer";
        exportRoot.mc_ChooseAvatarPanel.mc_AvatarJason.cursor = "pointer";
        exportRoot.mc_ChooseAvatarPanel.mc_AvatarFrank.cursor = "pointer";
        exportRoot.mc_ChooseAvatarPanel.mc_AvatarLucy.cursor = "pointer";
        exportRoot.mc_ChooseAvatarPanel.mc_AvatarPeppa.cursor = "pointer";

        console.log("set default avatar " + $.localStorage.get('storageIDAvatar'));
        if (!$.localStorage.isSet('storageIDAvatar')) {
            
            $.localStorage.set('storageIDAvatar', "1");
            console.log("set default avatar " + $.localStorage.get('storageIDAvatar'));
        }
    }
};

var mainMenu = {
    currentLeaderBoard: 0,
    initGame: function () {
        console.log("initGame");
        /* function InitLoad */
        defineValueDefault.checkButtonLogIn();
        defineValueDefault.checkexitsAvatar();
        defineValueDefault.checkdefaultAvatar();
        // defineValueDefault.checkexitsTwitter();


        //console.log($.localStorage.get('IDTwitter'));
        //$.localStorage().clear();

        exportRoot.mc_WinnerCup.visible = false;
        exportRoot.mc_BadgePostGame.visible = false;
        exportRoot.mc_BadgeYardGame.visible = false;
        exportRoot.mc_BadgeCounterGame.visible = false;
        exportRoot.mc_BadgeDriverGame.visible = false;

        exportRoot.mc_ChooseAvatarPanel.mc_bg.addEventListener ("click", function () {

        });

        exportRoot.mc_LoginPanel.mc_bg.addEventListener ("click", function () {

        });

        apiHelper.user_profile(function (data) {
            if (data && data.games.length > 0) {
                var badgeCount = 0;
                for (var i = 0; i < data.games.length; i++) {

                    if (data.games[i].earnedBadge == 1) {
                        if (data.games[i].id == 1) {
                            badgeCount++;
                            exportRoot.mc_BadgeCounterGame.visible = true;
                        } else if (data.games[i].id == 2) {
                            badgeCount++;
                            exportRoot.mc_BadgeDriverGame.visible = true;
                        } else if (data.games[i].id == 3) {
                            badgeCount++;
                            exportRoot.mc_BadgeYardGame.visible = true;
                        } else if (data.games[i].id == 4) {
                            badgeCount++;
                            exportRoot.mc_BadgePostGame.visible = true;
                        }
                    }
                }
                if (badgeCount == data.games.length) {
                    exportRoot.mc_WinnerCup.visible = true;
                }
            }
            defineValueDefault.checkButtonLogIn();
        });


        exportRoot.btn_SelectAvatar.addEventListener('click', function (e) {

            exportRoot.gotoAndPlay('ChooseAvatarOpen'); //15
            /** 
             *@ Remove Element DOM HTML input at Box Login
             **/
            loginLocal.removeInputBoxLogin();
            apiHelper.removeListLeaderBoard();
            /** 
             *@ List event are used to change Avatar User;
             **/
            avatarGameLocal.avatarEventClick();

        });

        exportRoot.mc_ChooseAvatarPanel.btn_CloseSelectAvatar.cursor = "pointer";

        exportRoot.mc_ChooseAvatarPanel.btn_CloseSelectAvatar.addEventListener('click', function () {
            exportRoot.gotoAndPlay('ChooseAvatarClose'); //27
        });


        exportRoot.btn_Login.addEventListener('click', function (e) {
            loginLocal.removeInputBoxLogin();
            if (exportRoot.btn_Login.childbtLogin.currentFrame == 0) {
                // mainMenu.addHandleClickForGameButtons(false);
                //exportRoot.mc_LoginPanel.gotoAndStop(0);
                console.log("open login");
                exportRoot.gotoAndPlay('LoginOpen'); //40
                exportRoot.mc_LoginPanel.gotoAndStop(0);
            } else {
                apiHelper.logout(exportRoot);
            }
        });


        exportRoot.mc_LoginPanel.btn_closeLogin.cursor = "pointer";

        exportRoot.mc_LoginPanel.btn_closeLogin.addEventListener('click', function () {
            /**
             *@ Remove Element DOM HTML input at Box Login
             **/
            loginLocal.removeInputBoxLogin();
            exportRoot.gotoAndPlay('LoginClose'); ///52

            // mainMenu.addHandleClickForGameButtons(true);


        });

        exportRoot.mc_LoginPanel.btn_createNewAccount.addEventListener('click', function (e) {
            loginLocal.removeInputBoxLogin();
            exportRoot.mc_LoginPanel.gotoAndStop(1);
            loginLocal.drawingCreateNewAccount(true);

            // $("#submit_createAccount").click(function () {
            //     loginLocal.creatAccount();
            // })

        });

        exportRoot.mc_LoginPanel.btn_creatAccountInside.cursor = "pointer";

        exportRoot.mc_LoginPanel.btn_creatAccountInside.addEventListener ("click", function (e) {
            loginLocal.creatAccount();
        });

        exportRoot.mc_LoginPanel.btn_forgotPassword.addEventListener('click', function (e) {
            loginLocal.removeInputBoxLogin();
            exportRoot.mc_LoginPanel.gotoAndStop(2);
            loginLocal.drawingLostPassword();

        })


        exportRoot.mc_LoginPanel.bnt_loginBox.addEventListener('click', function () {
            loginLocal.loginUser(exportRoot);
            //exportRoot.gotoAndPlay('LoginClose');//52
            // loginLocal.removeInputBoxLogin();

        });

        exportRoot.mc_LoginPanel.btn_loginFacebook.addEventListener('click', function () {
            FacebookSDK.loginFacebook();
        });
        exportRoot.mc_LoginPanel.btn_resetPassword.addEventListener('click', function () {
            loginLocal.resetPassword();
        });

        exportRoot.ShareOnFB.addEventListener('click', function () {
            apiHelper.shareFacebookViaGameMenu();
            /**
             *@ Remove Element DOM HTML input at Box Login
             **/
            loginLocal.removeInputBoxLogin();
        });
        exportRoot.ShareOnTwitter.addEventListener('click', function () {
            twitterLocal.loginTwitter('share');
            loginLocal.removeInputBoxLogin();
        })


        mainMenu.addHandleClickForGameButtons(true);
        mainMenu.addHandleLeaderBoardButtons();

        exportRoot.btn_leaderBoards.addEventListener('click', function (event) {
            exportRoot.gotoAndPlay('LeaderboardsOpen'); //65
            // apiHelper.runShowListLeaderBoard(1);
            loginLocal.removeInputBoxLogin();
            stage.removeChild(targetAvatar);
        });


        exportRoot.btn_Volume.addEventListener('click', function () {
            if (exportRoot.btn_Volume.currentFrame == 1) {
                exportRoot.btn_Volume.gotoAndStop(0);
                apiHelper.switchSound(true);
                createjs.Sound.play("sound_menu", {
                    interrupt: createjs.Sound.INTERRUPT_ANY,
                    loop: -1
                });
            } else {
                createjs.Sound.stop("sound_menu");
                apiHelper.switchSound(false);
                exportRoot.btn_Volume.gotoAndStop(1);
            }
        })

        exportRoot.mc_LoginPanel.btn_LoginTwitter.addEventListener('click', function () {
            twitterLocal.loginTwitter('login');
        });

        mainMenu.fixBrowserCompatible();
    },
    tickSoundLeadeboard: function () {
        if (exportRoot.btn_Volume.currentFrame == 1) {
            exportRoot.btn_Volume.gotoAndStop(0);
            apiHelper.switchSound(true);
            $("#isSound").attr('src',apiHelper.directMain + 'images/sound-1.png');
            createjs.Sound.play("sound_menu", {
                interrupt: createjs.Sound.INTERRUPT_ANY,
                loop: -1
            });
        } else {
            createjs.Sound.stop("sound_menu");
            apiHelper.switchSound(false);
            $("#isSound").attr('src',apiHelper.directMain + 'images/sound-0.png');
            exportRoot.btn_Volume.gotoAndStop(1);
        }
    },
    loginDialogOpen: function () {
        loginLocal.drawingInputBoxLogin();
    },
    openLeadDiaLogOpen: function () {
        apiHelper.removeListLeaderBoard();
        exportRoot.gotoAndPlay('LeaderboardsClosed'); //78 
        exportRoot.gotoAndPlay('LoginOpen'); //40
        mainMenu.loginDialogOpen();
    },
    startGamePost: function () {
        if (defineValueDefault.checkUserisLogin()) {
            exportRoot.gotoAndPlay('GoToPostRoomGame');
            loginLocal.removeInputBoxLogin();

        } else {
            apiHelper.callAlert('login_required');
            return false;
        }
    },
    startGameYard: function () {
        if (defineValueDefault.checkUserisLogin()) {
            exportRoot.gotoAndPlay('GoToYardGame');
            stage.removeChild(targetAvatar);
            loginLocal.removeInputBoxLogin();
            //yardgameLocal.redirectYard();
        } else {
            apiHelper.callAlert('login_required');
            return false;
        }
    },
    startGameDriver: function () {
        if (defineValueDefault.checkUserisLogin()) {
            exportRoot.gotoAndPlay('GoToDeliveryTruckGame');
            loginLocal.removeInputBoxLogin();
            //countergameLocal.redirectCounter(exportRoot);
        } else {
            apiHelper.callAlert('login_required');
            return false;
        }
    },
    startGameCounter: function () {
        if (defineValueDefault.checkUserisLogin()) {
            exportRoot.gotoAndPlay('GoToCounterGame');
            loginLocal.removeInputBoxLogin();
            //countergameLocal.redirectCounter(exportRoot);
        } else {
            apiHelper.callAlert('login_required');
            return false;
        }
    },
    addHandleClickForGameButtons: function (state) {
        if (state) {
            //add click
            exportRoot.btn_GotoGameYard.addEventListener("click", mainMenu.startGameYard);
            exportRoot.btn_GotoGameCounter.addEventListener("click", mainMenu.startGameCounter);
            exportRoot.btn_GotoGamePostRoom.addEventListener("click", mainMenu.startGamePost);
            exportRoot.btn_GotoGameDeliveryTruck.addEventListener("click", mainMenu.startGameDriver);
        } else {
            exportRoot.btn_GotoGameYard.removeAllEventListeners();
            exportRoot.btn_GotoGameCounter.removeAllEventListeners();
            exportRoot.btn_GotoGamePostRoom.removeAllEventListeners();
            exportRoot.btn_GotoGameDeliveryTruck.removeAllEventListeners();
        }
    },
    addHandleLeaderBoardButtons: function () {
        exportRoot.mc_LeaderBoardPanel.btn_closeMainLeaderBoard.addEventListener("click", mainMenu.closeLeaderboard);
        exportRoot.mc_LeaderBoardPanel.mc_NextLeaderBoard.addEventListener("click", mainMenu.nextLeaderBoard);
        exportRoot.mc_LeaderBoardPanel.mc_PrevLeaderBoard.addEventListener("click", mainMenu.prevLeaderBoard);
        exportRoot.mc_LeaderBoardPanel.mc_GameMenuLeaderBoard.addEventListener("click", mainMenu.showBoxOptionLeaderBorad);
    },
    showBoxOptionLeaderBorad: function () {
        apiHelper.runShowOptionalLeaderBoard(exportRoot);
    },
    showLoaderBoards: function () {
        mainMenu.currentLeaderBoard = 0;
        exportRoot.mc_LeaderBoardPanel.gotoAndStop(mainMenu.currentLeaderBoard);
        apiHelper.runShowListLeaderBoard();
    },
    closeLeaderboard: function () {
        loginLocal.removeInputBoxLogin();
        apiHelper.removeListLeaderBoard();
        exportRoot.gotoAndPlay('LeaderboardsClosed'); //78  
    },
    nextLeaderBoard: function () {
        mainMenu.currentLeaderBoard++;
        if (mainMenu.currentLeaderBoard >= 5) {
            mainMenu.currentLeaderBoard = 0;
        }
        exportRoot.mc_LeaderBoardPanel.gotoAndStop(mainMenu.currentLeaderBoard);
        var gameId = "1";
        if (mainMenu.currentLeaderBoard == 1) {
            gameId = "1"; //counter
        }
        if (mainMenu.currentLeaderBoard == 2) {
            gameId = "4"; //post
        }
        if (mainMenu.currentLeaderBoard == 3) {
            gameId = "2"; //delivery
        }
        if (mainMenu.currentLeaderBoard == 4) {
            gameId = "3"; //yard
        }
        $("#showBoxLeaderBoard").mCustomScrollbar("destroy");
        apiHelper.runShowListLeaderBoard(gameId);
        $("#showBoxLeaderBoard").mCustomScrollbar({
            scrollButtons: {
                enable: true
            }
        });
    },
    prevLeaderBoard: function () {
        mainMenu.currentLeaderBoard--;
        if (mainMenu.currentLeaderBoard < 0) {
            mainMenu.currentLeaderBoard = 4;
        }
        exportRoot.mc_LeaderBoardPanel.gotoAndStop(mainMenu.currentLeaderBoard);
        var gameId = "1";
        if (mainMenu.currentLeaderBoard == 1) {
            gameId = "1"; //counter
        }
        if (mainMenu.currentLeaderBoard == 2) {
            gameId = "4"; //post
        }
        if (mainMenu.currentLeaderBoard == 3) {
            gameId = "2"; //delivery
        }
        if (mainMenu.currentLeaderBoard == 4) {
            gameId = "3"; //yard
        }
        $("#showBoxLeaderBoard").mCustomScrollbar("destroy");
        apiHelper.runShowListLeaderBoard(gameId);

        $("#showBoxLeaderBoard").mCustomScrollbar({
            scrollButtons: {
                enable: true
            }
        });
    },
    showAlertWelcomeBack : function (user) {
        exportRoot.mc_AlertBox.visible = true;
        exportRoot.mc_AlertBox.gotoAndStop(3);
        exportRoot.mc_AlertBox.txt_Welcome.text = "Welcome back " + user +". \nNow let’s have some fun!";
        exportRoot.mc_AlertBox.btn_Close_Login.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_Close_Login.addEventListener("click", function () {
            exportRoot.mc_AlertBox.visible = false;
        });
        exportRoot.mc_AlertBox.btn_OK_Login.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_OK_Login.addEventListener("click", function () {
            exportRoot.mc_AlertBox.visible = false;
        });

    },
    showAlertLoginRequired : function () {
        exportRoot.mc_AlertBox.visible = true;
        exportRoot.mc_AlertBox.gotoAndStop(0);
        exportRoot.mc_AlertBox.btn_Close.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_Close.addEventListener("click", function () {
            exportRoot.mc_AlertBox.visible = false;
        });
        exportRoot.mc_AlertBox.btn_OK.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_OK.addEventListener("click", function () {
            exportRoot.mc_AlertBox.visible = false;
        });
    },
    showAlertNoUserFound : function () {
        exportRoot.mc_AlertBox.visible = true;
        exportRoot.mc_AlertBox.gotoAndStop(1);
        exportRoot.mc_AlertBox.btn_Close.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_Close.addEventListener("click", function () {
            exportRoot.mc_AlertBox.visible = false;
        });
        exportRoot.mc_AlertBox.btn_OK.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_OK.addEventListener("click", function () {
            exportRoot.mc_AlertBox.visible = false;
        });
    },
    showAlertRegisterComplete : function (callback) {
        exportRoot.mc_AlertBox.visible = true;
        exportRoot.mc_AlertBox.gotoAndStop(1);
        exportRoot.mc_AlertBox.btn_Close_Reg.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_Close_Reg.addEventListener("click", function () {
            exportRoot.mc_AlertBox.visible = false;
        });
        exportRoot.mc_AlertBox.btn_OKReg.removeAllEventListeners();
        exportRoot.mc_AlertBox.btn_OKReg.addEventListener("click", function () {
            exportRoot.mc_AlertBox.visible = false;
        });
    },
    fixBrowserCompatible : function () {
        if (browser.mozilla) {
            exportRoot.txt_Title.setTransform(270.9,65.5,0.8,0.8);
            exportRoot.mc_SelectGame.txt_SelectGame.setTransform(102.5,12.7);
            exportRoot.btn_leaderBoards.instance.text.setTransform(14.1,8.7);
            exportRoot.btn_Login.childbtLogin.text.setTransform(13.1,6.4);
            exportRoot.mc_ChooseAvatarPanel.text.setTransform(443.7,10.2,1.29,1.29);

            if (browser.mozilla || browser.safari) {
                exportRoot.mc_AlertBox.txt_TitleWelcome.setTransform(23.5,8);
                exportRoot.mc_AlertBox.txt_TitleLoginRequired.setTransform(30,8);
                exportRoot.mc_AlertBox.txt_TitleNoUser.setTransform(30,8);
                exportRoot.mc_AlertBox.txt_TitleRegComplete.setTransform(30,8);
            }
            exportRoot.mc_LoginPanel.txt_TitleResetPass.setTransform(30,115);
            exportRoot.mc_LoginPanel.text.setTransform(30,8);
            exportRoot.mc_LoginPanel.txt_TitleLogin.setTransform(30,8);
            
            exportRoot.mc_LoginPanel.text.setTransform(30,8);

        }
    }
};