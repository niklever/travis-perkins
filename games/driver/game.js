// Travis Perkins - The Delivery Truck
// main game functionality script

var debugging = false;

var GAME_NAME = "The Delivery Truck";
var GAME_ID = "2";
var GAME_NAME = "driver";

var gameMode = "menu";
var stopMC = null;
var forklift = { root:null, truck:null, forks:null, forksBase:null, forksBaseB:null, forksAnim:null, avatar:null, truckB:null, avatarB:null,
		speed:0, accel: 0, direction:0, lift:0, cx:0, cy:0, z:0, z0:0, forkZ:0, cxReset:0, cyReset:0, underControl:false };
var lift = {dir:-1, dy:5, autoLower:false, auto:false, target:0, level:0, pickup:2, raised:9, raisedHigh:18, lorryBase:6, topBase:12, topBaseHalf:9}; // updated Paul 14/10 - previously dir:0
var directions = new Array({x:0, y:1}, {x:1, y:0}, {x:0, y:-1}, {x:-1, y:0});
var collisionPoints = new Array({x:1, y:1}, {x:1, y:0}, {x:0, y:0}, {x:0, y:0});

var lorry = {mc:null, hit:null, loadarea:null, roof:null, pallets:null};
var pallet = {mc:null, pal:null, lifted:false, raised:false, frm:0, loaded:false, zInLorry:0, xReset:0, yReset:0, zReset:0};
var palletDescription;

var pothole;
var hitPothole;

var dudes;

var tiles;
var tileSize = 55;
var tileOriginZ;
var tileInLorryZ;
var lorryRoofZ;
var tilesZs;

var grids;
var gridIndex = 0;
var gridOrigin = {x:615, y:118};
var gridSize = {cols:7, rows:10};

var gameLevel;
var palletsToLoad;
var palletsOrder;
var palletsLoaded;
var palletLoadedCorrectly;

var palletNames = new Array("blank", "empty", "bricks", "concrete lintels", "top soil", "fabrics", "paving stones", "sand bags", "planters", "posts", "decking",
			    "concrete lintels", "bricks", "paving stones", "decking");

var topbar;
var topbarZ;
var quitConfirm;

var lives;
var score;
var timeLeft;
var timeInterval;
var palletTime;

var avatarType = 0;
var avatarIndex = 0;
var avatarId = 0;
var avatarActive;

var firstPlay = true;
var infopanel;
var infoStage = "prestart";
var reminder;
var boss;
var bossWarning = "";
var hsofficer;
var hsWarnings = new Array("don't travel or turn with your forks raised - it's a safety hazard.",
			   "pick a pallet up evenly or items are likely to fall off.",
			   "load the pallets neatly in the truck so you can fit 3 across.",
			   "avoid the potholes as your forklift can easily topple over.",
			   "stay at least 2m away from pedestrians.",
			   "don't try to pickup too much with your forklift.",
			   "you can push pallets back in the truck when loading a new one.",
			   "move pallets out of the way to get to the pallet you need."); // "you can load pallets on top of each other in the truck."
var hsIndex = 0;
var hsActive = false;

function isMalePlayer () {
	if ($.localStorage.get("storageIDAvatar") == 1 || $.localStorage.get("storageIDAvatar") == 5 || $.localStorage.get("storageIDAvatar") == 2 || $.localStorage.get("storageIDAvatar") == 6) {
		//male player: Steve, Frank..etc
        return true;
    }else{
        return false;
    }
}

function initAvatar(male){
	if (male){
		exportRoot.jason_mc.visible = false;
		exportRoot.frank_mc.visible = false;
		exportRoot.greg_mc.visible = false;
		exportRoot.steve_mc.visible = false;
	}else{
		exportRoot.kate_mc.visible = false;
		exportRoot.peppa_mc.visible = false;
		exportRoot.lucy_mc.visible = false;
		exportRoot.jane_mc.visible = false;
	}
	var id = $.localStorage.get("storageIDAvatar");
	var avatars = [ exportRoot.steve_mc, exportRoot.greg_mc, exportRoot.jane_mc, exportRoot.kate_mc, exportRoot.jason_mc, exportRoot.frank_mc, exportRoot.lucy_mc, exportRoot.peppa_mc ];
	avatars[id-1].visible = true;
	avatarActive = avatars[id-1];
	var ids = [ 0, 2, 10, 8, 4, 6, 12, 14];
	avatarId = ids[id-1];
	//avatarId = 10;
}

function InitGame()
{
	console.log("InitGame");
	//Init game from apiHelper
	apiHelper.init(GAME_ID);  
	
	exportRoot.stop();
	exportRoot.sfx_mc.stop();
	
	// forklift object	
	forklift.root = exportRoot.forklift_mc;
	forklift.truck = exportRoot.forklift_mc.truck_mc;
	forklift.forksBase = exportRoot.forklift_mc.lift_mc;
	forklift.forks = exportRoot.forks_mc;
	forklift.forksAnim = exportRoot.forks_mc.forks_mc;
	forklift.avatar = exportRoot.forklift_mc.truck_mc.avatar_mc;
	forklift.truckB = exportRoot.forklift_mc.truckB_mc;
	forklift.avatarB = exportRoot.forklift_mc.truckB_mc.avatar_mc;
	forklift.forksBaseB = exportRoot.forklift_mc.liftB_mc;
	
	// lorry
	lorry.mc = exportRoot.lorry_mc;
	lorry.hit = lorry.mc.hit_mc;
	lorry.loadarea = lorry.mc.loading_mc;
	lorry.roof = exportRoot.lorryRoof_mc;
	lorry.pallets = new Array();
	//for (var i = 0;i < 12;i++) lorry.pallets.push({mc:null, cx:0, cy:0});
	
	pothole = exportRoot.pothole_mc;
	
	dudes = new Array();
	for (var i = 0;i < 2;i++) {
		var mc = i == 0 ? exportRoot.dude1_mc : exportRoot.dude2_mc;
		dudes.push({mc:mc, anims:[mc.d0, mc.d1, mc.d2, mc.d3], cx:0, cy:0, direction:0, active:false, walking:false});
	}
	
	topbar = exportRoot.topbar_mc;
	
	avatarActive = avatarType == 0 ? exportRoot.steve_mc : exportRoot.kate_mc;
	
	SetupPalletGrids();
	
	SetupGameoverButtons();
	
	// menu buttons
	topbar.menu_mc.menu_btn.on("mousedown", function(event) {
		console.log("Back to Main Menu");
		var frm = topbar.menu_mc.currentFrame;
		if (frm==0){
			topbar.menu_mc.gotoAndStop(1);
		}else{
			topbar.menu_mc.gotoAndStop(0);
		}
	});
	
	topbar.menu_mc.menu_mc.leaderboard_btn.on("mousedown", function(event){
		console.log("Show leaderboard");
		apiHelper.openLeaderBoards(GAME_ID);
	});
	
	topbar.menu_mc.menu_mc.logout_mc.gotoAndStop(0);
	topbar.menu_mc.menu_mc.logout_mc.cursor = "pointer";
	topbar.menu_mc.menu_mc.logout_mc.on("mousedown", function(event){
		console.log("Logout");
		apiHelper.logoutFromGame( loggedOut );
	});
	
	var frm = (apiHelper.isSoundMute()) ? 1 : 0;
	topbar.menu_mc.menu_mc.sound_mc.gotoAndStop(frm);
	topbar.menu_mc.menu_mc.sound_mc.cursor = "pointer";
	topbar.menu_mc.menu_mc.sound_mc.on("mousedown", function(event){
		console.log("Sound");
		var frm = (topbar.menu_mc.menu_mc.sound_mc.currentFrame==1) ? 0 : 1;
		topbar.menu_mc.menu_mc.sound_mc.gotoAndStop(frm);
		apiHelper.switchSound(frm==0);
	});
	
	topbar.menu_mc.menu_mc.fb_btn.on("mousedown", function(event){
		console.log("FB");
		apiHelper.shareFacebookViaGameMenu();
	});
	
	topbar.menu_mc.menu_mc.twitter_btn.on("mousedown", function(event){
		console.log("Twitter");
		apiHelper.shareTwitterViaGameMenu();
	});
	
	topbar.quit_btn.on("mousedown", function(event) {
		console.log("Quit Game");
		topbar.confirm_mc.visible = true;
		topbar.confirm_mc.gotoAndPlay(1);
	});
	
	topbar.confirm_mc.yes_btn.on("mousedown", function(event) {
		console.log("Quit Game yes");
		quitConfirm = true;
		topbar.confirm_mc.play();
	});
	
	topbar.confirm_mc.no_btn.on("mousedown", function(event) {
		console.log("Quit Game no");
		quitConfirm = false;
		topbar.confirm_mc.play();
	});
	
	topbar.back_btn.on("mousedown", function(event) {
		switch(gameMode){
		case "controls":
			ShowInstructions();
			break;
		case "instructions":
			SetupWorkwear();
			break;
		case "leaderboard":
			BackToGameOver();
			break;
		default:
			topbar.confirm_mc.visible = true;
			topbar.confirm_mc.play();
			break;
		}
	});
	
	topbar.next_btn.on("mousedown", function(event) {
		if (avatarIndex >= 0) {
			ShowInstructions();
		}
	});
	
	topbar.replay_btn.on("mousedown", function(event) {
		StartGame();
	});
	
	// control buttons
	exportRoot.controls_mc.turnR_btn.on("mousedown", function(event) {
		Turn(-1);
	});

	exportRoot.controls_mc.turnL_btn.on("mousedown", function(event) {
		Turn(1);
	});
	
	exportRoot.controls_mc.forward_btn.on("mousedown", function(event) {
		StartDrive(2);
	});
	
	exportRoot.controls_mc.reverse_btn.on("mousedown", function(event) {
		StartDrive(-2);
	});
	
	exportRoot.controls_mc.stop_btn.on("mousedown", function(event) {
		forklift.speed = 0;
	});
	
	exportRoot.controls_mc.lift_btn.on("mousedown", function(event) {
		StartForkMove(1);
	});
	
	exportRoot.controls_mc.lower_btn.on("mousedown", function(event) {
		StartForkMove(-1);
	});
	
	// reminder button
	reminder = exportRoot.reminder_btn;
	reminder.on("mousedown", function(event) {
		ShowReminder();
	});
	reminder.visible = false;
	
	// fade from white
	stopMC = exportRoot.fade_mc;
	createjs.Ticker.addEventListener("tick", StopLoop);
	
	document.onkeydown = keydown;
	
	setTimeout(SetupWorkwear, 100); // go to workwear selection
	//setTimeout(StartGame, 100); // go straight to game for testing
}

function keydown(e) {
	// use e.which
	//console.log("down " + e.which);
	switch(e.which){
		case 32:
		forklift.speed = 0;
		break;
		case 37://left
		Turn(1);
		break;
		case 38://up
		StartDrive(2);
		break;
		case 39://right
		Turn(-1);
		break;
		case 40://down
		StartDrive(-2);
		break;
		case 65://A
		StartForkMove(1);
		break;
		case 90://Z
		StartForkMove(-1);
		break;
	}
}

function loggedOut(){
	console.log("loggedOut");
}

var music;
var fxForklift;

function quitGame(){
	console.log("Quit game confirmed");
	apiHelper.gotoMainMenu();
}

function PlaySound(id, vol, loop) {
	if (apiHelper.isSoundMute()) return;
	var instance = createjs.Sound.play(id, createjs.Sound.INTERRUPT_EARLY, 0, 0, loop);
	instance.volume = vol;
	if (id == "music") music = instance;
	else if (id == "forklift") fxForklift = instance;
}

// update debugging text fields
function UpdateDebug(mc, txt)
{
	if (debugging) mc.text = txt;
}

// loop function to stop playhead on a stop label  
function StopLoop()
{
	//console.log(stopMC.currentLabel);
	if (stopMC.currentLabel.indexOf("stop") >= 0) {
		stopMC.stop();
		createjs.Ticker.removeEventListener("tick", StopLoop);
	}
}


// pre-game functions

function SetupWorkwear()
{
	topbar.gotoAndStop("workwear");
	topbar.btnHighlight.visible = false;
	
	exportRoot.gotoAndStop("ww" + avatarType);
	avatarActive.gotoAndStop(0);
	exportRoot.wwinfo_mc.gotoAndStop(0);
	
	gameMode = "workwear";
	avatarIndex = -1;
	
	// workwear buttons
	exportRoot.ww_btn1.on("mousedown", function(event) {
		UpdateAvatar(1);
	});
	exportRoot.ww_btn2.on("mousedown", function(event) {
		UpdateAvatar(2);
	});
	exportRoot.ww_btn3.on("mousedown", function(event) {
		UpdateAvatar(4);
	});
	exportRoot.ww_btn4.on("mousedown", function(event) {
		UpdateAvatar(3);
	});
	
	exportRoot.wwinfo_mc.gotit_btn.on("mousedown", function(event) {
		exportRoot.wwinfo_mc.gotoAndPlay("close");
		stopMC = exportRoot.wwinfo_mc;
		createjs.Ticker.addEventListener("tick", StopLoop);
		avatarActive.gotoAndStop(0);
	});
}

function UpdateAvatar(val)
{
	avatarActive.gotoAndStop(val);
	if (val == 2 || val == 4) {
		exportRoot.wwinfo_mc.gotoAndPlay("start");
		stopMC = exportRoot.wwinfo_mc;
		createjs.Ticker.addEventListener("tick", StopLoop);
		avatarIndex = -1;
		topbar.btnHighlight.visible = false;
		PlaySound("wrong", 1, 0);
	} else {
		avatarIndex = avatarId + (val == 1 ? 0 : 1);
		topbar.btnHighlight.visible = true;
		PlaySound("correct", 1, 0);
	}
}

function ShowInstructions()
{
	topbar.gotoAndStop("intro");
	topbar.btnHighlight.visible = false;
	exportRoot.gotoAndPlay("instructions");
	gameMode = "instructions";
	stopMC = exportRoot;
	createjs.Ticker.addEventListener("tick", StopLoop);
	exportRoot.instructions_mc.gotit_btn.on("mousedown", function(event) {
		ShowControls();
	});

	//Init game session
	apiHelper.init_game_session(GAME_ID, GAME_NAME); 
}

function ShowControls()
{
	exportRoot.gotoAndPlay("controls");
	gameMode = "controls";
	stopMC = exportRoot;
	createjs.Ticker.addEventListener("tick", StopLoop);
	exportRoot.controlsinfo_mc.gotit_btn.on("mousedown", function(event) {
		StartGame();
	});
}

// start the game
function StartGame()
{
	exportRoot.gotoAndStop("game");
	gameMode = "game";
	
	if (firstPlay) InitialiseGame();
	
	forklift.z = forklift.z0 = exportRoot.getChildIndex(forklift.root);
	forklift.forkZ = exportRoot.getChildIndex(forklift.forks);
	
	lives = 3;
	score = 0;
	topbar.gotoAndStop("game");
	topbar.lives_mc.gotoAndStop(0);
	topbar.score_mc.score_txt.text = GetScoreString(score);
	
	hsIndex = 0;
	hsofficer.visible = true;
	
	gameLevel = -1;	
	
	NextLevel();
	
	exportRoot.setChildIndex(topbar, topbarZ);
	
	createjs.Ticker.addEventListener("tick", GameLoop);
	
	// show pallet description info
	if (firstPlay) {
		infopanel.info_txt.text = "click the pallets for a description";
		infopanel.gotoAndStop(0);
		infopanel.gotit_btn.y = 180;
		infopanel.x = 160;
		infopanel.y = 120;
		infopanel.visible = true;
		infoStage = "prestart";
		firstPlay = false;
	}

}

// setup gui panels



function SetupPalletDescription()
{
	if (palletDescription) return;
	var c = new lib.palletdescription_mc();
	palletDescription = exportRoot.addChild(c);
	palletDescription.visible = false;
}

function ShowPalletDescription(p)
{
	palletDescription.info_txt.text = palletNames[p.currentFrame];
	palletDescription.x = p.x;
	palletDescription.y = p.y;
	palletDescription.visible = true;
}

function HidePalletDescription()
{
	palletDescription.visible = false;
}

function SetupInfoPanel()
{
	if (infopanel) return;
	var c = new lib.infopanel_mc();
	infopanel = exportRoot.addChild(c);
	infopanel.gotoAndStop(0);
	infopanel.x = 150;
	infopanel.y = 100;
	infopanel.visible = false;
	infopanel.gotit_btn.on("mousedown", function(event) {
		if (infoStage == "prestart") StartLoad();
		else if (infoStage == "startload") HideLoadInfo();
		else if (infoStage == "nextlevel") NextLevel();
		else if (infoStage == "reminder") HideReminder();
		else if (infoStage == "gameover") GotoGameOver();
		else infopanel.visible = false;
	});	
}

function ShowReminder()
{
	infoStage = "reminder";
	reminder.visible = false;
	infopanel.visible = true;
}

function HideReminder()
{
	infopanel.visible = false;
	reminder.visible = true;
	infoStage = "ingame";
}

function SetupBoss()
{
	if (boss) return;
	var c = new lib.boss_mc();
	boss = exportRoot.addChild(c);
	boss.gotoAndStop(0);
	boss.x = 75;
	boss.y = 160;
	boss.visible = false;
	boss.gotit_btn.on("mousedown", function(event) {
		CloseBoss();
	});	
}

function CloseBoss()
{
	if (lives == 0) {
		EndOfGame("lives");
	} else if (bossWarning == "forks raised") { // reset forklift
		forklift.cx = forklift.cxReset;
		forklift.cy = forklift.cyReset;
		TransformForkLift();
		StartForkMove(-1);
	} else if (bossWarning == "wrong pallet" || bossWarning == "loaded wrong") {
		ResetPallet();
	} else if (bossWarning == "pothole") {
		ResetForklift();
		BackInControl();
	}
	bossWarning = "";
	boss.visible = false;
}

function SetupHSOfficer()
{
	hsIndex = 0;
	if (hsofficer) return;
	var c = new lib.healthandsafety_mc();
	hsofficer = exportRoot.addChild(c);
	hsofficer.gotoAndStop(0);
	hsofficer.x = 835;
	hsofficer.y = 425;
	hsofficer.visible = false;
	hsofficer.ok_btn.on("mousedown", function(event) {
		CloseHSOfficer();
	});
}

function CheckHS()
{
	if (hsIndex == 2) { // load in lorry correctly - show when near lorry
		if (pallet.lifted && InLoadingArea()) ShowHSOfficer();
	}
}

function ShowHSOfficer()
{
	if (infoStage == "gameover" || infoStage == "nextlevel") return;
	
	if (bossWarning != "") {
		setTimeout(ShowHSOfficer, 3000);
		return;
	}
	forklift.underControl = false;
	forklift.speed = 0;
	
	hsofficer.info_txt.text = hsWarnings[hsIndex];
	hsofficer.gotoAndPlay("talk");
	hsActive = true;
	stopMC = hsofficer;
	createjs.Ticker.addEventListener("tick", StopLoop);
	hsIndex++;
}

function CloseHSOfficer()
{
	if (!bossWarning) BackInControl();
	hsofficer.gotoAndStop(0);
	hsActive = false;
	if (hsIndex == 1 || hsIndex == 5) setTimeout(ShowHSOfficer, 10000);
}

/////////////////////////////////// HUD ///////////////////////////////////

function LoseLife()
{
	lives--;
	topbar.lives_mc.gotoAndStop(3 - lives);
	if (lives == 0) clearInterval(timeInterval);
}

function AddScore(val)
{
	score += val;
	topbar.score_mc.score_txt.text = GetScoreString(score);
	PlaySound("correct", 0.5, 0);

	//inc score
	apiHelper.inc_game_score(GAME_ID, val);
}

function GetScoreString(s)
{
	var scoreStr = s.toString();
	for (var i = 2;i <= 6;i++) {
		if (scoreStr.length < i) scoreStr = "0" + scoreStr;
	}
	return scoreStr;
}

function StartTimer()
{
	timeInterval = setInterval(UpdateTimer, 1000);
}

function UpdateTimer()
{
	if (hsActive) return;
	timeLeft--;
	var timeAllowed = 180 + gameLevel * 60;
	topbar.timer_mc.time_txt.text = GetTimeString(timeAllowed - timeLeft);
	if (timeLeft == 0) {
		EndOfGame("time");
	}
	//console.log("update time: " + timeLeft);
}

function GetTimeString(t)
{
	var mins = Math.floor(t / 60);
	var secs = (t % 60).toString();
	if (secs.length == 1) secs = "0" + secs;
	return mins + ":" + secs;
}

// start a new level
function NextLevel()
{
	lift.autoLower = lift.auto = false;
	forklift.underControl = false;
	lift.level = 0;
	infopanel.visible = false;
	
	if (gameLevel == 2) {
		EndOfGame("win");
		return;
	}
	
	gameLevel++;
	SetupLevel();
	
	ResetForklift();

	SetLorryZ();
	
	topbar.level_mc.level_txt.text = "LEVEL " + (gameLevel + 1);
	
	palletsLoaded = 0;
	timeLeft = 180 + gameLevel * 60;
	topbar.timer_mc.time_txt.text = GetTimeString(0);
	
	if (!firstPlay || gameLevel > 0) StartLoad();
	
	PlaySound("music", 0.3, -1);
	
}

function StartLoad()
{
	forklift.underControl = false;
	forklift.speed = 0;
	
	if (palletsLoaded > 0 && palletsOrder.length == 0) {
		EndOfLevel();
		return;
	}
	var sameNames = new Array(false, false, false);
	palletsToLoad = new Array();
	var pal = -1;
	var prevPal = -1;
	for (var i = 0;i <= gameLevel;i++) {
		if (palletsOrder.length > 0) {
			var pal = palletsOrder.shift();
			palletsToLoad.push(pal);
			if (i > 0 && pal == prevPal) {
				sameNames[i - 1] = true;
			}
			prevPal = pal;
		}

	}	
	infopanel.gotoAndStop(palletsToLoad.length - 1);
	console.log("palletsToLoad: " + palletsToLoad);
	
	var txt = "load ";
	for (i = 0;i <= gameLevel;i++) {
		if (i > 0) txt += i == gameLevel ? " and " : ", ";
		if (sameNames[i]) {
			txt += "2 pallets of " + palletNames[palletsToLoad[i]];
			i++;
		} else {
			txt += "1 pallet of " + palletNames[palletsToLoad[i]];
		}
	}
	infopanel.info_txt.text = txt;
	
	infopanel.gotit_btn.y = infopanel.currentFrame == 0 ? 160 : 290;
	infopanel.x = 160;
	infopanel.y = 120;
	infopanel.visible = true;
	infoStage = "startload";
	
	for (var i = 0;i < tiles.length;i++) {
		var f = tiles[i].currentFrame;
		for (var j = 0;j < palletsToLoad.length;j++) {
			if (f == palletsToLoad[j]) HighlightPallet(tiles[i], true);
		}		
	}
	
	
}

function HideLoadInfo()
{
	for (var i = 0;i < tiles.length;i++) {
		var f = tiles[i].currentFrame;
		for (var j = 0;j < palletsToLoad.length;j++) {
			if (f == palletsToLoad[j]) HighlightPallet(tiles[i], false);
		}		
	}
	infopanel.visible = false;
	infoStage = "ingame";
	forklift.underControl = true;
	
	reminder.visible = true;
	
	if (lorry.pallets.length == 0) {
		palletTime = timeLeft;
		StartTimer();
	}
	
	if (gameLevel == 0) {
		if (lorry.pallets.length == 0) setTimeout(ShowHSOfficer, 10000); // 1st message - forks raised
		else if (hsIndex == 3) setTimeout(ShowHSOfficer, 10000); // pothole message
	} else if (gameLevel == 1) {
		if (lorry.pallets.length == 0) setTimeout(ShowHSOfficer, 10000); // 1st message - pedestrians
		else if (lorry.pallets.length == 3) setTimeout(ShowHSOfficer, 10000); // load by pushing back
	} else if (gameLevel == 2) {
		if (lorry.pallets.length == 0) setTimeout(ShowHSOfficer, 10000); // 1st message - move pallets out the way
		//else if (lorry.pallets.length == 6) setTimeout(ShowHSOfficer, 4000); // load on top of each other
	}
	
}

function EndOfLevel()
{
	clearInterval(timeInterval);
	infopanel.gotoAndStop("endoflevel");
	var txt = infopanel.endoflevel_mc.info_txt;
	if (gameLevel == 2) {
		txt.text = "well done, you've loaded the delivery truck correctly and succesfully completed the game!";
	} else {
		txt.text = "well done, you've loaded the delivery truck correctly. ";
		if (gameLevel == 0) txt.text += "there are 2 more levels to complete. are you ready?";
		else if (gameLevel == 1) txt.text += "there is 1 more level to complete. are you ready?";
	}
	infopanel.gotit_btn.y = 360;
	infopanel.x = 380;
	infopanel.y = 100;
	infopanel.visible = true;
	infoStage = "nextlevel";
	
	reminder.visible = false;
	
	if (music!=null) music.stop();
}

function EndOfGame(val)
{
	createjs.Ticker.removeEventListener("tick", GameLoop);
	clearInterval(timeInterval);
	infopanel.gotoAndStop("endoflevel");
	var txt = infopanel.endoflevel_mc.info_txt;
	if (val == "lives") {
		topbar.gameover_mc.gameover_txt.text = "OUT OF LIVES";
		txt.text = "you've run out of lives. be more careful in future!\n\nlet's find out if you've earned a badge!";
	} else if (val == "time") {
		topbar.gameover_mc.gameover_txt.text = "TIME'S UP";
		txt.text = "you've run out of time. move a little quicker next time!\n\nlet's find out if you've earned a badge!";
	} else if (val == "win") {
		topbar.gameover_mc.gameover_txt.text = "GAME COMPLETE";
		txt.text = "well done, you've successfully completed the game!\n\nlet's find out if you've earned a badge!";
	}
	infopanel.gotit_btn.y = 360;
	infopanel.x = 380;
	infopanel.y = 100;
	infopanel.visible = true;
	infoStage = "gameover";
	
	reminder.visible = false;
	
	if (fxForklift) {
		fxForklift.stop();
		fxForklift = null;
	}
	if (music!=null) music.stop();

	
	
	console.log("END OF GAME");
}

function GotoGameOver()
{
	ClearLevel();
	hsofficer.visible = false;
	infopanel.visible = false;

	//Game Over: game id, total score, callback function to handle badges
	apiHelper.game_over(GAME_ID, score, function (data) {
	    //handle badge info
	    console.log(data);
	    if (!data) {
	        //any failured, we treat as did not earn badge
	        badgeState = 0;          
	    }else if (data.status == "OK" && data.badge_info){
	        if (data.badge_info.badge_status == 1) {
	        	//This user did not earn badge
	            //gameCounter.didNotEarnBadge();          
	            badgeState = 0;          
	        }else if (data.badge_info.badge_status == 2) {
	        	//This user already earned badge
	            // gameCounter.alreadyGotBadge();
				badgeState = 1;          
	        }else if (data.badge_info.badge_status == 3) {
	        	//This user earn badge for this game first time.
	            // gameCounter.earnedBadge();          
	            badgeState = 2;          
	        }else if (data.badge_info.badge_status == 4) {
	        	//This user earned all 4 badges of 4 games
	            // gameCounter.earnedAllBadge();          
	            
	        }else{
	        	//any failured, we treat as did not earn badge
	            badgeState = 0;
	        }
	    }else{
	    	//any failured, we treat as did not earn badge
	        badgeState = 0;
	    }

	    GameOver(score);
	});  
}


// main game loop 
function GameLoop()
{
	// check if Health & Safety messages are needed
	CheckHS();
	
	// move pedestrians
	TransformDudes(false);
	
	// drive
	var prevPos = {x:forklift.cx, y:forklift.cy};
	if (forklift.speed != 0) {
		if (forklift.accel < 1) forklift.accel += 0.1;
		forklift.cx += forklift.speed * forklift.accel * directions[forklift.direction].x;
		forklift.cy += forklift.speed * forklift.accel * directions[forklift.direction].y;
		TransformForkLift();
		if (!fxForklift) PlaySound("forklift", 0.3, -1);
	} else if (fxForklift) {
		fxForklift.stop();
		fxForklift = null;
	}
	
	// lower lift automatically
	if (lift.autoLower) {
		if (forklift.direction != 3 || forklift.cx > tileSize) {
			StartForkMove(-1);
		}
	}
	
	var dat = GetGridData(forklift.cx + Math.abs(directions[forklift.direction].x) * tileSize * 0.3 * forklift.speed, forklift.cy + Math.abs(directions[forklift.direction].y) * tileSize * 0.3 * forklift.speed);
	SlideOffPallets(dat);
	
	/*
	if (!forklift.underControl) {
		forklift.speed *= 0.9;
		if (Math.abs(forklift.speed) < 0.1) forklift.speed = 0;
		return;
	}
	*/
	
	if (forklift.speed != 0) {
		if (CheckCollision()) {
			forklift.underControl = false;
			if (hitPothole) {
				forklift.cx += forklift.speed * directions[forklift.direction].x * 10;
				forklift.cy += forklift.speed * directions[forklift.direction].y * 10;
				hitPothole = false;
			} else {
				forklift.cx = prevPos.x;
				forklift.cy = prevPos.y;
				setTimeout(BackInControl, 100);
			}
			TransformForkLift();
			forklift.speed = 0; //*= -1;			
		}
	}
	
	// set z level
	if ((dat.i >= 0 && forklift.z != (tileOriginZ + dat.i)) || (dat.i < 0 && forklift.z != forklift.z0)) {
		//forklift.z = dat.i < 0 ? forklift.z0 : tileOriginZ + dat.i;
		//exportRoot.setChildIndex(forklift.root, forklift.z);
		//SetForkZ();
		//console.log(dat.i + ", " +  exportRoot.getChildIndex(forklift.root));
	}
	
	//GetGridPosition(forklift.root.x, forklift.root.y);
	
	SetAllZ();
}

function ResetForklift()
{
	forklift.root.rotation = forklift.forks.rotation = 0;
	
	if (pallet.lifted) {
		ResetPallet();
		pallet.frm = 0;
		forklift.forks.pallet_mc.gotoAndStop(0);
		pallet.mc.visible = true;
		pallet.lifted = pallet.raised = pallet.loaded = false;
	}
	
	forklift.cx = tileSize;
	forklift.cy = tileSize;
	TransformForkLift();
		
	forklift.speed = 0;
	forklift.direction = 0;
	forklift.root.gotoAndStop(0);
	forklift.truck.gotoAndStop("down");
	forklift.avatar.gotoAndStop(avatarIndex);
	forklift.forks.gotoAndStop(0);
	forklift.forks.pallet_mc.gotoAndStop(0);
	
	lift.level = 0;
	lift.dir = -1; // updated Paul 14/10 - previously lift.dir = 0;
}

function StartDrive(val)
{
	if (forklift.underControl) {
		forklift.speed = val;
		forklift.accel = 0;
	}
	if (!InLoadingArea() && lift.level > lift.pickup && !lift.auto) TriggerWarning("forks raised");
}

function BackInControl()
{
	forklift.underControl = true;
	forklift.speed = 0;
}

// turn the forklift
function Turn(val)
{
	if (!forklift.underControl) return;
	if (!InLoadingArea() && lift.level > lift.pickup && !lift.auto) TriggerWarning("forks raised");
	
	var prevFrm = forklift.root.currentFrame;
	var frm = forklift.root.currentFrame + val;
	if (frm < 0) frm = 3;
	else if (frm > 3) frm = 0;
	var updateMCs = (frm <= 1 && forklift.direction > 1) || (frm > 1 && forklift.direction <= 1); // update child MCs when changing from forward to back trucks
	
	forklift.forks.gotoAndStop(frm);
	
	var noTurn = false;
	if (forklift.cx < tileSize * 0.5 && forklift.cy > tileSize * 2 && forklift.cy < tileSize * 7 && frm == 3) noTurn = true;
	else if (pallet.lifted) noTurn = GetPalletUnderForks(false);
	if (noTurn) {
		frm = prevFrm;
		//console.log("-----------can't turn");
		forklift.forks.gotoAndStop(frm);
	}
	
	forklift.root.gotoAndStop(frm);
	forklift.forks.pallet_mc.gotoAndStop(pallet.frm);
	forklift.direction = frm;
	
	CheckEdges();
		
	if (updateMCs) {	
		if (frm <= 1) {
			forklift.truck.gotoAndStop("down");
			forklift.avatar.gotoAndStop(avatarIndex);
			forklift.forksAnim = exportRoot.forks_mc.forks_mc;
		} else {
			forklift.truckB.gotoAndStop("down");
			forklift.avatarB.gotoAndStop(avatarIndex);
			forklift.forksAnim = exportRoot.forks_mc.forksB_mc;
		}
		SetTruckZ(true);
	}
	
	if (lift.level != 0) {
		var dy = lift.level * lift.dy;
		if (!noTurn) {
			if (frm <= 1) forklift.forksBase.y -= dy;
			else forklift.forksBaseB.y -= dy;
		}		
		forklift.forksAnim.y -= dy;
		forklift.forks.pallet_mc.y -= dy;
	}
	
	var hitdude = CheckCollision();
}

function TransformForkLift()
{
	CheckEdges();
	var pt = CartesianToIso(forklift.cx, forklift.cy);
	forklift.root.x = forklift.forks.x = pt.x;
	forklift.root.y = forklift.forks.y = pt.y;
	
	SetTruckZ(false);
	if (forklift.cx < tileSize * 0.3 && forklift.cy <= tileSize * 2 && forklift.direction == 3) SetForkZ();

	UpdateDebug(exportRoot.debuginfo_txt, "cx:" + forklift.cx + ", cy:" + forklift.cy);
	
}

function SetAllZ()
{
	// get truck z
	var dat = GetGridData(forklift.cx + Math.abs(directions[forklift.direction].x) * tileSize * 0.0 * forklift.speed, forklift.cy + Math.abs(directions[forklift.direction].y) * tileSize * 0.0 * forklift.speed);
	forklift.z = dat.i + tileOriginZ;
	// get forks z
	dat = GetGridData(forklift.cx + directions[forklift.direction].x * tileSize, forklift.cy + directions[forklift.direction].y * tileSize);
	//console.log(dat.i + ", " + dat.x + ", " + dat.y);
	forklift.forkZ = dat.i + tileOriginZ;
	var fz;
	if (lift.autoLower) fz = tileInLorryZ;
	else if (pallet.raised && forklift.direction == 3 && dat.x <= 0) fz = tileInLorryZ + dat.y * 2 - 1; // sets forks + pallet when loading onto lorry
	else if (forklift.cx < tileSize * 0.2 && forklift.cy <= tileSize * 2 && forklift.direction == 3) fz = exportRoot.getChildIndex(lorry.mc) - 2; // set behind front of lorry
	else if (pallet.lifted) fz = dat.i + tileOriginZ; // set to correct level when carrying pallet
	else if (forklift.forkZ > forklift.z) fz = forklift.z; // above forklift
	else if (forklift.forkZ < forklift.z) fz = forklift.forkZ - 2; // behind forklift
	// go thru and set pallet Zs
	for (var i = tileInLorryZ;i < tilesZs.length;i++) {
		if (tilesZs[i] != null) {
			if (tilesZs[i].currentFrame > 0) {
				exportRoot.setChildIndex(tilesZs[i], i);
				UpdateDebug(tilesZs[i].debug_txt, i);
			}
		}
		if (i == forklift.z) exportRoot.setChildIndex(forklift.root, forklift.z);
		if (i == fz) exportRoot.setChildIndex(forklift.forks, fz);
		if (i == lorryRoofZ) exportRoot.setChildIndex(lorry.roof, lorryRoofZ);
	}
	
	exportRoot.setChildIndex(reminder, lorryRoofZ + 2);
	
	UpdateDebug(forklift.root.debug_txt, exportRoot.getChildIndex(forklift.root));
	UpdateDebug(forklift.root.debugF_txt, exportRoot.getChildIndex(forklift.forks));
}

function SetTruckZ(forced)
{
	return;
	//ptHit = forklift.root.hit_mc;
	//if (forklift.speed < 0) ptHit = forklift.root.hitR_mc;
	//var pt = forklift.root.localToGlobal(ptHit.x, ptHit.y);
	//pt = IsoToCartesian(pt.x, pt.y);
	//var dat = GetGridData(pt.x, pt.y);
	var dat = GetGridData(forklift.cx + Math.abs(directions[forklift.direction].x) * tileSize * 0.0 * forklift.speed, forklift.cy + Math.abs(directions[forklift.direction].y) * tileSize * 0.0 * forklift.speed);
	//console.log(dat.i + ", " + dat.x + ", " + dat.y);
	if (forced || forklift.z != dat.i + tileOriginZ) {
		forklift.z = dat.i + tileOriginZ;
		exportRoot.setChildIndex(forklift.root, forklift.z);
		UpdateDebug(forklift.root.debug_txt, exportRoot.getChildIndex(forklift.root));
		SetSurroundingPalletsZ(forklift.z);
		SetForkZ();
	}	
}

function SetForkZ()
{
	return;
	//console.log("forklift.z: " + forklift.z);
	var dat = GetGridData(forklift.cx + directions[forklift.direction].x * tileSize, forklift.cy + directions[forklift.direction].y * tileSize);
	//console.log(dat.i + ", " + dat.x + ", " + dat.y);
	forklift.forkZ = dat.i + tileOriginZ;
	var fz;
	if (pallet.raised && forklift.direction == 3 && dat.x <= 0) fz = tileInLorryZ + dat.y - 1;
	else if (forklift.cx < tileSize * 0.2 && forklift.cy <= tileSize * 2 && forklift.direction == 3) fz = exportRoot.getChildIndex(lorry.mc) - 2;
	else if (pallet.lifted) fz = dat.i + tileOriginZ;
	else if (forklift.forkZ > forklift.z) fz = forklift.z;
	else if (forklift.forkZ < forklift.z) fz = forklift.forkZ - 2;
	exportRoot.setChildIndex(forklift.forks, fz);

	UpdateDebug(forklift.root.debugF_txt, exportRoot.getChildIndex(forklift.forks));
	SetSurroundingPalletsZ(fz);
}

function SetSurroundingPalletsZ(i)
{
	return;
	var below = i + 1;
	if (below >= tileOriginZ && below < tilesZs.length) {
		if (tilesZs[below] != null) exportRoot.setChildIndex(tilesZs[below], below);
	}
}

function SetLorryZ()
{
	return;
	exportRoot.setChildIndex(exportRoot.lorryRoof_mc, tileInLorryZ + 12);
	exportRoot.setChildIndex(reminder, tileInLorryZ + 13); // set reminder button z level above lorry roof
	
	console.log("lorry roof: " + (tileInLorryZ + 12) + ", topbar: " + exportRoot.getChildIndex(topbar));
	//exportRoot.setChildIndex(topbar, exportRoot.getChildIndex(topbar)); // set topbar z level above pallets
}

function CheckEdges()
{
	var px = forklift.cx;
	var py = forklift.cy;
	if (forklift.cx < 0) forklift.cx = 0;
	else if (forklift.cx > tileSize * (gridSize.cols - 1)) forklift.cx = tileSize * (gridSize.cols - 1);
	if (forklift.cy < 0) forklift.cy = 0;
	else if (forklift.cy > tileSize * (gridSize.rows - 1)) forklift.cy = tileSize * (gridSize.rows - 1);
	if (forklift.cy == tileSize * (gridSize.rows - 1) && forklift.cx > tileSize * 4) forklift.cx = tileSize * 4; // limit distance on bottom row
	if (forklift.cx != px || forklift.cy != py) forklift.speed = 0;
}

function SlideOffPallets(dat)
{
	if (forklift.direction == 0 || forklift.direction == 2) {
		if (Math.abs(dat.xo) > 0 && GetPallet(dat.y, dat.x + (dat.xo > 0 ? 1 : -1)) > 0) {
			forklift.cx -= dat.xo * 0.3;
		}
	} else {
		if (Math.abs(dat.yo) > 0 && GetPallet(dat.y + (dat.yo > 0 ? 1 : -1), dat.x) > 0) {
			forklift.cy -= dat.yo * 0.3;
		}		
	}	
}

function CheckCollision()
{
	// check lorry collsion
	var ptHit = pallet.raised ? forklift.root.hit_mc : forklift.root.hitL_mc;
	var pt = forklift.root.localToLocal(ptHit.x, ptHit.y, lorry.hit);
	var hit = lorry.hit.hitTest(pt.x, pt.y);
	if (hit && ((forklift.direction == 3 && forklift.speed > 0) || (forklift.direction == 1 && forklift.speed < 0))) return true;
	// check pothole
	ptHit = forklift.speed < 0 ? forklift.root.hitR_mc : forklift.root.hit_mc;
	pt = forklift.root.localToLocal(ptHit.x, ptHit.y, pothole);
	hit = pothole.hitTest(pt.x, pt.y);
	if (hit) {
		HitPothole();
		return true;
	}
	// check pedestrians
	ptHit = forklift.speed < 0 ? forklift.root.hitR_mc : forklift.root.hitL_mc;
	for (var i = 0;i < dudes.length;i++) {
		if (dudes[i].active) {
			pt = forklift.root.localToLocal(ptHit.x, ptHit.y, dudes[i].mc.hitarea_mc);
			hit = dudes[i].mc.hitarea_mc.hitTest(pt.x, pt.y);
			if (hit) {
				//console.log("hit dude");
				PlaySound("hitdude", 1, 0);
				TriggerWarning("dude");
				return true;
			}
		}

	}	
	// check pallets in lorry
	if (pallet.raised && forklift.cx < tileSize && forklift.cy > tileSize * 3.8 && forklift.cy < tileSize * 6.2 && forklift.direction == 3) {
		var blocked = MovePalletsInLorry();
		if (blocked) return true;
	}
	// check pallets collision
	ptHit = forklift.root.hit_mc;
	if (forklift.speed < 0) ptHit = forklift.root.hitR_mc;
	else if (pallet.lifted) ptHit = forklift.root.hitL_mc;
	for (i = 0;i < tiles.length;i++){
		if (tiles[i].visible && tiles[i].currentFrame > 0) {
			pt = forklift.root.localToLocal(ptHit.x, ptHit.y, tiles[i].hit_mc);
			hit = tiles[i].hit_mc.hitTest(pt.x, pt.y);
			if (hit) {
				//console.log("hit pallet: " + i);
				return true;
			}
		}

	}
	return false;
}

function HitPothole()
{
	var rot = 15;
	if ( (forklift.speed > 0 && (forklift.direction == 0 || forklift.direction == 3)) || (forklift.speed < 0 && (forklift.direction == 1 || forklift.direction == 2)) ) rot *= -1;
	forklift.root.rotation = forklift.forks.rotation = rot;
	hitPothole = true;
	TriggerWarning("pothole");
	PlaySound("pothole", 1, 0);
}

function InLoadingArea()
{
	var pt = forklift.root.localToLocal(forklift.root.hit_mc.x, forklift.root.hit_mc.y, lorry.loadarea);
	return lorry.loadarea.hitTest(pt.x, pt.y);
}

function CartesianToIso(x, y)
{
	var tempPt = {x:gridOrigin.x, y:gridOrigin.y};
	tempPt.x += x - y;
	tempPt.y += (x + y) / 1.75;
	//console.log(tempPt.x + ", " + tempPt.y);
	return tempPt;
	
}

function GetGridData(x, y)
{
	var data = {x:0, y:0, i:0, xo:0, yo:0};
	data.x = Math.round(x / tileSize);
	data.y = Math.round(y / tileSize);
	data.xo = x - data.x * tileSize;
	data.yo = y - data.y * tileSize;
	data.i = data.x * gridSize.rows + data.y;
	if (data.i < 0) data.i = 0;
	else if (data.i >= gridSize.rows * gridSize.cols) data.i = gridSize.rows * gridSize.cols - 1;
	//console.log(data.xo);
	return data;
}

function IsoToCartesian(x, y)
{
	x -= gridOrigin.x;
	y -= gridOrigin.y;
	
	
	
	var tempPt = {x:0, y:0};
	tempPt.x = (2 * y + x) / 2;
	tempPt.y = (2 * y - x) / 2;
	//console.log(tempPt.x + ", " + tempPt.y);
	return tempPt;
}

// start moving forks
function StartForkMove(val)
{
	// automatically lower forks
	if (lift.autoLower) {
		lift.dir = val;
		lift.target = 0;
		lift.autoLower = false;
		createjs.Ticker.addEventListener("tick", ForkLoop);
		PlaySound("lowerforks", 1, 0);
		return;
	}
	
	// warn if raising forks when moving
	if (!InLoadingArea() && forklift.speed != 0) TriggerWarning("forks raised");
	
	// lifting a pallet in the air
	if (pallet.lifted && !pallet.raised && val == 1) { // lifting a pallet in the air
		pallet.raised = true;
		lift.dir = val;
		lift.target = lift.raised;
		SetForkZ();
		createjs.Ticker.addEventListener("tick", ForkLoop);
		PlaySound("raiseforks", 1, 0);
		return;
	}
	
	// dropping pallet in truck
	if (forklift.direction == 3 && pallet.raised && val == -1) { // updated Paul 14/10 - removed forklift.cx < 10
		// added Paul 14/10 - check if pallet is lowered on edge of truck - warn and lose life
		if (forklift.cx < 52 && forklift.cx >= 10 && forklift.cy > tileSize * 3.8 && forklift.cy < tileSize * 6.2) {
			TriggerWarning("loaded outside");
			return;
		}
		// end of addition
		pt = forklift.root.localToLocal(forklift.root.hitR_mc.x, forklift.root.hitR_mc.y, lorry.loadarea);
		hit = lorry.loadarea.hitTest(pt.x, pt.y);
		if (hit) {
			/*
			if (lorry.pallets.length > 0) {
				for (var i = 0;i < lorry.pallets.length;i++) {
					if (lorry.pallets[i].cx == 0) {
						var dy = Math.abs(forklift.cy - lorry.pallets[i].cy);
						//console.log("dy: " + dy);
						if (dy < tileSize * 0.9) {
							Warning("can't fit pallet into truck");
							return;
						}	
					}
	
				}
			}
			*/
			pallet.loaded = true;
			lift.target = lift.lorryBase;
			lift.dir = val;
			createjs.Ticker.addEventListener("tick", ForkLoop);
			PlaySound("lowerforks", 1, 0);
			return;
		}
	}
	
	// raise & lower forks
	if (lift.dir != val) {
		var palletBeneath = GetPalletUnderForks(true);
		if (bossWarning == "tip pallet") return;
		lift.target = (val == 1) ? lift.raised : 0;
		if (val == 1) lift.target = (palletBeneath) ? lift.pickup : lift.raised;
		// else if (pallet.lifted && !pallet.raised) lift.target = lift.pickup;
		lift.dir = val;
		if (palletBeneath && val == 1) SetForkZ();
		createjs.Ticker.addEventListener("tick", ForkLoop);
		PlaySound(val == 1 ? "raiseforks" : "lowerforks", 1, 0);
	}
}

function GetPalletUnderForks(lifting)
{
	if (lifting && pallet.lifted) return false;
	var ptHit = {x:forklift.forks.hit_mc.x, y:forklift.forks.hit_mc.y};
	for (var i = 0;i < tiles.length;i++){
		if (tiles[i].visible && tiles[i].currentFrame > 0) {
			var pt = forklift.forks.localToLocal(ptHit.x, ptHit.y, tiles[i].hit_mc);
			var hit = tiles[i].hit_mc.hitTest(pt.x, pt.y);
			if (hit) {
				console.log("pallet under forks: " + i);
				if (!lifting) { // checking if pallet at forks when loaded
					return true;
				}
				if (tiles[i].currentFrame >= 13) {
					TriggerWarning("too heavy");
					return false;
				}
				var pt2 = CartesianToIso(forklift.cx + directions[forklift.direction].x * tileSize, forklift.cy + directions[forklift.direction].y * tileSize);
				pt2.x = Math.abs(pt2.x - tiles[i].x);
				pt2.y = Math.abs(pt2.y - tiles[i].y);
				if (pt2.x > 15 || pt2.y > 15) {
					TriggerWarning("tip pallet");
					return false;
				}
				PickupPallet(i);				
				return true;
			}
		}
	}
	if (lifting) pallet.lifted = false;
	return false;
}

function PickupPallet(i)
{
	UpdateGrid(forklift.cx + directions[forklift.direction].x * tileSize, forklift.cy + directions[forklift.direction].y * tileSize, 0);
	pallet.lifted = true;
	pallet.mc = tiles[i];
	pallet.frm = tiles[i].currentFrame;
	pallet.xReset = tiles[i].x;
	pallet.yReset = tiles[i].y;
	pallet.zReset = exportRoot.getChildIndex(tiles[i]);
	forklift.forks.pallet_mc.gotoAndStop(pallet.frm);
	HighlightPallet(forklift.forks.pallet_mc, false);
	pallet.mc.visible = false;
	tilesZs[i + tileOriginZ] = null;
}

// fork loop to control MC playhead
function ForkLoop()
{
	if (forklift.root.currentFrame <= 1) forklift.forksBase.y -= lift.dy * lift.dir;
	else forklift.forksBaseB.y -= lift.dy * lift.dir;
	forklift.forksAnim.y -= lift.dy * lift.dir;
	if (pallet.lifted) forklift.forks.pallet_mc.y -= lift.dy * lift.dir;
	if (lift.level == lift.target) {
		if (pallet.lifted && lift.dir == -1) DropPallet();
		if (lift.auto && lift.target == 0) {
			lift.auto = false;
			if (pallet.zInLorry > 0) UpdatePalletZ();
		}
		createjs.Ticker.removeEventListener("tick", ForkLoop);
	}else{
		lift.level += lift.dir;
	}
}

function DropPallet()
{
	palletLoadedCorrectly = false;
	if (pallet.loaded) LoadPalletOnLorry();
	else UpdateGrid(forklift.cx + directions[forklift.direction].x * tileSize, forklift.cy + directions[forklift.direction].y * tileSize, pallet.frm);
	var pt = CartesianToIso(forklift.cx + directions[forklift.direction].x * tileSize, forklift.cy + directions[forklift.direction].y * tileSize);
	if (!palletLoadedCorrectly) {
		pallet.mc.x = pt.x;
		pallet.mc.y = pt.y - (pallet.loaded ? lift.dy * lift.level : 0);
	}
	pallet.frm = 0;
	forklift.forks.pallet_mc.gotoAndStop(0);
	pallet.mc.visible = true;
	UpdatePalletZ();
	pallet.lifted = pallet.raised = pallet.loaded = false;
}

function LoadPalletOnLorry()
{
	
	
	// check pallet is correct one
	var palletsFound = new Array();
	for (var j = 0;j < palletsToLoad.length;j++) {
		if (pallet.mc.currentFrame == palletsToLoad[j] || (pallet.mc.currentFrame == 11 && palletsToLoad[j] == 3) || (pallet.mc.currentFrame == 12 && palletsToLoad[j] == 2)) {
			palletsFound.push(j);
			if (pallet.mc.currentFrame == palletsToLoad[j]) break; // don't check anymore if not picked up a double pallet
		}
	}	
	
	if (palletsFound.length == 0) {
		TriggerWarning("wrong pallet");
	} else {
		// check pallet is loaded correctly
		var loadY = -1;
		for (var i = 4;i <= 6;i++){
			var dy = Math.abs(forklift.cy - i * tileSize);
			//console.log("load dy: " + dy);
			if (dy < tileSize * 0.2) {
				loadY = i * tileSize;
				break;
			}
		}
		if (loadY > 0) { // pallet loaded correctly
			// check if no pallet is behind so it can be pushed back later
			var canMove = true;
			if (lorry.pallets.length > 0) {
				for (var i = 0;i < lorry.pallets.length;i++) {
					if (lorry.pallets[i].cx == 1 && lorry.pallets[i].cy == loadY) canMove = false;
				}
			}
			lorry.pallets.push({mc:pallet.mc, cx:0, cy:loadY, canMove:canMove});
			// add to score
			var sc = (100 - palletTime + timeLeft) * 100;
			if (sc < 100) sc = 100;
			AddScore(sc * palletsFound.length);
			palletsLoaded += palletsFound.length;
			palletTime = timeLeft;
			palletsToLoad.splice(palletsFound[0], palletsFound.length); // remove pallets found from pallets left to load
			if (palletsToLoad.length == 0) setTimeout(StartLoad, 500); // if no pallets left then show next pallets to load
			// snap pallet to correct position in lorry
			var pt = CartesianToIso(-tileSize, loadY);
			pallet.mc.x = pt.x;
			pallet.mc.y = pt.y - lift.dy * lift.level;
			palletLoadedCorrectly = true;
		} else { // pallet not laoded in line
			TriggerWarning("loaded wrong");
		}
	}
	
	// set z level
	pallet.zInLorry = Math.round(forklift.cy / tileSize) * 2 - 2 + tileInLorryZ;

	console.log("palletZ in lorry: " + pallet.zInLorry);
	console.log("palletsFound: " + palletsFound + ", palletsLeft:" + palletsToLoad);
	lift.autoLower = lift.auto = true;
	
}

function MovePalletsInLorry()
{
	if (lorry.pallets.length == 0) return false;
	var f = 1.2;
	for (var i = 0;i < lorry.pallets.length;i++) {
		var dy = Math.abs(lorry.pallets[i].cy - forklift.cy) / tileSize;
		//console.log("pallet in lorry: " + dy);
		if (lorry.pallets[i].cx == 0 && dy < 0.8) {
			if (!lorry.pallets[i].canMove || dy > 0.4) return true;
			lorry.pallets[i].mc.x -= forklift.speed * f;
			lorry.pallets[i].mc.y -= forklift.speed * f / 1.75;
			var pt = IsoToCartesian(lorry.pallets[i].mc.x, lorry.pallets[i].mc.y);
			if (pt.x < -120) { // reached the back
				lorry.pallets[i].mc.x -= forklift.speed * 2;
				lorry.pallets[i].mc.y -= forklift.speed * 2 / 1.75;
				lorry.pallets[i].cx = 1;
				lorry.pallets[i].canMove = false;
				var pz = exportRoot.getChildIndex(lorry.pallets[i].mc) - 6;
				//exportRoot.setChildIndex(lorry.pallets[i].mc, pz);
				tilesZs[pz] = lorry.pallets[i].mc;
				//UpdateDebug(lorry.pallets[i].mc.debug_txt, exportRoot.getChildIndex(lorry.pallets[i].mc));
			}
			//console.log("moving pallet x: " + pt.x);
		}	
	}
	return false;
}

function ResetPallet()
{
	pallet.mc.x = pallet.xReset;
	pallet.mc.y = pallet.yReset;
	//exportRoot.setChildIndex(pallet.mc, pallet.zReset);
	tilesZs[pallet.zReset] = pallet.mc;
	UpdateDebug(pallet.mc.debug_txt, exportRoot.getChildIndex(pallet.mc));
	pallet.zInLorry = 0;
}

function UpdatePalletZ()
{
	var dat = GetGridData(forklift.cx + directions[forklift.direction].x * tileSize, forklift.cy + directions[forklift.direction].y * tileSize);
	//console.log("update pallet z: " + dat.i + ", " + dat.x + ", " + dat.y);
	/*
	if (pallet.loaded) { // just loaded
		exportRoot.setChildIndex(pallet.mc, exportRoot.getChildIndex(forklift.forks) + 2);
		pallet.zInLorry = tileInLorryZ + dat.y - 1;
	} else
	*/
	if (pallet.zInLorry > 0) { // loaded in lorry
		//exportRoot.setChildIndex(pallet.mc, pallet.zInLorry);
		tilesZs[pallet.zInLorry] = pallet.mc;
		console.log("pallet in lorry: " + pallet.zInLorry);
		//UpdateDebug(pallet.mc.debug_txt, pallet.zInLorry + "x");
		pallet.zInLorry = 0;
		
	} else {
		//exportRoot.setChildIndex(pallet.mc, dat.i + tileOriginZ);
		var dat = GetGridData(forklift.cx + directions[forklift.direction].x * tileSize, forklift.cy + directions[forklift.direction].y * tileSize);
		tilesZs[dat.i + tileOriginZ] = pallet.mc;
		//UpdateDebug(pallet.mc.debug_txt, dat.i + tileOriginZ + "x");
	}
	/*
	for (var i = 0;i < tilesZs.length;i++) {
		console.log(i + " - " + tilesZs[i]);
	}
	*/
	
	
	SetLorryZ();
	
}

function UpdateGrid(x, y, i)
{
	var col = Math.round(x / tileSize);
	var row = Math.round(y / tileSize);
	if (col >= 0 && col < gridSize.cols && row >= 0 && row < gridSize.rows) {
		//console.log("update grid: " + row + ", " + col + " - " + i)
		grids[gridIndex][row][col] = i;
	}	
}

///////////////////// PEDESTRIANS ///////////////////////////

function SetupDude(i, col, row)
{
	var d = dudes[i];
	d.cx = col * tileSize;
	d.cy = row * tileSize;
	d.active = true;
	d.direction = d.cy < (gridSize.cols * tileSize * 0.5) ? 0 : 2;
	d.mc.gotoAndStop(d.direction);
	if (d.mc.currentFrame <= 1) d.mc.anim.gotoAndStop(0);
	else d.mc.animB.gotoAndStop(0);
	TransformDudes(true);
	if (i == 1) setTimeout(DudeStartWalk, 5000 + Math.random() * 3000, d);
}

function TransformDudes(update)
{
	for (var i = 0;i < 2;i++) {
		var d = dudes[i];
		if (d.active) {
			if (d.walking) {
				d.cx += directions[d.direction].x * 0.5;
				d.cy += directions[d.direction].y * 0.5;
				// check collsion
				if (GetDudeCollision(d)) {
					//console.log("dude collision");
					d.cx -= directions[d.direction].x * 1.5;
					d.cy -= directions[d.direction].y * 1.5;
					DudeStopWalk(d);
				}				
				update = true;
			}
			if (update) {
				var pt = CartesianToIso(d.cx, d.cy);
				d.mc.x = pt.x;
				d.mc.y = pt.y;
				var dat = GetGridData(d.cx, d.cy);
				exportRoot.setChildIndex(d.mc, tileOriginZ + dat.i);
			}
			
		}
	}
}

function DudeStartWalk(d)
{
	//var dat = GetGridData(d.cx, d.cy);
	var dir = Math.floor(Math.random() * 4);
	//console.log("start walking: " + dir);
	d.direction = dir;
	d.mc.gotoAndStop(d.direction);
	if (d.mc.currentFrame <= 1) d.mc.anim.gotoAndPlay(1);
	else d.mc.animB.gotoAndPlay(1);
	d.walking = true;
	
}

function DudeStopWalk(d)
{
	d.walking = false;
	if (d.mc.currentFrame <= 1) d.mc.anim.stop();
	else d.mc.animB.stop();
	setTimeout(DudeStartWalk, 3000 + Math.random() * 3000, d);
	//console.log("stop walking");
}

function GetDudeCollision(d)
{
	// check screen edges
	var prevDir = d.direction;
	if (d.cx < 60) d.direction = 1;
	else if (d.cx > 320) d.direction = 3;
	else if (d.cy < 10) d.direction = 0;
	else if (d.cy > 480) d.direction = 2;
	if (d.direction != prevDir) {
		//console.log("turn around: " + d.cx + ", " + d.cy)
		d.mc.gotoAndStop(d.direction);
		setTimeout(DudeStopWalk, 500, d);
	}
	// check proximity to forklift
	if (Math.abs(d.cx - forklift.cx) < 150 && Math.abs(d.cy - forklift.cy) < 150) {
		if ((d.cx > forklift.cx && d.direction == 3) || (d.cx < forklift.cx && d.direction == 1)) return true;
		if ((d.cy > forklift.cy && d.direction == 2) || (d.cy < forklift.cy && d.direction == 0)) return true;
	}
	// check pothole
	var ptHit = d.mc.hit_mc;
	var pt = d.mc.localToLocal(ptHit.x, ptHit.y, pothole);
	hit = pothole.hitTest(pt.x, pt.y);
	if (hit) return true;
	// check tiles collsions	
	for (var i = 0;i < tiles.length;i++){
		if (tiles[i].visible && tiles[i].currentFrame > 0) {
			pt = d.mc.localToLocal(ptHit.x, ptHit.y, tiles[i].hit_mc);
			hit = tiles[i].hit_mc.hitTest(pt.x, pt.y);
			if (hit) {
				//console.log("hit pallet: " + i);
				return true;
			}
		}

	}
	return false;
}

///////////////////////////////// GUI ///////////////////////////////////////////

function TriggerWarning(val)
{
	bossWarning = val;
	forklift.cxReset = forklift.cx;
	forklift.cyReset = forklift.cy;
	var t = val == "tip pallet" || val == "dude" ? 100 : 500;
	setTimeout(Warning, t, val);
}

function Warning(val)
{
	console.log("!!!WARNING: " + val);
	if (hsActive) CloseHSOfficer(); // close health & safety message when opening boss message
	forklift.speed = 0;
	var loseLife = false;
	var txt = "blank";
	if (val == "forks raised") txt = "Never travel or turn with your forks raised.\n\nLose a life!";
	else if (val == "tip pallet") txt = "Load lifted unevenly. Ensure you load the pallet centrally on the forklift.\n\nLose a life!";
	else if (val == "too heavy") txt = "That load is too heavy. Only load single pallets or two half pallets at a time.\n\nLose a life!";
	else if (val == "wrong pallet") txt = "Loaded wrong type of pallet. Check the reminder above and load the pallets requested.\n\nLose a life!";
	else if (val == "loaded wrong") txt = "You must load the pallets inside the guide marks in the truck.\n\nLose a life!";
	else if (val == "loaded outside") txt = "You must load the pallets fully inside the truck.\n\nLose a life!"; // added Paul 14/10
	else if (val == "pothole") txt = "You must avoid potholes or you will topple over.\n\nLose a life!";
	else if (val == "dude") txt = "You've almost hit someone - stay at least 2m away from all pedestrians.\n\nLose a life!";
	LoseLife();
	stopMC = boss;
	createjs.Ticker.addEventListener("tick", StopLoop);
	boss.info_txt.text = txt;
	boss.gotoAndPlay(1);
	boss.visible = true;
	PlaySound("wrong", 1, 0);
}

/////////////////////////////////////// LEVEL SETUP //////////////////////////////////

function InitialiseGame()
{
	var i;
	var j;
	tilesZs = new Array();
	// create blank slots for pallets in lorry plus lorry roof
	for (i = 0;i < 27;i++) {
		var c = new lib.blank_mc();
		var p = exportRoot.addChild(c);
		p.visible = false;
		if (i == 0) tileInLorryZ = exportRoot.getChildIndex(p);
		tilesZs.push(null);
	}
	for (i = 0;i < tileInLorryZ;i++) tilesZs.unshift(null);
	
	lorryRoofZ = tileInLorryZ + 24;
	
	UpdateDebug(lorry.mc.debug_txt, tileInLorryZ);
	UpdateDebug(lorry.roof.debug_txt, lorryRoofZ);
	
	exportRoot.setChildIndex(reminder, lorryRoofZ + 2);
	
	// populate tiles array
	tileOriginZ = -1;
	tiles = new Array();	
	for (i = 0;i < gridSize.cols;i++) { // column
		for (j = 0;j < gridSize.rows;j++) { // row
			c = new lib.pallette_mc();
			p = exportRoot.addChild(c); 
			var pt = CartesianToIso(i * tileSize, j * tileSize);
			p.x = pt.x; 
			p.y = pt.y; 
			p.gotoAndStop(0);
			p.btn1.on("mousedown", function(event) {
				ShowPalletDescription(this.parent);
			});
			p.btn1.on("mouseout", function(event) {
				HidePalletDescription();
			});
			tiles.push(p);
			tilesZs.push(p);
			var zi = exportRoot.getChildIndex(p);
			UpdateDebug(p.debug_txt, zi);
			if (tileOriginZ < 0) tileOriginZ = exportRoot.getChildIndex(p);
		}
	}
	
	topbarZ = zi;
	exportRoot.setChildIndex(topbar, topbarZ); // set topbar z level above pallets
	
	SetupPalletDescription();
	SetupHSOfficer();
	SetupInfoPanel();
	SetupBoss();

}

function ClearLevel()
{
	for (var i = 0;i < tiles.length;i++) tiles[i].gotoAndStop(0);
	dudes[0].x = dudes[1].x = pothole.x = -1000;
}

function SetupLevel()
{
	SetupPalletOrders();
	
	lorry.pallets = new Array();
	
	gridIndex = gameLevel * 3 + Math.floor(Math.random() * 3);
	
	var i;
	var j;
	
	// populate grid with pallet indices
	var orderCount = 0;
	for (i = 2;i < gridSize.cols;i++) { // column
		for (j = 1;j < gridSize.rows - 1;j++) { // row
			var pal = GetPallet(j, i);
			if (pal == 2) {
				grids[gridIndex][j][i] = palletsOrder[orderCount];
				orderCount++;
			} else if (pal == 1) {
				grids[gridIndex][j][i] = 2 + Math.round(Math.random() * 8);
			} else if (pal >= 11) {
				grids[gridIndex][j][i] = pal;
			}
		}
	}
	
	var dudeIndex = 0;
	dudes[0].active = dudes[1].active = false;
	var k = 0;
	for (i = 0;i < gridSize.cols;i++) { // column
		for (j = 0;j < gridSize.rows;j++) { // row
			pal = GetPallet(j, i);
			var p = tiles[k];
			exportRoot.setChildIndex(p, tileOriginZ + k);
			tilesZs[tileOriginZ + k] = p;
			var pt = CartesianToIso(i * tileSize, j * tileSize);
			p.x = pt.x; 
			p.y = pt.y;
			if (pal == -1) {
				pothole.x = p.x;
				pothole.y = p.y;
				p.gotoAndStop(0);
			} else if (pal == -2) {
				SetupDude(dudeIndex, i, j);
				dudeIndex++;
			} else {
				p.gotoAndStop(pal);
				HighlightPallet(p, false);
			}
			k++;
		}
	}
}

function GetPallet(row, col)
{
	if (col < 0 || col >= gridSize.cols || row < 0 || row >= gridSize.rows) return 0;
	return grids[gridIndex][row][col];
}

function HighlightPallet(p, val)
{
	if (p.currentFrame < 5) p.highlight_mc.visible = val;
	else p.highlightB_mc.visible = val;
}

function SetupPalletOrders()
{
	var orders = new Array();
	// level 1
	orders.push([5,7,10]);
	orders.push([2,6,4]);
	orders.push([3,8,9]);
	orders.push([4,5,6]);
	orders.push([7,8,9]);
	// level 2
	orders.push([6,6,2,2]);
	orders.push([3,3,10,5]);
	orders.push([2,5,8,7]);
	orders.push([6,9,10,10]);
	orders.push([10,8,4,4]);
	// level 3
	orders.push([8,7,7,2,3,6]);
	orders.push([3,5,2,10,10,4]);
	orders.push([2,2,4,6,7,8]);
	orders.push([6,6,10,8,3,3]);
	orders.push([10,4,5,7,7,6]);
	
	var r = gameLevel * 5 + Math.floor(Math.random() * 5);
	palletsOrder = orders[r];
	//palletsOrder = [6,2,2,10,10,7];
	
}

function SetupPalletGrids()
{
	grids = new Array();
	
	// level 1
	
	var grid = new Array();
	grid.push([0,0,1,1,0]);
	grid.push([0,2,1,1,0]);
	grid.push([0,0,0,2,0]);
	grid.push([0,0,0,0,0]);
	grid.push([0,0,0,-1,0]);
	grid.push([0,0,2,1,0]);
	grid.push([0,1,12,0,0]);
	grid.push([0,0,-2,0,0]);
	grids.push(PadGrid(grid));
	
	var grid = new Array();
	grid.push([0,1,1,0,0]);
	grid.push([0,0,1,1,1]);
	grid.push([0,0,0,2,1]);
	grid.push([0,0,-1,0,2]);
	grid.push([0,0,0,0,0]);
	grid.push([0,2,0,0,-2]);
	grid.push([0,1,1,1,0]);
	grid.push([1,1,11,0,0]);
	grids.push(PadGrid(grid));
	
	var grid = new Array();
	grid.push([0,1,1,0,1]);
	grid.push([0,1,0,0,2]);
	grid.push([0,0,2,0,1]);
	grid.push([0,-1,0,1,1]);
	grid.push([0,0,0,2,0]);
	grid.push([0,1,0,0,1]);
	grid.push([1,12,1,-2,0]);
	grid.push([0,0,1,0,0]);
	grids.push(PadGrid(grid));
	
	// level 2
	
	grid = new Array();
	grid.push([1,1,1,2,1]);
	grid.push([-1,0,-2,0,11]);
	grid.push([1,2,0,0,12]);
	grid.push([0,1,0,0,1]);
	grid.push([0,0,1,0,13]);
	grid.push([0,-2,0,0,2]);
	grid.push([1,14,1,0,0]);
	grid.push([1,2,0,0,0]);
	grids.push(PadGrid(grid));
	
	grid = new Array();
	grid.push([0,0,0,13,2]);
	grid.push([1,1,2,0,0]);
	grid.push([1,-2,0,0,1]);
	grid.push([0,0,0,0,1]);
	grid.push([-1,2,1,0,14]);
	grid.push([0,0,0,-2,12]);
	grid.push([1,1,11,0,0]);
	grid.push([1,1,2,0,0]);
	grids.push(PadGrid(grid));
	
	grid = new Array();
	grid.push([1,1,13,0,1]);
	grid.push([1,0,11,1,2]);
	grid.push([1,1,2,0,-2]);
	grid.push([0,2,-1,1,11]);
	grid.push([1,0,-2,0,1]);
	grid.push([0,1,0,0,14]);
	grid.push([1,2,1,0,0]);
	grid.push([1,1,12,0,0]);
	grids.push(PadGrid(grid));
	
	// level 3
	
	grid = new Array();
	grid.push([1,1,-1,13,2]);
	grid.push([1,0,2,2,1]);
	grid.push([0,0,1,12,0]);
	grid.push([1,0,0,0,-2]);
	grid.push([1,2,0,0,11]);
	grid.push([0,12,1,-1,2]);
	grid.push([0,0,12,0,0]);
	grid.push([1,14,2,-2,0]);
	grids.push(PadGrid(grid));
	
	grid = new Array();
	grid.push([0,0,11,14,2]);
	grid.push([1,1,-2,2,0]);
	grid.push([1,1,0,1,-1]);
	grid.push([0,0,0,1,14]);
	grid.push([1,1,14,2,13]);
	grid.push([-2,0,0,2,1]);
	grid.push([1,2,-1,12,0]);
	grid.push([1,1,2,0,0]);
	grids.push(PadGrid(grid));
	
	grid = new Array();
	grid.push([1,1,13,-1,11]);
	grid.push([1,2,2,0,0]);
	grid.push([1,2,1,11,0]);
	grid.push([0,-2,-1,1,2]);
	grid.push([0,0,1,1,2]);
	grid.push([0,1,0,2,12]);
	grid.push([0,1,1,0,-2]);
	grid.push([13,14,1,0,0]);
	grids.push(PadGrid(grid));
	
}

function PadGrid(g)
{
	g.unshift([0,0,0,0,0]);
	g.push([0,0,0,0,0]);
	for (var i = 0;i < g.length;i++) g[i].unshift(0,0);
	return g;
}



