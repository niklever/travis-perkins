<?php
class Api extends Controller
{
	  function Api()
    {
        parent::Controller();
        $this->load->model('user_model');
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: *");
		//header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
		header('Access-Control-Allow-Headers: Authorization');
		header('Content-Type: application/json');
    }

    function index(){
	//echo create_acesstoken();;
   }

 
  
function login(){
    	$email = $this->input->post('email'); 
    	$password = $this->input->post('password');
		$user_info = $this->user_model->is_exist_user($email, $password);			
		  if (!$user_info ){ 
         	 api_return('ERROR', 'Email or password is invalid');
          }else{
		     $user_info->access_token=create_acesstoken();
			 $data = array();
			 $data['access_token'] =$user_info->access_token ;
			 $this->user_model->update_user($user_info->id,$data);
			  api_return('OK','Login success',$user_info);
		  }
             
    	
     }
	 
function fblogin(){
		$facebook_id = $this->input->post('facebook_id'); 
		$name = $this->input->post('name');
     	$email = $this->input->post('email');
     	$country = $this->input->post('country');
            
		 if (strlen(trim($facebook_id))==0 ) {
				 api_return('ERROR', 'Facebook ID is invalid. Please try again.');
			} 
		$user_info = $this->user_model->get_facebook_user($facebook_id);
		if($user_info){
			$user_info->access_token=create_acesstoken();
			 $data = array();
			 $data['access_token'] =$user_info->access_token ;
			 $this->user_model->update_user($user_info->id,$data);
			  api_return('OK','Login success',$user_info);
		}else{	  
			if (strlen(trim($email))==0 ) {
				 api_return('ERROR', 'Email is invalid. Please try again.');
			}
			if (strlen(trim($name))==0 ) {
				 api_return('ERROR', 'Name is required. Please try again.');
			}
			/*if (strlen(trim($password))==0 ) {
				 api_return('ERROR', 'Password is not blank. Please try again.');
			}*/
			if ($this->user_model->is_exist_email($email)){
				api_return('ERROR', 'This Email is existed. Please try an other.');
			}
			
			$user = array();
			$user['email'] = mysql_real_escape_string($email);
			$user['name'] = mysql_real_escape_string($name);
			if(isset($country))
			$user['country_id'] = $country;
			$user['fb_id'] = $facebook_id;
			$user['access_token'] =create_acesstoken();
			$user['created_time'] = gmdate('Y-m-d H:i:s');
			$id=$this->user_model->insert_user($user);
			$user_info=$this->user_model->get_user($id);
			
			/*$emlobj = new stdClass();
			$emlobj->name = $name;
			$emlobj->email = $email;
			$emlobj->facebook_id = $facebook_id;
			$email_info = prepareEmail('notice-new-account-by_facebook', $emlobj);
			sendemail($email, 'Your have just created account on Travis Perkins', $email_info['body']);*/
		
			api_return('OK','Login success',$user_info);
         }    
    	
     }
	 
	 function twlogin(){
		$twitter_id = $this->input->post('twitter_id'); 
		$name = $this->input->post('name');
     	$email = $this->input->post('email');
     	$country = $this->input->post('country');
         if (strlen(trim($twitter_id))==0 ) {
				 api_return('ERROR', 'Twitter ID is invalid. Please try again.');
			} 
			
		$user_info = $this->user_model->get_twitter_user($twitter_id);
		if($user_info){
			$user_info->access_token=create_acesstoken();
			 $data = array();
			 $data['access_token'] =$user_info->access_token ;
			 $this->user_model->update_user($user_info->id,$data);
			  api_return('OK','Login success',$user_info);
		}else{	  
			if (strlen(trim($email))==0 ) {
				 //api_return('ERROR', 'Email is invalid. Please try again.');
				 $json = new stdClass();
				$json->status = 'ERROR';
				$json->code =400;
				$json->message = 'This twitter account is not existed on the DB';
				api_return_message($json);
				exit();	
			}
			if (strlen(trim($name))==0 ) {
				 api_return('ERROR', 'Name is required. Please try again.');
			}
			
			if ($this->user_model->is_exist_email($email)){
				api_return('ERROR', 'This Email is existed. Please try an other.');
			}
			
			$user = array();
			$user['email'] = mysql_real_escape_string($email);
			$user['name'] = mysql_real_escape_string($name);
			if(isset($country))
			$user['country_id'] = $country;
			$user['twitter_id'] = $twitter_id;
			$user['access_token'] =create_acesstoken();
			$user['created_time'] = gmdate('Y-m-d H:i:s');
			$id=$this->user_model->insert_user($user);
			$user_info=$this->user_model->get_user($id);
			api_return('OK','Login success',$user_info);
         }    
    	
     }

 function reset_pass(){
	$email = $this->input->post('email'); 
	if(!$this->user_model->is_exist_email(trim($email))){
		 api_return('ERROR', 'Email is not existed. Please try again.');
	}	
	$this->user_model->resetPassword(trim($email));		
	//api_return('OK','New password has been sent to your email.');//,$user_info);
	// header('Content-Type: application/json');
		$json = new stdClass();
		$json->status = 'OK';
		$json->message = 'New password has been sent to your email.';
		//echo json_encode($json);
		api_return_message($json);
		exit();
	
 }

 function signup(){
        
     	$facebook_id = $this->input->post('facebook_id'); 
		$twitter_id = $this->input->post('twitter_id'); 
     	$name = $this->input->post('name');
     	$email = $this->input->post('email');
     	$password = $this->input->post('password');
     	$country = $this->input->post('country');
     	
		if (strlen(trim($email))==0 ) {
			 api_return('ERROR', 'Email is invalid. Please try again.');
		}
		if (strlen(trim($name))==0 ) {
				 api_return('ERROR', 'Name is required. Please try again.');
			}
		if (strlen(trim($password))==0 ) {
			 api_return('ERROR', 'Password is not blank. Please try again.');
		}
		if ($this->user_model->is_exist_email($email)){
			api_return('ERROR', 'This Email is existed. Please try an other.');
		}
			
        /*if ($facebook_id){
       		$user_info = $this->user_model->get_facebook_user($facebook_id);
			if($user_info)
			    api_return('ERROR', 'This Facebook user is existed. ');
			
	    } else if ($twitter_id){
       		$user_info = $this->user_model->get_twitter_user($twitter_id);
			  if($user_info)
			    api_return('ERROR', 'This Facebook user is existed. ');
        } */ 
		$user = array();
		$user['email'] = mysql_real_escape_string($email);
		$user['password'] = md5( mysql_real_escape_string($password) );
		$user['name'] = mysql_real_escape_string($name);
		$user['access_token'] =create_acesstoken();
		if(isset($country))
		$user['country_id'] = $country;
		if(isset($facebook_id))
		$user['fb_id'] = $facebook_id;
		if(isset($twitter_id))
		$user['twitter_id'] = $twitter_id;
		$user['created_time'] = gmdate('Y-m-d H:i:s');
		$id=$this->user_model->insert_user($user);
		$user_info=$this->user_model->get_user($id);
		
		$emlobj = new stdClass();
        $emlobj->name = $name;
        $emlobj->email = $email;
        $emlobj->password = $password;
        $email_info = prepareEmail('notice-new-account', $emlobj);
        sendemail($email, 'Your have just created account on Travis Perkins', $email_info['body']);
		
		api_return('OK','Your account has been created.',$user_info);
	        
       
    }
	
	
 function countries(){
	//$countries=$this->user_model->getCountries();	
	$countries=get_country();
	api_return('OK','',$countries);

}
	

function logout(){
	//$user_id = $this->input->post('user_id'); 
	//$access_token = $this->input->post('access_token');
	$user_id = $this->input->server('PHP_AUTH_USER');
    $access_token = $this->input->server('PHP_AUTH_PW');
	
	$this->check_user_session($user_id,$access_token);
	
	 $data = array();
	 $data['access_token'] ='';
	 $this->user_model->update_user($user_id,$data);
	 //api_return('OK','Logout success');
	// header('Content-Type: application/json');
		$json = new stdClass();
		$json->status = 'OK';
		$json->message = 'Logout success';
		api_return_message($json);
		//echo json_encode($json);
		exit();
	
 } 

 function leaderboards(){
 	$game_id = $this->input->post('game_id'); 
	//$user_id = $this->input->server('PHP_AUTH_USER');
   // $access_token = $this->input->server('PHP_AUTH_PW');
	//$this->check_user_session($user_id,$access_token);
	
	$leaderboards=$this->user_model->getLeaderboards($game_id);	
	//$myscore=$this->user_model->getMyScore($game_id,$user_id);	
	
	
	$json = new stdClass();
	$json->status = 'OK';
	$json->leaderboards = $leaderboards;
	//$json->your_position = $myscore;

	api_return_message($json);
	exit();
}
 
 function init_game_session(){
	//$user_id = $this->input->post('user_id'); 
	//$access_token = $this->input->post('access_token');
	$game_id = $this->input->post('game_id');
	$user_id = $this->input->server('PHP_AUTH_USER');
    $access_token = $this->input->server('PHP_AUTH_PW');
	/*if (empty(user_id)) {
	   header('Access-Control-Allow-Origin:*');
       header('HTTP/1.0 401 Unauthorized');
       header('HTTP/1.1 401 Unauthorized');
       header('WWW-Authenticate: Basic realm="My Realm"');
       echo 'You must login to use this service'; // User sees this if hit cancel
       die();
    }*/
	
	
	$this->check_user_session($user_id,$access_token);
	
	$data = array();
	$data['user_id'] =$user_id;
	$data['game_id'] =$game_id;
	$data['total_score'] =0;
	$data['created_time'] = gmdate('Y-m-d H:i:s');
	$game_session=$this->user_model->create_game_session($data);
	if($game_session){
		//header('Content-Type: application/json');
		$json = new stdClass();
		$json->status = 'OK';
		$json->message = 'New game session created';
		$json->game_session = $game_session;
		$json->score = 0;
		//echo json_encode($json);
		api_return_message($json);
		exit();
	}else{
	     api_return('ERROR', 'Bad request');
	}
	
}


function inc_game_score(){
	//$user_id = $this->input->post('user_id'); 
	//$access_token = $this->input->post('access_token');
	$game_id = $this->input->post('game_id');
	$session_id = $this->input->post('session_id');
	$inc_score = $this->input->post('inc_score');
	
	$user_id = $this->input->server('PHP_AUTH_USER');
    $access_token = $this->input->server('PHP_AUTH_PW');
	
	$this->check_user_session($user_id,$access_token);
	
	$data = array();
	$data['session_id'] =$session_id;
	$data['score'] =$inc_score;
	$data['created_time'] = gmdate('Y-m-d H:i:s');
	$score_id=$this->user_model->add_game_score($data);
	if($score_id){
		//header('Content-Type: application/json');
		$json = new stdClass();
		$json->status = 'OK';
		$json->message = 'OK';
		$json->session_id = $session_id;
		$json->score =$this->user_model->get_current_score( $session_id);
		//echo json_encode($json);
		api_return_message($json);
		exit();
	}else{
	     api_return('ERROR', 'Bad request');
	}
	
}


function game_over(){
	//$user_id = $this->input->post('user_id'); 
//	$access_token = $this->input->post('access_token');
	$game_id = $this->input->post('game_id');
	$session_id = $this->input->post('session_id');
	$score = $this->input->post('score');
	$user_id = $this->input->server('PHP_AUTH_USER');
    $access_token = $this->input->server('PHP_AUTH_PW');
	
	$this->check_user_session($user_id,$access_token);
	
	$sum_score =$this->user_model->get_current_score($session_id);
	if($sum_score !=$score){
	    api_return('ERROR', 'Bad request');
	}
	
	$badge_info=new stdClass();
	$data = array();
	$data['total_score'] =$score;
	if($score<2000){
	    $badge_info->badge_status=1;
		$badge_info->message='Did not earn badge.';
	  
	}else{
	   $data['win_badge'] =1;
	   $current_win_badge=$this->user_model->get_current_win_badge($user_id,$game_id);
       if($current_win_badge>0){
	        $badge_info->badge_status=2;
			$badge_info->message='Already earn badge.';
	   }else{
	   		 $badge_info->badge_status=3;
			 $badge_info->message='Earned New Badge.';
	   }
	   $my_badges=$this->user_model->get_user_badges($user_id);	 
       if($my_badges>=4){
	       $badge_info->badge_status=4;
		  $badge_info->message='Earned all 4 badges.';
	   } 
	}
	$result=$this->user_model->update_game_over($session_id,$data);
	
	if($result){
		
		$json = new stdClass();
		$json->status = 'OK';
		$json->message = 'OK';
		$json->total_score = $score;
		//$badge_info=new stdClass();
		//$badge_info->badge_status=$win_badge;
		//$badge_info->message=get_win_msg($win_badge);
		$json->badge_info = $badge_info;
		api_return_message($json);
		exit();
	}else{
	     api_return('ERROR', 'Bad request');
	}
	
}

function user_profile(){
	header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: *");
	$user_id = $this->input->server('PHP_AUTH_USER');
    $access_token = $this->input->server('PHP_AUTH_PW');
	
	$user_info = $this->user_model->check_session($user_id,$access_token);			
	if (!$user_info ) {
		 api_return('ERROR', 'Invalid access token  or access token has expired');
     }
	 
    $games=array();
	for($i=1;$i<=4;$i++){
	  $current_win_badge=$this->user_model->get_current_win_badge($user_id,$i);
	  $badge_info = new stdClass();
	  $badge_info->id=$i;
	  $badge_info->earnedBadge=($current_win_badge>0)?1:$current_win_badge;
	  array_push($games,$badge_info);
     }
		
		$json = new stdClass();
		$json->status = 'OK';
		$json->message = 'User is valid';
		$data=new stdClass();
		$data->user_id=$user_id;
		$data->access_token=$access_token;
		$data->email=$user_info->email;
		$data->name=$user_info->name;
		$data->games=$games;
		$json->data = $data;
		api_return_message($json);
		exit();
	
}



function check_user_session($user_id,$access_token){
	 $user_info = $this->user_model->check_session($user_id,$access_token);			
	if (!$user_info ) 
		 api_return('ERROR', 'Invalid access token  or access token has expired');
}
	
}
?>