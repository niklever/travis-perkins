var loginLocal = {
    objectHtmlLogin: '#form_login',
    objectHtmlCreateAcount: '#form_create_account',
    objectHtmlLostPassword: '#form_lost_password',
    drawingInputBoxLogin: function () {
        if (!$(this.objectHtmlLogin).length) {
            var TagFormLogin, TagInputUserNameLogin, TagInputPasswordLogin;

            TagFormLogin = document.createElement('form');
            TagFormLogin.id = 'form_login';
            TagFormLogin.style.position = "absolute";
            TagFormLogin.style.top = 0;
            TagFormLogin.style.left = 0;



            TagInputUserNameLogin = document.createElement('input');
            TagInputUserNameLogin.setAttribute("placeholder", "Email");
            TagInputUserNameLogin.id = 'userName_login';
            TagFormLogin.appendChild(TagInputUserNameLogin);
            TagFormLogin.style.width = '';


            TagInputPasswordLogin = document.createElement('input');
            TagInputPasswordLogin.setAttribute("placeholder", "Password");
            TagInputPasswordLogin.id = 'userPassword_login';
            TagInputPasswordLogin.setAttribute("type", "password");
            TagFormLogin.appendChild(TagInputPasswordLogin);

            $(defineValueDefault.wrapContainer).append(TagFormLogin);

            var domHtml = new createjs.DOMElement(TagFormLogin);
            domHtml.x = 39.05 + 193.15 + parseInt(defineValueDefault.loadPositionCanvas().x);
            domHtml.y = 232.4 + 59.6 + parseInt(defineValueDefault.loadPositionCanvas().y);
            stage.addChild(domHtml);
            stage.update();
        }
    },
    drawingCreateNewAccount: function (flag) {
        console.log(flag);
        var TagForm, TagInputName, TagInputEmail, TagInputEmailCofirm, TagInputPasword, TagInputPaswordConfirm, TagInputSelect, TagSubmit , option;

        TagForm = document.createElement('form');
        TagForm.id = 'form_create_account';
        if(flag==false){
            TagForm.className  = 'form_create_account_social';
        }
        TagForm.style.position = "absolute";
        TagForm.style.top = 0;
        TagForm.style.left = 0;


        TagInputName = document.createElement('input');
        TagInputName.setAttribute("placeholder", "Name");
        TagInputName.id = 'userName_creatAccount';

        TagInputEmail = document.createElement('input');
        TagInputEmail.setAttribute("placeholder", "Email");
        TagInputEmail.id = 'email_creatAccount';

        TagInputEmailCofirm = document.createElement('input');
        TagInputEmailCofirm.setAttribute("placeholder", "Confirm Email");
        TagInputEmailCofirm.id = 'emailcofirm_creatAccount';

        TagInputPasword = document.createElement('input');
        TagInputPasword.setAttribute("placeholder", "Password");
        TagInputPasword.setAttribute("type", "password");
        TagInputPasword.id = 'password_creatAccount';

        TagInputPaswordConfirm = document.createElement('input');
        TagInputPaswordConfirm.setAttribute("placeholder", "Confirm Password");
        TagInputPaswordConfirm.setAttribute("type", "password");
        TagInputPaswordConfirm.id = 'passwordcofirm_creatAccount';

        
        // TagSubmit  = document.createElement('a');
        // TagSubmit.id = 'submit_createAccount';
        // TagSubmit.setAttribute("href", "javascript:void(0)");
        
        TagInputSelect = document.createElement('select');
        TagInputSelect.id = "selectCountry";
        option = document.createElement('option');
        option.setAttribute('value', 0);
        option.appendChild(document.createTextNode('Select County'));
        TagInputSelect.appendChild(option);
        /* Call API countries for select box */
        apiHelper.countries();

        TagForm.appendChild(TagInputName);
        TagForm.appendChild(TagInputEmail);
        TagForm.appendChild(TagInputEmailCofirm);
        if(flag!=false){
            TagForm.appendChild(TagInputPasword);
            TagForm.appendChild(TagInputPaswordConfirm);
        }
        TagForm.appendChild(TagInputSelect);
        // TagForm.appendChild(TagSubmit);

        $(defineValueDefault.wrapContainer).append(TagForm);
        var domHtml = new createjs.DOMElement(TagForm);

        domHtml.x = 39.05 + 193.15 + parseInt(defineValueDefault.loadPositionCanvas().x);
        domHtml.y = 135 + parseInt(defineValueDefault.loadPositionCanvas().y);
        stage.addChild(domHtml);
        stage.update();

    },

    drawingLostPassword: function () {
        if (!$(this.objectHtmlLostPassword).length) {
            var TagForm, TagEmail;
            TagForm = document.createElement('form');
            TagForm.id = 'form_lost_password';
            TagForm.style.position = "absolute";
            TagForm.style.top = 0;
            TagForm.style.left = 0;

            TagEmail = document.createElement('input');
            TagEmail.setAttribute("placeholder", "Email");
            TagEmail.id = 'email_LostPassword';

            TagForm.appendChild(TagEmail);
            $(defineValueDefault.wrapContainer).append(TagForm);
            var domHtml = new createjs.DOMElement(TagForm);

            domHtml.x = 43.05 + 193.15 + parseInt(defineValueDefault.loadPositionCanvas().x);
            domHtml.y = 234 + parseInt(defineValueDefault.loadPositionCanvas().y);
            stage.addChild(domHtml);
            stage.update();
        }
    },

    removeInputBoxLogin: function () {
        if ($(this.objectHtmlLogin).length || $(this.objectHtmlCreateAcount).length || $(this.objectHtmlLostPassword).length) {
            $(this.objectHtmlLogin).remove();
            $(this.objectHtmlCreateAcount).remove()
            $(this.objectHtmlLostPassword).remove();

        }
        return false;
    },

    creatAccount: function () {
        
        var params, _username, _email, _password , email_cofirm , password_cofirm , _country;
        _username = $('#userName_creatAccount').val();
        _email = $('#email_creatAccount').val();
        _password = $('#password_creatAccount').val();
        email_cofirm = $("#emailcofirm_creatAccount").val();
        password_cofirm = $("#passwordcofirm_creatAccount").val();
        _country = $("#selectCountry").val();
      
        if (_username == "") {
            apiHelper.callAlert("sign_up", "Please enter your name!");
            return false;
        }
        if(_email == "" || email_cofirm == "" || _email != email_cofirm){
            //alert('Please confirm your email');
            apiHelper.callAlert("sign_up", "Email does not matched. Please try again!");
            return false;
        }else if(_password == "" || password_cofirm == "" || password_cofirm != _password){
            // alert('Please cofirm your password');
            apiHelper.callAlert("sign_up", "Password does not matched. Please try again!");
            return false;
        }
        /*}else if(_password.length<6) {
            alert('Your password must have more than 6 characters');
            return false;
        }*/else if(_country ==0){
            // alert('Please choose your country');
            apiHelper.callAlert("sign_up", "Please select your county!");
            return false;
        }else{
            params = {
                'name': _username,
                'email': _email,
                'password': _password
            };
        }
        

        params = {
                'name': _username,
                'email': _email,
                'password': _password,
                'country': _country
            };

        apiHelper.signUp(params,exportRoot);
        return false;
    },
    
    resetPassword : function(){
        var _email , params;
        _email = $("#email_LostPassword").val();
        params = {
            'email' :  _email
        };
        apiHelper.resetPass(params);
        return false;
    },
    
    loginUser : function(exportRoot){
        var _email , _password ;
        _email  = $("#userName_login").val();
        _password = $('#userPassword_login').val();
        params = {
            'email' :  _email,
            'password'  : _password
        };
        apiHelper.login(params,exportRoot);
        return false;
    },
    createNewAccountbyTwitter : function(exportRoot){
        var _email , _name , _country;
        _name = $("#userName_creatAccount").val();
        _email =  $("#email_creatAccount").val();
        _email_cofirm =  $("#emailcofirm_creatAccount").val();
        _country = $("#selectCountry").val();
        
        var params = {
            'name' : _name,
            'email' : _email,
            'country' : _country
        };
        console.log(params);
    
        if(_email_cofirm!=_email){
            alert("Please confirm email !");
        }else{
            apiHelper.twlogin(params);
        }
    }
}