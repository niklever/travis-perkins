var targetAvatar;
var avatarGameLocal = {
    avatarEventClick: function (flag) {
        exportRoot.mc_ChooseAvatarPanel.mc_AvatarSteve.addEventListener('click', function (eventListener) {
            exportRoot.mc_ChooseAvatarPanel.gotoAndStop('5');
            exportRoot.btn_SelectAvatar.AvatarDefault.gotoAndStop(0);
            avatarGameLocal.storageAvatar(1,'Steve');
            exportRoot.gotoAndPlay('ChooseAvatarClose'); //27
        })
        
        exportRoot.mc_ChooseAvatarPanel.mc_AvatarGreg.addEventListener('click', function (eventListener) {
            exportRoot.mc_ChooseAvatarPanel.gotoAndStop('15');
            exportRoot.btn_SelectAvatar.AvatarDefault.gotoAndStop(1);
            avatarGameLocal.storageAvatar(2, 'Greg');
            exportRoot.gotoAndPlay('ChooseAvatarClose'); //27
        })
        
        exportRoot.mc_ChooseAvatarPanel.mc_AvatarJane.addEventListener('click', function (eventListener) {

            exportRoot.mc_ChooseAvatarPanel.gotoAndStop('20');
            exportRoot.btn_SelectAvatar.AvatarDefault.gotoAndStop(2);
            avatarGameLocal.storageAvatar(3, 'Jane');
            exportRoot.gotoAndPlay('ChooseAvatarClose'); //27

        })
        

        exportRoot.mc_ChooseAvatarPanel.mc_AvatarKate.addEventListener('click', function (eventListener) {
            exportRoot.mc_ChooseAvatarPanel.gotoAndStop('25');
            exportRoot.btn_SelectAvatar.AvatarDefault.gotoAndStop(3);
            avatarGameLocal.storageAvatar(4, 'Kate');
            exportRoot.gotoAndPlay('ChooseAvatarClose'); //27

        })
        
        exportRoot.mc_ChooseAvatarPanel.mc_AvatarJason.addEventListener('click', function (eventListener) {

            exportRoot.mc_ChooseAvatarPanel.gotoAndStop('32');
            exportRoot.btn_SelectAvatar.AvatarDefault.gotoAndStop(4);
            avatarGameLocal.storageAvatar(5, 'Jason');
            exportRoot.gotoAndPlay('ChooseAvatarClose'); //27

        })

        exportRoot.mc_ChooseAvatarPanel.mc_AvatarFrank.addEventListener('click', function (eventListener) {
            exportRoot.mc_ChooseAvatarPanel.gotoAndStop('38');
            exportRoot.btn_SelectAvatar.AvatarDefault.gotoAndStop(5);
            avatarGameLocal.storageAvatar(6, 'Frank');
            exportRoot.gotoAndPlay('ChooseAvatarClose'); //27

        })

        exportRoot.mc_ChooseAvatarPanel.mc_AvatarLucy.addEventListener('click', function (eventListener) {
            exportRoot.mc_ChooseAvatarPanel.gotoAndStop('45');
            exportRoot.btn_SelectAvatar.AvatarDefault.gotoAndStop(6);
            avatarGameLocal.storageAvatar(7, 'Lucy');
            exportRoot.gotoAndPlay('ChooseAvatarClose'); //27

        })

        exportRoot.mc_ChooseAvatarPanel.mc_AvatarPeppa.addEventListener('click', function (eventListener) {
            exportRoot.mc_ChooseAvatarPanel.gotoAndStop('50');
            exportRoot.btn_SelectAvatar.AvatarDefault.gotoAndStop(7);
            avatarGameLocal.storageAvatar(8, 'Peppa');
            exportRoot.gotoAndPlay('ChooseAvatarClose'); //27

        })
    },
    storageAvatar: function (ID,name) {
        $.localStorage.set('storageIDAvatar', ID);
        $.localStorage.set('storageNameAvatar',name);
    }

}