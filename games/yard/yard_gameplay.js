var data;
var house;
var highlights;
var dropTarget;
var remaining;

function gamePlay()
{
	exportRoot.levelup_mc.gotoAndStop(0);
	initNames(exportRoot);
	initBaskets();
	initLevel(1);
	exportRoot.levelup_mc.btn.removeAllEventListeners();
	exportRoot.levelup_mc.btn.on("click", function(){
		exportRoot.levelup_mc.visible = false;
		exportRoot.levelup_mc.gotoAndStop(1);
		initLevel(data.level+1);
	});
	gameYard.gamePlaying = true;
	gameYard.startGameTimer();
	gameYard.gameCompleted = false;
	
	exportRoot.healthBtn.buttonMode = true;
	exportRoot.healthBtn.removeAllEventListeners();
	exportRoot.healthBtn.on("click", function(){
		exportRoot.healthBtn.gotoAndPlay("StartTalking");
	});
	//sManager.stopBackgroundMusic();
}

function initNames(container){
	for(var childName in container){      
		if(container[childName] instanceof createjs.DisplayObject) {
			container[childName].name=childName;
			//console.log(childName+" now has a name!!");
		}
	}
}

function initBaskets(){
	var mcs = [exportRoot.city_mc, exportRoot.pts_mc, exportRoot.wickes1_mc, exportRoot.wickes2_mc, exportRoot.tile_mc, exportRoot.keyline_mc, exportRoot.tp_mc];
	
	for(var i=0; i<mcs.length; i++){
		mcs[i].mouseChildren = false;
		mcs[i].org = { x:mcs[i].x, y:mcs[i].y };
		mcs[i].removeAllEventListeners();
		mcs[i].on("pressmove", dragBasket);
		mcs[i].on("pressup", endDrag);
	}
}

function dragBasket(evt){
	evt.target.x = evt.stageX;
	evt.target.y = evt.stageY;
	hideHighlights();
	for(var i=0; i<highlights.length; i++){
		var pt = highlights[i].globalToLocal(evt.stageX, evt.stageY);
		if (highlights[i].hitTest(pt.x, pt.y)){
			if (data.level!=3) highlights[i].gotoAndStop(1);
			dropTarget = highlights[i].name;	
			break;
		}
	}
}

function endDrag(evt){
	if (evt.currentTarget.x==evt.currentTarget.org.x && evt.currentTarget.x==evt.currentTarget.org.x){
		//Not moved so show the tooltip
		evt.currentTarget.tooltip_mc.visible = true;
		setTimeout(function(){ hideTooltip(evt.currentTarget.tooltip_mc);}, 2000);
	}else{
		evt.currentTarget.x = evt.currentTarget.org.x;
		evt.currentTarget.y = evt.currentTarget.org.y;
		if (dropTarget!=""){
			var result = checkLegalDrop(evt.currentTarget);
			if (result.legal){
				if (result.mc==null){
					//No mc set we need to increment the main house mc
					gameYard.incScore(500);
					nextFrame(house);
					nextFrame(evt.currentTarget);
					data.house++;
					if (data.pipes>0){
						if (data.level==3){
							if (data.pipes==2){
								if (dropTarget=="pipehl_mc"){
									house.gotoAndStop(1);
									data.house = 1;
								}else{
									house.gotoAndStop(2);
									data.house = 2;
								}
							}else{
								house.gotoAndStop(3);
								data.house = 3;
							}
						}
						data.pipes--;
					}else if (data.bricks>0){
						data.bricks--;
					}
					sManager.playRightChoose(false);
					exportRoot.worker.children[0].gotoAndPlay("Correct_No_Speech");
				}else{
					if (result.mc.currentFrame<(result.mc.totalFrames-1)){
						gameYard.incScore(500);
						nextFrame(result.mc);
						nextFrame(evt.currentTarget);
						switch (result.mc.name){
							case "kitchen_mc":
								data.kitchen++;
								break;
							case "bathroom_mc":
								data.bathroom++;
								break;
							case "spare_mc":
								data.spare++;
								break;
						}
						sManager.playRightChoose(false);
						exportRoot.worker.children[0].gotoAndPlay("Correct_No_Speech");
						if (checkHouseComplete()){
							if (data.level==3){
								gameYard.gamePlaying = false;
                				gameYard.gamePause = true;
								gameYard.gameCompleted = true;
								setTimeout(gameYard.gameOver, 2000);
							}else{
								setTimeout(showLevelUp, 2000);
							}
							exportRoot.letsbuild_mc.visible = true;
							exportRoot.letsbuild_mc.gotoAndStop(3);
							house.gotoAndPlay("finished");
							setLetsBuild("complete");
						}else if (checkRemaining()==2){
							setLetsBuild("nearly");
						}
					}else{
						exportRoot.worker.children[0].gotoAndPlay("Incorrect_No_Speech");
						sManager.playWrongChoose(false);
						showBoss(true, 11);
					}
				}
			}else{
				sManager.playWrongChoose(false);
				showBoss(true, result.bossframe);
			}
		}
		hideHighlights();
	}
}

function checkRemaining(){
	var remaining = 0;
	
	remaining += (house.kitchen_mc.totalFrames - house.kitchen_mc.currentFrame - 1);
	remaining += (house.bathroom_mc.totalFrames - house.bathroom_mc.currentFrame - 1);
	if (data.level!=1) remaining += (house.spare_mc.totalFrames - house.spare_mc.currentFrame - 1);
	
	return remaining;
}

function showLevelUp(){
	exportRoot.levelup_mc.visible = true;
	exportRoot.letsbuild_mc.visible = false;
}

function checkHouseComplete(){
	var result = true;
	if (house.kitchen_mc.currentFrame!=(house.kitchen_mc.totalFrames-1)) result = false;
	if (house.bathroom_mc.currentFrame!=(house.bathroom_mc.totalFrames-1)) result = false;
	if (data.level>1){
		if (house.spare_mc.currentFrame!=(house.spare_mc.totalFrames-1)) result = false;
	}
	return result;
}

function nextFrame(mc){
	mc.gotoAndStop(mc.currentFrame + 1);
}

function checkLegalDrop(mc){
	console.log("checkLegalDrop " + mc.name + " dropTarget:" + dropTarget);
	var result = { legal:false, mc:null, bossframe:0 };
	if (mc.currentFrame==(mc.totalFrames-1)){
		result.bossframe = 12;
	}else if (data.pipes>0){
		//Only legal drop is a pipe
		result.bossframe = (house.currentFrame==0) ? 0 : 1;
		if (data.level==3 && data.pipes<3) result.bossframe = 2;
		if (dropTarget.substr(0, 4) == "pipe" && mc.name=="keyline_mc"){
			result.legal = true;
		}
	}else if (data.bricks>0){
		result.bossframe = ((data.level==1 && data.bricks==1)||(data.level>1 && data.bricks!=3)) ? 4 : 3;
		result.bossframe = 3;
		if (dropTarget.substr(0, 5) == "house" && mc.name=="tp_mc"){
			result.legal = true;
		}
	}else{
		if (dropTarget.substr(0, 7) == "kitchen"){
			if (house.kitchen_mc.currentFrame==(house.kitchen_mc.totalFrames-1)){
				result.bossframe = 11;
			}else{
				switch(data.kitchen){
					case 0:
					if (data.level==1){//Install
						result.bossframe=7;
						if (mc.name=="wickes1_mc"){
							result.legal = true;
							result.mc = house.kitchen_mc;
						}
					}else{//Paint
						result.bossframe=5;
						if (mc.name=="wickes2_mc"){
							result.legal = true;
							result.mc = house.kitchen_mc;
						}	
					}
					break;
					case 1:
					if (data.level==1){//Boiler
						result.bossframe=7;
						if (mc.name=="pts_mc"){
							result.legal = true;
							result.mc = house.kitchen_mc;
						}
					}else{//Install
						result.bossframe=7;
						if (mc.name=="wickes1_mc"){
							result.legal = true;
							result.mc = house.kitchen_mc;
						}	
					}
					break;
					case 2:
					if (data.level==1){//Tile
						result.bossframe=9;
						if (mc.name=="tile_mc"){
							result.legal = true;
							result.mc = house.kitchen_mc;
						}
					}else if (data.level==2){//Boiler
						result.bossframe=7;
						if (mc.name=="pts_mc"){
							result.legal = true;
							result.mc = house.kitchen_mc;
						}
					}else{//Install
						result.bossframe=7;
						if (mc.name=="wickes1_mc"){
							result.legal = true;
							result.mc = house.kitchen_mc;
						}	
					}
					break;
					case 3:
					if (data.level==2){//Tile
						result.bossframe=9;
						if (mc.name=="tile_mc"){
							result.legal = true;
							result.mc = house.kitchen_mc;
						}
					}else if (data.level==3){//Boiler
						result.bossframe=7;
						if (mc.name=="pts_mc"){
							result.legal = true;
							result.mc = house.kitchen_mc;
						}
					}
					break;
					case 4:
					if (data.level==3){//Tile
						result.bossframe=9;
						if (mc.name=="tile_mc"){
							result.legal = true;
							result.mc = house.kitchen_mc;
						}
					}
					break;
					case 5:
					if (data.level==3){//Tile
						result.bossframe=9;
						if (mc.name=="tile_mc"){
							result.legal = true;
							result.mc = house.kitchen_mc;
						}
					}
					break;
				}
			}
		}else if (dropTarget.substr(0, 8) == "bathroom"){	
			if (house.bathroom_mc.currentFrame==(house.bathroom_mc.totalFrames-1)){
				result.bossframe = 11;
			}else{
				switch(data.bathroom){
					case 0://Must install
					result.bossframe=6;
					if (mc.name=="city_mc"){
						result.legal = true;
						result.mc = house.bathroom_mc;
					}
					break;
					case 1://Must tile
					result.bossframe=9;
					if (mc.name=="tile_mc"){
						result.legal = true;
						result.mc = house.bathroom_mc;
					}
					break;
				}
			}
		}else if (dropTarget.substr(0, 5) == "spare"){
			result.bossframe = 5;
			if (mc.name=="wickes2_mc"){
				result.legal = true;
				result.mc = house.spare_mc;
			}
		}else{
			result.bossframe = 13;
		}
	}
	return result;
}

function showTooltip(evt, show){
	hideHighlights();
	evt.currentTarget.parent.tooltip_mc.visible = true;
	setTimeout(function(){ hideTooltip(evt.currentTarget.parent.tooltip_mc);}, 2000);
}

function hideTooltip(tip){
	tip.visible = false;
}

function initLevel(lvl){
	data = { level:lvl, pipes:1, bricks:2, house:0, kitchen:0, bathroom:0, spare:0 };
	
	if (lvl>1) data.bricks++
	if (lvl==3) data.pipes++;
	
	//var mcs = [exportRoot.city_mc, exportRoot.pts_mc, exportRoot.wickes1_mc, exportRoot.wickes2_mc, exportRoot.tile_mc, exportRoot.keyline_mc, exportRoot.tp_mc];
	var frm = (lvl==3) ? 0 : 1;
	exportRoot.keyline_mc.gotoAndStop(frm);
	frm = (lvl==1) ? 1 : 0;
	exportRoot.tp_mc.gotoAndStop(frm);
	frm = (lvl==1) ? 2 : 0;
	exportRoot.wickes2_mc.gotoAndStop(frm);
	frm = (lvl==3) ? 0 : 1;
	exportRoot.wickes1_mc.gotoAndStop(frm);
	exportRoot.city_mc.gotoAndStop(0);
	exportRoot.pts_mc.gotoAndStop(0);
	frm = (lvl==3) ? 0 : 1;
	exportRoot.tile_mc.gotoAndStop(frm);
	
	var frm = Math.floor(Math.random()*3) + (lvl-1)*3 + 1;
	exportRoot.house.gotoAndStop(frm);
	
	exportRoot.level_mc.gotoAndStop(lvl-1);
	exportRoot.levelup_mc.visible = false;
	exportRoot.hsspeech_mc.visible = false;
	exportRoot.letsbuild_mc.visible = false;
	
	showBoss(false, 0);
	
	initHouse();
}

function addHouseEventListener(){
	//Only call this if we want to see rollovers when not dragging
	if (house!=null){
		house.househl_mc.removeAllEventListeners();
		house.househl_mc.gotoAndStop(0);
		house.househl_mc.on("mouseover", showHighlight, null, false, true);
		house.househl_mc.on("mouseout", showHighlight, null, false, false);
		house.kitchenhl_mc.removeAllEventListeners();
		house.kitchenhl_mc.gotoAndStop(0);
		house.kitchenhl_mc.on("mouseover", showHighlight, null, false, true);
		house.kitchenhl_mc.on("mouseout", showHighlight, null, false, false);
		house.bathroomhl_mc.removeAllEventListeners();
		house.bathroomhl_mc.gotoAndStop(0);
		house.bathroomhl_mc.on("mouseover", showHighlight, null, false, true);
		house.bathroomhl_mc.on("mouseout", showHighlight, null, false, false);
		if (data.level>1){
			house.sparehl_mc.removeAllEventListeners();
			house.sparehl_mc.gotoAndStop(0);
			house.sparehl_mc.on("mouseover", showHighlight, null, false, true);
			house.sparehl_mc.on("mouseout", showHighlight, null, false, false);
			if (data.level==3){
				house.pipe2hl_mc.removeAllEventListeners();
				house.pipe2hl_mc.gotoAndStop(0);
				house.pipe2hl_mc.on("mouseover", showHighlight, null, false, true);
				house.pipe2hl_mc.on("mouseout", showHighlight, null, false, false);
			}
		}
	}
}

function initHouse(){
	var frm = exportRoot.house.currentFrame;
	
	console.log("initHouse:" + data.level + " frm:" + frm + " house" + frm + "_mc");
	
	house = exportRoot.house["house" + frm + "_mc"];
	highlights = new Array();
	//addHouseEventListeners();
	
	if (house!=null){
		highlights.push(house.pipehl_mc);
		highlights.push(house.househl_mc);
		highlights.push(house.kitchenhl_mc);
		highlights.push(house.bathroomhl_mc);
		house.gotoAndStop(0);
		
		if (data.level>1){
			highlights.push(house.sparehl_mc);
			if (data.level==3){
				highlights.push(house.pipe2hl_mc);
			}
		}
	}
	
	//Ensure that the house has settled before calling the stop methods
	setTimeout(stopHouseMCs, 10);
}

function stopHouseMCs(){
	if (house!=null){
		initNames(house);
		house.pipehl_mc.gotoAndStop(0);
		house.househl_mc.gotoAndStop(0);
		house.kitchenhl_mc.gotoAndStop(0);
		house.bathroomhl_mc.gotoAndStop(0);
		house.gotoAndStop(0);
		house.kitchen_mc.gotoAndStop(0);
		house.bathroom_mc.gotoAndStop(0);
		if (data.level>1){
			house.sparehl_mc.gotoAndStop(0);
			house.spare_mc.gotoAndStop(0);
			if (data.level==3){
				house.pipe2hl_mc.gotoAndStop(0);
			}
		}
		setLetsBuild("start");
		
	}
}

function setLetsBuild(str){
	var anim;
	switch(str){
		case "start":
			anim = "Saying_Lets_Build_this_thing";
			exportRoot.letsbuild_mc.gotoAndStop(0);
			break;
		case "nearly":
			anim = "Saying_NearlyDone_or_AlmostThere";
			if (Math.random()>0.5){
				exportRoot.letsbuild_mc.gotoAndStop(1);
			}else{
				exportRoot.letsbuild_mc.gotoAndStop(2);
			}
			break;
		case "complete":
			anim = "Saying_Sweet_End_of_Level";
			exportRoot.letsbuild_mc.gotoAndStop(3);
			break;
	}
	exportRoot.letsbuild_mc.visible = true;
	setTimeout(hideLetsBuild, 2000);
	exportRoot.worker.children[0].gotoAndPlay(anim);
}

function hideLetsBuild(){
	exportRoot.letsbuild_mc.visible = false;
}

function hideHighlights(){
	dropTarget = "";
	if (highlights!=null && highlights.length>0){
		for(var i=0; i<highlights.length; i++) highlights[i].gotoAndStop(0);
	}
}

function showHighlight(evt, show){
	hideHighlights();
	
	if (data.level==3) return;
	
	if (show){
		evt.currentTarget.gotoAndStop(1);	
	}else{
		evt.currentTarget.gotoAndStop(0);	
	}
}

function showBoss(show, frm){
	exportRoot.boss_mc.visible = show;
	data.bossFrame = frm;
	if (show){
		exportRoot.boss_mc.gotoAndPlay(1);
	}else{
		exportRoot.boss_mc.gotoAndStop(0);
	}
}

function setBossFrame(mc){
	mc.ok_btn.removeAllEventListeners();
	mc.ok_btn.on("click", function(){ showBoss(false, 0);});
	mc.speech_mc.removeAllEventListeners();
	mc.speech_mc.on("tick", function(){ exportRoot.boss_mc.speech_mc.gotoAndStop(data.bossFrame); });
}