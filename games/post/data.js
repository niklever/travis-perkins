var comments = {
	Commercial : [
		"Over 200 people work in the commercial teams at Travis Perkins Group.",
		"The Commercial team are responsible for choosing which products we sell and at what price. They also have to negotiate the prices with our suppliers.",
		"It is vital for us to get our post quickly, many of out products are sourced from China and take a long time to arrive. This mail could be really important.",
		"TP has over 1500 suppliers to their businesses.",
		"We manufacture products ourselves for some of our brands."
	],
	Finance : [
		"Over 500 people work in our Finance function.",
		"They're responsible for things such as buying new businesses, making sure people pay us, ensuring we don't spend more money than we make, preparing statements to the city of London and preparing our budgets.",
		"We process 4.5m documents per year - receiving post is important to help us do this as quickly as possible.",
		"We spend £175m per year with our number 1 supplier of the products we sell to our customers.",
		"Finance report how well we have done each year to our shareholders."
	],
	IT: [
		"The IT department make sure all our computers are working correctly.",
		"All of our electronic communications are looked after by our IT department.",
		"When one of our computers isn't working the IT department will fix it for us.",
		"Some of our new starters will get their laptops set up by our IT function.",
		"Over 350 people are employed in our IT function."
	],
	HR : [
		"Our HR teams support our colleagues in other departments in hiring new employees.",
"HR provide training opportunities for people wishing to further develop their skills.",
"Along with other departments HR look after the health and welfare of our colleagues.",
"They offer colleagues a range of discounts off new computers, food, holidays and many more products.",
"When our business buys other businesses then HR support with this."
	],
	Marketing : [
		"Our Marketing department consistantly listen to our customer to make sure we are providing them with what they need.",
"Understanding new trends with products is a very important part of what Marketing do.",
"Creating new ideas for products and services that we offer is important and Marketing support us with this.",
"Developing relationships with our customers and responding to their inquiries is a key part of the Marketing and Communications day to day job.",
"Writing a press release, dealing with the TV channels, organising and planning company events are just part of this functions responsibilities."
	],
	SupplyChain : [
		"Our supply chain department make sure all our products are delivered to our outlets on time.",
"Managing the supply and demand of our products is a key responsibilty of this department.",
"Planning where our products will be needed is a daily activity for the Supply Chain department.",
"We have 1000's of companies that supply products to us and managing these companies is a very imortant part of this department's role.",
"We have very large warehouses that hold our products and this department has to know what products and how many are in there at all times."
	],
	HnS : [
	"We have lots of machines in the postroom and it is very important that all of our colleagues know how to use these machines in the correct and safe way",
"We make sure all of our colleagues recieve the corect training to use these machines in the postroom",
"The post room is always very busy, there are lots of machines.  Wearing the correct safety clothing and remembering the correct safety procedures is very important"
	]
};