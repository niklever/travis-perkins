var sManager = {
    bgMusic : null,
    init : function () {

    },
	playRightChoose : function (loop) {
        //LANE CHANGE
        if (!apiHelper.isSoundMute()) {
            createjs.Sound.play("Correct_Answer" , {interrupt: createjs.Sound.INTERRUPT_ANY, loop: 0});    
        }
    },
    playWrongChoose : function (loop) {
        //LANE CHANGE
        if (!apiHelper.isSoundMute()) {
            createjs.Sound.play("Wrong_Answer" , {interrupt: createjs.Sound.INTERRUPT_ANY, loop: 0});    
        }
    },
    playBGMusic : function (loop) {
        //MUSIC BED (OPTION 1)
        if (!apiHelper.isSoundMute()) {
            sManager.bgMusic = null;
            sManager.bgMusic = createjs.Sound.play("Postroom_1158330_loop" , {interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
            // sManager.bgMusic.volume = 1.0;
            console.log("play bg sound");
        }
    },
    stopBackgroundMusic: function () {
        createjs.Sound.stop(sManager.bgMusic);
    }
}