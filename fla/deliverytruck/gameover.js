// all the game over functionality to set the badges won, grafters cup and leaderboards
// plus facebook and twitter calls

var badgePanel; // reference to badge MC
var badgeState = 0; // set in GameOver(), 0 - no badge earned, 1 - already got badge, 2 - earned badge
var finalScore;
var showNewMessage = false; // set in GameOver(), show the new message button when won the grafters cup
var newmessagebtn; // reference to new message button
var newmessageInterval; // reference to new message button interval to flash on and off
var cupPanel; // reference to grafters cup MC
var cupMove = 0; // set to 1 to move up, -1 to move down

function SetupGameoverButtons() // called on Init() to initialise all the buttons at game over screens
{
    badgePanel = exportRoot.badges_mc;
    newmessagebtn = exportRoot.newmessage_btn;
    cupPanel = exportRoot.cup_mc;
    
    // badge mc buttons
    badgePanel.facebook_btn.on("mousedown", function(event) {
        console.log("facebook code goes here");
    });
    
    badgePanel.twitter_btn.on("mousedown", function(event) {
        console.log("twitter code goes here");
    });
    
    badgePanel.leaderboard_btn.on("mousedown", function(event) {
        ShowLeaderboard();
    });
    
    // new message button
    newmessagebtn.on("mousedown", function(event) {
        clearInterval(newmessageInterval);
        newmessagebtn.visible = false;
        cupMove = 1;
        createjs.Ticker.addEventListener("tick", GraftersCupLoop);        
    });
    
    // grafters cup mc buttons
    cupPanel.facebook_btn.on("mousedown", function(event) {
        console.log("facebook code goes here");
    });
    
    cupPanel.twitter_btn.on("mousedown", function(event) {
        console.log("twitter code goes here");
    });
    
    cupPanel.close_btn.on("mousedown", function(event) {
        cupMove = -1;
        createjs.Ticker.addEventListener("tick", GraftersCupLoop); 
    });   
}

function GameOver(s) // set the badges panel and show the grafters cup award if won. this is called from GotoGameOver() in game.js
{
    finalScore = s;
    exportRoot.gotoAndStop("gameover");
    topbar.gotoAndStop("gameover");    
    
    // put badge condition code in here to set badgeState and showNewMessage
        
    if (badgeState == 0) {
        badgePanel.gotoAndStop("nobadge");
    } else if (badgeState == 1) {
        badgePanel.gotoAndStop("alreadygot");
    } else if (badgeState == 2) {
        badgePanel.gotoAndStop("badge");
    }
    badgePanel.score_mc.score_txt.text = GetScoreString(finalScore);
    
    cupMove = 0;
    if (showNewMessage) {
        newmessageInterval = setInterval(NewMessageFlash, 300);
        setTimeout(StopNewMessageFlash, 2000);
    } else {
        newmessagebtn.visible = false;
    }
    
}

function NewMessageFlash() // falshes new message button on + off
{
    newmessagebtn.visible = !newmessagebtn.visible;
}

function StopNewMessageFlash() // stops flashing button
{
    clearInterval(newmessageInterval);
    if (cupMove == 0) newmessagebtn.visible = true;
}

function GraftersCupLoop() // moves grafters cup panle up and down
{
    cupPanel.y -= cupMove * 80;
    if ((cupMove == 1 && cupPanel.y < 20) || (cupMove == -1 && cupPanel.y > 460)) {
        cupPanel.y = cupMove == 1 ? 0 : 480;
        cupMove = 0;
        createjs.Ticker.removeEventListener("tick", GraftersCupLoop);
    }
}

function ShowLeaderboard() // show the game leaderboard
{
    exportRoot.gotoAndStop("leaderboard");
    topbar.gotoAndStop("leaderboard");
    
    gameMode = "leaderboard"; // used by back button in game.js to call the BackToGameOver function below
    
    // put all the leaderboard code here
}

function BackToGameOver() // return to game over screen from leaderboard
{
    exportRoot.gotoAndStop("gameover");
    topbar.gotoAndStop("gameover");    
    
    if (badgeState == 0) {
        badgePanel.gotoAndStop("nobadge");
    } else if (badgeState == 1) {
        badgePanel.gotoAndStop("alreadygot");
    } else if (badgeState == 2) {
        badgePanel.gotoAndStop("badge");
    }
    badgePanel.score_mc.score_txt.text = GetScoreString(finalScore);
}