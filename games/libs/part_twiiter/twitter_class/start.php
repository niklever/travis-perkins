<?php
session_start();
ob_start();
error_reporting(E_ALL); 
ini_set('display_errors', '0'); 
require 'config.php';
require 'tmhOAuth.php';

$tmhOAuth = new tmhOAuth(array(
    'consumer_key'    => API_KEY_TWITTER,
    'consumer_secret' => API_SEC,	
    'curl_ssl_verifypeer'   => false
));

$tmhOAuth->request('POST', $tmhOAuth->url('oauth/request_token', ''));
$response = $tmhOAuth->extract_params($tmhOAuth->response["response"]);  


$temp_token = $response['oauth_token']; 
$temp_secret = $response['oauth_token_secret']; 
$time = $_SERVER['REQUEST_TIME'];
$_SESSION['OAUTH_TOKEN'] = $temp_token;
$_SESSION['OAUTH_SECRET'] = $temp_secret;
if(isset($_GET['share'])){
    $_SESSION['isShare']=true;
}else{
    $_SESSION['isShare']=false;
}
?> 
<?php
	$url = $tmhOAuth->url("oauth/authorize", "") . '?oauth_token=' . $temp_token;	
	//die($url);
	header('Location: ' . $url);
	exit();
?>
