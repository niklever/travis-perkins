<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Loader File
 *
 * @package		YATS -- The Layout Library
 * @subpackage	Views
 * @category	Template
 * @author		Mario Mariani
 * @copyright	Copyright (c) 2006-2007, mariomariani.net All rights reserved.
 * @license		http://svn.mariomariani.net/yats/trunk/license.txt
 */
if ($this->uri->segment(1)=='admin'){
   if ($this->uri->segment(2)=='account'){
		$this->load->view($data['settings']['views'] . $data['settings']['content'] . "$view",  $data);
	}else{
		$this->load->view($data['settings']['views'] . $data['settings']['commons'] . "back_end/header", $data);
		$this->load->view($data['settings']['views'] . $data['settings']['commons'] . "back_end/sidebar", $data);
		$this->load->view($data['settings']['views'] . $data['settings']['content'] . "$view",  $data);
		$this->load->view($data['settings']['views'] . $data['settings']['commons'] . "back_end/footer", $data);
	}
/*}else {
	$this->load->view($data['settings']['views'] . $data['settings']['commons'] ."front_end/header", $data);
	$this->load->view($data['settings']['views'] . $data['settings']['content'] . "$view",  $data);
	$this->load->view($data['settings']['views'] . $data['settings']['commons'] ."front_end/footer", $data);
*/	
}	
?>