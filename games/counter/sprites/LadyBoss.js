﻿(function(window) {
LadyBoss_instance_1 = function() {
	this.initialize();
}
LadyBoss_instance_1._SpriteSheet = new createjs.SpriteSheet({images: ["LadyBoss.png"], frames: [[0,0,119,169,0,-31.1,0],[119,0,119,169,0,-31.1,0],[238,0,119,169,0,-31.1,0],[357,0,119,169,0,-31.1,0],[476,0,119,169,0,-31.1,0],[595,0,119,169,0,-31.1,0],[714,0,119,169,0,-31.1,0],[833,0,119,169,0,-31.1,0],[952,0,119,169,0,-31.1,0],[1071,0,119,169,0,-31.1,0],[1190,0,119,169,0,-31.1,0],[1071,0,119,169,0,-31.1,0],[1309,0,119,169,0,-31.1,0],[1428,0,119,169,0,-31.1,0],[1547,0,119,169,0,-31.1,0],[1666,0,119,169,0,-31.1,0],[1785,0,119,169,0,-31.1,0],[1904,0,119,169,0,-31.1,0],[2023,0,119,169,0,-31.1,0],[2142,0,119,169,0,-31.1,0],[2261,0,119,169,0,-31.1,0],[595,0,119,169,0,-31.1,0],[476,0,119,169,0,-31.1,0],[357,0,119,169,0,-31.1,0],[238,0,119,169,0,-31.1,0],[119,0,119,169,0,-31.1,0],[0,0,119,169,0,-31.1,0]]});
var LadyBoss_instance_1_p = LadyBoss_instance_1.prototype = new createjs.Sprite();
LadyBoss_instance_1_p.Sprite_initialize = LadyBoss_instance_1_p.initialize;
LadyBoss_instance_1_p.initialize = function() {
	this.Sprite_initialize(LadyBoss_instance_1._SpriteSheet);
	this.paused = false;
}
window.LadyBoss_instance_1 = LadyBoss_instance_1;
}(window));

