var gameQuestions = [{
    no: 1,
    question: "How much of this paint will I need?",
    answers: [{
        answer: "Let's take a look at the coverage per litre details on the pot",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "Oh thank you I didn't realise that's how I find out",
        awayAFTER: true,
        manager_response: "Excellent, well done for letting the customer know how to find out how much paint they will need"
    }, {
        answer: "I'm not sure, try just one pot",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Are you sure, it looks a big pot?",
        awayAFTER: true,
        manager_response: "It's important to point out to the customer about the coverage and different size of pots we offer"
    }, {
        answer: "How big is the wall because that's a big tin",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "I don't know, not measured it, should I?",
        awayAFTER: true,
        manager_response: "A good response would have been to ask the customer if they have measure the wall"
    }, {
        answer: "If you have already measured the wall we can take a look at see",
        correct: true,
        avatar_animation: "Check_Stock_Computer",
        customer_response: "Okay, let me show you the measurements I have taken",
        awayAFTER: true,
        manager_response: "That's a good starting point because the pot of paint may not be big enough, you could also ask if they need brushes to paint with"
    }, {
        answer: "Maybe two or three",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "It's expensive and if you don't really know I will leave it",
        awayAFTER: false,
        manager_response: "Taking a look at the coverage information will allow you to advise the customer how many pots they will need"
    }]
}, {
    no: 2,
    question: "I want a spade, can you recommend one?",
    answers: [{
        answer: "We have a number of different spades, what will you be using it for?",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "I am digging up my flower bed in the back garden ",
        awayAFTER: true,
        manager_response: "That's great because a general purpose spade is what the customer will want"
    }, {
        answer: "Are you going to be using it",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "Yes, why do you ask ",
        awayAFTER: true,
        manager_response: "It's always good to ask who the main user will be because we have other products that could be suited better for different people and useage "
    }, {
        answer: "I don't really know much about them",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Right, okay i will take a look in another outlet",
        awayAFTER: false,
        manager_response: "You should always be honest but when you don't know always inform the customer that you can find out and speak with a colleague who will know the answer"
    }, {
        answer: "Any of the ones we sell",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Are you sure? Some of these look very different",
        awayAFTER: true,
        manager_response: "It's important to understand exactly what the customer is going to use the spade for as some are for general digging and some for trenches "
    }, {
        answer: "This one should be fine",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "You don't sound that sure about this?",
        awayAFTER: true,
        manager_response: "Always try and remember what the customer requirements are and then match the correct product"
    }]
}, {
    no: 3,
    question: "Which of these bricks would you recommend?",
    answers: [{
        answer: "I see you have chosen three different types, what do you intend to use them for?",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "Well I am looking to build a small garden wall",
        awayAFTER: true,
        manager_response: "Well done, it's important to make sure the customer uses the correct type of brick to build with"
    }, {
        answer: "Not that one ",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Right!, thank you i will take another look",
        awayAFTER: false,
        manager_response: "You may know that the customer has chosen the wrong brick but it always helps if you can ask what the customer wants to use the brick for"
    }, {
        answer: "These are very versatile and a popular brick, how many are you going to be using ",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "Thank you, I think I will need about 300",
        awayAFTER: true,
        manager_response: "Good response because it now allows you to understand exactly how many is needed and what they are going to be used for, plus good opportunity to offer other products"
    }, {
        answer: "They will all be fine",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "I'm not sure, what are the differences ",
        awayAFTER: true,
        manager_response: "It's important that we recommend the correct bricks for the customer in terms of safety "
    }, {
        answer: "I think those would be right for you",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "I really need a little help, are you sure about these?",
        awayAFTER: true,
        manager_response: "It can be dangerous to sell the wrong bricks because we always need to understand what the customer intends to be building/using them for "
    }]
}, {
    no: 4,
    question: "What's the guarantee on this drill?",
    answers: [{
        answer: "There is no guarantee ",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Well that's not great, I'm going elsewhere to buy",
        awayAFTER: true,
        manager_response: "That drill did have a guarantee and we know this because the manufacturer offers this  "
    }, {
        answer: "Its a good drill that never goes wrong",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Thank you for that, I will take a look at some others",
        awayAFTER: false,
        manager_response: "The customer needed to know what guarantee the drill has. You may have lost a sale, always important to know and respond to the customer's question"
    }, {
        answer: "It's a long guarantee on that",
        correct: false,
        avatar_animation: "Check_Stock_Computer",
        customer_response: "Are you sure?, can you just check?",
        awayAFTER: true,
        manager_response: "Not a good response because you have placed doubt in the customers mind and if you are not sure then just inform the customer that you will find out and let them know"
    }, {
        answer: "Thats a great general purpose drill with a three year guarantee ",
        correct: true,
        avatar_animation: "Check_Stock_Computer",
        customer_response: "Thank you, I'll take this one, can you recommend some drill bits?",
        awayAFTER: true,
        manager_response: "Great response because you have confirmed the guarantee and also confirmed it's a good product."
    }, {
        answer: "The manufacturer usually states, just let me check",
        correct: true,
        avatar_animation: "Phone_Check_Stock_Computer",
        customer_response: "Thank you, if you could just confirm before I buy?",
        awayAFTER: true,
        manager_response: "That's a great answer because you have been honest and are also letting the customer know you are checking before they purchase"
    }]
}, {
    no: 5,
    question: "I like this spirit level, do you sell many?",
    answers: [{
        answer: "I don't know ",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Right, well I will have a think about it then",
        awayAFTER: false,
        manager_response: "It's always important to ask one of your colleagues if you don't know and also try and match the product to our customers requirements "
    }, {
        answer: "I have never had to use one ",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Okay, thank you for that",
        awayAFTER: false,
        manager_response: "You have been honest and told the customer you have never used a level but you still haven't answered their question. Always let the customer know that you will find the answer if you don't know it"
    }, {
        answer: "They are very expensive which is why we don't",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Right, well I will take a look at others",
        awayAFTER: false,
        manager_response: "Not a good response because we don't know how much the customer is prepared to pay and therefore we have assumed which is always dangerous"
    }, {
        answer: "Yes, they are a very popular spirit level",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "That's good to know. I will buy this one",
        awayAFTER: true,
        manager_response: "That's a good answer because it gives our customer confidence that they are purchasing the right product"
    }, {
        answer: "They are popular because they are constructed from durable material",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "I thought they looked strong and robust",
        awayAFTER: true,
        manager_response: "Really good answer because the you are confirming with the customer what they have considered to be important with their decision "
    }]
}, {
    no: 6,
    question: "Will this radiator fit on my wall?",
    answers: [{
        answer: "Yes",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Are you sure?",
        awayAFTER: true,
        manager_response: "It's important to find out the size of wall first because the radiator may be too large or small"
    }, {
        answer: "Looks about right that one",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "I thought so as well",
        awayAFTER: true,
        manager_response: "Not a good response. What would happen if the customer bought the radiator and got home to find it was the wrong size? Always ask for more detail such as measurement, where it's going etc."
    }, {
        answer: "Thats not a for a house, it's for office building",
        correct: false,
        avatar_animation: "Check_Stock_Computer",
        customer_response: "Well that's what I wanted it for.",
        awayAFTER: true,
        manager_response: "It's never good to assume or second guess what the customer is going to use it for, always ask first and then you can advise accordingly "
    }, {
        answer: "What size wall is it to be fitted onto?",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "It's 13ft long by 7ft high",
        awayAFTER: true,
        manager_response: "That's a good answer because the wall may have been too small to fit the radiator "
    }, {
        answer: "What room is the radiator for and what's the size?",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "It's 13ft long by 7ft high and the bathroom",
        awayAFTER: true,
        manager_response: "That's a great answer because the customer may have a radiator not designed for a bathroom and not large enough"
    }]
}, {
    no: 7,
    question: "How long will it take me to fit these taps?",
    answers: [{
        answer: "About one hour",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "What if I have never fitted taps before?",
        awayAFTER: true,
        manager_response: "Our taps come with a data sheet that explains how to fit and it's important to remember that each of our customers will have different skill levels"
    }, {
        answer: "Have you fitted taps before?",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "No, never",
        awayAFTER: true,
        manager_response: "Good answer because the time to fit taps can vary depending upon the skill and experience level of our customer"
    }, {
        answer: "they will look lovely in the kitchen",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Oh! I was going to use them in the bathroom",
        awayAFTER: true,
        manager_response: "Always find out exactly what the customer is going to use the taps for and which room because you have missed an opportunity to offer the correct product and maybe additional products "
    }, {
        answer: "They are the last set so I would suggest you get them now",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Well I am not deciding now, just wanted to know how long to fit",
        awayAFTER: true,
        manager_response: "The customers question never got answered. Always answer the question first and then explain that you only have one set left but ordering more is not a problem"
    }, {
        answer: "Have you fitted taps before? There are instructions and guide notes included",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "No but I have spoken to my friend and they said it was easy",
        awayAFTER: true,
        manager_response: "A great answer because you have informed the customer that the taps come with fitting instructions and guidance"
    }]
}, {
    no: 8,
    question: "Will these nails be okay for a wall?",
    answers: [{
        answer: "What type of wall will you be using the nails for?",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "It's a brick wall",
        awayAFTER: true,
        manager_response: "Excellent, it's important to understand we stock different nails for different types of wall"
    }, {
        answer: "Do you have the correct hammer for those",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Yes, thank you",
        awayAFTER: true,
        manager_response: "First start by confirming with the customer what the nails are for and then you may get the opportunity to ask if they do actually have the correct hammer"
    }, {
        answer: "They are for outdoor use only",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "That's what I want them for.",
        awayAFTER: true,
        manager_response: "The customer just wanted to know if they had the right nails, asking for what they are to be used for is the first thing to ask"
    }, {
        answer: "Yes, they do come in different sizes as well",
        correct: true,
        avatar_animation: "Check_Stock_Computer",
        customer_response: "Oh right, I didn't know that can you show me",
        awayAFTER: true,
        manager_response: "Good answer because you can now show the customer and find out what they want them for and possibly offer products to support what they are doing "
    }, {
        answer: "They look okay to me ",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "But you haven't asked me what they are for!",
        awayAFTER: true,
        manager_response: "It's important to build trust and confidence with our customers, asking what they are going to use the nails for is a great starting point to in building this trust "
    }]
}, {
    no: 9,
    question: "How much are tiles?",
    answers: [{
        answer: "They vary ",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Oh, thank you ",
        awayAFTER: true,
        manager_response: "Not a great answer as it's important to understand what the tiles are for and then important to match to the customers need and price range "
    }, {
        answer: "What room are they for?",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "The kitchen ",
        awayAFTER: true,
        manager_response: "It's a good starting point to always understand where the tiles are to be placed"
    }, {
        answer: "We have tiles that have been reduced over there",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "I have seen those, thank you I will take a look elsewhere",
        awayAFTER: true,
        manager_response: "Not a good response because finding out what the customer plans to do with the tiles was the starting point, you can then introduce a range that would be suitable"
    }, {
        answer: "The more you order could affect the price",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Right, I wasn't sure because I haven't measure the wall",
        awayAFTER: true,
        manager_response: "The customer really wants some help and support. Start by asking what they want to do, in which room and had they got measurements"
    }, {
        answer: "What room are they for and are they for the wall or floor or both?",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "The kitchen wall, so you do them for the floor as well?",
        awayAFTER: true,
        manager_response: "Great answer because you now know the customer didn't know about floor tiles and you could sell these as well as the wall tiles"
    }]
}, {
    no: 10,
    question: "Are these fence posts okay for any fence panel?",
    answers: [{
        answer: "What size is the fence panel you are using?",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "it's 6ft x 6ft ",
        awayAFTER: true,
        manager_response: "Thats a great answer because it's important to make sure the fence post is the correct size for the fence panel "
    }, {
        answer: "Yes",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "You haven't asked me the size of the fence panel",
        awayAFTER: true,
        manager_response: "Not a good answer because the customer is clearly upset that you haven't confirmed size of the fence panel."
    }, {
        answer: "They haven't been treated but are okay ",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "I see, I didn't realise they had to be treated ",
        awayAFTER: true,
        manager_response: "You have confused the customer. They wanted to know if the posts are correct, you should have answered that question first and then recommended a suitable product to preserve the post"
    }, {
        answer: "They are designed for fences that are 7ft tall, what size is your fence",
        correct: true,
        avatar_animation: "Check_Stock_Computer",
        customer_response: "Thank you, my fence is only 3ft high",
        awayAFTER: true,
        manager_response: "Really good response becuase you have saved the customer time in potentially cutting the posts down and also saved them money"
    }, {
        answer: "They are a standard post",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Reall? They look so small",
        awayAFTER: true,
        manager_response: "Alway start by asking what the product is for and what the sizes are "
    }]
}, {
    no: 11,
    question: "Will this door fit in my kitchen?",
    answers: [{
        answer: "What are the measurements of the door frame?",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "I'll get the measurements, thanks I thought they were all the same!",
        awayAFTER: true,
        manager_response: "That's a good starting point because it's important to get the right measurements form the customer as the door may have been too small/big "
    }, {
        answer: "You may need to cut a little off the bottom or side, what measurements do you have?",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "I have measurements here, do you sell saws?",
        awayAFTER: true,
        manager_response: "Good response because you have asked for measurements, informed the customer that the door may need to be cut to size and now have a sale for a saw"
    }, {
        answer: "No that is an external door",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Oh I thought any door would do",
        awayAFTER: true,
        manager_response: "It's right that you have told the customer it's the wrong door but you should have shown the customer our range of internal doors"
    }, {
        answer: "That door will need extra hinges because its very heavy",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "I have extra hinges at home. Thank you I'll take a look in another outlet",
        awayAFTER: true,
        manager_response: "You have upset the customer. Always try and understand what the customer is going to use our products for and then you can ask if they have all the accessories needed to fit the door"
    }, {
        answer: "All doors are a standard size, so should fit",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "Are you sure? Some of them look smaller",
        awayAFTER: true,
        manager_response: "The customer is doubting what you have said, if you are unsure, check with a colleague"
    }]
}, {
    no: 12,
    question: "Do you charge for delivery?",
    answers: [{
        answer: "No",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "That's unusual",
        awayAFTER: true,
        manager_response: "We should always look to at least cover our costs on deliveries to non account customers"
    }, {
        answer: "We don't do deliveries for non account customers",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "That's no good, I'm off",
        awayAFTER: false,
        manager_response: "We should always look to at least cover our costs on deliveries to non account customers"
    }, {
        answer: "Yes, the charge depends on the size of delivery and destination",
        correct: true,
        avatar_animation: "Talk_Loop",
        customer_response: "Ok that's fine",
        awayAFTER: true,
        manager_response: "Excellent, well done!"
    }, {
        answer: "I don't know, can't you fit it in the car",
        correct: false,
        avatar_animation: "Talk_Loop",
        customer_response: "My car is very small and I need a lot of bricks",
        awayAFTER: true,
        manager_response: "Not a great answer. Always look to understand what the customer wants and then we can speak about delivery options"
    }, {
        answer: "Let me check because I am not that sure",
        correct: true,
        avatar_animation: "Phone_Check_Stock_Computer_Phone",
        customer_response: "Thank you, I will wait ",
        awayAFTER: true,
        manager_response: "That's a good starting point. Always be honest if you are not sure. "
    }]
}];