<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en-us" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dapaolo Group</title>

<link href="<?php echo base_url(); ?>assets/css/core.css" media="screen" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/core.js"></script>
</head>
<body>

<div id="wrap">
   <div class="product_list">
	<?php $this->view($content); ?>
	</div>
	<div class="cart_list">
		<h3>Your shopping cart</h3>
		<div id="cart_content">
			<?php echo $this->view('cart/cart.php'); ?>
		</div>
	</div>
</div>

</body>
</html>
<script type="text/javascript">
$(document).ready(function() { 
	/*place jQuery actions here*/ 
	var link = "<?php echo base_url(); ?>";
	
	
	$("ul.products form").submit(function() {
										  
		// Get the product ID and the quantity 
		var id = $(this).find('input[name=product_id]').val();
		var qty = $(this).find('input[name=quantity]').val();
		
		 $.post(link + "cart/add_cart_item", { product_id: id, quantity: qty, ajax: '1' },
  			function(data){
  			    
			if(data == ''){
				  window.location = "index.php";

			}else if(data == 'true'){
    			
    			$.get(link + "cart/show_cart", function(cart){
  					$("#cart_content").html(cart);
				});

    		}else{
    			alert(data);
    		}	
    		
 		 }); 

		return false;
	});
	
	$(".empty").live("click", function(){
    	$.get(link + "cart/empty_cart", function(){
    		$.get(link + "cart/show_cart", function(cart){
  				$("#cart_content").html(cart);
			});
		});
		
		return false;
    });


	
	
});
</script>